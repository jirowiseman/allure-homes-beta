<?php
namespace AH;

//echo "entered page-register.php";

require_once(__DIR__.'/../_classes/States.php');
//global $usStates;

$source = get_query_var('id');
//echo $source;

$seller = null;
$user = null;
$sellerState = -1;
$password = '';
$email = '';
$trackable = false;
$bre = null;
if ( !empty($source) ) {
	if ( $source != 'seller' ) {
		if ( strpos( $source, 'U-') === 0) {
			$source = intval(substr($source, 2));
			if (wp_get_current_user()->ID != $source) {
				$h = '<!DOCTYPE html>';
				$h .= '<html><head><h2>Failed to matched logged in user with desired user</h2></head>';
				$h .= '<body><h1>User with ID: '.$source.', could not be matched with the user that is logged in ('.wp_get_current_user()->ID.').<br/>Please contact administrator</h1></body>';
				$h .= '</html>';
				echo $h;
				die;
			}
			$user = wp_get_current_user();
		}
		elseif ( strpos( $source, 'T-') === 0) {
			$source = substr($source, 2);
			require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new Reservations();
			$reservation = $Reservations->get((object)['where'=>['id'=>$source]]);
			if (!empty($reservation)) {
				$trackable = true;
				$seller = new \stdClass();
				$seller->first_name = $reservation[0]->first_name;
				$seller->last_name = $reservation[0]->last_name;
				$seller->email = $reservation[0]->email;
				$seller->flags = isset($reservation[0]->portal) && !empty($reservation[0]->portal) ? SELLER_IS_PREMIUM_LEVEL_1 : 0;

				if (!empty($reservation[0]->meta)) foreach($reservation[0]->meta as $meta) {
					$meta = (object)$meta;
					if ($meta->action == ORDER_AGENT_MATCH) {
						$item = $meta->item[0];
						$seller->location = $item->locationStr;
						$seller->flags |= SELLER_IS_PREMIUM_LEVEL_2;
						$loc = explode(',', $item->locationStr);
						$agentstate = count($loc) == 2 ? trim($loc[1]) : '';
						global $usStates;
						if (!empty($agentstate)) foreach($usStates as $state) {
							if ($state == $agentstate) {
								$sellerState = $state;
								break;
							}
						}
					}
					elseif ($meta->action == SELLER_BRE) {
						$bre = $meta;
						$bre->BRE = trim($bre->BRE);
						$bre->state = trim($bre->state);
						if (!empty($bre->state))
							$sellerState = $bre->state;
					}
				}
			}
		}
	 	elseif (is_numeric($source) ) {
			require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers();
			$seller = $Sellers->get((object)[ 'where' => ['id'=>$source] ]);
			$seller = $seller ? array_pop($seller) : null;
			if (!empty($seller)) {
				if (!empty($seller->state)) {
					global $usStates;
					foreach($usStates as $state) {
						if ($state == $seller->state) {
							$sellerState = $state;
							break;
						}
					}
				}
				if (!empty($seller->meta)) {
					foreach($seller->meta as $meta) {
						if ($meta->action == SELLER_KEY) {
							$key = json_decode($meta->key);
							$len = count($key); // strlen($meta->key);
							for($i = 0; $i < $len; $i++)
								$password .= chr( $key[$i] ^ ($i + ord('.')) );
						}
						elseif ($meta->action == SELLER_BRE) {
							$bre = $meta;
							$bre->BRE = trim($bre->BRE);
							$bre->state = trim($bre->state);
							if (!empty($bre->state))
								$sellerState = $bre->state;
						}
					}
				}
				$email = $seller->email;
			}
		}
	}
	else if (is_user_logged_in()) { // user is logged in and is trying to register as a seller.
		$id  = wp_get_current_user()->ID;
		$user = get_userdata($id);
	}
}
?>

<script type="text/javascript">
var seller = null;
var user = null;
var source = null;
var sellerState = null;
var breMeta = null;
var nanda = '<?php echo $password; ?>';
var trackable = <?php echo $trackable ? 1 : 0 ?>;
</script>

<?php if ( $seller ) : ?>
<script type="text/javascript">
//jQuery( document ).tooltip();
seller = <?php echo json_encode($seller); ?>; 
sellerState = '<?php echo $sellerState ?>';
</script>
<?php endif; 

if ( $user ) : ?>
<script type="text/javascript">
user = <?php echo json_encode($user); ?>; 
</script>
<?php endif; 

if ( !empty($source) ) : ?>
<script type="text/javascript">
source = '<?php echo $source; ?>';
</script>
<?php endif;

if ( !empty($bre) ) : ?>
<script type="text/javascript">
breMeta = <?php echo json_encode($bre); ?>; 
</script>
<?php endif; ?>

<div id="page-register">
	<section>
        <div class="top" style="display: none">
            <div class="right-col">
			     <h2>Create an Account</h2>
			     <form id="register">
			     	<div id="name">
							<label for="user_login">Name:</label>
					    <input type="text" name="first-name" placeholder="First Name" />
					    <input type="text" name="last-name" placeholder="Last Name" />
					</div>
					<div id="login">
						<label for="user_login">Username:</label>
					    <input type="text" name="login-id" class="login-id" placeholder="(Defaults to Email)" />
					</div>
					<div id="email">
							<label for="user_login">Email:</label>
					    <input type="text" name="email" placeholder="john@example.com" />
					</div>
					<div id="passwordDiv">
							<label for="user_login">Password:</label>
				    	<input type="password" name="password" id="password" placeholder="8 or more Characters" value="<?php echo $password; ?>"></input>
					</div>
					<div id="verifyDiv">
							<label for="user_login">Verify Password:</label>
				    	<input type="password" name="password" id="verify" placeholder="Match password" value="<?php echo $password; ?>"></input><span id="marker" class="entypo-attention"></span>
				  	</div>
				  	<div id="professional">
				    	<input type="checkbox" id="realtor" /> <span id="identify">I am a real estate professional </span>
				  	</div>
				    <div id="bre">
					    <label>License Number</label><input type="text" id="breID" name="realtor-id" placeholder="ID Required" />
							<select name="States" id="states">
						  	<option value="-1">State</option>
						 	<?php global $usStates; foreach($usStates as $state) { ?>
						  	<option value="<?php echo $state; ?>"><?php echo $state; ?></option>
						  	<?php } ?>
						</select>
						<span id="invite">Invite code: </span><input type="text" id="invitation_code" placeHolder="(Optional)">
						<p style="margin:0;margin-left:150px;font-size:.8em;letter-spacing:0;color:#CDEAFF">* Register with the same email the agent used in MLS</p>
					</div>
				    <input type="submit" id="submitbtn" name="submit" value="Register Agent" />
				    <div id="opt-in">
						<input type="checkbox" id="opt-in" checked/>
						<span>Agent agreed to receive emails with helpful guides and updates related to their account.</span>
					</div>
					<div id="result" style="display:inline-block;margin-left:1em"></div>
			     </form>
			 </div>
		</div>
		<div class="bottom" style="display: none">
			<span><h1>This user needs to upgrade to become an agent.<br/>Will need their BRE and state of registry.<br/>Press button when ready.</h1></span>
			<button id="upgrade">Upgrade to Agent</button>
		</div>
    </section>
</div>
