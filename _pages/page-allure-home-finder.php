<div id="allure-home-finder">
	<section>
        <div class="calltoaction"><a href="<?php bloginfo('wpurl'); ?>/quiz/" class="startbutton">START</a></div>
        <div class="top">
            <div class="text">
		        <p class="matchtitle">TELL US WHAT YOU'RE LOOKING FOR</p>
		        <p class="sub">Answer a few quick questions about you and your home needs so we can match you with the best locations and homes tailored to you.</p>
                <a href="<?php bloginfo('wpurl'); ?>/quiz/" class="startbutton">START</a>
            </div>
            <div class="take-time"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-allure-home-finder/why-take-time.png"/></div>
		</div>
        <div class="middle">
            <div class="text">
                <span class="title">Search Smarter</span>
                <p>Search for a lot more than just bedroom counts and square footage.</p>
                <span class="title">Save Time</span>
                <p>Don’t waste  your time sorting through homes that meet general specifications, but not your lifestyle.</p>
                <span class="title">Discover</span>
                <p>Find new areas that have all the things you never want to be without.</p>
                <span class="title">Have Fun</span>
                <p>Shopping for a home should be a fun experience, we’ve designed our site with that in mind.</p>
                <span class="title">Feel Confident</span>
                <p>We put our users first, protecting your personal information and inbox is just the start.</p>
            </div>
        </div>
        <div class="bottom-top">
            <div class="text">
                <span class="title">What's Important</span>
                <p>We determined the most important characteristics of ideal living and carefully designed a short series of questions that measure them. By answering these questions, you tell us exactly what your lifestyle is so we can match you to beautiful homes and locations, together in one.</p>
            </div>
        </div>
        <div class="bottom-mid">
            <div class="text">
                <span class="title">Search Smarter</span>
                <p>Searching for homes here is different than on any other site, and so is the result.  Beyond lifestyle, we use everything from the features you'd like to have to the style of your homes interior, to make your search more accurate. By constantly testing and refining this process we have created a fun, efficient, and effective tool for matching you with your dream home.</p>
            </div>
        </div>
        <div class="bottom-last">
            <div class="text">
                <span class="title">Get Results</span>
                <p>Don’t waste time sorting through hundreds of listings. After taking our<br><span style="padding-left:1em;">short quiz, you immediately find a list of homes you’ll love, in places</span><br><span style="padding-left:2em;">that fit your lifestyle. Use powerful tools to help you explore</span><br><span style="padding-left:3em;">the areas and homes we have matched to you.</span><br><br><span style="padding-left:5em;">Refine your results and create a free account to save your</span><br><span style="padding-left:6em;">profile, share listings with friends, and even ask to receive</span><br><span style="padding-left:7em;">notifications when a new listing that fits you comes on the</span><br><span style="padding-left:8em;">market so you don’t have to keep checking back in.</span></p>
            </div>
        </div>
	</section>
</div>