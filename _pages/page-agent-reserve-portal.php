<?php
global $ALR; 
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers;
require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations;

$amTags = $Sellers->getAMTagList();
$intro = new \stdClass();
$intro->id= 0;
$intro->tag = "Choose LifeStyle";
array_unshift($amTags, $intro);

$seller_id = get_query_var('id');
$seller = null;
if ( !empty($seller_id) ) {
	$seller = $Sellers->get((object)['where'=>['id'=>$seller_id]]);
	if (!empty($seller)) {
		$seller = $seller[0];
	}
}
	
if (empty($seller)) {
	if (is_user_logged_in()) {
		$seller = $ALR->get('seller');	 
		if (empty($seller)) {
			$user = wp_get_current_user();
			if ($user && !is_wp_error( $user ) && gettype($user) == 'object' && property_exists($user, 'ID') && $user->ID) {
				$seller = new \stdClass();
				$seller->id = 0;
				$user = (object)$user;
				$seller->first_name = $user->user_firstname;
				$seller->last_name = $user->user_lastname;
				$seller->email = $user->user_email;
			}
		}
	}
}
	
if (empty($seller)) {
	$seller = new \stdClass();
	$seller->id = 0;
	$seller->first_name = '';
	$seller->last_name = '';
	$seller->email = '';
}


$service_area = '';
$meta = null;
if (!empty($seller)) {
	if (isset($seller->service_areas) &&
		!empty($seller->service_areas)) {
		$areas = explode(":", $seller->service_areas);
		$areas = explode(',', $areas[0]);
		if (count($areas) == 2) {
			$city = trim($areas[0]);
			$state = trim($areas[1]);
			$service_area = $city.', '.$state;
		}
	}
	elseif (isset($seller->city) &&
			isset($seller->state) &&
			!empty($seller->city) &&
			!empty($seller->state))
		$service_area = $seller->city.", ".$seller->state;

	$reservation = !empty($seller->id) ?  $Reservations->get((object)['where'=>['email'=>$seller->email]]) : null;
	if ($reservation && !empty($reservation[0]->meta)) foreach($reservation[0]->meta as $data) 
		if ($data->action == ORDER_AGENT_MATCH) {
			$meta = $data;
			break;
		}
}
?>

<script type="text/javascript">
var amTagList = <?php echo json_encode($amTags); ?>;
var seller = <?php echo json_encode($seller); ?>;
var service_area = '<?php echo $service_area; ?>';
var meta = <?php echo $meta ? json_encode($meta) : 0 ?>;
</script>

<div id="agent-reservation">
	<div id="left">
		<span id="title1">Agent Portal</br></span>
		<span id="title2">Your personal gateway to the most advanced real estate search engine</span>
		<div class="inman">
			<p style="margin-bottom:-.5em">Featured at</p>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/inman.png" />
		</div>
		<div id="main-graphics">
			<div class="step1">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/portalstep1.png" />
				<div class="steptext">
					<span class="number">1</span>
					<span class="title">Claim your Portal</span>
					<p class="sub">Create a custom portal link, i.e. Lifestyledlistings.com/John</p>
				</div>
			</div>
			<div class="step2">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/portalstep2.png" />
				<div class="steptext">
					<span class="number">2</span>
					<span class="title">Share it with the world</span>
					<p class="sub">Share your link with your social/personal networks.</p>
				</div>
			</div>
			<div class="step3">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-agent-reservation/portalstep3.png" />
				<div class="steptext">
					<span class="number">3</span>
					<span class="title">Get paid referral fees</span>
					<p class="sub">Get notified the second any communication starts and get credit for your referral</p>
				</div>
			</div>
		</div>
	</div>
	<span class="toptext">
		<!--<span style="color: #E78D35;margin-right: 10px;font-size: .8em;display: block;text-align: center;font-style: italic;">Early Adoptor Price</span>
		<div style="color: #E78D35;text-align: center;margin-top: 5px;font-size: .9em;font-style: italic;">
			<span style="font-size:1.2em">$99.99/</span>
			<span style="font-size:.75em">Month</span>
		</div>-->
		<span style="display: block;color: #333;font-size: .5em;text-align: center; margin-top: 60px;">No payment required at this time</span>
	</span>
	<div id="information">
		<div id="message">
			<span id="line1">Get in Now!</span>
			<span id="line4">Reserve your portal name before someone else claims it!</span>
			<span style="font-style:italic;font-size:1.3em;display:block">Lifestyledlistings.com/</span>
			<span style="text-align:center;display:block;margin:.5em 0 1em">
				<!--<label id="base-url" style="letter-spacing:.05em;font-weight:400;font-size:1.1em;color:#333"></label>-->
				<input id="portal" type="text" placeholder="Your Name..." style="margin-left:2px;margin-right:10px;padding:4px;width:140px" />
				<span id="marker" class="entypo-check"></span>
				<button id="check-it">Check it</button>
			</span>
		</div>
		<table id="user-data">
			<tbody>
				<tr>
					<td><label>First</label></td><td><input type="text" id="first_name" placeholder="first name" value="<?php echo (!empty($seller) && isset($seller->first_name) && !empty($seller->first_name) ? $seller->first_name : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Last</label></td><td><input type="text" id="last_name" placeholder="last name" value="<?php echo (!empty($seller) && isset($seller->last_name) && !empty($seller->last_name) ? $seller->last_name : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Phone</label></td><td><input type="text" id="phone" name="phone" placeholder="phone" value="<?php echo (!empty($seller) && isset($seller->phone) && !empty($seller->phone) ? $seller->phone : ''); ?>" /></td>
				</tr>
				<tr>
					<td><label>Email</label></td><td><input type="text" id="email" placeholder="email" value="<?php echo (!empty($seller) && isset($seller->email) && !empty($seller->email) ? $seller->email : ''); ?>" /></td>
				</tr>
			</tbody>
		</table>
		<div class="reserve-now">
			<div class="reserve-now">
				<button id="reserve">Reserve Portal</button>
				<span id="disclaimer">*Please expect a call from LifeStyled Listings to arrange your portal.<span>
			</div>
		</div>
		<span class="reserveportal">Learn more about being a <a id="agent-match" href="javascript:;">LifeStyled&#8482; agent</a> in your area!</span>
	</div>
</div>
