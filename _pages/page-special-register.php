<?php
// maybe do someting here?
	// $args = array('redirect' => get_permalink( get_page( $page_id_of_member_area ) ) );
	// wp_login_form( $args );
	// global $wp_embed;
	// $post_embed = $wp_embed->run_shortcode('[youtube=http://www.youtube.com/watch?v=VOGHM2tYIuA&w=640&h=385]');

	if ( is_user_logged_in() ): ?>
	<script> window.location = ah_local.wp; </script>
	<?php else: ?>
<section class="special-register">
	<section class="notice">
		<div class="divNotice">
			<h3>AGENT SIGN UP</h3>
			<div class="line"></div><br/><br/>
			<label>Create you Agent Match profile</label><br/><br/>
			<label>Find and prepare your listing for launch</label><br/><br/>
			<label>Beta test our site</label>
		</div>
	</section>
	<div id="outerdiv">
		<button class="closeVid">Close</button>
		<!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a> -->
		<iframe src="//player.vimeo.com/video/114051688" width="1080" height="720" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen>
		</iframe>
	</div>
	<a class="video" href="javascript:reg.showVid();" target="_blank" >WATCH VIDEO <span class="entypo-right-dir"></span></a>

	<section class="userData">
		<div class="userDiv">
			<div class="licenseData">
				<label><h2>2</h2></label><input type="text" placeHolder="BRE#" id="bre"><span> </span><label>State:</label>
				<select name="States" id="states">
				  	<option value="-1" selected="selected">Select one</option>
				 	<?php require_once(__DIR__.'/../_classes/States.php'); $i = 0; global $usStates; foreach($usStates as $state) { ?>
				  	<option value="<?php echo $state; $i++ ?>"><?php echo $state; ?></option>
				  	<?php } ?>
				</select>
			</div>
			<div class="line"></div>
			<label><h2>3</h2></label>
			<table id="socialLinkTable">
				<tr><td><a href="https://www.facebook.com/pages/Allure-Homes" target="_blank" class="entypo-facebook-circled"></a></td><td><button class="fbButton">Connect with Facebook</button>
				<tr><td><a href="https://plus.google.com/u/0/113777096276498268695" target="_blank" class="entypo-gplus-circled"></a></td><td><button class="gmButton">Connect with GMail</button>
				<tr><td><a href="https://linkedin.com" target="_blank" class="entypo-linkedin-circled"></a></td><td><button class="gmButton">Connect with LinkedIn</button>
			</table>
			<div class="line2"></div><span id="or">OR</span>
			<table id="userInput">
				<tr><td><input type="text" id="firstname" placeHolder="FIRST NAME"></td></tr>
				<tr><td><input type="text" id="lastname" placeHolder="LAST NAME"></td></tr>
				<tr><td><input type="text" id="email" placeHolder="EMAIL"></td></tr>
				<tr><td><input type="text" id="login" placeHolder="LOGIN AS (default to email)"></td></tr>
				<tr><td><input type="password" id="password" placeHolder="PASSWORD"></td></tr>
			</table>
			<div class="createAccount">
				<button class="create">CREATE ACCOUNT</button><span id="invite">  Invitation code: </span><input type="text" id="invitation_code" placeHolder="enter here">
			</div>
		</div>
		<label id="note">*Note -</label><span><br/>Please use the email address that:<br/>1: you used to register your listing with the local MLS.<br/>or<br/>2: you received our mailing from, so we can find you.<br/>or<br/>Enter the Invitation code so that you can be entered into our database, else your activation email will be sent after the BRE number is verified.</span>
	</section>
	
</section>
<?php endif; ?>