<?php
require_once(__DIR__.'/../_classes/Sellers.class.php');
$q = new stdClass(); 
$s = new AH\Sellers;
require_once(__DIR__.'/../_classes/Options.class.php');
$Options = new AH\Options();

$wpId = wp_get_current_user()->ID;
$q->where = array('author_id'=>$wpId);
$x = $s->get($q);
$seller = null;
if (!empty($x))
	$seller = $x[0];

$x = ($wpId == 0)  ? "0" :
     (current_user_can("create_users")) ? (!empty($x) ?  json_encode($x[0]) :  "2") :
	 (!empty($x)) ?  json_encode($x[0]) :  "1"; 

$nickname = '';
$orderFlags = 0;
if ($seller && !empty($seller->meta)) foreach($seller->meta as $meta) {
	if ($meta->action == SELLER_NICKNAME) 
		$nickname = $meta->nickname;
	elseif ($meta->action == SELLER_NEW_ORDER)
		$orderFlags = $meta->flags;
}

$opt = $Options->get((object)['where'=>['opt'=>'AgentMatchProductPricing']]);
$productPricing = !empty($opt) ? json_decode($opt[0]->value) : null;
 ?>
<script type="text/javascript">
var validSeller = <?php echo $x; ?>;
var nickname = '<?php echo $nickname; ?>';	
var orderFlags = '<?php echo $orderFlags; ?>';
var productPricing = <?php echo json_encode($productPricing); ?>;
</script>


<div id="checkout-container">
	<?php echo do_shortcode('[woocommerce_checkout]'); ?>
</div>