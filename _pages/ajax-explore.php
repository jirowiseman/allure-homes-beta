<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php'); 
require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../_classes/DistanceScale.php');
global $DistanceScale;

define('Golf', 2);
define('Dining', 3);
define('Shopping', 4);
define('Attractions', 5);
define('Nightlife', 6);
define('Outdoors', 7);
define('Arts', 8);
define('Schools', 9);

define('SEARCH_RADIUS', 8);
define('DEFAULT_RADIUS', 64000); // meters
define('DEFAULT_RADIUS_NEARBY_MILES', DEFAULT_RADIUS/1600);
define('CLOSE_ENOUGH_MILES', 2.5);

class AJAX_Explore extends Controller {

	private $places = [Golf=>['type' => ['point_of_interest'],
							  'name' => [['golf course']], // should be lowercase
							  'rating' => 3.0,
							  'icon' => 'golf', // this will override if there is no rating, but strpos(name) and strpos(icon) is not -1
							  'avoidText' => [['miniature','shop']],
							  'mustHaveText' => [['golf course','country club','golf club']] ], 
					   Dining=>['type' => ['restaurant'],
							    'rating' => 3.0,
							    'icon'=>'restaurant',
							    'avoidIcon' => ['bar'],
							    'avoidText' => [['club','bar']],
							    'acceptText' => [['restaurant','cafe','grill','burger','sandwich','breakfast','diner','kitchen','bbq','sushi','pizza','mediterranean','bistro','saloon','seafood','steakhouse','taqueria','noodles','bakery']] ],
					   Shopping=>['type' => ['department_store','shopping_mall','jewelry_store','bicycle_store','pet_store'],
					   			  'name'=> [0, 0, ['jewel','gem'], 0, 0],
					   			  'avoidText' => [0, 0, 0, 0,['sitting','shuttle']] ],
					   Attractions=>['type' => ['amusement_park','aquarium','bowling_alley','casino','library','stadium','zoo'],
					   			  	'name'=> [0, 0, ['bowl']],
					   			  	'avoidText' => [0,['service','shop','supplies','supply']] ],
					   Nightlife=>['type' => ['night_club','bar'],
							  	   'rating' => 3.0,
							    	'icon'=>'bar',
							    	'avoidIcon' => ['restaurant'],
							    	'acceptText' => [['bar','lounge','club','pub','karaoke','saloon'],['bar','lounge','club','pub','karaoke','saloon']] ],
					   Outdoors=>['type' => ['campground','park'],
					   			  'avoidIcon' => ['generic_business']],
					   Arts=>['type' => ['museum','art_gallery'],
					   		  'avoidText' => [0, ['la-z-boy']] ],
					   Schools=>['type' => ['university','school'],
					   			 'avoidText' => [0, ['warehouse']] ]
					   ];

	public function __construct(){
		//$this->qq = new QuizQuery();
		$in = parent::__construct();
		try {
			if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - No query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));
			$this->log = new Log(__DIR__.'/../_classes/_logs/ajax-explore.log') ;
			$this->dumpJson = false;
			$this->updateTimeLimit = 3600 * 24 * 7;
			// $opt = $this->getClass('Options')->get((object)['where'=>['opt'=>"QuizResultsDumpJson"]]);
			// if (!empty($opt)) $this->dumpJson = is_true($opt[0]->value);
			// $this->defaults['imgPath'] = realpath(__DIR__.'/../_img/_listings/845x350');
			switch ($in->query){
				case 'get-found-places':
					if ( !isset($in->data['explore_info'])) throw new \Exception("No explore info id sent");
					$explore_info = $in->data['explore_info'];
					$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$explore_info['id']],
																		'what'=>['flags']]);
					if ( !empty($listing) && 
						 ($listing[0]->flags & LISTING_GATHERING_POI) )
						$out = new Out('OK', ['poi'=> 0]);
					else {
						$points = $this->getPreviousPlaces((object)$explore_info);
						$out = new Out('OK', ['poi'=>$points]);
					}
					break;

				case 'get-website':
					if ( !isset($in->data['id'])) throw new \Exception("No place id sent");
					$out = $this->getClass('GooglePlaces', 1)->geoPlaceDetail($in->data['id'], 0);
					if ($out->status == 'OK') {
						if ( isset($out->data->website) )
							$out->data = $out->data->website;
						else
							$out->status = 'fail';
					}
					break;

				case 'user-places-one-page':
					if ( !isset($in->data['info'])) throw new \Exception("No info sent");
					if ( !isset($in->data['key']) || empty($in->data['key'])) throw new \Exception("No keyword sent");
					$start = microtime(true);
					$location = (object)$in->data['info'];
					$pagetoken = isset($in->data['pagetoken']) && !empty($in->data['pagetoken']) ? $in->data['pagetoken'] : '';
					$packet = (object)[	'lat'=>$location->lat,
										'lng'=>$location->lng,
										'query'=> [ 'type' => 'point_of_interest', 'name'=>$in->data['key'] ],
										'page_token' => $pagetoken,
										'radius'=>DEFAULT_RADIUS];
					$nearby = $this->getClass('GooglePlaces', 1)->geocodeNearby($packet, 0);
					$points = [];
					if ($nearby->status == 'OK'){
						$nearby = $nearby->data;
						foreach ($nearby->results as $point){
							$x = [
								'name' => $point->name,
								'address'=> $point->vicinity.(!empty($query['state']) ? ' '.$query['state'] : ''),
								'lng' => $point->geometry->location->lng,
								'lat' => $point->geometry->location->lat,
								'distance' => $this->getClass('GooglePlaces', 1)->getDistance(floatval($location->lat), floatval($location->lng), $point->geometry->location->lat, $point->geometry->location->lng),
								'meta' => (object)['google'=> (object)['place_id' => $point->place_id,
									 										'icon' => $point->icon,
									 										'photo' => isset($point->photos) && count($point->photos) ? $point->photos[0]->photo_reference : '',
																			'types' => $point->types,
									 									    'rating' => isset($point->rating) ? $point->rating : -1,
									 									    'distance' => $this->getClass('GooglePlaces', 1)->getDistance(floatval($location->lat), floatval($location->lng), $point->geometry->location->lat, $point->geometry->location->lng),
									 									    'types' => $point->types]] ];
							$points[] = $x;
							unset($point);
						}
					}

					$diff = microtime(true) - $start;
					$out = new Out('OK', ['total'=>count($points),
										  'pagetoken' => isset($nearby->next_page_token) && !empty($nearby->next_page_token) ? $nearby->next_page_token : '',
										  'points' => $points,
										  'time' => number_format($diff, 3)] );	
					break;

				case 'user-places':
					if ( !isset($in->data['info'])) throw new \Exception("No info sent");
					if ( !isset($in->data['key']) || empty($in->data['key'])) throw new \Exception("No keyword sent");

					$start = microtime(true);
					$location = (object)$in->data['info'];
					$found = $this->getClass('GooglePlaces', 1)->nearbySearch($location->lat, $location->lng, [ 'type' => 'point_of_interest', 'name'=>$in->data['key'] ], DEFAULT_RADIUS);
					$points = [];
					$this->log("user-places - returned $found->status, count:".($found->status == 'OK' ? count($found->data) : 'none'));
					if ($found->status == 'OK') foreach($found->data as $point) {
						$newPoint = ['name' => $point->name,
									 'address' => $point->address,
									 'lng' => $point->lng,
									 'lat' => $point->lat,
									 'distance' => $point->distance,
									 'meta' => (object)['google'=> (object)['place_id' => $point->place_id,
									 										'icon' => $point->icon,
									 										'photo' => $point->photo,
									 									    'rating' => $point->rating,
									 									    'distance' => $point->distance,
									 									    'types' => $point->types]]];
						$points[] = $newPoint;
						unset($point);
					}
					$diff = microtime(true) - $start;
					$out = new Out('OK', ['total'=>count($points),
										  'points' => $points,
										  'time' => number_format($diff, 3)] );																			
					break;

				case 'get-places':
					if ( !isset($in->data['listing'])) throw new \Exception("No listing info sent");
					$start = microtime(true);
					$listing = (object)$in->data['listing'];
					$lng = floatval($listing->lng);
					$lat = floatval($listing->lat);
					$city_id = isset($listing->city_id) && $listing->city_id ? intval($listing->city_id) : -1;
					$this->log("get-places received lng:$lng, lat:$lat, cityId:$city_id, listing:$listing->id ");

					if ($city_id == -1) {
						if ( !nonConformingLocation($listing->city) ) {
							$cityName = fixCityStr($listing->city);
							$cityName = fix2CharCity($cityName, $listing->state);
							$city = $this->getClass('Cities')->get((object)['where'=>['city' => $cityName,
																					  'state' => $listing->state]]);
							if (!empty($city)) {
								$city_id = $city[0]->id;
								$this->log("get-places got city_id:$city_id from $cityName $listing->state");
							}
							else {
								$cityGeo = $this->geocodeCity($cityName, $listing->state, (intval($listing->zip) ? $listing->zip : ''), $listing->country);
								if ($cityGeo !== null) {
									$info = ['city'=>$cityName,
											 'state'=>$listing->state,
											 'lng'=> $cityGeo->lng,
											 'lat'=> $cityGeo->lat];
									$city_id = $this->getClass('Cities')->add((object)$info);
									if ($lng == -1) {
										$lng = $cityGeo->lng;
										$lat = $cityGeo->lat;
									}
									$this->log("get-places add new city_id:$city_id from $cityName $listing->state with  lng:$cityGeo->lng, lat:$cityGeo->lat");
								}
							}
						}
						elseif ($lng != -1 && $lat != -1) {
							// make a new city using lng/lat
							$cityName = number_format($lng, 1).','.number_format($lat,1);
							$city = $this->getClass('Cities')->get((object)['where'=>['city' => $cityName,
																					  'state' => $listing->state]]);
							if (!empty($city)) {
								$city_id = $city[0]->id;
								$this->log("get-places got city_id:$city_id from $cityName $listing->state");
							}
							else {
								$info = ['city'=>$cityName,
										 'state'=>$listing->state,
										 'lng'=> -1,
										 'lat'=> -1];
								$city_id = $this->getClass('Cities')->add((object)$info);
								$this->log("get-places add new city_id:$city_id from $cityName $listing->state with  lng:-1, lat:-1");
							}
						}
						else {
							$this->log("get-places - no coords nor city for listing:$listing->id");
							$out = new Out('OK', ['added' => 0,
												  'total' => 0,
											      'time' => 'N/A'] );
							break;
						}
					}
					elseif ($lng == -1 && $lat == -1) {
						$city = $this->getClass('Cities')->get((object)['where'=>['id' => $city_id]]);
						if (!empty($city)) {
							$city = array_pop($city);
							if ( $city->lng == -1 ) {
								$cityName = fixCityStr($listing->city);
								$cityName = fix2CharCity($cityName, $listing->state);
								$cityGeo = $this->geocodeCity($cityName, $listing->state, (intval($listing->zip) ? $listing->zip : ''), $listing->country);
								if ($cityGeo) {
									$lng = $cityGeo->lng;
									$lat = $cityGeo->lat;
									$this->getClass('Cities')->set([(object)['where'=>['id' => $city_id],
																			 'fields'=>['lng'=>$lng,
																			 		    'lat'=>$lat]]]);
									$this->log("get-places - geocoded city:$cityName $listing->state to lng:$lng, lat:$lat");
								}
								else {
									$this->log("get-places - no coords nor city for listing:$listing->id");
									$out = new Out('OK', ['added' => 0,
														  'total' => 0,
													      'time' => 'N/A'] );
									break;
								}
							}
							else {
								$lng = $city->lng;
								$lat = $city->lat;
								$this->log("get-places - got coords from city:$cityName $listing->state, lng:$lng, lat:$lat");
							}
						}
					}

					if ( $city_id != -1) {
						$cityPoint = $this->getClass('CitiesPoints')->get((object)['where'=>['city_id'=>$city_id]]);
						if ( !empty($cityPoint) ) {
							$cityPoint = array_pop($cityPoint);
							$this->log("get-places found CitiesPoints for $listing->city, $listing->state - ".print_r($cityPoint, true));
						}

						// $city = $this->getClass('Cities')->get((object)['where'=>['id'=>$listing->city_id]]);
						// if (!empty($city))
						// 	$city = array_pop($city);
					}

					if (empty($lng) ||
						$lng == -1) {
						$this->log("get-places - no coords for listing:$listing->id");
						$out = new Out('OK', ['added' => 0,
											  'total' => 0,
										      'time' => 'N/A'] );
						break;
					}


					//$lng = !empty($city) && $city->lng != -1 ? $city->lng : $listing->lng;
					//$lat = !empty($city) && $city->lat != -1 ? $city->lat : $listing->lat;

					$this->log("get-places using lng:$lng, lat:$lat, cityId:$city_id, listing:$listing->id");
					$newCoords = true;

					$listing->flags = intval($listing->flags);
					if ( !($listing->flags & LISTING_GATHERING_POI) )
						$this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																   'fields'=>['flags'=>($listing->flags | LISTING_GATHERING_POI)]]]);

					$this->exitNow("user-places start gathering POIs");

					if (!empty($cityPoint) &&
						!empty($cityPoint->meta)) {
						foreach($cityPoint->meta as $meta) {
							if ($meta->action == CP_COORDS) {
								foreach($meta->list as $coords) {
									global $DistanceScale;
									$coords = (object)$coords;
									$index = (int)(abs( is_float($coords->lat) ? $coords->lat : (float)$coords->lat ) / 10);
									$diff1 = abs(floatval($coords->lng) - $lng);
									$diff2 = abs(floatval($coords->lat) - $lat);
									$dist1 = $diff1 * $DistanceScale[$index][0];
									$dist2 = $diff2 *  $DistanceScale[$index][1];
									$this->log("get-places - diff1:$diff1, diff2:$diff2, dist1:$dist1, dist2:$dist2");
									// if ( $diff1 <= 0.04 &&
									// 	 $diff2 <= 0.04) {
									if ( $dist1 <= CLOSE_ENOUGH_MILES &&
										 $dist2 <= CLOSE_ENOUGH_MILES) {
										$newCoords = false;
										break;
									}
								}
								break;
							}
						}

					}

					$existing = $this->getExistingPlaces((object)['lng'=>$lng, 'lat'=>$lat]);
					$now = time();

					if ( !empty($cityPoint) &&
						 !$newCoords ) {
						$diff = ($now - strtotime($cityPoint->updated));						
						if ( $diff < $this->updateTimeLimit ) {// then less than some time since an update
							if ($cityPoint->poi_count != count($existing))
								$this->getClass('CitiesPoints')->set([(object)['where'=>['id'=>$cityPoint->id],
																			   'fields'=>['poi_count'=>count($existing)]]]);
							$this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
															   		   'fields'=>['flags'=>($listing->flags & ~LISTING_GATHERING_POI)]]]);

							$out = new Out('OK', ['added' => 0,
												  'total' => count($existing),
											      'time' => 'N/A'] );
							break;
						}
						$this->log("get-places redoing, since last update for $listing->city, $listing->state was $cityPoint->updated");
					}

				
					$accumulated = [];
					$closed = [];
					foreach($this->places as $cat=>$set) {
						$found = null;
						foreach($set['type'] as $i=>$type) {
							$defunct = null;
							if ( isset($set['name']) &&
							     isset($set['name'][$i]) &&
							     !empty($set['name'][$i]) ) {
								foreach($set['name'][$i] as $name) {
									$query = ['type' => $type,
											  'name' => $name,
											  'cat' => $cat,
											  'state' => isset($listing->state) ? $listing->state : '',
											  'rating' => isset($set['rating']) ? $set['rating'] : 0,
											  'icon' => isset($set['icon']) ? $set['icon'] : 0,
											  'avoidIcon' => isset($set['avoidIcon']) ? $set['avoidIcon'] : 0,
											  'avoidText' => isset($set['avoidText']) && isset($set['avoidText'][$i]) && !empty($set['avoidText'][$i]) ? $set['avoidText'][$i] : 0,
											  'mustHaveText' => isset($set['mustHaveText']) && isset($set['mustHaveText'][$i]) && !empty($set['mustHaveText'][$i]) ? $set['mustHaveText'][$i] : 0,
											  'acceptText' => isset($set['acceptText']) && isset($set['acceptText'][$i]) && !empty($set['acceptText'][$i]) ? $set['acceptText'][$i] : 0 ];
									$found = $this->getClass('GooglePlaces', 1)->nearbySearch($lat, $lng, $query, DEFAULT_RADIUS, $defunct);
									$this->mergeAccumulated($accumulated, $found);
									$this->mergeClosed($closed, $defunct);
									$this->log("get-places for $type, $name, accumulated:".count($accumulated).", found:".(isset($found->data) && $found->data ? count($found->data) : 0).", defunct:".count($defunct));
									unset($found, $defunct);
								}
							}
							else {
								$found = $this->getClass('GooglePlaces', 1)->nearbySearch($lat, $lng, [	'type'=>$type, 
																										'cat'=>$cat,
							  																			'state' => isset($listing->state) ? $listing->state : '',
							  																			'rating' => isset($set['rating']) ? $set['rating'] : 0,
											  															'icon' => isset($set['icon']) ? $set['icon'] : 0,
											  															'avoidIcon' => isset($set['avoidIcon']) ? $set['avoidIcon'] : 0,
											  															'avoidText' => isset($set['avoidText']) && isset($set['avoidText'][$i]) && !empty($set['avoidText'][$i]) ? $set['avoidText'][$i] : 0,
											  															'mustHaveText' => isset($set['mustHaveText']) && isset($set['mustHaveText'][$i]) && !empty($set['mustHaveText'][$i]) ? $set['mustHaveText'][$i] : 0,
											  															'acceptText' => isset($set['acceptText']) && isset($set['acceptText'][$i]) && !empty($set['acceptText'][$i]) ? $set['acceptText'][$i] : 0 ],
							  																			DEFAULT_RADIUS,
							  																			$defunct);
								$this->mergeAccumulated($accumulated, $found);
								$this->mergeClosed($closed, $defunct);
								$this->log("get-places for $type, accumulated:".count($accumulated).", found:".(isset($found->data) && $found->data ? count($found->data) : 0).", defunct:".count($defunct));
								unset($found, $defunct);
							}
						} // end foreach($set)
					}

					if ( !empty($existing) ) {
						$keys = array_keys($existing);
						foreach($keys as $key)
							if ( array_key_exists($key, $accumulated))
								unset($accumulated[$key]);

						// now go through $existing and find ones with null meta data
						foreach($existing as $poi) {
							if ( empty($poi->meta) ) {
								foreach($accumulated as $key=>$newPoi) {
									if ($poi->name == $newPoi->name) {
										$meta = (object)['google'=> (object)['place_id' => $newPoi->place_id,
										 									 'icon' => $newPoi->icon,
										 									 'photo' => $newPoi->photo,
										 									 'rating' => $newPoi->rating,
										 									 'distance' => $newPoi->distance,
										 									 'types' => $newPoi->types]];
										$x = $this->getClass('Points')->set([(object)['where'=>['id'=>$poi->id],
																				 	  'fields'=>['meta'=>$meta]]]);
										$this->log("get-places ".(!empty($x) ? 'updated' : 'failed to update')." existing:$poi->name, id:$poi->id with meta data");
										unset($accumulated[$key]); // don't add another one..
										unset($meta);
										break;
									}
									unset($newPoi);
								}
							}
							unset($poi);
						}
					}

					$savedToDb = 0;
					if (!empty($accumulated)) {
						foreach($accumulated as $point) {
							$newPoint = ['name' => $point->name,
										 'address' => $point->address,
										 'lng' => $point->lng,
										 'lat' => $point->lat,
										 'meta' => (object)['google'=> (object)['place_id' => $point->place_id,
										 										'icon' => $point->icon,
										 										'photo' => $point->photo,
										 									    'rating' => $point->rating,
										 									    'distance' => $point->distance,
										 									    'types' => $point->types]]];
							$row = $this->Points->add((object)$newPoint);
							if (!empty($row)) {
								$newTaxonomy = ['category_id' => $point->category,
												'point_id' => $row];
								$taxAdded = $this->getClass('PointsTaxonomy')->add((object)$newTaxonomy);
								$this->log("get-places added point:$row to taxomonmy:$taxAdded for point:".print_r($point, true));
								$savedToDb++;
							}
							else
								$this->log("get-places failed to add Point:".print_r($point, true));
						}
					}

					$removed = 0;
					$this->log('get-places - closed:'.count($closed));
					if (!empty($closed)) {
						foreach($closed as $point) {
							$this->log("get-places removing $point->name at $point->lng, $point->lat");
							$x = $this->getClass('Points')->delete((object)['name'=>$point->name,
																			'lng'=>$point->lng,
																			'lat'=>$point->lat]);
							if (!empty($x)) $removed++;
							else $this->log("get-places failed to delete $point->name");
							unset($point);
						}
					}

					$total = $savedToDb + count($existing);

					if ( !empty($cityPoint) ) {
						$fields = [];
						if ($cityPoint->poi_count != $total)
							$fields['poi_count'] = $total;
						if ($cityPoint->radius < DEFAULT_RADIUS)
							$fields['radius'] = DEFAULT_RADIUS; 
						$metas = [];
						$coords = null;
						if (!empty($cityPoint->meta)) {
							foreach($cityPoint->meta as $meta) {
								if ($meta->action == CP_COORDS) {
									$coords = $meta;
								}
								else
									$metas[] = $meta;
							}
							if (!$coords) {
								$metas[] = (object)['action'=>CP_COORDS,
													'list'=>[(object)['lng'=>$lng,
																	  'lat'=>$lat]]];
							}
							else {
								$coords->list[] = (object)['lng'=>$lng,
														   'lat'=>$lat];
								$metas[] = $coords;
							}
							$fields['meta'] = $metas;
						}
						else {
							$fields['meta'] = [(object)['action'=>CP_COORDS,
														'list'=>[(object)['lng'=>$lng,
																		  'lat'=>$lat]]]];
						}
						if ( count($fields) ) {
							$this->getClass('CitiesPoints')->set([(object)['where'=>['city_id'=>$city_id],
																			'fields'=>$fields]]);
							$this->log("get-places updated exiting CityPoint for cityId:$listing->city_id with ".print_r($fields, true));
						}
					}
					else {
						$row = $this->getClass('CitiesPoints')->add((object)['city_id'=>$city_id,
																			'poi_count'=>$total,
																			'radius'=>DEFAULT_RADIUS,
																			'meta' => [(object)['action'=>CP_COORDS,
																								'list'=>[(object)['lng'=>$lng,
																												  'lat'=>$lat]]]] 
																		   ]);
						$this->log("get-places ".(!empty($row) ? 'added' : 'failed to add')." CityPoint - cityId:$city_id");
					}

					$this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
															   'fields'=>['flags'=>($listing->flags & ~LISTING_GATHERING_POI)]]]);

					$diff = microtime(true) - $start;
					$out = new Out($savedToDb ? 'OK' : 'fail', ['added' => $savedToDb,
																'total' => $total,
																'removed' => $removed,
																'time' => number_format($diff, 3)] );
					break;
				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) {
				if ($this->dumpJson) {
					$json = json_encode($out);
					$this->log("json result: $json");
					echo $json;
				}
				else
					echo json_encode($out);
			}
			
			// $this->qq->add((object)array('type' => 'Quiz',
			// 				  		'data' => 'echo out: '.json_encode($out)));
		} catch (\Exception $e) { return parseException($e, true); }
	}

	private function sortByPlaceId(&$array) {
		usort($array, function($a, $b) {
			return strcmp($a->place_id, $b->place_id);
		});
	}

	private function mergeClosed(&$closed, $defunct) {
		if ( empty($closed) ) {
			$closed = $defunct;
			return;
		}

		$foundKeys = array_keys($defunct);
		foreach ($foundKeys as $key) {
			if ( !array_key_exists($key, $closed) )
				$closed[$key] = $defunct[$key];
		}
	}
	private function mergeAccumulated(&$accumulated, $found) {
		if ( $found->status != 'OK')
			return;

		if ( empty($accumulated) ) {
			$accumulated = $found->data;
			// $this->sortByPlaceId($accumulated);
			return;
		}
		// $this->sortByPlaceId($found); // it is now sorted
		if ( empty($found->data) )
			return;

		$foundKeys = array_keys($found->data);
		foreach ($foundKeys as $key) {
			if ( !array_key_exists($key, $accumulated) )
				$accumulated[$key] = $found->data[$key];
		}
	}

	private function geocodeCity($cityStr, $state, $zip, $country) {
		$city = (object)['city'=>$cityStr,
						 'state' => $state,
						 'zip' => $zip,
						 'country' => nonConformingLocation($country) ? '' : $country,
						 'lng' => null,
						 'lat' => null];
		// if (!empty($listing['zip']))
		// 	$city->zip = $listing['zip'];
		$lng = $lat = -1;
		$result_city = $google_result = null;
		try {
			if (!$this->getClass('Cities')->geocodeCity($this->getClass('GoogleLocation'), $city, $lng, $lat, $result_city, $google_result)) {
				$this->log("Failed to geocode city:$city->city, $state $country");
				return null;
			}
		}
		catch(\Exception $e) {
			$this->log("Exception from geocodeCity:".$e->getMessage());
			return null;
		}
		$city->lng = $lng;
		$city->lat = $lat;

		return $city;
	}

	private function getPreviousPlaces($location) {
		global $DistanceScale;
		// get the categories
		$PointsCategories = $this->getClass('PointsCategories');
		$PointsCategories = $PointsCategories->get((object)[ 'where' => [ 'parent' => 0 ] ]); // top tier

		// parse categories
		$places = [];
		foreach ($PointsCategories as $row){
		    $places[intval($row->id)] = (object)[
		        'category' => $row->category,
		        'points' => [],
		    ];
		}

		// look for POIs within range of listing->lat & listing->lng
		$Points = $this->getClass('Points');
		$lat = (int)(abs( is_float($location->lat) ? $location->lat : (float)$location->lat ) / 10);
		$deltaLng = SEARCH_RADIUS / $DistanceScale[$lat][0];
		$deltaLat = SEARCH_RADIUS / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 69 miles/degree
		$range = [ $deltaLat, $deltaLng ]; // lat, lng radius (in degrees)
		$points_list = $Points->rawQuery("SELECT a.id, a.name, a.address, a.lat, a.lng, a.meta, b.category_id FROM {$Points->getTableName($Points->table)} as a LEFT JOIN {$Points->getTableName('points-taxonomy')} as b ON a.id = b.point_id WHERE lat > {$Points->prepare('%f', $location->lat - $range[0])} AND lat < {$Points->prepare('%f', $location->lat + $range[0])} AND lng > {$Points->prepare('%f', $location->lng - $range[1])} AND lng < {$Points->prepare('%f', $location->lng + $range[1])}");

		$google = $this->getClass('GoogleLocation');
		if (!empty( $points_list ))
		    foreach ($points_list as $row){
		        $row->category_id = intval($row->category_id);
		        $row->id = intval($row->id);
		        $meta = isset($row->meta) && !empty($row->meta) ? json_decode($row->meta) : null;
		        $place_id = $meta && isset($meta->google) && isset($meta->google->place_id) ? $meta->google->place_id : $row->id;
		        $icon = $meta && isset($meta->google) && isset($meta->google->icon) ? $meta->google->icon : '';
		        $photo = $meta && isset($meta->google) && isset($meta->google->photo) ? $meta->google->photo : '';
		        $places[$row->category_id]->points[$place_id] = (object)[
		            'id' => $row->id,
		            'lat' => floatval($row->lat),
		            'lng' => floatval($row->lng),
		            'name' => $row->name,
		            'address' => $row->address,
		            'place_id' => $place_id,
		            'icon' => $icon,
		            'photo' => $photo,
		            'meta' => $meta,
		            'distance' => $google->getDistance($row->lat, $row->lng, $location->lat, $location->lng)
		        ];
		    }

		$x = [];
		foreach ($places as $row) {
		    $x[$row->category] = $row->points;
		    unset($row);
		}
		$places = $x;
		return $places;
	}

	private function getExistingPlaces($location) {
		global $DistanceScale;
		// get the categories
		$PointsCategories = $this->getClass('PointsCategories');
		$PointsCategories = $PointsCategories->get((object)[ 'where' => [ 'parent' => 0 ] ]); // top tier

		// parse categories
		$points = [];
		foreach ($PointsCategories as $row){
		    $points[intval($row->id)] = (object)[
		        'category' => $row->category,
		        'points' => [],
		    ];
		}

		// look for POIs within range of listing->lat & listing->lng
		$Points = $this->getClass('Points');
		$lat = (int)(abs( is_float($location->lat) ? $location->lat : (float)$location->lat ) / 10);
		$deltaLng = DEFAULT_RADIUS_NEARBY_MILES / $DistanceScale[$lat][0];
		$deltaLat = DEFAULT_RADIUS_NEARBY_MILES / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 69 miles/degree
		$range = [ $deltaLat, $deltaLng ]; // lat, lng radius (in degrees)
		$points_list = $Points->rawQuery("SELECT a.id, a.name, a.address, a.lat, a.lng, a.meta, b.category_id FROM {$Points->getTableName($Points->table)} as a LEFT JOIN {$Points->getTableName('points-taxonomy')} as b ON a.id = b.point_id WHERE lat > {$Points->prepare('%f', $location->lat - $range[0])} AND lat < {$Points->prepare('%f', $location->lat + $range[0])} AND lng > {$Points->prepare('%f', $location->lng - $range[1])} AND lng < {$Points->prepare('%f', $location->lng + $range[1])}");

		// Parse results and calculate distance
		$google = $this->getClass('GoogleLocation');
		if (!empty( $points_list ))
		    foreach ($points_list as $row){
		        $row->category_id = intval($row->category_id);
		        $row->id = intval($row->id);
		        $meta = isset($row->meta) && !empty($row->meta) ? json_decode($row->meta) : null;
		        $place_id = $meta && isset($meta->google) && isset($meta->google->place_id) ? $meta->google->place_id : $row->id;
		        $icon = $meta && isset($meta->google) && isset($meta->google->icon) ? $meta->google->icon : '';
		        $photo = $meta && isset($meta->google) && isset($meta->google->photo) ? $meta->google->photo : '';
		        $points[$row->category_id]->points[$place_id] = (object)[
		            'id' => $row->id,
		            'lat' => floatval($row->lat),
		            'lng' => floatval($row->lng),
		            'name' => $row->name,
		            'address' => $row->address,
		            'place_id' => $place_id,
		            'icon' => $icon,
		            'photo' => $photo,
		            'meta' => $meta,
		            'distance' => $google->getDistance($row->lat, $row->lng, $location->lat, $location->lng)
		        ];
		    }

		$x = [];
		foreach ($points as $cat=>$row) {
			if (!empty($row->points)) foreach($row->points as $point) {
				$point->category = $cat;
		    	$x[$point->place_id] = $point;
		    	unset($point);
		    }
		    unset($row);
		}
		$points = $x;

		return $points;
	}
}
new AJAX_Explore();
