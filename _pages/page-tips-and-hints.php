<div id="page-tips-and-hints">
    <div class="top">
        <a href="<?php bloginfo('wpurl'); ?>/sellers" class="back-to-admin">Back to Admin</a>
        <a href="<?php bloginfo('wpurl'); ?>/sellers" class="premium-services">Premium Services</a>
        <div class="title">Tips and Hints</div>
        <p>Bring excellence to your listing</p>
    </div>
    <div class="below-top">
        <p>As with anything in the world, attention to detail is everything and creating a listing is certainly no exception. However, there are three key ingredients that will help you get the most out of our portal. We designed this site to highlight certain qualities in a listing, this is where we tell you exactly how to make them work for you. If you follow these tips, you will have a very solid foundation that you can build the wonderful details that make your listing special.</p>
    </div>
    <div class="section1">
        <div class="title-bar">
            <div class="title">1. Tag Effectively</div>
            <p><span class="quote-top">“Tagging correctly and thoroughly is without a doubt one</span>
            of the most important things you can do for your listing”</p>
        </div>
        <div class="body">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/section1.jpg" />
            <p>To make the tagging system as clear as possible we have designed a checklist that you can print and take to your listing, or fill out by memory. It is crucial that you do this accurately so you don’t find yourself with angry buyers that feel mislead, and thoroughly so that you don’t miss any opportunities to reach matching buyers.</p>
        </div>
    </div>
    <div class="section2">
        <div class="title-bar">
            <div class="title">2. Improve Your Look</div>
            <p><span class="quote-top">“Nothing brings a home to life more than</span>
            beautiful, vivid and professional photographs”</p>
        </div>
        <div class="body">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/section2.jpg" />
            <p>You would be better off excluding photographs than including sub-par images. In response to Mobile demand, many sites sought to “optimize” by dumbing down image quality to help load times on slow data networks and small screens. Thankfully, with widespread availability of wifi, faster data connections like 3G and 4G, and ease of use of mobile apps, those limitations are fast fading if not entirely gone. Allure homes has the latest compression programs to drastically cut data size without sacrificing quality. This lets you upload sharp and vivid images that are a breath of fresh air and really make a difference in hooking a buyer.</p>
        </div>
    </div>
    <div class="section3">
        <div class="title-bar">
            <div class="title">3. Improve Your Area</div>
            <p><span class="quote-top">“Living is all about lifestyle, and nothing affords</span>
            lifestyle more than the location of your listing”</p>
        </div>
        <div class="body">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/section3.jpg" />
            <p>Does your home give buyers an extravagant getaway to the lush wine country and the world-class restaurants of Napa Valley? Or, does your condo on Rodeo Drive make weekends full of high-end shopping and exclusive clubs a must? One of the biggest draws to Allure Homes is to be matched with areas and homes at the same time. Buyers get excited about the homes they see, and even more excited about what they’ll find by exploring the area. Don’t miss your opportunity to make them fall in love with your listings location.<br/>Check to see what points of interest show up around your listing and use your local expertise to suggest anything that is missing. Maybe a new nightclub just opened or a hotel scored a celebrity chef. We research suggestions and gladly add them to the map if they qualify, we’ll even give you rewards for improving the richness of our site. These are the things that bring buyers closer to a sale. They are buying a home, but paying for a lifestyle. Don’t forget it!</p>
        </div>
    </div>
    <div class="section4">
        <div class="title-bar">
            <div class="title">4. Size up the Competition</div>
            <p><span class="quote-top">“Looking to see what the best are doing</span>
            is a great way to gain insights”</p>
        </div>
        <div class="body">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/section4.jpg" />
            <p>If you’re the kind of agent that constantly looks to improve, then you know that looking to see what the best are doing is a great way to gain insights. We track how every listing is performing in terms of number of views, time spent on the listing, likes, and shares. We use these metrics to bring you our “Top 10” lists. These lists aren’t just to pat the top performers on the back, they are also to give other agents a heads up on who is doing things right according to buyers. Examine these top performers and see how their listing pages are different from yours. Of course, the uniqueness of a property is a factor but we’re sure you can find some value in how the descriptions are worded, the quality and types of images displayed, and what kind of points of interest are included.<br/>“Top 10” lists are great for general ideas but unless those homes are in a similar price range, area, and style, then you won’t be competing directly with them. Look at listings that are similar to yours to see what buyers will be comparing your home to. Make sure you highlight the things that make your listing stand out compared to others in your area, and areas that are similar.</p>
        </div>
    </div>
    <div class="section5">
        <div class="title-bar">
            <div class="title">5. Premium Services</div>
            <div class="quote"><span class="quote-top">“Get help and get it done right. The Allure</span>
            team are masters of perfection”</div>
        </div>
        <div class="set-yourself-apart">
            <div class="title">Set Yourself Apart with Allure</div>
            <p>Stunning photography, professional video, and extenisve post production processing combined with Allure’s extensive distribution network is the perfect way to stand out and impress the right buyers.</p>
            <a href="#" class="start-now">START NOW</a>
        </div>
        <div class="premium-video">
            <div class="title">Premium Video Content</div>
            <p>No amount of words or images can show the quality of a property quite like rich video content. Get ahead of the competition with professionally produced video, complete with voice-over narration and music.</p>
        </div>
        <div class="sell-listing">
            <div class="title">The Quickest Way to Get Your Listing Sold</div>
            <p>With our social media platforms, blogs and other syndication tools, we put your listing in front of thousands of potential buyers overnight.</p>
            <div class="sell-split">
                <div class="split1">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/split-1.jpg"/>
                    <span class="title">Allure Homes E-Magazine</span>
                    <p>Be featured in our monthly magazine</p>
                </div>
                <div class="split2">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/split-2.jpg"/>
                    <span class="title">Constant Content</span>
                    <p>See your listing in our daily blogs and other organic content</p>
                </div>
                <div class="split3">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-tips-and-hints/split-3.jpg"/>
                    <span class="title">Complete Social Media</span>
                    <p>Our massive media channels will spread your featured content further</p>
                </div>
            </div>
        </div>
        <div class="invest">
            <div class="title">Invest in Your Listing</div>
            <p>Investing in a <span style="color:#ce8800;">GOLD</span> MEDIA PACKAGE includes the following premium services:</p>
            <div class="bullets">
                <p>Comprehensive photography of the home and property by a professional</p>
                <p>Post production photography enhancement</p>
                <p>Professional filming of the listing</p>
                <p>Post production editing and voice over of video content</p>
                <p>Featured placement on the Allure Homes website and web based marketing through multiple media channels</p>
            </div>
            <a href="#" class="start-now">START NOW</a>
        </div>
    </div>
</div>