jQuery(document).ready(function($){
	// var url = "url("+ah_local.tp+"/_img/backgrounds/splash-page-sign-up.jpg"+") no-repeat 0 0";
	// $('.special-register').css('background', url);
	$('.special-register').css('background-size', '100% 100%');	
	$('.special-register #outerdiv .closeVid').hide();
	$('.special-register #outerdiv').hide();
	$('.special-register #outerdiv .closeVid').on('click', function() {
		reg.closeFrame();
	});
	//var h = '<div class="box><a id="closeVid" href="javascript:reg.closeFrame();">[X]</a></div>';
	//$('.special-register #outerdiv iframe .sidedock').prepend(h);

	window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '995896017092647',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));

	reg = new function() {
		this.state = -1;

		this.showVid = function() {
			$('.special-register #outerdiv').show();
			$('.special-register #outerdiv .closeVid').show();
		}
		this.closeFrame = function() {
			$('.special-register #outerdiv').css("display", "none");
		}

		$('.userDiv .licenseData #states').change(function() {
			var val = $(".userDiv .licenseData #states option:selected").val();
			if (val == -1)
				ahtb.alert("Please pick the state where your license is registered.",
					{height: 150});
			else {
				reg.state = val;
				console.log("State is now set to "+val);
			}
		});
	}

	$(".create").on('click', function() {
		var bre = $('#bre').val();
		var first = $('#firstname').val();
		var last = $('#lastname').val();
		var pass = $('#password').val();
		var email = $('#email').val();
		var login = $('#login').val();
		var invitationCode = $('#invitation_code').val();

		var allGood = true;
		if (bre.length == 0) {
			ahtb.alert("Please enter your BRE number.", {height: 150}); allGood = false; }
		else if (first.length == 0) {
			ahtb.alert("Please enter your first name.", {height: 150}); allGood = false; }
		else if (last.length == 0) {
			ahtb.alert("Please enter your last name.", {height: 150}); allGood = false; }
		else if (email.length == 0) {
			ahtb.alert("Please enter your email address.", {height: 150}); allGood = false; }
		else if (pass.length == 0 ||
				 pass.length < 8) {
			ahtb.alert("Please enter a password at least 8 characters long.  Mix numbers and special characters for best security.", {height: 180}); allGood = false; }
		else if (reg.state == -1) {
			ahtb.alert("Please choose the state that issued your license.", {height: 150}); allGood = false; }

		if (!allGood)
			return;

		if (login.length == 0 &&
			!confirm("Are you sure you want to user your email as your login name?"))
			return;

		var data = {
			email: email,
			first_name: first,
			last_name: last,
			password: pass,
			realtor_id: bre,
			login: login.length ? login : email,
			state: reg.state,
			inviteCode: invitationCode
		}

		$.ajax({
			url: ah_local.tp+'/_pages/ajax-register.php',
			data: { query: 'register-agent',
					data: data },
			dataType: 'JSON',
			type: 'POST',
			error: function($xhr, $status, $error){
				var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
				ahtb.alert('You Have Encountered an Error during agent registration.'+
							'<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' ,
						   {title: 'ALERT!',
						    height: 220,
							width: 500});
			},					
		  	success: function(data){
		  	if (data == null || data.status == null ||
		  		typeof data == 'undefined' || typeof data.status == 'undefined') 
		  		ahtb.open({ title: 'You Have Encountered an Error during agent registration', height: 180, width: 400, html: '<p>Got a return of NULL</p>' });
		  	else if (data.status != 'OK')
		  		ahtb.open({ title: 'You Have Encountered an Error', height: 180, width: 400, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
		  	else {
			  	console.log("Agent Registration: "+data.data);
			  	ahtb.alert(data.data,
			  			   {height: 300,
			  			    width: 600});
		  	}
		  }
		});
	})
});
