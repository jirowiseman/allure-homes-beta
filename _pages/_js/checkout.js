window.onload = function() {
	return;
	// don't call the below code... it's just not mature yet
	window.setTimeout(function() {
		var oneTime = 0;
		var monthly = 0;
		var quarterly = 0;
		for(var i in validSeller.meta) {
			if (validSeller.meta[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) {
				for(var j in validSeller.meta[i].item) {
					if (validSeller.meta[i].item[j].mode == AgentOrderMode.ORDER_BUYING) {
						if (validSeller.meta[i].item[j].type == AgentOrderMode.ORDER_PORTAL)
							monthly += 99;
						if (validSeller.meta[i].item[j].type == AgentOrderMode.ORDER_SIGN_UP)
							oneTime += 100;
						if (validSeller.meta[i].item[j].type == AgentOrderMode.ORDER_AGENT_MATCH) {
							if (validSeller.meta[i].item[j].subscriptionType == ProductType.MONTHLY_SUBSCRIPTION)
								monthly += getPrice(ProductType.MONTHLY_SUBSCRIPTION, validSeller.meta[i].item[j].priceLevel);
							else
								quarterly += getPrice(ProductType.QUARTERLY_SUBSCRIPTION, validSeller.meta[i].item[j].priceLevel);
						}
					}
				}
			}
		}
		if ( (oneTime && monthly) ||
			 (oneTime && quarterly) ||
			 (monthly && quarterly) ) {
			var h = '<div class="amount" id="subtotal">';
			var needBreak = false;
			if (oneTime) {
				h+= '<span>$'+oneTime.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' one time</span>';
				needBreak = true;
			}
			if (monthly) {
				h+= (needBreak ? '<br/>' : '')+'<span>$'+monthly.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' / month</span>';
				needBreak = true;
			}
			if (quarterly) {
				h+= (needBreak ? '<br/>' : '')+'<span>$'+quarterly.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' every 3 months</span>';
				needBreak = true;
			}
			h += '</div>';
			var ele = $('#order_review .cart-subtotal td');
			ele.html(h);

			h = '<div class="amount" id=orderTotal">';
			h += '<span>$'+(oneTime+monthly+quarterly).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' today</span>';
			h += '</div>';
			$('#order_review .order-total td').html(h);
		}
	}, 5000);
}

jQuery(document).ready(function($){
	checkout = new function() {
		this.dbBusy = false;
		this.init = function() {
			console.log("checkout init");
			if (validSeller == 0) {
				ahtb.open({
				    title: 'Access Restriction',
				    height: 185,
				    width: 550,
				    html: '<p>Only sellers are allowed in checkout.</p>',
				    	buttons:[
							{text:"OK", action:function(){ 
								window.location = ah_local.wp;
							}},
						],
				    closed: function(){
				      window.location = ah_local.wp;
				      // ahtb.showClose(250);
				      // ahtb.closeOnClickBG(1);
				    }
				});
			}

			orderFlags = parseInt(orderFlags);
			if ( (orderFlags & SellerFlags.SELLER_IS_PREMIUM_LEVEL_1) &&
				 nickname.length == 0) {
				checkout.getPortal();
			}

			$('input[name=billing_first_name]').val(validSeller.first_name);
			$('input[name=billing_last_name]').val(validSeller.last_name);
			$('input[name=billing_company]').val(validSeller.company);
			$('input[name=billing_address_1]').val(validSeller.street_address);
			$('input[name=billing_city]').val(validSeller.city);
			$('input[name=billing_postcode]').val(validSeller.zip);
			$('input[name=billing_phone]').val(validSeller.phone);
			$('input[name=billing_phone]').mask('(999) 000-0000');

			if ( $('.woocommerce .order_details').length) {
				// var h = '<div class="store"><a href="'+ah_local.wp+"/product"+'">Go to store</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'+ah_local.wp+"/sellers"+'">Go back to seller dashboard</a></div>';
				var h = '<div class="store"><a href="'+ah_local.wp+"/my-account"+'">Go to MyAccount</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'+ah_local.wp+"/sellers"+'">Go back to seller dashboard</a></div>';
				$('.woocommerce .order_details').after(h);
			}			
		}

		this.checkPortalName = function(val, closeAhtb) {
			checkout.DBget({
				query: 'check-portal-name',
				data: val,
				done: function(d) {
	    			$('#marker').show();
	    			$('#portal').val(d);
	    			if (typeof closeAhtb != 'undefined')
	    				ahtb.close();
				},
				error: function(d) {
					ahtb.push(checkout.warnDuplicate);
					$('#portal').val('');
					$('#marker').hide();
				}
			});
		}

		this.getPortal = function() {
			var h = '<div id="define-portal">' +
						'<span id="title4">Please choose a portal name</span>'+
						'<span style="text-align:center;display:block;margin:.5em 0"><label id="base-url" style="letter-spacing:.05em;font-weight:400;font-size:1.1em;color:white"></label><input id="portal" type="text" placeholder="Your name..." style="margin-left:2px;margin-right:10px" /><span id="marker" class="entypo-check"></span><button id="check-it">Check it</button></span>' +
					'</div>';
			ahtb.open({html: h,
					   width: 550,
					   height: 170,
					   buttons:[
					   		{text:'Use It', action:function() {
					   			var val = $('#portal').val();
					   			if (val.length == 0) {
					   				ahtb.push(checkout.warnNoPortal);
									return;
					   			}
					   			else
					   				checkout.checkPortalName(val, true);
					   		}},
					   		{text:'Cancel', action: function() {
					   			ahtb.alert("Loading...", {width: 450, height: 150});
					   			window.location = ah_local.wp+"/sellers";
					   		}}],
					   opened: function() {
					   		$('#marker').css('color', 'lightgreen');
							$('#marker').hide();
							$('#base-url').html('LifestyledListings.com/');
							$('#portal').keyup(function(e){
								e.preventDefault();
								$('#marker').hide();
							})
							$('#check-it').on('click', function() {
								var val = $('#portal').val();
								if (val.length == 0) {
									ahtb.push(checkout.warnNoPortal);	
									return;
								}
								checkout.checkPortalName(val);
							});
					   }
					})
		}

		this.warnNoPortal = function() {
			ahtb.open({ html:"<p>Please enter a portal name.</p>", 
						height: 150,
						buttons: [
						 	{text:"OK", action: function() {
						 		ahtb.pop();
						 	}}],
						closed:function() {
							ahtb.pop();
						}});
		}

		this.warnDuplicate = function() {
			ahtb.open({html: "<p>The portal name "+val+" is already in use.</br>Please try a different name.</p>", 
					height: 180, 
					width: 450,
					buttons: [
						 	{text:"OK", action: function() {
						 		ahtb.pop();
						 	}}],
					closed: function() {
						ahtb.pop();
					}});
		}

		this.DBget = function(d){
			if(typeof d.query !== 'undefined'){
				var h = [];
				if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
				if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
				// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
				if (typeof d.error !== 'undefined') h.error = d.error;
				if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
				checkout.dbBusy = true;
				$.post(ah_local.tp+'/_sellers/_ajax.php', h.headers, function(){h.before()},'json')
					.done(function(x){
						checkout.dbBusy = false;
						if (x.status == 'OK') h.done(x.data);
						else if (h.error) h.error(x.data);
						else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
					})
					.fail(function(x){
						checkout.dbBusy = false;
						if (h.fail) h.fail(x);
						else ahtb.alert("Database failure, please retry.", {height: 150});
					});
			} else ahtb.alert('Query was undefined');
		}
	}

	checkout.init();
})