jQuery(document).ready(function($){
	myAccount = new function() {
		this.init = function() {
			console.log("myAccount init");
			if (validSeller == 0) {
				ahtb.open({
				    title: 'Access Restriction',
				    height: 185,
				    width: 550,
				    html: '<p>Only sellers are allowed in checkout.</p>',
				    	buttons:[
							{text:"OK", action:function(){ 
								window.location = ah_local.wp;
							}},
						],
				    closed: function(){
				      window.location = ah_local.wp;
				      // ahtb.showClose(250);
				      // ahtb.closeOnClickBG(1);
				    }
				});
			}

			if ( $('.woocommerce_account_subscriptions .no_subscriptions a').length != 0 ) {
				$('.woocommerce_account_subscriptions .no_subscriptions a').attr('href', ah_local.wp+"/agent-benefits");
				$('.woocommerce_account_subscriptions .no_subscriptions a').html("agent benefits page");
			}
			else {
				// var h = '<div class="store"><a href="'+ah_local.wp+"/product"+'">Go to store</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="'+ah_local.wp+"/sellers"+'">Go back to seller dashboard</a></div>';
				var h = '<div class="store"><a href="'+ah_local.wp+"/sellers"+'">Go back to seller dashboard</a></div>';
				$('.woocommerce_account_subscriptions').before(h);
				$('.woocommerce_account_subscriptions .shop_table td a').each(function(index, ele) {
					if ($(ele).hasClass('button') && $(ele).hasClass('renew'))
						$(ele).hide();
				})
			}
		}
	}

	myAccount.init();
})