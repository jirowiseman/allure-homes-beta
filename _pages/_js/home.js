
var waitedForFirstDirective = false;

function waitForDirective() {
  // $('#quiz button').prop('disabled', gettingDirective);
  if (gettingDirective)
    window.setTimeout(function() {
      waitForDirective();
      // console.log("wating for directive");
    }, 1000);
  else
    $('a.start').prop('disabled', gettingDirective);
}

function jumpToPredefinedQuiz(url) {
  $('div#overlay-bg').show();
    la( AnalyticsType.ANALYTICS_TYPE_CLICK,
              'PredefinedQuiz',
              ah_local.agentID,
              url,
              function() {
                window.location = url;
        },
        function() {
          window.location = url;
        }
    );
    
    return true;
}

jQuery(document).ready(function($){
  var cityName = '';
  myPortalRow = typeof myPortalRow == 'undefined' ? 0 : myPortalRow;
  console.log("home sees myPortalRow:"+myPortalRow);

  window.history.pushState('pure','Title',ah_local.wp);

  if (!waitedForFirstDirective) {
    $('a.start').prop('disabled', gettingDirective);
    window.setTimeout(function() {
      waitForDirective();
    }, 500);
    waitedForFirstDirective = true;
  }

  if (!isMobile &&
      showSideBar) {
    // $('#home-top .toptext').css('left', '150px');
    // $('#home-top #quiz').css('left', '343px');
    // $('#home-top #arrow-down img').css('left', '340px');
    $('div.three-image').css('padding-left', '280px');
    $('div.opening').css('padding-left', '200px');
  }



	$('.home-wrap a.start').on('click', function(){
    window.location = ah_local.wp+'/quiz/#sq=0';
  })
	
  // $('.home-wrap a.oldquiz-start').on('click',function(){
  //   var quizID = $(this).attr('quiz');
  //   var valid = true;
  //   var cityNames = [];
  //   if (quizID != 1) {
  //     // console.log("Search hit: "+$('#quiz .autocomplete').attr('city_id') );
  //     var val = $('#quiz .autocomplete').attr('city_id');
  //     var quizLoc = val ? parseInt(val) : 0;
  //     console.log( quizLoc );
  //     if (quizLoc == 0 || quizLoc <= cityLimit) {
  //       ga('send', {
  //         'hitType': 'event',          // Required.
  //         'eventCategory': 'button',   // Required.
  //         'eventAction': 'click',      // Required.
  //         'eventLabel': 'Quiz Search'
  //       });
  //       la(AnalyticsType.ANALYTICS_TYPE_CLICK,
  //         'Quiz Search');
  //       var city = '';
  //       if ( (city = $('.autocomplete input').val()) != '' &&
  //             city != "ENTER CITY OR STATE") {
  //           $('<span class="validation-text" style="color:white">Validating entry...</span>').appendTo('#quiz .autocomplete').hide().fadeIn({duration:250,queue:false});
  //           $.post(ah_local.tp+'/_pages/ajax-quiz.php',
  //               { query:'find-city',
  //                 data: { city: city,
  //                         distance: doDistance ? distance : 0 }
  //               },
  //               function(){}, 'JSON').done(function(d){
  //                 $('#quiz .autocomplete span').fadeOut({duration:250,queue:false,complete:function(){$(this).remove()}});                        
  //                 if (d &&
  //                     d.data &&
  //                     d.data.city &&
  //                     (typeof d.data.city == 'object' ||
  //                       d.data.city.length) ){
  //                   if (d.status == 'OK' &&
  //                       (typeof d.data.city == 'object' || d.data.city.length) ) {
  //                     console.log(JSON.stringify(d.data));

  //                     if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
  //                       ah.quiz.validate(QuizMode.STATE, [], 0, 0, [d.data.city[0]]);
  //                       return;
  //                     }

  //                     var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
  //                     if (len > 1) {
  //                       if (d.data.allState == 'false' || d.data.allState == false) {
  //                         var h = '<p>Please pick one of the cities:</p>' +
  //                                   '<ul class="cities">';
  //                         for(var i in d.data.city) {
  //                           h += '<li><a href="javascript:cityPick('+d.data.city[i].id+','+quizID+",['"+d.data.city[i].city+','+d.data.city[i].state+"'],"+doDistance+','+distance+');">'+d.data.city[i].city+', '+d.data.city[i].state+'</a></li>';
  //                         }
  //                         h += '</ul>';
  //                         ahtb.open(
  //                           { title: 'Cities List',
  //                             width: 380,
  //                             height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
  //                             html: h,
  //                             buttons: [
  //                                   {text: 'Cancel', action: function() {
  //                                     ahtb.close();
  //                                   }},
  //                                   {text: 'Use All', action: function() {
  //                                     var cities = [];
  //                                     for(var i in d.data.city) {
  //                                       cities[i] = d.data.city[i].id;
  //                                       cityNames[cityNames.length] = d.data.city[i].city+', '+d.data.city[i].state;
  //                                     }
  //                                     ah.quiz.validate(quizID, cities, doDistance, distance, cityNames);
  //                                   }}],
  //                             opened: function() {
  //                               // $('#TB_ajaxContent ul').css('height', (150 + (len * 25)) < 460 ? (150 + (len * 25)) : 460)
  //                             }
  //                           })
  //                       }
  //                       else {
  //                         var cities = [];
  //                         for(var i in d.data.city) {
  //                           cities[i] = d.data.city[i].id;
  //                           cityNames[cityNames.length] = d.data.city[i].city+', '+d.data.city[i].state;
  //                         }
  //                         // doDistance = true;
  //                         // distance = 30; // much faster than city/state search
  //                         ah.quiz.validate(quizID, cities, doDistance, distance, cityNames);
  //                       }
  //                     } // only one!
  //                     else {
  //                       cityNames[cityNames.length] = d.data.city[0].city+', '+d.data.city[0].state;
  //                       ah.quiz.validate(quizID, [d.data.city[0].id], doDistance, distance, cityNames);
  //                     }
  //                   }
  //                   else {
  //                     $('#quiz .autocomplete input').addClass('invalid');
  //                     ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, {title: 'City Match', 
  //                               height:180, 
  //                               width:600});
  //                     // ahtb.alert('Failed find a matching city id for '+city, {title: 'City Match', height:150, width:600});
  //                   }
  //                 }
  //                 else {
  //                     $('#quiz .autocomplete input').addClass('invalid');
  //                     ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, {title: 'City Match', 
  //                                 height:180, 
  //                                 width:600});                    
  //                 }
  //               });
  //         return;
  //       }
  //       else
  //         valid = false;
  //     }
  //     else {
  //       quizLoc -= cityLimit;
  //       cityNames[cityNames.length] = cityName;
  //     }
  //   }
  //   else // general Start nation-wide
  //     ga('send', {
  //         'hitType': 'event',          // Required.
  //         'eventCategory': 'button',   // Required.
  //         'eventAction': 'click',      // Required.
  //         'eventLabel': 'Quiz Start'
  //       });
  //     la(AnalyticsType.ANALYTICS_TYPE_CLICK,
  //       'Quiz Start');

  //   if (!valid) { $('#quiz .autocomplete input').addClass('invalid');
  //     ahtb.alert('Please select a valid city or state before continuing.', {title: 'City Match', height:150, width:600}); }
  //   else
  //     ah.quiz.validate(quizID, quizLoc, doDistance, distance, cityNames);
  // });
});