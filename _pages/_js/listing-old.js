var listings;

var SellerTasks = {
  MORE_TAGS: 0,
  HIRES_PHOTOS: 1,
  BETTER_DESC: 2,
  ADD_BEDS: 3,
  ADD_BATHS: 4,
  ADD_SELLER_PHOTO: 5,
  WRITE_AREA_BLOG: 6
}

var MetaDataType = {
  SELLER_IMPROVEMENT_TASKS: (1 << 0),
  SELLER_KEY: (1 << 1),
  SELLER_BRE: (1 << 2),
  SELLER_INVITE_CODE: (1 << 3)
}

var ImageEditType = {
  TRIM: 0,
  BLACKOUT: 1,
  GENERATE: 2
}
var SellerImprovements = [
  {visible: "Tags",
   element: 'tags',
   action: SellerTasks.MORE_TAGS},

  {element: 'images',
   visible: "HiRes Photos",
   action: SellerTasks.HIRES_PHOTOS},

  {element: 'about',
   visible: 'Better description',
   action: SellerTasks.BETTER_DESC},

  {element: 'beds',
   visible: 'Beds',
   action: SellerTasks.ADD_BEDS},

  {element: 'baths',
   visible: 'Baths',
   action: SellerTasks.ADD_BATHS},

  {element: 'seller-photo',
   visible: "Seller Photo",
   action: SellerTasks.ADD_SELLER_PHOTO},

  {element: 'area-blog',
   visible: 'Write Area Blog',
   action: SellerTasks.WRITE_AREA_BLOG}
]

var ActiveState = {
  INACTIVE: 0,
  ACTIVE: 1,
  WAITING:   2,
  REJECTED: 3,
  TOOCHEAP: 4,
  NEVER: 5,
  AVERAGE: 6
}

var ActiveStateNames = [
  'inactive',
  'active',
  'waiting',
  'rejected',
  'toocheap',
  'never',
  'average'
]

var ButtonOptions = [ [1,5], [2,3,5], [1,3,5], [1,2,5], [1,2,3,5,6], [2], [2,5] ];
listingAgentDetail = decodeURIComponent(listingAgentDetail);

jQuery.extend({
    keyCount : function(o) {
        if(typeof o == "object") {
            var i, count = 0;
            for(i in o) {
                if(o.hasOwnProperty(i)) {
                    count++;
                }
            }
            return count;
        } else {
            return false;
        }
    }
});

function showAgentExpertise(i, a) {
  var tag = premierAgents[i].tags[a];
  var h = '<div id="expertise"><p>'+(typeof tag.user_profile != 'undefined' && tag.user_profile && tag.user_profile.length ? tag.user_profile : tag.desc)+'</p></div>';
  height = 150 + (h.length/100)* 30;
  ahtb.open({html: h,
            width: 600,
            height:height});
}

function makeCoverAgent(index) {
  var metas = typeof premierAgents[index].meta == 'string' ? $.parseJSON(premierAgents[index].meta) : premierAgents[index].meta;
  var meta = null;
  // gather everyone's language specialties
  for(var j in metas) {             
    if (parseInt(metas[j].action) == SellerMetaFlags.SELLER_PROFILE_DATA) {
      meta = metas[j];
      break;
    }
  }

  var languages = '';
  if (meta && meta.languages)
    for(var k in meta.languages)
      languages += languages.length ? ","+meta.languages[k] : meta.languages[k];

  var date = new Date();
  var yr = date.getFullYear() + 1;
  var showQualifications = meta && 
                           (parseInt(meta.inArea) != -1 ||
                           parseInt(meta.sold) != 0 ||
                           parseInt(meta.year) != -1 ||
                           languages.length);

  var h = '<section id="top-section">' +
            '<div id="header">' +
              '<span id="title">Lifestyled Agents</span>' +
              // '<span id="title2">Agents that match you in</span>'+
              // '<span id="title3">'+premierAgent[index].city+", "+premierAgent[index].state+'</span>' +
            '</div>'+
            '<div id="left-side">' +
              '<div id="agent-meta1">' +
                '<span id="name">'+premierAgents[index].first_name+' '+premierAgents[index].last_name+'</span>' +
								'<div id="locality">' +
									'<span id="city">'+premierAgents[index].city+'</span><span id="state">,'+premierAgents[index].state+'</span>' +
								'</div>'; // end locality
                if (userID == 8)
                  h += '<span id="percentage">'+premierAgents[index].percentage+'&#37 Match</span>';
                else if (typeof premierAgents[index].company != 'undefined' &&
                         premierAgents[index].company != null &&
                         premierAgents[index].company.length)
                  h += '<span id="percentage">'+premierAgents[index].company+'</span>';
            h +='<img src="'+ah_local.tp+'/_img/_authors/250x250/'+(premierAgents[index].photo != null ? premierAgents[index].photo : '_blank.jpg')+'" />' +
              '</div>'+
              '<button id="view-profile">View Profile</button>'+
            '</div>' + // end left-side

            '<div id="right-side">';
            if (showQualifications) {
            h+='<div id="qualifications">' +
                '<span class="sub-title">Qualifications</span>' +
                '<ul id="qualifications">';
                if (meta) {
                  if (parseInt(meta.inArea) != -1)
                    h+= "<li >- Resident for over "+(yr - meta.inArea)+' years</li>';
                  if (parseInt(meta.year) != -1)
                    h+= "<li >- "+(yr - meta.year)+' Years of experience</li>';
                  if (parseInt(meta.sold) != 0)
                    h+= "<li >- "+meta.sold+' Transactions per year</li>';
                  if (languages.length)
                    h+= "<li >- Business language(s): "+languages+'</li>';
                }
            h+= '</ul>' +
              '</div>'; // end qualifications
            }
            h+='<div id="specialties">' +
              '<span class="sub-title">Agent Specialties</span>' +
                '<ul id="specialties">';
              var count = 0;
              for(var a in premierAgents[index].tags) {
                if (premierAgents[index].tags[a].tag != null &&
                    premierAgents[index].tags[a].icon != null &&
                    count < 3) {
                  count++;
                  h += '<li><a href="javascript:showAgentExpertise('+index+','+a+');"><img src="'+ah_local.tp+'/_img/tag_icons/'+premierAgents[index].tags[a].icon+'" style="width:35px;height:35px" title="'+(typeof premierAgents[index].tags[a].user_profile != 'undefined' && premierAgents[index].tags[a].user_profile && premierAgents[index].tags[a].user_profile.length ? premierAgents[index].tags[a].user_profile : premierAgents[index].tags[a].desc)+'"/></a><span id="tag-desc">'+premierAgents[index].tags[a].tag+'</span></li>';
                }
              }
            h+= '</ul>' +
              '</div>' + // end specialties
              '<div id="contact">' +
                '<a href="#" class="entypo-mail contact-button" style="width:50px;margin-right:5px;" for="'+index+'"><span>&nbsp;Email Me</span></a>' +
              '</div>' + // end contact
            '</div>' + // end ight-side
          '</section>'; // top-section
  return h;
}

function showAllPremierAgents(index) {
  if (premierAgents == "0") {
    console.log("Hey, premierAgents is 0!!");
    return;
  }

  currentPos = 0;

  var h = '<div id="all-agents">' + 
            '<div id="coverAgent"></div>' +
            '<div id="agent-list">' +
              '<ul class="premier-agents-list" />' + //style="display: inline; overflow: auto;" />' +
            '</div>' +
            '<div id="buttonDiv">' +
              '<div id="prev">' +
                '<a href="javascript:;" for="prev"><span class="entypo-left-open-big" style="font-size:2em;display:inline-block;margin-top:28px;margin-left:127px;float:left"></span><span style="display:inline-block;margin-top:29px;margin-left:10px;float:left;text-transform:uppercase;font-weight:400;font-size:.75em"> Back</span></a>' +
              '</div>' +
              '<div id="next">' +
                '<a href="javascript:;" for="next"><span style="display:inline-block;margin-top:29px;margin-left:123px;float:left;text-transform:uppercase;font-weight:400;font-size:.75em">Next </span><span class="entypo-right-open-big" style="font-size:2em;display:inline-block;margin-top:28px;margin-left:10px;float:left"></span></a>' +
              '</div>' +
          '</div>';
  var list = '';
  for (var i in premierAgents) {
    // if (i == index)
    //   continue;
    var attr = '';
    list += '<li>'+
              '<div class="agent-li '+(index == i ? 'active' : '')+'">' +
                // '<a href="javascript:agentProfile('+"'"+ah_local.wp+'/agent/'+premierAgent[i].nickname+"'"+');" target="_blank"><img src="'+ah_local.tp+'/_img/_authors/55x55/'+(premierAgent[i].photo != null ? premierAgent[i].photo : '_blank.jpg')+'"/></a>' +
                '<a href="javascript:;" for="'+i+'"><img src="'+ah_local.tp+'/_img/_authors/55x55/'+(premierAgents[i].photo != null ? premierAgents[i].photo : '_blank.jpg')+'"/></a>' +
                '<div class="meta" style="width: 150px;">' +
                  // '<a href="javascript:agentProfile('+"'"+ah_local.wp+'/agent/'+premierAgent[i].nickname+"'"+');" target="_blank"><h4 class="name" style="margin: 0; width: 150px;">'+premierAgent[i].first_name+' '+premierAgent[i].last_name+'</h4></a>' +
                  '<a href="javascript:;" for="'+i+'"><h4 class="name" style="margin: 0; width: 150px;">'+premierAgents[i].first_name+' '+premierAgents[i].last_name+'</h4></a>' +
                  '<span id="percentage" style="display: '+(userID == 8 ? 'block' : 'none')+';">'+premierAgents[i].percentage+'&#37; Match</span>' +
          //         '<span class="company">'+premierAgent[i].company+'</span>' +
          //         '<table class="tag-icons">' +
          //           '<tbody>' +
          //             '<tr>';
          //             var count = 0;
          //             for(var a in premierAgent[i].tags) {
          //         list += '<td><a href="javascript:explainIconsPremier(premierAgent);"><img src="'+ah_local.tp+'/_img/tag_icons/'+premierAgent[i].tags[a].icon+'" style="width:40px;height:40px"/></a></td>';
          //               attr += (count++ ? ", " : '')+premierAgent[i].tags[a].tag;
          //               if (count == 3) // max
          //               break;
          //             }
          // list +=     '</tr>' +
          //           '</tbody>' +
          //         '</table>' +
          //         '<span class="attr" ">'+attr+'</span>' +
          //       '<span class="location" ">in '+premierAgent[i].city+", "+premierAgent[i].state+'</span>'+
              '</div>'+
            '</li>';
  }
  ahtb.open({
    title: "Premier Agents",
    width: 700,
    height: 620,
    html: h,
    hideSubmit: true,
		hideTitle: true,
    opened: function() {
      currentPos = index;
      $('#all-agents #agent-list .premier-agents-list').html(list);
      // $('#agent-list .ul li:nth-child('+(currentPos+1)+')').hide();

     $('#all-agents #agent-list .premier-agents-list li a').on('click', function() {
        var val = parseInt($(this).attr('for'));
        console.log("Agent: "+val);
        if (val == currentPos)
          return;

        // $('#all-agents #agent-list .premier-agents-list li div.active').parent().removeClass('active');
        // var div = 
        $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').removeClass('active');
        // var div = $(li, ':nth-child(1)');
        // div.removeClass('active');
        // $('#agent-list .ul li:nth-child('+(currentPos+1)+')').show();
        // $('#agent-list .ul li:nth-child('+(val+1)+')').hide();
        currentPos = val;
        $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').addClass('active');
        //$(this).parent().addClass('active');
        $('#all-agents #coverAgent').html(makeCoverAgent(val));
        $('#contact .contact-button').on('click', function() {
            var agentId = $(this).attr('for');
            console.log("Contact button for ", agentId);
            var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
            ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, isPortal ? portalAgent.id : 0, userID);
        })
        $('#coverAgent #view-profile').on('click', function() {
            console.log("view profile clicked");
            var nickname = premierAgents[val].nickname.length ? premierAgents[val].nickname : premierAgents[val].author_id;
            agentProfile(ah_local.wp+'/agent/'+nickname+":"+listing_info.id);
        })

       //showAllPremierAgents(val);
      })
      $('#all-agents #coverAgent').html(makeCoverAgent(index));
      $('#coverAgent #view-profile').on('click', function() {
        console.log("view profile clicked");
        var nickname = premierAgents[index].nickname.length ? premierAgents[index].nickname : premierAgents[index].author_id;
        agentProfile(ah_local.wp+'/agent/'+nickname+":"+listing_info.id);
      })

      $('#contact .contact-button').on('click', function() {
          var agentId = $(this).attr('for');
          console.log("Contact button for ", agentId);
          var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
          ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, isPortal ? portalAgent.id : 0, userID);
      })
 
      $('#buttonDiv a').on('click', function() {
        var val = $(this).attr('for');
        var top = $('#all-agents #agent-list .premier-agents-list').scrollLeft();
        console.log("pressed for "+val+", top:"+top);
        switch(val) {
          case "prev": 
            if (!currentPos)
              return;
            $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').removeClass('active');
            currentPos--;
            $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').addClass('active');
            var width = $('#agent-list ul li:nth-child('+(currentPos+1)+')').width();
            console.log("Width: "+width);
            // $('.premier-agents-list').animate({
              // scrollLeft: $('.premier-agents-list li:nth-child('+currentPos+')').position.left
            $('#agent-list').animate({
              scrollLeft: currentPos*width
            },'slow');
            $('#all-agents #coverAgent').html(makeCoverAgent(currentPos));
            $('#contact .contact-button').on('click', function() {
                var agentId = $(this).attr('for');
                console.log("Contact button for ", agentId);
                var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
                ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, isPortal ? portalAgent.id : 0, userID);
            })
            $('#coverAgent #view-profile').on('click', function() {
              console.log("view profile clicked");
              var nickname = premierAgents[currentPos].nickname.length ? premierAgents[currentPos].nickname : premierAgents[currentPos].author_id;
              agentProfile(ah_local.wp+'/agent/'+nickname+":"+listing_info.id);
            })
            break;
         case "next": 
            if (currentPos == (premierAgents.length - 1))
              return;
            $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').removeClass('active');
            currentPos++;   
            $('.premier-agents-list li:nth-child('+(currentPos+1)+') div.agent-li').addClass('active');
            var ele = $('#agent-list ul li:nth-child('+(currentPos)+')');
            var width = ele.width();
            console.log("Width: "+width);
            // $('.premier-agents-list').animate({
              // scrollLeft: $('.premier-agents-list li:nth-child('+currentPos+')').position.left
            $('#agent-list').animate({
              scrollLeft: currentPos*width
            },'slow');
            $('#all-agents #coverAgent').html(makeCoverAgent(currentPos));
            $('#contact .contact-button').on('click', function() {
                var agentId = $(this).attr('for');
                console.log("Contact button for ", agentId);
                var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
                ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, isPortal ? portalAgent.id : 0, userID);
            })
            $('#coverAgent #view-profile').on('click', function() {
              console.log("view profile clicked");
              var nickname = premierAgents[currentPos].nickname.length ? premierAgents[currentPos].nickname : premierAgents[currentPos].author_id;
              agentProfile(ah_local.wp+'/agent/'+nickname+":"+listing_info.id);
            })
            break;
        }
      })
    }
  })
  

}

jQuery(document).ready(function($){
  listings = {
    init: function(){
      listings.currentTag = 0;
      listings.gallery.images = [];
      listings.video = videojs('listing');

      listings.preppingPOI = false;
      listings.waitingToGoToExploreArea = false;
      listings.prepExploreArea();

      $('.videoDiv .closeVid').on('click', function() {
        listings.video.pause();
      });

      $('.gallery-full').on('click',function(){ 
        listings.gallery.open() 
      });

      $('.meta .contact-button').on('click', function() {
        var agentId = $(this).attr('for'); 
        console.log("Contact button for ", agentId);
        var mode = agentId == seller.id ? FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT;
        var agentId2 = mode == FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT && isPortal ? portalAgent.id : 0;
        ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, agentId2);
      })

      $('a#client-share').on('click', function() {
        var mode = FeedbackMode.MESSAGE_EMAIL_CLIENT_AS_AGENT;
        var content = 'View this listing at '+listing_info.street_address+'.';
        ahfeedback.openModal(mode, sellerId, listing_info.id, listing_info.listhub_key, 0, 0, null, null, content);
      })

      if (portalAgent != "0") {
        if (isPortal) {
          $('.agent').addClass('agent-squeeze');
          $('.listing-about').addClass('with_portal')
          $('.premier-agent').show();
        }
      }

      sellerId ? $('a#client-share').show() : $('a#client-share').hide();

      if (premierAgents != "0") {
          $('.listing-about').addClass('with_premier')
          $('.premier-agents').show();
          var list = '';
          countP = 0;
          var languages = [];
          var haveExperts = false;
          for (var i in premierAgents) {
            var attr = '';
            if (countP < 3) {
              list += '<li style="display: inline">'+
                        '<div class="agent-li" style="width: 33%; margin: 0 0 0 0; float: left;">' +
													'<div class="agent-li-contact" style="float:left;">' +
                          	'<a href="javascript:showAllPremierAgents('+countP+');"><img src="'+ah_local.tp+'/_img/_authors/250x250/'+(premierAgents[i].photo != null ? premierAgents[i].photo : '_blank.jpg')+'"/></a>' +
														'<button id="view-profile" class="contact-button" for="'+premierAgents[i].id+'">Contact</button>' +
													'</div>' +
                          '<div class="meta" style="width: 150px;">' +
                            // '<a href="javascript:agentProfile('+"'"+ah_local.wp+'/agent/'+premierAgent[i].nickname+"'"+');" target="_blank"><h4 class="name" style="margin: 0; width: 150px;">'+premierAgent[i].first_name+' '+premierAgent[i].last_name+'</h4></a>' +
                            '<a href="javascript:showAllPremierAgents('+countP+');"><h4 class="name" style="margin: 0; width: 150px;">'+premierAgents[i].first_name+' '+premierAgents[i].last_name+'</h4></a>' +
                            '<span class="location" "><span style="color:#2582e1;">'+premierAgents[i].city+", "+premierAgents[i].state+'</span></span>' +
														'<div class="tag-icons" style="width: 40px;margin-right: 8px;float: left;">';
                                var count = 0;
                                for(var a in premierAgents[i].tags) {
                                  haveExperts = true;
                                   if (premierAgents[i].tags[a].tag != null &&
                                       premierAgents[i].tags[a].icon != null &&
                                        count < 3) {
                                list += '<div id="tagUnit"><a href="javascript:showAgentExpertise('+i+','+a+');" style="display:block"><img src="'+ah_local.tp+'/_img/tag_icons/'+premierAgents[i].tags[a].icon+'" style="width:38px;height:38px;border:0;margin-bottom: 2px;margin-top: 2px;" title="'+(typeof premierAgents[i].tags[a].user_profile != 'undefined' && premierAgents[i].tags[a].user_profile && premierAgents[i].tags[a].user_profile.length ? premierAgents[i].tags[a].user_profile : premierAgents[i].tags[a].desc)+'"/>'+
                                        '</a></div></td>';
                                      attr += (count++ ? '<div style="height: 28px;"></div>' : '')+premierAgents[i].tags[a].tag;
                                    }
                                  if (count == 3) // max
                                    break;
                                }
                    	list += '</div>' +
                            '<span class="attr" style="text-transform:capitalize;margin-top: 13px;font-size: .95em;font-family:Open Sans;font-weight:400;letter-spacing: 1px;">'+attr+'</span>';
                list += '</div>'+
                      '</li>';
            }
            countP++;
            var meta = typeof premierAgents[i].meta == 'string' ? $.parseJSON(premierAgents[i].meta) : premierAgents[i].meta;
            // gather everyone's language specialties
            for(var j in meta) {             
              if (parseInt(meta[j].action) == SellerMetaFlags.SELLER_PROFILE_DATA)
                if (meta[j].languages)
                  for(var k in meta[j].languages)
                    if (languages.indexOf(meta[j].languages[k]) == -1)
                      languages[languages.length] = meta[j].languages[k];
            }

            if ( parseInt(ah_local.agency_id) != 0) {
              var baseMsg = ah_local.agency_shortname+" Agents that are experts in your ";
              if (haveExperts)
                baseMsg += "lifestyle";
              else
                baseMsg += "area";
              $('.premier-agents #agents-header').html(baseMsg);
            }

          }
          if (languages.length) {
            var h = '';
            var haveNonEnglish = false;
            for(var i in languages) {
              h += '<option value="'+languages[i]+'">'+languages[i]+'</option>';
              if (languages[i] != "English")
                haveNonEnglish = true;
            }
            if (haveNonEnglish) {
              $('.premier-agents #languages').html(h);
              $('.premier-agents #languages').show();
              $('.premier-agents #languageLabel').show();
              $('.premier-agents #agents-header').css('margin-left','-230px');
              $('.premier-agents #header #languages').change(function(){
                var val = $('.premier-agents #languages option:selected').val();
                countP = 0;
                var list = '';         
                for (var i in premierAgents) {
                  var attr = '';    
                  var meta = typeof premierAgents[i].meta == 'string' ? $.parseJSON(premierAgents[i].meta) : premierAgents[i].meta;
                  // gather everyone's language specialties
                  var valid = false;
                  if (val != 'English')
                    for(var j in meta) {             
                      if (parseInt(meta[j].action) == SellerMetaFlags.SELLER_PROFILE_DATA)
                        if (meta[j].languages)
                          for(var k in meta[j].languages)
                            if (meta[j].languages[k] == val) {
                              valid = true;
                              break;
                            }
                    }
                  else valid = true;
                  if (valid && countP < 3) {
                    list += '<li style="display: inline">'+
                              '<div class="agent-li" style="width: 330px; margin: 0 0 0 0; float: left;">' +
                                '<a href="javascript:showAllPremierAgents('+countP+');"><img src="'+ah_local.tp+'/_img/_authors/250x250/'+(premierAgents[i].photo != null ? premierAgents[i].photo : '_blank.jpg')+'"/></a>' +
                                '<div class="meta" style="width: 150px;">' +
                                  '<a href="javascript:showAllPremierAgents('+countP+');"><h4 class="name" style="margin: 0; width: 150px;">'+premierAgents[i].first_name+' '+premierAgents[i].last_name+'</h4></a>' +
                                  '<span class="location" ">in '+premierAgents[i].city+", "+premierAgents[i].state+'</span>'+
                                  '<table class="tag-icons">' +
                                    '<tbody>' +
                                      '<tr>';
                                      var count = 0;
                                      for(var a in premierAgents[i].tags) {
                                  list += '<td><a href="javascript:explainIconsPremier(premierAgent);"><img src="'+ah_local.tp+'/_img/tag_icons/'+premierAgents[i].tags[a].icon+'" style="width:40px;height:40px;border:0"/></a></td>';
                                        attr += (count++ ? ", " : '')+premierAgents[i].tags[a].tag;
                                        if (count == 3) // max
                                          break;
                                      }
                          list +=     '</tr>' +
                                    '</tbody>' +
                                  '</table>' +
                                  '<span class="attr" ">'+attr+'</span>' +
                                '</div>'+
                            '</li>';
                    countP++;
                  }
                }
               $('.premier-agents ul').html(list);
              })
            }
          }
          $('.premier-agents ul').html(list);
          $('.premier-agents ul').css('width', (premierAgents.length*100)+'px' );

          $('.agent-li .contact-button').on('click', function() {
            var agentId = $(this).attr('for');
            console.log("Contact button for ", agentId);
            var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
            ahfeedback.openModal(mode, agentId, listing_info.id, listing_info.listhub_key, isPortal ? portalAgent.id : 0, userID);
        })
      }
      

      if ($('.adminControl').length == 0) {
        console.log("no adminControl");
        var content = ''
        if (emailTracker != 0) {
          if (emailTracker.meta &&
              emailTracker.meta.length) for(var i in emailTracker.meta) {
            if (emailTracker.meta[i].action == EmailTrackerType.ET_CONTENT ||
                emailTracker.meta[i].action == EmailTrackerType.ET_REPLY)
              content = emailTracker.meta[i].content;
          }

          // figure out what is the action type for the email that's going out to respond to this incoming message
          var actionType = 0;
          switch(emailTracker.flags) {
            case FeedbackMode.MESSAGE_EMAIL_AGENT:
            case FeedbackMode.MESSAGE_EMAIL_LISTING_AGENT:
            case FeedbackMode.MESSAGE_REPLY_TO_AGENT:
              actionType = FeedbackMode.MESSAGE_REPLY_TO_USER;
              break;
            case FeedbackMode.MESSAGE_REPLY_TO_USER:
              actionType = FeedbackMode.MESSAGE_REPLY_TO_AGENT;
              break;
            case FeedbackMode.MESSAGE_SELLER:
              actionType = FeedbackMode.MESSAGE_CONTACT_US;
              break;
            case FeedbackMode.MESSAGE_EMAIL_FROM_LLC:
              break;
          }

          if (actionType != 0) // else must be a click back from an email we sent
            ahfeedback.openModal(actionType, emailTracker.agent_id, emailTracker.listing_id, emailTracker.listhub_key, 0,
                                emailTracker.user_id, emailTracker.first, emailTracker.user_email, content,
                                etId, emailTracker.last);
          window.history.pushState('pure','Title',ah_local.wp+'/listing/'+emailTracker.listing_id);
        }
        else if (ah_local.redirect.length) {
          var page = 'listing';
          console.log("before @2 pushState for "+page+", redirect:"+ah_local.redirect);
          var pushValue = command == 'seo' && ah_local.redirect.length ? ah_local.redirect : '/'+page+'/'+listing_info.id;
          window.history.replaceState('pure','Title',ah_local.wp+pushValue);
          la( AnalyticsType.ANALYTICS_TYPE_EVENT,
              'SEO',
              listing_info.id,
              ah_local.redirect);
          ah_local.redirect = '';
          setCookie('RedirectURL', '', 1);
        }
      }
      else {
        console.log("got adminControl!");
        var tagsFromPage = listing_info.tags;
        var h = [];
        h[0] = '<div class="add-tags"><div id="tag-window"></div></div>';
        $('.adminControl').html(h[0]);

        $(window).bind("unload", function() { 
          var beds = $('.bedBathControl #beds').val();
          var baths = $('.bedBathControl #baths').val();
          if (beds != listing_info.beds ||
              baths != listing_info.baths) {
            listings.saveBedsBaths(beds, baths);
          }

        });
                // h[1]+= '<button type="button" name="back" value="Back" class="button-back"><span class="entypo-left-circled icon" data-text="right-open-big"></span> <span class="text">Back</span></button>'+
                // '<button type="button" name="next" value="Next" class="button-next"><span class="text">Next</span> <span class="entypo-right-circled icon" data-text="right-open-big"></span></button>';
        if (true) {    
          $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "get-tags"}, 
              dataType: 'json',
              type: 'POST',
              success: function(d){
                if (d.status == 'OK'){
                  var hh = ['<ul class="tag-categories">',''];
                  var pos = 1;
                  for (var i in d.data) if (d.data[i].length > 0){
                      var cat = new function(){};
                      cat.tags = d.data[i];
                      cat.title = i.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                      hh[0]+= '<li class="tag-category" tag-category="'+i.replace(/ /g, '-')+'" tag-pos='+pos+'>'+cat.title+'<span class="entypo-record"></span></li>';
                      hh[1]+= '<ul class="tags-list" tag-category="'+i.replace(/ /g, '-')+'"';
                      hh[1]+= ' style="width: '+250*Math.ceil(cat.tags.length/7)+'px">';
                      for (var j in cat.tags){
                        var match = false;
                        for (var k in tagsFromPage){ if (tagsFromPage[k].tag == cat.tags[j].tag){match=true;break;} }
                        hh[1]+= '<li' +' data-tag-id="'+cat.tags[j].id+'" data-tag-type="'+cat.tags[j].type+'" class="tag';
                        if (match===true) hh[1]+= ' matched';
                        hh[1]+= '" tag="'+encodeURI(cat.tags[j].tag)+'" description="'+cat.tags[j].description+'"><span class="text"><span class="checkmark"><img src="'+ah_local.tp+'/_img/_sellers/check.png" /></span>'+cat.tags[j].tag+'</span></li>';
                      }
                      hh[1]+= '</ul>';
                      pos++;
                  }
                  hh[0]+= '</ul>';
                  hh[0]+= '<div class="tag-next" > <button type="button" name="next" value="Next" id="tagNextButton"><span class="text">More Tags</span> <span class="entypo-right-circled" data-text="right-circled"></span></button> </div>'
                  // hh[0]+= '<div class="tag-definition"><h5>Definition:</h5><span></span></div>';
                  // hh[0]+= '<div class="main-info"><h2>4. Tags</h2><p>Tag your listings with simple defining terms and let our matching system put your home in front of the right buyers.</p></div>';

                  // hh[0]+= '<div class="tag-confirm"> <input type="checkbox" name="agreement" id="tag-agree-terms">I agree to the <a href="javascript:listingAdmin.showTermsAndConditions();"> Terms and Conditions. </a> </input>  </div>'
                  var tagsToAdd = [];
                  var tagsToRemove = [];
                  $('#tag-window').append(hh[0]);
                  $('#tag-window').append(hh[1]);
                  $('.tags-list').hide();
                  // $('#tag-agree-terms').prop("disabled", false);
                  // $('#tag-agree-terms').prop("checked", parseInt(listingAdmin.listing.agreedTagTerms) == 0 ? false : true);
                  $('#tagNextButton').on('click', function() {
                    var nextTagPos = listings.currentTag+1;
                    if (listings.currentTag == $('.tag-category').length)
                      nextTagPos = 1;
                    $('[tag-pos='+nextTagPos+']').click();
                  });
                  $('.tag-category').each(function(){
                    $(this).on('click', function(){ 
                      var newCat = $(this).attr('tag-category');
                      var tagPos = $(this).attr('tag-pos');
                      listings.currentTag = parseInt(tagPos);
                      var delay = 0;
                      $('.tag-category').removeClass('selected');
                      $('li[tag-category='+newCat+']').addClass('selected');
                      $('ul.tags-list').each(function(){ 
                        if ($(this).css('display') != 'none'){ delay+= 150; $(this).fadeOut(150); }
                      });
                      if ($('ul[tag-category='+newCat+']').css('display') == 'none'){
                        setTimeout(function(){
                          $('ul[tag-category='+newCat+']').fadeIn(250);
                          delay = 0;
                        }, delay);
                      }
                    });
                  });
                  $('li.tag').each(function(){
                    $(this).on('mouseover', function(){
                      var theTag = $(this).attr('tag').replace(/%20/g, ' ').replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                      var theDef = $(this).attr('description') == 'null' ? '(none)' : $(this).attr('description');
                      $('.tag-definition h5').html(theTag+':');
                      $('.tag-definition span').html(theDef);
                    });
                    $(this).on('click',function(){
                      //listingAdmin.pageHasUnsavedData = true;
                      var oldTags = listing_info.tags;
                      var theTag = $(this).attr('tag').replace(/%20/g, ' ');
                      var theType = $(this).attr('data-tag-type')
                      var id = $(this).attr('data-tag-id');
                      if ( $(this).hasClass('matched') ){
                        var t = [];
                        for (var i in oldTags) if (oldTags[i].tag != theTag) t.push(oldTags[i]);
                        listing_info.tags = t;
                        $(this).removeClass('matched');
                        console.log( "no match " );
                      } else {
                        var found = false;
                        var newTag = new function(){}; // make an object
                        newTag.id = parseInt(id);
                        newTag.tag = theTag;
                        newTag.type = theType;
                        for (var i in oldTags){ if (oldTags[i].tag == theTag){found=true;break;} }
                        if (typeof listing_info.tags == 'string') listing_info.tags = $.parseJSON(listing_info.tags);
                        if (!found) {
                          // if (typeof listing_info.tags != 'object')
                          //   listing_info.tags = [];
                          if ( Array.isArray(listing_info.tags) )
                            listing_info.tags.push(newTag);
                          else
                            listing_info.tags[id] = newTag;
                        }
                        $(this).addClass('matched');
                        console.log ( "matched " );
                      }
                 
                      listings.saveTags();
                    });
                  });
                  $('[tag-category=home-features]').click();
                  
                  console.log("end process tags");
                  listings.createBedBathControl();
                  listings.createSellerAdminControl();
                  listings.createImageEditControl();
                  listings.createTransferControl();
                }
              },
              error: function(d) {
                ahtb.alert('Database Tag Error'+'<pre>'+d.data+'</pre>',
                            {height: 120});
                $('#adminTags').html('<h2>Failed to load tags</h2>');
              }
          });
        }
          // $('footer .controls button.button-back').on('click',function(){ listingAdmin.getPage(listingAdmin.pages[listingAdmin.currentPageID-1]); });
          // $('footer .controls button.button-next').on('click',function(){ 
          //   if ($('#tag-agree-terms').prop("checked"))
          //     listingAdmin.getPage(listingAdmin.pages[listingAdmin.currentPageID+1]); 
          //   else
          //   {
          //     listingAdmin.listing.agreedTagTerms = "0";
          //     listingAdmin.showTermsAndConditions();
          //   }
          // });
          // $('.click-to-edit').each(function(){ 
          //   if ( $(this).hasClass('about') ) listingAdmin.editAbout($(this));
          //   else listingAdmin.editTagWord($(this));
          // });
          // $('.add-tags .scrollbar').perfectScrollbar({suppressScrollX: true});
       
      }
    },
    saveBedsBaths: function(beds, baths) {
      $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "update-beds-baths",
                     data: {
                       id: listing_info.id,
                       newbeds: beds,
                       newbaths: baths } }, 
              dataType: 'json',
              type: 'POST',
              success: function(d){
                console.log(d.data);
              },
              error: function(d) {
                console.log('Failed to update beds and baths');
              }
      });
    },
    saveTags: function() {
      $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "update-tags",
                     data: {
                       id: listing_info.id,
                       tags: listing_info.tags} }, 
              dataType: 'json',
              type: 'POST',
              success: function(d){
                console.log(d.data);
                hListings = '';
                hCity = '';
                for(var i in listing_info.tags) {
                  if (listing_info.tags[i].type == 1)
                    hCity+= '<li><span>'+listing_info.tags[i].tag+'</span></li>';
                  else
                    hListings += '<li><span>'+listing_info.tags[i].tag+'</span></li>';
                }
                $('ul.showTagsListings').html(hListings);
                $('ul.showTagsCity').html(hCity);
              },
              error: function(d) {
                ahtb.alert('Database Tag Error'+'<pre>'+d.data+'</pre>',
                            {height: 120});
                console.log('Failed to update tags');
              }
      });
    },
    getTransferControlBody: function() {
      var h = '';
      if (ButtonOptions[listing_info.active].length) {    
          h += '<span>Transfer:</span>';     
          for(var j = 0; j < ButtonOptions[listing_info.active].length; j++) {
            var name = ActiveStateNames[ ButtonOptions[listing_info.active][j] ];
            if (j > 0)
              h += "/";
            h += "<a href=javascript:listings.transfer("+listing_info.id+","+ButtonOptions[listing_info.active][j]+")>"+name+"</a>";
          }
      }
      return h;
    },
    createTransferControl: function() {
      var h = '<div class="transferControl">';
      h += listings.getTransferControlBody();
      h += '</div>';
      $('.adminControl').append(h); 
    },
    createBedBathControl: function() {
      var h = '<div class="bedBathControl">'+
                '<label><span>Beds: </span></label><input type="text" id="beds" />'+
                '<label><span>Baths: </span></label><input type="text" id="baths" />'+
              '</div>';
      $('.adminControl').append(h); 

      $('.bedBathControl #beds').val(listing_info.beds);
      $('.bedBathControl #baths').val(listing_info.baths);

      $('.bedBathControl input').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    },
    createImageEditControl: function() {
      var h = '<div class="imageEditControl">' +
                '<span><h1>White Space Control</h1></span>'+
                '<div class="buttonContainer">'+
                  '<button class="trim">Trim</button>&nbsp;<button class="blackout">BlackOut</button>&nbsp;<button class="generate">Regen</button>'+
                  //'<div class="spin-wrap">'+
                    '<div class="spinner">'+
                    // '<div class="cube1"></div>'+
                    // '<div class="cube2"></div>'+
                  //'</div></div>'+
                  '</div>' +
                '</div>'+
              '</div>';
      $('.adminControl').append(h);
      $('.imageEditControl .spinner').fadeOut(1, function(){});

      $('.imageEditControl .trim').on('click', function() {
        console.log("Trim");
        $('.imageEditControl .spinner').fadeIn(250, function() {
          listings.imageEdit(ImageEditType.TRIM);
        })
      });

      $('.imageEditControl .blackout').on('click', function() {
         console.log("BlackOut");
       $('.imageEditControl .spinner').fadeIn(250, function() {
           listings.imageEdit(ImageEditType.BLACKOUT);         
        })
      });

      $('.imageEditControl .generate').on('click', function() {
         console.log("Regen");
       $('.imageEditControl .spinner').fadeIn(250, function() {
           listings.imageEdit(ImageEditType.GENERATE);         
        })
      });
    },
    imageEdit: function(mode) {
      var data = {
        id: listing_info.id,
        mode: mode
      }
      $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "image-edit",
                     data: data },
              dataType: 'json',
              type: 'POST',
              success: function(d){
                $('.imageEditControl .spinner').fadeOut(250, function(){});
                console.log(d.data);
                if (d.status == 'OK') {
                  ahtb.alert('Image Edit Success'+'<pre>'+d.data+'</pre>',
                            {height: 150});
                  if (mode == ImageEditType.GENERATE ||
                      d.data.indexOf("Updated images") != -1)
                    location.reload();
                  console.log(d.data);
                }
              },
              error: function(d) {
                $('.imageEditControl .spinner').fadeOut(250, function(){});
                ahtb.alert('Image Edit Error'+'<pre>'+d.data+'</pre>',
                            {height: 150});
                console.log('Failed to edit image');
              }
      });
    },
    createSellerAdminControl: function() {
      var h = '<div class="improvementBySeller">'+
                '<label><h1>Improvements by Seller</h1></label>'+
                '<table class="adminSellerTable">'+
                    '<thead>'+
                        '<tr>'+
                            '<th scope="col" class="manage-column column-task">Add</th>'+
                            '<th scope="col" class="manage-column column-action"></th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody> </tbody>'+
                '</table>'+
                '<button class="review">Review Others</button><span>  </span><button class="submit">Submit</button>'+
              '</div>';
      $('.adminControl').append(h);

      h = '';
      for(var a in SellerImprovements) {
        h += '<tr><td>'+SellerImprovements[a].visible+'</td><td><input type="checkbox" action="'+SellerImprovements[a].action+'" data-element="'+SellerImprovements[a].element+'"/>';
      }
      $('.adminSellerTable tbody').html(h);

      var countImprovementMeta = 0;
      if (seller != null &&
          seller.meta != null) {
        for(var i in seller.meta) {
          if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS) {
            countImprovementMeta++;
            if ( (!jQuery.isNumeric(seller.meta[i].listhub_key) ? seller.meta[i].listhub_key == listing_info.listhub_key : seller.meta[i].listhub_key == listing_info.id) ) {
              for(var j in seller.meta[i]) {
                if (j != 'listhub_key')
                  $('.improvementBySeller .adminSellerTable input[data-element="'+j+'"]').prop('checked', true);          
              }
            }
          }
        }
      }

      if (countImprovementMeta < 2)
       // $('.improvementBySeller button.review').prop("disabled", true);
        $('.improvementBySeller button.review').hide();

      $('.improvementBySeller .submit').on('click', function() {
        var meta = {};
        meta['listhub_key'] = listing_info.id;
        meta['action'] = MetaDataType.SELLER_IMPROVEMENT_TASKS;
        $('.improvementBySeller input[type="checkbox"').each(function() {
          if ($(this).prop('checked'))
            meta[$(this).attr('data-element')] = 1;
        });
        listings.saveSellerMeta(meta);
      });

      $('.improvementBySeller button.review').on('click', function() {
        listings.populateSellerReview();
     });
    },
    populateSellerReview: function() {
      var data = {
        id: listing_info.author,
        author_has_account: listing_info.author_has_account,
        current: listing_info.id
      }
      $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "get-seller-meta-listings",
                     data: data },
              dataType: 'json',
              type: 'POST',
              success: function(d){
                if (d.status != 'OK') {
                  console.log(d.data);
                  ahtb.alert(d.data, {height: 180});
                  return;
                }
                var h = '<table class="sellerMetaTable">'+
                    '<thead>'+
                        '<tr>'+
                            '<th scope="col" class="manage-column column-mls">Key</th>'+
                            '<th scope="col" class="manage-column column-listing">Listing</th>';
                for(var i in SellerImprovements) {
                  h += '<th scope="col" class="manage-column column-'+SellerImprovements[i].element+'">'+SellerImprovements[i].visible+'</th>';
                }

                    h +='</tr>'+
                    '</thead>'+
                    '<tbody>';
                var countImprovementMeta = 0;

                for(var i in seller.meta) {
                  if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS &&
                      listing_info.listhub_key != seller.meta[i].listhub_key) {
                    countImprovementMeta++;
                    h += '<tr><td>'+seller.meta[i].listhub_key+'</td>';
                    for(var j in d.data) {
                      if (d.data[j].listhub_key == seller.meta[i].listhub_key) {
                        h += '<td><a href="'+ah_local.wp+'/listing/1-'+d.data[j].id+'" target="_blank">'+d.data[j].street_address+","+d.data[j].city+" "+d.data[j].state+"</a></td>";
                        break;
                      }
                    }
                    for(var j in SellerImprovements) {
                      h += '<td>';
                      if ( (SellerImprovements[j].element in seller.meta[i]) )
                        h += '<span class="checkmark"><img src="'+ah_local.tp+'/_img/_sellers/check.png" /></span></td>';
                      else
                        h += '<span></span></td>';
                    }
                    h += '</tr>';
                  }
                }
                h +='</tbody>'+
                '</table>';
            
                ahtb.open({ hideSubmit: false, height: 150+(countImprovementMeta * 50), width: 1000, title: 'All Listings To Improve', html: h});
              },
              error: function(d) {
                ahtb.alert('Database Error'+'<pre>'+d.data+'</pre>',
                            {height: 120});
                console.log('Failed to get seller meta data listings');
              }
      });
    },
    saveSellerMeta: function(meta) {
      var saveIt = false;
      var foundIt = false;
      var allMeta = [];
      if (seller.meta != null) {
        for(var i in seller.meta) {
          if ( (seller.meta[i].listhub_key == listing_info.listhub_key ||
                seller.meta[i].listhub_key == listing_info.id) &&
              parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS) {
            foundIt = true;
            if ($.keyCount(seller.meta[i]) != $.keyCount(meta)) {
              saveIt = true;
            }
            if (!saveIt)
              for(var j in seller.meta[i]) {
                if (j != 'listhub_key' &&
                  !(j in meta)) {
                  saveIt = true;
                  break;
                }             
              }
            if (saveIt)
              allMeta[allMeta.length] = meta;
          } // had same listhub_key
          else {// didn't have same listhub_key, but pre-existing
            allMeta[allMeta.length] = seller.meta[i];
          }
        } // end for()
        if (!foundIt) {// so must be a new one for this seller
          saveIt = true;
          allMeta[allMeta.length] = meta;
        }
      }
      else { // first one for the seller
        saveIt = true;
        allMeta[allMeta.length] = meta;
      }

      var data = {
        id: listing_info.author,
        fields: { meta: allMeta },
        author_has_account: listing_info.author_has_account,
        why: EmailTypes.IMPROVE_LISTING
      }

      if (saveIt)
        $.ajax({
              url: ah_local.tp+"/_sellers/_ajax.php", 
              data: {query: "update-seller",
                     data: data },
              dataType: 'json',
              type: 'POST',
              success: function(d){
                ahtb.alert("Updated seller's meta tags successfully",
                  {height: 150});
                console.log(d.data);
                if (allMeta.length >= 2)
                  $('.improvementBySeller .review').show();
                  // $('.improvementBySeller .review').prop("disabled", false);
              },
              error: function(d) {
                ahtb.alert('Database Error'+'<pre>'+d.data+'</pre>',
                            {height: 120});
                console.log('Failed to update seller meta data');
              }
      });
    },
    transfer: function(listingID, dest){
      if (confirm("Are you sure you want to transfer listing: "+listing_info.id+" to the "+ActiveStateNames[dest]+" category?")) {
        $.post(ah_local.tp+"/_admin/ajax_viewer.php",
            {'query':'transfer-listing',
             'data':{id:listingID,
                 active: dest,
                 user: ah_local.author_id }},
            function(){},
            'json')
          .done(function(data){
            if (data.status == 'OK'){ 
              ahtb.alert("Listing has been moved to '"+ActiveStateNames[dest]+"' category. Please refresh the Listings Viewer page to see the change.",
                {height: 180,
                 width: 500});
              listing_info.active = dest;
              $('.transferControl').html( listings.getTransferControlBody() );
            }
            console.log(data.data);
          });
      } 
    },
    gallery: {
      counter: 0,
      wasInit: false,
      open: function(){ ahtb.open({
        hideTitle: true, buttons: [], close: function(){
          listings.gallery.wasInit = false;
          listings.gallery.images = [];
        },
				width: 1400,
				height: 700,
        html: '<div class="gallery-window">'+
          '<a class="gallery-close entypo-cancel"></a>'+
          '<section class="top">'+
            '<div class="gallery-image">'+
              '<div class="spin-wrap"><div class="spinner">'+
                '<div class="cube1"></div>'+
                '<div class="cube2"></div>'+
              '</div></div>'+
              '<div class="image"></div>'+
              // '<div class="controls">'+
              //   '<a class="prev"><span class="entypo-left-open-big"></a>'+
              //   '<a class="next"><span class="entypo-right-open-big"></a>'+
              // '</div>'+
            '</div>'+
            '<div class="gallery-meta">'+
              '<h3 class="image-title">Image Gallery</h3>'+
              '<span class="image-counter"></span>'+
              '<span class="image-description"></span>'+
            '</div>'+
          '</section>'+
          '<div class="gallery-divider"></div>'+
          '<div class="gallery-thumbnails-wrap">'+
            '<div class="scrollbar"><div class="handle"><div class="mousearea"></div></div></div>'+
            '<div class="frame" id="gallery-thumbnails"><ul class="clearfix"></ul></div>'+
          '</div>'+
          '<div class="gallery-nav">'+
            '<a class="prev"><span class="entypo-left-open-big"></span> Prev</a>'+
            '<a class="next">Next <span class="entypo-right-open-big"></span></a>'+
          '</div>'+
        '</div>',
        opened:function(){
          $('#TB_window').css({'border-color': '#777'});
          $('.gallery-close').on('click',function(){ ahtb.close() });
          for (var i in gallery_images) if (gallery_images[i])
            listings.gallery.printThumbnail(i, function(){
              listings.gallery.loadThumbnail(i);
              setTimeout(function(){
                var $frame  = $('#gallery-thumbnails');
                var $slidee = $frame.children('ul').eq(0);
                var $wrap   = $frame.parent();
                var $sly_options = {
                  horizontal: 1,
                  itemNav: 'forceCentered',
                  smart: 1,
                  activateOn: 'click',
                  mouseDragging: 1,
                  touchDragging: 1,
                  releaseSwing: 1,
                  startAt: 0,
                  scrollBar: $wrap.find('.scrollbar'),
                  scrollBy: 1,
                  speed: 300,
                  elasticBounds: 1,
                  easing: 'easeOutExpo',
                  dragHandle: 1,
                  dynamicHandle: 1,
                  clickBar: 0,
                  keyboardNavBy: 'items',
                  prev: $('.gallery-nav .prev'),
                  next: $('.gallery-nav .next'),
                }
                var $sly_events = {
                  load: function(){ if (!listings.gallery.wasInit) setTimeout(function(){listings.gallery.loadImage(0);},100); },
                  active: function(){ listings.gallery.loadImage(this.rel.activeItem); }
                }
                listings.gallery.sly = new Sly($frame, $sly_options, $sly_events).init();
                // $('.gallery-nav .next').on('click',function(){ $('.controls .next').click() });
                // $('.gallery-nav .prev').on('click',function(){ $('.controls .prev').click() });
              }, 50);
            });

        }
      })},
      startLoadingThumbImage: function(index, callback){
        if (typeof listings.gallery.images[index] == 'undefined' || typeof listings.gallery.images[index].thumb == 'undefined' || typeof listings.gallery.images[index].thumb.loading == 'undefined'){
          if (typeof listings.gallery.images[index] == 'undefined'){
            listings.gallery.images[index] = {};
            for (var i in gallery_images[index]) listings.gallery.images[index][i] = gallery_images[index][i];
            listings.gallery.images[index].hasThumb = listings.gallery.images[index].file.substr(0,7) != 'http://';
          }
          $img = listings.gallery.images[index];
          if (!$img.hasThumb) listings.gallery.startLoadingFullImage(index, callback ? callback : null);
          else {
            $image = new Image();
            $image.loaded = false;
            $image.loading = true;
            $image.onerror = function(){ listings.gallery.images[index].thumb.loading = false; console.error($img); }
            $image.onload = function(){
              listings.gallery.images[index].thumb.loading = false;
              listings.gallery.images[index].thumb.loaded = true;
              if (callback) callback();
            }
            $img.thumb = $image;
            listings.gallery.images[index] = $img;
            listings.gallery.images[index].thumb.src = (!$img.hasThumb ? $img.file : ah_local.tp + '/_img/_listings/210x120/' + $img.file);
          }
        }
        else if (listings.gallery.images[index].thumb.loaded && callback) callback();
      },
      startLoadingFullImage: function(index, callback){
        if (typeof listings.gallery.images[index] == 'undefined' || typeof listings.gallery.images[index].img == 'undefined' || typeof listings.gallery.images[index].img.loading == 'undefined'){
          if (typeof listings.gallery.images[index] == 'undefined'){
            listings.gallery.images[index] = {};
            for (var i in gallery_images[index]) listings.gallery.images[index][i] = gallery_images[index][i];
            listings.gallery.images[index].hasThumb = listings.gallery.images[index].file.substr(0,7) != 'http://';
          }
          $img = listings.gallery.images[index];
          $image = new Image();
          $image.loaded = false;
          $image.loading = true;
          $image.onerror = function(){ listings.gallery.images[index].img.loading = false; console.error($img); }
          $image.onload = function(){
            listings.gallery.images[index].img.loading = false;
            listings.gallery.images[index].img.loaded = true;
            if (callback) callback();
          }
          $img.img = $image;
          listings.gallery.images[index] = $img;
          listings.gallery.images[index].img.src = (!$img.hasThumb ? $img.file : ah_local.tp + '/_img/_listings/900x500/' + $img.file);
        }
        else if (listings.gallery.images[index].img.loaded && callback) callback();
        // else console.log(index)
      },
      loadImage: function(index){
        this.current = parseInt(index);
        if (this.images[index] && this.images[index].img && this.images[index].img.loaded) $('.gallery-image .image').fadeOut(125, function(){
          var $img = listings.gallery.images[index].img;
          $('.gallery-meta .image-counter').html('Image '+(index+1)+' / '+listings.gallery.images.length);
          var $desc = null;
          if (listings.gallery.images[index].desc){
            $desc = listings.gallery.images[index].desc;
          } else {
            $desc = '<table class="listing-details">'+
                (listing_info.street_address ? '<tr><td class="address" colspan="2">'+listing_info.street_address+'</td></tr>' : '')+
                (listing_info.city || listing_info.state ? '<tr><th>Location:</th><td>'+(listing_info.city ? listing_info.city+(listing_info.state ? ', '+listing_info.state : '') : '')+'</td></tr>' : '')+
                (listing_info.country && listing_info.country != 'US' ? '<tr><th>Country:</th><td>'+listing_info.country+'</td></tr>' : '')+
                (listing_info.price ? '<tr><th>Offered Price:</th><td>$'+listing_info.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</td></tr>' : '')+
                (listing_info.interior ? '<tr><th>Interior:</th><td>'+listing_info.interior.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' '+(listing_info.interior_std == 'meters' ? 'm<sup>2</sup>' : 'ft<sup>2</sup>')+'</td></tr>' : '')+
                (listing_info.lotsize ? '<tr><th>Lot Size:</th><td>'+listing_info.lotsize.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' '+(listing_info.lotsize_std == 'acres' ? 'acres' : (listing_info.lotsize_std == 'meters' ? 'm<sup>2</sup>' : 'ft<sup>2</sup>'))+'</td></tr>' : '')+
                (listing_info.beds ? '<tr><th>Beds:</th><td>'+listing_info.beds+'</td></tr>' : '')+
                (listing_info.baths ? '<tr><th>Baths:</th><td>'+parseInt(listing_info.baths.toString().split('.')[0])+' / '+parseInt(listing_info.baths.toString().split('.')[1])+'</td></tr>' : '')+
            '</table>';
          }
          $('.gallery-meta .image-description').html($desc);
          var width,height;
          var maxWidth = Math.round($('body').width()*0.9), maxHeight = 500, borderX = 350, borderY = 140;
          if ($img.width > $img.height){
            width = $img.width+borderX < maxWidth ? $img.width : maxWidth-borderX;
            height = ($img.height * width / $img.width);
          } else {
            height = $img.height+borderY < maxHeight ? $img.height : maxHeight-borderY;
            width = ($img.width * height / $img.height);
          }
          $('.gallery-image .image').html('<a href="javascript:listings.gallery.sly.next();" target="_blank"><img src="'+$img.src+'" height="'+height+'" width="'+width+'" /></a>');
          //ahtb.resize(width+borderX, height+borderY);
          $('.gallery-image, .gallery-image .image').css({ 'width': width, 'height': height });
          $('.gallery-image .image').fadeIn(250);
          $('.gallery-image .spinner').fadeOut(250, function(){
            if (!listings.gallery.wasInit) listings.gallery.wasInit = true;
          });
        }); else $('.gallery-image .spinner').fadeIn(250, function(){
          $('.gallery-image .image').fadeOut(250, function(){ $(this).empty() });
          listings.gallery.startLoadingFullImage(index, function(){setTimeout(function(){ listings.gallery.loadImage(index) }, 500)});
        });
      },
      loadThumbnail: function(index){
        if ( this.images[index] && (
          (this.images[index].hasThumb && typeof this.images[index].thumb != 'undefined' && this.images[index].thumb.loaded) ||
          (!this.images[index].hasThumb && typeof this.images[index].img != 'undefined' && this.images[index].img.loaded)
        )) {
          $('.gallery-thumb[index='+index+'] .image').fadeOut(125, function(){
            var $img = listings.gallery.images[index];
            $img = $img.hasThumb ? $img.thumb : $img.img;
            $(this).html('<img src="'+$img.src+'" />');
            $('.gallery-thumb[index='+index+'], .gallery-thumb[index='+index+'] .image').css({ 'width':Math.round($img.width * 90 / $img.height), 'height': 90 });
            $(this).fadeIn(250);
            $('.gallery-thumb[index='+index+'] .spinner').fadeOut(250, function(){
              listings.gallery.sly.reload();
            });
          });
        } else $('.gallery-thumb[index='+index+'] .spinner').fadeIn(250, function(){
          $('.gallery-thumb[index='+index+'] .image').fadeOut(250, function(){ $(this).empty() });
          listings.gallery.startLoadingThumbImage(index, function(){setTimeout(function(){ listings.gallery.loadThumbnail(index) }, 500)});
        });
      },
      printThumbnail: function(index, callback){
        $('#gallery-thumbnails ul').append('<li class="gallery-thumb" index="'+index+'">'+
          '<div class="spin-wrap"><div class="spinner">'+
            '<div class="cube1"></div>'+
            '<div class="cube2"></div>'+
          '</div></div>'+
          '<div class="image"></div>'+
        '</li>');
        if (callback) callback();
      },
    },
    gotoExploreMap: function(mode) {
        // if ( typeof mode != 'undefined' &&
        //      listings.map.maps[listings.primary] != null ) {
        //     console.log("gotoExploreMap from php");
        //     return;
        // }

        if ( listings.preppingPOI ) {
          listings.waitingToGoToExploreArea = true;
          ahtb.open({html: '<div>Please wait a moment, gathering points of interest..</div>',
                     width: 450,
                     //height: 150,
                     hideClose: true,
                     closeOnClickBG: false,
                     hideSubmit: true});
          return;
        }

        // var page = thisPage.indexOf('-new') ? 'explore-the-area-new' : 'explore-the-area';
        var page = 'explore-the-area-new';
        window.location = ah_local.wp+'/'+page+'/'+listing_info.id;
    },
    prepExploreArea: function() {
      listings.preppingPOI = true;
      $.ajax({
              url: ah_local.tp+"/_pages/ajax-explore.php", 
              data: {query: "get-places",
                     data: {
                       listing: listing_info} 
                    }, 
              dataType: 'json',
              type: 'POST',
              success: function(d){
                console.log("POI - total:"+d.data.total+", added: "+d.data.added+", took "+d.data.time+" secs");
                listings.preppingPOI = false;
                if ( listings.waitingToGoToExploreArea ) {
                  // var page = thisPage.indexOf('-new') ? 'explore-the-area-new' : 'explore-the-area';
                  var page = 'explore-the-area-new';
                  window.location = ah_local.wp+'/'+page+'/'+listing_info.id;
                }

              },
              error: function(d) {
                console.log('Failed to add any POI to db');
                listings.preppingPOI = false;
                if ( listings.waitingToGoToExploreArea ) {
                  // var page = thisPage.indexOf('-new') ? 'explore-the-area-new' : 'explore-the-area';
                  var page = 'explore-the-area-new';
                  window.location = ah_local.wp+'/'+page+'/'+listing_info.id;
                }
              }
      });
    },
  }
  listings.init();
});