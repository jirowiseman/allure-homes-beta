jQuery(document).ready(function($){

	function waitForController() {
		if (!controller) {
			window.setTimeout(waitForController, 250);
			return;
		}
		initController();

		controller.prepPortalUserCapture();
	}

	$('div#portal-user-capture .closeDiv').on('click', function() {
    	$('div#portal-user-capture').hide();
        $('#quiz-results .results-scroll').css('overflow-y', 'scroll');

        var email = $('div#portalAgentCapture input.email').val();
        var name = fbLogInData.first_name.length ? fbLogInData.first_name +' '+ fbLogInData.last_name : '';
		if (name.length == 0)
			name = $('div#portalAgentCapture input.name').val(); // see if it's in here then..

        la(AnalyticsType.ANALYTICS_TYPE_EVENT,
		   'closed-capture-popup',
		   ah_local.agentID,
		   "event:"+controller.captureEvent+", email:"+(email.length ? email : 'N/A')+", name:"+(name.length ? name : 'N/A')+', fbId:'+(fbLogInData.id.length ? fbLogInData.id : 'N/A')
		);

		if (controller.callback)
			controller.callback();
		controller.callback = null;
    })


	function initController() {
		if (phoneMode) {
				$('.input#phone').show();
				$('input.phone').mask('(999) 000-0000');
		}
		
		$('div#portalAgentCapture input').on('keypress', function() {
	    	var marker = $(this).siblings('#marker');
	    	if (marker.length)
	    		marker.addClass('hidden');
	    })
		$('input.email').keypress(function(e){
			if(e.keyCode==13) {
              if ($('div#portalAgentCapture a.next').css('display') == 'block'){
				controller.nextStepPortalUserCapture();
              }
            }
		})
		if (isRetina){
			$('div#portal-user-capture div#portalAgentCapture #userdata #fbDiv a.fb-fakebutton img').replaceWith( '<img src="'+ah_local.tp+"/_img/social_media/facebook-icon-retina.png"+'" />' );
		}

		controller.callback = null;

		$('button.openCapture').on('click', function() {
			var where = controller.captureEvent.replace('LeadIn', '');
			var page = typeof controller.page != 'undefined' ? controller.page : -2;
			$('div#portalAgentCaptureLeadIn').fadeOut(500, function() {
				if (typeof captureOptions[where] == 'undefined') {
					captureOptions[where] = {done: 0,
											 captureMode : 1};
				}
				captureOptions[where].done = 0;
				captureOptions[where].captureMode = captureOptions[controller.captureEvent].captureMode == 2 ? 2 : (captureOptions[where].captureMode ? captureOptions[where].captureMode : 1);
				controller.showPortalUserCapture(where, page, true, controller.callback);
			});
		})
		
		controller.loginSimple = function() {
	      var reg_html_login =
	        '<div><form id="sacrifice"></form>' +
	            '<span id="extra-message2"></span>' +
	             login_form + 
	            '<p class="forgot-password"><a href="'+lostPasswordUrl+'" title="Forgot your password?">Forgot your password?</a></p>' +
	               // '</form>'+
	        '</form></div>';
	        $('div#portal-user-capture').css('z-index', 100000);
	        ahtb.open({ html: reg_html_login,
	                    width: 450,
	                    height: 240,
	                    hideSubmit: true,
	                	closed: function() {
	    			   		$('div#portal-user-capture').css('z-index', 1000000);
	    			   }})
          /*$('.global-signin-popup').show();
          tempSignInScrollTop = $(window).scrollTop();
          $('body').css('overflow','hidden');
                  if (isMobile)
                      $('body').css('position','fixed');
          $('.global-signin-popup .signin-content .close').on('click', function() {
            closeLoginForm();
          });*/
	    }
        /*function closeLoginForm() {
          $('.global-signin-popup').hide();
          $('body').css('overflow','');
          if (isMobile)
            $('body').css('position','relative');
          $(window).scrollTop(tempSignInScrollTop);
        }
        $(document).keyup(function(e) {
          if (e.keyCode == 27) { // escape key
            if($('.global-signin-popup').css('display') == 'block'){
              closeLoginForm();
            }
          }
        });*/

	    controller.showPrivacyPolicy = function() {
	    	window.open(ah_local.wp+'/terms');
	    	// $('div#portal-user-capture').css('z-index', 100000);
	    	// ahtb.open({html:'<p>Only the portal agent will have access to your information, and we will not be sharing this information with any other entitiy.  For full disclosure, please read the Privacy Policy section of our <a href="'+ah_local.wp+'/terms'+'">Terms and Conditions</a></p>',
	    	// 		   width: 450,
	    	// 		   height: 230,
	    	// 		   closed: function() {
	    	// 		   	$('div#portal-user-capture').css('z-index', 1000000);
	    	// 		   }});
	    }

		controller.nextStepPortalUserCapture = function() {
			if ( !$('div#portalAgentCapture').is(':visible') )
				return;

			prepOverlaySpinner();

			var email = $('div#portalAgentCapture input.email').val();
			if (email.length == 0 ||
				!validateEmail(email)) {
                $('div#portalAgentCapture div#email #marker').removeClass('hidden');
				$('div#portal-user-capture').css('z-index', 100000);
				ahtb.open({html:'<p>Please enter a valid email</p>',
						   width: 400,
						   height: 150, 
						   closed: function() {
		    			   	$('div#portal-user-capture').css('z-index', 1000000);
		    			   }});
				return;
			}

			// anchors don't like disabled attribute, so use class if it is, else if button, would work also 
			$('a.next').addClass('disabled');
			$('a.next').prop('disabled', true);

			var name = fbLogInData.first_name.length ? fbLogInData.first_name +' '+ fbLogInData.last_name : '';
			if (name.length == 0)
				name = $('div#portalAgentCapture input.name').val(); // see if it's in here then..

        	if (name.length ) 
				$('div#portalAgentCapture div#name #marker').addClass('hidden');
			else {
				$('div#portalAgentCapture div#name #marker').removeClass('hidden');
				$('div#portalAgentCapture div#name #marker').show();
			}
			

			if ( phoneMode <= PortalLandingElementMode.PORTAL_LANDING_OPTIONAL ) 
				$('div#portalAgentCapture div#phone #marker').addClass('hidden');
			else
				$('div#portalAgentCapture div#phone #marker').removeClass('hidden');
			
			if (name.length ) {
                $('div#portalAgentCapture .input#name #marker').show();
				$('div#portalAgentCapture .input#name #marker').removeClass('entypo-attention');
				$('div#portalAgentCapture .input#name #marker').addClass('entypo-check');
			}

			controller.DB({
				query: 'email-only-captured',
				url: ah_local.tp+'/_pages/ajax-register.php',
				data: {email: email,
					   name: name,
					   fbId: fbLogInData.id,
					   photo:fbLogInData.photo,
					   agentID: portalAgent.author_id,
					   sessionID: ah_local.activeSessionID,
					   quizID: quiz_id,
					   page: thisPage,
                       campaign: ah_local.agentCampaign,
					   textMessageOnEmailCapture: textMessageOnEmailCapture,
					   event: controller.captureEvent},
				error: function(d){$('div#overlay-bg').hide();},
				done: function(d){
					if (typeof d.reloadPage != 'undefined') {
						console.log("Reloading page after email capture");
						$('div#portal-user-capture').fadeOut(250, function() {
							ahtb.open({html:"<p>Logging you in as "+d.login+"  Please wait a moment.</p>",
									   width: 450,
									   height: 180,
									   opened: function() {
									   		if (!controller.callback)
									   			window.location.reload();
									   		else {
									   			controller.callback();
									   			setCookie('ReloadPage', 1, 1);
									   		}
									   		controller.callback = null;
									   }});
						});
						return;
					}
					else
						$('div#overlay-bg').hide();
				}
			});
          
            $('div#portalAgentCapture .goBack').show();

			if (fbLogInData.id.length)
				$('div#portalAgentCapture #fbDiv').fadeOut(500);

			$('div#portalAgentCapture #pitch .first-pitch-wrapper').animate({opacity: '0'},
															 {queue: false,
															  duration: 500,
															  done: function() {
															  	$('div#portalAgentCapture #pitch .first-pitch-wrapper').hide();
															  	$('div#portalAgentCapture #pitch .second-pitch-wrapper').show().animate({opacity: '1'},
															  													  {queue: false,
															  													   duration: 500});
															  }});
          
            $('div#portalAgentCapture .next').fadeOut(500, function() {
				$('div#portalAgentCapture .start').fadeIn(500);
			});
          
            $('div#portalAgentCapture .input#email').fadeOut(500, function() {
				$('div#portalAgentCapture .input#name').fadeIn(500);
                if (phoneMode) {
                  $('div#portalAgentCapture .input#phone').fadeIn(500);
                }
			});

			/*if (phoneMode) {
				var addForFB = fbMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED && !fbLogInData.id.length ? ADDED_HEIGHT_FOR_FB : 0;
				var baseHeight = 625 + addForFB; 
				$('div#portalAgentCapture').css('height',baseHeight+'px');
				$('div#portalAgentCapture .input#phone').fadeIn(500);
			}*/
		}

		controller.prepPortalUserCapture = function() {
            $('div#portalAgentCapture #pitch .first-pitch-wrapper').show().css('opacity', '1');
			$('div#portalAgentCapture #pitch .second-pitch-wrapper').hide().css('opacity', '0');
			$('div#portalAgentCapture .goBack').hide();

			if (fbLogInData.id.length)
				$('div#portalAgentCapture #fbDiv').hide();

			$('a.next').prop('disabled', false);
			$('a.next').removeClass('disabled');

			// if email is not used or optional, then force the capture popup to be a self-contained capture, instead of being split into two
			if (emailMode <= PortalLandingElementMode.PORTAL_LANDING_OPTIONAL)
				queryMode = PortalLandingElementMode.PORTAL_LANDING_NOT_USED;

			/*var heightPortalCaptureDiv = 680; //parseInt($('div#portalAgentCapture').css('height'));
		    var addForFB = fbMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED && fbLogInData.id.length == 0 ? ADDED_HEIGHT_FOR_FB : 0;*/
		    if (queryMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED) // then doing phased login
			    switch (emailMode) {
			    	case PortalLandingElementMode.PORTAL_LANDING_NOT_USED:
			    	case PortalLandingElementMode.PORTAL_LANDING_OPTIONAL:
			    		// $('div#portal-user-capture .next').hide();
			    		// break;
			    		console.log("emailMode should be at least PORTAL_LANDING_MUST_HAVE, when queryMode is active (not PORTAL_LANDING_NOT_USED)");
			    		emailMode = PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE;

			    	default:
			    		//var baseHeight = 565 + addForFB;
			    		//$('div#portalAgentCapture').css('height',baseHeight+'px');
			    		$('div#portalAgentCapture .next').show();
			    		$('div#portalAgentCapture .start').hide();
			    		$('.input#email').show();
			    		$('.input#name').hide();
			    		$('.input#phone').hide();
			    		break;
			    }
			else {
				/*if (addForFB) {
					$('div#portalAgentCapture').css('height',(heightPortalCaptureDiv+addForFB)+'px');
				}*/
				$('div#portalAgentCapture .next').hide();
				if (emailMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL)
					$('div#portalAgentCapture div#email #marker').addClass('hidden');
			}

			if (fbMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED) {
				nameMode = PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE;
				if (fbLogInData.id.length == 0)
					$('div#portalAgentCapture #fbDiv').show();
			}
          
            if (markerMode == PortalLandingElementMode.PORTAL_LANDING_NOT_USED ||
				((markerMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL &&
				 !fbMode) || fbMode) ) {
				$('div#portalAgentCapture div#email #marker').addClass('hidden');
				$('div#portalAgentCapture div#name #marker').addClass('hidden');
				$('div#portalAgentCapture div#phone #marker').addClass('hidden');
			}
			else { // then it's PORTAL_LANDING_MUST_HAVE or in fbMode with PORTAL_LANDING_OPTIONAL
				if (emailMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL)
					$('div#portalAgentCapture div#email #marker').addClass('hidden');
				else
					$('div#portalAgentCapture div#email #marker').removeClass('hidden');

				if (nameMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL)
					$('div#portalAgentCapture div#name #marker').addClass('hidden'); 
				else
					$('div#portalAgentCapture div#name #marker').removeClass('hidden'); 

				if (phoneMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL)
					$('div#portalAgentCapture div#phone #marker').addClass('hidden');
				else
					$('div#portalAgentCapture div#phone #marker').removeClass('hidden');
			}
		}

		controller.showPortalUserCapture = function(where, currentPage, fromLeadIn, callback) {
			// first check to see if the portal agent is the same the user that's signed in
			if (ah_local.agentID == ah_local.author_id)
				return false;
			
			var whereLeadIn = where+"LeadIn";
			fromLeadIn = typeof fromLeadIn == 'undefined' ? false : fromLeadIn;
			if (typeof captureOptions == 'undefined' ||
				(typeof captureOptions[where] == 'undefined' &&
				 typeof captureOptions[whereLeadIn] == 'undefined'))
				return false;

			if ( $('div#portalAgentCapture').is(':visible') )
				return where == 'viewListing' ? true : false;

			if ( $('div#portalAgentCaptureLeadIn').is(':visible') )
				return where == 'viewListing' ? true : false;

			if ( (typeof captureOptions[where].done != 'undefined' &&
				  captureOptions[where].done) ||
				 (typeof captureOptions[whereLeadIn] != 'undefined' &&
				  (typeof captureOptions[whereLeadIn].done != 'undefined' &&
				   captureOptions[whereLeadIn].done)) )
				return false;

			controller.callback = typeof callback == 'function' ? callback : null;

			// clean slate..
			$('div#portalAgentCapture').hide();
			$('div#portalAgentCaptureLeadIn').hide();

			if (where == 'quizLoad' ||
				where == 'viewListing') {
				var cookieName = where == 'quizLoad' ? 'QuizLoadCount' : 'ViewListingCount';
				var cValue = getCookie(cookieName);
				if (cValue.length) 
					cValue = parseInt(cValue);
				else
					cValue = 0;
				cValue++;
				setCookie(cookieName, cValue, 1); // update with incremented value

				if (typeof captureOptions.byPassCount != 'undefined' &&
					typeof captureOptions.byPassCount[where] != 'undefined') {
					if (captureOptions.byPassCount[where] > (cValue-1)) {
						return false;
					}
				}
			}

			if (where == 'quizLoad') {
				if (controller.query.tags.length == 0 &&
					typeof captureOptions.forceHardStopOnViewAllListingAtQuizLoad != 'undefined' &&
					captureOptions.forceHardStopOnViewAllListingAtQuizLoad)
				captureOptions[where].captureMode = PortalUserCaptureMode.CAPTURE_MUST;
				captureOptions[where].delay = captureOptions[where].mobileDelay = 2000;
			}

			var doingLeadIn = typeof captureOptions[where] == 'undefined' || !captureOptions[where].captureMode;
			if (doingLeadIn &&
				(typeof captureOptions[whereLeadIn] == 'undefined' ||
				 !captureOptions[whereLeadIn].captureMode) )
				return false;

			where = controller.captureEvent = doingLeadIn ? whereLeadIn : where;

			var phoneModeOrig = phoneMode;
			if (forcePhoneAsPasswordIfUsed) {
				if (phoneMode < PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD &&
				    emailMode < PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD) {
					if (phoneMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED)
						phoneMode = PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD;
					else if (emailMode != PortalLandingElementMode.PORTAL_LANDING_NOT_USED)
						emailMode = PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD;
				}

				if (phoneModeOrig != phoneMode) {
					var placeHolder = 'Enter Phone as Password';
					$('input.phone').attr('placeholder', placeHolder);
				}
			}

			controller.page = typeof currentPage == 'undefined' ? -2 : currentPage;
			var targetPage = typeof captureOptions[where] == 'undefined' || typeof captureOptions[where].page == 'undefined' ? -1 : captureOptions[where].page;
			var portalUserID = parseInt(ah_local.portalUser);
			var userID = parseInt(ah_local.author_id); // this is from wpUser->ID
			if (havePortalOwner &&
				typeof captureOptions[where] != 'undefined' &&
				captureOptions[where].captureMode &&
				(portalUserID == 0 ||
				 (userID == 0 &&
				  (phoneMode == PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD ||
				   emailMode == PortalLandingElementMode.PORTAL_LANDING_USE_AS_PASSWORD))) &&
				(targetPage == -1 || // no page defined
				 targetPage == controller.page) ) { // else right page
				var delay = fromLeadIn ? 0 : (isMobile ? (typeof captureOptions[where].mobileDelay != 'undefined' ? captureOptions[where].mobileDelay : (where == 'quizLoad' || where == 'quizLoadLeadIn' ? 3000 : 0)) : (typeof captureOptions[where].delay != 'undefined' ? captureOptions[where].delay : (where == 'quizLoad' || where == 'quizLoadLeadIn' ? 2000 : 0)));

				if (captureOptions[where].captureMode > PortalUserCaptureMode.CAPTURE_OPTIONAL) 
					$('div#portal-user-capture .closeDiv').hide();
				else
					$('div#portal-user-capture .closeDiv').show();

				this.prepPortalUserCapture();

				if (where == 'cityScroll' ||
					where == 'listingScroll' ||
					where == 'cityScrollLeadIn' ||
					where == 'listingScrollLeadIn') {
					captureOptions[where].done = 1;
				}
				// see if any data was captured already from ahfeedback, when the user hit 'Contact Me', and the data 
				// saved in fbLoginData
				var userName = getCookie('Landing-UserName');
				var userEmail = getCookie('Landing-UserEmail');
				if (userName.length &&
					userEmail.length) {
					portal_user_register.userName = userName;
					portal_user_register.userEmail = userEmail;
					$('div#portalAgentCapture input.name').val(portal_user_register.userName);
					$('div#portalAgentCapture input.email').val(portal_user_register.userEmail);
				}

				window.setTimeout( function() {
					if (!doingLeadIn)
						$('div#portalAgentCapture').show();
					else {
						$('div#portalAgentCaptureLeadIn #title').html(captureOptions[whereLeadIn].title);
						var message = captureOptions[whereLeadIn].message.replace(/%agent%/g, agent.first_name);
						$('div#portalAgentCaptureLeadIn #message').html(message);
						$('div#portalAgentCaptureLeadIn').show();
					}
					$('div#portal-user-capture').fadeIn(500);
                    var whichPopup = !doingLeadIn ? 'portalAgentCapture' : 'portalAgentCaptureLeadIn';
                    var captureDivHeight = $('div#portal-user-capture div#'+whichPopup).innerHeight();
                    var captureDivHeightHalf = captureDivHeight / 2;
                    $('div#portal-user-capture div#'+whichPopup).css('margin-top', '-'+captureDivHeightHalf+'px');
                    $('#quiz-results .results-scroll').css('overflow-y', 'hidden');
                    $('body').css('overflow', 'hidden');
                    if (isMobile)
                      $('body').css('position','fixed');
				}, delay);  
				return true;
			}
			return false;
		}

		controller.actionFBInitialized = function() {
		}

		controller.actionFBLoggedIn = function() {
			portal_user_register.userName = fbLogInData.first_name +' '+ fbLogInData.last_name;
			portal_user_register.userEmail = fbLogInData.email;
			portal_user_register.userPhoto = fbLogInData.photo.length ? fbLogInData.photo : '';
			portal_user_register.fbId = fbLogInData.id;
			$('div#portalAgentCapture input.name').val(portal_user_register.userName);
			$('div#portalAgentCapture input.email').val(portal_user_register.userEmail);
			controller.nextStepPortalUserCapture();
		}

		controller.actionPortalUserRegistered = function(d) {
			if (typeof d == 'undefined') {
				console.log("actionPortalUserRegistered got undefined data");
				return;
			}
			// this portion really not needed, but used for debuggin purpose
			ah_local.portalUser = d.data.id;
			ah_local.portalUserFirstName = d.data.first_name;
			ah_local.portalUserLastName = d.data.last_name;
			ah_local.portalUserEmail = d.data.email;
			ah_local.activeSessionID = typeof d.data.session != 'undefined' ? d.data.session : ah_local.activeSessionID;
			ah_local.portal = typeof d.data.portal != 'undefined' ? d.data.portal : ah_local.portal;
			ah_local.sessionID = typeof d.data.session_id != 'undefined' ? d.data.session_id : ah_local.sessionID;
			$('div#portal-user-capture').fadeOut(500);
            $('body').css('overflow', '');
            if (isMobile)
                $('body').css('position','relative');
            $('#quiz-results .results-scroll').css('overflow-y', 'scroll');
			

			if (typeof d.data.requireLogin != 'undefined' &&
				d.data.requireLogin) {
				$('div#overlay-bg').hide();
				controller.login(redirect, ah_local.wp+'/quiz-results/-1', 'Please login using your existing credentials');
			}
			else {
				if (typeof captureOptions.informUserNotFullyRegisteredUserAfterSignUp != 'undefined' &&
					captureOptions.informUserNotFullyRegisteredUserAfterSignUp) {
					var phone = $('div#portalAgentCapture input.phone').val();
					if ((typeof d.data.wp_user_id == 'undefined' ||
						 d.data.wp_user_id == 0) &&
						phone.length == 0 &&
						phoneMode > PortalLandingElementMode.PORTAL_LANDING_NOT_USED) {
						$('div#overlay-bg').hide();
						var reloading = false;
						ahtb.open({html:'<p>You are now '+agent.first_name+"'s portal user, but not a fully registered user.  Please register to be able save your searches.",
								   width: 500,
								   height: 180,
								   buttons: [{text:'OK', action:function() {
								   		controller.login(redirect, ah_local.wp+'/quiz-results/-1');
								   }},
								   {text:'Later', action: function() {
								   		reloading = true;
								   		$('div#overlay-bg').fadeIn(50);
										location.reload();
								   }}],
								   closed: function() {
								   		if (!reloading) {
										   	$('div#overlay-bg').fadeIn(50);
										   	location.reload();
										}
								   }});
					}
					else {
						location.reload();
					}
				}
				else
					location.reload();
			}
		}
	}

	waitForController();
});