jQuery(document).ready(function($){
	agent = new function() {
		this.activity = [1, 1, 1];

		this.init = function() {
			if (validSeller == 0) {
				ahtb.open({
				    title: 'Access Restriction',
				    height: 185,
				    width: 550,
				    html: '<p>There is no agent named '+nickname+'.</p>',
				    	buttons:[
							{text:"OK", action:function(){ 
								window.location = ah_local.wp;
							}},
						],
				    closed: function(){
				      window.location = ah_local.wp;
				      // ahtb.showClose(250);
				      // ahtb.closeOnClickBG(1);
				    }
				});
			}

			$('.contact-button').on('click', function() {
		        var agentId = validSeller.id;
		        console.log("Contact button for ", agentId);
		        var mode = !isPortal ? FeedbackMode.MESSAGE_EMAIL_AGENT : FeedbackMode.MESSAGE_EMAIL_AGENT_VIA_PORTAL;
		        ahfeedback.openModal(mode, agentId, listingId, listhub_key, isPortal ? portalAgent.id : 0, userID);
		    })

		    
		   // agent.activity[2] = parseInt($('#lifestyles table td:nth-child(1) a').attr('for'));
		   	if (length(specialtyTags) == 0 &&
		   		length(lifestyleTags) == 0) {
		   		var ele = $('.agent-nav table td:nth-child(2) a');
		   		// $('.agent-nav table#options td:nth-child(2) a').prop('disabled', true);
		   		// ele.prop('disabled', true);
		   		ele.hide();
		   	}

		    $('.agent-content div#agentMatch').hide();
		    $('.agent-nav table td:nth-child(1) a').addClass('active');

		    // local guide disabling
		    // $('.agent-nav td:nth-child(3) a').prop('disabled', true);
		    $('.agent-nav table td:nth-child(3) a').hide();

		    var val = parseInt($('#specialties table td:nth-child(1) a').attr('for'));
		    if (length(specialtyTags))
		    	$('#specialties #about').html( specialtyTags[val].desc );
		    else
		    	$('#specialties #about').html( "This agent will be updating this information shortly.");
		    val = parseInt($('#lifestyles table td:nth-child(1) a').attr('for'));
		    if (length(lifestyleTags)) 
		    	$('#lifestyles #about').html( lifestyleTags[val].desc );
		    else
		    	$('#lifestyles #about').html( "This agent will be updating this information shortly.");

		    $('#specialties table td:nth-child(1) a').addClass('active');
		    $('#lifestyles table td:nth-child(1) a').addClass('active');
		    

		    $('.agent-nav table a').on('click', function() {
		    	var index = parseInt($(this).attr('for'));
		    	if (index == 3)
		    		return;
		    	if (index != agent.activity[0]) {
		    		$('.agent-nav table td:nth-child('+agent.activity[0]+') a').removeClass('active');
		    		switch(agent.activity[0]) {
		    			case 1: 
		    				$('.agent-content div#overview').hide();
		    				$('.agent-content div#agentMatch').show();
		    				break;
		    			case 2: 
		    				$('.agent-content div#overview').show();
		    				$('.agent-content div#agentMatch').hide();
		    				break;
		    		}
		    	}
		    	$(this).addClass('active');
		    	agent.activity[0] = index;

		    })
		    $('#specialties table a').on('click', function() {
		    	var val = parseInt($(this).attr('for'));
		    	var index = parseInt($(this).attr('index'));
		    	if (!index)
		    		return;
		    	if (index != agent.activity[1])
		    		$('#specialties table td:nth-child('+agent.activity[1]+') a').removeClass('active');
		    	$(this).addClass('active');
		    	agent.activity[1] = index;
		    	var desc = specialtyTags[val].desc;
		    	$('#specialties #about').html( desc );

		    })
		    $('#lifestyles table a').on('click', function() {
		    	var val = parseInt($(this).attr('for'));
		    	var index = parseInt($(this).attr('index'));
		    	if (!index)
		    		return;
		    	if (index != agent.activity[2])
		    		$('#lifestyles table td:nth-child('+agent.activity[2]+') a').removeClass('active');
		    	$(this).addClass('active');
		    	agent.activity[2] = index;
		    	var desc = lifestyleTags[val].desc;
		    	$('#lifestyles #about').html( desc );
		    })

		    if (specialtyMatchChild != -1)
		    	$('#specialties table a[index='+specialtyMatchChild+']').click();
		    if (lifestyleMatchChild != -1)
		    	$('#lifestyles table a[index='+lifestyleMatchChild+']').click();

		    if (typeof validSeller.about == 'undefined' ||
		    	validSeller.about == null)
		    	$('.agent-nav table a[for="'+2+'"]').click();
		}
	}

	agent.init();
})