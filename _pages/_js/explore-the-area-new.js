var explore;

var LISTING_GATHERING_POI = 4;

jQuery(document).ready(function($){
	explore = new function(){
		this.init = function(){
			this.visibleCategories = {};
			this.latlng = new google.maps.LatLng(explore_info.lat, explore_info.lng);
			this.latlngOffset = new google.maps.LatLng(explore_info.lat, explore_info.lng+.12);
			this.pagetoken = '';
			this.keyword = '';
			this.hideHint = true;

			if ( explore_info.flags & LISTING_GATHERING_POI ) {
				ahtb.alert("Please wait a moment while we are gathering data..");
				explore.checkForPlaces();
			}
			else
				explore.readyInit();
			
		}

		this.login = function(callback, arg) {
	      callback = typeof callback == 'undefined' ? redirect : callback;
	      arg = typeof arg == 'undefined' ? window.location.href : arg;
	      ahreg.setCallback(callback, arg);
	      ahreg.extraMessage = '';
	      if (typeof arg != 'undefined' &&
	        arg.indexOf('http') != -1)
	        ahreg.setRedirect(arg);
	      ahreg.openModal();
	    }

		this.checkForPlaces = function() {
			$.ajax({
	              	url: ah_local.tp+"/_pages/ajax-explore.php", 
	              	data: {	query: "get-found-places",
	                     	data: { explore_info: explore_info }
	                      },
	                dataType: 'json',
	              	type: 'POST',
	              	success: function(d){
	              		if (d.status == 'OK') {
	              			if (d.data.poi) {
	              				ahtb.close();
	              				explore_info.places = d.data.poi;
	              				explore.readyInit();
	              			}
	              			else {
	              				console.log("apparently not ready yet");
	              				explore.checkForPlaces();
	              			}
	              		}
	              	},
	              	error: function(d) {
	              		ahtb.alert('Failed to get any previous POIs');
	                	console.log('Failed to get any previous POIs');
	              	}
	     		 });   
		}

		this.readyInit = function() {
			// $('ul#places-list > li').hide();
			var minDistance = 9999;
			for(var i in explore_info.places) {
				for(var j in explore_info.places[i]) {
					if (i == 'Arts')
						continue;
					if (explore_info.places[i][j].distance < minDistance)
						minDistance = explore_info.places[i][j].distance;
				}
			}

			var zoomIt = 15;
			if (minDistance > 5.0)
				zoomIt = 12;
			else if (minDistance > 2.5)
				zoomIt = 13;
			else if (minDistance > 1.0)
				zoomIt = 14;

			this.map = new google.maps.Map(document.getElementById('map-canvas'), {
				center: new google.maps.LatLng(explore_info.lat, explore_info.lng),
				zoom: zoomIt,
				scrollwheel: false,
				mapTypeControl: true,
			    mapTypeControlOptions: {
			        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			        position: google.maps.ControlPosition.BOTTOM_CENTER
			    },
			});
			this.homeMarker = new google.maps.Marker({position: explore.latlng, map: explore.map, icon: ah_local.tp+'/_img/page-explore-the-area/map_icons/home.png'});

			 var listener = google.maps.event.addListener(explore.map, "tilesloaded", function() {
                                google.maps.event.removeListener(listener);
				console.log("tilesloaded");
				ahtb.alert("Loading "+explore_info.poi_count+" markers...");
				 window.setTimeout(function() {
					explore.parse.markers(explore_info.places, explore.parse.list);
					ahtb.close();
				},500);
			});
			/* window.setTimeout(function() {
				explore.parse.markers(explore_info.places, explore.parse.list);
			},2000);
			*/
			var ele = $('ul#places-menu li');
			$('ul#places-menu li').on('click',function(){ 
				if ( explore.hideHint ) {
					$('div#map-wrap #hint').hide();
					explore.hideHint = false;
				}
				explore.toggleCategory($(this).attr('category'))
			}).hover(function() {
				var category = $(this).attr('category');
				if ( explore.visibleCategories[category] ) {
					var display = $('ul#places-list .'+category).css('display');
					console.log("ul display for "+category+" is "+display);
					if (display != 'none') {
						return;
					}

					$('ul#places-list > li').each(function() {
						$(this).fadeOut('fast');
						explore.visibleCategories[$(this).attr('category')] = false;
					});
					explore.visibleCategories[category] = true;
					$('ul#places-list .'+category).fadeIn(250);
					for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
						this.markers[i][j].marker.setAnimation(google.maps.Animation.DROP);
						this.markers[i][j].marker.setMap(explore.map);
						this.markers[i][j].marker.setVisible(true);
					}
				}
			}, function() {
				console.log("hover out of "+$(this).attr('category'));
			});

			// $('ul#places-menu li.searchby').off();
			$('ul#places-menu li.searchby a').on('click', function() {
				var text = $(this).siblings('input').val();
				console.log("Got text:"+text);
				explore.keyword = text;
				explore.userSearchInBatches.init(text);
			})

			$('ul#places-menu li.searchby input').on('keypress', function(e){
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			     	var text = $(this).val();
					console.log("Got text:"+text);
					explore.keyword = text;
					explore.userSearchInBatches.init(text);
			    }
			})

			$('ul#places-list .searchby #more').on('click', function() {
				explore.userSearchInBatches.init(explore.keyword, explore.pagetoken);
			})

			// this.toggleCategory('searchby');
			$('header section.banner li').on('click',function(){
				if ( $(this).hasClass('intro') ){
					$('.content-wrap > section').each(function(){
						if ($(this).hasClass('intro')) $(this).animate({left: '0%'},{duration: 500, queue: false});
						else $(this).animate({left: '100%'},{duration: 500, queue: false});
					});
				} else if ( $(this).hasClass('map') ){
					$('.content-wrap > section').each(function(){
						if ($(this).hasClass('map')) $(this).animate({left: '0%'},{duration: 500, queue: false});
						else if ($(this).hasClass('area-guides')) $(this).animate({left: '100%'},{duration: 500, queue: false});
						else $(this).animate({left: '-100%'},{duration: 500, queue: false});
					});
				} else if ( $(this).hasClass('area-guides') ){
					$('.content-wrap > section').each(function(){
						if (!$(this).hasClass('area-guides')) $(this).animate({left: '-100%'},{duration: 500, queue: false});
						else $(this).animate({left: '0%'},{duration: 500, queue: false});
					});
				}
			});
		}

		this.userSearchInBatches = new function() {
			this.init = function(keyword, token) {
				var aVal = keyword + (typeof token != 'undefined' ? ':more' : '');
				la( AnalyticsType.ANALYTICS_TYPE_CLICK,
          			'Explore', 
          			explore_info.id,
          			aVal);
				ga('send', {
				  'hitType': 'event',          // Required.
				  'eventCategory': 'button',   // Required.
				  'eventAction': 'click',      // Required.
				  'eventLabel': 'Search'+':'+keyword
				});
				ahtb.alert('Finding '+(typeof token != 'undefined' ? 'more ' : '')+'places that match: '+keyword+"...",
							{width: 500});
				explore.userSearchInBatches.oneBatch(keyword, typeof token != 'undefined' ? token : '', typeof token != 'undefined' ? 1 : 0);
			}
			this.oneBatch = function(keyword, token, nthTry) {
				$.ajax({
	              	url: ah_local.tp+"/_pages/ajax-explore.php", 
	              	data: {	query: "user-places-one-page",
	                     	data: {
		                       	info: {//lng: explore_info.lng,
		                       		   //lat: explore_info.lat},
						lng: explore.map.getCenter().lng(),
						lat: explore.map.getCenter().lat() },
		                   		key: keyword,
		                   		pagetoken: token
	                   	 	} 
	                      }, 
	              	dataType: 'json',
	              	type: 'POST',
	              	success: function(d){
	              		if (d.status == 'OK') {
		                	console.log("POI - found:"+d.data.total+", key:"+keyword+", pagetoken:"+d.data.pagetoken+", took "+d.data.time+" secs");
		                	ahtb.close();
		                	if ( d.data.total ) {
		                		if ( typeof d.data.pagetoken == 'undefined' || d.data.pagetoken.length == 0 ) {
		                			explore.pagetoken = '';
		                			$('ul#places-list .searchby #more').hide();
		                		}
		                		else {
		                			explore.pagetoken = d.data.pagetoken;
		                			$('ul#places-list .searchby #more').show();
		                		}
		                			
		                		// else {
		                		// 	ahtb.alert("Getting more...");
			                	// 	window.setTimeout( function() {
			                	// 		explore.userSearchInBatches.oneBatch(keyword, d.data.pagetoken, nthTry+1);
			                	// 	}, 1900);
		                		// }
			                	var points = [];
			                	points['searchby'] = d.data.points;
			                	explore.parse.addSearchBy(points, nthTry);			                		
			                }
			                else
			                	ahtb.alert("Nothing found using keyword:"+keyword+".  Try adding another specifier.");
		                }
		                else {
		                	ahtb.close();
		                	console.log("Failed user-places");
		                }
	              	},
	              	error: function(d) {
	              		ahtb.close();
	                	console.log('Failed to get any POI');
	              	}
	     		 });
			}

		}

		this.gotoWeb = function(place_id) {
			console.log("gotoWeb for "+place_id);
			$.ajax({
              	url: ah_local.tp+"/_pages/ajax-explore.php", 
              	data: {	query: "get-website",
                     	data: {
	                       	id: place_id
                   	 	} 
                      }, 
              	dataType: 'json',
              	type: 'POST',
              	success: function(d){
              		if (d.status == 'OK') {
	                	console.log("site - found:"+d.data);
	                	window.open(d.data);
	                }
	                else {
	                	ahtb.alert("Sorry, could not find website.");
	                }
              	},
              	error: function(d) {
              		ahtb.alert("Sorry, could not locate website.");
                	categoryonsole.log('Failed get-website');
              	}
     		 });
		}

		this.userSearch = function(keyword) {
			ahtb.alert('Finding places that match: '+keyword+"...");
			$.ajax({
              	url: ah_local.tp+"/_pages/ajax-explore.php", 
              	data: {	query: "user-places",
                     	data: {
	                       	info: {lng: explore_info.lng,
	                       		   lat: explore_info.lat},
	                   		key: keyword
                   	 	} 
                      }, 
              	dataType: 'json',
              	type: 'POST',
              	success: function(d){
              		if (d.status == 'OK') {
	                	console.log("POI - found:"+d.data.total+", took "+d.data.time+" secs");
	                	if ( d.data.total ) {
	                		ahtb.close();
		                	var points = [];
		                	points['searchby'] = d.data.points;
		                	explore.parse.addSearchBy(points);
		                }
		                else
		                	ahtb.alert("Nothing found using keyword:"+keyword+".  Try adding another specifier.");
	                }
	                else {
	                	ahtb.close();
	                	console.log("Failed user-places");
	                }
              	},
              	error: function(d) {
              		ahtb.close();
                	categoryonsole.log('Failed to get any POI');
              	}
     		 });
		}

		this.parse = new function(){
			this.addSearchBy = function(d, append) {
				var isNew = typeof append == 'undefined' || !append;
				var category = 'searchby';

				// if is new, then clean up any existing map markers now...
				if ( isNew &&
					 typeof explore.markers[category] != 'undefined') {
					for(var i in explore.markers[category]) explore.markers['searchby'][i].marker.setMap(null);
					explore.markers[category] = [];
				}

				var count = typeof explore.markers[category] != 'undefined' ? explore.markers[category].length : 0;
				explore.parse.addMarkers(d, category, append);
				explore.parse.addListCategory(category, append);
				var isSelected = $('ul#places-list li.'+category).hasClass('selected');
				if (isNew) {
					// if (!explore.visibleCategories['searchby']) {
					// 	var ele = $('ul#places-list > li');
					// 	$('ul#places-list > li').each(function() {
					// 		 if ($(this).is('.selected') )
					// 		 	$(this).fadeOut(250);
					// 	});
					if (!isSelected)
						explore.toggleCategory('searchby');
					else {
						explore.visibleCategories[category] = true;
						$('ul#places-list .'+category).fadeIn(250);
						for (var i in explore.markers) if (i == category) for (var j in explore.markers[i]) {
							explore.markers[i][j].marker.setAnimation(google.maps.Animation.DROP);
							explore.markers[i][j].marker.setMap(explore.map);
							explore.markers[i][j].marker.setVisible(true);
						}
					}
					// }
					//explore.showCategory('searchby');
					// $('ul#places-list > li').each(function() {
					// 	$(this).fadeOut('fast');
					// 	explore.visibleCategories[$(this).attr('category')] = false;
					// });
					// explore.visibleCategories[category] = true;
					// $('ul#places-list .'+category).fadeIn(250);
					// for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
					// 	this.markers[i][j].marker.setAnimation(google.maps.Animation.DROP);
					// 	this.markers[i][j].marker.setMap(explore.map);
					// 	this.markers[i][j].marker.setVisible(true);
					// }
				}
				else {
					$('ul#places-list li.searchby').show();
					while (true) {
						explore.markers['searchby'][count].marker.setAnimation(google.maps.Animation.DROP);
						explore.markers['searchby'][count].marker.setMap(explore.map);
						explore.markers['searchby'][count].marker.setVisible(true);
						count++;
						if ( explore.markers['searchby'].length == count )
							break;
					}
				}
			}

			this.addMarkers = function(d, i, append) {
				var category = i.toLowerCase();
				if ( typeof append == 'undefined' ||
					 !append )
					explore.markers[category] = [];
				var index = explore.markers[category].length;
				for (var j in d[i]){
					var point = d[i][j];
					var place_id = '';
					if ( typeof point.meta != 'undefined' &&
						 point.meta != null && 
						 typeof point.meta.google != 'undefined' && 
						 typeof point.meta.google.place_id != 'undefined')
						place_id = point.meta.google.place_id;
					// explore.markers[category][j] = {
					explore.markers[category][index] = {
						info: point,
						infowindow: new google.maps.InfoWindow({content:
							'<div class="info-content">'+
								'<h1>'+point.name+'</h1>'+
								'<div id="bodyContent">'+
									'<span>'+point.address+'</span>'+
								(place_id.length ? '<a href="javascript:explore.gotoWeb('+"'"+place_id+"'"+')">Go to website</a>' : '')+
								'</div>'+
							'</div>'
						}),
						marker: new google.maps.Marker({
							// icon: {url: point.meta && (typeof point.meta.google != 'undefined') && (typeof point.meta.google.icon != 'undefined') && point.meta.google.icon.length ? point.meta.google.icon : ah_local.tp+'/_img/page-explore-the-area/map_icons/'+category+'_sm.png' },
							icon: {url: ah_local.tp+'/_img/page-explore-the-area/map_icons/'+category+'_sm.png',
							size: new google.maps.Size(30, 30),
							origin: new google.maps.Point(0,0),
							anchor: new google.maps.Point(10, 30) },
							// position: new google.maps.LatLng(0,0),
							position: new google.maps.LatLng(point.lat, point.lng),
							visible: false,
							animation: google.maps.Animation.DROP,
							// id: parseInt(j),
							id: index,
							category: category
						})
					};
					// google.maps.event.addListener(explore.markers[category][j].marker,'click',function(){
					google.maps.event.addListener(explore.markers[category][index].marker,'click',function(){
						for (var i in explore.markers) for (var j in explore.markers[i])
							if (j != this.id) explore.markers[i][j].infowindow.close();
						marker = explore.markers[this.category][this.id];
						marker.infowindow.open(explore.map, marker.marker);
					});
					index++;
				}
			}
			this.markers = function(d, callback){
				explore.markers = {};
				explore.markers['searchby'] = [];
				for (var i in d){
					explore.parse.addMarkers(d, i);
				}
				if (callback && $.isFunction(callback)) callback();
			}

			this.addListCategory = function(i, append) {
				var points = explore.markers[i];
				var category = i;
				if ( category.length == 0 )
					return;

				$('ul#places-list > li').hide();
				$('ul#places-list .'+category+' .count').html( Object.keys(points).length );
				if ( category == 'searchby')
					$('ul#places-list .'+category+' ul').empty();

				var sorted = [];
				for (var j in explore.markers[category]) sorted.push({id:j, distance:explore.markers[category][j].info.distance});
				sorted.sort(function(a, b){ return a.distance - b.distance; });

				for (var j in sorted){
					var point = explore.markers[category][sorted[j].id].info;
					h = '<li class="place" place-id="'+sorted[j].id+'" category="'+category+'">'+
							'<div class="image"><img src="'+ah_local.tp+'/_img/page-explore-the-area/map_icons/'+category+'.png" /></div>'+
							'<span class="name">'+point.name.replace(/\//g,'')+'</span>'+
							'<span class="address">'+point.address+'</span>'+
							'<span class="distance">'+point.distance+' miles</span>';
					if ( typeof point.meta != 'undefined' &&
						 point.meta != null && 
						 typeof point.meta.google != 'undefined' && 
						 typeof point.meta.google.rating != 'undefined' &&
						 point.meta.google.rating != -1)
						h+= '<span class=rating">rated: '+point.meta.google.rating+'</span>';
					h+= '</li>';
					$('ul#places-list .'+category+' ul').append(h);
				}
				$('ul#places-list li.place').off('click').on('click',function(){
					var thisMarker = explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ];
					for (var i in explore.markers) for (var j in explore.markers[i])
							if (j != thisMarker.marker.id) explore.markers[i][j].infowindow.close();
					thisMarker.infowindow.open(explore.map, thisMarker.marker);
					explore.map.panTo(thisMarker.marker.getPosition());
				})
				$('ul#places-list li.place').hover(function(){
					explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ].marker.setAnimation(google.maps.Animation.BOUNCE);
				},function(){
					explore.markers[ $(this).attr('category') ][ $(this).attr('place-id') ].marker.setAnimation(null);
				});
			}

			this.list = function(){
				if (typeof explore.markers == 'undefined' || explore.markers == null || explore.markers.length < 1)
					console.log('no places to list');
				else for (var i in explore.markers){
					var category = i;
					if ( category.length == 0 )
						continue;
					explore.parse.addListCategory(i);
					explore.visibleCategories[category] = false;
				}
			}
		}
		this.hideCategory = function(category){
			explore.visibleCategories[category] = false;
			$('ul#places-list .'+category).fadeOut(250);
			for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
				this.markers[i][j].marker.setPosition( new google.maps.LatLng(0,0) );
				// this.markers[i][j].marker.setVisible(false);
				this.markers[i][j].marker.setMap(null);
				this.markers[i][j].marker.setVisible(false);
			}
		}
		this.toggleCategory = function(category){
			if ( this.markers[category].length == 0)
				return;

			var isSelected = $('ul#places-list li.'+category).hasClass('selected');
			if (isSelected &&
				category == 'searchby') {
				console.log("toggleCategory for searchby and is already selected, exiting");
				return;
			}
			$('li.'+category).hasClass('selected') ? $('li.'+category).removeClass('selected') : $('li.'+category).addClass('selected');
			
			// isSelected ? $('ul#places-list li.'+category).fadeIn(250) : $('ul#places-list li.'+category).fadeOut(250) ;
			var savedValue = explore.visibleCategories[category];
			$('ul#places-list > li').each(function() {
				$(this).fadeOut('fast');
				explore.visibleCategories[$(this).attr('category')] = false;
			});
			if (savedValue) this.hideCategory(category);
			else this.showCategory(category);
		}
		this.showCategory = function(category){
			explore.visibleCategories[category] = true;
			$('ul#places-list .'+category).fadeIn(250);
			var spacer = Math.round( 6000 / (this.markers[category].length ? this.markers[category].length : 1) );
			for (var i in this.markers) if (i == category) for (var j in this.markers[i]) {
				//explore.showMarker(category, j, spacer);
				var pos = j;
				//window.setTimeout(function() {
				if ( explore.markers[category][pos].marker.getAnimation() === null )
					explore.markers[category][pos].marker.setAnimation(google.maps.Animation.DROP);
				var latlng = new google.maps.LatLng(explore.markers[category][pos].info.lat, explore.markers[category][pos].info.lng);
				explore.markers[category][pos].marker.setPosition(latlng);
				// explore.markers[category][pos].marker.setVisible(true);
				explore.markers[category][pos].marker.setMap(explore.map);
				explore.markers[category][pos].marker.setVisible(true);
				//}, spacer);
			}
		}
		this.showMarker = function(category, pos, spacer) {
			window.setTimeout(function() {
                                        //explore.markers[category][pos].marker.setAnimation(google.maps.Animation.DROP);
                                        explore.markers[category][pos].marker.setMap(explore.map);
                                        explore.markers[category][pos].marker.setVisible(true);
                                }, spacer);
		}
	}

	explore.init();
	controller = explore;
});
