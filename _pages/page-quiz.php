<?php

require_once(__DIR__.'/../_classes/Options.class.php');
$Options = new AH\Options(1);
$x = $Options->get((object)['where'=>['opt'=>'SystemMaintenance']]);
$SystemMaintenance = !empty($x) ? intval($x[0]->value) : 0;

if ($SystemMaintenance) : ?>
<div class="quiz-wrap" style="background: url(<?php echo get_template_directory_uri(); ?>/_img/backgrounds/site-maintenance.jpg) no-repeat ; background-size: contain; width: 1440px; height: 800px;">
</div>
<script>
var SystemMaintenance = <?php echo $SystemMaintenance; ?>;
</script>
<?php else:

global $agent; // from header.php
global $havePortal; // from header.php
global $showAgentInfo; // from header.php

$arg = get_query_var('id');

$allowed = [3,4,5,8,27,284,381];
$opt = $Options->get((object)['where'=>['opt'=>'AddressPermittedUsers']]);
if (!empty($opt)) {
	$Options->log("AddressPermittedUsers - {$opt[0]->value}");
	$optAr = json_decode($opt[0]->value);
	if (count($optAr)) {
		foreach($optAr as $i=>$val)
			$optAr[$i] = intval($val);
		$allowed = array_merge($allowed, $optAr);
	}
}
else
	$Options->log("Did not find AddressPermittedUsers");

$user = null;
if (is_user_logged_in()) {
	$user = wp_get_current_user();
	$Options->log("User ID:$user->ID");
}

$opt = $Options->get((object)['where'=>['opt'=>'QuizOptions']]);
$quizOptionList = empty($opt) ? [] : json_decode($opt[0]->value);
$quizOptions = null;
if (!empty($quizOptionList) ) {
	if (!empty($user)) foreach($quizOptionList as $item) {
		if ($item->agentID == $user->ID) {
			$quizOptions = $item;
		}
		unset($item);
		if ($quizOptions)
			break;
	}
	elseif (!empty($agent)) foreach($quizOptionList as $item) {
		if ($item->agentID == $agent->author_id) {
			$quizOptions = $item;
		}
		unset($item);
		if ($quizOptions)
			break;
	}
	// if we didn't find a match, see if there is a global one
	if (empty($quizOptions)) foreach($quizOptionList as $item) {
		if ($item->agentID == -1) {
			$quizOptions = $item;
		}
		unset($item);
		if ($quizOptions)
			break;
	}
}

//PredefinedQuizList
$opt = $Options->get((object)['where'=>['opt'=>'PredefinedQuizList']]);
$predefinedQuizList = empty($opt) ? [] : json_decode($opt[0]->value);
$predefinedQuizOptions = null;
if (!empty($predefinedQuizList) ) {
	if (!empty($user)) foreach($predefinedQuizList as $item) {
		if ($item->agentID == $user->ID) {
			$predefinedQuizOptions = $item;
		}
		unset($item);
		if ($predefinedQuizOptions)
			break;
	}
	elseif (!empty($agent)) foreach($predefinedQuizList as $item) {
		if ($item->agentID == $agent->author_id) {
			$predefinedQuizOptions = $item;
		}
		unset($item);
		if ($predefinedQuizOptions)
			break;
	}
	// if we didn't find a match, see if there is a global one
	if (empty($predefinedQuizOptions)) foreach($predefinedQuizList as $item) {
		if ($item->agentID == -1) {
			$predefinedQuizOptions = $item;
		}
		unset($item);
		if ($predefinedQuizOptions)
			break;
	}
}

$havePermit = !empty($user) && in_array($user->ID, $allowed);
$Options->log("havePermit - AddressPermittedUsers: $havePermit");

$opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
$noAskAllowMinPrice = !empty($opt) ? intval($opt[0]->value) : 800000;

$priceList = null;
$priceListMax = null;
$lastPrice = 20000000;
if ($noAskAllowMinPrice < 800000) {
	$base = $noAskAllowMinPrice / 1000;
	$firstPrice = $noAskAllowMinPrice+1;
	$priceList = (object)[$noAskAllowMinPrice=>"Min Price",
						  $firstPrice=>'$'.number_format($base).'K'];
	
	for( $start = $noAskAllowMinPrice+50000; $start <= 800000; $start += 50000)
		$priceList->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceList->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	// max price list
	$base = $noAskAllowMinPrice + 100000;
	$priceListMax = (object)[];
	
	for( $start = $base; $start <= 800000; $start += 50000)
		$priceListMax->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceListMax->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	$start = $lastPrice-1;
	$priceListMax->$start = '$'.number_format($lastPrice/1000000).' Million';
	$priceListMax->$lastPrice = 'Max Price';
}

$desktopPredefinedList = '';
$desktopPredefinedListOnPage = '';
$mobilePredefinedList = '';
if ($quizOptions &&
	isset($quizOptions->doPredefinedQuiz) &&
	$quizOptions->doPredefinedQuiz &&
	$predefinedQuizOptions) {
	$width = $predefinedQuizOptions->desktop->slideSizeDialog == 'custom' ? $predefinedQuizOptions->desktop->widthDialog : explode('x', $predefinedQuizOptions->desktop->slideSizeDialog)[0];
	$height = $predefinedQuizOptions->desktop->slideSizeDialog == 'custom' ? $predefinedQuizOptions->desktop->heightDialog : explode('x', $predefinedQuizOptions->desktop->slideSizeDialog)[1];
	$widthOnPage = $predefinedQuizOptions->desktop->slideSizeOnPage == 'custom' ? $predefinedQuizOptions->desktop->widthOnPage : explode('x', $predefinedQuizOptions->desktop->slideSizeOnPage)[0];
	$heightOnPage = $predefinedQuizOptions->desktop->slideSizeOnPage == 'custom' ? $predefinedQuizOptions->desktop->heightOnPage : explode('x', $predefinedQuizOptions->desktop->slideSizeOnPage)[1];
	$predefinedQuizOptions->imgPath = html_entity_decode($predefinedQuizOptions->imgPath);

	foreach($predefinedQuizOptions->desktop->quiz_list as $quiz) {
		$quizHref = get_home_url().($quiz->quizMode == 0 ? "/quiz-results/R-$quiz->quizID" : "/$quiz->quizSEO");
		// $desktopPredefinedList .= '<li class="slide" style="width:'.$width.'px'.';height:'.$height.'px;">';
		// $desktopPredefinedList .=	'<a href="javascript:dimPredfinedQuizDialog('."'".$quizHref."'".');" class="predefined-quiz"';
		// $desktopPredefinedList .=		'<h2>'.$quiz->title.'</h2>';
		// $desktopPredefinedList .=		'<img class="image" src="'.get_template_directory_uri().'/'.$predefinedQuizOptions->imgPath.'/'.$predefinedQuizOptions->desktop->slideSize."/$quiz->img".'"/>';
		// $desktopPredefinedList .=	'</a>';
		// $desktopPredefinedList .= '</li>';
		$desktopPredefinedList .= '<li class="slide" style="width:'.$width.'px'.';height:'.$height.'px;">';
		$desktopPredefinedList .=	'<div class="image" style="width:'.$width.'px'.';height:'.$height.'px;background:url('.get_template_directory_uri()."/".$predefinedQuizOptions->imgPath.'/'.$predefinedQuizOptions->desktop->slideSizeDialog."/$quiz->img".'); norepeat;">';
		$desktopPredefinedList .=		'<a href="javascript:dimPredfinedQuizDialog('."'".$quizHref."'".');" class="predefined-quiz mobilegray" >';
		if (!empty($quiz->icon))
			$desktopPredefinedList .=			'<img class="icon" src="'.get_template_directory_uri()."/_img/tag_icons/".$quiz->icon.'" />';
		$desktopPredefinedList .=			'<span class="text">'.$quiz->title.'</span>';
		$desktopPredefinedList .=		'</a>';
		$desktopPredefinedList .= 	'</div>';
		$desktopPredefinedList .= '</li>';

		$desktopPredefinedListOnPage .= '<li class="slide" style="width:'.$widthOnPage.'px'.';height:'.$heightOnPage.'px;">';
		$desktopPredefinedListOnPage .=	'<div class="image" style="width:'.$widthOnPage.'px'.';height:'.$heightOnPage.'px;background:url('.get_template_directory_uri()."/".$predefinedQuizOptions->imgPath.'/'.$predefinedQuizOptions->desktop->slideSizeOnPage."/$quiz->img".'); norepeat;">';
		$desktopPredefinedListOnPage .=		'<a href="javascript:dimPredfinedQuizDialog('."'".$quizHref."'".');" class="predefined-quiz mobilegray" >';
		if (!empty($quiz->icon))
			$desktopPredefinedListOnPage .=			'<img class="icon" src="'.get_template_directory_uri()."/_img/tag_icons/".$quiz->icon.'" />';
		$desktopPredefinedListOnPage .=			'<span class="text">'.$quiz->title.'</span>';
		$desktopPredefinedListOnPage .=		'</a>';
		$desktopPredefinedListOnPage .= 	'</div>';
		$desktopPredefinedListOnPage .= '</li>';
	}

	$width = $predefinedQuizOptions->mobile->slideSize == 'custom' ? $predefinedQuizOptions->mobile->width : explode('x', $predefinedQuizOptions->mobile->slideSize)[0];
	$height = $predefinedQuizOptions->mobile->slideSize == 'custom' ? $predefinedQuizOptions->mobile->height : explode('x', $predefinedQuizOptions->mobile->slideSize)[1];
	foreach($predefinedQuizOptions->mobile->quiz_list as $quiz) {
		$quizHref = get_home_url().($quiz->quizMode == 0 ? "/quiz-results/R-$quiz->quizID" : "/$quiz->quizSEO");
		$mobilePredefinedList .= '<li class="slide">';
		$mobilePredefinedList .=	'<div class="image" style="width:'.$width.'px'.';height:'.$height.'px;background:url('.get_template_directory_uri()."/".$predefinedQuizOptions->imgPath.'/'.$predefinedQuizOptions->mobile->slideSize."/$quiz->img".'); norepeat;">';
		$mobilePredefinedList .=		'<a href="javascript:dimPredfinedQuizDialog('."'".$quizHref."'".');" class="predefined-quiz mobilegray" >';
		if (!empty($quiz->icon))
			$mobilePredefinedList .=			'<img class="icon" src="'.get_template_directory_uri()."/_img/tag_icons/".$quiz->icon.'" />';
		$mobilePredefinedList .=			'<span class="text">'.$quiz->title.'</span>';
		$mobilePredefinedList .=		'</a>';
		$mobilePredefinedList .= 	'</div>';
		$mobilePredefinedList .= '</li>';
	}
}

?>
<script>
var SystemMaintenance = 0;
var noAskAllowMinPrice = <?php echo $noAskAllowMinPrice; ?>;
var priceList = <?php echo json_encode($priceList); ?>;
var priceListMax = <?php echo json_encode($priceListMax); ?>;
var lastPrice = <?php echo $lastPrice; ?>;
var quizOptions = <?php echo json_encode($quizOptions ? $quizOptions : []); ?>;
var predefinedQuizOptions = <?php echo json_encode($predefinedQuizOptions ? $predefinedQuizOptions : []); ?>;
var arg = '<?php echo empty($arg) ? '' : $arg; ?>';

jQuery(document).ready(function($){
	$('.quiz-wrap div.quiz-select .chooseQuiz .fullsearch a').on('click', function() {
		$('.quiz-wrap div.quiz-select .chooseQuiz .fullsearch span').addClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .specificsearch span').removeClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .addresssearch span').removeClass( 'active' );
	});
	$('.quiz-wrap div.quiz-select .chooseQuiz .specificsearch a').on('click', function() {
		$('.quiz-wrap div.quiz-select .chooseQuiz .specificsearch span').addClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .fullsearch span').removeClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .addresssearch span').removeClass( 'active' );
	});
	$('.quiz-wrap div.quiz-select .chooseQuiz .addresssearch a').on('click', function() {
		$('.quiz-wrap div.quiz-select .chooseQuiz .addresssearch span').addClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .specificsearch span').removeClass( 'active' );
		$('.quiz-wrap div.quiz-select .chooseQuiz .fullsearch span').removeClass( 'active' );
	});
});
</script>

<!--Media Query Section--> <section class="mobile-identifier-landscape" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="mobile-identifier-all" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="isthreeslide-identifier" style="display: none;"></section> <!--<![endif]-->
<!--Media Query Section--> <section class="istwoslide-identifier" style="display: none;"></section> <!--<![endif]-->

<div id="overlay-bg" style="display: none;">
	<div class="predefined desktop">
		<div class="intro">
			<?php 
				if ( isset($predefinedQuizOptions) && !empty($predefinedQuizOptions) ) {
					if (isset($predefinedQuizOptions->desktop) && isset($predefinedQuizOptions->desktop->intro))
						AH\outputDynamicHtml($predefinedQuizOptions->desktop->intro);
				}
			?>
			<button id="custom-quiz"><?php echo $predefinedQuizOptions ? $predefinedQuizOptions->desktop->buttonString : ''; ?></button>
		</div>
		<div class="divider">
			<div class="circle">
				<p>OR</p>
			</div>
		</div>
		<div class="slides-wrapper">
			<div class="title"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-quiz/fire.png"/>Popular Searches</div>
			<div class="arrow" id="left">
				<span class="point">
					<span class="click entypo-left-open-big"></span>
				</span>
			</div>
			<div class="slides">
				<ul id="quiz-list desktop" style="right: 0px;">
					<?php echo $desktopPredefinedList; ?>
				</ul>
			</div>
			<div class="arrow" id="right">
				<span class="point">
					<span class="click entypo-right-open-big"></span>
				</span>
			</div>
		</div>
	</div>
	<div class="predefined mobile">
		<div class="intro">
			<?php 
				if ( isset($predefinedQuizOptions) && !empty($predefinedQuizOptions) ) {
					if (isset($predefinedQuizOptions->mobile) && isset($predefinedQuizOptions->mobile->intro))
						AH\outputDynamicHtml($predefinedQuizOptions->mobile->intro);
				}
			?>
			<button id="custom-quiz"><?php echo $predefinedQuizOptions ? $predefinedQuizOptions->desktop->buttonString : ''; ?></button>
		</div>
		<div class="divider">
			<div class="circle">
				<p>OR</p>
			</div>
		</div>
		<div class="slides-wrapper">
			<div class="title"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-quiz/fire.png"/>Popular Searches</div>
			<div class="slides">
				<ul id="quiz-list mobile">
					<?php echo $mobilePredefinedList; ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="quiz-wrap">
	<span class="desktopbar">
		<div id="progress-bar">
			<div class="quizlogo"><a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a></div>
			<div class="progress"><span class="mobilequestion">Question </span><span class="number">2</span> of 10</div>
			<div class="bar"><div class="fill"></div></div>
			<a class="skip-to-end" href="javascript:;"><div class="smallskip">Skip to my listing results</div><div class="largeskip"><span>Skip the rest,</span> I want to see my homes</div> <span class="entypo-right-open-big"></span></a>
		</div>
	</span>
	<span class="mobilebar">
		<div id="progress-bar">
			<div class="progress"><span class="number"></span> of 10</div>
			<div class="bar"><div class="fill"></div></div>
		</div>
	</span>
	<div class="basic-info-bg" style="display: none;"></div>
	<article class="quiz-question">
		<div class="question-info">
			<div class="questiondiv">
				<div class="question-wrapper">
					<span class="question-number">Q<span class="number"></span>:</span>
					<span class="main-question"></span>
				</div>
			</div>
			<span class="dekstopquizbuttons">
				<button type="button" id="button-next" name="next" value="next" class="button next">
					<span class="text">Next Question</span>
				</button>
				<button type="button" id="button-back" name="back" value="back" class="button back"><span class="entypo-left-open-big"></span>Previous Question</button>
			</span>
		</div>
	</article>
	<div class="selectimages">
		<div class="slides"></div>
	</div>
	<div class="quiz-select" style="display: none;">
		<span class="title"><?php if ($showAgentInfo) echo '<img src="'.get_template_directory_uri().'/_img/_authors/250x250/'.$agent->photo.'"; id="agent-img" />' ?>Where do you want to look?</span>
        <div class="chooseQuiz"> <!-- even == city, odd == nationwide or statewide -->
          <span class="fullsearch"><a href="javascript:;" class="choose selected" quiz="<?php echo QUIZ_NATION; ?>"><span class="circle active"></span>Anywhere in the U.S.</a></span>
          <span class="specificsearch"><a href="javascript:;" class="choose" quiz="<?php echo QUIZ_LOCALIZED; ?>"><span class="circle"></span>Near a City or State</a></span>
          <span class="addresssearch"><a href="javascript:;" <?php echo !$havePermit ? 'style="display: none;"' : ''; ?> class="choose" quiz="<?php echo QUIZ_BY_ADDRESS; ?>"><span class="circle"></span>By Address</a></span>
        </div>
        <div class="specific quiz-0" style="display: none;">
        	<div class="autocomplete area">
            	<input type="text" class="inactive" placeholder="Enter City or State"></input>
            </div>
        	<div class="searchexact">
        		<input type='checkbox' class='distance' checked ></input><span class="title">Within a radius of</span>
				<select name='distance' id='miles'> 
                  <option value='8' selected='selected'>8 Miles</option> 
                  <option value='15'>15 Miles</option> 
                  <option value='30'>30 Miles</option> 
                  <option value='50'>50 Miles</option> 
                  <option value='80'>80 Miles</option> 
                  <option value='120'>120 Miles</option> 
              	</select>
             </div> 
        </div> <!-- specific div -->
        <div class="address quiz-0" style="display: none;">
        	<!-- <input type="text" class="address" placeholder="Enter Street Address"></input> -->
        	<div class="autocomplete address">
            	<input type="text" class="inactive" placeholder="Enter Address"></input>
            </div>
            <span id="marker" class="entypo-attention" style="color: red; display: none;" title="Unable to find a listing by that address"></span>
        </div>
        <div id="control">
	        <button id="nextQ">Search by lifestyle</button>
	        <button id="showAll" style="display: none;">Or skip to all homes <span class="entypo-right-open-mini"></span></button>
	        <br/>
	        <?php if ($quizOptions && $quizOptions->showListingFilter) : ?>
	        <div id="median-range" style="display: none;">
				<span id="msg">Listings filter</span>
				<div id="radio-item" style="display: none"><input type="checkbox" name="no-limit" value="0" checked><span>No median price limit</span></div>
				<div id="radio-item"><input type="checkbox" name="commercial-ok" value="1"><span>Include commercial</span></div>
				<div id="radio-item"><input type="checkbox" name="rental-ok" value="2"><span>Include rental</span></div>
				<div id="radio-item"><input type="checkbox" name="rental-only" value="3"><span>Only rentals</span></div>
			</div>
			<?php endif; ?>
	        <button id="findListing" style="display: none;">Show listing</button>
	    </div>
    </div> <!-- quiz-wrapper div -->
    <div class="predefined-quiz">
	    <div class="predefined desktop">
			<div class="intro">
			<?php 
				if ( isset($predefinedQuizOptions) && !empty($predefinedQuizOptions) ) {
					if (isset($predefinedQuizOptions->desktop) && isset($predefinedQuizOptions->desktop->intro))
						AH\outputDynamicHtml($predefinedQuizOptions->desktop->intro);
				}
			?>
			</div>
			<div class="slides">
				<ul id="quiz-list desktop">
					<?php echo $desktopPredefinedListOnPage; ?>
				</ul>
			</div>
		</div>
		<!--
		<div class="predefined mobile">
			<div class="intro">
			<?php 
				if ( isset($predefinedQuizOptions) && !empty($predefinedQuizOptions) ) {
					if (isset($predefinedQuizOptions->mobile) && isset($predefinedQuizOptions->mobile->intro))
						AH\outputDynamicHtml($predefinedQuizOptions->mobile->intro);
				}
			?>
			</div>
			<div class="slides">
				<ul id="quiz-list mobile">
					<?php echo $mobilePredefinedList; ?>
				</ul>
			</div>
		</div>
		-->
	</div>
	<div class="home-options-progress" id="progress-bar" style="display:none">
		<div class="quizlogo"><a href="<?php bloginfo('wpurl'); ?>" class="logo"><span class="life notranslate">Life</span><span class="notranslate">styled</span><span class="lifestyledsmall notranslate">Listings</span></a></div>
		<div class="progress"><span class="mobilequestion">Question </span>1 of 10</div>
		<div class="bar"><div class="fill"></div></div>
	</div>
	<div class="home-options" style="display: none;">
		<span class="title"><?php if ($showAgentInfo) echo '<img src="'.get_template_directory_uri().'/_img/_authors/250x250/'.$agent->photo.'"; id="agent-img" />' ?>First, the basics.</span>
		<div class='selectBoxmax priceList'>
			<span class='selected'>Set Max Price<span class='selectArrow'></span></span>
			<ul class="prices" id="max" class="price"></ul>
		</div>
		<div class='selectBed homeOptions'>
			<span class='selected'>Set Bedroom Count<span class='selectArrow'></span></span>
			<ul class="options" id="beds" class="home"></ul>
		</div>
		<button type="button" id="button-next" name="next" value="next" class="button next">	
			<span class="text">Next Question</span>
		</button>
	</div>
	<span class="mobilequizbuttons">
		<button type="button" id="button-next" name="next" value="next" class="button next">
			<span class="text">Next Question</span>
		</button>
		<button type="button" id="button-back" name="back" value="back" class="button back">
			<span class="text">Previous Question</span>
		</button>
		<a class="skip-to-end" href="javascript:;">Skip the rest, I want to see my homes <span class="entypo-right-open-big"></span></a>
	</span>
</div>
<?php endif; ?>