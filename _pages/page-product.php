<?php

$product = get_query_var('id');

$gotPortal = (!empty($product) && strpos($product, "portal-agent") !== false) ? true : false;
$goToPortalPage = false;
if ($gotPortal &&
	strlen($product) > strlen("portal-agent"))
	$goToPortalPage = true;

$gotAM = (!empty($product) && strpos($product, "agent-match") !== false) ? true : false;
$goToAMPage = false;
if ($gotAM &&
	strlen($product) > strlen("agent-match"))
	$goToAMPage = true;

require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options();
$agentMatchId = $Options->get((object)['where'=>['opt'=>'AgentMatchProductID']]);
$portalId = $Options->get((object)['where'=>['opt'=>'PortalAgentProductID']]);
if (empty($agentMatchId)) 
	$agentMatchId = 174;
else 
	$agentMatchId = intval($agentMatchId[0]->value);

if (empty($portalId)) 
	$portalId = 143;
else 
	$portalId = intval($portalId[0]->value);


if ($gotPortal) { 
	if (!$goToPortalPage) : ?>
		<div id="checkout-container">
			<?php  /* echo do_shortcode('[products ids="143, 142, 173" orderby="date" order="asc"]'); */?>
			<?php  echo do_shortcode('[products ids="'.$portalId.'"]'); ?>
		</div> 
 <?php 
 	else : ?>
 		<script type="text/javascript">
		jQuery(document).ready(function($){
			window.location = ah_local.wp + '/sellers/#portal';
		})
		</script>
 <?php
 	endif;
}
else if ($gotAM) { 
	if (!$goToAMPage) : ?>
	<div id="checkout-container">
		<?php  /* echo do_shortcode('[products ids="175, 176, 174" orderby="sku" order="asc"]');  */?>
		<?php  echo do_shortcode('[products ids="'.$agentMatchId.'"]'); ?>
	</div> 
 <?php 
 	else : ?>
 		<script type="text/javascript">
		jQuery(document).ready(function($){
			window.location = ah_local.wp + '/sellers/#agent_match_reserve';
		})
		</script>
 <?php
 	endif;
}
else { ?>
	<div id="checkout-container">
		<?php  echo do_shortcode('[products ids="'.$portalId.', '.$agentMatchId.',258"]'); ?>
		<?php  /* echo do_shortcode('[products ids="143, 173, 142" orderby="sku" order="asc"]'); */ ?>
		<?php  /* echo do_shortcode('[products ids="174, 175, 176" orderby="sku" order="asc"]'); */ ?>
		<?php  /* echo do_shortcode('[products'); */  ?>
		<?php /* echo do_shortcode('[woocommerce_one_page_checkout template="product-list" product_ids="139,143,142,173,174, 175,176"]'); */ ?>
	</div> 
<?php 
}



