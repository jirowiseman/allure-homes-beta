<?php
require_once(__DIR__.'./../_classes/Options.class.php'); $Options = new AH\Options(1);

$allowed = [3,4,5,8];
$opt = $Options->get((object)['where'=>['opt'=>'DirectoryPermittedUsers']]);
if (!empty($opt)) {
	$Options->log("DirectoryPermittedUsers - {$opt[0]->value}");
	$optAr = json_decode($opt[0]->value);
	if (count($optAr)) {
		foreach($optAr as $i=>$val)
			$optAr[$i] = intval($val);
		$allowed = array_merge($allowed, $optAr);
	}
}
else
	$Options->log("Did not find DirectoryPermittedUsers");

$user = null;
if (is_user_logged_in()) {
	$user = wp_get_current_user();
	$Options->log("User ID:$user->ID");
}

$havePermit = !empty($user) && in_array($user->ID, $allowed);
$Options->log("havePermit: $havePermit");

if (!is_user_logged_in() ||
	!$havePermit) {
	if (headers_sent())
		echo '<script type="text/javascript"> window.location = "'.get_home_url().'"; </script>';
	else {
		header('Location: '.get_home_url());
		header("Connection: close");
	}
	exit;
}
else
	require_once(__DIR__.'/../_admin/sales.php');
?>

