<?php
namespace AH;

//echo "entered page-register.php";

require_once(__DIR__.'/../_classes/States.php');
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new Options();
global $usStates;

$source = get_query_var('id');
//echo $source;

$seller = null;
$user = null;
$sellerState = -1;
$isAdmin = 0;
if ( !empty($source) ) {
	if ( $source != 'seller' &&
	 	 is_numeric($source) ) {
		require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new Sellers();
		$seller = $Sellers->get((object)[ 'where' => ['id'=>$source] ]);
		$seller = $seller ? array_pop($seller) : null;
		foreach($usStates as $state) {
			if ($state == $seller->state) {
				$sellerState = $state;
				break;
			}
		}
	}
	elseif ( $source == 'admin' )
		$isAdmin = 1;
	else if (is_user_logged_in()) { // user is logged in and is trying to register as a seller.
		$id  = wp_get_current_user()->ID;
		$user = get_userdata($id);
	}
}

if ($_SERVER['SERVER_NAME'] == 'localhost') $local = true; // So I can work locally without pulling remote scripts
else $local = false;

global $thisPage;	
$opt = $Options->get((object)['where'=>['opt'=>'VideoList']]);
$videoId = 120089261;
if (!empty($opt)) {
	$videoList = json_decode($opt[0]->value);
	foreach($videoList as $page=>$video) {
		if (strtolower($page) == $thisPage) {
			$videoId = $video;
			break;
		}
	}
}

$passwordURL = get_home_url().'/wp-login.php?action=lostpassword';

?>

<script type="text/javascript">
var seller = null;
var user = null;
var source = null;
var sellerState = null;
var isAdmin = <?php echo $isAdmin; ?>;
</script>

<?php if ( $seller ) : ?>
<script type="text/javascript">
//jQuery( document ).tooltip();
var seller = <?php echo json_encode($seller); ?>; 
var sellerState = '<?php echo $sellerState ?>';
</script>
<?php endif; 

if ( $user ) : ?>
<script type="text/javascript">
var user = <?php echo json_encode($user); ?>; 
</script>
<?php endif; 

if ( !empty($source) ) : ?>
<script type="text/javascript">
var source = '<?php echo $source; ?>';
var videoId = '<?php echo $videoId; ?>';
</script>
<?php endif; ?>

<div id="agent-verify">
</div>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<div id="page-register">
	<div class="videoDiv">
		<button class="closeVid"><span class="line1"></span><span class="line2"></span></button>
		<span class="video">
			<iframe class="video" src="//player.vimeo.com/video/<?php echo $videoId; ?>" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</span>
	</div>
	<section>
        <div class="calltoaction"> <!-- <img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-register/start-selling.png"/> --> </div>
        <div class="top">
            <div class="left-col">
                <div class="text">
		            <p class="matchtitle">GET SPECIFIC</p>
                    <p class="sub-title">Match buyers to your home</p>
		            <p class="small">Market the things that really matter about your listing by connecting with<br/>buyers over the unique features and lifestyle of your property.</p>
                </div>
				<a class="video" href="javascript:;" target="_blank" >Watch Video <span class="entypo-right-dir"></span></a>
            </div>

            <div class="right-col-wrapper">
							<div class="right-col">
            <?php // if(get_option('users_can_register')) : ?>
			     <h2>Already a Member?</h2>
			 <?php
			     $form = wp_login_form( array( 	'echo' => false, 
			     								'redirect' => get_home_url(),
													'label_username' => __( '' ), 
			     								'label_password' => __( '' ), 
			     								'form_id' => 'login-form', 
			     								'remember' => false) );
	             $form = explode('</form>', $form);
	             echo $form[0];
	         ?>
	             <p class="forgot-password"><a href="<?php echo $passwordURL; ?>" title="Forgot your password?">Forgot your password?</a></p>
              <button type="submit" id="login-button" name="login-button">Log in</button>   
	            </form>
			     <h2>Create an Account</h2>
			     <form id="register">
			     	<div id="name">
					    <input type="text" name="first-name" placeholder="First Name" />
					    <input type="text" name="last-name" placeholder="Last Name" />
					</div>
					<div id="email">
					    <input type="text" name="email" placeholder="Enter email: Name@example.com" />
					</div>
					<div id="login">
					    <input type="text" name="login-id" class="login-id" placeholder="Login Name (Defaults to Email)" title="Optional, defaults to your email">
					</div>
					<div id="passwordDiv">
				    	<input type="password" name="password" id="password" placeholder="Password (At least 8 characters)" />
					</div>
					<div id="verifyDiv">
							<div class="check-pass">
								<span id="marker" class="entypo-attention"></span>
							</div>
				    	<input type="password" name="password" id="verify" placeholder="Verify Password" />
				  </div>
				  <div id="professional">
				    	<input type="checkbox" id="realtor" /> <span id="identify">I am a real estate professional </span>
				  </div>
				  <div id="profData">
				    <div id="bre">
					    <input type="text" id="breID" name="realtor-id" placeholder="RE License Number" />
							<div class="statescode">
								<select name="States" id="states">
									<option value="-1" selected="selected">State</option>
									<?php foreach($usStates as $state) { ?>
										<option value="<?php echo $state; ?>"><?php echo $state; ?></option>
									<?php } ?>
								</select>
								<input type="text" id="invitation_code" placeHolder="Invite Code (Optional)">
							</div>
							<p style="margin: 0 10%; font-size: .75em; letter-spacing: 0; color: #333;">* Register with the same email you use for your MLS</p>
						</div>
				  </div>
				  <button type="submit" id="submitbtn" name="submit">Join Us</button>
				  <div id="opt-in">
						<input type="checkbox" id="opt-in"/>
						<span>I would like to receive emails with helpful guides and updates related to my account.</span>
				  </div>
				  <div id="result" style="display:inline-block;"></div>
			    </form>

		<?php // else : 
			// echo "Registration is currently disabled. Please try again later."; 
		// endif; ?>
            </div>
			</div>
		</div>
    </section>
</div>
