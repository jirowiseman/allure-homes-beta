<?php
require_once(__DIR__.'/../_classes/Sellers.class.php');
$q = new stdClass(); 
$s = new AH\Sellers;
$wpId = wp_get_current_user()->ID;
$q->where = array('author_id'=>$wpId);
$x = $s->get($q);

$x = ($wpId == 0)  ? "0" :
     (current_user_can("create_users")) ? (!empty($x) ?  json_encode($x[0]) :  "2") :
	 (!empty($x)) ?  json_encode($x[0]) :  "1"; 
 ?>
<script type="text/javascript">
var validSeller = <?php echo $x; ?>;
</script>


<div id="checkout-container">
	<?php echo do_shortcode('[woocommerce_my_account]'); ?>
</div>