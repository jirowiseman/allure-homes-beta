<?php
global $ALR; 
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1);
require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations;
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options;
require_once(__DIR__.'/../_classes/States.php');
global $usStates;
global $ALR;

$amTags = $Sellers->getAMTagList();
$intro = new \stdClass();
$intro->id= 0;
$intro->tag = "Choose LifeStyle";
array_unshift($amTags, $intro);

$seller_id = get_query_var('id');
$seller = null;
$track = null;
$trackId = 0;
$discount = 0;
$coupon = "Portal10Ok";
$discount = 29;
$promo = '';

if (strpos($seller_id, "-") != false) {
	$parts = explode("-", $seller_id);
	
	if (strpos($parts[0], "T") === false &&
		strpos($parts[0], "C") === false) {
		$isAdmin = intval($parts[0]);
		$Sellers->log("page-agent-purchase-portal - isAdmin: ".$parts[0]);
	}
	else if (strpos($parts[0], "T") !== false) {
		$seller_id = $parts[1];
		$trackId = substr($parts[0], 1);
		$Sellers->log("page-agent-purchase-portal - track: ".$parts[0].", id: $trackId");
		require_once(__DIR__.'/../_classes/EmailTracker.class.php'); $et = new AH\EmailTracker(1);
		$track = $et->get((object)['where'=>['id'=>$trackId]]);
		$et->log("page-agent-purchase-portal - trackId:$trackId, got ".(!empty($track) ? 'one' : 'none'));
		if (!empty($track)) {
			$track = array_pop($track);
			$et->log("page-agent-purchase-portal - track: ".print_r($track, true));
			switch(intval($track->type)) {
				case PORTAL_PROMO_1:
					$opt = $Options->get((object)['where'=>['opt'=>'PORTAL_PROMO_1']]);
					$et->log("page-agent-purchase-portal - opt: ".print_r($opt, true));
					if (empty($opt)) {
						$discount = 29.00;
						$coupon = "Portal10OK";
					}
					else {
						$opt = json_decode($opt[0]->value);
						$discount = $opt->discount;
						$coupon = $opt->coupon;
					}
					break;
			}
		}
	}
	else if (strpos($parts[0], "C") !== false) {
		$promo = $seller_id;
		$seller_id = 0;
		$opt = $Options->get((object)['where'=>['opt'=>$parts[1]]]);
		$Sellers->log("page-agent-purchase-portal - opt: ".print_r($opt, true));
		if (empty($opt)) {
			$discount = 29.00;
			$coupon = "Portal10OK";
		}
		else {
			$opt = json_decode($opt[0]->value);
			$discount = $opt->discount;
			$coupon = $opt->coupon;
		}
	}
}
else {
	$opt = $Options->get((object)['where'=>['opt'=>'DefaultPortalCoupon']]);
	if (!empty($opt)) {
		$Sellers->log("page-agent-purchase-portal - opt: ".print_r($opt, true));
		$opt = json_decode($opt[0]->value);
		$discount = $opt->discount;
		$coupon = $opt->coupon;
	}
	else
		$Sellers->log("page-agent-purchase-portal - no DefaultPortalCoupon found.");
}

$isFree = $discount == 39;

if ( !empty($seller_id) ) {
	$seller = $Sellers->get((object)['where'=>['id'=>$seller_id]]);
	if (!empty($seller)) {
		$seller = $seller[0];
	}
}
	
if (empty($seller)) {
	if (is_user_logged_in()) {
		$seller = $ALR->get('seller');	 
		if (empty($seller)) {
			$user = wp_get_current_user();
			if ($user && !is_wp_error( $user ) && gettype($user) == 'object' && property_exists($user, 'ID') && $user->ID) {
				$seller = new \stdClass();
				$seller->id = 0;
				$user = (object)$user;
				$seller->first_name = $user->user_firstname;
				$seller->last_name = $user->user_lastname;
				$seller->email = $user->user_email;
				$seller->state = '';
			}
		}
	}
}
	
if (empty($seller)) {
	$seller = new \stdClass();
	$seller->id = 0;
	$seller->first_name = '';
	$seller->last_name = '';
	$seller->email = '';
	$seller->state = '';
}

$seller->portal = ''; // default value

$i = 0; $states = '';
foreach($usStates as $long=>$state) {
	$states .= '<option value="'.$state.'">'."$long - ($state)".'</option>';
	$i++;
}

$allStates = [];
foreach($usStates as $long=>$state) {
	$allStates[] = (object)['label'=>$long,
							'value'=>$long,
							'id'=>$state];
	$allStates[] = (object)['label'=>$state,
							'value'=>$state,
							'id'=>$state];
}

$service_area = '';
$meta = null;
if (!empty($seller)) {
	if (isset($seller->service_areas) &&
		!empty($seller->service_areas)) {
		$areas = explode(":", $seller->service_areas);
		$areas = explode(',', $areas[0]);
		if (count($areas) == 2) {
			$city = trim($areas[0]);
			$state = trim($areas[1]);
			$service_area = $city.', '.$state;
		}
	}
	elseif (isset($seller->city) &&
			isset($seller->state) &&
			!empty($seller->city) &&
			!empty($seller->state))
		$service_area = $seller->city.", ".$seller->state;

	$reservation = !empty($seller->id) ?  $Reservations->get((object)['where'=>['email'=>$seller->email]]) : null;
	if (!empty($reservation)) {
		if  (!empty($reservation[0]->meta)) foreach($reservation[0]->meta as $data) 
			if ($data->action == ORDER_AGENT_MATCH) {
				$meta = $data;
				break;
			}
		if (isset($reservation[0]->portal) &&
			!empty($reservation[0]->portal))
			$seller->portal = $reservation[0]->portal;
	}
}

$sellerEmail = !empty($seller) && isset($seller->email) && !empty($seller->email) ? str_replace('"','', str_replace("'",'',$seller->email)) : '';

$Sellers->log("page-agent-purchase-portal - sellerEmail: $sellerEmail");
?>

<script type="text/javascript">
// var amTagList = <?php echo json_encode($amTags); ?>;
var seller = <?php echo json_encode($seller); ?>;
// var service_area = '<?php echo $service_area; ?>';
// var meta = <?php echo $meta ? json_encode($meta) : 0 ?>;
var et = '<?php echo !empty($track) ? $track->track_id : 0 ?>';
var key = <?php echo $trackId; ?>;
var type = <?php echo !empty($track) ? $track->type : 0 ?>;
var discount = <?php echo $discount; ?>;
var coupon = '<?php echo $coupon; ?>';
var sellerEmail = "<?php echo $sellerEmail; ?>";
var allStates = <?php echo json_encode($allStates); ?>;
var promo = '<?php echo $promo; ?>';
// console.log("discount:"+discount+", coupon:"+coupon);
</script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.portalinput .urlinput').on('click', function() {
			event.preventDefault(); 
			$('.portalinput #portal').focus(); 
		});
		$('.portalinput #portal').keypress(function(e){
      if(e.keyCode==13)
      $('.portalinput #check-it').click();
    });
		$('.portalinputmobile .urlinput').on('click', function() {
			event.preventDefault(); 
			$('.portalinputmobile #portal').focus(); 
		});
		$('.portalinputmobile #portal').keypress(function(e){
      if(e.keyCode==13)
      $('.portalinputmobile #check-it').click();
    });
	});
</script>
<div class="videoDiv" id="portal">
  <button class="closeVid">Close</button>
  <!-- <a id="closeVid" href="javascript:reg.closeFrame();">[X]</a> -->
  <iframe src="//player.vimeo.com/video/141358615" width="900" height="600" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen>
  </iframe>
</div>

<div id="agent-portal-reservation">
	<div class="top">
		<div class="title">It's simple, just choose your Portal Name</div>
		<!--<div class="subtitle">Start making money while you sleep</div>-->
		<div class="portalinput">
			<div class="urlinput">www.lifestyledlistings.com/</div>
			<input autocomplete="off" autocorrect="off" spellcheck="false" id="portal" type="text" placeholder="Your portal name here" />
			<button id="check-it">Get Started!</button>
		</div>
		<div class="portalinputmobile">
			<div class="urlinput">lifestyledlistings.com/</div>
			<input autocomplete="off" autocorrect="off" spellcheck="false" id="portal" type="text" placeholder="Your portal name" />
			<button id="check-it">Start</button>
		</div>
		<div class="hiddendiv" style="display:block;"></div>
		<div id="information" style="display:none;">
			<span class="title">Congratulations, your portal is available! To start using it please sign up with us below</span>
			<div id="user-data">
				<div class="left">
					<div class="names">
						<input type="text" id="first_name" name="first_name" placeholder="First Name" value="<?php echo (!empty($seller) && isset($seller->first_name) && !empty($seller->first_name) ? $seller->first_name : ''); ?>" />
						<input type="text" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo (!empty($seller) && isset($seller->last_name) && !empty($seller->last_name) ? $seller->last_name : ''); ?>" />
					</div>
					<input type="text" id="phone" name="phone" placeholder="Mobile Phone" value="<?php echo (!empty($seller) && isset($seller->phone) && !empty($seller->phone) ? $seller->phone : ''); ?>" />
					<input type="text" id="email" name="email" placeholder="Name@example.com" value="<?php echo (!empty($seller) && isset($seller->email) && !empty($seller->email) ? $seller->email : ''); ?>" />
					<input type="text" id="login-id" name="login-id" placeholder="Login Name (Defaults to Email)" value="" />
				</div>
				<div class="right">
					<input type="text" id="breID" placeholder="RE License Number" value="" />
					<input type="text" id="states" placeholder="State" value="" />
					<!--<div class="states"> 
						<span id="states">State</span>&nbsp;
						<select name="States" id="states">
							<option value="-1" selected="selected">Select one</option>
							<?php echo $states; ?>
						</select>
					</div> -->
					<input type="password" id="password" placeholder="Password (At least 8 characters)" value="" />
					<input type="password" id="verify" placeholder="Verify Password" value="" /><span id="marker" class="entypo-attention"></span>
					<input type="text" id="invitation_code" placeholder="Invite Code (optional)" style="display: <?php echo $isFree ? 'none' : 'block'; ?>"/>
				</div>
			</div>
			<div class="reserve-now">
				<div class="reserve-now">
					<button id="buyIt">Setup Your Portal</button>
					<span id="disclaimer" style="display: none;">*Please expect a call from LifeStyled Listings to arrange your portal.<span>
				</div>
			</div>
			<span class="reserveportal" style="display: none;">Learn more about being a <a id="agent-match" href="javascript:;">LifeStyled&#8482; agent</a> in your area!</span>
			<div id="warn-email-usage">
				<span>* Please use the email you associate with your MLS listings, so we can match you with any of your listings that may be on our site!</span>
				<div id="opt-in">
					<input type="checkbox" id="opt-in"/>
					<span>I would like to receive emails with helpful guides and updates related to my account.</span>
				</div>			
			</div>
		</div>
		<div class="whatisportal">
			<div class="left">
				<iframe src="https://player.vimeo.com/video/141358615" style="padding-left: 12%;" width="600" height="334" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<div class="right">
				<span class="title"><span style="color:#F9A054">What</span> is Agent Portal?</span>
				<p>Agent Portal gives your clients a nation wide search that keeps you in touch with them, and their transaction. Your clients search with an agent they trust and you increase your outbound referral business. It's a win-win for you and your clients.</p>
				<p>And perhaps best of all - it's <?php echo $isFree ? 'free!' : "$".$discount." off!"; ?></p>
			</div>
		</div>
	</div>
	<div class="mid">
		<div class="left">
			<span class="title"><span style="color:#F9A054">Why</span> get a Lifestyled Portal?</span>
			<p>Because YOU are important to your clients no matter where they buy their next home.</p>
			<p class="mobilespace">Agent portal is about giving your clients the ability to have someone they trust at their fingertips. <span style="font-weight:400">Whether your clients are shopping for a vacation home, retiring, or just relocating, people who know and trust you can ask you questions or get a second opinion on things at anytime because you are with them the whole way.</span></p>
		</div>
		<div class="divider">
			<div class="line1"></div>
			<span>+</span>
			<div class="line2"></div>
		</div>
		<div class="right">
			<span class="title"><span style="color:#F9A054">What's</span> in it for you?</span>
			<p>How about a 25% referral fee paid to you for sending your network through the portal?</p>
			<p class="mobilespace" style="font-weight:400">You show up on every listing across the US and we track your clients whenever they go through your portal so we can give you credit when they contact another agent on our site. We start the referral process for you automatically, so you simply get an email notifying you that you just sent a referral.</span></p>
		</div>
		<div class="mobilefree">
			<span class="title"><span style="color:#F9A054">Why</span> is it free?</span>
			<p class="mobilespace">Because helping you helps us! The more traffic that you put through our site, the more success we will have!</p>
		</div>
	</div>
	<div class="circle">
		<span class="title">Why are we so different?</span>
		<p>We've found that most consumers don't think of their local agent when considering property out of the area, so we've built a tool that makes sure you get in the loop.</p>
		<p style="width: 60%; margin: 20px auto 0;">Then we get you paid a 25% referral fee for it.</p>
	</div>
	<div class="bottom">
		<div class="sidetext">
			<span class="title"><?php echo $isFree ? 'Why is it free?' : 'Why is it so affordable?' ?></span>
			<p>Because helping you helps us! When you bring your clients through our site, it helps all the agents involved!</p>
		</div>
		<!--<div class="pointer"><p>You show up on every listing nationwide!</p></div>-->
	</div>
	<div class="calltoaction">
		<a href="#"><span class="entypo-up-open-mini"></span> Sound Good? Sign up for free now! <span class="entypo-up-open-mini"></span></a>
	</div>
</div>
