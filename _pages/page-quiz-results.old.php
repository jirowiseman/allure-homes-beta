<?php
require_once(__DIR__.'./../_classes/Tags.class.php'); $Tags = new AH\Tags(); $Tags = $Tags->get(array('sortby'=>'tag'));
require_once(__DIR__.'./../_classes/TagsCategories.class.php'); $TagsCategories = new AH\TagsCategories(); $TagsCategories = $TagsCategories->get();
require_once(__DIR__.'./../_classes/TagsTaxonomy.class.php'); $TagsTaxonomy = new AH\TagsTaxonomy(); $TagsTaxonomy = $TagsTaxonomy->get();
require_once(__DIR__.'./../_classes/SessionMgr.class.php'); $SessionMgr = new AH\SessionMgr();
require_once(__DIR__.'./../_classes/Options.class.php'); $Options = new AH\Options();
require_once(__DIR__.'./../_classes/States.php');

$SessionMgr->log("entered page-quiz-results.php");

function getLastQuiz($session_id) {
	require_once(__DIR__.'./../_classes/QuizActivity.class.php'); $QuizActivity = new AH\QuizActivity();
	// $session_id = $_COOKIE['PHPSESSID'];
	try {
		if (empty($session_id) ) throw new \Exception('No session id provided.');
		$activity = $QuizActivity->get( (object) array('where'=>array('session_id'=>$session_id)) );
		if (empty($activity)) { return false; }
		else {
			$activity = array_pop($activity);
			$size = count($activity->data);

			$a = $activity->data;
			$index = count($a) > 0 ? count($a)-1 : 0;
			$b = &$a[$index];
			$newData = [];
			while ($index >= 0 && !isset($b->results)){
				$index--;
				$b = &$a[$index];
			}
			if (empty($b->results)) return false;		
			// remove all non-result data from the activity..
			for($i = 0; $i <= $index; $i++)
				$newData[] = $a[$i];
			$QuizActivity->set([(object)['where'=>['session_id'=>$session_id],
										'fields'=>['status'=>'done',
												   'data'=>$newData]]]);					
			return $index;
		}
	} catch (\Exception $e) { 
		parseException($e); 
		return false;
	}
}

function validQuizId($index, $session_id) {
	require_once(__DIR__.'./../_classes/QuizActivity.class.php'); $QuizActivity = new AH\QuizActivity();
	//$session_id = $_COOKIE['PHPSESSID'];
	try {
		if (empty($session_id)) throw new \Exception('No session id provided.');
		$activity = $QuizActivity->get( (object) array('where'=>array('session_id'=>$session_id)) );
		if (empty($activity)) { return false; }
		else {
			$activity = array_pop($activity);
			$size = count($activity->data);

			if ($activity->status != 'done')
				return false;

			$a = $activity->data;
			if ( $index >= $size )
				return false;

			$b = &$a[$index];
			if (empty($b->results)) return false;							
			return true;
		}
	} catch (\Exception $e) { 
		parseException($e); 
		return false;
	}
}

foreach ($TagsCategories as &$category){
	$category->tags = array();
	foreach ($TagsTaxonomy as $tax)
		if ($tax->category_id == $category->id)
			foreach ($Tags as $tag) 
				if ($tag->id == $tax->tag_id){ $category->tags[] = $tag; break; }
	usort($category->tags,function($a,$b){ return strcmp($a->tag, $b->tag); });
}
usort($TagsCategories,function($a,$b){ return strcmp($a->category, $b->category); });
$input = get_query_var('id');
$id = 0;
$sessionID = $SessionMgr->getCurrentSession($id);
$quizId = -1;
$hash = '';
$timezone_adjust = 7;

// check if this is a compound input <quizID>:<sessionID>
if (empty($input)) {
	require_once(__DIR__.'./../_classes/Sessions.class.php'); $Sessions = new AH\Sessions();
	$ses = $Sessions->get((object)['where'=>['id'=>$id]]);
	if (!empty($ses) && 
		!empty($ses[0]->quiz_id)) {
		$input = $ses[0]->quiz_id;
		$hash = 'Reentry';
	}
}
elseif (strpos($input, ":")) {
	$input = explode(":", $input);
	$input = $input[0]; // this would be the quizId, row indes from QuizActivity
}
elseif (strpos($input, "-") != false) {
		$parts = explode("-", $input);
		$input = $parts[1];
		
		if (strpos($parts[0][0], "R") !== false) { // run quiz
			$hash = "RunQuiz";
			$referer = isset($_SERVER["HTTP_REFERER"]) ? (!empty($_SERVER["HTTP_REFERER"]) ? strtolower($_SERVER["HTTP_REFERER"]) : "") : '';
			$date = date("Y-m-d H:i:s", time() - ($timezone_adjust*3600));
			global $browser;
			require_once(__DIR__.'./../_classes/Analytics.class.php'); $Analytics = new AH\Analytics();
			$from = '';
			if (strlen($parts[0]) > 1) switch($parts[0][1]) {
				case 'F': $from = "Facebook"; break;
				case 'I': $from = "LinkedIn"; break;
				case 'G': $from = "Google+"; break;
				case 'T': $from = "Twitter"; break;
				case 'M': $from = "Email"; break;
				case 'C': $from = "Contact"; break;
			}
			$Analytics->add(['type'=>ANALYTICS_TYPE_EVENT,
							 'origin'=>'quiz-results',
							 'session_id'=>$id,
							 'referer'=>$referer,
							 'what'=>'sharing',
							 'value_str'=>$from,
							 'value_int'=>$input,
							 'added'=>$date,
							 'browser'=>$browser['shortname'],
							 'connection'=>$browser,
							 'ip'=>$SessionMgr->userIP()]);
		}
		elseif (strpos($parts[0][0], "L") !== false) { // reload from listing
			$hash = 'Reload';
		}
}

is_numeric($input) ? $quizId = $input : $hash = $input;
$incomingQuizId = intval($quizId); // save it, else may get forced to -1

if (isset($quizId) && empty($quizId) && $quizId !== 0 && $quizId !== '0')
	$quizId = $incomingQuizId != -1 ? $incomingQuizId : (isset($_COOKIE['QuizCompleted']) ? $_COOKIE['QuizCompleted'] : -1);
else if ($hash !== 'RunQuiz' &&
		 $hash !== 'Reload' &&
	     $hash !== 'Reentry') {
	$quizId = $incomingQuizId != -1 ? $incomingQuizId : -1;
	if ($incomingQuizId != -1) $hash = 'Reload';
}

$gotBadQuizId = false;

$opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
$noAskAllowMinPrice = !empty($opt) ? intval($opt[0]->value) : 800000;
$opt = $Options->get((object)['where'=>['opt'=>'ListHubFeedSize']]);
$listHubFeedSize = !empty($opt) ? intval($opt[0]->value) : 215000;

$priceList = null;
$priceListMax = null;
$lastPrice = 20000000;
if ($noAskAllowMinPrice < 800000) {
	$base = $noAskAllowMinPrice / 1000;
	$firstPrice = $noAskAllowMinPrice+1;
	$priceList = (object)[$noAskAllowMinPrice=>"Min Price",
						  $firstPrice=>'$'.number_format($base).'K'];
	
	for( $start = $noAskAllowMinPrice+50000; $start <= 800000; $start += 50000)
		$priceList->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceList->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceList->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceList->$start = '$'.number_format($start/1000000).' Million';

	// max price list
	$base = $noAskAllowMinPrice + 100000;
	$priceListMax = (object)[];
	
	for( $start = $base; $start <= 800000; $start += 50000)
		$priceListMax->$start = '$'.number_format($start/1000).'K';

	if ($start > 800000) {// oops skipped 
		$start = 800000;
		$priceListMax->$start = '$'.number_format($start/1000).'K';
	}

	$count = 0;
	for( $start = 1000000; $start < 2000000; $start += 200000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count == 0 ? 0 : 1).' Million';

	$count = 0;
	for( $start = 2000000; $start < 5000000; $start += 500000, $count++)
		$priceListMax->$start = '$'.number_format($start/1000000, $count % 2).' Million';

	for( $start = 5000000; $start < 10000000; $start += 1000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	for( $start = 10000000; $start <= 16000000; $start += 2000000)
		$priceListMax->$start = '$'.number_format($start/1000000).' Million';

	$start = $lastPrice-1;
	$priceListMax->$start = '$'.number_format($lastPrice/1000000).' Million';
	$priceListMax->$lastPrice = 'Max Price';
}

$emailBody = 'Go check out this search results at Lifestyled Listings!  Copy and paste into browser: '.get_home_url().'/quiz-results/RM-%quiz-id%';
$emailSubject = 'Check out this search I did at Lifestyled Listings!';
$emailHref = "mailto:?subject=$emailSubject&body=$emailBody";

$SessionMgr->log("page-quiz-results: outputting page data");
?>
<script type="text/javascript">
	var quiz_id = <?php echo $quizId; ?>;
	var incomingQuizId = <?php echo $incomingQuizId; ?>;
	var hash = '<?php echo empty($hash) ? 0 : $hash; ?>'; 
	setCookie("QuizCompleted", quiz_id, 2);
	console.log("quiz-results.php set quiz_id to "+quiz_id);
	tags_categories = <?php echo json_encode($TagsCategories); ?>;
	ah_local.sessionID = '<?php echo $sessionID; ?>';
	var activePulldown = -1;
	var quizProfile = null;
	var tagsEdited = false;
	var noAskAllowMinPrice = <?php echo $noAskAllowMinPrice; ?>;
	var priceList = <?php echo json_encode($priceList); ?>;
	var priceListMax = <?php echo json_encode($priceListMax); ?>;
	var lastPrice = <?php echo $lastPrice; ?>;
	var listHubFeedSize = <?php echo $listHubFeedSize; ?>;
	var quizPagePer = <?php echo QUIZ_PAGE_PER; ?>;
</script>
<script type="text/javascript">
	
	function setupMobileTagSelector() {
		if (quizProfile == null ||
			  typeof quizProfile.query == 'undefined') {
			window.setTimeout(function() {
				setupMobileTagSelector();
			}, 200);
			return;
		}
		var h = quizProfile.makeTagSelector();
		$('.dropdown .add-tags .add-tags-content').html(h);
		quizProfile.setTagSelectorHandlers();
	}
	
//	function mobileselectMyprofilesetup() {
//		if (quizProfile == null ||
//			  typeof quizProfile.query == 'undefined') {
//			window.setTimeout(function() {
//				mobileselectMyprofilesetup();
//			}, 200);
//			return;
//		}
//		var h = quizProfile.mobileselectMyprofile();
//		$('.dropdown .searchcollapse').html(h);
//		quizProfile.mobileselectMyprofile();
//	}
	
	function mobilecitysetup() {
		if (quizProfile == null ||
			  typeof quizProfile.query == 'undefined') {
			window.setTimeout(function() {
				mobilecitysetup();
			}, 200);
			return;
		}
		var h = quizProfile.mobilecityselector();
		$('.dropdown .my-cities .citiescollapse').html(h);
		quizProfile.setmobilecityselector();
	}
	
	jQuery(document).ready(function($){
		$('.mobile-nav li.edit-tags a.mlink').on('click', function() {
			var val = $('.mobile-nav li.edit-tags').attr('id');
			if ($('.mobile-nav li.edit-tags a.mlink .expandlink').css('display') != 'none') {
				$('.mobile-nav li.edit-tags a.mlink .expandlink').css('display','none');
				$('.mobile-nav li.edit-tags a.mlink .collapselink').css('display','inline-block');
				$('.mobile-nav li.option a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.option a.mlink .collapselink').css('display','none');
				$('.mobile-nav li.add-tags-main a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.add-tags-main a.mlink .collapselink').css('display','none');
				$('.dropdown .add-tags .add-tags-content').empty();
			}
			else {
				$('.mobile-nav li a.mlink .collapselink').css('display','none');
				$('.mobile-nav li a.mlink .expandlink').css('display','inline-block');
			}
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
			}
			else {
				activePulldown = -1;
				//if (tagsEdited)
					//quizProfile.saveTagSelection();
			}
		});
		$('.mobile-nav li.add-tags-main a.mlink').on('click', function() {
			if ($('.mobile-nav li.add-tags-main a.mlink .expandlink').css('display') != 'none') {
				$('.mobile-nav li.add-tags-main a.mlink .expandlink').css('display','none');
				$('.mobile-nav li.add-tags-main a.mlink .collapselink').css('display','inline-block');
				$('.mobile-nav li.edit-tags a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.edit-tags a.mlink .collapselink').css('display','none');
				$('.mobile-nav li.option a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.option a.mlink .collapselink').css('display','none');
			}
			else {
				$('.mobile-nav li a.mlink .collapselink').css('display','none');
				$('.mobile-nav li a.mlink .expandlink').css('display','inline-block');
				$('.dropdown .add-tags .add-tags-content').empty();
			}
			var val = $('.mobile-nav li.add-tags-main').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
				if (val == '3')
					setupMobileTagSelector();
			}
			else {
				activePulldown = -1;
				//if (tagsEdited)
					//quizProfile.saveTagSelection();
			}
		});
		$('.mobile-nav li.option a.mlink').on('click', function() {
			if ($('.mobile-nav li.option a.mlink .expandlink').css('display') != 'none') {
				$('.mobile-nav li.option a.mlink .expandlink').css('display','none');
				$('.mobile-nav li.option a.mlink .collapselink').css('display','inline-block');
				$('.mobile-nav li.add-tags-main a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.add-tags-main a.mlink .collapselink').css('display','none');
				$('.mobile-nav li.edit-tags a.mlink .expandlink').css('display','inline-block');
				$('.mobile-nav li.edit-tags a.mlink .collapselink').css('display','none');
				$('.dropdown .add-tags .add-tags-content').empty();
			}
			else {
				$('.mobile-nav li .collapselink').css('display','none');
				$('.mobile-nav li .expandlink').css('display','inline-block');
			}
			var val = $('.mobile-nav li.option').attr('id');
			if (activePulldown != -1)
				$('.mobile-nav li[id="'+activePulldown+'"] .dropdown').slideToggle('fast');
			if (activePulldown != val) {
				$('.mobile-nav li[id="'+val+'"] .dropdown').slideToggle('fast');
				activePulldown = val;
			}
			else
				activePulldown = -1;
		});
		$('.mobile-nav li.add-tags-main button').on('click', function() {
				quizProfile.saveTagSelection();
				activePulldown = -1;
				$('.mobile-nav li .dropdown').hide();
				$('.mobile-nav .centerarrow').click();
				$('.mobile-nav li a.mlink .collapselink').css('display','none');
				$('.mobile-nav li a.mlink .expandlink').css('display','inline-block');
				$('.dropdown .add-tags .add-tags-content').empty();
		});
	})
</script>
<script type="text/javascript">
								jQuery(document).ready(function($){
									$('.mobile-nav .centerarrow').on('click', function() {
										transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';
										if ($('.resultsnav').hasClass('animatedown')){
											$('#quiz-results article>section.left-col').css('-webkit-overflow-scrolling','touch');
											$('.mobile-nav li a.mlink .collapselink').css('display','none');
											$('.mobile-nav li a.mlink .expandlink').css('display','inline-block');
											activePulldown = -1;
											$('.mobile-nav li .dropdown').hide();
											$('.resultsnav li').removeClass( 'animatein' );
											$('.centerarrow .arrow').removeClass( 'active' );
											$('.centerarrow .arrow .left').removeClass( 'active' );
											$('.centerarrow .arrow .right').removeClass( 'active' );
											timeoutID = window.setTimeout(delayanimateup, 500);
											function delayanimateup() {
												$('.resultsnav').addClass( 'animateup' );
												$('.resultsnav').removeClass( 'animatedown' );
											}
											timeoutID = window.setTimeout(delayanimatehide, 1500);
											function delayanimatehide() {
												$('.resultsnav').removeClass( 'animateup' );
											}
										}
										else {
											$('#quiz-results article>section.left-col').css('-webkit-overflow-scrolling','auto');
											$('.resultsnav').addClass( 'animatedown' );
											$('.resultsnav').removeClass( 'animateup' );
											$('.centerarrow .arrow').addClass( 'active' );
											$('.centerarrow .arrow .left').addClass( 'active' );
											$('.centerarrow .arrow .right').addClass( 'active' );
											timeoutID = window.setTimeout(delayanimatedown, 200);
											function delayanimatedown() {
												$('.resultsnav li').addClass( 'animatein' );
											}
										}
									});
									$('.expand').on('click', function() {
										$('.collapse').slideToggle('fast');
										$('.expand').hide();
										$('.collapsehref').show();
									});
									$('.collapsehref').on('click', function() {
										$('.collapse').slideToggle('fast');
										$('.collapsehref').hide();
										$('.expand').show();
									});
									$('.saveexpand').on('click', function() {
										var saveisDropped = $('.savecollapse').css('display') != 'none';
										if (ah_local.author_id == '0') {	
											return;
											console.log("user wants to register!");
											self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
										}
										else {
											if (getCookie("QuizNeedSaveProfile") == '1' &&
													!saveisDropped) {
												$('.savecollapse').show();
												$('.searchcollapse').hide();
												$('.newcollapse').hide();
											}
											else
												$('.savecollapse').hide();
										}
									});
									$('.searchexpand').on('click', function() {
										var searchisDropped = $('.searchcollapse').css('display') != 'none';
											if (searchisDropped)
												$('.searchcollapse').hide();
											if ((!searchisDropped) && (ah_local.author_id !== '0'))
												$('.mobile-nav li.more .dropdown').slideToggle('fast');
												activePulldown = -1;
												$('.searchcollapse').hide();
												$('.savecollapse').hide();
												$('.newcollapse').hide();
									});
									$('.newexpand').on('click', function() {
										var newisDropped = $('.newcollapse').css('display') != 'none';
										if (ah_local.author_id == '0') {
											return;
											console.log("user wants to register!");
											self.login(redirect, ah_local.wp+'/quiz-results/saveProfile', 'Log in now to save this search.');
										}
										else {
											if (newisDropped)
												$('.newcollapse').hide();
											if (!newisDropped)
												ah.openModal('find-a-home');
												$('.mobile-nav li.more .dropdown').slideToggle('fast');
												activePulldown = -1;
												$('.newcollapse').hide();
												$('.searchcollapse').hide();
												$('.savecollapse').hide();
										}
									});
									$('.citiesexpand').on('click', function() {
										$('.citiescollapse').slideToggle('fast');
										$('.citiesexpand').hide();
										$('.citiescollapsehref').show();
									});
									$('.citiescollapsehref').on('click', function() {
										$('.citiescollapse').slideToggle('fast');
										$('.citiescollapsehref').hide();
										$('.citiesexpand').show();
									});
									$('.view-selector li.city').on('click', function() {
										$('.view-selector li.map').removeClass( 'active' );
										$('.view-selector li.city').addClass( 'active' );
										$('.view-selector-bg .slide').addClass( 'left' );
										$('.view-selector-bg .slide').removeClass( 'right' );
									});
									$('.view-selector li.map').on('click', function() {
										$('.view-selector li.city').removeClass( 'active' );
										$('.view-selector li.listings').removeClass( 'active' );
										$('.view-selector li.map').addClass( 'active' );
										$('.view-selector-bg .slide').addClass( 'right' );
										$('.view-selector-bg .slide').removeClass( 'left' );
									});
									$('.view-selector li.listings').on('click', function() {
										$('.view-selector li.map').removeClass( 'active' );
										$('.view-selector li.listings').addClass( 'active' );
										$('.view-selector-bg .slide').addClass( 'left' );
										$('.view-selector-bg .slide').removeClass( 'right' );
									});
										var listingmaploaded = $('#quiz-results div#listing-map').css('display') == 'block';
										if (listingmaploaded) {
											$('.view-selector .backtocity').hide();
											$('.view-selector .backtolistings').show();
										}
										var citymaploaded = $('#quiz-results div#city-map').css('display') == 'block';
										if (citymaploaded) {
											$('.view-selector .backtocity').hide();
											$('.view-selector .backtolistings').show();
										}
										var citiesloaded = $('#quiz-results div#cities').css('display') == 'block';
										if (citiesloaded) {
											$('.view-selector .backtocity').hide();
											$('.view-selector .backtolistings').hide();
										}
										$('#quiz-results a').on('click', function() {
											timeoutID = window.setTimeout(delaycheck, 750);
											function delaycheck() {
												var listingmaploaded = $('#quiz-results div#listing-map').css('display') == 'block';
												if (listingmaploaded) {
													$('.view-selector .backtocity').hide();
													$('.view-selector .backtolistings').show();
												}
												var citymaploaded = $('#quiz-results div#city-map').css('display') == 'block';
												if (citymaploaded) {
													$('.view-selector .backtocity').show();
													$('.view-selector .backtolistings').hide();
												}
												var citiesloaded = $('#quiz-results div#cities').css('display') == 'block';
												if (citiesloaded) {
													$('.view-selector .backtocity').hide();
													$('.view-selector .backtolistings').hide();
												}
											}
										});
								});
	
</script>

<div id="quiz-results"><main>
	<article>
		<div class="mobile-nav">
			<div class="mobile-nav-header">
				<div class="backbutton">
					<div class="view-selector">
						<div class="largemobile">
							<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back to Cities</a>
							<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back to Listings</a>
						</div>
						<div class="smallmobile">
							<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
							<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
						</div>
					</div>
				</div>
				<div class="centerarrow"><span class="title">Edit Search</span><div class="arrow"><span class="click"><span class="left"></span><span class="right"></span></span></div></div>
				<ul class="view-selector">
					<li class="city leftmost active"><a class="city">City View</a></li>
					<li class="listings" style="display: none;"><a class="list">List View</a></li>
					<li class="map"><a class="map">Map View</a></li>
				</ul>
			</div>
			<ul class="resultsnav">
				<li class="more" id="1">
					<a class="save-profile">Save Search</a>
					<a class="my-profiles">My Searches</a>
					<a class="new-profile">New Search</a>
				</li>
				<li class="option" id="2">
					<a class="mlink">Price and Home Options<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink" style="display:none;"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<div class="quiz-tags">
							<div class="price-slider">
								<div style="margin:0 auto; text-align:center">
									<select name="min" class="price" id="min"></select>
									<select name="max" class="price" id="max" style="margin-right:0!important"></select>
								</div>
							</div>
							<div class="home-options">
								<div style="margin:0 auto; text-align:center">
									<select name="bed" class="option" id="bed"></select>
									<select name="bath" class="option" id="bath" style="margin-right:0!important"></select>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="add-tags-main" id="3">
					<a class="mlink">Add Tags<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink" style="display:none;"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<button class="saveandclose">Save and Apply</button>
						<div class="add-tags">
							<div class="add-tags-content"></div>
						</div>
					</div>
				</li>
				<li class="edit-tags" id="4">
					<a class="mlink">Edit Tags<span class="expandlink"> <span class="entypo-down-open-big"></span></span><span class="collapselink" style="display:none;"> <span class="entypo-up-open-big"></span></span></a>
					<div class="dropdown" style="display: none;">
						<!--<button class="saveandclose">Save and Apply</button>-->
						<div class="editable-tags">
							<span class="title-tags">Feel free to edit and refine.</span>
							<!--<span class="subtitle"><span class="questioncircle"><span>?</span></span> Explain to me how this works.</span>-->
							<div class="quiz-tags">
								<div class="tags-wrap tag-editor">
									<div class="main-header">
										<div class="top">
											<span class="fh-switch" style="position: absolute;margin-left: 5px;">
												<input type="checkbox" id="switch-id" name="myswitch" checked />
												<label for="switch-id" style="cursor:default;"></label>
												<span class="fh-switch-knob"></span>
											</span>
											<span class="sub">Toggle Your &quot;Must Haves&quot;</span>
										</div>
									</div>
									<ul class="tags-list scrollbar scrollbar-left"></ul>
									<div id="actions">
										<button id="undo" disabled >Undo</button>
										<button id="apply" disabled >Apply Changes</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<section class="left-col quiz-listings">
			<div class="results-loading-overlay">
				<p>Loading Your Results...</p>
				<div class="spin-wrap">
					<div class="spinner">
						<div class="cube1"></div>
						<div class="cube2"></div>
					</div>
				</div>
			</div>
			<div class="results-error-overlay">
				<p>No listings were found using current search preferences.  Please adjust your price range, untoggle "Must Haves", and/or change tags and retry</p>
				<div id="right-arrow"><span class="entypo-right-open-mini"></span><span class="entypo-right-open-mini" style="left: 6px;"></span></div>
			</div>
			<ul class="view-selector">
				<div class="largedesktop">
					<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back to Cities</a>
					<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back to Listings</a>
				</div>
				<div class="smalldesktop">
					<a class="backtocity" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
					<a class="backtolistings" style="display:none;"><span class="entypo-left-open-big"></span> Back</a>
				</div>
				<li class="city leftmost active"><a class="city"><span style="display: block; margin: 2px 0 0 0;">City View</span></a></li>
				<li class="listings" style="display: none;"><a class="list"><span style="display: block; margin: 2px 0 0 0;">List View</span></a></li>
				<li class="map"><a class="map"><span style="display: block; margin: 2px 0 0 0;">Map View</span></a></li>
			</ul>
			<div class="view-selector-bg">
				<div class="slide left"></div>
			</div>
			<div id="cities">
				<ul id="cities" class="scrollbar"></ul>
				<span id="loading" style="display:none;">Loading more cities...</span>
			</div>
			<div id="listings" style="display:none;">
				<div class="citynamebanner">
					<!--<table class="tag-icons"></table>-->
					<span class="cityname"></span>
					<span class="score">Lifestyle Match: <span></span></span>
				</div>
				<ul id="listings" class="scrollbar"></ul>
				<span id="loading" style="display:none;">Loading more listings...</span>
			</div>
			<div id="city-map" style="display:none;"></div>
			<div id="listing-map" style="display:none;"></div>
		</section>
		<section class="right-col">
      <!--<header>
        <a href="#" class="help">Help</a>
        <h3 class="title">Search Tools</h3>
      </header>-->
			<ul class="profile-tiles">
				<li class="save-profile">Save Search</li>
				<li class="my-profiles">My Searches</li>
				<li class="new-profile">New Search</li>
			</ul>
			<div id="shareDiv">
				<div id="title">Share Your Results</div>
				<a id="facebook-share" href="javascript:shareSocialNetwork('FB_Q',1,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().'/quiz-results/RF-%quiz-id%'; ?>')" target="_blank"></a>
				<a id="linkedin-share" href="javascript:shareLinkedIn({type:'LI_Q',	comment:'Hi friends check out this search result!', title:'Great listings found at LifeStyled Listings', description:'Find great homes using your lifestyle and favorite things in life!', url:'<?php echo get_home_url().'/quiz-results/RI-%quiz-id%'; ?>', img:'<?php echo get_template_directory_uri().'/_img/backgrounds/header-fb.jpg'; ?>', where: 1});" target="_blank"></a> 
				<a id="googleplus-share" href="javascript:shareSocialNetwork('GP_Q',1,'https://plus.google.com/share?url=<?php echo get_home_url().'/quiz-results/RG-%quiz-id%'; ?>')" target="_blank"></a>
				<a id="twitter-share" href="javascript:shareSocialNetwork('TW_Q',1,'https://twitter.com/share?url=<?php echo get_home_url().'/quiz-results/RT-%quiz-id%'; ?>')" target="_blank"></a>
				<!-- <a id="email-share" href="mailto:%20?subject=<?php echo $emailSubject; ?>&body=<?php echo $emailBody; ?>"></a> -->
				<a id="email-share" href="javascript:shareSocialNetwork('EM_Q',1,'<?php echo $emailHref; ?>');"></a>
			</div>
      		<div class="quiz-tags">
				<div class="selectors">
					<div class="location-mod" style="display:none;position: relative;top: 20px;">
						<table style="margin: -35px auto 30px auto;">		          
								<tr>
									<td><h3 style="font-weight: 300; text-transform: capitalize; font-size: 1em; font-family: 'Open Sans'; letter-spacing: 1px;">Expand search to within</h3></td> 
											<td><select name='distance' id='miles' style="margin: 0 5px;"> 
													<option value='0'>City limits</option>
														<option value='8' selected='selected'>8 miles</option> 
														<option value='15'>15 miles</option> 
														<option value='30'>30 miles</option> 
														<option value='50'>50 miles</option> 
														<option value='80'>80 miles</option> 
														<option value='120'>120 miles</option> 
												</select></td> 
											<td><h3 style="font-weight: 300; text-transform: capitalize; font-size: 1em; font-family: 'Open Sans'; letter-spacing: 1px;">of the city</h3></td> 
								</tr> 
						</table>
					</div>
					<div class="price-slider">
						<div class="minmaxprice">
							<div class='selectBoxmin priceList'>
							<span class='selected'></span>
								<span class='selectArrow'></span>
								<ul id="min" class="prices"></ul>
							</div>
							<div class='selectBoxmax priceList'>
								<span class='selected'></span>
								<span class='selectArrow'></span>
								<ul id="max" class="prices"></ul>
							</div>
						</div>
						<div class="bedbath">
							<div class='selectBed homeOptions'>
								<span class='selected'>BEDS</span>
								<span class='selectArrow'></span>
								<ul class="options" id="beds" class="home"></ul>
							</div>
							<div class='selectBath homeOptions'>
								<span class='selected'>BATHS</span>
								<span class='selectArrow'></span>
								<ul class="options" id="baths" class="home"></ul>
							</div>	
						</div>
					</div>
				</div>
				<div class="lifestyle-profile"><span>Your Lifestyle Profile</span></div>
				<div class="tags-bg">
					<div class="tags-header">
						<span class="title-tags">Feel free to edit and refine.</span>
						<!--<span class="subtitle"><span class="questioncircle"><span>?</span></span> Explain to me how this works.</span>-->
					</div>
					<div class="tags-wrap tag-editor">
						<div class="main-header">
							<!--<div class="add-tags"><span class="entypo-plus-circled"></span> Add More Tags</div>-->
							<div class="top">
								<span class="fh-switch" style="position: absolute;margin-left: 5px;pointer-events: none;">
										<input type="checkbox" id="switch-id" name="myswitch" checked />
										<label for="switch-id" style="cursor:default;"></label>
										<span class="fh-switch-knob"></span>
								</span>
								<span class="sub">Toggle Your &quot;Must Haves&quot;</span>
							</div>
						</div>
						<ul class="tags-list scrollbar scrollbar-left"></ul>
						<div id="actions">
							<div class="add-tags"><span class="entypo-plus-circled" style="margin-right: 2px;"></span> Add More Things</div>
							<button id="apply" disabled >Apply Changes</button>
							<button id="undo" disabled >Undo</button>
						</div>
					</div>
				</div>
			</div>
		</section>
	</article>
</main></div>
