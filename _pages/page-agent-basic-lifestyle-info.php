<?php
require_once(__DIR__.'/../_classes/Listings.class.php');
require_once(__DIR__.'/../_classes/Sellers.class.php');
require_once(__DIR__.'/../_classes/SessionMgr.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Tags.class.php');
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options;
require_once(__DIR__.'/../_classes/States.php');
global $usStates;

$q = new stdClass(); 
$s = new AH\Sellers(1);
$input_var = get_query_var('id');

$amTags = $s->getAMTagList();
$lifestyleOptions = '';
foreach($amTags as $tag_data) {
	$lifestyleOptions .= '<li value="'.$tag_data->id.'">'."$tag_data->tag".'</li>';
}
$tagKeys = array_keys($amTags);

$discount = 0;
$coupon = "Lifestyle10Ok";
$discount = 89;
$tag = 0;
$clearBrowserUrl = false;
if (!empty($input_var)) {
	if (strpos($input_var, "-") != false) {
		$parts = explode("-", $input_var);
		if (strpos($parts[0], 'C') !== false) {
			$clearBrowserUrl = true;
			$opt = $Options->get((object)['where'=>['opt'=>$parts[1]]]);
			$s->log("page-agent-lifestyle-basic-info - opt: ".print_r($opt, true));
			if (empty($opt)) {
				$discount = 89.00;
				$coupon = "Lifestyle10OK";
			}
			else {
				$opt = json_decode($opt[0]->value);
				$discount = $opt->discount;
				$coupon = $opt->coupon;
				if (isset($opt->tag))
					$tag = $opt->tag;
			}
		}
	}
}
else {
	$opt = $Options->get((object)['where'=>['opt'=>'DefaultLifestyleCoupon']]);
	if (!empty($opt)) {
		$s->log("page-agent-lifestyle-basic-info - opt: ".print_r($opt, true));
		$opt = json_decode($opt[0]->value);
		$discount = $opt->discount;
		$coupon = $opt->coupon;
	}
	else
		$s->log("page-agent-lifestyle-basic-info - no DefaultLifestyleCoupon found.");
}

$isFree = $discount == 99;
$kickOut = false;
$wpId = wp_get_current_user()->ID;
if ($wpId == 0) 
	$kickOut = true;
else {
	$q->where = array('author_id'=>$wpId);
	$x = $s->get($q);
	if (empty($x))
		$kickOut = true;
}
if ($kickOut) : // then not registered user nor seller
?>
<script type="text/javascript">
var tag = <?php echo $tag ? $tag : 0; ?>;
var isFree = <?php echo $isFree ? 1 : 0; ?>;
var discount = <?php echo $discount; ?>;
var coupon = '<?php echo $coupon; ?>';
var profileDesc = '';
var item = null;
var service_area = '';
var city_id = 0;
var tag = 0;
var allStates = [];
var sellerEmail = '';

function returnToBase() {
	window.location = ah_local.wp;
}

jQuery(document).ready(function($){
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	// $('.center').css('margin-left', ((windowWidth/2)-40).toString()+'px');
	$('.center').css('margin-top', ((windowHeight/2)-20).toString()+'px');

	ahtb.open({html: "<p>Sorry, only registered agents allowed here</p>",
			   width: 450,
			   height: 180,
			   buttons: [{text:'OK', action:function() {
			   		window.location = ah_local.wp;
			   }}]});

	$('#exitNow').on('click', function() { returnToBase(); })
})
</script>
<section id="main-section">
	<div id="agent-basic-lifestyle-info">
		<div class="center">
			<span>Only registered agents allowed</span>
			<button id="exitNow">Exit Page</button>
		</div>
	</div>
</section>

<?php else:
$user = get_userdata($wpId, 'nickname');

// $nickname = !empty($user) && isset($user->nickname) && !empty($user->nickname) ? $user->nickname : 'unknown';
// $nicename = !empty($user) && isset($user->user_nicename) && !empty($user->user_nicename) ? $user->user_nicename : 'unknown';
// $havePortal = ($nickname != 'unknown' && stripos($nicename, 'unknown') === false) ? PORTAL_VALID : PORTAL_NOT_EXIST;
// $reservedPortalName = '';
$portalMsg = '';
// $s->log("Seller page entered for userID:$wpId, ID:".(!empty($user) ? $user->ID : 0).", nickname:$nickname, nicename:$nicename, havePortal:$havePortal");


$item = null;
$seller = null;
$haveItem = false;
$profileDesc = '';
$visitorNames = ['Unknown']; // first one, with index 0 is unknown always
global $havePortal;

if (!empty($x)) { // will always be true here...
	$x[0]->phone = !empty($x[0]->phone) ? AH\fixPhone($x[0]->phone) : (!empty($x[0]->mobile) ? AH\fixPhone($x[0]->mobile) : '');
	$x[0]->mobile = !empty($x[0]->mobile) ? AH\fixPhone($x[0]->mobile) : '';
	
	$Listings = new AH\Listings();
	$l = $Listings->get((object)['where'=>['author'=>$wpId,
											 // 'listhub_key'=>'notnull',
										   'author_has_account'=>1]]);
	$fromListhub = 0;
	if (!empty($l)) foreach($l as $listing) {
		if (!empty($listing->listhub_key)) {
			$fromListhub++;
		}
		if (!empty($listing->meta)) foreach($listing->meta as $meta)
			if ($meta->action == LISTING_VISITATIONS) {
				$meta->users = (array)$meta->users;
				foreach($meta->users as $id=>$data) {
					if (!empty($id) &&
						!isset($visitorNames[$id])) {// then it's not zero
						$visitor = $s->get((object)['where'=>['author_id'=>$id]]);
						$visitorNames[$id] = !empty($visitor) ? $visitor[0]->first_name.' '.$visitor[0]->last_name : 'Unknown';
						unset($visitor, $data);
					}
				}
			}
	}
	unset($l);

	$x[0]->fromListhub = $fromListhub > 0;
	$amOrder = null;
	$descriptionFromProfile = false;
	if (!empty($x[0]->meta)) {
		$metas = $x[0]->meta;
		$temp = [];
		foreach($metas as &$info) {
			$info = (object)$info;
			$info->action = intval($info->action);
			$temp[$info->action]= $info;
			unset($info);
		}
		unset($metas);
		$metas = $temp;
		$metaIds = [SELLER_AGENT_ORDER, SELLER_AGENT_MATCH_DATA, SELLER_NICKNAME, SELLER_VISITATIONS];

		foreach($metaIds as $mId) {
			if (!isset($metas[$mId]))
				continue; // it's a don't care..

			$meta = $metas[$mId];
			if ($meta->action == SELLER_NICKNAME) {
				$SessionMgr = new AH\SessionMgr;
				$out = $SessionMgr->checkPortalName($meta->nickname);
				if ($out->status == 'OK') {
					$reservedPortalName = $meta->nickname;
					if (isset($meta->flags) &&
						!empty($meta->flags))
						$havePortal = $meta->flags & IC_BOUGHT_PORTAL ? PORTAL_VALID : ($meta->flags & IC_REMOVED ? PORTAL_REMOVED : ($meta->flags & IC_EXPIRED ? PORTAL_EXPIRED : PORTAL_NOT_EXIST));
				}
				else {
					$portalMsg = $out->data;
					if (isset($meta->flags) && $meta->flags & (IC_WITHIN_1 | IC_WITHIN_7 | IC_WITHIN_14 | IC_WITHIN_30))
						$havePortal = PORTAL_EXPIRING;
				}
			}
			elseif ($meta->action == SELLER_VISITATIONS) {
				$meta->users = (array)$meta->users;
				foreach($meta->users as $id=>$data) {
					if (!empty($id) &&
						!isset($visitorNames[$id])) {// then it's not zero
						$visitor = $s->get((object)['where'=>['author_id'=>$id]]);
						$visitorNames[$id] = !empty($visitor) ? $visitor[0]->first_name.' '.$visitor[0]->last_name : 'Unknown';
						unset($visitor, $data);
					}
				}
			}
			elseif ($meta->action == SELLER_AGENT_ORDER) {
				$amOrder = $meta;
				foreach($meta->item as $item) {
					if ($item->type == ORDER_AGENT_MATCH &&
						($item->mode == ORDER_BUYING || // grab first BUYING OR IDLE item
						 $item->mode == ORDER_IDLE) ) {
						$haveItem = true;
						$item->specialty = intval($item->specialty);
						$tag = $item->specialty;
						$item->desc = AH\removeslashes($item->desc);
					}
					if ($haveItem) break;
					unset($item);
				}
			}
			elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
				$found = false;
				$itemUpdated = false;
				foreach($meta->specialties as $existing) {
					$existing = (object)$existing;
					if ($existing->used &&
						in_array($existing->id, $tagKeys)) {
						if ($haveItem) {
							if ($item->specialty == $existing->id) {
								$profileDesc = AH\removeslashes($existing->desc);
								$itemDesc = AH\removeslashes($item->desc);
								if ( strlen($profileDesc) > strlen($itemDesc)) {
									$item->desc = $profileDesc;
									$itemUpdated = $found = true;
								}
							}
						}
						else {
							$found = true;
							$tag = $existing->id;
							$profileDesc = AH\removeslashes($existing->desc);
							$descriptionFromProfile = true;
						}
					}
					unset($existing);
					if ($found) break;
				}
				if (!$found) foreach($meta->lifestyles as $existing) {
					$existing = (object)$existing;
					if ($existing->used &&
						in_array($existing->id, $tagKeys)) {
						if ($haveItem) {
							if ($item->specialty == $existing->id) {
								$profileDesc = AH\removeslashes($existing->desc);
								$itemDesc = AH\removeslashes($item->desc);
								if ( strlen($profileDesc) > strlen($itemDesc)) {
									$item->desc = $profileDesc;
									$itemUpdated = $found = true;
								}
							}
						}
						else {
							$found = true;
							$tag = $existing->id;
							$profileDesc = AH\removeslashes($existing->desc);
							$descriptionFromProfile = true;
						}
					}
					unset($existing);
					if ($found) break;
				}
			}
		} // foreach($metaIds as $mId)
	} // end if (!empty($x[0]->meta))
}

$seller = $x[0]; // will always be true here
if ( $seller->flags & SELLER_IS_PREMIUM_LEVEL_2 &&
	!($seller->flags & SELLER_NEEDS_BASIC_INFO) &&
	$wpId != 8) : // then already LA seller
?>
<script type="text/javascript">
var tag = <?php echo $tag ? $tag : 0; ?>;
var isFree = <?php echo $isFree ? 1 : 0; ?>;
var discount = <?php echo $discount; ?>;
var coupon = '<?php echo $coupon; ?>';
var profileDesc = '';
var item = null;
var service_area = '';
var city_id = 0;
var tag = 0;
var allStates = [];
var sellerEmail = '';

function returnToBase() {
	window.location = ah_local.wp+"/sellers";
}

jQuery(document).ready(function($){
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	// $('.center').css('margin-left', ((windowWidth/2)-40).toString()+'px');
	$('.center').css('margin-top', ((windowHeight/2)-20).toString()+'px');

	ahtb.open({html: "<p>You are already a Lifestyled Agent.</p>",
			   width: 450,
			   height: 180,
			   buttons: [{text:'OK', action:function() {
			   		window.location = ah_local.wp+"/sellers";
			   }}]});

	$('#exitNow').on('click', function() { returnToBase(); })
})
</script>
<section id="main-section">
	<div id="agent-basic-lifestyle-info">
		<div class="center">
			<span>Only non-Lifestyled Agents allowed</span>
			<button id="exitNow">Exit Page</button>
		</div>
	</div>
</section>

<?php else:

$reservedPortalName = '';
if ($havePortal == PORTAL_NOT_EXIST &&
	empty($reservedPortalName) &&
	!empty($x)) { // then no SELLER_NICKNAME, so check if there's a reservation with a portal
	require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations();
	$res = $Reservations->get((object)['where'=>['email'=>$x[0]->email]]);
	if (!empty($res) &&
		!empty($res[0]->portal)) {
		$SessionMgr = new AH\SessionMgr;
		$out = $SessionMgr->checkPortalName($res[0]->portal);
		if ($out->status == 'OK') {
			$reservedPortalName = $res[0]->portal;
			$havePortal = PORTAL_RESERVED;
			$s->log("Reservation:{$res[0]->id} has a valid portal:{$res[0]->portal}");
		}
		else {
			$havePortal = PORTAL_INVALID;
			$portalMsg = $out->data;
			$s->log("Reservation:{$res[0]->id} has an invalid portal:{$res[0]->portal}, why: $out->data");
		}
	}

}

$service_area = '';
$city_id = 0;
require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities;
if (isset($seller->service_areas) &&
	!empty($seller->service_areas)) {
	$areas = explode(":", $seller->service_areas);
	$areas = explode(',', $areas[0]);
	if (count($areas) == 2) {
		$city = trim($areas[0]);
		$state = trim($areas[1]);
		$service_area = $city.', '.$state;
		$city = $Cities->get((object)['where'=>['city'=>$city,
												'state'=>$state]]);
		if (!empty($city))
			$city_id = $city[0]->id;
	}
}

if ($city_id == 0 &&
	isset($seller->city) &&
	isset($seller->state) &&
	!empty($seller->city) &&
	!empty($seller->state)) {
	$service_area = $seller->city.", ".$seller->state;
	$city = $Cities->get((object)['where'=>['city'=>$seller->city,
											'state'=>$seller->state]]);
	if (!empty($city))
		$city_id = $city[0]->id;
}

// if from profile, then add a new order item
if ( $descriptionFromProfile ) {
	$newItem = new \stdClass();
	$newItem->mode = ORDER_BUYING;
	$newItem->type = ORDER_AGENT_MATCH;
	$newItem->desc = addslashes($profileDesc);
	$newItem->location = $city_id;
	$newItem->locationStr = $service_area;
	$newItem->specialty = $tag;
	$newItem->specialtyStr = $amTags[$tag]->tag;
	$newItem->cart_item_key = 0;
	$newItem->order_id = 0;
	if ($amOrder) {
		$amOrder->item[] = $newItem;
	}
	else {
		$amOrder = new \stdClass();
		$amOrder->action = SELLER_AGENT_ORDER;
		$amOrder->order_id_last_purchased = 0;
		$amOrder->order_id_last_cancelled = 0;
		$amOrder->agreed_to_terms = 0;
		$amOrder->item = [$newItem];
	}
	$haveItem = true;
	$item = $newItem;
	$metas = [];
	foreach($seller->meta as $meta)
		if ($meta->action != SELLER_AGENT_ORDER)
			$metas[] = $meta;

	$metas[] = $amOrder;
	$s->set([(object)['where'=>['id'=>$seller->id],
					  'fields'=>['meta'=>$metas]]]);
	$s->log("agent-basic-lifestyle-info - created new ORDER_AGENT_MATCH from profile data for seller:$seller->id");
	unset($metas);
}

$x = ($wpId == 0)  ? "0" :
     (current_user_can("create_users")) ? (!empty($x) ?  json_encode($x[0]) :  "2") :
	 (!empty($x)) ?  json_encode($x[0]) :  "1"; 

$allStates = [];
foreach($usStates as $long=>$state) {
	$allStates[] = (object)['label'=>$long,
							'value'=>$long,
							'id'=>$state];
	$allStates[] = (object)['label'=>$state,
							'value'=>$state,
							'id'=>$state];
}

// $browser = AH\getBrowser();
global $browser;

$Tags = new AH\Tags();
$allTags = $Tags->get();
$y = [];
foreach($allTags as $tag_data)
	$y[$tag_data->id] = $tag_data;

$allTags = $y;
unset($y);

$sellerEmail = !empty($seller) && isset($seller->email) && !empty($seller->email) ? str_replace('"','', str_replace("'",'',$seller->email)) : '';

$showSimplifiedAgentLifestylePages = 0;
$opt = $Options->get((object)['where'=>['opt'=>'ShowSimplifiedAgentLifestylePages']]);
if (!empty($opt))
	$showSimplifiedAgentLifestylePages = AH\is_true($opt[0]->value);

?>
<script type="text/javascript">
var validSeller = <?php echo $x; ?>;
var portalMsg = '<?php echo $portalMsg; ?>';
var reservedPortalName = '<?php echo $reservedPortalName; ?>';
var amTagList = <?php echo json_encode($amTags); ?>;
var allTags = <?php echo json_encode($allTags); ?>;
var clearBrowserUrl = <?php echo $clearBrowserUrl ? 1 : 0; ?>;
var browser = <?php echo json_encode( $browser ); ?>;
var visitorNames = <?php echo json_encode( $visitorNames ); ?>;
var fromListhub = <?php echo $fromListhub; ?>;
var tag = <?php echo $tag ? $tag : 0; ?>;
var profileDesc = '<?php echo rawurlencode($profileDesc); ?>';
var isFree = <?php echo $isFree ? 1 : 0; ?>;
var showSimplifiedAgentLifestylePages = <?php echo $showSimplifiedAgentLifestylePages ? 1 : 0; ?>;
var discount = <?php echo $discount; ?>;
var haveItem = <?php echo $haveItem ? 1 : 0; ?>;
var coupon = '<?php echo $coupon; ?>';
var sellerEmail = "<?php echo $sellerEmail; ?>";
var item = <?php echo $haveItem ? json_encode( $item ) : 0; ?>;
var allStates = <?php echo json_encode($allStates); ?>;
var service_area = "<?php echo $service_area; ?>";
var city_id = <?php echo $city_id ? $city_id : 0; ?>;
</script>

<section id="main-section">
	<div id="agent-basic-lifestyle-info">
		<div id="top">
			<div class="text">
				<span class="title">Welcome to Lifestyled Listings</span>
				<p>Your profile is almost live! Simply upload a profile picture and write a few sentences about why you are an expert in your chosen specialty and we will activate your Lifestyled Agent service.</p>
			</div>
			<div class="hiddendiv"></div>
			<form id="dropzone" class="dropzone" action="#"></form>
			<div class="seller-photo">
				<img src="<?php echo get_template_directory_uri().'/_img/_authors/272x272/'.($seller && $seller->photo != null ? $seller->photo : '_blank.jpg'); ?>" />
				<div class="overlay"><a class="edit-cropping"><p><span class="entypo-popup"></span> Edit Cropping</p></a><a class="upload-photo" id="delete-photo-2"><p><span class="entypo-upload-cloud"></span> Upload New</p></a></div>
				<div id="deleteDiv"><button id="delete-photo">Change photo</button></div>
			</div>
		</div>
		<div id="agent-lifestyle-reservation">
			<div class="top">
				<span class="lifestyletitle">Confirm Your Expertise</span>
				<div class="lifestyleinput">
					<div class="lifestyletags">
						<span class='selectArrow'></span>
						<span class='selected'>Select Lifestyle</span>
						<ul id="tags" style="display: none">
							<?php echo $lifestyleOptions; ?>
						</ul>
					</div>
					<div class="lifestylecity">
						<input autocomplete="off" autocorrect="off" spellcheck="false" id="city" type="text" placeholder="Enter City Name" />
					</div>
				</div>
			</div>
			<div id="information">
				<span class="title">Why are you an expert in this specialty?</span>
				<span class="subtitle">This will be displayed to home buyers.</span>
				<div id="user-data">
					<textarea id="description" placeholder="I am an expert in... (Must be at least 300 characters, this may be edited later)"></textarea>
					<div id="counter">
						<label>Characters Left:</label>&nbsp;<label id="chCount">300</label>
					</div>
				</div>
				<div class="activate-now">
					<div id="moreAction">
						<span id="get-leads" style="margin: 5px 0 6px;"><input type="checkbox" id="agreeToTerms" /> I agree to the <a href="javascript:lifestyleAgentTermsAndConditions()">Terms and Conditions</a></span>
						<span id="get-leads"><input type="checkbox" id="callme" /> *Check this if you would like for us to contact you about claiming more cities or lifestyles.</span>
					</div>
					<button id="activateIt" <?php echo $isFree ? 'style="display: inline;"' : '' ?> ><?php echo $isFree ? 'Activate Now!' : 'Purchase Now!'; ?></button>
					<label id="coupon" <?php echo $isFree ? 'style="display: none;"' : '' ?> >Invite Code/Coupon</label>
					<input type="text" id="coupon" <?php echo $isFree ? 'style="display: none;"' : '' ?> placeholder="Enter coupon/code" <?php echo !empty($coupon) ? 'value="'.$coupon.'"' : ''; ?>></input>
				</div>
			</div>
		</div>
	</div>
	<div id="nextStep">
		<span id="message"></span>
		<span id="messageSmall" class="desktop">Please choose from one of the options below to continue your journey with us.</span>
		<span id="messageSmall" class="mobile">Please choose one of the options below.</span>
		<button id="gotoProfile">My Profile</button>
		<span id="explainWhy" class="desktop">Complete your profile for potential buyers by adding some basic information about you.</span>
		<span id="explainWhy" class="mobile">Complete your profile for potential buyers.</span>
		<button id="gotoLAPage">My Lifestyled Agent</button>
		<span id="explainWhy" class="desktop">Edit your specialties, add more lifestyles, or view yourself on a sample listing.</span>
		<span id="explainWhy" class="mobile">Add/edit lifestyles or view yourself on a listing.</span>
		<button id="gotoDashboard">Continue to dashboard <span class="entypo-right-open-big"></span></button>
		<!--<button id="gotoDashboard">My Seller's Dashboard</button>
		<span id="explainWhy" class="desktop">Choose to edit listings, check out your lifestyle, complete your profile, and more!</span>
		<span id="explainWhy" class="mobile">Edit listings, add Lifestyle, and more!</span>-->
		<!--<button id="gotoAddNewListing">Go add a new listing</button>
		<span id="explainWhy">Create a new listing, upload HD images, add tags, etc..<span>
		<button id="gotoHome">Go to home page</button>-->
	</div>
	<div id="nextStep" class="background"></div>
</section>

<?php endif; ?>
<?php endif; ?>
