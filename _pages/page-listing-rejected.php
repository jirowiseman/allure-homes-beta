<?php
$listing_id = get_query_var('id');
if ( empty($listing_id) ) :
	?>
	<h2>Query not set.</h2>
	<?php
else :

	require_once(__DIR__.'/../_classes/ListingsRejected.class.php'); $Listings = new AH\ListingsRejected();
	$x = $Listings->get((object)array('where'=>array('id'=>get_query_var('id'))));
	$x = array_pop( $x );

	if (!isset($x)) :
		?>
		<h2>Unable to find listing <?php echo get_query_var('id') ?></h2>
		<?php
	else :
		require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
		require_once(__DIR__.'/../_classes/ListingsRejectedTags.class.php'); $ListingsTags = new AH\ListingsRejectedTags();
		require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags(); $Tags = $Tags->get();

		$x->tags = array();
		$ListingsTags = $ListingsTags->get((object)array('where'=>array( 'listing_id'=>$x->id )));
		foreach (wp_list_pluck($ListingsTags, 'tag_id') as $list_tag_id)
			foreach ($Tags as $tag)
				if ($tag->id == $list_tag_id){ $x->tags[] = $tag; break; }

		if ($x->author_has_account) $query = (object) array( 'where'=>array('author_id'=>$x->author) );
		else $query = (object) array( 'where'=>array('id'=>$x->author) );
		$agent = $Sellers->get($query)[0];

		$images = [];
		if (!empty($x->images)) foreach ($x->images as $image)
			$images[] = ["file" => $image->file, "title" => $image->title, "desc" => $image->desc];
?>
<script type="text/javascript">var gallery_images = <?php echo json_encode($images); ?></script>
<div class="listing">
	<a href="<?php bloginfo('wpurl'); ?>/quiz-results/" class="quiz-results"><span class="entypo-left-open-big"></span> &nbsp;Back to Your Results</a>
	<a class="gallery-full"><span class="entypo-resize-full"></span><p>Full Gallery</p><span class="subtitle">With Description</span></a>
	<section>
		<ul class="listing-gallery">
			<?php if ($x->images) foreach ($x->images as $i=>$img){
				if ($i >= 6) break;
				if (substr($img->file, 0, 4 ) == 'http') :
				?><li class="listing-image"><img src="<?php echo $img->file; ?>" /></li><?php
				else :
				?><li class="listing-image"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/_listings/1440x575/<?php echo $img->file; ?>" /></li><?php
				endif;
			} ?>
		</ul>
		<div class="listing-meta">
			<?php
			$listingHTML = '<table>';
			$listingHTML .= '<tr><td class="title" colspan="2">'.strlen($x->title) > 40 ? substr($x->title, 0, 40) : $x->title.'</td></tr>';
			$listingHTML .= '<tr><td class="street_address" colspan="2">'.$x->street_address.'</td></tr>';
			$listingHTML .= '<tr><th>Location</th><td>';
				if ($x->city != '' && $x->city != -1)
					$listingHTML .= $x->city.', ';
				if ($x->state != '' && $x->state != -1)
					$listingHTML .= $x->state;
			$listingHTML .= '</td></tr>';
			$listingHTML .= '<tr><th>Country</th><td>'.$x->country.'</td></tr>';
			$listingHTML .= '<tr><th>Offered Price</th><td>$'.number_format($x->price).'</td></tr>';
			if ($x->lotsize > 0){
				if ($x->lotsize_std == 'meters') $x->lotsize_std = 'm<sup>2</sup>';
				else if ($x->lotsize_std == 'acres') $x->lotsize_std = 'acres';
				else $x->lotsize_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><th>Lot Size</th><td>'.number_format($x->lotsize).' '.$x->lotsize_std.'</td></tr>';
			}
			if ($x->interior > 0){
				if ($x->interior_std == 'meters') $x->interior_std = 'm<sup>2</sup>';
				else if ($x->interior_std == 'acres') $x->interior_std = 'acres';
				else $x->interior_std = 'ft<sup>2</sup>';
				$listingHTML .= '<tr><th>Interior</th><td>'.number_format($x->interior).' '.$x->interior_std.'</td></tr>';
			}
			$listingHTML .= '<tr><th>Beds</th><td>'.$x->beds.'</td></tr>';
			if (substr($x->baths, 0, 1) == '[') {
				$x->baths = json_decode($x->baths);
				$listingHTML .= '<tr><th>Baths (Full / Partial)</th><td>'.$x->baths[0].' / '.$x->baths[1].'</td></tr>';
			} else $listingHTML .= '<tr><th>Baths</th><td>'.$x->baths.'</td></tr>';
			$listingHTML .= '</table>';
			echo $listingHTML;
			?>
			<a href="<?php bloginfo('wpurl'); ?>/explore-the-area/<?php echo $x->id ?>">
				<div class="explore">
					<div class="compass">
						<img class="bottom" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/compass-on.png" style="cursor:pointer" />
						<img class="top" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/compass.png" style="cursor:pointer" />
					</div>
					<p>Explore the Area</p><span class="entypo-right-dir"></span>
				</div>
			</a>
		</div>
	</section>
	<div class="listing-info">
		<div class="listing-about">
			<h3>About</h3>
			<div class="discover-this-listing"><span class="subtitle1">This</span><br/><span class="subtitle2">Listing</span></div>
			<div class="favorite">
				Favorite this home
				<a href="#">
					<img class="bottom" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite.jpg" style="cursor:pointer" />
					<img class="top" src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/favorite-on.jpg" style="cursor:pointer" />
				</a>
			</div>
			<div class="text scrollbar"><?php echo $x->about; ?></div>
		</div>
		<div class="agent">
			<h3>Listing Agent</h3>
			<img src="<?php bloginfo('template_directory'); ?>/_img/_authors/250x250/<?php echo $agent->photo ? $agent->photo : '_blank.jpg' ; ?>" />
			<div class="meta">
				<h4 class="name"><?php echo $agent->first_name.' '.$agent->last_name; ?></h4>
				<span class="phone"><?php strlen($agent->phone) == 11 ? $agent->phone = substr($agent->phone, 1) : null ; echo $agent->phone ? '('.substr($agent->phone, 0, 3).') '.substr($agent->phone, 3, 3).'-'.substr($agent->phone, 6) : ''; ?></span>
				<span class="company"><?php echo $agent->company; ?></span>
			</div>
		</div>
		<img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/matched-agents.png" />
		<a href="<?php bloginfo('wpurl'); ?>/explore-the-area/<?php echo $x->id; ?>">
			<div class="explore">
				<div class="text">
					<span class="title">Discover what's around this listing</span>
					<p>Check out all the local hotspots and activity in the area.</p>
					<p>Click here to discover the world around your listing.</p>
				</div>
			</div>
		</a>
    <!--
		<div class="video">
      <div class="player">
	      <div onclick="thevid=document.getElementById('thevideo'); thevid.style.display='block'; this.style.display='none'"><img src="<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube.jpg" onmouseover="this.src='<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube-on.jpg'" onmouseout="this.src='<?php bloginfo('stylesheet_directory'); ?>/_img/page-listings/youtube.jpg'" style="cursor:pointer" /></div>
				<div id="thevideo" style="display:none">
					<object width="706" height="397"><param name="movie" value="//www.youtube.com/v/3UHOY_VwjOk?rel=0&vq=hd1080;version=3&amp;hl=en_US&amp;autoplay=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/3UHOY_VwjOk?rel=0&vq=hd1080;version=3&amp;hl=en_US&amp;autoplay=1" type="application/x-shockwave-flash" width="706" height="397" allowscriptaccess="always" allowfullscreen="true"></embed>
					</object>
				</div>
			</div>
		</div>
		-->
			<div class="tags">
				<div class="title">Listing Tags</div>
				<ul>
					<?php
						if (!empty($x->tags)){
							sort($x->tags);
							foreach ($x->tags as $tag) echo '<li><span>'.$tag->tag.'</span></li>';
						}
					?>
				</ul>
			</div>
	</div>
</div>
<?php endif; endif; ?>