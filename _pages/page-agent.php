<?php
$input = explode(":", get_query_var('id'));
// $nickname = $input[0];
$id = $input[0];
$listingId = count($input) > 1 ? $input[1] : 0;

if (!is_numeric($id)) { // then a string, name?
	$blogusers = get_users( array( 'search' => $id ) );
	// Array of WP_User objects.
	if (count($blogusers) != 1) {
		echo "<p><h3>Expecting only one user with name: $id, got ".count($blogusers)." instead.</h3></p>";
		echo "<a href=\"javascript:history.go(-1)\";><strong>GO BACK</strong></a>";
		return;
	}
	$author_id = $blogusers[0]->ID;
	$nickname = !empty($blogusers[0]->nickname) ? $blogusers[0]->nickname : $blogusers[0]->user_nicename;
	$nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
}
else {
	$author_id = $id;
	if ( ($user = get_userdata($id)) !== false) {
		$nickname = !empty($user->nickname) ? $user->nickname : $user->user_nicename;
		$nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
	}
}

require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
$seller = $Sellers->get((object)['where'=>['author_id'=>$author_id]]);
if (empty($seller)) {
	echo "<p><h3>Could not find a seller with author_id of $author_id</h3></p>";
	echo "<a href=\"javascript:history.go(-1)\";><strong>GO BACK</strong></a>";
	return;
}

define('MAX_AGENT_CERTIFICATION_PER_ROW', 8);

// $user = array_pop($blogusers);
// require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers();
// $seller = $Sellers->get((object)['where'=>array('author_id'=>$user->ID)]);
$x = !empty($seller) ? json_encode($seller[0]) : "0";
$photo = !empty($seller) ? (!empty($seller[0]->photo) ? $seller[0]->photo : '_blank.jpg') : '_blank.jpg';
$first = !empty($seller) ?  $seller[0]->first_name : "";
$last = !empty($seller) ?  $seller[0]->last_name : "";
$company = !empty($seller) ?  $seller[0]->company : "";
$website = !empty($seller) ?  $seller[0]->website : "";
$phone = !empty($seller) ?  $seller[0]->phone : "";
$phone = AH\fixPhone($phone);
$mobile = !empty($seller) ?  $seller[0]->mobile : "";
$mobile = AH\fixPhone($mobile);

// $mobile = !empty($seller) ?  (!empty($seller[0]->mobile ) ? $seller[0]->mobile : "XXXXXXXXXX") : "XXXXXXXXXX";
// $mobile = AH\fixPhone($mobile);

$user_email = ''; //!empty($seller) ?  $seller[0]->email : "";
$about = !empty($seller) ?  str_replace("\\", "", filter_var($seller[0]->about, FILTER_SANITIZE_STRING)) : '';
if (empty($about)) $about = "Knowledgable agent who can help you find the best homes and assist you in every way.";
$service_areas = !empty($seller) ?  str_replace('"', '&quot;', $seller[0]->service_areas) : ""; /* " just to make quote matching happy */
$service_areas = explode(":", $service_areas)[0];
if (empty($service_areas)) {
	$service_areas = !empty($seller[0]->city) ? $seller[0]->city : '';
	$service_areas.= !empty($seller[0]->state) ? (!empty($service_areas) ? ', ' : '').$seller[0]->state : '';
}
//$specialties = !empty($seller) ?  eregi_replace('/"/g', '&quot;', $seller[0]->specialties) : ""; /* " just to make quote matching happy */
$social = !empty($seller) ? $seller[0]->social : [];
$profileData = null;
$amData = null;
$amOrder = null;
$specialtyTags = [];
$lifestyleTags = [];
$multiLingual = false;
$languageList = '';

if (!empty($seller &&
	!empty($seller[0]->meta))) foreach($seller[0]->meta as $meta) {
	if ($meta->action == SELLER_PROFILE_DATA) {
		$profileData = $meta;
		if (isset($meta->languages) &&
			(count($meta->languages) > 1 ||
			 (count($meta->languages) && $meta->languages[0] != 'English')) ) {
			$multiLingual = true;
			foreach($meta->languages as $lang)
				$languageList .= '<li><span id="lang-desc">'.$lang.'</span></li>';
		}
	}
	elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
		$amData = $meta;
		foreach($meta->specialties as $i=>$tag) {
			$tag = (object)$tag;
			if ($tag->used) {
				$tag->desc = str_replace("\\", "", filter_var($tag->desc, FILTER_SANITIZE_STRING));
				$specialtyTags[$i] = $tag;
			}
			unset($tag);
		}
		foreach($meta->lifestyles as $i=>$tag) {
			$tag = (object)$tag;
			if ($tag->used) {
				$tag->desc = str_replace("\\", "", filter_var($tag->desc, FILTER_SANITIZE_STRING));
				$lifestyleTags[$i] = $tag;
			}
			unset($tag);
		}
	}
	elseif ($meta->action == SELLER_AGENT_ORDER) {
		$amOrder = $meta;
	}
}

$specialtyMatchChild = -1;
$lifestyleMatchChild = -1;
if ($amData && $amOrder) {
	$amOrder->item = (array)$amOrder->item;
		$gotOne = false;
		foreach($amOrder->item as $item) {
			$item = (object)$item;
			if ($item->type == ORDER_AGENT_MATCH &&
				$item->mode == ORDER_BOUGHT) {
				$nthChild = 1;
				$gotOne = true;
				if ($specialtyMatchChild == -1) foreach($amData->specialties as $i=>$tag) {
					$tag = (object)$tag;
					if ($tag->used) {
						if ($tag->id == $item->specialty) {
							$specialtyMatchChild = $nthChild;
						}
						$nthChild++;
					}
					unset($tag);
					if ($specialtyMatchChild != -1) break;
				}

				$nthChild = 1;
				if ($lifestyleMatchChild == -1) foreach($amData->lifestyles as $i=>$tag) {
					$tag = (object)$tag;
					if ($tag->used) {
						if ($tag->id == $item->specialty) {
							$lifestyleMatchChild = $nthChild;
						}
						$nthChild++;
					}
					unset($tag);
					if ($lifestyleMatchChild != -1) break;
				}
			}
			unset($item);
			if ($gotOne) break;
		}
}

if ($profileData &&
	!empty($profileData->contact_email))
	$user_email = $profileData->contact_email;
else
	$user_email = $seller[0]->email;

require_once(__DIR__.'/../_classes/Listings.class.php'); $Listings = new AH\Listings();
$listhub_key = '';
if ($listingId != 0) {
	$l = $Listings->get((object)['where'=>['id'=>$listingId]]);
	if (!empty($l))
		$listhub_key = $l[0]->listhub_key;
}

// portal agent
global $ALR; 
$isPortal = true;
$portalAgent = $ALR->get('portal-agent');
if ($portalAgent == "0")
	$isPortal = false;
?>
<script type="text/javascript">
var validSeller = <?php echo $x; ?>;
var nickname = "<?php echo $nickname; ?>";
var userID = '<?php echo wp_get_current_user()->ID; ?>';
var specialtyTags = <?php echo json_encode((object)$specialtyTags); ?>;
var lifestyleTags = <?php echo json_encode((object)$lifestyleTags); ?>;
var portalAgent = <?php echo $portalAgent != "0" ? json_encode($portalAgent) : "0"; ?>;
var isPortal = <?php echo $isPortal ? 1 : 0; ?>;
var listingId = <?php echo $listingId; ?>;
var listhub_key = '<?php echo $listhub_key; ?>';
var specialtyMatchChild = <?php echo $specialtyMatchChild; ?>;
var lifestyleMatchChild = <?php echo $lifestyleMatchChild; ?>;
</script>

<div id="agent-profile">
	<header>
	<?php if ($listingId) : ?>
        <section class="sub-nav">
            <div class="left">
                <a href="<?php bloginfo('wpurl'); ?>/quiz-results">My Search</a> <span class="entypo-right-open-big"></span>
                <a href="<?php bloginfo('wpurl'); ?>/listing/<?php echo $listingId; ?>">Listing</a> <span class="entypo-right-open-big"></span>
                <a href="#"><?php echo $first; ?> <?php echo $last; ?></a>
            </div>
            <div class="right">
                <a href="#" class="contact-button">Contact Agent</a>
                <!--<a href="#">Save This Listing</a>-->
            </div>
        </section>
     <?php else: ?>
    	<section class="sub-nav">
            <div class="left">
                <a href="<?php bloginfo('wpurl'); ?>">Home</a> <span class="entypo-right-open-big"></span>
								<a href="#"><?php echo $first; ?> <?php echo $last; ?></a>
            </div>
            <div class="right">
								<a href="<?php get_site_url(); ?>/freebird">Disconnect From Portal Agent</a>
                <a href="#" class="contact-button">Contact Agent</a>
            </div>
        </section>
	<?php endif; ?>
	</header>
	<section class="content">
		<div class="page-profile">
			<div class="top">
				<div class="seller-photo">
					<img src="<?php echo get_template_directory_uri(); ?>/_img/_authors/272x272/<?php echo $photo; ?>"/>	
				</div>
				<div class="right-col">
					<div class="name notranslate"><?php echo $first; ?> <?php echo $last; ?></div>
					<div class="company notranslate"><?php echo $company; ?></div>
					<div class="line"></div>
					<div class="phone" style="display: <?php echo empty($mobile) ? "none;" : "block;"?>"><span>Mobile</span>&nbsp;<?php echo $mobile; ?></div>
					<div class="phone" style="display: <?php echo empty($phone) ? "none;" : "block;"?>"><span>Office</span>&nbsp;<?php echo $phone; ?></div>
					<div class="website"><a href="http://<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a></div>
					<table class="contacts">
						<tr>
			<?php if (!empty($user_email))
				echo '<td><a href="#" class="entypo-mail contact-button" style="width:37px;margin-right:21px;"></a><p style="margin-right:21px;">Email</p></td>';
			?>
			<?php if (!empty($social)) foreach($social as $info) {
						switch($info->type) {
							case 'Facebook': 
								echo '<td><a = href="'.$info->value.'" class="entypo-facebook-circled" target="_blank"></a><p>Facebook</p></td>';
								break;
							case 'Google+':
								echo '<td><a = href="'.$info->value.'" class="entypo-gplus-circled" target="_blank"></a><p>Google</p></td>';
								break;
							case 'LinkedIn':
								echo '<td><a = href="'.$info->value.'" class="entypo-linkedin-circled" target="_blank"></a><p>LinkedIn</p></td>';
								break;
							case 'Twitter':
								echo '<td><a = href="'.$info->value.'" class="entypo-twitter-circled" target="_blank"></a><p>Twitter</p></td>';
								break;
						}
					}
			?>
						</tr>
					</table>
				</div> <!-- end right-col -->
			</div> <!-- end top -->
		
			<div class="agent-content">
				<div class="agent-nav">
					<table id="select">
						<tr id="options">
							<td><a href="javascript:;" class="selector topnav" id="overview" for="1">Overview</a></td>
							<td><a href="javascript:;" class="selector topnav" id="agent-match" for="2">Agent Specialties</a></td>
							<!--<td><a href="javascript:;" class="selector topnav" id="local-guide" for="3">Local Guide</a></td>-->
						</tr>
					</table>
				</div> <!-- end agent-nav -->
				<div id="overview">
					<div class="mid">
						<div class="left">
							<span id="about"><?php echo $about ?></span>
						</div>
						<div class="right">
							<div class="qualifications">
		                		<span class="sub-title">Qualifications</span>
		                		<ul id="qualifications">
		                	<?php
		                		$yr = intval(date("Y"));
		                		if ($profileData) {
		                			$multiYear = intval($yr - $profileData->inArea) > 0;
		                 			echo "<li >- Resident for".($multiYear ? " Over ".(intval($yr - $profileData->inArea)+1).' Years' : "1 Year").'</li>';
		                  			echo "<li >- ".(intval($yr - $profileData->year)+1).($multiYear ? ' Years' : ' Year').' of Experience</li>';
		                  			if ($profileData->sold >= 1)
		                  				echo "<li >- ".$profileData->sold.' Transaction'.($profileData->sold > 1 ? 's' : '').' Last Year</li>';
		                  			//$languages = '';
		                  			//if (!empty($profileData->languages)) foreach($profileData->languages as $lang)
		                  			//	$language .= strlen($language) ? ",".$lang : $lang;
				                  	//if ($languages != '')
				                    //	echo "<li >- Business Language(s): ".$languages.'</li>';
		                		} 
		                	?>
		            			</ul>
		              		</div> <!-- end qualifications -->
		              		<?php if ($multiLingual) : ?>
		              		<div class="languages">
		              			<span class="title">Languages</span>
		              			<ul id="languages">
		              			<?php echo $languageList; ?>
			              		</ul>
			              	</div>
		              		<?php endif; ?>
		              		<?php if (!empty($service_areas)) : ?>
							<div class="service-areas">
								<span class="title">Service Areas</span>
								<span class="content notranslate"><?php echo $service_areas; ?></span>
							</div>
							<?php endif; ?>
							<?php if ($multiLingual ||
									  !empty($specialtyTags) ||
									  !empty($lifestyleTags)) : ?>
								<div class="specialties">
									<span class="title">Specialties</span>
									<ul id="specialties">
								<?php
								if ($multiLingual)
									echo '<li><span id="tag-desc">Multi-Lingual</span></li>';
								if (!empty($specialtyTags)) foreach($specialtyTags as $tag)
									echo '<li><span id="tag-desc">'.ucwords($tag->tag).'</span></li>';
								if (!empty($lifestyleTags)) foreach($lifestyleTags as $tag)
									echo '<li><span id="tag-desc">'.ucwords($tag->tag).'</span></li>';
								?>
									</ul>
								</div> <!-- end specialties -->
							<?php endif; ?>
						</div>	<!-- end right -->
					</div> <!-- end mid -->
					<div class="line-bottom"></div>
					<?php if (!empty($seller[0]->associations)) : ?>
					<div class="associations">
						<h1 class="title">Designations and Certifications</h1>
						<?php
							if (empty($seller))
								echo '<span>Not available</span>';
							else {
								echo '<table id="accomplishments" style="margin:3.5em 0 1em;">';
								if (!empty($seller[0]->associations)) {
									$associations = gettype($seller[0]->associations) == 'string' ? json_decode($seller[0]->associations) : $seller[0]->associations;
									$count = 0;
									$gotOne = false;
									foreach($associations as $what=>$is) {
										$icon = get_template_directory_uri().'/_img/_sellers/assoc_'.$what.'.png';
										if ($is) {
											$gotOne = true;
											if (($count % MAX_AGENT_CERTIFICATION_PER_ROW) == 0)
												echo '<tr>';
											echo '<td><img src="'.$icon.'" /></td>';
											$count++;
											if (($count % MAX_AGENT_CERTIFICATION_PER_ROW) == 0)
												echo '</tr>';
										}
									}
									if ($gotOne && ($count % MAX_AGENT_CERTIFICATION_PER_ROW) != 0)
										echo '</tr>';
								}
								echo '</table>';
							}
						?>
					</div> <!-- end associations -->
					<?php endif; ?>
				</div> <!-- end overview -->
				<div id="agentMatch">
					<div class="agent-match" id="specialties">
						<div class="am-header">
							<h1 class="title1">Agent Specialty</h1>
							<h3 class="title2"><?php echo count($specialtyTags) > 1 ? "Click a tag to see " : "See "; ?>what makes <?php echo $first; ?> a specialist on <?php echo count($specialtyTags) > 1 ? "that" : "this"; ?> topic</h3>
						</div>
						<div class="agent-match-nav">
							<table id="select">
								<tr>
						<?php $index = 1; $firstTagId = 0; if (!empty($specialtyTags)) foreach($specialtyTags as $i=>$tag) {
									if ($index == 1 ) $firstTagId = $i;
									echo '<td><a href="javascript:;" class="selector specialties" id="overview" index="'.$index++.'" for="'.$i.'">'.ucwords($tag->tag).'</a></td>';
								}
								else
									echo '<td><a href="javascript:;" class="selector specialties" id="overview" index="'.$index++.'" for="0">Waiting for agent info</a></td>';
						?>
								</tr>
							</table>
						</div> <!-- end agent-match-nav -->
						<div class="about">
							<span id="about"><?php echo !empty($specialtyTags) && count($specialtyTags) ? $specialtyTags[$firstTagId]->desc : ''; ?></span>
						</div>
					</div> <!-- specialties -->
					<div class="agent-match" id="lifestyles">
						<div class="am-header">
							<h1 class="title1">Agent is Knowledgable About</h1>
							<h3 class="title2"><?php echo count($lifestyleTags) > 1 ? "Click a tag to see " : "See "; ?>what makes <?php echo $first; ?> knowledgable about <?php echo count($lifestyleTags) > 1 ? "that" : "this"; ?> topic</h3>
						</div>
						<div class="agent-match-nav">
							<table id="select">
								<tr>
						<?php $index = 1; $firstTagId = 0; if (!empty($lifestyleTags)) foreach($lifestyleTags as $i=>$tag) {
									if ($index == 1 ) $firstTagId = $i;
									echo '<td><a href="javascript:;" class="selector lifestyles" id="overview" index="'.$index++.'" for="'.$i.'">'.ucwords($tag->tag).'</a></td>';
								}
								else
									echo '<td><a href="javascript:;" class="selector lifestyles" id="overview" index="'.$index++.'" for="0">Waiting for agent info</a></td>';
						?>
								</tr>
							</table>
						</div> <!-- end agent-match-nav -->
						<div class="about">
							<span id="about"><?php echo !empty($lifestyleTags) && count($lifestyleTags) ? (gettype($lifestyleTags[$firstTagId]) == 'object' ? $lifestyleTags[$firstTagId]->desc : $lifestyleTags[$firstTagId]['desc']): ''; ?></span>
						</div>
					</div> <!-- lifestyles -->
				</div> <!-- agentMatch -->
			</div> <!-- agent-content -->
		</div> <!-- end page-profile -->
	</section> <!-- content -->
</div> <!-- agent-profile -->