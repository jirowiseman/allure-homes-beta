<?php
// require_once(__DIR__.'../../../../../wp-includes/registration.php');
// require_once(__DIR__.'../../../../../wp-config.php');
// require_once(__DIR__.'../../../../../wp-load.php');
// require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Options.class.php');
require_once(__DIR__.'/../_classes/States.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

// require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'../../../../../wp-config.php');
require_once(__DIR__.'../../../../../wp-load.php');
// require_once(__DIR__.'../../../../../wp-includes/wp-db.php');

//global $wpdb;

$feedback = [
'General',
'Improvement',
'Suggestion',
'Question',
'BugReport'
];

class AJAX_Register extends Controller {
	private $logIt = true;
	// comes from Controller now
	// private $timezone_adjust = -7;

	public function __construct() {
		$in = parent::__construct();
		try {
			if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-register.log') : null;
			$this->log("ajax-register, query: $in->query");
			switch($in->query) {
				case 'email-only-captured':
					if (empty($in->data)) throw new \Exception('No data sent.');					
					if (!isset($in->data['email'])) throw new \Exception('No email sent.');
					if (!isset($in->data['name'])) throw new \Exception('No name sent.');
					if (empty($in->data['agentID'])) throw new \Exception('No agentID sent.');
					if (!isset($in->data['fbId'])) throw new \Exception('No fbId sent.');
					if (!isset($in->data['photo'])) throw new \Exception('No photo sent.');
					if (!isset($in->data['campaign'])) throw new \Exception('No campaign sent.');
					if (!isset($in->data['textMessageOnEmailCapture'])) throw new \Exception('No textMessageOnEmailCapture sent.');

					$email = !empty($in->data['email']) ? filter_var($in->data['email'], FILTER_SANITIZE_EMAIL) : '';
					$origName = $name = html_entity_decode( filter_var($in->data['name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
					$agentID = intval($in->data['agentID']);
					$quizID = isset($in->data['quizID']) ? intval($in->data['quizID']) : 0;
					$sessionID = isset($in->data['sessionID']) ? intval($in->data['sessionID']) : 0;
					$page = isset($in->data['page']) ? $in->data['page'] : 'unknown';
					$event = isset($in->data['event']) ? $in->data['event'] : 'unknown';
					$ip = $this->userIP();
					$fbId = $in->data['fbId'];
					$photo = $in->data['photo'];
					$campaign = !empty($in->data['campaign']) ? $in->data['campaign'] : '';
					$textMessageOnEmailCapture = isset($in->data['textMessageOnEmailCapture']) ? intval($in->data['textMessageOnEmailCapture']) : 0;

					$user = $this->getClass('PortalUsers')->get((object)['where'=>['email'=>$email]]);
					$id = 0;

					$first_name = '';
					$last_name = '';
					if (!empty($name)) {
						$name = explode(' ', $name);
						$x = [];
						foreach($name as $part)
							if (!empty($part))
								$x[] = trim( ucfirst( strtolower($part) ));

						foreach($x as $part)
							if (empty($first_name)) $first_name = $part;
							else $last_name .= empty($last_name) ? $part : ' '.$part;
					}

					$alreadyNotifed = false;
					$alreadyRegistered = false;

					if (empty($user)) {
						$data = ['email'=>$email,
							   	'agent_id'=>$agentID,
							   	'ip'=>$ip,
							   	'ip_orig'=>$ip,
							   	'campaign'=>$campaign,
							   	'campaign_orig' => $campaign,
							   	'session_id'=>$sessionID,
							   	'flags'=>PORTAL_USER_EMAIL_CAPTURED | ($textMessageOnEmailCapture ? PORTAL_USER_EMAIL_CAPTURE_NOTIFIED : 0)];
						if (!empty($first_name))
							$data['first_name'] = $first_name;
						if (!empty($last_name))
							$data['last_name'] = $last_name;
						// these only from FB
						if (!empty($fbId))
							$data['fb_id'] = $fbId;
						if (!empty($photo))
							$data['photo'] = $photo;

						$stamp = date("Y-m-d", time() + ($this->timezone_adjust*3600));
						$siteEntries = new \stdClass();
						$siteEntries->action = SITE_ENTRIES;
						$siteEntries->list = [];
						$siteEntries->list[$page] = (object)[$ip=>[(object)['date'=>$stamp,
															  		   		'count'=>1]]];

						$metas = [$siteEntries];

						if (!empty($campaign)) {
							$agentCampaigns = new \stdClass();
							$agentCampaigns->action = AGENT_CAMPAIGNS;
							$agentCampaigns->list = [];
							$agentCampaigns->list[] = (object)['campaign'=>$campaign,
															   'count'=>1];
							$metas[] = $agentCampaigns;
						}
						$data['meta'] = $metas;

						$id = $this->getClass('PortalUsers')->add((object)$data);
						if (!empty($id))
							$this->getClass('PortalUsersMgr')->updateSeller($agentID, $id); // record it for the portal agent
						$out = new Out('OK', ['id'=>$id]);
					}
					else {
						$id = $user[0]->id;
						$out = new Out('OK', ['id'=>$id]);
						$alreadyNotifed = $user[0]->flags & PORTAL_USER_EMAIL_CAPTURE_NOTIFIED;
						// now check to see if this email belongs to an existing WP user
						$wpUser = get_user_by( 'email', $user[0]->email );
						$fields = [];
						if (!empty($first_name) &&
							(empty($user[0]->first_name) ||
							 $user[0]->first_name != $first_name))
							$fields['first_name'] = $first_name;
						if (!empty($last_name) &&
							(empty($user[0]->last_name) ||
							 $user[0]->last_name != $last_name))
							$fields['last_name'] = $last_name;
						if (!empty($sessionID) &&
							(empty($user[0]->session_id) ||
							 $user[0]->session_id != $sessionID))
							$fields['session_id'] = $sessionID;
						if ( empty($wpUser) &&
							!($user[0]->flags & PORTAL_USER_EMAIL_CAPTURED) )
							$user[0]->flags = $fields['flags'] |= $user[0]->flags | PORTAL_USER_EMAIL_CAPTURED;
						if (!$alreadyNotifed)
							$fields['flags'] |= $user[0]->flags | PORTAL_USER_EMAIL_CAPTURE_NOTIFIED;
						if (!empty($campaign)) {
							if ($user[0]->campaign != $campaign)
								$fields['campaign'] = $campaign;
							if (empty($user[0]->campaign_orig))
								$fields['campaign_orig'] = $campaign;
						}

						$this->getClass('PortalUsersMgr')->updateExistingPortalUserCampaign($user[0], $campaign, $fields);
						
						if (count($fields))
							$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$id],
																		  'fields'=>$fields]]);

						// if we have wpUser and portal user already has a phone number, then this must
						// be a re-visit of a registered user.
						if ($wpUser &&
							!empty($user[0]->phone)) {
							if ($this->getClass('PortalUsersMgr')->loginAsWpUser($wpUser, $user[0], $agentID, $sessionID, $user[0]->phone, $quizID, $out, true)) {
								$out->data['reloadPage'] = 1;
								$out->data['login'] = $wpUser->user_login;
								$alreadyRegistered = true;
								$this->getClass('PortalUsersMgr')->sendEmail($user[0]->id, $agentID, $ip, $page,  PortalUsersMgr::PORTAL_USER_ENTER_SITE);
								$this->log("email-only-captured logged in portalUser:{$user[0]->id}, {$user[0]->first_name} {$user[0]->last_name}");
							}
						}
					}
					if (!empty($id) &&
						$quizID)
						$this->getClass('PortalUsersMgr')->recordPortalUserQuiz($id, $quizID);

					$this->log("email-only-captured - campaign:$campaign, textMessageOnEmailCapture:".($textMessageOnEmailCapture ? 'yes' : 'no').", alreadyNotifed:".($alreadyNotifed ? 'yes' : 'no').", alreadyRegistered:".($alreadyRegistered ? 'yes' : 'no'));

					if (!$alreadyNotifed ) {
						$this->log("calling sendEmail for $id, EMAIL_CAPTURED");
						$this->getClass('PortalUsersMgr')->sendEmail($id, $agentID, $ip, $page, PortalUsersMgr::EMAIL_CAPTURED, $campaign, $textMessageOnEmailCapture);
					}

					$value_str = "event:$event, email:$email, name:".(!empty($origName) ? $origName : 'N/A').", fbId:".(!empty($fbId) ? $fbId : 'N/A').", portalUserID:".(!empty($id) ? $id : 'N/A'); //.", quizID:".(!empty($quizID) ? $quizID : 'N/A');
					$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
					$browser = getBrowser();
					if (!$alreadyNotifed)
						$x = $this->getClass('Analytics', 1)->add(['type'=>ANALYTICS_TYPE_EVENT,
																'origin'=>$page,
																'session_id'=>$sessionID,
																'what'=>'email-only-captured',
																'value_str'=>$value_str,
																'value_int'=>$agentID,
																'value_int2'=>$quizID,
																'added'=>$date,
																'browser'=>$browser['shortname'],
																'connection'=>$browser,
																'ip'=>$ip]);
					break;
				// called from portal-landing or quiz-results page
				case 'new-portal-user':
					if (empty($in->data)) throw new \Exception('No data sent.');					
					if (empty($in->data['name'])) throw new \Exception('No name sent.');
					// if (empty($in->data['email'])) throw new \Exception('No email sent.');
					if (empty($in->data['agentID'])) throw new \Exception('No agentID sent.');
					if (empty($in->data['session_id'])) throw new \Exception('No session_id sent.');
					if (!isset($in->data['emailMode'])) throw new \Exception('No emailMode sent.');
					// if (empty($in->data['phone'])) throw new \Exception('No phone sent.');
					if (!isset($in->data['phoneMode'])) throw new \Exception('No phoneMode sent.');
					if (!isset($in->data['fbId'])) throw new \Exception('No fbId sent.');
					if (!isset($in->data['phone'])) throw new \Exception('No phone sent.');
					if (!isset($in->data['photo'])) throw new \Exception('No photo sent.');
					if (!isset($in->data['campaign'])) throw new \Exception('No campaign sent.');
					if (empty($in->data['thisPage'])) throw new \Exception('No thisPage sent.');

					$name = html_entity_decode( filter_var($in->data['name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
					$agentID = intval($in->data['agentID']);
					$sessionID = intval($in->data['session_id']);
					$emailMode = intval($in->data['emailMode']);
					$email = isset($in->data['email']) ? filter_var($in->data['email'], FILTER_SANITIZE_EMAIL) : '';
					$phone = isset($in->data['phone']) ? $in->data['phone'] : 0;
					$phoneMode = intval($in->data['phoneMode']);
					$event = isset($in->data['event']) ? $in->data['event'] : 'unknown';
					$fbId = $in->data['fbId'];
					$photo = $in->data['photo'];
					$thisPage = $in->data['thisPage'];
					$campaign = !empty($in->data['campaign']) ? $in->data['campaign'] : '';
					$registerUser = $emailMode == PORTAL_LANDING_USE_AS_PASSWORD ||
						 			$phoneMode == PORTAL_LANDING_USE_AS_PASSWORD;

					if ($emailMode > PORTAL_LANDING_OPTIONAL &&
						empty($in->data['email']))
						throw new \Exception('No email sent.');

					if ($phoneMode > PORTAL_LANDING_OPTIONAL &&
						empty($in->data['phone']))
						throw new \Exception('No phone sent.');

					if ($phoneMode == PORTAL_LANDING_USE_AS_PASSWORD &&
						empty($phone) &&
						empty($fbId))
						throw new \Exception('No phone nor FB id supplied for password');

					if ($phoneMode > PORTAL_LANDING_NOT_USED &&
						!empty($phone))
						$registerUser = true;

					$out = $this->getClass('PortalUsersMgr')->addPortalUserFromLanding($name, $email, $phone, $agentID, $sessionID, $registerUser, $thisPage, $fbId, $photo, $campaign);
					if ($out->status == 'OK' &&
						($registerUser)) {
						if ($out->data['wp_user_id'] == 0) {
							$user = get_user_by('login', $out->data['first_name']);
							$password = $emailMode == PORTAL_LANDING_USE_AS_PASSWORD ? $email : (empty($phone) ? $fbId : $phone);
							if ($password == $fbId) {
								$newPassword = '';
								$len = strlen($fbId);
								for($i = 0; $i < $len; $i += 2) {
									$letter = rand(1, 100) % 2;
									$modulus = $letter ? 10 : 26;
									$offset = $letter ? 97 : 48;
									$newPassword .= chr( (intval(substr($fbId, $i, 2)) % $modulus) + $offset );
								}
								$this->log("new-portal-user - fbId:$fbId, password:$newPassword");
								$password = $newPassword;
							}
							$login = !empty($email) ? $email :
									 (!empty($user) ? $out->data['first_name'].$out->data['last_name'] : $out->data['first_name']);
							$user = get_user_by('login', $login);
							if (!empty($user)) { // then make sure we have a unique login with the login
								$try = 1;
								$base = $login;
								while ( !empty($user = get_user_by('login', $login)) ) 
									$login = $base.$try++;
							}
							$regOut = $this->getClass('Register')->basicRegistry($email, 
																				$out->data['first_name'], 
																				$out->data['last_name'], 
																				$password, 
																				$out->data['id'], // overloading between portalUserID and realtor_id
																				$login);
							$out->data['wp_user_id'] = $regOut->data->id; // could be zero, if registration failed
							if (isset($regOut->data->failure_msg)) 
								$out->data['failure_msg'] = $regOut->data->failure_msg;

							$out->data['session_id'] = $regOut->data->session_id;
							$out->data['session'] = $regOut->data->session;
							$out->data['portal'] = $regOut->data->nickname;
						}
					}

					$value_str = "event:$event, status:$out->status, email:$email, name:".(!empty($name) ? $name : 'N/A').", phone:".(!empty($phone) ? $phone : 'N/A').", wp_user_id:".(isset($out->data['wp_user_id']) && !empty($out->data['wp_user_id']) ? $out->data['wp_user_id'] : 'N/A');
					$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
					$browser = getBrowser();
					$ip = $this->userIP();
					$x = $this->getClass('Analytics', 1)->add(['type'=>ANALYTICS_TYPE_EVENT,
															'origin'=>$thisPage,
															'session_id'=>$sessionID,
															'what'=>'new-portal-user',
															'value_str'=>$value_str,
															'value_int'=>$agentID,
															'added'=>$date,
															'browser'=>$browser['shortname'],
															'connection'=>$browser,
															'ip'=>$ip]);

					
					break;

				// called from header.js when we have ANALYTICS_TYPE_PAGE_ENTRY_INITIAL
				// case 'record-portal-user-entry':
				// 	if (empty($in->data)) throw new \Exception('No data sent.');					
				// 	if (empty($in->data['portalUser'])) throw new \Exception('No portalUser sent.');
				// 	if (empty($in->data['agentID'])) throw new \Exception('No agentID sent.');
				// 	if (empty($in->data['session_id'])) throw new \Exception('No session_id sent.');
				// 	if (empty($in->data['thisPage'])) throw new \Exception('No thisPage sent.');

				// 	$portalUserID = intval($in->data['portalUser']);
				// 	$agentID = intval($in->data['agentID']);
				// 	$sessionID = $in->data['session_id'];
				// 	$page = $in->data['thisPage'];

				// 	$out = $this->getClass('PortalUsersMgr')->recordPortalUserEntry($portalUserID, $agentID, $sessionID, $page);
				// 	break;

				// called from register.js, agent-get-lifestyle, agent-get-portal.js
				case 'check-if-listhub-seller':
					if (empty($in->data)) throw new \Exception('No data sent.');					
					if (empty($in->data['email'])) throw new \Exception('No email sent.');
					if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
					if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
					// if (empty($in->data['phone'])) throw new \Exception('No phone sent.');
					// if (empty($in->data['city'])) throw new \Exception('No city sent.');
					// if (empty($in->data['password'])) throw new \Exception('No password sent.');
					if (empty($in->data['login'])) throw new \Exception('No login id sent.');
					// if (empty($in->data['realtor_id'])) throw new \Exception('No realtor_id sent.');
					if (empty($in->data['state'])) throw new \Exception('No state if registry sent.');

					$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
					$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
					$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
					// $password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
   					// $realtor_id = !empty($in->data['realtor_id']) ? filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING) : '';
   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);
   					// $phone = filter_var($in->data['phone'], FILTER_SANITIZE_STRING);
   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
   					// $city = !empty($in->data['city']) ? filter_var($in->data['city'], FILTER_SANITIZE_STRING) : '';

   					$this->log("check-if-listhub-seller for $first_name $last_name, login:$login, email:$email");

   					$user = null;
   					$user = get_user_by( 'login', $login );
   					if (!empty($user)) {
   						$out = new Out('fail', "Sorry, this login:$login is taken.  Please log in if you already have an account");
   						break;
   					}

   					global $usStates;
   					$longState = array_search($state, $usStates);

   					$q = new \stdClass();
   					$q->like = ['last_name'=>$last_name];
   					$q->what = ['first_name', 'last_name', 'city', 'email'];
   					if ($longState) {
   						$q->or = ['state'=>[$state, $longState]];
   						$q->where = ['author_id' => null ];
   					}
   					else
   						$q->where = ['state' => $state,
   									 'author_id' => null];


   					$Sellers = $this->getClass('Sellers', 1);
   					// try to get the broadest list of matching sellers
   					// $sellers = $Sellers->get((object)['where'=>['state' => $state,
   					// 											'author_id' => null],
   					// 								 'like'=>['last_name'=>$last_name],
   					// 								 'what'=>['first_name', 'last_name', 'city', 'email']]);
   					$sellers = $Sellers->get($q);

   					$matches = [];
   					if (empty($sellers)) {
   						$out = new Out('OK', []);
   						break;
   					}

   					$this->log("check-if-listhub-seller - list:".print_r($sellers, true));

   					$exact = 0;
    				$first_name = ucwords(trim($first_name));
    				$last_name = ucwords(trim($last_name));
  					$firstChar = ucfirst($first_name)[0];
   					$email = trim( strtolower($email) );
   					foreach($sellers as $seller) {
   						if (empty($seller->first_name))
   							continue;



   						// found exact match..
   						if ($email == trim(strtolower($seller->email)) &&
   							$first_name == ucwords(trim($seller->first_name)) &&
   							$last_name == ucwords(trim($seller->last_name)) ) {
   							$matches = []; // dump out whatever may have been in there
   							$matches[] = $seller;
   							$exact = 1;
   							break;
   						}

   						$matches[] = $seller;
   						// $this->log("check-if-listhub-seller - adding ".$seller->first_name.' '.$seller->last_name);

   						// $seller->first_name = ucwords(trim($seller->first_name));
   						// if ($first_name == $seller->first_name)
   						// 	$matches[] = $seller;
   						// else if ($email == trim(strtolower($seller->email)))
   						// 	$matches[] = $seller;
   						// else {
   						// 	$testChar = $seller->first_name[0];
   						// 	if($firstChar == $testChar)
   						// 		$matches[] = $seller;
   						// 	else {
   						// 		switch($firstChar) {// reference http://usefulenglish.ru/vocabulary/mens-names, http://usefulenglish.ru/vocabulary/womens-names
   						// 			case 'A': if (in_array($testChar,['B','D','F','G','H','I','L','M','N','S','T','L','V'])) $matches[] = $seller; break; 
   						// 			case 'B': if (in_array($testChar,['F','G','H','L','R','T','W'])) $matches[] = $seller; break; // Bob or Bill matching Robert, William
   						// 			case 'C': if (in_array($testChar,['F','K','L','M','R','S','T','V'])) $matches[] = $seller; break; 
   						// 			case 'D': if (in_array($testChar,['L','N','R'])) $matches[] = $seller; break; 
   						// 			case 'E': if (in_array($testChar,['A','B','D','G','H','L','M','N','R','S','T','V','W'])) $matches[] = $seller; break; 
    					// 			case 'F': if (in_array($testChar,['C','L','R'])) $matches[] = $seller; break; 
    					// 			case 'G': if (in_array($testChar,['B','F','J','T','V'])) $matches[] = $seller; break; 
    					// 			case 'H': if (in_array($testChar,['B','M','N'])) $matches[] = $seller; break; 
    					// 			case 'I': if (in_array($testChar,['B','M'])) $matches[] = $seller; break; 
    					// 			case 'J': if (in_array($testChar,['G','M','N'])) $matches[] = $seller; break; 
    					// 			case 'K': if (in_array($testChar,['C','R'])) $matches[] = $seller; break; 
    					// 			case 'L': if (in_array($testChar,['E','F','N','P','R','V'])) $matches[] = $seller; break; 
    					// 			case 'M': if (in_array($testChar,['A','B','D','G','L','N','P','R','S','T'])) $matches[] = $seller; break; 
    					// 			case 'N': if (in_array($testChar,['C','B','E'])) $matches[] = $seller; break; 
    					// 			case 'O': if (in_array($testChar,['L'])) $matches[] = $seller; break; 
    					// 			case 'P': if (in_array($testChar,['T'])) $matches[] = $seller; break; 
    					// 			case 'Q': break; 
  							// 		case 'R': if (in_array($testChar,['B','D','K','L','N','O'])) $matches[] = $seller; break; // Robert maybe be Bob, Richard to Dick
  							// 		case 'S': if (in_array($testChar,['C','D','F','L','M','V'])) $matches[] = $seller; break; // Robert maybe be Bob, Richard to Dick
    					// 			case 'T': if (in_array($testChar,['D'])) $matches[] = $seller; break; 
    					// 			case 'U': if (in_array($testChar,['S'])) $matches[] = $seller; break; 
    					// 			case 'V': if (in_array($testChar,['G','J','N','R'])) $matches[] = $seller; break; 
   						// 			case 'W': if (in_array($testChar,['B','F','L'])) $matches[] = $seller; break; // William maybe be Bill
     				// 				case 'X': break;
     				// 				case 'Y': if (in_array($testChar,['V'])) $matches[] = $seller; break; 
     				// 				case 'Z': break; 
  							// 	}
   						// 	}
   						// }
   					}

   					$out = new Out('OK', ['matches'=>$matches,
   										  'exact'=>$exact]);
					break;

				case 'verify-seller':
					if (empty($in->data)) throw new \Exception('No data sent.');					
					if (empty($in->data['email'])) throw new \Exception('No email sent.');
					if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
					if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
					if (empty($in->data['phone'])) throw new \Exception('No phone sent.');
					if (empty($in->data['state'])) throw new \Exception('No state if registry sent.');

					$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
					$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
					$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
   					$phone = filter_var($in->data['phone'], FILTER_SANITIZE_STRING);
   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';

   					global $usStates;
   					$longState = array_search($state, $usStates);

   					$q = new \stdClass();
   					$q->likeonlycase = ['email'=>$email];
   					$q->what = ['first_name', 'last_name', 'city', 'email'];
   					if ($longState) {
   						$q->or = ['state'=>[$state, $longState]];
   						$q->where = [	'first_name' => $first_name,
										'last_name' => $last_name,
										'author_id' => null ];
   					}
   					else
   						$q->where = [	'state' => $state,
										'first_name' => $first_name,
										'last_name' => $last_name,
										'author_id' => null ];

					$q->or2 = ['elements' => [['phone'=>$phone],
										   	  ['mobile'=>$phone]] ];

   					$Sellers = $this->getClass('Sellers', 1);
   					// try to get the broadest list of matching sellers
   					// $seller = $Sellers->get((object)['where'=>[	'state' => $state,
   					// 											'first_name' => $first_name,
   					// 											'last_name' => $last_name,
   					// 											'author_id' => null ],
   					// 								 'likeonlycase'=>['email'=>$email],
   					// 								 'or' => ['elements' => [['phone'=>$phone],
								// 		   				 					 ['mobile'=>$phone]] ],
   					// 								 'what'=>['first_name', 'last_name', 'city', 'email']]);
   					$seller = $Sellers->get($q);
   					$out = new Out(!empty($seller) ? 'OK' : 'fail', $seller);
					break;

				// called from ahreg to see if this session is owned by someone
				case 'check-current-session-has-owner':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['session'])) throw new \Exception('No session id sent.');
					$sessions = $this->getClass('Sessions')->get((object)['where'=>['session_id'=>$in->data['session']],
																		  'orderby'=>'updated',
																		  'order'=>'desc']);
					if (empty($sessions))
						$out = new Out('fail', "Session id not found!");
					else {
						if (empty($sessions[0]->user_id))
							$sessions[0]->user_id= '0';
						$userId = is_string($sessions[0]->user_id) ? $sessions[0]->user_id : number_format($sessions[0]->user_id);
						$out = new Out('OK', $userId);
					}
					break;
				// called from header.js during initialization
				case 'get-directive':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['target'])) throw new \Exception('No target sent.');
					$row = 0;
					$portal = 0;
					$source = '';
					$page = "home";
					$version = FE_VERSION;
					if (isset($in->data['portalRow']))
						$row = intval($in->data['portalRow']);
					if (isset($in->data['portal']))
						$portal = intval($in->data['portal']);
					if (isset($in->data['page']))
						$page = $in->data['page'];
					if (isset($in->data['version']))
						$version = $in->data['version'];
					// if (isset($in->data['target2']))
					// 	$source = $in->data['target2'];
					$reg = $this->getClass('SessionMgr');
					// $target = !empty($source) ? $source : $in->data['target'];
					// $this->log("get-directive - source:$source, target:$target");
					$target = $in->data['target'];
					$this->log("get-directive - target:$target, page: $page, version:$version, current:".FE_VERSION);
					$out = $reg->getDirective($target, false, $row, $portal, $page);
					$out->data->version = $version; // current FE version

					// if (!empty($version)) { // if it's empty, it won't know what to do with reboot anyway...
					// 	$incoming = explode('.', $version);
					// 	$current = explode('.', FE_VERSION);
					// 	if (count($incoming) != count($current))
					// 		$out->data->reboot = 1;
					// 	else foreach($incoming as $i=>$v) {
					// 		$v = intval($v);
					// 		$v2 = intval($current[$i]);
					// 		if ($v < $v2) 
					// 			$out->data->reboot = 1;
					// 	}
					// }
					break;
				// called from seller::portal, check if valid portal name
				case 'check-portal-name':
					if (empty($in->data)) throw new \Exception('No data sent.');
					$portal = filter_var($in->data, FILTER_SANITIZE_URL);
					$reg = $this->getClass('SessionMgr');
					$out = $reg->checkPortalName($portal);
					break;
				// called from external, reply to agent contact
				case 'agent-reply':
					if (empty($in->data)) {
						if (empty($_GET['data']))
							throw new \Exception('No activation code sent.');
						else
							$replyTo = $_GET['data'];
					}
					else
						$replyTo = $in->data;

					$reg = $this->getClass('Register');
					$reg->replyTo($replyTo);
					die(); // it should have redirected to a new page or failed to find magic number!
					// echo "magic: $replyTo";
					break;
				// called from ahfeedback
				case 'save-feedback':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['feedback'])) throw new \Exception('No feedback sent.');
					
					$data = [];
					if (!empty($in->data['feedback']['first']))
						$data['first'] = $in->data['feedback']['first'];
					if (!empty($in->data['feedback']['last']))
						$data['last'] = $in->data['feedback']['last'];
					if (!empty($in->data['feedback']['user_email']))
						$data['user_email'] = $in->data['feedback']['user_email'];
					$data['user_id'] = isset($in->data['feedback']['user_id']) ? $in->data['feedback']['user_id'] : 0;
					if (!empty($in->data['feedback']['type']))
						$data['type'] = intval($in->data['feedback']['type']); // general, improvement, suggestion, question
					else
						$data['type'] = 0;
					if (!empty($in->data['flags']))
						$data['flags'] = $in->data['flags']; // MESSAGE_FEEDBACK, MESSAGE_EMAIL_AGENT, MESSAGE_REPLY_TO_AGENT, MESSAGE_REPLY_TO_USER
					else
						$data['flags'] = 0;
					if (!empty($in->data['feedback']['session_id']))
						$data['session_id'] = $in->data['feedback']['session_id'];
					if (!empty($in->data['feedback']['comment'])) {
						// $this->log("comment before stripslashes(): ".$in->data['feedback']['comment']);
						$data['comment'] = stripslashes($in->data['feedback']['comment']);
						// $this->log("comment after stripslashes(): ".$data['comment']);
					}
					if (!empty($in->data['etId']))
						$data['etId'] = intval($in->data['etId']); // id from EmailTracker being replied to
					else
						$data['etId'] = 0;
					$data['listing_agent'] = isset($in->data['listing_agent']) && !empty($in->data['listing_agent']) ? intval($in->data['listing_agent']) : 0;

					$data['user_email'] = isset($data['user_email']) ? filter_var($data['user_email'], FILTER_SANITIZE_EMAIL) : '';

					$msg = ($data['type'] == FB_BUGREPORT ? "BugReport" : "Feedback")." - from: ".$data['first'].(isset($data['last']) ? " ".$data['last'] : '').(isset($data['user_id']) ? ", id:".$data['user_id'] : '');
					$msg.= (!empty($in->data['feedback']['user_email']) ? ", email: ".$in->data['feedback']['user_email'] : '');
					$msg.= ", comment: ".stripslashes( (isset($data['comment']) ? $data['comment'] : ''));

					$address = SUPPORT_EMAIL;

					$reg = $this->getClass('Register');
					$f = $this->getClass('Feedback');

					if (!empty($in->data['flags']) &&
						(intval($in->data['flags']) == MESSAGE_EMAIL_AGENT ||
						 intval($in->data['flags']) == MESSAGE_EMAIL_LISTING_AGENT ||
						 intval($in->data['flags']) == MESSAGE_EMAIL_AGENT_VIA_PORTAL ||
						 intval($in->data['flags']) == MESSAGE_REPLY_TO_AGENT ||
						 intval($in->data['flags']) == MESSAGE_REPLY_TO_USER ||
						 intval($in->data['flags']) == MESSAGE_EMAIL_CLIENT_AS_AGENT ||
						 intval($in->data['flags']) == MESSAGE_EMAIL_PORTAL_AGENT ||
						 intval($in->data['flags']) == MESSAGE_EMAIL_LISTING_OFFICE ) &&
						!empty($in->data['agent_id'])) { 
						//!empty($in->data['listing_id'])) {
						$primary = $this->getClass('Sellers')->get((object)['where'=>['id'=>$in->data['agent_id']]]);
						$listing = null;
						if (empty($primary)) {
							$out = new Out('fail', "Could not get seller:".$in->data['agent_id'] );
							break;
						}
						if (!empty($in->data['listing_id'])) {
							$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$in->data['listing_id']]]);
							if (empty($listing)) {
								$out = new Out('fail', "Could not get listing" );
								break;
							}
						}
						$primary = array_pop($primary);
						$listing = !empty($listing) ? array_pop($listing) : null;
						// $data['seller'] = $seller->first_name." ".$seller->last_name;
						$data['agent_id'] = $in->data['agent_id'];
						$data['agent_id2'] = !empty($in->data['agent_id2']) ? $in->data['agent_id2'] : 0;
						$data['listing_id'] = $in->data['listing_id'];
						// $data['listingAddr'] = $listing->street_address.",".$listing->state;					
						
						$out = $reg->contact((object)$data, $primary, $listing);
						// $f->add($data);
						break;
					}

					$subject = "Feedback";
					$blogType = "Feedback";
					switch($data['type']) {
						case FB_BUGREPORT:
							$subject = "Feedback";
							$blogType = "Feedback";
							break;
						default:
							switch(intval($in->data['flags'])) {
								case MESSAGE_CONTACT_US:
									$subject = "Contact Us";
									$blogType = "Contact Us";
									break;
							}
					}

					global $feedback;
					$sent = $reg->sendMail($address, $subject, $msg, $blogType, "User input:".$feedback[$data['type']] );	
					unset($data['etId']);				
					$x = $f->add($data);
					if (!$x || !$sent) {
						$msg = (!$sent ? "Failed to send feedback mail, " : '').(!$x ? "Failed to add to Feedback table " : '')."for {$feedback[$data['type']]} from {$data['first']} {$data['last']} - {$data['user_email']}, data:".print_r($data, true);
						$reg->sendMail(SUPPORT_EMAIL,
									   "Send Feedback problem",
									   $msg,
									   "Internal Message",
									   "Emailing issue or SQL issue");
						$this->log( $msg );
					}
					// $out = new Out($x ? 'OK' : 'fail', $x ? "Added feedback at row $x" : "Failed to add feedback");
					$out = new Out($sent ? 'OK' : 'fail', $sent ? "Sent feedback" : "Failed to send feedback");
					break;

				case 'get-user-data':
					if (empty($in->data)) throw new \Exception('No data sent.');
					$userId = intval($in->data['user_id']);
					$r = $this->getClass('Register');
					$out = $r->getUserData($userId);
					break;

				// called from header
				case 'logout':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['redirect'])) throw new \Exception('No redirection sent.');
					$redirect = filter_var($in->data['redirect'], FILTER_SANITIZE_URL);
					$ip = $this->userIP();
					$session = session_id();

					$msg = "wp_logout - ip:$ip - session:$session";
					$this->log($msg);
					setcookie('ProfileData', '', time() + (86400 * 1), "/"); // 86400 = 1 day

					$out = new Out('OK', array('redirect'=>$redirect));
					break;
				case 'update-user':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['email'])) throw new \Exception('No email sent.');
					//if (empty($in->data['password'])) throw new \Exception('No password sent.');
					if (empty($in->data['login'])) throw new \Exception('No login id sent.');

					$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
					if (!empty($in->data['password']))
						$password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);

   					$r = $this->getClass('Register');
					$out = $r->updateUser($login, $email, $password);
					break;
				case 'get-user':
					$r = $this->getClass('Register');
					$out = $r->getUser();
					break;
				case 'resend-verification':
					$r = $this->getClass('Register');
					$out = $r->resendVerificaton();
					break;
				case 'vision-enable':
					$o = $this->getClass('Options');
					$q = new \stdClass();
					$data = array('opt'=>'contact',
								  'value'=>json_encode($_SERVER));
					$o->add($data);
					break;
				case 'upgrade-register':
					if (empty($in->data)) throw new \Exception('No data sent.');

					$realtor_id = !empty($in->data['realtor_id']) ? filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING) : '';
   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
   					$inviteCode = filter_var($in->data['inviteCode'], FILTER_SANITIZE_STRING);
   					$special = isset($in->data['special']) ? intval($in->data['special']) : 0;

   					$out = $this->getClass('Register')->getUserData(-1);
   					$allOk = true;
   					if ($out->status == 'OK') {
	   					$reservation = $this->getClass('Reservations')->get((object)['where'=>['email'=> $out->data->email]]);
	   					if (!empty($reservation)) {
	   						if (!empty($reservation[0]->meta)) foreach($reservation[0]->meta as $meta) {
	   							if ($meta->action == SELLER_BRE) {
	   								if ($meta->BRE != $realtor_id) {
	   									$out = new Out('fail', "The realtor license #:$realtor_id is not the same one as in your reservation.");
	   									$allOk = false;
	   									break;
	   								}
	   							}
	   						}
	   					}
	   				}

   					if ($allOk) {
	   					$r = $this->getClass('Register');
						$out = $r->upgradeRegistry($realtor_id, $state, $inviteCode, $special);
						if ($out->status == 'OK') {
							$seller = $this->getClass('Sellers')->get((object)['where'=>['bre'=>$realtor_id]]);
							if (!empty($seller)) {
								$zohoData = null;
								foreach($seller[0]->meta as $meta)
									if ($meta->action == SELLER_ZOHO_LEAD_ID) {
										$zohoData = $meta;
										break;
									}
								$reservation = $this->getClass('Reservations')->get((object)['where'=>['email'=>$seller[0]->email]]);
								if (!empty($reservation)) {
									$reservation[0]->seller_id = $seller[0]->id;
									$oldLeadId = $reservation[0]->lead_id;
									if ($zohoData)
										$reservation[0]->lead_id = $zohoData->lead_id;
									$this->getClass('Zoho')->processOneZohoCrm(false, $reservation[0], $seller[0]);
									$this->log("upgrade-register - calling updateProfileFromReservation for seller:".$seller[0]->id);
									$reservation[0]->lead_id = $oldLeadId; // this will get updated in updateProfileFromReservation if needed.
									updateProfileFromReservation($reservation[0], $seller[0], $this);
								}
							}
						}
					}
					break;

				case 'basic-register':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['email'])) throw new \Exception('No email sent.');
					if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
					if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
					if (empty($in->data['password'])) throw new \Exception('No password sent.');
					//if (empty($in->data['realtor_id'])) throw new \Exception('No realtor_id sent.');
					if (empty($in->data['login'])) throw new \Exception('No login id sent.');
					//if (empty($in->data['state'])) throw new \Exception('No state if registry sent.');

					$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
					$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
					$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
					$password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
   					$realtor_id = !empty($in->data['realtor_id']) ? filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING) : '';
   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);
   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
   					$inviteCode = filter_var($in->data['inviteCode'], FILTER_SANITIZE_STRING);
   					$special = isset($in->data['special']) ? intval($in->data['special']) : 0;
   					$isAdmin = isset($in->data['isAdmin']) ? intval($in->data['isAdmin']) : 0;
   					$optIn = isset($in->data['optIn']) ? (is_true($in->data['optIn']) ? 1 : 0) : 0;
   					$page = isset($in->data['page']) ? $in->data['page'] : 'unknown';
   					$campaign = isset($in->data['campaign']) && !empty($in->data['campaign']) ? $in->data['campaign'] : '';
   					$agentID = isset($in->data['agentID']) ? intval($in->data['agentID']) : 0;
   					$isPhoneNumber = isset($in->data['isPhoneNumber']) ? is_true($in->data['isPhoneNumber']) : 0;

   					$r = $this->getClass('Register');
   					$this->log("basic-register - optIn:$optIn");

					$out = $r->basicRegistry($email, $first_name, $last_name, $password, $realtor_id, $login, $state, $inviteCode, $special, $isAdmin, ORDER_IDLE, $optIn);
					// see if we need to update the reservation's seller id
					if ($out->status == 'OK') {
						$this->log("basic-register - registered $first_name $last_name");
						if ($special) {
							$reservation = $this->getClass('Reservations')->get((object)['like'=>['email'=>$email]]);
							if (!empty($reservation) &&
								empty($reservation[0]->seller_id)) {
								$reservation[0]->seller_id = $out->data->seller_id;
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$out->data->seller_id]]);
								$zohoData = null;
								if (!empty($seller)) foreach($seller[0]->meta as $meta)
									if ($meta->action == SELLER_ZOHO_LEAD_ID) {
										$zohoData = $meta;
										break;
									}

								if (!empty($seller) &&
									!$isAdmin) { // then most likely from agent-purchase-portal page
									$oldLeadId = $reservation[0]->lead_id;
									if ($zohoData) $reservation[0]->lead_id = $zohoData->lead_id;
									$this->getClass('Zoho')->processOneZohoCrm(false, $reservation[0], $seller[0]);
									$this->log("basic-register - calling updateProfileFromReservation for seller:".$seller[0]->id);
									$reservation[0]->lead_id = $oldLeadId; // this will get updated in updateProfileFromReservation if needed.
									updateProfileFromReservation($reservation[0], $seller[0], $this);
								}
								$fields = [];
								$fields['seller_id'] = $reservation[0]->seller_id;
								if ($zohoData) $fields['lead_id'] = $zohoData->lead_id;
								$this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservation[0]->id],
																				'fields'=>$fields]]);
								unset($reservation, $seller, $fields);
							}
						}

						$session_id = 0;
						$session = null;
						$this->getClass('SessionMgr')->getCurrentSession($session_id, $session);
						if (empty($campaign))
							$agent = isset($_COOKIE['AGENT_CAMPAIGN']) && !empty($_COOKIE['AGENT_CAMPAIGN']) ? $_COOKIE['AGENT_CAMPAIGN'] : 
									 (isset($_SESSION['AGENT_CAMPAIGN']) && !empty($_SESSION['AGENT_CAMPAIGN']) ? $_SESSION['AGENT_CAMPAIGN'] : '');
						$this->getClass('PortalUsersMgr')->addPortalUserFromWP($session_id, $out->data->id, $page, $agentID, $campaign, $isPhoneNumber, $password);
					}
					break;

				case 'register-agent':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['email'])) throw new \Exception('No email sent.');
					if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
					if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
					if (empty($in->data['password'])) throw new \Exception('No password sent.');
					if (empty($in->data['realtor_id'])) throw new \Exception('No realtor_id sent.');
					if (empty($in->data['login'])) throw new \Exception('No login id sent.');
					if (empty($in->data['state'])) throw new \Exception('No state of registry sent.');
					if (!isset($in->data['track']) || (empty($in->data['track']) && $in->data['track'] !== 0)) throw new \Exception('No track sent.');

					$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
					$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
					$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
					$password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
   					$realtor_id = filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING);
   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);
   					$state = filter_var($in->data['state'], FILTER_SANITIZE_STRING);
   					$inviteCode = filter_var($in->data['inviteCode'], FILTER_SANITIZE_STRING);
   					$track = intval($in->data['track']);

					$r = $this->getClass('Register');
					$out = $r->createAgent($email, $first_name, $last_name, $password, $realtor_id, $login, $state, $inviteCode);
					if ($out->status == 'OK' &&
						gettype($out->data) == 'object' &&
						$track) {
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$out->data->seller_id]]);
						$reservation = $this->getClass('Reservations')->get((object)['where'=>['id'=>$track]]);
						if (!empty($seller) &&
							!empty($track)) {
							$seller = array_pop($seller);
							$sellerAMData = null;
							if (!empty($seller->meta)) foreach($seller->meta as $meta)
								if ($meta->action == SELLER_AGENT_MATCH_DATA) {
									$sellerAMData = $meta;
									break;
								}

							if (!$sellerAMData) {
								global  $agent_match_specialties;
								global  $agent_match_lifestyles;
								$Tags = $this->getClass('Tags');
								$specialties = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_specialties]]);
								$lifestyles = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_lifestyles]]);
								foreach($specialties as &$tag) {
									$tag->used = 0;
									$tag->desc = '';
								}
								foreach($lifestyles as &$tag) {
									$tag->used = 0;
									$tag->desc = '';
								}
								$sellerAMData = new \stdClass();
								$sellerAMData->action = SELLER_AGENT_MATCH_DATA;
								$sellerAMData->specialties = $specialties;
								$sellerAMData->lifestyles = $lifestyles;
								$seller->meta[] = $sellerAMData;
								// $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
								// 										  'fields'=>['meta'=>$seller->meta]]]);
							}
							$reservation = array_pop($reservation);
							$reservation->id = $seller->id;
							updateProfileFromReservation($reservation, $seller, $this);
						}
					}
				break;

				case 'agent-verified':
					if (empty($in->data)) {
						if (empty($_GET['data']))
							throw new \Exception('No activation code sent.');
						else
							$activate = $_GET['data'];
					}
					else
						$activate = $in->data;

					$r = $this->getClass('Register');
					$r->agentVerify($activate);
					//echo "Got agent-verified with activate: $activate";
					break;

				case 'agent-respond':
					if (empty($in->data)) {
						if (empty($_GET['data']))
							throw new \Exception('No code sent.');
						else
							$respond = $_GET['data'];
					}
					else
						$respond = $in->data;

					$r = $this->getClass('Chimp');
					$r->agentRespond($respond);
					//echo "Got agent-verified with activate: $activate";
					break;

				case 'agent-unsubscribe':
					if (empty($in->data)) {
						if (empty($_GET['data']))
							throw new \Exception('No code sent.');
						else
							$respond = $_GET['data'];
					}
					else
						$respond = $in->data;

					$r = $this->getClass('Chimp');
					$r->agentUnsubscribe($respond);
					//echo "Got agent-verified with activate: $activate";
					break;
			} // end switch
			if (isset($out)) echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}
}

new AJAX_Register();
