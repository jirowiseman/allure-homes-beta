<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class ListingsActivity extends Base {
	public function getParsedByAuthor($sort = 'listing', $id = null){
		if ($id === null) $id = wp_get_current_user()->ID;
		$where = new \stdClass(); $where->format = '%s'; $where->key = 'author'; $where->value = $id;
		$q = new \stdClass(); $q->where = array($where);
		$activity = $this->get($q);
		if ($activity->status == 'OK'){
			$out = array();
			if ($sort == 'listing') foreach ($activity->data as $a){
				if (empty($out[$a->listing_id])) {
					$a->listing = json_decode($a->activity);
					$a->activity = null;
					$out[$a->listing_id] = $a;
				} elseif (empty($out[$a->listing_id]->activity)){
					$b = array('activity'=>json_decode($a->activity),'time'=>$a->time);
					$out[$a->listing_id]->activity = array($b);
				} else {
					$b = array('activity'=>json_decode($a->activity),'time'=>$a->time);
					array_push($out[$a->listing_id]->activity, $b);
				}
			} else $out = $activity->data;
			return new Out('OK', $out);
		} else return $activity;
	}
	private function createOriginalRecord($id = null){ // creates an original record for referencing changes
		if (empty($id)) return new Out(0, 'Activity::createOriginalRecord: no listing id provided.');
		else {
			require_once(__DIR__.'/Listings.class.php');
			$l = new Listings;
			$where = new \stdClass(); $where->key = 'id'; $where->value = $id; $where->format = '%d';
			$q = new \stdClass(); $q->where = array(); $q->where[] = $where;
			$listing = $l->get($q);
			if ($listing->status != 'OK') return $listing; else {
				$listing = $listing->data;
				foreach ($listing as $key=>$val) if ($val == -1) $listing->$key = null;
				$sql = 'INSERT INTO '.getTableName('listing-activity').' (activity,author,listing_id) VALUES (%s,%d,%d)';
				$sql = $this->wpdb->prepare($sql, json_encode($listing), $listing->author, $listing->id);
				// return new Out(0, $sql);
				$x = $this->wpdb->query( $sql );
				if ($x) return new Out('OK','Activity::createOriginalRecord: Created new activity record for listing '.$listing->id.' in DB.');
				else return new Out(0,'Activity::createOriginalRecord: Unable to add new activity record for listing '.$listing->id.' to DB.');
			}
		}
	}
}