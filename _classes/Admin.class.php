<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
class Admin {
	public function __construct($pages = null){
		if ($pages == null) echo __FILE__.': ('.__LINE__.') Admin class requires pages array from Loader';
		else $this->pages = $pages;
	}
	public function __call($method, $args){
		?>
		<style type="text/css">
		#tb-submit button{
			border: 1px solid #ccc;
			border-radius: 5px;
			padding: .5em 1em;
		}
		</style>
		<div class="wrap">
		<?php
			$file = __DIR__.'/../_admin/'.$method.'.php';
			if ($method == 'index'):
				?>
				<style type="text/css">#nav-allure li  { display: inline-block; margin-right:1em; }</style>
				<h2>Allure Homes Admin</h2>
				<p>Welcome to the admin page. Please select a menu item below:</p>
				<ul id="nav-allure">
					<?php
					foreach ($this->pages as $p){
						echo '<li><a href="'.get_bloginfo('wpurl').'/wp-admin/admin.php?page='.$p['slug'].'">'.$p['title'].'</a></li>';
					} ?>
				</ul>
				<?php
			elseif (file_exists($file)) :
				?><h2><?php echo ucwords(str_replace('-',' ',$method)); ?></h2><?php
				require_once($file);
			else : ?>
				<h2>Page Not Found</h2>
				<p>Unable to locate page: "<?php echo $method; ?>"</p>
				<?php
			endif;
			?>
		</div>
			<?php
	}
}