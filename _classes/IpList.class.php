<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/States.php');


class IpList extends Base {
	public function __construct($logIt = 0){
		parent::__construct($logIt);
		// $this->logFile = new Log(__DIR__.'/_logs/QA.log');
		$this->allowCallToip2locationDotcom = false;
	}

	public function getCity($ip) {
		if ($ip == '::1' ||
			$ip == '0' ||
			$ip == '127.0.0.1') {
			return ['city'=>'This PC',
					'state'=>''];
		}
		$city = parent::get((object)['where'=>['ip'=>$ip]]);
		if (!empty($city)) {
			return ['city'=>$city[0]->city,
					'state'=>$city[0]->state];
		}

		$this->log("getCity got ip:$ip");
		$call = "http://ipinfo.io/".$ip;
		$result = @json_decode(file_get_contents($call));
		$this->log("getCity for $ip got back:".(!empty($result) ? print_r($result, true) : "N/A"));

		if (empty($result) ||
			!isset($result->city) ||
			empty($result->city)) {
			if (is_callable(geoip_record_by_name)) {
				$result = geoip_record_by_name($ip);
				$this->log("getCity - geoip_record_by_name for $ip got back:".(!empty($result) ? print_r($result, true) : "N/A"));
				if (!empty($result))
					$result = (object)$result;
				elseif ($this->allowCallToip2locationDotcom) { // last resort, only 20 calls per day as demo user, but highly suspect results
					$raw = file_get_contents( 'http://api.ip2location.com/?' . 'ip='.$ip.'&key=demo' . '&package=WS3&format=json', true );
					$result = @json_decode( $raw );
					if (!empty($result) &&
						isset($result->city_name)) {
						$result->city = $result->city_name;
						$result->region = $result->region_name;
					}
				}
			}
		}

		if( !empty($result) &&
			isset($result->city) &&
			isset($result->region) ) {
			if ( isset($result->country) ||
				 isset($result->country_code)) {
				global $usStates;
				$city = empty($result->city) ? 'Somewhere' : $result->city;
				$country = isset($result->country) ? $result->country : $result->country_code;
				$state = empty($result->region) ? $country : $result->region;
				if ( array_key_exists($result->region, $usStates) )
					$state = $usStates[$result->region];
				elseif ( in_array($result->region, $usStates) )
					$state = $result->region;

				$city = formatName($city, false, true);

				$newIp = ['ip'=>$ip,
						  'city'=>$city,
						  'state'=>$state,
						  'country'=>$country];
				parent::add($newIp);

				return ['city'=>$city,
						'state'=>$state];
			}
		}
		else 
			$this->log("getCity - for $ip did not get good results:".(!empty($result) ? print_r($result, true) : 'empty'));
		return null;
	}

	public function get($in = null){
		if ($in != null) {
			$ip = isset($in->where['ip']) ? $in->where['ip'] : '';
			if ($ip == '::1' ||
				$ip == '0' ||
				$ip == '127.0.0.1') {
				return [(object)['city'=>'This PC',
								 'state'=>'',
								 'country'=>'']];
			}
		}

		$x = parent::get($in);
		return $x;
	}
}