<?php
namespace AH;

// require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'../../../../../wp-config.php');
// require_once(__DIR__.'../../../../../wp-load.php');
// require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/EmailContents.php');
require_once(__DIR__.'/Utility.class.php');

global $unsubscribe;
global $addToContact;
global $unsubscribe_notable;
global $addToContact_notable;
$unsubscribe = '<table><tbody><tr><td><span>If you wish to unsubscribe, click <a href="%unsubscribe%">here</a>.  But you&#39;ll miss out on you great adventure in luxury market.</span></td></tr></tbody></table>';
$addToContact = "<table><tbody><tr><td><span>Please take a moment to add %admin% to your <strong>contact list</strong>.</span></td><td><span>Don&#39;t miss any of our exciting updates and promotions!</span></td></tr></tbody></table>";
$unsubscribe_notable = '<p><span>If you wish to unsubscribe, click <a href="%unsubscribe%">here</a>.  But you&#39;ll miss out on you great adventure in luxury market.</span></p>';
$addToContact_notable = "<p><span>Please take a moment to add %admin% to your <strong>contact list</strong>.  </span><span>Don&#39;t miss any of our exciting updates and promotions!</span></p>";

class Email extends Controller {
	// comes from Controller now
	// protected $timezone_adjust = 7;
	
	public function __construct($logIt = 0) {
		parent::__construct();
		$this->debugLevel = 3;
		$o = $this->getClass('Options');
		$x = $o->get((object)array('where'=>array('opt'=>'EmailClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/email.log') : null;	
		$this->lockfile = fopen(__DIR__.'/_logs/lockEmail.lock', 'c'); 

		set_time_limit(0);	
	}

	public function lock() {
		if ($this->lockfile !== null)
			flock($this->lockfile, LOCK_EX);
	}
	public function unlock() {
		if ($this->lockfile !== null)
			flock($this->lockfile, LOCK_UN);
	}

	public function textMessage($phoneNumber, $subject, $msg) {
		$this->log("textMessage - entered for $phoneNumber");
		$carrier_list = [	'att'           => 'txt.att.net',
					        'verizon'       => 'vtext.com',
					        'tmobile'       => 'tmomail.net',
					        'sprint'        => 'messaging.sprintpcs.com',
					        'nextel'        => 'messaging.nextel.com',
					        'pcs'			=> 'pcsone.net',
					        'metropcs'		=> 'mymetropcs.com',
					        'comcast'		=> 'comcastpcs.textmsg.com',
					        'cellularone'	=> 'mobile.celloneusa.com'
					    ];
		$this->log("textMessage - carrier_list:".print_r($carrier_list, true));


		$headers = 	'From: '. SUPPORT_EMAIL . "\r\n" .
				    'Reply-To: '. SUPPORT_EMAIL . "\r\n" .
				    'X-Mailer: PHP/' . phpversion();

		$this->log("textMessage - header:".print_r($headers, true));
		$sentTo = '';
		foreach($carrier_list as $carrier)
			$sentTo .= (!empty($sentTo) ? ',' : '').$phoneNumber."@$carrier";

		$this->log("textMessage - sending to:$sentTo, msg:$msg");
		$result = mail($sentTo, $subject, $msg, $headers);
		$this->log("textMessage - sentTo:$sentTo, msg:$msg was ".($result ? 'successful' : 'failed'), 3);

		return $result;
	}

	public function sendMail($email, $subject, $msg, 
								$blogname = 'Welcome!', 
								$blogdescription = "You're now part of the best lifestyled listings revolution!",
								$attachments = []) {
		// $user = wp_get_current_user();

		// global $html_body;
		// $html_body = str_replace('%blog_url%', get_home_url(), $html_body);
		// $html_body = str_replace('%blog_name%', $blogname, $html_body);
		// $html_body = str_replace('%blog_description%', $blogdescription, $html_body);
		// $html_body = str_replace('%admin_email%', SUPPORT_EMAIL, $html_body);
		// $html_body = str_replace('%content%', $msg, $html_body);
		// $html_body = str_replace('%date%', date('Y-m-d'), $html_body);
		// $html_body = str_replace('%time%', date('h:i:s'), $html_body);

		$this->log("recipient:$email, subject:$subject, SUPPORT_EMAIL:".SUPPORT_EMAIL.", SALES_EMAIL:".SALES_EMAIL, 3);


		update_option( 'blogurl', get_home_url());
		update_option( 'blogname', $blogname);
		update_option( 'blogdescription', $blogdescription);
		update_option( 'admin_email', SUPPORT_EMAIL); 

		$headers = ['From: Lifestyled Listings <'.SUPPORT_EMAIL.'>' . "\r\n"]; //,
					// 'Content-Type: text/html; charset=UTF-8' . "\r\n",
					// 'Content-Transfer-Encoding: 8bit'."\r\n"];
	  	$success = wp_mail( $email, $subject, $msg, $headers, $attachments); 

	  	update_option( 'blogname', 'Welcome!');
	  	update_option( 'blogdescription', 'The best lifestyled listing site!');
	  	return $success;
	}

	public function sendLeadEmail($portalUserName,
								  $portalUserEmail,
								  $portalUserPhone,
								  $email, $subject, $msg, 
								  $blogname = 'Welcome!', 
								  $blogdescription = "You're now part of the best lifestyled listings revolution!",
								  $extra = '') {

		$this->lock();

		update_option('leadname', $portalUserName);
		update_option('leademail', $portalUserEmail);
		update_option('leadphone', $portalUserPhone);
		update_option('leadmessage', $extra);

		$retval = $this->sendMail($email, $subject, $msg, 
								$blogname, 
								$blogdescription);

		update_option('leadname', 'undefined');
		update_option('leademail', 'undefined');
		update_option('leadphone', 'undefined');
		update_option('leadmessage', 'undefined');

		$this->unlock();
		return $retval;
	}

	public function sendTestEmail($author_id) {
		$s = $this->getClass('Sellers');
		$seller = $s->get((object)['where'=>['author_id'=>$author_id]]);
		if (empty($seller))
			return new Out('fail', 'Could not find seller with id:'.$author_id);

		$seller = array_pop($seller);
		$email = $seller->email;
		if (!empty($seller->meta)) foreach($seller->meta as $info) {
			if ($info->action == SELLER_PROFILE_DATA) {
				if (!empty($info->contact_email))
					$email = $info->contact_email;
				break;
			}
		}

		$retval = $this->sendMail($email, "Testing", "This is a test email from Lifestyled Listings!",
										"Email test",
										"Hope you got this! :)");
		return new Out($retval ? 'OK' : 'fail', $retval ? "Sent test email to $email" : "Failed to send teset email to $email");
	}

	public function getFile($what) {
		switch($what) {
			case PROMO_FIRST: 
			case LLC_INTRO: return __DIR__.'/../_mail/_content/basic_intro.html.trim';
			case PROMO_SECOND:
			case LLC_FOLLOWUP: return __DIR__.'/../_mail/_content/agent_match_reservation.html.trim';
			case PROMO_THIRD:
			case PROMO_FOURTH:
			case LLC_COMPLEX_FOLLOWUP: return __DIR__.'/../_mail/_content/agent_match_reservation_with_graphics.html.trim';
			case IMPROVE_LISTING: return __DIR__.'/../_mail/_content/improve_listing.html.trim';
			case LLC_NO_TABLE_INTRO: return __DIR__.'/../_mail/_content/all_text_intro.html';
			case PORTAL_PROMO_1: 
			case PORTAL_TOOLS_PKG: 
			case PORTAL_PROMO_CONTENT_TEST: return __DIR__.'/../_mail/_content/basic_portal_promo.html.trim';
		}
	}

	protected function getSellerEmailDb($seller, $smtpServer = null) {
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'host'=>strtolower($email[1])];
				if ($smtpServers != null)
					$db->smtp_id = $smtpServer->id;
				$msg = "getSellerEmailDb - added sellerId: {$seller->id}, email:{$seller->email}";
				$this->log($msg, 3);
				$id = $EmailDb->add($db);
				unset($db);
				return $EmailDb->get((object)['where'=>['id'=>$id]])[0];
			}
			else
				return false;
		}
		else if (!empty($seller->email))
			return $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
		else
			return false;
	}

	// this function cannot be used with buildStats() :(
	protected function presortTarget(&$targetList) {
		$sql = "SELECT a.id, a.email, a.meta, b.smtp_id, c.name0 FROM {$this->getClass('Sellers')->getTableName()} AS a ";
		$sql.= "INNER JOIN {$this->getClass('Sellers')->getTableName('sellers-email-db')} AS b ON a.id = b.seller_id ";
//		$sql.= "INNER JOIN {$this->getClass('Sellers')->getTableName('smtp-server')} AS c ON b.smtp_id = c.id ";
		$sql.= "WHERE %idList%";
		$sql.= "ORDER BY b.smtp_id ASC";
		$where = '';
		foreach($targetList as $user_id) {
			$user_id = (object)$user_id;
			$where .= (strlen($where) ? ' OR ' : '(')."a.id = $user_id->id";
		}
		$where .= ') ';
		$sql = str_replace('%idList%', $where, $sql);
		$theList = $this->getClass('Sellers')->rawQuery($sql);
		$this->log("presortTarget - ".count($theList)." objects returned from sql: $sql", 3);

		// find google smtp
		// $goog = $this->getClass('SmtpServer')->rawQuery("SELECT * FROM {$this->getClass('Sellers')->getTableName('smtp-server')} WHERE `name0` LIKE 'google.com' OR `name1` LIKE 'google.com' OR `name0` LIKE 'googlemail.com' OR `name1` LIKE 'googlemail.com'");
		// if (empty($goog))
		// {
		// 	$targetList =$theList;
		// 	return;
		// }
		// $goog = array_pop($goog);

		// usort($theList, function($a, $b) {
		// 	if ($a->smtp_id == $goog->id &&
		// 		$b->smtp_id == $goog->id)
		// 		return 0;

		// 	if ($a->smtp_id == $goog->id)
		// 		return 1;

		// 	return -1;
		// });
		unset($targetList);
		usort($theList, function($a, $b) {
			return $a->smtp_id < $b->smtp_id ? -1 : ($a->smtp_id == $b->smtp_id ? 0 : 1 );
		});

		$targetList = $theList;
		$this->log("presortTarget - ".print_r($targetList, true), 3);
	}

	protected function useSmtpServer($smtpServer, $hosts) {
		if ($smtpServer == null)
			return false;

		foreach(['name0','name1','name2'] as $index)
			if ( isset($smtpServer->$index) && in_array($smtpServer->$index, $hosts) )
				return true;
		return false;
	}

	public function buildStats($id, $what, $targetList, &$stats) {
		try {
			$sql = '';
			$exceptions = [];
			$today = date('Y-m-d');
			$this->log("buildStats for $id, $what on $today for ".(!empty($targetList) ? count($targetList) : 0)." users", 3);

			$access = $this->getClass('SmtpAccessCount', 1)->get((object)['where'=>['flags'=>EMAIL_STATS]]);
																					//'date'=>$today]]);
			// reset counters
			if (!empty($access)) {
				$ids = [];
				foreach($access as $data)
					$ids[] = $data->id;
				$this->getClass('SmtpAccessCount')->set([(object)['where'=>['id'=>$ids],
																  'fields'=>['count'=>0]]]);
			}

			$smtpServers = [];

			//$this->presortTarget($targetList);
			$smtpServer = null;
			$access = null;
			$reuse = false;

			foreach($targetList as $user_id) {
			// foreach($targetList as $user) {
				$user_id = (object)$user_id;
				// $user = (object)$user;
				$user = $this->getClass('Sellers')->get((object)['where'=>['id'=>$user_id->id]]);
				if (!empty($user)) {
					$user = array_pop($user);
					$email = $user->email;
					if ( isset($user->meta) &&
					 	!empty($user->meta) ) {
						// $user->meta = json_decode($user->meta);
					 	foreach($user->meta as $meta) {
							if ($meta->action == SELLER_PROFILE_DATA &&
								!empty($meta->contact_email) )
								$email = $meta->contact_email;
						}
					}

					if (empty($email) ||
						count( explode('@', $email) ) != 2) {
						$this->log("buildStats - failed for sellerId: $user->id, for $id, $what - no email address", 3);
						$msg = "No email address for sellerId:$user->id.";
						if (!in_array($msg, $exceptions))
							$exceptions[] = $msg;
						continue;
					}

					$emailSplit = explode("@", $email);
					$mxhosts = [];
					// $smtpServer = null;
					if ( count($emailSplit) == 2 &&
						 getmxrr($emailSplit[1], $mxhosts)) {
						$this->log("buildStats - getmxrr() - ".print_r($mxhosts, true));
						$dnsRecord = dns_get_record($emailSplit[1], DNS_MX);
						$this->log("buildStats - dns_get_record() - ".print_r($dnsRecord, true));
						$hostName = gethostbyname($emailSplit[1]);
						$this->log("buildStats - gethostbyname() - $hostName");
						if (!filter_var($hostName, FILTER_VALIDATE_IP) === false ||
							!filter_var($hostName, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false) {
						    // echo("$ip is a valid IP address");
							$hostAddr = gethostbyaddr($hostName);
						} else {
						    // echo("$ip is not a valid IP address");
						    $hostAddr = $hostName; // so the check fails below
						}
						
						$this->log("buildStats - gethostbyaddr() - $hostAddr");
						if (count($mxhosts) < 3 &&
							$hostName != $hostAddr)
							$mxhosts[] = $hostAddr;
						// $sql = '';
						$where = '';
						$i = 0;
						$hosts = [];
						foreach($mxhosts as &$host) {
							$host = explode('.', $host);
							$host = strtolower($host[count($host)-2]).'.'.strtolower($host[count($host)-1]);
							if (!in_array($host, $hosts)) {
								$i++;
								$where .= (strlen($where) ? ' OR ' : '')."`name0` LIKE '$host'"." OR "."`name1` LIKE '$host'"." OR "."`name2` LIKE '$host'";
								if ($i >= 3)
									break;
								$hosts[] = $host;
							}
							unset($host);
						}
						foreach($dnsRecord as $rec) {
							$host = explode('.', $rec['target']);
							$host = !is_numeric($host[count($host)-2]) && !is_numeric($host[count($host)-1]) ? strtolower($host[count($host)-2]).'.'.strtolower($host[count($host)-1]) : "NoMatch";
							if ($host != "NoMatch" &&
								!in_array($host, $hosts)) {
								$i++;
								$where .= (strlen($where) ? ' OR ' : '')."`name0` LIKE '$host'"." OR "."`name1` LIKE '$host'"." OR "."`name2` LIKE '$host'";
								if ($i >= 3)
									break;
								$hosts[] = $host;
							}
							unset($host);
						}
						unset($xmhosts);
						$sqlSmtpServer = "SELECT * FROM {$this->getClass('SmtpServer', 1)->getTableName()} WHERE $where";
						$this->log("buildStats - query to find smtpServer: $sqlSmtpServer");
						$reuse = $this->useSmtpServer($smtpServer, $hosts);
						if (!$reuse) {
							unset($access, $smtpServer);
						}
						$smtpServer = !$reuse ? $this->getClass('SmtpServer')->rawQuery($sqlSmtpServer) : [$smtpServer];
						if (empty($smtpServer)) {
							$where = [];
							foreach($hosts as $i=>&$host)
								$where["name$i"] = $host;
							$x = $this->getClass('SmtpServer')->add((object)$where);
							if (empty($x)) 
								$this->log("buildStats - failed to create SmtpServer for {$where['name0']}");
							else {
								$smtpServer = $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$x]])[0];
								$this->log("buildStats - added new smtp server: $smtpServer->name0 on $today");
							}
							unset($where);
						}
						else
							$smtpServer = array_pop($smtpServer);

						if (!empty($smtpServer)) {
							if ( !in_array($smtpServer->id, $smtpServers) )
								$smtpServers[] = $smtpServer->id;
							if ($reuse && $access)
								$access = [$access];
							else
								$access = $this->getClass('SmtpAccessCount', 1)->get((object)['where'=>['smtp_id'=>$smtpServer->id,
																										'flags'=>EMAIL_STATS,
																									 	'date'=>$today]]);
							if (!empty($access)) {
								$access = array_pop($access);
								
								$sqlUpdate = "UPDATE ".$this->getClass('SmtpAccessCount')->getTableName()." SET `count` = `count` + 1 WHERE `id` = $access->id";
								$this->log("buildStats - query to find smtpAccessCount: $sqlUpdate");
								$this->getClass('SmtpAccessCount')->rawQuery($sqlUpdate);
								$this->log("buildStats - access counter at ".($access->count+1)." for $smtpServer->name0 on $today");
							}
							else {
								$x = $this->getClass('SmtpAccessCount')->add((object)['smtp_id'=>$smtpServer->id,
																					  'flags'=>EMAIL_STATS,
																					  'count'=>1,
																					  'date'=>$today]);
								if (!empty($x)) {
									$this->log("buildStats - create access counter for $smtpServer->name0 on $today");
									$access = $this->getClass('SmtpAccessCount')->get((object)['where'=>['id'=>$x]])[0];
								}
							}

							$emailDb = $this->getSellerEmailDb($user, $smtpServer);
							if ( !$emailDb->smtp_id ||
								 $emailDb->smtp_id != $smtpServer->id)
								$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
																				 'fields'=>['smtp_id'=>$smtpServer->id]]]);
							unset($emailDb);
						}
					}
				}
			}
			$stats = $this->getClass('SmtpAccessCount', 1)->get((object)['where'=>['flags'=>EMAIL_STATS,
																					'smtp_id'=>$smtpServers,
																					'date'=>$today],
																		 'orderby'=>'smtp_id',
																		 'order'=>'ASC']);
																					// 'date'=>$today]]);
			unset($smtpServers);
			$this->log("buildStats exiting with ".count($stats)." stats", 3);
		}
		catch(\Exception $e) {
			return parseException($e); 
		}
	}

	protected function userHasAgentMatch($user) {
		return $user->flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME);
	}

	protected function userHasPortal($user) {
		return $user->flags & (SELLER_IS_PREMIUM_LEVEL_1);
	}

	protected function userHasPortalReservation($user) {
		$retval = false;
		$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$user->id]]);
		if (!empty($reservation)) {
			if ($reservation->type & SELLER_IS_PREMIUM_LEVEL_1)
				$retval = true;
			unset($reservation);
		}

		return $retval;
	}

	protected function userHasAgentMatchReservation($user) {
		$retval = false;
		$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$user->id]]);
		if (!empty($reservation)) {
			if ($reservation->type & SELLER_IS_PREMIUM_LEVEL_2)
				$retval = true;
			unset($reservation);
		}

		return $retval;
	}

	public function sendEmail($id, $what, $targetList, &$csv, $mailchimp = 0, $noTrip = 0) {
		try {
			global $unsubscribe;
			global $addToContact;
			global $unsubscribe_notable;
			global $addToContact_notable;
			$orig_content = trim(file_get_contents($this->getFile($id)));
			$blogname = "Join Us!";
			$blogdescription = "Grow your luxury network!";
			$testStat = false;
			$sql = '';
			$portal_promo_price = '10.00';
			
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'EmailTestStats']]);
			if (!empty($opt) &&
				$what != 'email-test') $testStat = $opt[0]->value == "1" ? true : false;

			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'PortalPromoPrice']]);
			if (!empty($opt))
				$portal_promo_price = $opt[0]->value;

			$today = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
			$this->log("sendEmail for $id, $what on $today for ".(!empty($targetList) ? count($targetList) : 0)." users, mailchimp: $mailchimp", 3);
			

			switch($id) {
				case PROMO_FIRST: 
				// case NO_TABLE_INTRO:
					$blogname = "AN EXCLUSIVE INVITATION";
					$blogdescription = file_get_contents(__DIR__.'/../_mail/_content/agent_match_reservation_description.html.trim');
					$promoImg = get_template_directory_uri().'/_img/_promo/Inman-logos.png';
					$blogdescription = str_replace("%promo_img%", $promoImg, $blogdescription);
					$sql = "SELECT a.id,a.tag_id,c.tag,a.city_id FROM {$this->getClass('CitiesTags')->getTableName()} AS a ";
					$sql.= "INNER JOIN {$this->getClass('Cities')->getTableName()} AS b ON b.id = a.city_id ";
					$sql.= "INNER JOIN {$this->getClass('Tags')->getTableName()} AS c ON a.tag_id = c.id ";
					$sql.= "WHERE a.score >= 7 ";
					break;
				case PROMO_SECOND: 
				case PROMO_THIRD:
					$blogname = "AN EXCLUSIVE INVITATION";
					$blogdescription = file_get_contents(__DIR__.'/../_mail/_content/agent_match_reservation_description.html.trim');
					$promoImg = get_template_directory_uri().'/_img/_promo/Inman-logos.png';
					$blogdescription = str_replace("%promo_img%", $promoImg, $blogdescription);
					$imgSrc = get_template_directory_uri().'/_mail/_img/email-background.png';
					$imgSrc1 = get_template_directory_uri().'/_mail/_img/step1.png';
					$imgSrc2 = get_template_directory_uri().'/_mail/_img/step2.png';
					$imgSrc3 = get_template_directory_uri().'/_mail/_img/step3.png';
					// $content = str_replace('%first_name%', "Joe", $content);
					// $content = str_replace("%last_name%", "Champion", $content);
					// $content = str_replace("%listing_count%", "5 listings", $content);
					$content = str_replace("%img_src%", $imgSrc, $content);
					$content = str_replace("%img_src1%", $imgSrc1, $content);
					$content = str_replace("%img_src2%", $imgSrc2, $content);
					$content = str_replace("%img_src3%", $imgSrc3, $content);
					$this->log("sendEmailPromo for COMPLEX_INTRO_FOLLOWU - $imgSrc1, $imgSrc2, $imgSrc3");
					// $content = str_replace("%link_back%", "dummy", $content);
					break;
				case IMPROVE_LISTING:
					$blogname = "IMPROVE YOUR LISTINGS";
					$blogdescription = "Stand out amongst your peers!";
					break;

				case PORTAL_PROMO_1:
				case PORTAL_TOOLS_PKG:
				case PORTAL_PROMO_CONTENT_TEST:
					$blogname = "GET MORE REFERRALS";
					$blogdescription = "Leverage your contacts, be seen!";
					break;

			}
			$wpUser = null;
			if (empty($targetList) &&
				$what == 'email-test') { //must be
				$wpUser = wp_get_current_user();
				$userId = ($wpUser && !is_wp_error( $wpUser )) ? $wpUser->ID : 0;
				$user = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$userId]]);
				if (!empty($user))
					$targetList = [$user[0]];
			}

			$exceptions = [];
			$processed = 0;
			$bypassed = 0;
			$separator = ',';
			$life_separator = '    ';
			$excludeEmailFlaggingIds = [1,2,3,4,5,6,7,8,9];
			foreach($targetList as $user_id) {
				$processed++;
				$commaLine = '';
				$content = $orig_content;
				if ( ($processed % 10) == 0)
					time_nanosleep(0, 30000000);
				$user_id = (object)$user_id;
				$user = $this->getClass('Sellers')->get((object)['where'=>['id'=>$user_id->id]]);
				if (!empty($user)) {
					$user = array_pop($user);
					$author = !empty($user->author_id) ? $user->author_id : $user->id;		
					$author_has_account = !empty($user->author_id) ? 1 : 0;
					$listings = $this->getClass('Listings')->get((object)['where'=>['author'=>$author,
																					'author_has_account'=>$author_has_account,
																					'active'=>1]]);
					// $lastName = ($pos = strpos($user->last_name, ",")) === false ? $user->last_name : substr($user->last_name, 0, $pos);
					// $lastName = ucwords(strtolower($lastName));
					// if (substr($lastName, 0,2) == "Mc" &&
					// 	strlen($lastName) > 2)
					// 	$lastName[2] = chr( ord($lastName[2]) - 32 ); // cap that 3rd letter
					// $firstName = ($pos = strpos($user->first_name, ",")) === false ? $user->first_name : substr($user->first_name, 0, $pos);
					// $firstName = ucwords(strtolower($user->first_name));

					/// name should have go through formatName()
					$firstName = $user->first_name;
					$lastName = $user->last_name;
					$flagIt = isset($user->author_id) ? (!in_array($user->author_id, $excludeEmailFlaggingIds) ? 1 : 0) : 1;

					$commaLine .= "$firstName$separator $lastName$separator ";
					switch($id) {
						case PROMO_FIRST: 
						// case NO_TABLE_INTRO:
							if ( $flagIt &&
								 $this->userHasAgentMatch($user)) {
								$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already is LifeStyled Agent";
								$this->log("sendEmailPromo - $msg", 3);
								$exceptions[] = $msg;
								continue;
							}

							// if ($this->userHasPortalReservation($user)) {
							// 	$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already has Portal reservation";
							// 	$this->log("sendEmailPromo - $msg", 3);
							// 	$exceptions[] = $msg;
							// 	continue;
							// }

							$content = str_replace('%first_name%', $user->first_name, $content);
							$content = str_replace('%city%', $user->city ? $user->city : ($user->state ? $user->state : 'your service area'), $content);
							$tags = $sql.' AND b.city="'.addslashes($user->city).'" AND b.state="'.$user->state.'"';
							// $this->log("sendEmailPromo - tags sql: $tags");
							$commaLine .=  !empty($user->city) ? "$user->city$separator " : "your service area$separator ";
							$chimpCount = 0;
							if (!empty($user->city))
								$tags = $this->getClass('Tags')->rawQuery($tags);
							else
								$tags = 0;
							$lifestyles = '';
							if (!empty($tags)) {
								foreach($tags as $tag) {
									$lifestyles .= (!empty($lifestyles) ? "$life_separator " : '').$tag->tag;
									if ($chimpCount < 8)
										$commaLine .= "$tag->tag$separator ";
									$chimpCount++;
									// $this->log("sendEmailPromo - tag: $tag->tag");
								}
								// $this->log("sendEmailPromo - used ".count($tags)." tags");
								for(; $chimpCount < 8; $chimpCount++)
									$commaLine .= "$separator  ";
							}
							else {
								// $this->log("sendEmailPromo - used no tags");
								$lifestyles = "such as: golf    art    music    ocean front property   beach";
								$commaLine .= "such as: golf$separator art$separator music$separator hiking$separator fishing$separator ocean boating$separator $separator   $separator  ";
							}
							$this->log("sendEmailPromo - lifestyles: $lifestyles");
							$content = str_replace('%life_styles%', $lifestyles, $content);
							unset($tags);
							
							break;
						case PROMO_SECOND: 
						case PROMO_THIRD:
							if ($flagIt &&
								$this->userHasAgentMatch($user)) {
								$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already is LifeStyled Agent";
								$this->log("sendEmailPromo - $msg", 3);
								$exceptions[] = $msg;
								continue;
							}

							if ($flagIt &&
								$this->userHasAgentMatchReservation($user)) {
								$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already has LifeStyled Agent reservation";
								$this->log("sendEmailPromo - $msg", 3);
								$exceptions[] = $msg;
								continue;
							}

							$content = str_replace('%first_name%', $user->first_name, $content);
							$content = str_replace("%last_name%", $user->last_name, $content);
							$content = str_replace("%listing_count%", (empty($listings) ? "no active listing" : count($listings)." active listings"), $content);
							$content = str_replace("%connector%", (empty($listings) ? "but" : "and"), $content);
							$commaLine .= (empty($listings) ? "no active listing" : count($listings)." active listings")."$separator ".(empty($listings) ? "but" : "and")."$separator ";
							break;
						case IMPROVE_LISTING:
							$content = str_replace('%first_name%', $user->first_name, $content);
							$optional_message = ''; $optional_message2 = ''; $optional_message3 = '';
							if ( !($user->flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)) ) {
								$optional_message2 = '<p>Please consider joining as a Lifestyled&#8482; Agent gives you the opportunity to dominate your local market,';
								$optional_message2.= ' connect with clients that share your same passions, and close more luxury transactions. ';
								$optional_message2.= 'We offer this service to only 3 qualified agents per lifestyle so  act now to secure your place. ';
								$optional_message2.= 'This is a premium service, not lead selling, we do not touch agent commissions.</p>';
								$optional_message3 = '<p><span>To apply or learn more click <a href="%link_back%" >here</a>.</span></p>';
							}

							if (empty($user->author_id))
								$optional_message = '<p><Please register with us now so that you can edit your listings! Click <a href="%link_back2%" >here</a> to register now!/p>';
							$content = str_replace("%optional_message%", $optional_message, $content);
							$content = str_replace("%optional_message2%", $optional_message2, $content);
							$content = str_replace("%optional_message3%", $optional_message3, $content);

							$improvements = '<th>Listing Address</th><th>Improve on these</th>';
							foreach($user->meta as $meta) {
								if ( intval($meta->action) == SELLER_IMPROVEMENT_TASKS ) {
									if (!is_numeric($meta->listhub_key)) {
										$listing = $this->getClass('Listings')->get((object)['where'=>['listhub_key'=>$meta->listhub_key]]);
										if (empty($listing))
											continue; // give up
										$meta->listhub_key = $listing[0]->id;
									}
									else {
										$listing = $this->getClass('Listings')->get((object)['where'=>['id'=>$meta->listhub_key]]);
										if (empty($listing))
											continue; // give up
									}
									if (empty($listings)) $listings = $listing; // so we can get the subject line as listing address
									$improvements .= '<tr>';
									$improvements .= 	'<td><a href="'.get_home_url()."/listing/T%emailTrackerId%-{$listing[0]->id}".'">'.$listing[0]->street_address.'</a></td>';
									$improvements .= 	'<td><span>';
														$todos = '';
														foreach($meta as $key=>$value) {
															if ($key != 'listhub_key' &&
																$key != 'action') 
																$todos .= (!empty($todos) ? ', ': '').$key;
														}
									$improvements .=		"$todos</span></td>";
									$improvements .= '</tr>';
								}
							}
							$content = str_replace("%improvements%", $improvements, $content);		
							break;

						case PORTAL_PROMO_1:
						case PORTAL_TOOLS_PKG:
						case PORTAL_PROMO_CONTENT_TEST:
							if ($flagIt &&
								$this->userHasAgentMatch($user)) {
								$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already is LifeStyled Agent";
								$this->log("sendEmailPromo - $msg", 3);
								$exceptions[] = $msg;
								continue;
							}
							if ($flagIt &&
								$this->userHasPortal($user)) {
								$msg = "Mailtype: $id - sellerId: $user->id, $user->first_name $user->last_name already has a portal";
								$this->log("sendEmailPromo - $msg", 3);
								$exceptions[] = $msg;
								continue;
							}

							$content = str_replace('%first_name%', $user->first_name, $content);
							$content = str_replace("%last_name%", $user->last_name, $content);
							$content = str_replace("%price%", $portal_promo_price, $content);
							break;
					}
					
					$listingAddr = '';
					if (!empty($listings)) foreach($listings as $listing) {
						if ( strpos( strtolower($listing->street_address), "www.") === false ) {
							$listingAddr = $listing->street_address;
							unset($listing);
							break;
						}
						unset($listing);
					}

					$email = $user->email;
					if ($user->meta) foreach($user->meta as $meta) {
						if ($meta->action == SELLER_PROFILE_DATA &&
							!empty($meta->contact_email) )
							$email = $meta->contact_email;
					}

					if (empty($email) ||
						count( explode('@', $email) ) != 2) {
						$this->log("sendEmail - failed for sellerId: $user->id, for $id, $what - no email address", 3);
						$msg = "No email address for sellerId:$user->id.";
						if (!in_array($msg, $exceptions))
							$exceptions[] = $msg;
						continue;
					}

					$email = trim($email);
					$emailSplit = explode("@", $email);
					$mxhosts = [];
					$smtpServer = null;
					$emailDb = $this->getSellerEmailDb($user, $smtpServer);

					if ( (!$emailDb || empty($emailDb->smtp_id)) &&
						 count($emailSplit) == 2 &&
						 getmxrr($emailSplit[1], $mxhosts)) {
						$this->log("sendEmail - getmxrr() - ".print_r($mxhosts, true));
						$dnsRecord = dns_get_record($emailSplit[1], DNS_MX);
						$this->log("sendEmail - dns_get_record() - ".print_r($dnsRecord, true));
						$hostName = gethostbyname($emailSplit[1]);
						$this->log("sendEmail - gethostbyname() - $hostName");
						if (!filter_var($hostName, FILTER_VALIDATE_IP) === false ||
							!filter_var($hostName, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false) {
						    // echo("$ip is a valid IP address");
							$hostAddr = gethostbyaddr($hostName);
						} else {
						    // echo("$ip is not a valid IP address");
						    $hostAddr = $hostName; // so the check fails below
						}
						$this->log("sendEmail - gethostbyaddr() - $hostAddr");
						if (count($mxhosts) < 3 &&
							$hostName != $hostAddr)
							$mxhosts[] = $hostAddr;
						// $sql = '';
						$where = '';
						$i = 0;
						$hosts = [];
						foreach($mxhosts as &$host) {
							$host = explode('.', $host);
							$host = strtolower($host[count($host)-2]).'.'.strtolower($host[count($host)-1]);
							if (!in_array($host, $hosts)) {
								$i++;
								$where .= (strlen($where) ? ' OR ' : '')."`name0` LIKE '$host'"." OR "."`name1` LIKE '$host'"." OR "."`name2` LIKE '$host'";
								if ($i >= 3)
									break;
								$hosts[] = $host;
							}
							unset($host);
						}
						foreach($dnsRecord as $rec) {
							$host = explode('.', $rec['target']);
							$host = !is_numeric($host[count($host)-2]) && !is_numeric($host[count($host)-1]) ? strtolower($host[count($host)-2]).'.'.strtolower($host[count($host)-1]) : "NoMatch";
							if ($host != "NoMatch" &&
								!in_array($host, $hosts)) {
								$i++;
								$where .= (strlen($where) ? ' OR ' : '')."`name0` LIKE '$host'"." OR "."`name1` LIKE '$host'"." OR "."`name2` LIKE '$host'";
								if ($i >= 3)
									break;
								$hosts[] = $host;
							}
							unset($host);
						}
						unset($xmhosts);
						$sqlSmtpServer = "SELECT * FROM {$this->getClass('SmtpServer', 1)->getTableName()} WHERE $where";
						$this->log("sendEmail - query to find smtpServer: $sqlSmtpServer");
						$smtpServer = $this->getClass('SmtpServer')->rawQuery($sqlSmtpServer);
						if (empty($smtpServer)) {
							$where = [];
							foreach($hosts as $i=>&$host)
								$where["name$i"] = $host;
							$x = $this->getClass('SmtpServer')->add((object)$where);
							if (empty($x)) 
								$this->log("sendEmail - failed to create SmtpServer for {$where['name0']}", 3);
							else {
								$smtpServer = $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$x]])[0];
								$this->log("sendEmail - added new smtp server: $smtpServer->name0 on $today", 3);
							}
							unset($where);
						}
						else
							$smtpServer = array_pop($smtpServer);
					} // if ( (!$emailDb || empty($emailDb->smtp_id)) && count($emailSplit) == 2 && getmxrr($emailSplit[1], $mxhosts))
					elseif ($emailDb && !empty($emailDb->smtp_id)) {
						$smtpServer =  $this->getClass('SmtpServer')->get((object)['where'=>['id'=>$emailDb->smtp_id]]);
						if (!empty($smtpServer)) $smtpServer = array_pop($smtpServer);
					}

					if (!empty($smtpServer)) {
						$access = $this->getClass('SmtpAccessCount', 1)->get((object)['where'=>['smtp_id'=>$smtpServer->id,
																							 	'date'=>$today]]);
						if (!empty($access)) {
							$access = array_pop($access);
							if (!$mailchimp &&
								$access->count >= MAX_EMAIL_PER_SMTP ) {
								$this->log("sendEmail - maximum of ".MAX_EMAIL_PER_SMTP." has been reached for $smtpServer->name0", 3);
								$msg = "Reached limit of ".MAX_EMAIL_PER_SMTP." for $smtpServer->name0";
								if (!in_array($msg, $exceptions))
									$exceptions[] = $msg;
								$bypassed++;
								continue;
							}
							else {
								$curTime = microtime(true);
								if ( !$mailchimp &&
									 ($curTime - $access->last_sent) < 1.0)
									time_nanosleep(0, ($curTime - $access->last_sent+0.01)*1000000000);

								$sqlUpdate = "UPDATE ".$this->getClass('SmtpAccessCount')->getTableName()." SET `count` = `count` + 1, `last_sent`=".microtime(true)." WHERE `id` = $access->id";
								$this->log("sendEmail - query to find smtpAccessCount: $sqlUpdate");
								$this->getClass('SmtpAccessCount')->rawQuery($sqlUpdate);
								$this->log("sendEmail - access counter at ".($access->count+1)." for $smtpServer->name0 on $today", 3);
							}
						}
						else {
							$x = $this->getClass('SmtpAccessCount')->add((object)['smtp_id'=>$smtpServer->id,
																				  'count'=>1,
																				  'date'=>$today]);
							if (!empty($x))
								$this->log("sendEmail - create access counter for $smtpServer->name0 on $today");
						}
						unset($access);
					}
					else
						$this->log("sendEmail - no smtpServer for $user->email", 3);
					

					
					$fields = [];
					$needUpdate = false;

					// map LLC sent email to match flags used with MailChimp
					switch($id) {
						case LLC_NO_TABLE_INTRO:
						case LLC_INTRO: $id = PROMO_FIRST; break;
						case LLC_FOLLOWUP: $id = PROMO_SECOND; break;
						case LLC_COMPLEX_FOLLOWUP: $id = PROMO_THIRD; break;
					}

					
					if ($flagIt) {					
						if ($emailDb === false)
							$this->log("sendEmail - failed to get email DB for sellerId: $user->id, for $id, $what, $email", 3);
						else {
							if ($emailDb->flags & $id &&
								$id != IMPROVE_LISTING) { // multiple IMPROVE_LISTING can be sent out
								$this->log("sendEmail - already have sent mail type: $id to sellerId: $user->id", 3);
								$msg = "Mailtype: $id already sent for sellerId:$user->id.";
								$bypassed++;
								if (!in_array($msg, $exceptions))
									$exceptions[] = $msg;
								continue;
							}
							if ($emailDb->flags & EMAIL_UNSUBSCRIBED) {
								$this->log("sendEmail - sellerId: $user->id has unsubscribed out of our system.", 3);
								$msg = "sellerId:$user->id is unsubscribed";
								$bypassed++;
								if (!in_array($msg, $exceptions))
									$exceptions[] = $msg;
								continue;
							}
							if (!$noTrip &&
								$what != 'email-test') {
								$this->log("sendEmail - sellerId: $user->id flagged with $id");
								$emailDb->flags |= $id;												
								$fields['flags'] = $emailDb->flags;
								$needUpdate = true;
							}
						}
					}

					$magic = microtime().$listingAddr.$email;
					$md5 = '';
					$md5 = hash("sha256", $magic);
					if (!$noTrip) {
						$et = $this->getClass("EmailTracker");
						$etData = [];				
						$etData['track_id'] = $md5;					
						$etData['agent_id'] = $user->id; // seller id and not author_id since it can be from an unregistered agent
						$etData['agent_email'] = $email;
						$etData['user_id'] = $user->author_id ? $user->author_id : 0; // this is always from wordpress user table, i.e. WP_User->ID
						$etData['user_email'] = $wpUser ? $wpUser->user_email : $email;
						$etData['first'] = $user->first_name;
						$etData['listing_id'] = !empty($listings) ? $listings[0]->id : 0;
						$etData['type'] = $id; // general, improvement, suggestion, question, PROMO_FIRST, PROMO_THIRD, IMPROVE_LISTING, PORTAL_PROMO_1, etc..
						$etData['flags'] = MESSAGE_EMAIL_FROM_LLC; // MESSAGE_FEEDBACK, MESSAGE_EMAIL_AGENT, MESSAGE_REPLY_TO_AGENT, MESSAGE_REPLY_TO_USER
						$x = $et->add($etData);	

						// only for IMPROVE_LISTING now....
						if ($smtpServer &&
							$emailDb &&
							$id == IMPROVE_LISTING) {
							$metas = [];
							$sentMeta = null;
							$action = 0;
							switch($id) {
								case PROMO_FIRST: 
								// case NO_TABLE_INTRO: 
									$action = SENT_FIRST_EMAIL; 
									break;
								case PROMO_SECOND:
									$action = SENT_SECOND_EMAIL; 
									break;
								case PROMO_THIRD: 
									$action = SENT_THIRD_EMAIL; 
									break;
								case IMPROVE_LISTING: 
									$action = IMPROVE_LISTING_SENT; 
									break;
							}
							if (!empty($emailDb->meta)) foreach($emailDb->meta as $meta) {
								if ($meta->action == $action &&
									$action != IMPROVE_LISTING_SENT) { // IMPROVE_LISTING_SENT may have multiples
									$sentMeta = $meta;
								}
								$metas[] = $meta;
							}

							if ( $id &&
								 ($emailDb->flags & $id) == 0 ) {
								$fields['flags'] = $emailDb->flags | $id;
								$needUpdate = true;
							}


							if ($sentMeta)
								$this->log("sendEmail - replacing meta for $action");

							$sentMeta = new \stdClass();
							$sentMeta->action = $action;
							$sentMeta->date = $today;
							$sentMeta->smtp_id = $smtpServer->id;
							$sentMeta->emailTrackerId = $md5;
							$metas[] = $sentMeta;
							$fields['meta'] = $metas;
							$needUpdate = true;
						}

						if ($smtpServer &&
							$emailDb &&
							$smtpServer->id != $emailDb->smtp_id) {
							$fields['smtp_id'] = $smtpServer->id;
							$needUpdate = true;
						}
						if ($needUpdate)
							$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
																			 'fields'=>$fields]]);
					}
					unset($fields, $smtpServer, $emailDb);

					$md5 .= "__".strval($user->id);
					$url = get_template_directory_uri().'/_pages/ajax-register.php?query=agent-respond&data='.$md5;
					$responseUrl = $url;
					$url_unsubscribe = get_template_directory_uri().'/_pages/ajax-register.php?query=agent-unsubscribe&data='.$md5;
					if (strpos($content, '%link_back%') !== false) {
						$chimpUrl = '<a href="%url%">here</a>';
						$chimpUrl = str_replace("%url%", $url, $chimpUrl);
						$commaLine .= "$chimpUrl$separator ";
						$content = str_replace("%link_back%", $url, $content);
					}
					if (strpos($content, '%link_back2%') !== false) {
						switch($id) {
							case IMPROVE_LISTING:
								$url = get_site_url()."/register/{$user->id}";
								break;
						}
						$chimpUrl = '<a href="%url%">here</a>';
						$chimpUrl = str_replace("%url%", $url, $chimpUrl);
						$commaLine .= "$chimpUrl$separator ";
						$content = str_replace("%link_back2%", $url, $content);
					}

					if (strpos($content, '%link_back3%') !== false) {
						switch($id) {
							case IMPROVE_LISTING:
								//$url = get_site_url().'/register';
								$url = get_site_url()."/listing/T$x-{$listings[0]->id}";
								break;
						}
						$chimpUrl = '<a href="%url%">here</a>';
						$chimpUrl = str_replace("%url%", $url, $chimpUrl);
						$commaLine .= "$chimpUrl$separator ";
						$content = str_replace("%link_back3%", $url, $content);
					}

					if ($id == IMPROVE_LISTING)
						$content = str_replace("%emailTrackerId%", $x, $content);	

					$subject = !empty($listingAddr) && $id != PORTAL_TOOLS_PKG ? ( (($pos = strpos($listingAddr, ",")) !== false) ? $listingAddr = substr($listingAddr, 0, $pos) : $listingAddr ) : "LifeStyled Listings invites you!";
					$commaLine .= "$subject$separator ";
					$msg = $content;

					if (!empty($listings) &&
						$id != IMPROVE_LISTING) {
						$here = '<a href="'.get_home_url()."/listing/T$x-{$listings[0]->id}".'">here</a>';
						$commaLine .= "$here$separator ".count($listings)."$separator ";
						// if ($id != NO_TABLE_INTRO) 
							$h = '<table><tbody><tr><td>Click '.$here.' to see '.(count($listings) > 1 ? 'one of' : '').' your listing on our site.</td></tr></tbody></table>';
						// else
						// 	$h = '<p>Click '.$here.' to see '.(count($listings) > 1 ? 'one of' : '').' your listing on our site.</p>';
						$msg .= $h;
					}
					else
						$commaLine .= "$separator 0$separator ";

					$chimpUrl = '<a href="%url%">here</a>';
					$chimpUrl = str_replace("%url%", $url_unsubscribe, $chimpUrl);
					// last entries, no separator at end!!!
					$commaLine .= "$chimpUrl$separator $responseUrl$separator $user->id";
					$this->log("sendEmail - line: $commaLine", 3);

					$csv .= "$email$separator ".$commaLine."\n\r";

					// if ($id != NO_TABLE_INTRO) {
						$this->log("addToContact: $addToContact, unsubscribe: $unsubscribe");
						$msg .= trim($addToContact);
						$msg = str_replace("%admin%", SUPPORT_EMAIL, $msg);
						$msg .= trim($unsubscribe);
						$msg = str_replace("%unsubscribe%", $url_unsubscribe, $msg);
					// }
					// else {
					// 	$this->log("addToContact: $addToContact_notable, unsubscribe: $unsubscribe_notable");
					// 	$msg .= trim($addToContact_notable);
					// 	$msg = str_replace("%admin%", SUPPORT_EMAIL, $msg);
					// 	$msg .= trim($unsubscribe_notable);
					// 	$msg = str_replace("%unsubscribe%", $url_unsubscribe, $msg);
					// }

					$msg = trim($msg);

					if (!$testStat &&
						!$mailchimp &&
						!$noTrip) {
						$this->log("sendEmail - $what. -\n".$msg, 3);

						$success = $this->sendMail( $email, $subject, $msg, 
					  								$blogname,
					  								$blogdescription); 

						$what = ucwords(str_replace("-", " ", $what));
					  	$this->sendMail( SUPPORT_EMAIL, $subject, $msg, 
					  	  								$what,
				  										"Email Tracker id: $md5");
					}
				  	unset($etData, $listings, $user);
				}
			}
			$this->log("sendEmail - ".count($targetList)." users, processed:$processed, bypassed:$bypassed", 3);
			$this->log("sendEmail -  with exceptions: ". (count($exceptions) ? implode(", ", $exceptions) : "None"), 2);
			return new Out('OK', (object)['status'=>"sent out ".count($targetList)." email(s), exceptions: ".(count($exceptions) ? implode(", ", $exceptions) : "None")]);
		}
		catch(\Exception $e) {
			return parseException($e); 
		}
	}


}
