<?php
namespace AH;
require_once(__DIR__.'/_Controller.class.php');

// require_once(__DIR__.'/../../../../wp-load.php');
// require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
// require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );

class Quiz extends Controller {
	private $options = [
		'max_listings' => 200, // maximum # listings each tag set will produce
		'max_cities_init' => 200, // initial maximum # cities to include for sorting out listings.  Not all cities will have listings
		'max_cities' => 50, // maximum # cities to include
		'cities_chunk_size' => 10,
		'max_city_score' => 10,
		'city_bias' => 0,
		'show_extra_timings' => 0,// !! use Options::QuizShowExtraTimings to trigger this, dont' change this!!
		'warn_culling_percentage' => 10,
		'minPrice' => 800000,
		'local_recursion_trigger' => 12,
		'quizGrabAllIntoCities' => 0,
		'imgPath' => '/../_img/_listings/uploaded', // this is for the small listing image ul in the city ul in quiz results
		'imgPathSubSize' => '/../_img/_listings/845x350', // this is for the small listing image ul in the city ul in quiz results
		'imgPathCity' => '/../_img/_cities/845x350', // this is for the small listing image ul in the city ul in quiz results
		'haveActiveParallelProcessing' => 0,
		'allowPartialQuizResults' => 0,
		'debugLevel' => 1,
		'checkListImageExists' => 0,
		'doubleCheckListImageExists' => 0,
		'allowDiscardImages' => 0
	];
	public function __construct(){ 
		require_once(__DIR__.'/Utility.class.php'); 
		$this->log = new Log(__DIR__.'/_logs/Quiz.log') ;

		$this->options['imgPath'] = __DIR__.'/../_img/_listings/uploaded';
		$this->options['imgPathSubSize'] = __DIR__.'/../_img/_listings/845x350';
		$this->options['imgPathCity'] = __DIR__.'/../_img/_cities/845x350';
		$this->options['haveActiveParallelProcessing'] = $this->haveActiveParallelProcessing();
		$this->options['minRentalPrice'] = 300;

		$Options = $this->getClass('Options');
		$opt = $Options->get((object)['where'=>['opt'=>'QuizShowExtraTimings']]);
		if (!empty($opt))
			$this->options['show_extra_timings'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'QuizMaxResultSize']]);
		if (!empty($opt))
			$this->options['max_listings'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'MaxCities']]);
		if (!empty($opt))
			$this->options['max_cities'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'MaxInitialCities']]);
		if (!empty($opt))
			$this->options['max_cities_init'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'CitiesChunkSize']]);
		if (!empty($opt))
			$this->options['cities_chunk_size'] = intval($opt[0]->value);

		// MIN_RENTAL_PRICE
		$opt = $Options->get((object)['where'=>['opt'=>'MinRentalPrice']]);
		if (!empty($opt))
			$this->options['minRentalPrice'] = intval($opt[0]->value);

		$this->startCityCountUsedForListings = 0;
		$this->currentMaxCityCountUsedForListings = $this->options['cities_chunk_size']; // * 2;

		$opt = $Options->get((object)['where'=>['opt'=>'AllowPartialQuizResults']]);
		if (!empty($opt))
			$this->options['allowPartialQuizResults'] = intval($opt[0]->value);


		// $this->currentMaxCityCountUsedForListings = floor($this->options['max_cities']/2) + floor($this->options['max_cities'] * 0.1);
		// cap at double # of cities being returned
		//$this->options['max_cities_init'] = $this->options['max_cities_init'] > (2* $this->options['max_cities']) ? (2* $this->options['max_cities']) : $this->options['max_cities_init'];

		$opt = $Options->get((object)['where'=>['opt'=>'QuizWarnCullingPercentage']]);
		if (!empty($opt))
			$this->options['warn_culling_percentage'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'QuizLocalSearchRecurseTrigger']]);
		if (!empty($opt))
			$this->options['local_recursion_trigger'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'QuizDebugLevel']]);
		if (!empty($opt))
			$this->options['debugLevel'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
		$this->options['minPrice'] = !empty($opt) ? intval($opt[0]->value) : 800000;
		$this->log("minPrice - ".$this->options['minPrice']);

		$opt = $Options->get((object)['where'=>['opt'=>'QuizGrabAllIntoCities']]);
		$this->options['quizGrabAllIntoCities'] = !empty($opt) ? is_true($opt[0]->value) : 0;

		$this->log("quizGrabAllIntoCities: ".($this->options['quizGrabAllIntoCities'] ? "true" : "false").", found opt:".(!empty($opt) ? "yes" : "no"));

		$opt = $Options->get((object)['where'=>['opt'=>'CheckListImageExists']]);
		if (!empty($opt))
			$this->options['checkListImageExists'] = intval($opt[0]->value);

		$opt = $Options->get((object)['where'=>['opt'=>'DoubleCheckListImageExists']]);

		// QuizAllowDiscardImages
		$opt = $Options->get((object)['where'=>['opt'=>'QuizAllowDiscardImages']]);
		if (!empty($opt))
			$this->options['allowDiscardImages'] = intval($opt[0]->value);

		if (!empty($opt))
			$this->options['doubleCheckListImageExists'] = intval($opt[0]->value);
	}

	public function setOption($name, $value) {
		$this->options[$name] = $value;
	}

	
	// session_id = row index from Sessions table
	// quiz_id = row index from QuizActivity
	public function parseQuizActions($session_id = null, $quiz_id = -1){
		// if ($session_id == null) throw new \Exception('no session id provided.');
		require_once(__DIR__.'/QuizActivity.class.php'); $QuizActivity = new QuizActivity(1);
		require_once(__DIR__.'/Sessions.class.php'); $Sessions = new Sessions();

		$ses = null;
		if ($session_id) {
			$ses = $Sessions->get((object)['where'=>['id'=>$session_id]]);
			if (empty($ses))
				throw new \Exception("Failed to find session id:$session_id");
			$ses = array_pop($ses);
		}
		// else {
		// 	require_once(__DIR__.'/SessionMgr.class.php'); $SessionMgr = new SessionMgr();
		// 	$session = $SessionMgr->getCurrentSession($session_id);
		// 	$SessionMgr->log("parseQuizActions is using session id:$session_id from getCurrentSession()");
		// }
		
		// $quiz_id = intval($quiz_id) == -1 ? ($ses ? $ses->quiz_id :  throw new \Exception("Failed to find session and quiz_id is -1")) : $quiz_id;
		$incomingQuizId = $quiz_id;
		$this->log("parseQuizActions called with session:$session_id, quiz_id:$incomingQuizId");
		if ( intval($quiz_id) == -1  ) {
			if ($ses)
				$quiz_id = $ses->quiz_id;
			else
				throw new \Exception("Failed to find session and quiz_id is -1");
		}

		$activity = $QuizActivity->get((object)['where'=>['id'=>$quiz_id]] ); //[0];

		if (empty($activity)) throw new \Exception("No activity found for session id:$session_id, quiz_id:$quiz_id.");
		$activity = array_pop($activity);

		if (empty($activity->data)) throw new \Exception('No data found in activity.');
		$data = array_pop($activity->data);  // get actions from array
		if (empty($data)) throw new \Exception('No data found in activity data.');

		if ($activity->status == 'processing') {
			$this->log("parseQuizActions called for session: $session_id, and it's already processing!");
			return 'processing';
		}

		$QuizActivity->set([(object)['where'=>[	'id'=>$activity->id,
												'session_id'=>$session_id],
									 'fields'=>['status'=>'processing']]]);

		$results = new \stdClass(); $results->tags = array(); $results->max = 0;
		if ( isset($data->results) &&
			 isset($data->results[0]->tags)) {
			$results->tags = $data->results[0]->tags;
			$this->log("parseQuizActions reusing tags from existing results:".print_r($results->tags, true));
		}
		else {
			try {
				$this->calculateWeights($results, $data);
			}
			catch(\Exception $e){
				$out = json_decode(parseException($e));
				$this->log("parseQuizActions - caught exception from calculateWeights, status:".$out->data);
				$fields = ['status'=>'done'];
				$QuizActivity->set([(object)['where'=>[	'id'=>$activity->id,
														'session_id'=>$session_id],
										 	'fields'=>$fields]]);
				return false;
			}
		}

		if ($data->quiz != 1 &&  // then local or state
			empty($data->state) && // but no state
			empty($data->location)) { // and no location
			$data->quiz = 1; // so it's now nationwide
			$this->log("parseQuizActions - forcing quiz type $data->quiz to be 1, as no state nor location is defined, quizId:$activity->id");
		}
		elseif ($data->quiz == 5 && // is state
				empty($data->state)) { // but no state
			if (!empty($data->location)) { // but have location
				$data->quiz = 0; // now local
				$this->log("parseQuizActions - forcing quiz type 5 to be 0, as no state is defined but location is, quizId:$activity->id");
			}
		}

		$first = true;
		$tagMsg = '';
		foreach($results->tags as $tag) {
			$tag = (object)$tag;
			$tagMsg .= ($first ? '' : ', ')."($tag->id,$tag->tag)";
			$first = false;
		}

		if (empty($session_id)) {// must be from SiteMapper, so set allowPartialQuizResults to 0
			$this->options['allowPartialQuizResults'] = 0;
			$this->options['max_cities_init'] = 100;
		}

		$this->log("parseQuizActions called with status:".$activity->status.", session:$session_id, quiz_id:$incomingQuizId, using quiz: $activity->id, tags:".$tagMsg.", QuizShowExtraTimings:{$this->options['show_extra_timings']}");

		// reset counters
		$this->startCityCountUsedForListings = 0;
		$this->currentMaxCityCountUsedForListings = $this->options['cities_chunk_size']; // * 2;
		
		$data->results = array(
			$this->parseAndReturnResultSet((object)array(
				'tags' => $results->tags,
				'location' => (isset($data->location) && $data->location ? (is_array($data->location) ? $data->location : [$data->location]) : []),
				'distance' => (isset($data->distance) && $data->distance ? $data->distance : 0),
				'sessionID' => $session_id,
				'quiz' => $data->quiz,
				'state' => isset($data->state) ? $data->state : '',
				'price' => isset($data->price) ? $data->price : [$this->options['minPrice'], -1],
				'homeOptions' => isset($data->homeOptions) ? $data->homeOptions : ['beds'=>0, 'baths'=>0],
				'filter' => isset($data->filter) ? $data->filter : 0
				// entering Quiz from here would mean that this needs to be run, so don't set partial quiz values...
				// 'allowPartialQuizResults' => isset($data->results) &&  isset($data->results[0]->allowPartialQuizResults ? $data->results[0]->allowPartialQuizResults : $this->options['allowPartialQuizResults'],
				// 'startCityCountUsedForListings' => isset($data->results) &&  isset($data->results[0]->startCityCountUsedForListings ? $data->results[0]->startCityCountUsedForListings : $this->startCityCountUsedForListings,
				// 'currentMaxCityCountUsedForListings' => isset($data->results) &&  isset($data->results[0]->currentMaxCityCountUsedForListings ? $data->results[0]->currentMaxCityCountUsedForListings : $this->currentMaxCityCountUsedForListings,
			))
		);

		$activity->data[] = $data;

		// $query = (object) array( 'where'=>array('session_id'=>$session_id) );
		$query = (object) array( 'where'=>array('id'=>$activity->id,
												'session_id'=>$session_id) );
		$status = $data->results[0]->allowPartialQuizResults ? 'partial' : 'done'; // if partial, fe will need to call runParial()
		$query->fields = array('data'=>$activity->data,
							   'status'=>$status);
		$retval = $QuizActivity->set(array($query)) ? true : false;
		$countCitySorted = isset($data->results[0]->city_sorted) ? count($data->results[0]->city_sorted) : 0;
		$this->log("Set QuizActivity to $status for quiz index: ".count($activity->data).", with retval:".$retval.", session:$session_id, quiz_id:$incomingQuizId, city_sorted:".$countCitySorted.", admitted:".$data->results[0]->admitted);

		return $retval;
	}

	public function copyArray($ar1) {
		$ar2 = [];
		$ar1 = (array)$ar1;
		foreach($ar1 as $i => $data)
			$ar2[$i] = $data;
		return $ar2;
	}

	public function parseAndReturnResultSet($in, $recursed = false, $partialResults = null, $previousFindings = null, $moreCities = false) {
		require_once(__DIR__.'/Options.class.php'); $Options = new Options();
		$opt = $Options->get((object)['where'=>['opt'=>'QuizShowExtraTimings']]);
		if (!empty($opt))
			$this->options['show_extra_timings'] = intval($opt[0]->value);
		try {
			$allowPartialQuizResults = isset($in->allowPartialQuizResults) ? $in->allowPartialQuizResults : $this->options['allowPartialQuizResults'];
			if (isset($in->startCityCountUsedForListings) && !empty($in->startCityCountUsedForListings)) {
				$moreCities = true;
				if ( !$previousFindings &&
					 isset($in->out)) {// must be from outside call to do a partial
					$in->out->cities->percents = $this->copyArray($in->out->cities->percents);
					$in->out->cities->tag_percents = $this->copyArray($in->out->cities->tag_percents);
					$in->out->listings->percents = $this->copyArray($in->out->listings->percents);
					$in->out->listings->tag_percents = $this->copyArray($in->out->listings->tag_percents);
					$previousFindings = $in->out;
				}
				$in->tags = $this->copyArray($in->tags);
				$in->price = $this->copyArray($in->price);
				$in->homeOptions = $this->copyArray($in->homeOptions);
				$in->cities = $this->copyArray($in->cities);
				$in->city_sorted = $this->copyArray($in->city_sorted);
				foreach($in->city_sorted as &$city) {
					$city->listings = $this->copyArray($city->listings);
					unset($city);
				}
				// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $in->city_sorted)) );
				$this->log("parseAndReturnResultSet - entered with startCityCountUsedForListings:$in->startCityCountUsedForListings, currentMaxCityCountUsedForListings:$in->currentMaxCityCountUsedForListings"); //, cities:".$ids);
			}
			$this->startCityCountUsedForListings = isset($in->startCityCountUsedForListings) && !empty($in->startCityCountUsedForListings) ? $in->startCityCountUsedForListings : $this->startCityCountUsedForListings;
			$this->currentMaxCityCountUsedForListings = isset($in->currentMaxCityCountUsedForListings) && !empty($in->currentMaxCityCountUsedForListings) ? $in->currentMaxCityCountUsedForListings : $this->currentMaxCityCountUsedForListings;
			$this->log("parseAndReturnResultSet - state:".(empty($in->state) ? "N/A" : $in->state).", location:".(empty($in->location) ? "N/A" :implode(',',array_values($in->location))));
			if (empty($in->location) &&
				empty($in->state))
				return $this->parseAndReturnResultSet2($in, $previousFindings, $moreCities);

			$time = microtime(true);
			$cities_from_listings = [];

			$Listings = $this->getClass('Listings');
			$Tags = $this->getClass('Tags');
			$ListingsTags = $this->getClass('ListingsTags');
			$CitiesTags = $this->getClass('CitiesTags');
			$Cities = $this->getClass('Cities');
			$qq = $this->getClass('QuizQuery');

			// $doingRecursiveWithPartialResults = ($partialResults == null || count($partialResults->listings) == 0) ? 0 : count($partialResults->listings);
			$doingRecursiveWithPartialResults = ($partialResults == null || $partialResults->admitted == 0) ? 0 : $partialResults->admitted;
						   
			if ($moreCities) {
				$r = $in;
				$r->listings = []; // blank slate
			}
			else
				$r = (object) array(
					'updated'=> date("Y-m-d H:i:s"),
					'location'=> empty($in->location) ? [] : $in->location,
					'distance' => empty($in->distance) ? 0 : ( is_int($in->distance) ? $in->distance : (is_numeric($in->distance) ? intval($in->distance) : 0) ),
					'price'=> empty($in->price) ? [$this->options['minPrice'], -1] : $in->price,
					'homeOptions' => !empty($in->homeOptions) ? (array)$in->homeOptions : ['beds'=>'0', 'baths'=>'0'],
					'listings' => ($partialResults == null || !isset($partialResults->listings) || count($partialResults->listings) == 0 ? [] : $partialResults->listings),
					'tags' => $in->tags,
					'cities' =>  ($partialResults == null || count($partialResults->cities) == 0 ? [] : $partialResults->cities),
					'city_sorted' =>  ($partialResults == null || count($partialResults->city_sorted) == 0 ? [] : $partialResults->city_sorted),
					'time' => $partialResults == null ? [] : $partialResults->time,
					'state' => isset($in->state) ? $in->state : '',
					'total_listings_found' => $partialResults == null ? 0 : $partialResults->total_listings_found,
					'admitted' => $partialResults == null ? 0 : $partialResults->admitted,
					'filter' => isset($in->filter) ? $in->filter : 0
				);

			// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $r->city_sorted)) );
			// $this->log("parseAndReturnResultSet @1 ids:$ids");

			$core = "startCityCountUsedForListings:$this->startCityCountUsedForListings, currentMaxCityCountUsedForListings:$this->currentMaxCityCountUsedForListings, doingRecursiveWithPartialResults:$doingRecursiveWithPartialResults";
			$core.= ",moreCities:".($moreCities ? 'yes' : 'no').", cities:".(!empty($r->cities) ? count($r->cities) : 'none').", city_sorted:".(!empty($r->city_sorted) ? count($r->city_sorted) : 'none');
			$core.= ", price:{$r->price[0]},{$r->price[1]}, state:".(!empty($r->state) ? $r->state : "N/A").", location:".(!empty($r->location) ? $r->location[0] : "N/A");
			$core.= ", distance:$r->distance, beds:".(isset($r->homeOptions['beds']) ? $r->homeOptions['beds'] : 0).", baths:".(isset($r->homeOptions['baths']) ? $r->homeOptions['baths'] : 0);
			$core.= ", quizGrabAllIntoCities: ".($this->options['quizGrabAllIntoCities'] ? "true" : "false").", debugLevel:".$this->options['debugLevel'].", filter:".$r->filter;

			$this->log("parseAndReturnResultSet - $core");
			if ($this->options['show_extra_timings'] !== 0) 
				$this->log("tags:".print_r($in->tags, true));

			$this->convertToNumericValues($r);

			// sort tags by percent, if same percent then alphabetically
			usort($r->tags, function($a,$b){
				return $b->percent != $a->percent ? $b->percent - $a->percent : strcasecmp($a->tag, $b->tag);
			});

			// init objects
			$query = new \stdClass();
			$tag_ids = new \stdClass();
			$out = $previousFindings ? $previousFindings : new \stdClass();

			foreach (['listings', 'cities'] as $type){
				$tag_ids->$type = [];
				if (!$previousFindings &&
					!$moreCities)
					$out->$type = (object)[
						'flagged_tags' => [],
						'percents' => [],
						'results' => [],
						'tag_percents' => [],
						'tag_query' => '',
						'tags' => [],
						'total' => 0,
					];
				else { // this mean it's been recursed to find all listing in this city without any tags, keep percentages for any listings found
					$out->$type->tag_query = '';
					$out->$type->tags = [];
					$this->log("parseAndReturnResultSet resetting tag_query and tags for $type for recursion, percent count:".count($out->$type->percents));
				}
			}

			$this->setupTagIds($tag_ids, $r, $out, $Tags);

			$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'before-queries' ]; $time = microtime(true);

			// set max concat length
			$ListingsTags->rawQuery('SET SESSION group_concat_max_len = 16777216');

			// $result = new \stdClass();

			if ($out->listings->tag_query == '' &&
				$out->cities->tag_query == '' &&
				isset($r->location) )
				$in->quiz = ($partialResults == null || 
							(isset($partialResults->listings) && count($partialResults->listings) == 0)) ||
							(isset($partialResults->city_sorted) && count($partialResults->city_sorted) == 0) ? 2 : 3; // force this to two so that it's as though we are searching for all homes in an area.
			else
				$in->quiz = 1; // limit to max count (200)

			// construct the queries
			foreach ([
				'cities' => [
					'field' => 'city_id',
					'class' => &$CitiesTags
				],
				'listings' => [
					'field' => 'listing_id',
					'class' => &$ListingsTags
				]
			] as $type => $type_info) {
				$class = &$type_info['class'];
				$query->$type = empty($out->$type->tag_query) ? '' : "{$out->$type->tag_query}";
				$query->hasTagQuery = !empty($out->$type->tag_query);

				if ($type == 'listings') {
					$in->filter = isset($in->filter) ? $in->filter : 0;
					$range = "AND b.price >= {$r->price[0]}" . ( $r->price[1] > -1 ? " AND b.price <= {$r->price[1]}" : '' );
					$priceRange = '';

					switch($in->filter) {
						case 0: $priceRange = "b.active = 1 $range"; break;
						default:
							if ( $in->filter & QUIZ_ONLY_RENTAL) 
								$priceRange = "b.active = 7";
							else {
								if ( !($in->filter & QUIZ_NO_LOWER_PRICE_LIMIT) ) { // just active = 1 (and maybe rentals)
									if ( $in->filter & QUIZ_INCLUDE_RENTAL ) // with rental
										$priceRange = "((b.active = 1 $range) OR b.active = 7)";
									else
										$priceRange .= "b.active = 1 $range";
								}
								else {	// all listings, except AH_INACTIVE(0)
									$this->options['allowDiscardImages'] = 1;
									if ( $in->filter & QUIZ_INCLUDE_RENTAL ) // with rental
										$priceRange .= "((b.active > 0 AND b.active != 7 $range) OR b.active = 7)";
									else
										$priceRange .= "(b.active > 0 AND b.active != 7 $range)";
								}
								if ( !($in->filter & QUIZ_INCLUDE_COMMERCIAL) ) // no commercial
									$priceRange .= (!empty($priceRange) ? ' AND ' : '').'!(error & '.COMMERCIAL.')';
								else
									$this->options['allowDiscardImages'] = 1;
							}
							
							$priceRange .= (!empty($priceRange) ? ' AND ' : '')."price IS NOT NULL AND city IS NOT NULL";
							$priceRange = "($priceRange)";
							$this->log("priceRange: $priceRange");
							break;
					}

					$query->$type .= (empty(trim($query->$type)) ? "" : " AND ")." $priceRange"; //." %extraCities%";
				}

				// where clause - location
				$cityLocaleIsOK = true;
				$extraCities = '';
				if ($type == 'cities') {
					if ($r->distance > 0 &&
						isset($r->location) &&
						count($r->location)) {
						require_once(__DIR__.'/../_classes/DistanceScale.php');
						global $DistanceScale;

						$locations = '';
						foreach($r->location as $id) {
							$city = $Cities->get((object)(array('where'=>array('id'=>$id))));
							if (empty($city)) throw new \Exception("Failed to find city with id: ".$id);
							$city = array_pop($city);
							
							// $lat = (int)(abs( is_float($city->lat) ? $city->lat : floatval($city->lat) ) / 10);
							$lat = (int)(abs( is_float($city->lat) ? $city->lat : (float)$city->lat ) / 10);
							$deltaLng = $r->distance / $DistanceScale[$lat][0];
							$deltaLat = $r->distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
							$locations .= (!empty($locations) ? ' OR ' : "")."(c.lng >= ".($city->lng - $deltaLng)." AND c.lng <= ".($city->lng + $deltaLng)." AND c.lat >= ".($city->lat - $deltaLat)." AND c.lat <= ".($city->lat + $deltaLat).")";
							unset($city);
						}

						$sql = "SELECT DISTINCT c.id FROM ".$Cities->getTableName($Cities->table)." AS c WHERE ".$locations;
						$cities = $Cities->rawQuery($sql);
						$temp = array();
						foreach($cities as $city)
							$temp[$city->id] = $city;
						$cities = $temp;
						unset($temp);

						$first = true;
						if (!empty($cities)) {
							foreach($r->location as $id) 
								if ( !empty($id) && $id != '' && $id != 0 && !array_key_exists($id, $cities) ) // add original into the list
									$cities[$id] = (object)array('id'=>$id);

							$locations = '';
							foreach($cities as $city) {
								if (!empty($city))
									$locations .=  (!empty($locations) ? ' OR ' : "")."a.city_id = $city->id";
								unset($city);
							}
							if (!empty($locations))
								$query->$type .= (!empty($query->$type) ? ' AND' : '')." ($locations)";
						}
					}
					elseif (isset($r->location) &&
						    count($r->location)) { // city limit
						$locations = '';
						foreach($r->location as $id) {
							if (!empty($id))
								$locations .=  (!empty($locations) ? ' OR ' : "")."a.city_id = $id";
						}
						if (!empty($locations))
							$query->$type .= (!empty($query->$type) ? ' AND' : '')." ($locations)";
					}
					elseif (isset($r->state)) { // tag query will never be empty for a state search.
						$query->$type .= " AND b.state = '$r->state'";
						$this->log("parseAndReturnResultSet - for cities in state mode: ".$query->$type);
					}
					else
						throw new \Exception("No location defined!");
				}
				else { // then 'listings'
					if ( empty($r->cities) ) {// no cities?!
						break; // see if we can recurse...
					}
					$first = true;
					$index = 0;
					$keys = array_keys($r->cities);
					$cityCounter = $this->startCityCountUsedForListings;

					if ($cityCounter == 0)
						$this->log("parseAndReturnResultSet - all cities to consider for run, len:".count($r->cities).", list:".implode(',', $keys));

					$extraCities = " AND (";
					foreach( $keys as $city) {
						if (empty($city))
							continue;
						if ($index < $cityCounter) {
							$index++;
							continue;
						}
						$first ? $first = false : $extraCities .= ' OR ';
						$extraCities .= "(b.city_id = $city)";
						if ($cityCounter == $this->currentMaxCityCountUsedForListings)
						unset($city);
						$cityCounter++;
						$index++;
						if ($cityCounter == $this->currentMaxCityCountUsedForListings)
							break;
					}
					$extraCities .= ")";
					$this->startCityCountUsedForListings = $cityCounter;
				}

				// finish creating query
				if (!empty($query->$type) && $type == 'listings'){
					$this->getListings($query, $type, $r, $class, $out, $time, $qq, $in, $extraCities, $cities_from_listings, $doingRecursiveWithPartialResults );
				} elseif (!$doingRecursiveWithPartialResults && !$moreCities && $type == 'cities' ) {
						  // !empty($query->$type) && $type == 'cities' && $cityLocaleIsOK ) {
					$this->getCities($query, $type, $r, $class, $out, $time, $qq, $in, $cities_from_listings);
				}
				unset($type, $type_info);
			}

			if ($doingRecursiveWithPartialResults && 
				!$moreCities) {
				$out->cities = $previousFindings->cities;
				$this->log("previousFindings - cities: ".print_r($out->cities, true));
			}

			$this->postProcessData($r, $out, $time, $in, $Cities, $qq, $doingRecursiveWithPartialResults, $query, $cities_from_listings);

			if ( !$recursed &&
				 !isset($r->listings) && // should have been unset if it was doing it by cites
				 count($r->city_sorted) < $this->options['max_cities'] ) { // we don't have 50 yet
				if ( count($out->cities->percents) > $this->currentMaxCityCountUsedForListings) { // there are more cities to be picked off
					$this->currentMaxCityCountUsedForListings += $this->options['cities_chunk_size']; // do some more
					$r->sessionID = $in->sessionID;
					$r->startCityCountUsedForListings = $this->startCityCountUsedForListings;
					$r->currentMaxCityCountUsedForListings = $this->currentMaxCityCountUsedForListings;
					if (!$this->options['allowPartialQuizResults'] ||
						!$r->admitted)
						$r = $this->parseAndReturnResultSet($r, false, null, $out, true);
				}
				else
					$this->options['allowPartialQuizResults'] = 0; // allow recursion to happen if needed
			}
			else
				$this->options['allowPartialQuizResults'] = 0; // done with any partial result gathering
			$r->allowPartialQuizResults = $this->options['allowPartialQuizResults'];
			if ( $allowPartialQuizResults ) $r->out = $out;

			if (empty($r->state)) { // then doing a city localization
				if (!$this->options['allowPartialQuizResults'] && // else return partial results
					!empty($r->cities) &&
					count($out->cities->flagged_tags) == 0 &&
					count($out->listings->flagged_tags) == 0 &&
					((!$this->options['quizGrabAllIntoCities'] &&
					  count($r->listings) < $this->options['local_recursion_trigger']) ||
					 ($this->options['quizGrabAllIntoCities'] &&
					  $r->admitted < $this->options['local_recursion_trigger'])) ) { 	// either default of 12 or Options::QuizLocalSearchRecurseTrigger
					if (!$recursed) { // so the gist of it is, if the number of listings found is less than this, we go and retry the search with no tags, grabbing everything in the city to show..
						$this->log("parseAndReturnResultSet - retrying - found $r->admitted results for $core with tags:". implode(',',array_values(array_map( function($ele) { return $ele->tag.":$ele->id"; }, $in->tags))).", cities:".implode(',',array_values(array_map( function($ele) { return $ele->city.":$ele->id"; }, $r->cities))));
						$oldTags = $in->tags;
						// $oldCities = $r->cities;
						$oldHomeOptions = $r->homeOptions;
						unset($in->tags);
						$in->tags = [];
						$in->homeOptions = [];
						$copiedToListingsArray = false;

						if ($this->options['quizGrabAllIntoCities'] &&
							(!isset($r->listings) ||
							 empty($r->listings)) &&
							!empty($r->city_sorted)) {
							$copiedToListingsArray = true;
							if (!isset($r->listings)) $r->listings = [];
							foreach($r->city_sorted as $city) {
								foreach($city->listings as $l) {
									$r->listings[] = $l;
									unset($l);
								}
								unset($city);
							}
						}

						$hadPartial = isset($r->listings) && count($r->listings) > 0;
						if (isset($in->startCityCountUsedForListings) && $in->startCityCountUsedForListings) unset($in->startCityCountUsedForListings);
						if (isset($in->currentMaxCityCountUsedForListings) && $in->currentMaxCityCountUsedForListings) unset($in->currentMaxCityCountUsedForListings);
						$this->startCityCountUsedForListings = 0; // else won't get the cities
						$this->currentMaxCityCountUsedForListings = count($r->cities); // do all of them
						$r = $this->parseAndReturnResultSet($in, true, $r, $out);
						$r->tags = $oldTags; // restore tags
						// if ( !$this->options['quizGrabAllIntoCities'] ) 
						// 	$r->cities = $oldCities; // restore previous findings
						$r->homeOptions = $oldHomeOptions;
						$haveResults = false;
						if ( !$this->options['quizGrabAllIntoCities'] ) {
							$this->log("after recursion - cities:".print_r($r->cities, true));

							if (isset($r->listings) && count($r->listings) == 0) 
								$msg = "There are no listings for this city in our database.";
							else {
								$haveResults = true;
							}
						}
						else {
							$this->log("parseAndReturnResultSet after recursion, copiedToListingsArray:".($copiedToListingsArray ? 'yes' : 'no').", count: city_sorted:".count($r->city_sorted).", listings:".(isset($r->listings) ? count($r->listings) : 0).", admitted:".$r->admitted);
							if (count($r->city_sorted) &&
								isset($r->listings)) {
								unset($r->listings);
								$this->log("parseAndReturnResultSet after recursion dumping listings array");
							}
							if ($r->admitted == 0)
								$msg = "There are no listings for this city in our database.";
							else {
								$haveResults = true;
								$this->log("parseAndReturnResultSet has some results:".print_r($r->city_sorted, true));
							}
						}

						if ($haveResults) {
							if (!$hadPartial)
								$msg = $r->admitted." listings are ".($r->location ? 'near' : 'in')." this city, but none with ".(count($oldTags) > 1 ? "all these tags: " : " this tag: ");
							else
								$msg = $r->admitted." listings are ".($r->location ? 'near' : 'in')." this city, and some matched ".(count($oldTags) > 1 ? " some of these tags: " : " this tag: ");
							$first = true;
							$r->showFailureMsg = true;
							foreach($oldTags as $tag) {
								$tag = (object)$tag;
								$msg .= ($first ? '' : ', ')."$tag->tag";
								$first = false;
							}
						}
						$r->failureMsg = $msg;
					}
					else
						$this->log("parseAndReturnResultSet - recursed - found $r->admitted results for $core with tags:".print_r($in->tags, true));
				}
			}
			elseif (!empty($out->cities->tags) &&
					empty($r->cities)) {
				$msg = '';
				$msg = $r->admitted." listings are in this state, and no cities with ".(count($out->cities->tags) > 1 ? "all these tags: " : " this tag: ");
				$first = true;
				foreach($out->cities->tags as $tag) {
					$tag = (object)$tag;
					$msg .= ($first ? '' : ', ')."$tag->tag";
					$first = false;
				}
			}
			
			return $r;
		} catch (\Exception $e) { parseException($e); }
	}

	// call when $in->location and $in->state are empty!
	public function parseAndReturnResultSet2($in, $previousFindings = null, $moreCities = false){
		try {
			$time = microtime(true);
			$cities_from_listings = [];

			$Listings = $this->getClass('Listings');
			$Tags = $this->getClass('Tags');
			$ListingsTags = $this->getClass('ListingsTags');
			$CitiesTags = $this->getClass('CitiesTags');
			$Cities = $this->getClass('Cities');
			$qq = $this->getClass('QuizQuery');

			$allowPartialQuizResults = isset($in->allowPartialQuizResults) ? $in->allowPartialQuizResults : $this->options['allowPartialQuizResults'];
		
			if ($moreCities) {
				$r = $in;
				$r->listings = []; // blank slate
			}
			else
				$r = (object) array(
					'updated'=> date("Y-m-d H:i:s"),
					'location'=> empty($in->location) ? [] : $in->location,
					'distance' => empty($in->distance) ? 0 : ( is_int($in->distance) ? $in->distance : (is_numeric($in->distance) ? intval($in->distance) : 0) ),
					'price'=> empty($in->price) ? [$this->options['minPrice'], -1] : $in->price,
					'homeOptions' => !empty($in->homeOptions) ? (array)$in->homeOptions : ['beds'=>'0', 'baths'=>'0'],
					'listings' => [],
					'tags' => $in->tags,
					'cities' => [],
					'time' => [],
					'state' => isset($in->state) ? $in->state : '',
					'total_listings_found' => 0,
					'admitted' => 0,
					'filter' => isset($in->filter) ? $in->filter : 0
				);

			$core = "parseAndReturnResultSet2 - price:{$r->price[0]},{$r->price[1]}, beds:".(isset($r->homeOptions['beds']) ? $r->homeOptions['beds'] : 0).", baths:".(isset($r->homeOptions['baths']) ? $r->homeOptions['baths'] : 0);
			$core.= "allowPartialQuizResults:$allowPartialQuizResults,startCityCountUsedForListings:$this->startCityCountUsedForListings, currentMaxCityCountUsedForListings:$this->currentMaxCityCountUsedForListings";
			$core.= ",moreCities:$moreCities".", filter:".$r->filter;
			$this->log($core);

			if ($this->options['show_extra_timings'] !== 0)
				$this->log("tags:".print_r($in->tags, true));

			$this->convertToNumericValues($r);

			// sort tags by percent, if same percent then alphabetically
			usort($r->tags, function($a,$b){
				return $b->percent != $a->percent ? $b->percent - $a->percent : strcasecmp($a->tag, $b->tag);
			});

			// init objects
			$query = new \stdClass();
			$tag_ids = new \stdClass();
			$out = $previousFindings ? $previousFindings : new \stdClass();

			foreach (['listings', 'cities'] as $type){
				$tag_ids->$type = [];
				if (!$moreCities)
					$out->$type = (object)[
						'flagged_tags' => [],
						'percents' => [],
						'results' => [],
						'tag_percents' => [],
						'tag_query' => '',
						'tags' => [],
						'total' => 0,
					];
				else { // this mean it's been recursed to get more cities keep percentages for any listings found
					$out->$type->tag_query = '';
					$out->$type->tags = [];
					$this->log("parseAndReturnResultSet2 resetting tag_query and tags for $type for recursion, percents count:".count($out->$type->percents));
				}
			}

			$this->setupTagIds($tag_ids, $r, $out, $Tags);

			$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'before-queries' ]; $time = microtime(true);

			// set max concat length
			$ListingsTags->rawQuery('SET SESSION group_concat_max_len = 16777216');

			// $result = new \stdClass();

			if ($out->listings->tag_query == '' &&
				$out->cities->tag_query == '' &&
				(isset($r->location) &&
				 !empty($r->location)))
				$in->quiz = 2; // force this to two so that it's as though we are searching for all homes in an area.
			elseif ( !empty($out->cities->tag_query) )
				$in->quiz = 1; // limit to max count (200)
			else {
				$r->listings = [];
				$r->admitted = 0;
				$r->showFatalMsg = 1;
				$r->failureMsg = "Cannot do a nationwide search without any city tags";
				$r->city_sorted = [];
				$this->log("parseAndReturnResultSet2 - $r->showFailureMsg");
				return $r;
			}

			// construct the queries
			foreach ([
				'cities' => [
					'field' => 'city_id',
					'class' => &$CitiesTags
				],
				 'listings' => [
					'field' => 'listing_id',
					'class' => &$ListingsTags
				]
			] as $type => $type_info) {
				$class = &$type_info['class'];
				$query->$type = empty($out->$type->tag_query) ? '' : "{$out->$type->tag_query}";

				if ($type == 'listings') {
					$in->filter = isset($in->filter) ? $in->filter : 0;
					$range = "AND b.price >= {$r->price[0]}" . ( $r->price[1] > -1 ? " AND b.price <= {$r->price[1]}" : '' );
					$priceRange = '';

					switch($in->filter) {
						case 0: $priceRange = "b.active = 1 $range"; break;
						default:
							if ( $in->filter & QUIZ_ONLY_RENTAL) 
								$priceRange = "b.active = 7";
							else {
								if ( !($in->filter & QUIZ_NO_LOWER_PRICE_LIMIT) ) { // just active = 1 (and maybe rentals)
									if ( $in->filter & QUIZ_INCLUDE_RENTAL ) // with rental
										$priceRange = "((b.active = 1 $range) OR b.active = 7)";
									else
										$priceRange .= "b.active = 1 $range";
								}
								else {	// all listings, except AH_INACTIVE(0)
									$this->options['allowDiscardImages'] = 1;
									if ( $in->filter & QUIZ_INCLUDE_RENTAL ) // with rental
										$priceRange .= "((b.active > 0 AND b.active != 7 $range) OR b.active = 7)";
									else
										$priceRange .= "(b.active > 0 AND b.active != 7 $range)";
								}
								if ( !($in->filter & QUIZ_INCLUDE_COMMERCIAL) ) // no commercial
									$priceRange .= (!empty($priceRange) ? ' AND ' : '').'!(error & '.COMMERCIAL.')';
								else
									$this->options['allowDiscardImages'] = 1;
							}
							
							$priceRange .= (!empty($priceRange) ? ' AND ' : '')."price IS NOT NULL AND city IS NOT NULL";
							$priceRange = "($priceRange)";
							$this->log("priceRange: $priceRange");
							break;
					}

					$query->$type .= (empty(trim($query->$type)) ? "" : " AND ")." $priceRange"; //." %extraCities%";
				}

				// where clause - location
				$cityLocaleIsOK = true;
				$extraCities = '';
				if (!empty($r->cities)){
					if ($type == 'listings') {
						$first = true;
						$index = 0;
						$cityCounter = $this->startCityCountUsedForListings;
						$this->log("parseAndReturnResultSet2 - before getting cities startCityCountUsedForListings:$this->startCityCountUsedForListings, currentMaxCityCountUsedForListings:$this->currentMaxCityCountUsedForListings");

						$extraCities = " AND (";
						foreach($r->cities as $city=>$data) {
							if (empty($city))
								continue;
							if ($index++ < $cityCounter)
								continue;
							$first ? $first = false : $extraCities .= ' OR ';
							$extraCities .= "(b.city_id = $city)";
							unset($city);
							$cityCounter++;
							if ($cityCounter == $this->currentMaxCityCountUsedForListings)
								break;
						}
						$extraCities .= ")";
						$this->startCityCountUsedForListings = $cityCounter;
						$this->log("parseAndReturnResultSet2 - after getting cities:$extraCities");
						// }
						if ($this->options['show_extra_timings'] !== 0)
							$qq->add((object)array('session_id'=>$in->sessionID,
												   'type'=>'city-id-query',
												   'data'=>$extraCities));

						// $query->location .= $locations." ) d ON d.listing_id = b.id ";
					}
				}
				elseif ($type == 'listings') {
					$this->log("parseAndReturnResultSet2 has no cities to check for listings.");
					break;
				}

				// finish creating query
				if (!empty($query->$type) && $type == 'listings'){
					$this->getListings($query, $type, $r, $class, $out, $time, $qq, $in, $extraCities, $cities_from_listings);
				} elseif (!$moreCities && !empty($query->$type) && $type == 'cities' && $cityLocaleIsOK ) {
					$this->getCities($query, $type, $r, $class, $out, $time, $qq, $in, $cities_from_listings);
				}
				unset($type, $type_info);
			}

			$this->postProcessData($r, $out, $time, $in, $Cities, $qq, false, $query);

			if ( !isset($r->listings) &&
				 !empty($r->cities) && // should have been unset if it was doing it by cites
				 count($r->city_sorted) < $this->options['max_cities'] ) { // we don't have 50 yet
				if ( count($out->cities->percents) > $this->currentMaxCityCountUsedForListings) { // there are more cities to be picked off
					$this->currentMaxCityCountUsedForListings += $this->options['cities_chunk_size']; // do some more
					$r->sessionID = $in->sessionID;
					$r->startCityCountUsedForListings = $this->startCityCountUsedForListings;
					$r->currentMaxCityCountUsedForListings = $this->currentMaxCityCountUsedForListings;
					if (!$this->options['allowPartialQuizResults'] ||
						!$r->admitted)
						$r = $this->parseAndReturnResultSet2($r, $out, true);
				}
				else
					$this->options['allowPartialQuizResults'] = 0; // allow recursion to happen if needed
			}
			else
				$this->options['allowPartialQuizResults'] = 0; // done with any partial result gathering

			$r->allowPartialQuizResults = $this->options['allowPartialQuizResults'];
			if ( $allowPartialQuizResults ) $r->out = $out;

			return $r;
		} catch (\Exception $e) { parseException($e); }

	}

	protected function getListings(&$query, $type, &$r, $class, &$out, $time, $qq, $in, $extraCities, &$cities_from_listings, $doingRecursiveWithPartialResults = false) {
		// $what = $in->quiz == 2 ? "a.listing_id, b.city_id, b.city, b.state, b.price," : "a.listing_id, b.city_id, b.city, b.state,";
		$what = "a.listing_id, b.city_id, b.city, b.state, b.price,";
		if ($this->options['quizGrabAllIntoCities']) $what .= " b.images, b.first_image, b.list_image, b.beds, b.baths,";
		//$query->$type = "SELECT a.listing_id, b.city_id, GROUP_CONCAT(a.tag_id) as tags FROM {$class->getTableName('listings-tags')} as a INNER JOIN {$class->getTableName('listings')} as b ON a.listing_id = b.id ".(isset($query->location) ? $query->location : '')." WHERE {$query->$type} GROUP BY a.listing_id";
		$where = $query->$type;
		$query->$type = "SELECT ".$what." GROUP_CONCAT(a.tag_id) as tags FROM {$class->getTableName('listings-tags')} as a ";
		$query->$type.= "INNER JOIN {$class->getTableName('listings')} as b ON a.listing_id = b.id ";
		$query->$type.= "WHERE $where ".(isset($extraCities) ? $extraCities : '')." GROUP BY b.id";
		//$query->$type = str_replace("%extraCities%", $extraCities, $query->$type);

		$qq->add((object)array('session_id'=>$in->sessionID,
							   'type'=>$type.'-query',
							   'data'=>$query->$type));

		if (isset($extraCities))
			$this->log("getListings - cities:".$extraCities);

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'before-listing-query' ]; $timeBeginParse = microtime(true);
		// run listings query and parse results, store city ids for later

		if ($doingRecursiveWithPartialResults) {
			if ($this->options['debugLevel'] > 1)
				$this->log("getListings - recursion call, previous listing results:".print_r($r->listings, true));
			$temp = []; // reset $r->listings so that it is row-id indexed
			if (count($r->listings)) foreach($r->listings as $row) {
				$out->listings->percents[$row->id] = $row->percent;
				$out->listings->results[$row->id] = $row;
				$temp[$row->id] = $row;
			}
			$r->listings = $temp;
			unset($temp);
		}

		// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $r->city_sorted)) );
		// $this->log("getListings entered ids:$ids");

		$page = 0;
		$pagePer = 60000;
		$done = false;
		while ( !$done ) {
			$result = $class->rawQuery( $query->$type.' LIMIT '.($page * $pagePer).','.$pagePer);
			$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'after-listing-query' ]; $time = microtime(true);

			if (!empty($result)) {
				$cr = count($result);
				$qq->add((object)array('session_id'=>$in->sessionID,
							   'type'=>'listing-read',
							   'data'=>'read in '.($cr + ($page*$pagePer)).', page:'.$page.', memory usage:'.memory_get_usage()));
				$page++;
				foreach ($result as &$row){
					// parse result
					// $row->id = intval($row->listing_id); 
					$row->id = (int)$row->listing_id; 
					unset($row->listing_id);
					// $row->city_id = intval($row->city_id);
					$row->city_id = (int)$row->city_id;
					$row->percent = 0;
					//if ($in->quiz == 2)
					// $row->price = intval($row->price);
					$row->price = (int)$row->price;
					// if ($this->options['quizGrabAllIntoCities']) {
					// 	$row->images = $row->images ? json_decode($row->images) : null;
					// 	$row->first_image = $row->first_image ? json_decode($row->first_image) : null;
					// }
					// if (!empty($row->tags))
					// 	$row->tags = explode(',', $row->tags);

					// parse tags and listing percent, add to percent array
					if (!empty($row->tags)) {
						$row->tags = explode(',', $row->tags);
						// foreach ($row->tags as $i => $tag_id){
						foreach ($row->tags as $i=>&$tag_id){
							// $tag_id = intval($tag_id);
							$tag_id = (int)$tag_id;

							// if (!empty($out->listings->tag_percents) && isset($out->listings->tag_percents[$tag_id]))
							if (isset($out->listings->tag_percents[$tag_id]))
								$row->percent += $out->listings->tag_percents[$tag_id];

							// $row->tags[$i] = $tag_id;
							// $row->tags[$i] = $tag_id;
							// unset($tag_id, $i);
							unset($tag_id);
						}
					}

					$keep = true;

					// if listing tags are flagged and not present in row, $keep = false
					if (!empty($out->listings->flagged_tags))
						foreach ($out->listings->flagged_tags as $tag_id) {
							if (empty($row->tags) || !in_array($tag_id, $row->tags)){
								$keep = false;
								break;
							}
							unset($tag_id);
						}

					if ($keep){
						$row->percent = round($row->percent);
						// save the percent for sorting
						if (!isset($out->listings->percents[$row->id]) &&
							!isset($out->listings->results[$row->id])) { // don't overwrite results from pre-recursion
							$out->listings->percents[$row->id] = $row->percent;
							$out->listings->results[$row->id] = $row;
						}
					}

					unset($row, $keep);
				}
				unset($result);
			}
			else
				$done = true;						
		}

		// $this->log("getListings - post loop with out->listings->results:".print_r($out->listings->results, true));

		$record = [ 'time' => microtime(true) - $timeBeginParse, 'action' => 'after-listing-parse' ]; $time = microtime(true);
		$r->time[] = $record;
		if ($this->options['show_extra_timings'] !== 0)
			$qq->add((object)array('session_id'=>$in->sessionID,
								   'type'=>'after-listing-parse',
								   'data'=>json_encode($record)));

		$temp_cities = [];
		$Cities = $this->getClass('Cities');
		foreach ($out->listings->percents as $id => $percent)
			if ( isset($out->listings->results[$id]) ){
				if (empty($out->listings->results[$id]->city_id) ||
					$out->listings->results[$id]->city_id == -1) {// uh oh!  try to get it...
					$city = $Cities->get((object)['like'=>['city'=>$out->listings->results[$id]->city],
												  'like2'=>['state'=>$out->listings->results[$id]->state]]);
					if (!empty($city)) {
						$this->log("found a city:{$city[0]->id}, {$city[0]->city} {$city[0]->state}");
						$out->listings->results[$id]->city_id = $city[0]->id;
						$x = $this->getClass('Listings')->set([(object)['where'=>['id'=>$id],
											 							'fields'=>['city_id'=>$city[0]->id]]]);
						$this->log("updating listing:$id with cityId:".$city[0]->id." - ".$out->listings->results[$id]->city.", ".$out->listings->results[$id]->state." was ".(!empty($x) ? "successful" : " a failure"));
					}
					else {
						$this->log("failed to get cityId for listing:$id - ".$out->listings->results[$id]->city.", ".$out->listings->results[$id]->state);
						continue;
					}
				}
				if (!isset($r->listings[$id])) // this may be redundant with the check above for !isset($out->listings->percents[$row->id]) && !isset($out->listings->results[$row->id])
					$r->listings[$id] = $out->listings->results[$id];
				elseif ($doingRecursiveWithPartialResults) {
					// if ($r->admitted) 
					// 	$r->admitted--;
					if ($r->total_listings_found)
						$r->total_listings_found--;
					// $this->log("duplicate listing with doingRecursiveWithPartialResults:$doingRecursiveWithPartialResults, so decrement admitted to:$r->admitted, total_listings_found to:$r->total_listings_found");
					$this->log("duplicate listing with doingRecursiveWithPartialResults:$doingRecursiveWithPartialResults, so decrement total_listings_found to:$r->total_listings_found");
				}

				if (!empty($out->listings->results[$id]->city_id)) 
					isset($temp_cities[$out->listings->results[$id]->city_id]) ? $temp_cities[$out->listings->results[$id]->city_id]++ : $temp_cities[$out->listings->results[$id]->city_id] = 1;
				unset( $out->listings->results[$id], $id, $percent );
			}

		// NOTE: we are losing the number of listings per city here...
		$cities_from_listings = array_keys($temp_cities);
		unset($Cities);

		$record = [ 'time' => microtime(true) - $time, 
					'action' => 'after-listing-post-parse',
					'resultCount' => count($r->listings) ]; $time = microtime(true);
		$r->time[] = $record;
		if ($this->options['show_extra_timings'] !== 0)
			$qq->add((object)array('session_id'=>$in->sessionID,
								   'type'=>'after-listing-post-parse',
								   'data'=>json_encode($record)));

		if ($this->options['debugLevel'] > 1)
			$this->log("getListings - doingRecursiveWithPartialResults:$doingRecursiveWithPartialResults, results:".print_r($r->listings, true));

		// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $r->city_sorted)) );
		// $this->log("getListings exiting ids:$ids");
	}

	protected function addCity($query, $type, &$r, $class, &$out, $time, $qq, $in, $city_id) {

	} 

	protected function getCities(&$query, $type, &$r, $class, &$out, $time, $qq, $in, $cities_from_listings) {
		$what = "a.city_id, ";
		$concat = "a.tag_id,':',a.score";
		if ($this->options['quizGrabAllIntoCities']) {
			$what .= "b.image, b.city, b.state, b.lng, b.lat, ";
			$concat .=",':',c.tag";
		}
		$where = $query->$type;
		$this->log("getCities - where: $where");
		if ( !empty($out->cities->tag_query) ) {
			$sql = "SELECT $what GROUP_CONCAT($concat) as tags FROM {$class->getTableName('cities-tags')} AS a ";
			if ($this->options['quizGrabAllIntoCities']) {
				$sql.= "INNER JOIN {$class->getTableName('cities')} AS b ON b.id = a.city_id ";
				$sql.= "INNER JOIN {$class->getTableName('tags')} AS c ON c.id = a.tag_id ";
			}
			$sql.= "WHERE {$where} GROUP BY b.id";
		}
		else {
			$where = str_replace('city_id', 'id', $where);
			$sql = "SELECT id as city_id, image, city, state, lng, lat FROM {$class->getTableName('cities')} AS a ";
			$sql.= "WHERE {$where} GROUP BY a.id";
		}
		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'before-city-query' ]; $time = microtime(true);

		$qq->add((object)array('session_id'=>$in->sessionID,
							   'type'=>$type.'-query',
							   'data'=>$sql));

		$result = $class->rawQuery( $sql );
		$temp = [];

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'after-city-query' ]; $time = microtime(true);
		$qq->add((object)array('session_id'=>$in->sessionID,
							   'type'=>'cities-read',
							   'data'=>'read in '.count($result).', memory usage:'.memory_get_usage()));

		if (empty($result) &&
			count($cities_from_listings)) {
			$result = $this->getClass('Cities')->get((object)['where'=>['id'=>$cities_from_listings]]);
			$qq->add((object)array('session_id'=>$in->sessionID,
							   'type'=>'cities-read-using-cities_from_listings',
							   'data'=>'read in '.count($result).', memory usage:'.memory_get_usage()));
		}

		// run cities query and parse results
		if (!empty( $result ))
			foreach ($result as &$row){
				// parse result
				// $row->id = intval($row->city_id); unset($row->city_id);
				$row->id = (int)$row->city_id; unset($row->city_id);
				$row->percent = 0;

				// parse the city tags
				if (!empty($row->tags)) // expects (string) tag_id1:score1, tag_id2:score2, ... , tag_idN:scoreN
					$row->tags = explode(',', $row->tags);

				if (!empty($row->tags)){
					$new_tags = [];
					$tag_name = [];
					foreach ($row->tags as $tag_info) // expects (string) tag_id:score
						if (!empty($tag_info)){
							$tag_info = explode(':', $tag_info);
							// $tag_id = intval($tag_info[0]);
							$tag_id = (int)$tag_info[0];
							if (count($tag_info) >= 2)
								// $score = intval($tag_info[1]);
								$score = (int)$tag_info[1];
							else
								$score = 0;

							// if tag has no score, set it to max score
							if ($score == -1)
								$score = $this->options['max_city_score'];

							// add to the city percent
							if ( $score && !empty($out->cities->tag_percents) && isset($out->cities->tag_percents[$tag_id]) )
								$row->percent += round( $out->cities->tag_percents[$tag_id] * ( $score / $this->options['max_city_score'] ) );

							$new_tags[$tag_id] = $score;
							$tag_name[$tag_id] = $this->options['quizGrabAllIntoCities'] ? $tag_info[2] : '';
							unset($tag_info, $tag_id, $score);
						}

					if (count($new_tags) > 1) {
						arsort($new_tags);
						// $new_tags = array_reverse( $new_tags, true);
					}
					// new_tags: (array) [ tag_id1: score1, tag_id2: score2, ... ]
					$row->tags = $new_tags;
					if ( $this->options['quizGrabAllIntoCities'] ) {
					 	if (!isset($row->image) ||
					      	 empty($row->image) ||
					      	 (isset($row->image) &&
					      	 !file_exists($this->options['imgPathCity'].'/'.$row->image)) ) {
					 		$orig = '';
					 		if (isset($row->image) &&
					      	 	!file_exists($this->options['imgPathCity'].'/'.$row->image)) {
					 			// $this->log("getCities - could not locate:".$this->options['imgPathCity'].'/'.$row->image.", 'DOCUMENT_ROOT': {$_SERVER['DOCUMENT_ROOT']}");
					      	 	$orig = $row->image;
					      	}

						  	if (count($new_tags) && strlen($top_tag = $tag_name[ array_keys($new_tags)[0]]) )
						  		$row->image = strtolower( str_replace(' ', '_', $top_tag) ).'.jpg';
						  	else {
						  		$sql = "SELECT b.id, b.tag, a.score FROM {$class->getTableName('cities-tags')} AS a ";
						  		$sql.= "INNER JOIN {$class->getTableName('tags')} AS b ON b.id = a.tag_id ";
						  		$sql.= "WHERE a.city_id = $row->id";
						  		$tags = $class->rawQuery($sql);
						  		if (!empty($tags)) {
						  			$highest = -1;
						  			$name = '';
						  			foreach($tags as $tag) {
						  				// $tag->score = intval($tag->score) == -1 ? $this->options['max_city_score'] : intval($tag->score);
						  				$tag->score = (int)$tag->score == -1 ? $this->options['max_city_score'] : (int)$tag->score;
						  				if ($tag->score > $highest) {
						  					$highest = $tag->score;
						  					$name = $tag->tag;
						  				}
						  				unset($tag);
						  			}
						  			$row->image = strtolower( str_replace(' ', '_', $name) ).'.jpg';
						  			unset($tags);
						  		}
						  		else
						  			$row->image = "_blank.jpg";
						  	}

						  	if (!file_exists($this->options['imgPathCity'].'/'.$row->image))
						  		$row->image = "_blank.jpg";

						  	if (!empty($orig))
						  		$this->log("getCities - city:$row->id: city image change from $orig to $row->image");
						}
					}

					unset($new_tags, $tag_name);
				}
				else if (empty($row->image))
					$row->image = "_blank.jpg";


				$keep = true;

				if (!empty($out->cities->flagged_tags))
					foreach ($out->cities->flagged_tags as $tag_id)
						if (empty($row->tags) || !array_key_exists( $tag_id, $row->tags )){
							$keep = false;
							break;
						}

				// if (!$keep)
				// 	$row->percent = 0;

				// save the percent for sorting
				if ($keep) {
					if (!isset($out->cities->percents[$row->id]))
						$out->cities->percents[$row->id] = $row->percent;
					$temp[$row->id] = $row;
				}

				unset($row, $keep);
			}

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'cities-inital-process' ]; $time = microtime(true);
		if ($this->options['show_extra_timings'] !== 0)
			$qq->add((object)array('session_id'=>$in->sessionID,
								   'type'=>'cities-inital-process',
								   'data'=>'read in '.count($result).', memory usage:'.memory_get_usage()));

		

		// sort the results, save the top 200 + any cities from listings
		arsort($out->cities->percents);
		// $out->cities->percents = array_reverse($out->cities->percents, true);

		$temp_percents = [];
		foreach ($out->cities->percents as $city_id => $percent){
			if (count($temp_percents) < $this->options['max_cities_init'] ||
				(!empty($cities_from_listings) && in_array( $city_id, $cities_from_listings )) )
				$temp_percents[$city_id] = $percent;
			unset($city_id, $percent);
		}

		$out->cities->percents = $temp_percents;
		unset($temp_percents);

		// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $r->city_sorted)) );
		$ids = implode(',', array_keys($out->cities->percents));
		$this->log("getCities: out->cities->percents:$ids");


		// keep only the city info for the results we want

		foreach ($out->cities->percents as $id => $percent){
			if ( isset($temp[$id]) &&
				!isset($r->cities[$id]) ) { // keep cities from pre-recursion
				$r->cities[$id] = $temp[$id];
				unset($temp[$id]);
			}
			unset($id, $percent);
		}

		$this->swapMainCity($r);
		$ids = implode(',', array_keys($r->cities));
		$this->log("getCities: r->cities:$ids");
		//$this->log('getCities - after-city-parse city:'.print_r($r->cities, true));

		unset($temp);

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'after-city-parse' ]; $time = microtime(true);
		if ($this->options['show_extra_timings'] !== 0)
			$qq->add((object)array('session_id'=>$in->sessionID,
								   'type'=>'after-city-parse',
								   'data'=>'read in '.count($result).', memory usage:'.memory_get_usage()));

		unset($result);
	}

	private function haveActiveParallelProcessing() {
		$Options = $this->getClass('Options');
		$x = $Options->get((object)['where'=>['opt'=>'activePP']]);
		if (empty($x))
			$x = $Options->get((object)['where'=>['opt'=>'activeParseToDb']]);
		return !empty($x);
	}

	private function getFirstImage($listing) {
		$haveFile = false;
		if ( ($haveFile = isset($listing->image_path) && !empty($listing->image_path)) ) {
			if ( substr($listing->image_path, 0, 4 ) != 'http') {
				if (file_exists($this->options['imgPath'].'/'.$listing->image_path)) {
					return $listing->image_path;
				}
				else
					$haveFile = false;
			}
			else
				return $listing->image_path;
		}

		if ( !$haveFile &&
			 ($haveFile = !empty($listing->list_image)) ) {
			if ( substr($listing->list_image->file, 0, 4 ) != 'http') {
				if ($this->options['checkListImageExists']) {
					if (file_exists($this->options['imgPath'].'/'.$listing->list_image->file))  {
						if ($this->options['doubleCheckListImageExists']) {
							if (file_exists($this->options['imgPathSubSize'].'/'.$listing->list_image->file)) {
								$listing->list_image = (object)$listing->list_image;
								return $listing->list_image->file;
							}
							else {
								$this->log("getFirstImage found the uploaded image for id:$listing->id, but could not find it in ".$this->options['imgPathSubSize']." folder: ".$listing->list_image->file.", resetting list_image to NULL");
								$this->getClass('Listings')->set([(object)['where'=>['id'=>$listing->id],
																		   'fields'=>['list_image'=>'NULL']]]);
								$haveFile = false;
							}
						}
						else
							return $listing->list_image->file;
					}
					else
						$haveFile = false;
				}
				else
					return $listing->list_image->file;
			}
			else
				$haveFile = false;
		}

		if ( !$haveFile &&
			 ($haveFile = !empty($listing->first_image)) ) {
			if ( (substr($listing->first_image->file, 0, 4 ) != 'http' &&
				  file_exists($this->options['imgPath'].'/'.$listing->first_image->file)) ||
				  substr($listing->first_image->file, 0, 4 ) == 'http') {
				$listing->first_image = (object)$listing->first_image;
				return removeslashes($listing->first_image->file);
			}
			else
				$haveFile = false;
		}

		if (!$haveFile) {
			if (!empty($listing->images)) {
				foreach($listing->images as $i=>$img) {
					$img = (object)$img;
					if ( !empty($img->file) ||
						 ($this->options['allowDiscardImages'] &&
						 !empty($img->discard)) ) { // can be a discard
						$file = !empty($img->file) ? $img->file : $img->discard;
						$isHttp = substr($file, 0, 4 ) == 'http';
						if ($isHttp) {
							return removeslashes($file);
						}
						elseif ($this->options['haveActiveParallelProcessing']) {
							if (isset($img->url) &&
								!empty($img->url)) {
								return removeslashes($img->url);
							}
						}
						elseif (!file_exists($this->options['imgPath'].'/'.$file)) {
							if (isset($img->url)) {
								return removeslashes($img->url); // revert to url...
							}
						}
						else {
							return $file;
						}
					}
					unset($img);
				}
			}
			return null;
		}					
	}

	protected function postLog($qq, $in, $type, $data) {
		if ( $this->options['show_extra_timings'] )
			$qq->add((object)array('session_id'=>$in->sessionID,
								   'type'=>$type,
								   'data'=>$data)); //'read in '.($cr + ($page*$pagePer)).', page:'.$page.', memory usage:'.memory_get_usage()));
	}

	protected function swapMainCity(&$r) {
		if ( empty($r->location) )
			return;

		if ( empty($r->cities) ) {
			$this->log("swapMainCity - cities is empty");
			return;
		}

		if ( !isset($r->cities[$r->location[0]]) ) {
			$this->log("swapMainCity - cities does not have {$r->location[0]}");
			return;
		}

		$temp = [];
		$temp[$r->location[0]] = $r->cities[$r->location[0]];
		$this->log("swapMainCity moved city ".$r->location[0]." to top");
		foreach(array_keys($r->cities) as $id) {
			if ($id != $r->location[0])
				$temp[$id] = $r->cities[$id];
		}
		unset($r->cities);
		$r->cities = $temp;
	}

	protected function postProcessData(&$r, &$out, $time, $in, &$Cities, &$qq, $doingRecursiveWithPartialResults = false, $query = null, $cities_from_listings = null) {
		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'before-sort' ]; $time = microtime(true);

		// if there were any city tags flagged, then take out listings that are part of any cities that were tossed out.
		$this->postLog($qq, $in, 'postProcess', 'entered post processing, count flagged cities:'.count($out->cities->flagged_tags));

		// $ids = implode(',', array_values( array_map(function($ele) { return $ele->id.":".count($ele->listings); }, $r->city_sorted)) );
		// $this->log("postProcessData entered ids:$ids");

		if (count($out->cities->flagged_tags)) {
			//$temp = $r->listings;
			// $r->listings = [];
			$rejected = [];
			$temp = [];
			foreach($r->listings as $listing)
				if ( array_key_exists($listing->city_id, $out->cities->percents) )
					$temp[$listing->id] = $listing;
				else
					$rejected[] = $listing;
			$record = ['listing total'=> count($r->listings),
					   'accepted' => count($temp),
					   'rejected' => count($rejected),
					   'flagged'=> count($out->cities->flagged_tags)];
			if ($this->options['show_extra_timings'] !== 0)
				$qq->add((object)array('session_id'=>$in->sessionID,
										   'type'=>'flagging',
										   'data'=>json_encode($record)));
			$r->listings = $temp;
			unset($temp, $rejected);
		}

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'post-flag-city-trim' ]; $time = microtime(true);
		$this->postLog($qq, $in, 'post-flag-city-trim', 'presort - listing-percentage, memory usage:'.memory_get_usage() );

		// average percentage for listing
		$out->listings->percent_averages = [];
		if (!empty( $r->listings )) {
			$i = 0;
			foreach ($r->listings as &$row){
				if ($i >= $doingRecursiveWithPartialResults) {
					$city_isset = isset($out->cities->percents[$row->city_id]);
					$city_percent = is_array($out->cities->percents) ? $out->cities->percents[$row->city_id] : $out->cities->percents->{$row->city_id};
					// $city_percent = isset($out->cities->percents[$row->city_id]) ? $out->cities->percents[$row->city_id] : 0;
					if ($row->city_id > 0 && $city_percent ){
						$row->percent_average = round(($row->percent + $city_percent) / 2, 2);
						$out->listings->percent_averages[$row->id] = $row->percent_average;
					}
					else {
						$out->listings->percent_averages[$row->id] = $row->percent_average = round($row->percent/ 2, 2);
						// $row->percent_average = $out->listings->percent_averages[$row->id];
					}
				}
				else
					$out->listings->percent_averages[$row->id] = $row->percent_average;
				$i++;
				unset($row);
			}
		}

		$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'post-avg-listing-percentage' ]; $time = microtime(true);
		$this->postLog($qq, $in, 'post-avg-listing-percentage', 'memory usage:'.memory_get_usage() );

		$admitted = 0;
		// sort by average percentage
		$temp = $r->listings;
		$r->listings = [];
		// $r->total_listings_found += count($temp);
		if (!isset($r->city_sorted))
			$r->city_sorted = [];

		$this->postLog($qq, $in, 'pre-sorting-listings-into-cities', "doingRecursiveWithPartialResults: ".($doingRecursiveWithPartialResults ? 'yes' : 'no').", quizGrabAllIntoCities:".($this->options['quizGrabAllIntoCities'] ? 'yes' : 'no')." memory usage:".memory_get_usage() );
		
		// if ($in->quiz != 2) { // 2 = get everything flag
		if (count($r->cities) > 1 ||
			($cities_from_listings &&
			 count($cities_from_listings) > 1) ) { 
			if (!$doingRecursiveWithPartialResults) {
				if ( !$this->options['quizGrabAllIntoCities'] ) {
					asort( $out->listings->percent_averages );
					$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after asort(listings->percent_averages), memory usage:'.memory_get_usage() );

					$out->listings->percent_averages = array_reverse($out->listings->percent_averages, true);
					
					$out->listings->percent_averages = array_slice( $out->listings->percent_averages, 0, $this->options['max_listings'], true );
					$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after array_reverse(listings->percent_averages), memory usage:'.memory_get_usage() );

					foreach ($out->listings->percent_averages as $id => $percent)
						$r->listings[$id] = $temp[$id];

					$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after reassigning listings to percent_average sort, memory usage:'.memory_get_usage() );
				}
				else
					$r->listings = $temp; // restore it

				uasort( $r->listings, function($a, $b) {
					if ($a->percent_average != $b->percent_average)
						return $b->percent_average - $a->percent_average;

					if ($a->percent != $b->percent)
						return $b->percent - $a->percent;

					return $b->price - $a->price;

				});
				$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after usort(listings), memory usage:'.memory_get_usage() );
			}
			else {
				if (count($temp)) uasort( $temp, function($a, $b) {
					if ($a->percent_average != $b->percent_average)
						return $b->percent_average - $a->percent_average;

					if ($a->percent != $b->percent)
						return $b->percent - $a->percent;

					return $b->price - $a->price;

				});
				$r->listings = $temp;
			}

			if ($this->options['quizGrabAllIntoCities']) {
				// $out->cities->percents is sorted by percentage now, as is $r->cities
				$this->postLog($qq, $in, 'sorting-listings-into-cities', 'before creating city_sorted from cities, memory usage:'.memory_get_usage() );
				// $r->total_listings_found = 0;
				$newCityIds = [];
				
				if (!isset($r->city_sorted)) $r->city_sorted = [];
				foreach(array_keys($r->cities) as $id) {
					if (!isset($r->city_sorted[$id])) {
						// $this->log("added $id to city_sorted");
						$r->city_sorted[$id] = (object)['id'=>$id,
														'listings'=>[],
														'excess'=>0];
					}
					// else
					// 	$this->log("city: $id, has ".count($r->city_sorted[$id]->listings)." listings");
				}
				$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after creating city_sorted from cities, memory usage:'.memory_get_usage() );
				$max = $this->options['max_listings'] * $this->options['max_cities'];
				foreach($r->listings as $listing) {
					if (!isset($r->city_sorted[$listing->city_id])) {
						if ( empty($out->city->tag_query) ) {// then was grabbing everything..
							$r->city_sorted[$listing->city_id] = (object)['id'=>$listing->city_id,
																		  'listings'=>[],
																		  'excess'=>0];
							$this->log("postProcessData - added cityId:$listing->city_id for listing:$listing->id,");
							$newCityIds[] = $listing->city_id;
						}
						else {
							$this->log("postProcessData - unexpected cityId:$listing->city_id for listing:$listing->id, listing is ignored");
							continue;
						}
					}

					$r->total_listings_found++;

					if (!in_array($listing->id, array_keys($r->city_sorted[$listing->city_id]->listings)) ) {				// check in case it was recursed
						if ( count($r->city_sorted[$listing->city_id]->listings) < $this->options['max_listings'] ) {
							// call json_decode() only as needed...
							$listing->images = $listing->images ? json_decode($listing->images) : null;
							$listing->first_image = $listing->first_image ? json_decode($listing->first_image) : null;
							$listing->list_image = $listing->list_image ? json_decode($listing->list_image) : null;
							if ( ($file = $this->getFirstImage($listing)) != null )	{// max out listings per city 
								if (isset($listing->images)) unset($listing->images);
								unset($listing->city, $listing->state);
								$listing->image_path = $file;
								if (isset($listing->first_image)) unset($listing->first_image);
								if (isset($listing->list_image)) unset($listing->list_image);
								$r->city_sorted[$listing->city_id]->listings[$listing->id] = $listing; 
								$admitted++;
							}
							else {
								$this->log("postProcessData - listing:$listing->id has no image to show, skipped, debugLevel:".$this->options['debugLevel']);
								if ($this->options['debugLevel'] > 2)
									$this->log("postProcessData -  no image listing:".print_r($listing, true));
							}
						}
						else
							$r->city_sorted[$listing->city_id]->excess++;
					}
					else
						$this->log("postProcessData - duplicate listing:$listing->id in city:$listing->city_id");

					if (($r->admitted + $admitted) >= $max)
						break;
				}
				$this->postLog($qq, $in, 'sorting-listings-into-cities', 'after assigning listings to city_sorted, memory usage:'.memory_get_usage() );

				$this->log("postProcessData - pre scan city_sorted count: ".count($r->city_sorted));
				$temp = [];
				foreach($r->city_sorted as $id=>$city) 
					if ( !empty($city->listings) &&
						count($temp) < $this->options['max_cities'])
						$temp[$id] = $city;

				unset($r->city_sorted);
				$r->city_sorted = $temp; // no longer associated
				// $this->swapMainCity($r);
				$this->log("postProcessData - post scan city_sorted count: ".count($r->city_sorted).", city ids:".implode(',',array_keys($r->city_sorted)));
				unset($r->listings); // whoo hoo!  no more listing data!!

				if ( !empty($newCityIds) ) {
					$extraCities = $this->getClass('Cities')->get((object)['where'=>['id'=>$newCityIds],
																		   'what'=>['id','image','city','state','lng','lat']]);
					foreach($extraCities as $row) {
						if (!isset($row->image) ||
					      	 empty($row->image) ||
					      	 (isset($row->image) &&
					      	 !file_exists($this->options['imgPathCity'].'/'.$row->image)) ) 
						  	$row->image = "_blank.jpg";
						$row->percent = 0;
						if (!isset($r->cities[$row->id])) $r->cities[$row->id] = $row;
						if ( ($i = array_search($row->id, $newCityIds)) !== false) {
							if ($i == 0)
								array_shift($newCityIds);
							elseif ( ($i+1) == count($newCityIds))
								array_pop($newCityIds);
							else
								$newCityIds =  array_merge( array_slice($newCityIds, 0, $i), array_slice($newCityIds, $i+1) );
						}
					}

					if (count($newCityIds)) {
						$this->log("postProcessData - unaccounted cities: ".print_r($newCityIds, true));
						foreach($newCityIds as $id) {
							if (isset($r->city_sorted[$id])) {
								$this->log("postProcessData - removing city_id:$id from city_sorted, reducing admitted by ".count($r->city_sorted[$id]));
								$admitted -= count($r->city_sorted[$id]);
								unset($r->city_sorted[$id]);
							}
						}
					}
				}
			}
			$r->quizGrabAllIntoCities = $this->options['quizGrabAllIntoCities'];

			$r->time[] = [ 'time' => microtime(true) - $time, 'action' => 'post-sort-trim-listings' ]; $time = microtime(true);
			if ($this->options['show_extra_timings'] !== 0)
				$qq->add((object)array('session_id'=>$in->sessionID,
									   'type'=>'post-sort-trim-listings',
									   'data'=>'memory usage:'.memory_get_usage()));
		}
		else {		
			// account for a city having muliple entries in Cities table for one reason or another.
			// the lng/lat will have taken care of that in the geo location, but now, we need to sort
			// them properly, so go find all the cities with the same names and associate those
			// city_id values in the listings found.		
			if (count($r->location) == 1 &&
				isset($r->distance)) {
				$cityLoc = [];
				$city = $Cities->get((object)['where'=>['id'=>$r->location[0]]]);
				$cities = $Cities->get((object)['where'=>['city'=>$city[0]->city,
														  'state'=>$city[0]->state]]);
				foreach($cities as $city)
					$cityLoc[] = $city->id;
				$r->location = $cityLoc;
			}

			$matchLoc = [];
			foreach($temp as $i=>$item)
				if (in_array($item->city_id, $r->location)) {
					$matchLoc[$i] = $item;
					unset($temp[$i]);
				}

			// sort listings with matching city id's
			if (count($matchLoc)) usort( $matchLoc, function($a, $b) {
				if ($a->city != $b->city)
					return strcmp($a->city, $b->city);

				if ($a->state != $b->state)
					return strcmp($a->state, $b->state);

				return $b->price - $a->price;

			});

			if (count($temp)) usort( $temp, function($a, $b) {
				if ($a->city != $b->city)
					return strcmp($a->city, $b->city);

				if ($a->state != $b->state)
					return strcmp($a->state, $b->state);

				return $b->price - $a->price;

			});
			$r->listings = array_merge($matchLoc, $temp);
			$temp = $r->listings;
			$r->admitted = 0;
			$r->listings = [];
			foreach($temp as $listing) {
				$listing->images = $listing->images ? json_decode($listing->images) : null;
				$listing->first_image = $listing->first_image ? json_decode($listing->first_image) : null;
				$listing->list_image = $listing->list_image ? json_decode($listing->list_image) : null;
				if ( ($file = $this->getFirstImage($listing)) != null )	{// max out listings per city 
					if (isset($listing->images)) unset($listing->images);
					unset($listing->city, $listing->state);					
					$listing->image_path = $file;
					if (isset($listing->first_image)) unset($listing->first_image);
					if (isset($listing->list_image)) unset($listing->list_image);
					$r->listings[] = $listing; 
					$admitted++;
					$r->total_listings_found++;
				}
				else {
					$this->log("postProcessData - listing:$listing->id has no image to show, skipped, debugLevel:".$this->options['debugLevel']);
					if ($this->options['debugLevel'] > 2)
						$this->log("postProcessData -  no image listing:".print_r($listing, true));
				}
			}
			$admitted = count($r->listings);
			$this->log("postProcessData - post scan listings count: ".count($r->listings));
		}
		
		unset($temp);
		$this->log("total_listings_found: $r->total_listings_found, count of listings accepted this run:".(!$this->options['quizGrabAllIntoCities'] ? count($r->listings) : $admitted).", cities:".count($r->cities));

		// $cullPercentage = $r->total_listings_found <= $this->options['max_listings'] ? 0 : 100 - (($this->options['max_listings']/$r->total_listings_found)*100);
		$cullPercentage = 100;
		if ($r->total_listings_found)
			$cullPercentage = !$this->options['quizGrabAllIntoCities'] ? 100 - ((($r->admitted + $admitted)/$r->total_listings_found)*100)
																		: 0;
																	   //: 100 - (($admitted/$r->total_listings_found)*100);
		$record = [ 'time' => microtime(true) - $time, 
					'action' => 'end',
					'resultCount' => $admitted ]; 
		// $time = microtime(true);
		$r->time[] = $record;
		$r->showWarning = !empty($in->tags) && $cullPercentage > $this->options['warn_culling_percentage'];
		$r->cullPercentage = number_format($cullPercentage, 1);
		$r->shownPercentage = number_format(100-$cullPercentage, 0);
		$r->admitted += !$this->options['quizGrabAllIntoCities'] ? count($r->listings) : $admitted;

		if ($this->options['show_extra_timings'] !== 0)
			$qq->add((object)array('session_id'=>$in->sessionID,
										   'type'=>'end-stats',
										   'data'=>json_encode($record)));

		$qq->add((object)array('session_id'=>$in->sessionID,
								'type'=>'results',
								'data'=> !$this->options['quizGrabAllIntoCities'] ? JSON_encode(array_keys((array)$r->listings)) : JSON_encode(array_keys((array)$r->city_sorted))));

		// QUESTION: if $in->quiz != 2 do we want to do this here?  We have all the listing id's in city_sorted... unset($r->listings);
	}

	private function buildQuestions(&$Questions, &$Slides, &$SlideTags, &$Tags) {
		$cQ = count($Questions);
		for ($qI = 0; $qI < $cQ; $qI++) {
			$Questions[$qI]->slides = [];
			$sQ = count($Slides);
			$gotQuestionMatch = false;
			for($sI = 0; $sI < $sQ; $sI++) if ($Slides[$sI]->question_id != $Questions[$qI]->id && !$gotQuestionMatch) continue; else
				if ($Slides[$sI]->question_id == $Questions[$qI]->id) {
					$gotQuestionMatch = true;
					$Slides[$sI]->tags = [];
					$stQ = count($SlideTags);
					$tQ = count($Tags);
					$gotSlideTag = false;
					for($stI = 0; $stI < $stQ; $stI++) if ($SlideTags[$stI]->slide_id != $Slides[$sI]->id && !$gotSlideTag) continue; else
						if ($SlideTags[$stI]->slide_id == $Slides[$sI]->id) {
							$gotSlideTag = true;
							for($tI = 0; $tI < $tQ; $tI++) if ($Tags[$tI]->id == $SlideTags[$stI]->tag_id) {
								$Slides[$sI]->tags[] = $Tags[$tI];
								break;
							}
						}
						else if ($gotSlideTag)
							break;
					$Questions[$qI]->slides[] = $Slides[$sI];
				}
				else if ($gotQuestionMatch)
					break;
		}
	}

	private function calculateWeights(&$results, &$data) {
		foreach ($data->actions as $i=>$action){
			if (!empty($action->selected)) foreach ($action->selected as $selected_id) {
				// $selected_id = intval($selected_id);
				$selected_id = (int)$selected_id;
				$data->actions = (array)$data->actions;
				$i = (int)$i;
				// if (isset($data->actions[(intval($i)+1)])) $next_action = $data->actions[(intval($i)+1)];
				if (isset($data->actions[($i+1)])) $next_action = $data->actions[($i+1)];
				$prev_action = $data->actions[$i-1];

				$slides = array();
				if (empty($prev_action->get)) {
					if (empty($prev_action->next)) throw new \Exception('Could not find question '.$selected_id.' in previous action.');
					if (empty($prev_action->next->slides)) throw new \Exception('Could not find slides for question '.$prev_action->get->id.'.');
					$slides = $prev_action->next->slides;
				} else {
					if (empty($prev_action->get->slides)) throw new \Exception('Could not find slides for question '.$prev_action->get->id.'.');
					$slides = $prev_action->get->slides;
				}

				if (empty($slides)) throw new \Exception('Could not find slides for question '.$prev_action->get->id.'.');
				if ( empty($next_action) || (isset($next_action) && substr($next_action->action,0,4) != 'back') ){
					foreach ($slides as $slide) if ( $slide->id == $selected_id && !empty($slide->tags) ) foreach ($slide->tags as $slide_tag) {
						if (empty($results->tags[$slide_tag->id])) // setup tag if not found
							$results->tags[$slide_tag->id] = (object) array(
								'tag' => $slide_tag->tag,
								'id' => $slide_tag->id,
								'count' => 0,
								'flag'	=> empty($prev_action->get) ? (isset($prev_action->next->flagged) ? intval($prev_action->next->flagged) : 0 ) :
																	  (isset($prev_action->get->flagged) ? intval($prev_action->get->flagged) : 0 ),
								// 'description' => $slide_tag->description,
							);
						$weight = 1;
						// if (isset($prev_action->get->weight)) $weight = intval($prev_action->get->weight);
						// elseif (isset($prev_action->next->weight)) $weight = intval($prev_action->next->weight);
						if (isset($prev_action->get->weight)) $weight = (int)$prev_action->get->weight;
						elseif (isset($prev_action->next->weight)) $weight = (int)$prev_action->next->weight;

						$results->tags[$slide_tag->id]->count += $weight;
						if ($results->max < $results->tags[$slide_tag->id]->count) $results->max = $results->tags[$slide_tag->id]->count;
						unset($weight);
					}
					unset($slide, $slide_tag);
				}
				unset($next_action, $prev_action, $slides);
			}
		}

		$results->total = 0;
		usort($results->tags,function($a,$b){ return $b->count - $a->count; });
		foreach ($results->tags as &$r_tag){
			$r_tag->percent = round(100 * $r_tag->count / $results->max, 2);
			unset($r_tag->count);
		} unset($r_tag);
		$this->log("calculateWeights exiting");
	}

	private function convertToNumericValues(&$r) {
		// parse the tags
		foreach ($r->tags as $i => $tag){
			if (is_array($tag))
				$tag = (object) $tag;
			// $tag->id = intval($tag->id);
			$tag->id = (int)$tag->id;
			$tag->flag = is_true($tag->flag) ? 1 : 0;
			// $tag->percent = floatval($tag->percent);
			$tag->percent = (float)$tag->percent;
			$r->tags[$i] = $tag;
			unset($i, $tag);
		}
	}

	private function addHomeOptions(&$out, $type, $tag, $value, &$first, &$init, $isMax) {
		if ($init) {
			if ( !empty($out->$type->tag_query) && $out->$type->tag_query != '' )
				$out->$type->tag_query .= ' AND (';
			else
				$out->$type->tag_query .= ' (';
		}
		$init = false;

		if (!$first)
			$out->$type->tag_query .= ' AND ';
		$first = false;



		switch($tag) {
			case 'baths':
			case 'beds':
				if ($isMax) 
					$out->$type->tag_query .= '`'.$tag.'`'.' >= '.floor($value);
				else {
					if (strpos($value, '+') !== false)
						$value = str_replace('+','.5', $value);
					// $halfBath = floor( (fmod(floatval($value), 1)*10)+0.1 ) == 3; // make into integer, but add 0.1 to account for possible float rounding error
					// $orMore = floor( (fmod(floatval($value), 1)*10)+0.1 ) == 5; // ditto
					$halfBath = floor( (fmod((float)$value, 1)*10)+0.1 ) == 3; // make into integer, but add 0.1 to account for possible float rounding error
					$orMore = floor( (fmod((float)$value, 1)*10)+0.1 ) == 5; // ditto
					$this->log("addHomeOptions = $tag, halfBath:$halfBath, orMore:$orMore, value:$value");
					if (!$halfBath && !$orMore)
						$out->$type->tag_query .= '`'.$tag.'`'.' = '.$value;
					elseif ($halfBath)
						$out->$type->tag_query .= "(`$tag` > ".floor($value)." AND `$tag` < ".ceil($value).")";
					else // $orMore
						$out->$type->tag_query .= '`'.$tag.'`'.' >= '.floor($value);
				}
				break;
			default:
				$out->$type->tag_query .= '`'.$tag.'`'.($isMax ? ' >= ' : ' = ').$value;
				break;
		}
		
	}

	private function setupTagIds(&$tag_ids, &$r, &$out, $Tags) {
		$all_tags = $Tags->get((object)[ 'what' => ['id', 'type'] ]);
		$tag_ids->listings = [];
		$tag_ids->cities = [];
		if (!empty($all_tags))
			foreach ($all_tags as $tag)
				// switch (intval($tag->type)){
				switch ((int)$tag->type){
					// case 0: $tag_ids->listings[] = intval($tag->id); break;
					// case 1: $tag_ids->cities[] = intval($tag->id); break;
					case 0: $tag_ids->listings[] = (int)$tag->id; break;
					case 1: $tag_ids->cities[] = (int)$tag->id; break;
				}
		unset($tag);

		// add up percentages for city and listing tags
		$tagsUsed = ['cities'=>[],
					 'listings'=>[]];
		$gotHeli  = false;
		$gotAirstrip = false;

		foreach ($r->tags as $i => $tag) {
			if ($tag) {
				if (is_array($tag))
					$r->tags[$i] = $tag = (object) $tag;

				// $tag->id = intval($tag->id);
				$tag->id = (int)$tag->id;

				// check tag type and parse
				foreach (['listings', 'cities'] as $type) {
					// $out->$type->tags = (array)$out->$type->tags;
					// $this->log("setupTagIds loop for $type, out->tags:".print_r($out->$type->tags, true));
					if ( !empty($tag_ids->$type) && 
						 in_array( $tag->id, $tag_ids->$type ) && 
						 !array_key_exists($tag->id, $out->$type->tags)) {
						// add to $type_info total
						$out->$type->total += $tag->percent;
						$out->$type->tags[$tag->id] = $tag;

						if ( empty($out->$type->tag_query) || $out->$type->tag_query == '' ) // add parenthesis to group all these OR's together
							$out->$type->tag_query .= '(';
						// add to $type query
						if ( !empty($out->$type->tag_query) && $out->$type->tag_query != '(' )
							$out->$type->tag_query .= ' OR ';							

						$out->$type->tag_query .= '`tag_id` = ' . $tag->id;
						$tagsUsed[$type][] = $tag->id;

						// add to flagged $type tags
						if ($tag->flag &&
							!in_array($tag->id, $out->$type->flagged_tags))
							$out->$type->flagged_tags[] = $tag->id;

						if ($tag->id == 44)
							$gotHeli = true;
						if ($tag->id == 76)
							$gotAirstrip = true;
					}
				} // end foreach (['listings', 'cities'] as $type)
			} // end if ($tag)

			if (!$gotAirstrip &&
				$gotHeli) {
				if ( !empty($out->listings->tag_query) && $out->listings->tag_query != '' )
					$out->listings->tag_query .= ' OR ';
				$out->listings->tag_query .= '`tag_id` = 76'; // add 'airstrip' to capture Aviation AM agents
				$tagsUsed['listings'][] = 76;
			}
			unset($tag, $i);
		} // end foreach ($r->tags as $i => $tag)

		$this->log("setupTagIds - listings flagged:".(count($out->listings->flagged_tags) ? print_r($out->listings->flagged_tags, true) : "none"));
		$this->log("setupTagIds - cities flagged:".(count($out->cities->flagged_tags) ? print_r($out->cities->flagged_tags, true) : "none"));

		// close off the parenthesis if needed
		foreach (['listings', 'cities'] as $type) {
			if ( !empty($out->$type->tag_query) && $out->$type->tag_query != '' )
				$out->$type->tag_query .= ')';
		}

		if (!empty($r->homeOptions)) {
			$first = true;
			$init = true;
			foreach($r->homeOptions as $tag=>$value) {
				switch($tag) {
					case 'beds':
						if (!empty($value)) {// then it's non-zero, zero mean ALL, so don't put anything
							// $this->addHomeOptions($out, 'listings', $tag, $value, $first, $init, intval($value) == 7);
							$this->addHomeOptions($out, 'listings', $tag, $value, $first, $init, (int)$value == 7);
							$this->log("calling addHomeOptions - beds:{$r->homeOptions['beds']}");
						}
						break;
					case 'baths':
						if (!empty($value)) {// then it's non-zero, zero mean ALL, so don't put anything
							// $this->addHomeOptions($out, 'listings', $tag, $value, $first, $init, intval($value) == 8);
							$this->addHomeOptions($out, 'listings', $tag, $value, $first, $init, (int)$value == 8);
							$this->log("calling addHomeOptions - baths:{$r->homeOptions['baths']}");
						}
						break;
				}
			}
			if (!$first)
				$out->listings->tag_query .= ')';
		}

		// update metrics for what tags are used as input
		foreach (['listings', 'cities'] as $type) {
			if (count($tagsUsed[$type])) {
				$sql = '(';
				// use external counter, had one failure where $i wasn't starting at zero
				$i = 0;
				foreach ($tagsUsed[$type] as &$v){
					$sql .= ($i>0 ? ' OR ': '' ) . "`id` = $v";
					unset($v);
					$i++;
				}
				$sql.= ')';
				$Tags->rawQuery("UPDATE ".$Tags->getTableName($Tags->table)." SET `clicks` = `clicks` + 1 WHERE $sql");
			}
		}
		unset($tagsUsed);

		// get each tag's percent of total
		if (!empty($r->tags)) foreach ($r->tags as $tag){
			if ($tag)
				foreach (['listings', 'cities'] as $type)
					if ( !empty($tag_ids->$type) && in_array( $tag->id, $tag_ids->$type ) && !array_key_exists($tag->id, $out->$type->tag_percents))
						$out->$type->tag_percents[$tag->id] = $out->$type->total ? round((100 * $tag->percent / $out->$type->total), 2) : 0;

			unset($tag);
		}
	
	}
}