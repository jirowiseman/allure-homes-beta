<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class QuizActivity extends Base {
	public function __construct($logIt = 0){
		parent::__construct($logIt);
		// $this->logFile = new Log(__DIR__.'/_logs/QA.log');
	}

	// public function log($message, $hide_timestamp = false) {
	// 	if ($this->logFile !== null)
	// 		$this->logFile->add($message, $hide_timestamp);
	// }

	public function add($input = null) {
		require_once(__DIR__.'/Sessions.class.php'); $Sessions = new Sessions();
		$myCopy = $input;
		$session_id = $myCopy->session_id;
		$status = isset($myCopy->status) ? $myCopy->status : null;
		$ses = null;
		if (!empty($session_id)) {
			$ses = $Sessions->get((object)['where'=>['id'=>$session_id]]);
			if (empty($ses)) 
				throw new \Exception("Failed to find session with id: $session_id");
		}

		$row = parent::add($input);
		$this->log("add - session_id:$session_id, status:$status, row:$row");

		if (!empty($ses)) {
			$ses = array_pop($ses);
			$q = new \stdClass();
			$q->where = ['id'=>$session_id];
			$q->fields = ['status'=>$status,
						  'quiz_id'=>$row];
			$Sessions->set([$q]);
		}

		return $row;
	}

	// $in = [{[...]}]
	public function set($in = null) {
		if (empty($in)) throw new \Exception('No input provided.');
		if (!is_array($in) && !is_object($in)) throw new \Exception('Input must be array or object, '.gettype($in).' provided.');
		$in = (array)$in;
		$myCopy = $in[0]; // get the nested object
		if (!isset($myCopy->fields['status'])) throw new \Exception("Input missing status value");
		if (!isset($myCopy->where['id'])) throw new \Exception("Input missing id value");
		if (isset($myCopy->where['session_id']) &&
			!empty($myCopy->where['session_id'])) { //throw new \Exception("Input missing session_id value");
			$session_id = $myCopy->where['session_id'];
			$quiz_id = $myCopy->where['id'];
			$status = $myCopy->fields['status']; 

			$q = new \stdClass();
			$q->where = ['id'=>$session_id];
			// $q->notand = ['type'=>SESSION_RETIRED];
			
			if (isset($q->fields))
				$q->fields['status'] = $status;
			else
				$q->fields = ['status'=>$status];

			$this->log("set - session_id:$session_id, status:$status, quiz_id:".(isset($myCopy->fields['data']) && isset($myCopy->fields['data']->quiz_id) ? $myCopy->fields['data']->quiz_id : "N/A"));

			require_once(__DIR__.'/Sessions.class.php'); $Sessions = new Sessions();
			$ses = $Sessions->get((object)['where'=>['id'=>$session_id]]);
			$ses = array_pop($ses);
			if ( $ses->quiz_id != $quiz_id )
				$q->fields['quiz_id'] = $myCopy->where['id'];
			$Sessions->set([$q]);

			// if (isset($myCopy->fields['data'])) {
			// 	$size = count($myCopy->fields['data']);
			// 	$data = (object)$myCopy->fields['data'][$size-1]; // get last one
			// 	$data->quiz_id = $size-1; //assign the quiz_id
			// 	$q->fields['data'] = $data;
			// }
			// $q->where = ['session_id'=>$ses->session_id];

			// remove session_id, as the caller may not be the owner
			if (isset($in[0]->where['session_id'])) unset($in[0]->where['session_id']);
		}
		elseif (empty($in[0]->where['session_id'])) unset($in[0]->where['session_id']);

		$citiesSorted = isset($in[0]->fields['data']) && isset($in[0]->fields['data'][0]->results[0]->city_sorted) ? count($in[0]->fields['data'][0]->results[0]->city_sorted) : "none";
		$admitted = isset($in[0]->fields['data']) && isset($in[0]->fields['data'][0]->results[0]->admitted) ? $in[0]->fields['data'][0]->results[0]->admitted : "none";

		$saved = $this->log_to_file;
		$this->log_to_file = 0;
		$retval = parent::set($in);
		$this->log_to_file = $saved;
		$this->log("set (".(empty($retval) ? 'failed' : 'succeeded').")- quizId:".$in[0]->where['id'].", status:".$in[0]->fields['status'].", city_sorted:".$citiesSorted.", admitted:$admitted");

		return $retval;
	}

	public function get($in = null){
		if ($in != null) {
			$session_id = isset($in->where['session_id']) ? $in->where['session_id'] : (isset($in->where['id']) ? $in->where['id'] : "N/A");
		}

		$x = parent::get($in);
		$this->log("get - session_id:".(!empty($session_id) ? $session_id : "N/A").", id:".(isset($in->where['id']) ? $in->where['id'] : "N/A").", got something:".(!empty($x) ? 'true' : 'false'));

		return $x;
	}

	public function getDetails($quizId) {
		$activity = $this->get((object)['where'=>['id'=>$quizId]]);
		if (empty($activity)) {
			$this->log("getDetail failed to find quiz:$quizId");
			return new Out('fail', "getDetail failed to find quiz:$quizId"); 
		}
		$activity = array_pop($activity);
		$last_quiz = array_pop($activity->data); // pop the last quiz
		if (!isset($last_quiz->results)) { 
			$this->log("getLastResult - quizId: $quizId, results array not set!"); 
			return new Out('fail', "No results not set quizId:$quizId"); 
		}
		$results = array_pop($last_quiz->results); // pop the last result set
		if (empty($results)) { 
			$this->log("getLastResult - quizId: $quizId, no results!"); 
			return new Out('fail', "Empty results for quizId:$quizId"); 
		}

		if (isset($results->location) && !empty($results->location)) {
			require_once(__DIR__.'/Cities.class.php'); $Cities = new Cities(1);
			$cities = $Cities->get((object)['where'=>['id'=>$results->location]]);
			if (!empty($cities)) {
				$results->location = $cities[0]->city.', '.$cities[0]->state;
			}
			$results->quiz = 0;
		}
		else
			$results->quiz = empty($results->state) ? 1 : 5;

		$detail = ['tags'=>$results->tags,
				   'price'=>$results->price,
				   'homeOptions'=>$results->homeOptions,
				   'quizType'=>$results->quiz, // 0 = city, 1 = nationwide, 5 = state
				   'state'=>(isset($results->state) && !empty($results->state) ? $results->state : ''),
				   'location'=>(isset($results->location) && !empty($results->location) ? $results->location : ''),
				   'distance'=>(isset($results->distance) ? $results->distance : 0)];
		$this->log("getDetail returning: ".print_r($detail, true));
		return new Out('OK', $detail);

	}
}