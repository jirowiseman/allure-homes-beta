<?php
namespace AH;

require_once(__DIR__.'/../../../plugins/vendor/autoload.php');
require_once(__DIR__.'/_Controller.class.php');

use Aws\Ec2\Ec2Client;
use Aws\Ec2\Exception\Ec2Exception;

class AWS extends Controller {
	const UPDATE = 1;
	const RESULT = 2;
	const ERROR  = 3;
	const FATAL  = 4;

	public function __construct() {
		$this->size = 1;
		$this->debugLevel = 1;
		$this->haveOutputFile = false;
		$ec2Clients = array();
	}

	protected function updateProgress($msg, $type)
	{
		if ($type < $this->debugLevel)
			return;
		$progress = $this->getClass("ListhubProgress");
		$q = new \stdClass();
		$q->type = $type;
		$q->data = $msg;
		$this->getClass("ListhubProgress")->add($q);
		unset($q);
	}

	protected function record($buffer, $level = 1)
	{
		if ($level >= $this->debugLevel)
		{
			$this->updateProgress($buffer, self::UPDATE);
			if ($this->haveOutputFile)
				echo $buffer;
		}
	}

	// start/end time are values from microtime();
	protected function diffTime($starttime, $endtime)
	{
		//time before
		list($usec, $sec) = explode(' ',$starttime);
		$querytime_before = ((float)$usec + (float)$sec);
		/* your code */

		//time after
		list($usec, $sec) = explode(' ',$endtime);
		$querytime_after = ((float)$usec + (float)$sec);
		$diff = (float)($querytime_after - $querytime_before);
		return number_format($diff, 4);
	}

	protected function createEc2Instance() {
		$ec2Client = Ec2Client::factory(array(
		    'key'    => 'AKIAJLXWHA6MJCVSV5PA',
		    'secret' => 'SrJrf7U8s1bzIsaWyA+HxbVKUFzkoWZ8veS4shfu',
		    'region' => 'us-west-2' // (e.g., us-east-1)
		));
		return $ec2Client;
	}

	public function terminateEc2Instances($dryRun = false) {
		$o = $this->getClass('Options');
		$q = new \stdClass();
		$q->where = array('opt'=>'AWS_EC2_Keys');
		$x = $o->get($q);
		if (empty($x)) {
			return new Out('fail', 'Could find AWS_EC2_Keys');
		}

		$keyPairs = JSON_decode($x[0]->value);
		$startTime = microtime();
		$ec2Client = $this->createEc2Instance();

		$keyPairName = 'parser-keypair';
		$securityGroupName = 'my-security-group';
		$count = 0;

		foreach($keyPairs as $key=>$value) {
			if (gettype($value) == 'object') {
				$this->record("Begin terminating instance for $key\n", 3);
				if (!$dryRun) {
					$ec2Client->terminateInstances(array(
					    'InstanceIds' => $value->InstanceIds));
					$ec2Client->waitUntilInstanceTerminated(array(
					    'InstanceIds' => $value->InstanceIds));
					$count++;
				}
				$keyName = !empty($value->KeyName) ? $value->KeyName : $keyPairName."-$key";
				$ec2Client->deleteKeyPair(array('KeyName' => $keyName));
				$this->record("Terminated instance with $key\n", 3);
			}
		}
		// if (!empty($keyPairs->SecurityGroups) ){
		// 	$this->record("About to delete security group - name: $securityGroupName, id: ".$keyPairs->SecurityGroups[0]."\n", 1);
		// 	$ec2Client->deleteSecurityGroup(array(
		// 		    'GroupName'     => $securityGroupName,
		// 		    'GroupId'		=> $keyPairs->SecurityGroups[0]));
		// 	$this->record("Delete security group - name: $securityGroupName, id: ".$keyPairs->SecurityGroups[0]."\n", 1);
		// }
		$endTime = microtime();
		$this->record("Time to terminate $count instances - ".$this->diffTime($startTime, $endTime)."\n", 3);

		$o->delete($q->where); // get rid of the old

		return new Out('OK', "Terminated $size instances");
	}

	public function stopEc2Instances($dryRun = false) {
		$o = $this->getClass('Options');
		$q = new \stdClass();
		$q->where = array('opt'=>'AWS_EC2_Keys');
		$x = $o->get($q);
		if (empty($x)) {
			return new Out('fail', 'Could find AWS_EC2_Keys');
		}

		$keyPairs = JSON_decode($x[0]->value);
		$startTime = microtime();
		$ec2Client = $this->createEc2Instance();

		$keyPairName = 'parser-keypair';
		$securityGroupName = 'my-security-group';

		$needUpdate = false;
		$count = 0;
		foreach($keyPairs as $key=>$value) {
			if (gettype($value) == 'object') {
				$this->record("Begin stopping instance for $key\n", 3);
				if (!$dryRun) {
					$ec2Client->stopInstances(array(
					    'InstanceIds' => $value->InstanceIds));
					$ec2Client->waitUntilInstanceStopped(array(
					    'InstanceIds' => $value->InstanceIds));
				}
				// $keyName = !empty($value->KeyName) ? $value->KeyName : $keyPairName."-$key";
				// $ec2Client->deleteKeyPair(array('KeyName' => $keyName));
				$keyPairs->$key->State = 'stopped';
				$needUpdate = true;
				$this->record("Stopped instance with $key\n", 3);
				$count++;
			}
		}
		// if (!empty($keyPairs->SecurityGroups) ){
		// 	$this->record("About to delete security group - name: $securityGroupName, id: ".$keyPairs->SecurityGroups[0]."\n", 1);
		// 	$ec2Client->deleteSecurityGroup(array(
		// 		    'GroupName'     => $securityGroupName,
		// 		    'GroupId'		=> $keyPairs->SecurityGroups[0]));
		// 	$this->record("Delete security group - name: $securityGroupName, id: ".$keyPairs->SecurityGroups[0]."\n", 1);
		// }
		$endTime = microtime();
		$this->record("Time to stop $count instances - ".$this->diffTime($startTime, $endTime)."\n", 3);

		//$o->delete($q->where); // get rid of the old
		$keyPairsStr = JSON_encode($keyPairs);
		if ($needUpdate){
			$q->fields = array('value'=>$keyPairsStr);
			$a = array();
			$a[] = $q;
			$x = $o->set($a);
			if ($x)
				$this->record("Updated AWS_EC2_Keys with EC2 instance state change(s)\n", 3);
			else
				$this->record("Failed to update AWS_EC2_Keys with EC2 instance state change(s)\n", 3);
		}

		$data = array('opt'=>'AWS_EC2_Keys',
					  'value'=>$keyPairsStr);
		return new Out('OK', $data);
	}

	// always called from createInstances()
	protected function startInstances($size, &$keyPairs, &$needUpdate, $dryRun) {
		$startTime = microtime();
		$ec2Client = $this->createEc2Instance();

		$keyPairName = 'parser-keypair';
		$securityGroupName = 'my-security-group';

		$i = 0;
		$needUpdate = false;
		foreach($keyPairs as $key=>$value) {
			if (gettype($value) == 'object') {
				if ($value->State == 'stopped') {
					$this->record("Begin starting instance for $key\n", 3);
					if (!$dryRun) {
						$ec2Client->startInstances(array(
						    'InstanceIds' => $value->InstanceIds));
						$ec2Client->waitUntilInstanceRunning(array(
						    'InstanceIds' => $value->InstanceIds));
						// Describe the now-running instance to get the public URL
						$result = $ec2Client->describeInstances(array(
						    'InstanceIds' => $value->InstanceIds));
						$keyPairs->$key->DNS = current($result->getPath('Reservations/*/Instances/*/PublicDnsName'));
						$keyPairs->$key->PublicIP = current($result->getPath('Reservations/*/Instances/*/PublicIpAddress'));
						$keyPairs->$key->VpcId = current($result->getPath('Reservations/*/Instances/*/VpcId'));
					}
					
					$keyPairs->$key->State = 'running';
					$needUpdate = true;
					$this->record("Started instance with $key\n", 3);
				}
				$i++;
				if ($i == $size) // if we have more stopped than we want to start
					break;
			}
		}
		
		$endTime = microtime();
		$this->record("Time to start $i instances - ".$this->diffTime($startTime, $endTime)."\n", 3);

		if ($needUpdate){
			$keyPairsStr = JSON_encode($keyPairs);
			$q = new \stdClass();
			$q->where = array('opt'=>'AWS_EC2_Keys');
			$q->fields = array('value'=>$keyPairsStr);
			$a = array();
			$a[] = $q;
			$o = $this->getClass('Options');
			$x = $o->set($a);
			if ($x) {
				$this->record("Updated AWS_EC2_Keys with EC2 instance state change(s)\n", 3);
				$needUpdate = false;
			}
			else
				$this->record("Failed to update AWS_EC2_Keys with EC2 instance state change(s)\n", 3);
		}

		return $i;
	}

	public function createEc2Instances($size, $dryRun = false) {
		$this->size = $size;
		// Create the key pair
		$keyPairName = 'parser-keypair';
		$securityGroupName = 'my-security-group';
		$o = $this->getClass('Options');
		$q = new \stdClass();
		$q->where = array('opt'=>'AMI_Image');
		$x = $o->get($q);
		if (!empty($x))
			$image = $x[0]->value;
		else {
			return new Out('fail', "Unable to find AMI_Image in Options database");
		}
			

		$q->where = array('opt'=>'AWS_EC2_Keys');
		$x = $o->get($q);
		$i = 0;
		if (!empty($x)) {
			$keyPairs = JSON_decode($x[0]->value);
			$needUpdate = false;
			$startCount = $this->startInstances($size, $keyPairs, $needUpdate, $dryRun);
			if ($startCount == $size) {
				$keyPairsStr = JSON_encode($keyPairs);

				$data = array('opt'=>'AWS_EC2_Keys',
						  'value'=>$keyPairsStr);
				if ($needUpdate) {
					$q->fields = array('value'=>$keyPairsStr);
					$a = array();
					$a[] = $q;
					$x = $o->set($a);
				}
				return new Out('OK', $data);
			}
			$i = $startCount;
		}
		else
			$keyPairs = new \stdClass(); // to keep this homogeneous with existing data from Options

		$securityGroupIds = null;
		$failRunInstance = false;
		for(; $i < $size; !$failRunInstance ? $i++ : $i) {
			$startTime = microtime();
			$ec2Clients[$i] = $this->createEc2Instance();
			$this->record("After createEc2Instance for $i\n", 3);
			$keyPairs->$i = new \stdClass();

			try {
				$result = $ec2Clients[$i]->createKeyPair(array('KeyName' => $keyPairName."-$i"));
				$keyPairs->$i->KeyName = $keyPairName."-$i";
				$keyPairs->$i->Keys = $result->getPath('KeyMaterial');
			}
			catch(Ec2Exception $ex) {
				$msg = "Exception for createKeyPair - code:".$ex-> getExceptionCode();
				$msg .= ", type:".$ex-> getExceptionType();
				$msg .= ", request:".$ex-> getRequest();
				$msg .= ", requestId:".$ex-> getRequestId();
				$msg .= ", response:".$ex-> getResponse();
				$msg .= ", statusCode:".$ex-> getStatusCode();
				$msg .= ", message:".$ex-> getMessage();
				$msg .= "\n";
				$this->record($msg, 1);
			}
			
			// // Create the security group
			// try {
			// 	if ($i == 0) {// only need one
			// 		$result = $ec2Clients[$i]->createSecurityGroup(array(
			// 		    'GroupName'   => $securityGroupName,
			// 		    'Description' => 'Basic web server security'
			// 		));
			// 		if (empty($keyPairs->SecurityGroups))
			// 			$keyPairs->SecurityGroups = array();
			// 		if (!empty($result->getPath('GroupId')))
			// 			$keyPairs->SecurityGroups[$i] = $securityGroupIds = $result->getPath('GroupId');
			// 		else
			// 			$keyPairs->SecurityGroups[$i] = '';
			// 		$this->record("Security GroupId: ".$result->getPath('GroupId')."\n", 1);
			// 	}
			// }
			// catch(Ec2Exception $ex) {
			// 	$msg = "Exception for createSecurityGroup - code:".$ex-> getExceptionCode();
			// 	$msg .= ", type:".$ex-> getExceptionType();
			// 	$msg .= ", request:".$ex-> getRequest();
			// 	$msg .= ", requestId:".$ex-> getRequestId();
			// 	$msg .= ", response:".$ex-> getResponse();
			// 	$msg .= ", statusCode:".$ex-> getStatusCode();
			// 	$msg .= ", message:".$ex-> getMessage();
			// 	$msg .= "\n";
			// 	$this->record($msg, 1);
			// }
			// // Set ingress rules for the security group
			// try {
			// 	if ($i == 0) {// only need one
			// 		$result = $ec2Clients[$i]->authorizeSecurityGroupIngress(array(
			// 		    'GroupName'     => $securityGroupName,
			// 		    'IpPermissions' => array(
			// 		    	// HTTP
			// 		        array(
			// 		            'IpProtocol' => 'tcp',
			// 		            'FromPort'   => 0,
			// 		            'ToPort'     => 65535,
			// 		            'IpRanges'   => array(
			// 		                array('CidrIp' => '0.0.0.0/0')
			// 		            ),
			// 		        ),
			// 		        // SSH
			// 		        array(
			// 		            'IpProtocol' => 'tcp',
			// 		            'FromPort'   => 22,
			// 		            'ToPort'     => 22,
			// 		            'IpRanges'   => array(
			// 		                array('CidrIp' => '0.0.0.0/0')
			// 		            ),
			// 		        ),
			// 		        // MYSQL
			// 		        array(
			// 		        	'IpProtocol' => 'tcp',
			// 		            'FromPort'   => 3306,
			// 		            'ToPort'     => 3306,
			// 		            'IpRanges'   => array(
			// 		                array('CidrIp' => '54.144.0.0/12')
			// 		            ),
			// 		        ),
			// 		        // RDP
			// 		        array(
			// 		        	'IpProtocol' => 'tcp',
			// 		            'FromPort'   => 3389,
			// 		            'ToPort'     => 3389,
			// 		            'IpRanges'   => array(
			// 		                array('CidrIp' => '54.144.0.0/12')
			// 		            ),
			// 		        )
			// 		    )
			// 		));
			// 	}
			// }
			// catch(Ec2Exception $ex) {
			// 	$msg = "Exception for authorizeSecurityGroupIngress - code:".$ex-> getExceptionCode();
			// 	$msg .= ", type:".$ex-> getExceptionType();
			// 	$msg .= ", request:".$ex-> getRequest();
			// 	$msg .= ", requestId:".$ex-> getRequestId();
			// 	$msg .= ", response:".$ex-> getResponse();
			// 	$msg .= ", statusCode:".$ex-> getStatusCode();
			// 	$msg .= ", message:".$ex-> getMessage();
			// 	$msg .= "\n";
			// 	$this->record($msg, 1);
			// }
			if (!$dryRun) {
				// Launch an instance with the key pair and security group
				$startInstanceTime = microtime();
				$failRunInstance = false;
				try {
					$result = $ec2Clients[$i]->runInstances(array(
					    'ImageId'        => $image,
					    'MinCount'       => 1,
					    'MaxCount'       => 1,
					    'InstanceType'   => 'm3.large',
					    'KeyName'        => $keyPairName."-$i",
					    'SecurityGroupsIds' => array('sg-26c68543'),
					    'SubnetId'		 => 'subnet-e3a744ba'
					));
					$instanceIds = $result->getPath('Instances/*/InstanceId');
					$keyPairs->$i->InstanceIds = $instanceIds;
				}
				catch(Ec2Exception $ex) {
					$msg = "Exception for runInstances - code:".$ex-> getExceptionCode();
					$msg .= ", type:".$ex-> getExceptionType();
					$msg .= ", request:".$ex-> getRequest();
					$msg .= ", requestId:".$ex-> getRequestId();
					$msg .= ", response:".$ex-> getResponse();
					$msg .= ", statusCode:".$ex-> getStatusCode();
					$msg .= ", message:".$ex-> getMessage();
					$msg .= "\n";
					$this->record($msg, 1);
					$failRunInstance = true;
				}
				// if (empty($keyPairs['InstanceIds']))
				// 	$keyPairs['InstanceIds'] = array();

				// Wait until the instance is launched
				if (!$failRunInstance) {
					$ec2Clients[$i]->waitUntilInstanceRunning(array(
					    'InstanceIds' => $instanceIds,
					));

					// Describe the now-running instance to get the public URL
					$result = $ec2Clients[$i]->describeInstances(array(
					    'InstanceIds' => $instanceIds
					));
					$keyPairs->$i->DNS = current($result->getPath('Reservations/*/Instances/*/PublicDnsName'));
					$keyPairs->$i->PublicIP = current($result->getPath('Reservations/*/Instances/*/PublicIpAddress'));
					$keyPairs->$i->VpcId = current($result->getPath('Reservations/*/Instances/*/VpcId'));
					$keyPairs->$i->State = 'running';
					$endTime = microtime();
					$this->record("Time to start Instance # $i: ".$this->diffTime($startInstanceTime, $endTime)." secs, DNS: ".$keyPairs->$i->DNS.", PublicIP:".$keyPairs->$i->PublicIP.", VpcId:".$keyPairs->$i->VpcId."\n", 3);
				}
				else {
					unset($keyPairs->$i);
					$this->record("Failed to run instance for $i\n", 3);
				}

			}

			$endTime = microtime();
			$this->record("Time to process Instance # $i: ".$this->diffTime($startTime, $endTime)." secs\n", 3);
		}

		if (!empty($keyPairs)) {
			$q = new \stdClass();
			$q->where = array('opt'=>'AWS_EC2_Keys');
			$x = $o->get($q);
			if (!empty($x)) // update with new
				$o->delete($q->where); // get rid of the old

			$keyPairsStr = JSON_encode($keyPairs);

			$data = array('opt'=>'AWS_EC2_Keys',
						  'value'=>$keyPairsStr);
			$x = $o->add((object)$data);
			return new Out('OK', $data);
		}
		else
			return new Out('fail', "Nothing made.");
	}
}