<?php
namespace AH;
require_once(__DIR__.'/Logger.class.php');
require_once(__DIR__.'/../../../../wp-includes/wp-db.php');

/**
 * Almost all the DB interaction classes extend this class
 * @package AllureHome
 * @since 1.0
 */
class Base extends Logger {
	// comes from Logger now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	/**
	 * [__construct description]
	 */
	public function __construct($logIt = 0){
		parent::__construct( $logIt, substr(get_class($this), 3) );
		// date_default_timezone_set('America/Los_Angeles');
		$this->rows_per_page = 30;
		$this->xdebug_enable = false;
		// $this->log_to_file = $logIt;
		$this->lock_sql = false;
		$this->lockfile = $this->lock_sql ? fopen(__DIR__.'/_logs/lockBaseSql.lock', 'c') : null; 
		// $this->log = null;
		try {
			$this->table = strtolower(preg_replace( '/(?<!^)([A-Z])/', '-\\1', substr(get_class($this), 3) ));
			if (empty($this->table)) throw new \Exception('No table provided.');

			require_once(__DIR__.'/Tables.class.php');
			$this->Tables = new Tables();
			if (!$this->Tables->exists($this->table)) throw new \Exception('Table does not exist.');

			global $wpdb;
			$this->wpdb = &$wpdb;

			// if ($this->log_to_file) {
			// 	$this->log = new Log(__DIR__."/_logs/".$this->table.".log");
			// 	$this->log->add( get_class($this).'::_construct()' );
			// 	$this->log->options->buffer_length = -1;
			// }
		} catch (\Exception $e) { parseException($e); }
	}
	public function __destruct(){
		// if ($this->log_to_file) $this->log->add( get_class($this).'::_destruct()' . "\r\n" );
		if ($this->lockfile !== null)
			fclose($this->lockfile);
	}

	public function lock() {
		if ($this->lockfile !== null)
			flock($this->lockfile, LOCK_EX);
	}
	public function unlock() {
		if ($this->lockfile !== null)
			flock($this->lockfile, LOCK_UN);
	}

	/**
	 * Add a new row to the database
	 * @param [type] $input, field=>value to be added to database
	 * @return [bool/int] false on fail, otherwise inserted row id
	 */
	public function add($input = null){
		// $this->log("entered add() with ".print_r($input, true));
		try {
			$start_time = microtime(true);
			$fields = array();
			// check for input values to add to row
			if (!empty($input)) {
				foreach ($input as $field=>$value) if (isset($value) && !empty($field)) { // cycle through the $input fields
					$is_unique = $this->isFieldUnique($field);

					// if $field is not unique OR ($field is unique AND $field!=$value in any row in the database)
					if ( !$is_unique || ($is_unique && !$this->exists(array($field=>$value))) ){
						// sanitize field and value for DB insertion
						$field_sanitized = $this->prepare('%s', $field, '`');
						$value_sanitized = $this->prepare($this->getFieldFormat($field), $value, null, $this->isFieldJSON($field));

						$fields[$field_sanitized] = $value_sanitized;
					}
				} unset($field, $value, $is_unique, $value_sanitized, $field_sanitized);
			}

			// search the rows for a default value if the $field was not set
			foreach ($this->Tables->tables[$this->table]['cols'] as $field=>$field_info) if (!array_key_exists($field, $fields)) {
				// set added $field to now
				if ($field == 'added')
					$fields[$this->prepare('%s','added','`')] = $this->prepare('%s',date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600) ));
				// check if $field has a default value
				elseif (isset($field_info['default']) && $field_info['default'] != 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
					$fields[$this->prepare('%s',$field,'`')] = $this->prepare($this->getFieldFormat($field), $field_info['default']);
			} unset($field, $field_info);

			// build SQL query
			$sql = 'INSERT INTO '.$this->getTableName($this->table).' ('.implode(',',array_keys($fields)).') VALUES ('.implode(',',array_values($fields)).')';

			// run the query and throw exception on fail, $sql_results will be an interger: # of rows affected (usually 1)
			global $wpdb;
			$this->lock();
			$this->log("add - $sql");
			if (!$sql_results = $wpdb->query($sql)) {
				$this->unlock();
				throw new \Exception('Unable to add row and data to database using: '.$sql);
			}
			$this->unlock();
			// if (!$sql_results = $this->wpdb->query($sql)) throw new \Exception('Unable to add row and data to database using: '.$sql);

			if ($this->xdebug_enable){ xdebug_break(); null; }
			return $this->wpdb->insert_id; // return ID of newly inserted row
		} catch (\Exception $e) { parseException($e); throw $e; }
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
	}

	/**
	 * Add a new row to the database
	 * @param [type] $input, arrays of field=>value to be added to database
	 * @return [bool/int] false on fail, otherwise inserted row id
	 */
	public function addMultiple($elements = null){
		try {
			$start_time = microtime(true);
			$blockSize = 20;
			// check for input values to add to row
			$elements = (array)$elements;
			if (!empty($elements)) {
				$fields = [];
				$counter = 0;
				foreach($elements as &$input) {
					$counter++;
					$values = [];
					if ( !isset($fields) ) $fields = [];
					foreach ($input as $field=>$value) if (isset($value) && !empty($field)) { // cycle through the $input fields
						$is_unique = $this->isFieldUnique($field);

						// if $field is not unique OR ($field is unique AND $field!=$value in any row in the database)
						if ( !$is_unique || ($is_unique && !$this->exists(array($field=>$value))) ){
							// sanitize field and value for DB insertion
							$field_sanitized = $this->prepare('%s', $field, '`');
							$value_sanitized = $this->prepare($this->getFieldFormat($field), $value, null, $this->isFieldJSON($field));

							$values[$field_sanitized] = $value_sanitized;
						}
					} unset($field, $value, $is_unique, $value_sanitized, $field_sanitized);
					$fields[] = $values;
					unset($values);

					if ( $counter != count($elements) &&
						 ($counter % $blockSize) != 0)
						continue; // keep bulding the fields[] and values[]....
			

					// search the rows for a default value if the $field was not set
					foreach($fields as &$values) {
						foreach ($this->Tables->tables[$this->table]['cols'] as $field=>$field_info) {
							if (!array_key_exists($field, $values)) {
								// set added $field to now
								if ($field == 'added')
									$values[$this->prepare('%s','added','`')] = $this->prepare('%s',date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600) ));
								// check if $field has a default value
								elseif (isset($field_info['default']) && $field_info['default'] != 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
									$values[$this->prepare('%s',$field,'`')] = $this->prepare($this->getFieldFormat($field), $field_info['default']);
							} 
							unset($field, $field_info);
						}
						unset($values);
					}

					// build SQL query
					$firstSet = $fields[0];
					$sql = 'INSERT INTO '.$this->getTableName($this->table).' ('.implode(',',array_keys($firstSet)).') VALUES '; 
					$first = true;
					foreach($fields as $values) {
						$sql .= ($first ? '' : ',').'('.implode(',',array_values($values)).')';
						$first = false;
						unset($values);
					}
					unset($fields);

					// run the query and throw exception on fail, $sql_results will be an interger: # of rows affected (usually 1)
					global $wpdb;
					$this->lock();
					$this->log("addMultiple - $sql");
					if (!$sql_results = $wpdb->query($sql)) {
						$this->unlock();
						throw new \Exception('Unable to add row and data to database using: '.$sql);
					}
					$this->unlock();
					// if (!$sql_results = $this->wpdb->query($sql)) throw new \Exception('Unable to add row and data to database using: '.$sql);
				} // for()
			} // !empty()
			if ($this->xdebug_enable){ xdebug_break(); null; }
			return $this->wpdb->insert_id; // return ID of newly inserted row
		} catch (\Exception $e) { parseException($e); }
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
	}
	/**
	 * returns number of rows
	 * @param  [array|object] $input [ where => [ $field1 => $value1, $field2 => $value2, ..., $fieldn => $valuen ] ]
	 * @return [int]        [number of rows]
	 */
	public function count($input = null){
		$start_time = microtime(true);
		try {
			$sql = 'SELECT COUNT(*) FROM '.$this->getTableName($this->table);
			if (!empty($input)) $sql.= ' WHERE ' . ( ($x = $this->formatWhereClause($input)) ? $x : 1 );
			$this->log("count - sql:$sql");
			return intval($this->wpdb->get_var($sql));
		} catch (\Exception $e) { parseException($e); }
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
	}
	/**
	 * Delete row
	 * @param  [array] $input, field=>value for SQL where clause
	 * @param  [string] $field (optional, default 'id')
	 * @return [bool] true on success, false on fail
	 * @throws no input, invalid input
	 */
	public function delete($input = null){
		$start_time = microtime(true);
		try {
			if (empty($input)) throw new \Exception('No input provided.');
			if (!is_array($input) && !is_object($input)) throw new \Exception('Input must be array or object, '.gettype($in).' provided.');
			$fields = [];
			foreach ($input as $field=>$value) $fields[] = $this->getFieldFormat($field);

			// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );

			return $this->wpdb->delete($this->getTableName($this->table), (array) $input, (array) $fields) ? true : false;
		} catch (\Exception $e){ parseException($e); }
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
	}
	/**
	 * Empties all rows from the table
	 * @param  boolean $default [must === true to run]
	 * @return int        			[number of rows deleted]
	 */
	public function emptyTable($default = false){
		if ($default !== true) throw new Exception('Tried to delete table '.$this->table);
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(), true );
		return $this->wpdb->query("TRUNCATE TABLE ".$this->getTableName($this->table));
	}
	/**
	 * gets the fist entry in the table
	 * @param  [mixed] $what [order by this field, descending order]
	 * @return [array]       [last row data]
	 */
	public function getFirst($what = 'id', $in = null) {
		$start_time = microtime(true);
		$where = '';
		if ($in != null)
			$where = 'WHERE '.$this->formatWhereClause($in);

		$last = $this->wpdb->get_results("SELECT * FROM ".$this->getTableName($this->table)." $where ORDER BY `".$what."` ASC LIMIT 1");
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
		$this->log("getFirst for $this->table is ".(!empty($last) ? $last[0]->id : " a failure"));
		return $last;
	}
	/**
	 * gets the last entry in the table
	 * @param  [mixed] $what [order by this field, descending order]
	 * @return [array]       [last row data]
	 */
	public function getLast($what = 'id') {
		$start_time = microtime(true);
		$last = $this->wpdb->get_results("SELECT * FROM ".$this->getTableName($this->table)." ORDER BY `".$what."` DESC LIMIT 1");
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
		$this->log("getLast for $this->table is ".(!empty($last) ? $last[0]->id : " a failure"));
		return $last;
	}
	/**
	 * Check if row exists in database
	 * @param  [array/object] $input, field=>value for SQL where clause
	 * @return [boolean] row exists
	 * @throws no input, invalid input, invalid table, wpdb fail
	 */
	public function exists($in = null){
		try {
			if (empty($in)) throw new \Exception('No input provided.');
			if (!is_array($in) && !is_object($in)) throw new \Exception('Input must be array or object, '.gettype($in).' provided.');
			global $wpdb;
			$out = intval( $wpdb->get_var("SELECT EXISTS(SELECT 1 FROM " . $this->getTableName($this->table) . " WHERE " . $this->formatWhereClause((object)['where'=>$in]) . ")") ) ? true : false;
			// $out = intval( $this->wpdb->get_var("SELECT EXISTS(SELECT 1 FROM " . $this->getTableName($this->table) . " WHERE " . $this->formatWhereClause((object)['where'=>$in]) . ")") ) ? true : false;
			return $out;
		} catch (\Exception $e) { parseException($e); }
	}
	/**
	 * Checks if the field has the SQL flag "UNIQUE"
	 * @param  [string]  $field
	 * @return [boolean]
	 * @throws If no input, invalid input, wpdb fail
	 */
	public function isFieldUnique($field){
		try {
			if (empty($field)) throw new \Exception('No field provided.');
			if (empty($this->table)) throw new \Exception('Table is not set.');
			return $this->Tables->isFieldUnique($this->table, $field);
		} catch (\Exception $e) { parseException($e); }
	}
	/**
	 * Checks if the field stores json data
	 * @param  [string]  $field, database column
	 * @return [boolean]
	 * @throws If no input, invalid input, wpdb fail
	 */
	public function isFieldJSON($field){
		try {
			if (empty($field)) throw new \Exception('No field provided.');
			return isset($this->Tables->tables[$this->table]['cols'][$field]['json']) ? true : false;
		} catch (\Exception $e) { parseException($e); }
	}
	/**
	 * [get description]
	 * @param  [type] $in
	 * @return [type]
	 */
	public function get($in = null){
		$start_time = microtime(true);
		// $this->log("get entered with ".print_r($in, true));
		try {
			// $this->log("get @start");
			$sql = new \stdClass();
			$use_join = false;
			if (!empty($in->with)) $use_join = true;

			// $this->log("get @0");

			// what to query
			$sql->what = '';
			if ( !empty($in->what) && (is_array($in->what) || is_object($in->what)) ) foreach ($in->what as $i=>&$what){
				if (!in_array($what, array_keys($this->Tables->tables[$this->table]['cols']))) throw new \Exception('invalid field: '.$what);
				if ($i > 0) $sql->what.=',';
				if ($use_join) $what='a.'.$what;
				$sql->what.= $this->prepare('%s',$what,'`');
			} elseif (!empty($in->what) && $in->what == '*') $sql->what = ($use_join ? 'a.' : '').'*';
			unset($i, $what);
 			if (strlen($sql->what) < 1) $sql->what = ($use_join ? 'a.' : '').'*';

 			// $this->log("get @1");

			// setup for join
			if ($use_join) {
				$join_index = 'b';
				foreach ($in->with as &$with_info){
					$with_info['parent_index'] = 'a';
					$with_info['index'] = $join_index; $join_index++;
					$sql->what.= ',' . $this->prepare('%s',$with_info['as'],'`');
					if (!empty($with_info['with'])) foreach ($with_info['with'] as &$with_with_info){
						$with_with_info['parent_index'] = &$with_info['index'];
						$with_with_info['index'] = $join_index; $join_index++;
						if (!empty($with_with_info['with'])) foreach ($with_with_info['with'] as &$with_with_with_info){
							$with_with_with_info['parent_index'] = &$with_with_info['index'];
							$with_with_with_info['index'] = $join_index; $join_index++;
						}
					}
				} unset ($with_table_name, $with_info, $with_with_table_name, $with_with_info);
			}

			// $this->log("get @2");

			// from & join
			$sql->from = ' FROM '.$this->getTableName($this->table);
			if ($use_join) {
				$sql->from.= ' AS a';
				array_walk($in->with, array(&$this, 'walker'));
				foreach ($in->with as &$with_info) $sql->from.= $with_info['sql'];
			}

			// $this->log("get @3");

			// where clause
			$sql->where = $this->formatWhereClause($in, $use_join);
			if (strlen($sql->where) > 0) $sql->where = ' WHERE '.$sql->where;

			// set max length if merging into some json
			global $wpdb;
			// if ( $use_join &&
			// 	( $this->wpdb->use_mysqli && !mysqli_query($this->wpdb->dbh, 'SET SESSION group_concat_max_len = 16777216')) ||
			// 		!$this->wpdb->use_mysqli && !mysql_query('SET SESSION group_concat_max_len = 16777216', $this->wpdb->dbh) )
			if ( $use_join &&
				( $wpdb->use_mysqli && !mysqli_query($wpdb->dbh, 'SET SESSION group_concat_max_len = 16777216')) ||
					!$wpdb->use_mysqli && !mysql_query('SET SESSION group_concat_max_len = 16777216', $wpdb->dbh) )
			 	throw new \Exception('Unable to increase max string buffer.');

			 // $this->log("get @4");

			// order by
			$sql->orderby = '';
			if (!empty($in->orderby))
				$sql->orderby .= ' ORDER BY '.$this->prepare('%s', $in->orderby, '`').(!empty($in->order) ? (strtolower($in->order) == 'desc' ? ' DESC' : ' ASC') : '');

			// limit + pagination
			$sql->limit = '';
			if ( isset($in->limit) || isset($in->page) || isset($in->rows) ){
				if (isset($in->rows)){
					$sql->limit.= ' LIMIT '.intval($in->rows[0]).', '.intval($in->rows[1]);
				} else {
					if ( !isset($in->limit) ) $in->limit = $this->rows_per_page;
					if ( isset($in->page) ) $sql->limit.= ' LIMIT '.(intval($in->limit) * intval($in->page)).', '.intval($in->limit);
					else $sql->limit.= ' LIMIT '.intval($in->limit);
				}
			}

			// $this->log("get @5");

			// merge the parts of the sql query and run
			$sql_print = 'SELECT '  . $sql->what . $sql->from . (strlen($sql->where) > 0 ? $sql->where : '').$sql->orderby.$sql->limit;
			$this->log($sql_print);
			$sql_results = $wpdb->get_results($sql_print);
			// $sql_results = $this->wpdb->get_results($sql_print);

			// $this->log("get @6");

			// parse the rows
			$table_info = $this->Tables->get('table', $this->table);
			foreach ($sql_results as &$row){
				foreach ($row as $field=>&$value){
					$value = is_numeric($value) ? ($value*1 == (int)($value*1) ? intval($value) : floatval($value)) : $value;
					if (strlen(trim($value)) < 1) $value = null;
					elseif ($this->isFieldJSON($field)) $value = json_decode($value);
					elseif ($use_join) foreach ($in->with as $with_table_name=>&$with_info) if ($with_info['as'] == $field) {
						$value = json_decode('['.$value.']');
						break;
					}
					if (is_array($value) || is_object($value)){
						foreach ($value as &$v)
							if (!is_array($v) && !is_object($v)) $v = is_numeric($v) ? ($v*1 == (int)($v*1) ? intval($v) : floatval($v)) : $v;
							else foreach ($v as &$vv) if (!is_array($vv) && !is_object($vv)) $vv = is_numeric($vv) ? ($vv*1 == (int)($vv*1) ? intval($vv) : floatval($vv)) : $vv;
						unset($v, $vv);
					}
				}
				unset($field, $value);
			} unset($row, $table_info, $with_table_name, $with_info);

			// $this->log("get @7");

			if ($this->xdebug_enable){ xdebug_break(); null; }
			unset($in);

			// $this->log("Base::get - ".print_r($sql_results, true));
			if (empty($sql_results) || count($sql_results) < 1) {
				$sql_results = false;
			}

			// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );

			// $this->log("get @8");
			return $sql_results;
		} catch (\Exception $e) { parseException($e); }
	}
	public function getGroup($in = null){
		$start_time = microtime(true);
		if (empty($in)) throw new \Exception('No input.');
		if (is_array($in)) $in = (object) $in;
		if (empty($in->field)) throw new \Exception('Field required, none provided.');
		if (empty($in->where)) throw new \Exception('Where clause required, none provided.');

		if (
			($this->wpdb->use_mysqli && !mysqli_query($this->wpdb->dbh, 'SET SESSION group_concat_max_len = 16777216') ) ||
			(!$this->wpdb->use_mysqli && !mysql_query('SET SESSION group_concat_max_len = 16777216', $this->wpdb->dbh) )
		) throw new Exception('Unable to increase max string buffer.');

		$count = $this->count( $in );
		$per_page = 500000;
		$total_pages = $count > 0 && $count <= $per_page ? 1 : ceil($count / $per_page);
		$page = 0;
		$results = [];
		$where = $this->formatWhereClause( $in );
		if (strlen($where) < 2) $where = '1';
		$field = &$in->field;
		$field_sani = $this->prepare('%s', $field, '`');
		$field_format = $this->getFieldFormat($field);
		if (!empty($in->group_by)) $group_by = &$in->group_by;

		while ($page < $total_pages){
			$sql = 'SELECT '. ( isset($in->group_by) ? $this->prepare('%s', $in->group_by, '`').' ,' : null ) . $field_sani.' FROM '.$this->getTableName($this->table).' WHERE '.$where. ' LIMIT '.( $per_page * $page ).', '.$per_page;
			$sql_result = $this->wpdb->get_results( $sql );
			if (!empty($sql_result)) foreach ($sql_result as &$row) if (!empty( $row->$field )) {
				if ($field_format == '%f') $row->$field = floatval($row->$field);
				elseif ($field_format == '%d') $row->$field = intval($row->$field);
				if (!isset($in->group_by)) $results[] = $row->$field;
				else {
					if (!isset($results[$row->$group_by])) $results[$row->$group_by] = [];
					$results[$row->$group_by][] = $row->$field;
				}
				unset($row);
			}
			unset($sql, $sql_result);
			$page++;
		}

		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
		return empty($results) ? false : $results;
	}
	/**
	 * [getFieldFormat description]
	 * @param  [type] $field
	 * @return [type]
	 */
	public function getFieldFormat($field = null){
		try {
			if (empty($field)) throw new \Exception('No field provided.');
			return $this->Tables->get('field-format', $this->table, $field);
		} catch (\Exception $e) { parseException($e); }
	}
	/**
	 * [getTableName description]
	 * @param  [type] $table_name [description]
	 * @return [type]             [description]
	 */
	public function getTableName($table_name = null){
		if ($table_name === null)
			$table_name = $this->table;
		try {
			return $this->Tables->getTableName($table_name);
		} catch (\Exception $e){ parseException($e); }
	}
	public function rawQuery($query = null){
		$start_time = microtime(true);
		if (empty($query)) throw new \Exception('No query sent');
		$query = trim($query);
		// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
		$this->log( "rawQuery: $query");

		if (strpos( strtolower($query), 'group_concat' ) !== FALSE){
			if (($this->wpdb->use_mysqli && !mysqli_query($this->wpdb->dbh, 'SET SESSION group_concat_max_len = 16777216') ) ||
				(!$this->wpdb->use_mysqli && !mysql_query('SET SESSION group_concat_max_len = 16777216', $this->wpdb->dbh) )
			) throw new Exception('Unable to increase max string buffer.');
		}

		return strtolower(substr($query, 0, 6)) == 'select' ? $this->wpdb->get_results($query) : $this->wpdb->query($query);
	}
	/**
	 * [set description]
	 * @param [type] $in (array of query objects)
	 */
	public function set($in = null){
		$start_time = microtime(true);
		try {
			if (empty($in)) throw new \Exception('No input provided.');
			if (!is_array($in) && !is_object($in)) throw new \Exception('Input must be array or object, '.gettype($in).' provided.');
			$out = array();
			foreach ($in as &$q){
				if (empty($q->where)) throw new \Exception('no where clause sent');
				if (empty($q->fields)) throw new \Exception('no fields sent');
				$sql = 'UPDATE '.$this->getTableName($this->table).' SET ';

				// sanitize fields
				$fields = array(); $first = true;
				foreach ($q->fields as $field=>$value){
					if ($first) $first = false; else $sql.= ',';
					$sql.= $this->prepare('%s',$field,'`') . '=' . $this->prepare($this->getFieldFormat($field), $value, null, $this->isFieldJSON($field));
				} unset($field, $value, $first);

				// where clause
				$where = array(); $first = true; $sql.= ' WHERE ';
				foreach ($q->where as $field=>&$value){ // set up the where clause
					if ($first) $first = false; else $sql.= ' AND ';
					if (is_array($value) || is_object($value)){
						$sql.= '(';
						$sub_first = true;
						foreach ($value as &$sub_value){
							if ($sub_first) $sub_first = false; else $sql.= ' OR ';
							$sql.= $this->prepare('%s',$field,'`') . '=' . $this->prepare($this->getFieldFormat($field), $sub_value);
						}
						$sql.= ')';
					}
					else
						 $sql.= $this->prepare('%s',$field,'`') . '=' . $this->prepare($this->getFieldFormat($field), $value);
				} unset($field, $value, $first);

				$sql = str_replace("='NULL'", "=NULL", $sql); // fix for wpdb null

				$this->lock();
				$this->log("set - $sql");
				$sql_results = $this->wpdb->query($sql); // execute the query
				$this->unlock();
				if ($this->xdebug_enable){ $sql_result = &$x; xdebug_break(); }
				if ($sql_results === false) throw new \Exception('unable to update table, SQL:'.$sql);
				$out[] = array($sql, $sql_results);
			}
			// if ($this->log_to_file) $this->log->add( $this->log->getBackTraceString(microtime(true) - $start_time), true );
			return $out;
		} catch (\Exception $e){ parseException($e); }
	}
	/**
	 * [set description]
	 * @param [type] $in (array of query objects)
	For example:
		using image processing scenario:
		have a list of $listings, and if $listings->needUpdate is true, then make $elements array to update as a batch
		So, given $fieldTypes and $elements, we need to do a double nested loop to gather and make CASE statements for
		each $field we are updating..

		$elements = [];
		$fieldTypes = ['images']; // can be an array of non-homegenous updates, some row can have one or more fields
								  // in this example, just one field
		$keyType = 'id'; // what column to trigger on
		foreach($listings as $listing) {
			if ($listing->needUpdate) {
				$elements[] = (object)['key'=>$listing->id,
									   'fields=>['images'=>$listing->images]];
			}
		}
		Base->setMultiple($keyType, $fieldTypes, $elements);

	 */
	public function setMultiple($keyType, $fieldTypes, $in = null){
		$start_time = microtime(true);
		try {
			if (empty($keyType)) throw new \Exception('No keyType provided.');
			if (empty($fieldTypes)) throw new \Exception('No fieldTypes provided.');
			if (empty($in)) throw new \Exception('No input provided.');
			if (!is_array($fieldTypes) && !is_object($fieldTypes)) throw new \Exception('Input must be array or object for fieldTypes, '.gettype($fieldTypes).' provided.');
			if (!is_array($in) && !is_object($in)) throw new \Exception('Input must be array or object for elements, '.gettype($in).' provided.');
			foreach ($in as &$q){
				if (empty($q->key)) throw new \Exception('no key clause sent');
				if (empty($q->fields)) throw new \Exception('no fields sent');
			}
			$out = array();
			$sql = 'UPDATE '.$this->getTableName($this->table).' SET '; // base query
			$first = true;
			// loop thru each field type to update
			foreach ($fieldTypes as $field) {
				$haveElementWithField = false;
				foreach ($in as &$q){
					// see if this element has the field to update
					if ( array_key_exists($field, $q->fields)) {
						$haveElementWithField = true;
						break;
					}
				}
				if ($haveElementWithField) {
					$sql .= ($first ? '' : ', ').$this->prepare('%s',$field,'`')."= CASE ".$this->prepare('%s',$keyType,'`').' ';
					// go over each element to see if it has this field to update
					foreach ($in as &$q){
						// see if this element has the field to update
						if ( array_key_exists($field, $q->fields)) {
							$sql .= "WHEN ".$this->prepare($this->getFieldFormat($field), $q->key)." THEN ";
							$sql .= $this->prepare($this->getFieldFormat($field), $q->fields[$field], null, $this->isFieldJSON($field)).' ';
						}
					}
					// all important ELSE clause to make sure existing ones don't get overwritten if they don't have this field to update
					$sql .= 'ELSE '.$this->prepare('%s',$field,'`').' END ';
					$first = false;
				}
			}	
			$sql .= "WHERE ".$this->prepare('%s',$keyType,'`')." IN (".implode( ',', array_map(function($ele){ return $ele->key; }, $in)).");";

			
			$sql = str_replace("='NULL'", "=NULL", $sql); // fix for wpdb null

			$this->log("setMultiple - sql:$sql");
			if (!$first) { // then there was at least one valid CASE statement
				$this->lock();
				$sql_results = $this->wpdb->query($sql); // execute the query
				$this->unlock();
				if ($this->xdebug_enable){ $sql_result = &$x; xdebug_break(); }
				if ($sql_results === false) throw new \Exception('unable to update table, SQL:'.$sql);
				$out[] = array($sql, $sql_results);
			}
			else
				$out = 0;
			return $out;
		} catch (\Exception $e){ parseException($e); }
	}

	public function prepare($field_format = null, $value = null, $replace_quotes_with = null, $encode_json = false){
		if (empty($value) && intval($value) !== 0) throw new \Exception('No value passed to prepare.');
		$found = false; foreach (array('%s','%d','%f','for-replace') as $f) if ($field_format == $f){ $found = true; break; }
		if (!$found) throw new \Exception('Invalid field format: '.$field_format); unset($found);
		$out = null;
		switch ($field_format){
			case 'for-replace':
				$out = $this->prepare('%s',$value,'"').':"\',REPLACE(REPLACE(REPLACE('. $this->prepare('%s',$value,'`') . ', "/", "\\/")' . ',"\\\\","")' . ',\'"\',\'\\\\"\'),\'"\'';
				if (empty($out) && $out !== '0') throw new \Exception('Prepare returned empty field');
				break;
			default:
				if ($encode_json && (is_array($value) || is_object($value)) ) $value = json_encode($value);
				global $wpdb;
				$out = $wpdb->prepare($field_format, $value);
				// $out = $this->wpdb->prepare($field_format, $value);
				if (empty($out) && $out !== '0') throw new \Exception('Prepare returned empty field');
				$out = empty($replace_quotes_with) && $replace_quotes_with !== '' ? $out : str_replace("'", $replace_quotes_with, $out);
				break;
		}
		return empty($out) && $out !== '0' ? false : $out;
	}
	public function formatWhereClause($in, $use_join = false){
		if (!$in || empty($in))
			return '';

		$in = (object)$in;
		$sql = '';
		$first = true;
		$joiner = isset($in->joiner) && strtolower($in->joiner) == 'or' ? ' OR ' : ' AND ';
		foreach (['where','likefront','likeback','likeonlycase','like','like2','like3','like4','not','notlike','between','between2','greaterthan', 'greaterthanequal','lessthan','lessthanequal','greaterthan2', 'greaterthanequal2','lessthan2','lessthanequal2','notand', 'bitand', 'notor', 'bitor','or','or2'] as $clause_type){
			switch ($clause_type){
				case 'not': $operator = ' != '; break;
				case 'likefront':
				case 'likeback':
				case 'likeonlycase':
				case 'like4':
				case 'like3':
				case 'like2':
				case 'like': $operator = ' LIKE '; break;
				case 'notlike': $operator = ' NOT LIKE '; break;
				case 'between':
				case 'between2': $operator = ' BETWEEN '; break;
				case 'greaterthan': $operator = ' > '; break;
				case 'greaterthanequal': $operator = ' >= '; break;
				case 'lessthan': $operator = ' < '; break;
				case 'lessthanequal': $operator = ' <= '; break;
				case 'greaterthan2': $operator = ' > '; break;
				case 'greaterthanequal2': $operator = ' >= '; break;
				case 'lessthan2': $operator = ' < '; break;
				case 'lessthanequal2': $operator = ' <= '; break;
				case 'bitand':
				case 'notand': $operator = ' & '; break;
				case 'bitor':
				case 'notor': $operator = ' | '; break;
				case 'or2':
				case 'or': $operator = ' OR '; break;
				case 'where': default: $operator = ' = '; break;
			}
			$likeAsWildcard = strpos($clause_type, 'like') !== false && $clause_type != 'likeonlycase';
			$likeFront = $likeAsWildcard && strpos($clause_type, 'back') === false;
			$likeBack = $likeAsWildcard && strpos($clause_type, 'front') === false;
			if (isset($operator) && !empty($in->$clause_type)) foreach ($in->$clause_type as $field=>&$value){
				if (!isset($first)) $sql .= $joiner;
				if ($clause_type == 'or' ||
					$clause_type == 'or2') {
					$this->log("formatWhereClause - $clause_type has ".print_r($value, true));
					if (is_array($value) && count($value) >= 2 ) {
							$sql .= '(';
						// use external counter, had one failure where $i wasn't starting at zero
						$i = 0;
						if ( gettype($value[0]) == 'array')
							foreach ($value as $val) {
								foreach ($val as $field=>$v) {
									$field_sani = ($use_join ? 'a.' : '').$this->prepare('%s', ($use_join && false ? 'a.' : '').$field, '');
									$sql .= ($i>0 ? $operator : '' ) . $field_sani . " = " . $this->prepare($this->getFieldFormat($field), $v);
									unset($field_sani, $field, $v);
									$i++;
								}
							}
						else
							foreach($value as $v) {
								$field_sani = ($use_join ? 'a.' : '').$this->prepare('%s', ($use_join && false ? 'a.' : '').$field, '');
								$sql .= ($i>0 ? $operator : '' ) . $field_sani . " = " . $this->prepare($this->getFieldFormat($field), $v);
								unset($field_sani, $v);
								$i++;
							}
						$sql.= ')';
					} 
				}
				else {
					$field_sani = ($use_join ? 'a.' : '').$this->prepare('%s', ($use_join && false ? 'a.' : '').$field, '');
					
					if ($value === null || $value === 'null') $sql .= $field_sani . " IS NULL";
					elseif ($value === 'notnull') $sql .= $field_sani . " IS NOT NULL";
					elseif (is_array($value) && 
							($clause_type == 'between' ||
							 $clause_type == 'between2') ) {
						if ($value[0] > $value[1]){
							$value[2] = $value[0];
							$value[0] = $value[1];
							$value[1] = $value[2];
							unset($value[2]);
						}
						$sql .= '(' . $field_sani . '>=' . $this->prepare( $this->getFieldFormat($field), $value[0] ) . ' AND ' . $field_sani . '<=' . $this->prepare( $this->getFieldFormat($field), $value[1] ) . ')';
					} elseif (is_array($value)) {
						$sql .= '(';
						// use external counter, had one failure where $i wasn't starting at zero
						$i = 0;
						foreach ($value as &$v){
							$sql .= ($i>0 ? (isset($in->arr_joiner) && strtolower($in->arr_joiner) == 'and' ? ' AND ' : ' OR ') : '' ) . $field_sani . $operator . $this->prepare($this->getFieldFormat($field), ($likeFront ? '%' : '') . $v . ($likeBack? '%' : ''));
							unset($v);
							$i++;
						}
						$sql.= ')';
					} 
					elseif ($clause_type == 'notand' ||
							$clause_type == 'notor') {
						$prepped = $this->prepare($this->getFieldFormat($field), ($likeFront ? '%' : '') . $value . ($likeBack ? '%' : ''));
						$sql .= "!(".$field_sani.$operator.$prepped.")";
					}
					elseif ($clause_type == 'bitand' ||
							$clause_type == 'bitor') {
						$prepped = $this->prepare($this->getFieldFormat($field), ($likeFront ? '%' : '') . $value . ($likeBack ? '%' : ''));
						$sql .= "(".$field_sani.$operator.$prepped.")";
					}
					else $sql.= $field_sani . $operator . $this->prepare($this->getFieldFormat($field), ($likeFront ? '%' : '') . $value . ($likeBack ? '%' : ''));
					unset($field, $value, $field_sani);
				}
				if (isset($first)) unset($first);				
			}
		}
		return $sql;
	}
	public function walker(&$with_info, $with_table_name){
		$with_info['cols'] = &$this->Tables->tables[$with_table_name]['cols'];
		$sql = '';
		$key = $value = null;
		foreach ($with_info['on'] as $kk=>&$vv) { $key = $kk; $value = $vv; break; } unset($kk, $vv);
		$sql.= ' LEFT JOIN (SELECT ' . $this->prepare('%s',$value,'`');

		if (!empty($with_info['with'])) foreach ($with_info['with'] as &$with_with_info){ // sub
			$sql.= ',' . $this->prepare('%s',array_keys($with_with_info['on'])[0],'`');
			$sql.= ',' . $this->prepare('%s',$with_with_info['as'],'`');
		}

		// group concat - level 1
		$sql.= ', GROUP_CONCAT(CONCAT(\'{\',';
		$first = true;
		if (empty($with_info['what']))
			$with_info['what'] = array_keys($with_info['cols']);
		foreach($with_info['what'] as &$field){
			$sql.= (isset($first) ? "'" : ",',") . $this->prepare('for-replace', $field);
			if (isset($first)) unset($first);
		} unset($field);

		if (!empty($with_info['with'])){
			foreach ($with_info['with'] as &$with_with_info) // sub
			$sql.= ',\','.str_replace("'",'"',$this->wpdb->prepare('%s', $with_with_info['as'])).':[\','.str_replace("'",'`',$this->wpdb->prepare('%s', $with_with_info['as'])).',\']\'';
			$sql.= ",'}'))";

			$sql.=' AS '. $this->prepare('%s',$with_info['as'],'`') .' FROM '.$this->getTableName($with_table_name).' AS sub_'.$with_info['index'];
			array_walk($with_info['with'], array($this, 'walker_sub'));

	 		foreach ($with_info['with'] as &$with_with_info)
	 			if (isset($with_with_info['sql'])) $sql.= $with_with_info['sql'];
	 		unset($with_with_info);
		}

		$sql.= ' GROUP BY '.$this->prepare('%s', $value, '`').') AS '.$with_info['index'];
		$sql.= ' ON '.$this->prepare('%s',$with_info['parent_index'].'.'.$key,'').'='.$this->prepare('%s',$with_info['index'].'.'.$value,'');

		$with_info['sql'] = $sql;
	}
	public function walker_sub(&$with_with_info, $with_with_table_name){
		// if (isset($with_with_info['sub'])) $with_with_info['sub'].= 'sub_';
		// else $with_with_info['sub'] = 'sub_';
		// if (!empty($with_with_info['with'])) foreach ($with_with_info['with'] as &$with_with_with_info) $with_with_with_info['sub'] = $with_with_info['sub'];

		$with_with_info['cols'] = &$this->Tables->tables[$with_with_table_name]['cols'];

		$sql = '';
		$sub_key = $sub_value = null;
		foreach ($with_with_info['on'] as $sk=>&$sv) { $sub_key = $sk; $sub_value = $sv; break; } unset($sk, $sv);
		$sql.= ' LEFT JOIN (SELECT '. $this->prepare('%s',$sub_value,'`');
		$sql.=', GROUP_CONCAT(CONCAT(\'{\',';

		$first = true;
		if (empty($with_with_info['what']))
			$with_with_info['what'] = array_keys($with_with_info['cols']);
		foreach($with_with_info['what'] as &$field){
			$sql.= (isset($first) ? "'" : ",',") . $this->prepare('for-replace', $field);
			if (isset($first)) unset($first);
		} unset($field);
		$sql.= ",'}'))";

		$sql.=' AS '. $this->prepare('%s',$with_with_info['as'],'`') .' FROM '.$this->getTableName($with_with_table_name);

		if (!empty($with_with_info['with'])) {
			array_walk($with_with_info['with'], array($this, 'walker_sub'));
			foreach ($with_with_info['with'] as &$with_with_with_info) if (!empty($with_with_with_info['sql'])) $sql.= $with_with_with_info['sql'];
		}

		$sql.=' GROUP BY '. $this->prepare('%s',$sub_value,'`') .') AS '. $with_with_info['index'];
		$sql.=' ON '. $this->prepare('%s',$sub_key,'') .'='. $this->prepare('%s',$with_with_info['index'].'.'.$sub_value,'');
		unset($sub_key, $sub_value);
		$with_with_info['sql'] = $sql;
	}
}
