<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class CitiesCounts extends Base {
	public function __construct($logIt = 0){
		parent::__construct($logIt);
		// $this->logFile = new Log(__DIR__.'/_logs/QA.log');
	}

	// class will map between incoming 'id' to 'city_id', and visa versa when outgoing
	protected function mapIn(&$input) {
		$input = (object)$input;
		if (!isset($input->city_id)) {
			if (!isset($input->id))
				throw new \Exception("Must have either or city_id defined");
			$input->city_id = $input->id;
			unset($input->id);
		}
	}

	protected function mapOut(&$input) {
		$input = (object)$input;
		$input->id = $input->city_id;
		unset($input->city_id);
	}
	public function add($input = null) { 
		if ( $input == null )
			throw new \Exception("Input missing for add");

		$this->mapIn($input);

		return parent::add($input);
	}

	public function get($in = null) {
		$x = parent::get($in);
		if (!empty($x))
			foreach($x as &$input)
				$this->mapOut($input);

		return $x;

	}

	public function addMultiple($elements = null) {
		if (!$elements)
			throw new \Exception("Elements not defined");

		set_time_limit(0);
		foreach($elements as &$input) {
			$this->mapIn($input);
			unset($input);
		}

		return parent::addMultiple($elements);
	}
}