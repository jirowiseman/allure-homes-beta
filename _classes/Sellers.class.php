<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/QuizActivity.class.php');
require_once(__DIR__.'/Cities.class.php');
require_once(__DIR__.'/GoogleLocation.class.php');
require_once(__DIR__.'/Listings.class.php');


class Sellers extends Base {
	// public $tag_city = array(10, 42, 123, 163, 18, 164, 6, 66, 49, 145, 72, 143, 25, 40, 167, 169, 172);
	// private $tag_listing = array(8, 9, 14, 20, 23, 168, 59, 65, 69, 86, 147, 44, 76, 144);
	private $tag_extra = array(170); // multilingual

	const UPDATE_EMAIL_DB_ENTERED = 200;
	const UPDATE_EMAIL_DB_START = 201;
	const UPDATE_EMAIL_DB_NO_LISTING = 202;
	const UPDATE_EMAIL_DB_ADDED = 203;
	const UPDATE_EMAIL_DB_UPDATED = 204;
	const UPDATE_EMAIL_DB_END= 205;
	
	const CITY_TAG_WEIGHT_MULTIPLIER = 1.5;


	public function __construct($logIt = 0) {
		parent::__construct($logIt);
		$this->fromParser = false;
	}

	public function __destruct() {
		// if ($this->log !== null)
		// 	$this->log->writeStringToFile();
	}

	public function record($message, $reason) {
		if ($this->fromParser)
			$this->getClass('ListhubProgress')->add(['type'=>$reason,
													 'data'=>$message]);
		$this->log($message);
	}

	/**
	 * [get description]
	 * @param  [type] $in
	 * @return [type]
	 */
	public function get($in = null){
		$result = parent::get($in);
		$keepListHubNaming = isset($in->keepListHubNaming) ? $in->keepListHubNaming : false;
		if ( !empty($result) &&
			 !$keepListHubNaming ) {
			foreach($result as &$seller) {
				$this->fixName($seller);
			}
		}
		return $result;
	}

	protected function fixName(&$seller) {
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			$meta = (object)$meta; // should be already, but just in case...
			if ($meta->action == SELLER_PROFILE_DATA) {
				if (isset($meta->first_name) &&
					!empty($meta->first_name))
					$seller->first_name = $meta->first_name;
				if (isset($meta->last_name) &&
					!empty($meta->last_name))
					$seller->last_name = $meta->last_name;
				break;
			}
		}
		$seller->first_name = removeslashes($seller->first_name);
		$seller->last_name = removeslashes($seller->last_name);
		if (!empty($seller->about))
			$seller->about = removeslashes($seller->about);
	}

	// called from ajax_developer.php when updaing the SellerEmailDb table
	public function updateEmailDb() {
		$sql = "SELECT %mode% FROM {$this->getTableName()} AS a ";
		$sql.= "INNER JOIN {$this->getTableName('listings')} AS b ";
		$sql.= "WHERE ((b.author_has_account = 0 AND a.id = b.author) OR (b.author_has_account = 1 AND a.author_id = b.author)) AND (b.active = 1 OR b.active = 2)";

		$count = str_replace('%mode%', 'COUNT(DISTINCT a.id)', $sql);
		global $wpdb;
		$count = $wpdb->get_var($count);
		$this->log("updateEmailDb - expecting $count entries");

		$sql = str_replace('%mode%', 'DISTINCT a.*', $sql);
		$page = 0; $pagePer = 5000; $row = 0;
		require_once(__DIR__.'/SellersEmailDb.class.php');
		$emailDb = new SellersEmailDb;
		$count = 0;

		while ( !empty($sellers = $this->rawQuery($sql." LIMIT $row, $pagePer")) ) {
		// if (empty($sellers)) {
		// 	$this->log("updateEmailDb got an empty list.");
		// 	return false;
		// }
			$count++;
			$page++;
			$row = $page * $pagePer;
			foreach($sellers as $seller) {
				if ( !$emailDb->exists(['seller_id'=>$seller->id]) &&
					 !empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$db = (object)['seller_id'=>$seller->id,
										'name'=>$email[0],
										'host'=>strtolower( $email[1] )];
						$emailDb->add($db);
						$this->log("updateEmailDb - sellerId: $seller->id, email: $seller->email, $seller->first_name $seller->last_name");
						$count++;
						unset($db);
					}
				}
				else if (!empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$data = $emailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
						$incoming = strtolower( $email[1] );
						if ($data->host != $incoming) {
							$count++;
							$emailDb->set([(object)['where'=>['seller_id'=>$seller->id],
													'fields'=>['host'=>$incoming]]]);
							$this->log("updateEmailDb - updated sellerId: $seller->id, email: $incoming, $seller->first_name $seller->last_name");
						}
						unset($data);
					}
				}
				unset($seller);
			}
			unset($sellers);
		}
		return $count;
	}

	// called from ajax_developer.php when updaing the SellerEmailDb table
	public function updateAllEmailDb() {
		// $sql = "SELECT %mode% FROM {$this->getTableName()} AS a ";
		// $sql.= "INNER JOIN {$this->getTableName('listings')} AS b ";
		// $sql.= "WHERE ((b.author_has_account = 0 AND a.id = b.author) OR (b.author_has_account = 1 AND a.author_id = b.author)) AND (b.active = 1 OR b.active = 2)";

		// $count = str_replace('%mode%', 'COUNT(DISTINCT a.id)', $sql);
		// global $wpdb;
		// $count = $wpdb->get_var($count);
		// $this->log("updateEmailDb - expecting $count entries");

		// $sql = str_replace('%mode%', 'DISTINCT a.*', $sql);

		header("Connection: close");
		ignore_user_abort(true); // just to be safe
		ob_start();
		$out = array("status" => 'OK',
					"data"=>"Starting to update SellersEmailDb table...");
		echo( json_encode($out) );
		$size = ob_get_length();
		header("Content-Length: $size");
		ob_end_flush(); // Strange behaviour, will not work
		flush(); // Unless both are called !

		$page = 0; $pagePer = 50; $row = 0;
		require_once(__DIR__.'/SellersEmailDb.class.php');
		require_once(__DIR__.'/Listings.class.php');
		$EmailDb = new SellersEmailDb();
		$Listings = new Listings();
		$count = 0;
		$processed = 0;
		$added = 0;
		$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

		// while ( !empty($sellers = $this->rawQuery($sql." LIMIT $row, $pagePer")) ) {
		while ( !empty($sellers = $this->get((object)['what'=>['id', 'author_id', 'email', 'first_name', 'last_name'],
													  'limit'=>$pagePer,
													  'page'=>$page])) ) {
		
			$page++;
			$row = $page * $pagePer;
			//$this->log("updateAllEmailDb - $page, $row - sellercount:".count($sellers) );

			foreach($sellers as $seller) {
				$count++;
				// gather what kind of listings this seller has
				$author_id = isset($seller->author_id) && !empty($seller->author_id) ? $seller->author_id : $seller->id;
				$author_has_account = !empty($seller->author_id) ? 1 : 0;
				$listings = $Listings->get((object)['where'=>['author'=>$author_id,
															  'author_has_account'=>$author_has_account],
													'what'=>['active']]);
				$seller->listingCount = !empty($listings) ? count($listings) : 0;
				$seller->listingTypes = array();
				// collate the listings
				if ($seller->listingCount) foreach($listings as $listing) {
					if (empty($seller->listingTypes[$listing->active]))
						$seller->listingTypes[$listing->active] = 1;
					else
						$seller->listingTypes[$listing->active]++;
				}
				// else
				// 	$this->log("updateAllEmailDb - no listings for sellerId: $seller->id, email: $seller->email, $seller->first_name $seller->last_name");

				$sellerListingFlag = 0;
				if (!empty($seller->listingTypes)) foreach($seller->listingTypes as $type=>$number) {
					$sellerListingFlag |= (1 << (20+intval($type)));
				}

				// check up on existing SellerEmailDb
				//$this->log("updateAllEmailDb - look for $seller->id");
				if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
					 !empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$db = (object)['seller_id'=>$seller->id,
										'name'=>$email[0],
										'flags'=>$sellerListingFlag,
										'host'=>strtolower( $email[1] )];
						$EmailDb->add($db);
						$this->log("updateAllEmailDb - added sellerId: $seller->id, email: $seller->email, $seller->first_name $seller->last_name");
						$added++;
						unset($db);
					}
				}
				else if (!empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$data = $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
						$incoming = strtolower( $email[1] );
						if ($data->host != $incoming ||
							($data->flags & $allFlags) != $sellerListingFlag) {
							$processed++;
							$fields = [];
							$updated = '';
							if ($data->host != $incoming) {
								$fields['host'] = $incoming;
								$updated = 'host ';
							}
							if (($data->flags & $allFlags) != $sellerListingFlag) {
								$updated = (!empty($updated) ? "and " : '')."flags from ".($data->flags & $allFlags)." to $sellerListingFlag";
								$data->flags &= ~$allFlags;
								$fields['flags'] = $data->flags | $sellerListingFlag;
							}

							$EmailDb->set([(object)['where'=>['seller_id'=>$seller->id],
													'fields'=>$fields]]);
							$this->log("updateAllEmailDb - updated:$updated - sellerId: $seller->id, email: $incoming, $seller->first_name $seller->last_name");
							unset($fields);
						}
						unset($data);
					}
				}
				unset($seller, $listings);
				$this->flush();
			}
			$this->log("updateAllEmailDb - finished with ".($page-1).", updated: $processed, added: $added");
			unset($sellers);
		}
		$this->log("updateAllEmailDb - Total sellers: $count, updated: $processed, added: $added");
		die;
		// return new Out('OK', "Total sellers: $count, updated: $processed, added: $added");
	}

	// called from ajax_developer.php when updaing the SellerEmailDb table
	public function updatePageEmailDb($page, $pagePer, $fromParser = false) {
		$this->fromParser = $fromParser;
		$row = 0;
		$msg = "updatePageEmailDb entered for page:$page, pagePer:$pagePer, fromParser:$fromParser";
		$this->record($msg, self::UPDATE_EMAIL_DB_ENTERED);
		require_once(__DIR__.'/SellersEmailDb.class.php');
		require_once(__DIR__.'/Listings.class.php');
		$EmailDb = new SellersEmailDb();
		$Listings = new Listings();
		$count = 0;
		$processed = 0;
		$added = 0;
		$allFlags = SED_ALL_INACTIVE | SED_ACTIVE | SED_WAITING | SED_REJECTED | SED_TOOCHEAP | SED_NEVER | SED_AVERAGE;

		// while ( !empty($sellers = $this->rawQuery($sql." LIMIT $row, $pagePer")) ) {
		if ( !empty($sellers = $this->get((object)['what'=>['id', 'author_id', 'email', 'first_name', 'last_name'],
													  'limit'=>$pagePer,
													  'page'=>$page])) ) {
		
			$row = $page * $pagePer;
			$this->record("updateAllEmailDb - $page, $row - sellercount:".count($sellers), self::UPDATE_EMAIL_DB_START );

			foreach($sellers as $seller) {
				$count++;
				// gather what kind of listings this seller has
				$author_id = isset($seller->author_id) && !empty($seller->author_id) ? $seller->author_id : $seller->id;
				$author_has_account = isset($seller->author_id) && !empty($seller->author_id) ? 1 : 0;
				$listings = $Listings->get((object)['where'=>['author'=>$author_id,
															  'author_has_account'=>$author_has_account],
													'what'=> ['active']]);
				$seller->listingCount = !empty($listings) ? count($listings) : 0;
				$seller->listingTypes = array();
				// collate the listings
				if ($seller->listingCount) foreach($listings as $listing) {
					if (empty($seller->listingTypes[$listing->active]))
						$seller->listingTypes[$listing->active] = 1;
					else
						$seller->listingTypes[$listing->active]++;
				}
				// else
				// 	$this->record("updateAllEmailDb - no listings for sellerId: $seller->id, email: $seller->email, $seller->first_name $seller->last_name", self::UPDATE_EMAIL_DB_NO_LISTING);

				$sellerListingFlag = 0;
				if (!empty($seller->listingTypes)) foreach($seller->listingTypes as $type=>$number) {
					$sellerListingFlag |= (1 << (20+intval($type)));
				}

				// check up on existing SellerEmailDb
				// $this->log("updateAllEmailDb - look for $seller->id");
				if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
					 !empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$db = (object)['seller_id'=>$seller->id,
										'name'=>$email[0],
										'flags'=>$sellerListingFlag,
										'host'=>strtolower( $email[1] )];
						$EmailDb->add($db);
						$this->record("updateAllEmailDb - added sellerId: $seller->id, email: $seller->email, $seller->first_name $seller->last_name", self::UPDATE_EMAIL_DB_ADDED);
						$added++;
						unset($db);
					}
				}
				else if (!empty($seller->email)) {
					$email = explode('@', $seller->email);
					if (count($email) == 2) {
						$data = $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
						$incoming = strtolower( $email[1] );
						if ($data->host != $incoming ||
							($data->flags & $allFlags) != $sellerListingFlag) {
							$processed++;
							$fields = [];
							$updated = '';
							if ($data->host != $incoming) {
								$fields['host'] = $incoming;
								$updated = 'host ';
							}
							if (($data->flags & $allFlags) != $sellerListingFlag) {
								$updated = (!empty($updated) ? "and " : '')."flags from ".($data->flags & $allFlags)." to $sellerListingFlag";
								$data->flags &= ~$allFlags;
								$fields['flags'] = $data->flags | $sellerListingFlag;
							}

							$EmailDb->set([(object)['where'=>['seller_id'=>$seller->id],
													'fields'=>$fields]]);
							$this->record("updateAllEmailDb - updated:$updated- sellerId: $seller->id, email: $incoming, $seller->first_name $seller->last_name", self::UPDATE_EMAIL_DB_UPDATED);
							unset($fields);
						}
						unset($data);
					}
				}
				unset($seller, $listings);
				$this->flush();
			}
			$this->record("updateAllEmailDb - finished with ".($page-1).", Total sellers: $count, updated: $processed, added: $added", self::UPDATE_EMAIL_DB_END);
			unset($sellers);
			return new Out('OK', "Total sellers: $count, updated: $processed, added: $added");
		}
		else
			return new Out('fail', "No sellers on page:$page with pagerPer:$pagePer");
	}

	public function getAMTagList() {
		global  $tag_city;
		global  $tag_listing;

		require_once(__DIR__.'/Tags.class.php'); $Tags = new Tags();
		$cityTags = $Tags->get((object)['where'=>['id'=>$tag_city]]);
		$listingTags = $Tags->get((object)['where'=>['id'=>$tag_listing]]);
		$extraTags = $Tags->get((object)['where'=>['id'=>$this->tag_extra]]);

		$mergedList = array_merge($cityTags, $listingTags, $extraTags);
		$tagList = [];
		foreach($mergedList as $i=>$tag) {
			if ($tag->id == 76) // airstrip
				continue;
			elseif ($tag->id == 44)
				$tag->tag = 'aviation friendly';
			$tag->tag = ucwords($tag->tag);
			$tagList[$tag->id] = $tag;
		}
		unset($cityTags, $listingTags, $mergedList, $extraTags);

		usort($tagList, function($a, $b) {
			return strcmp($a->tag, $b->tag);
		});

		$x = [];
		foreach($tagList as $tag)
			$x[$tag->id] = $tag;
		unset($tagList);
		$tagList = $x;

		return $tagList;
	}

	public function premierAgents($tags, $city_id, $portalAgent, $listingId, $sampleView = false, $userId = 0, $quizId = 0) {
		if (count($tags) == 0) {
			$this->log("Exiting premierAgents, tags count is 0.");
			return "0";
		}

		if ($city_id == -1) {
			$this->log("Exiting premierAgents, empty city id");
			return "0";
		}

		require_once(__DIR__.'/SessionMgr.class.php'); $SessionMgr = new SessionMgr(1);
		$id = 0;
		$session_id = $SessionMgr->getCurrentSession($id);


		$haveSession = isset($_SESSION) && !empty($_SESSION);
		global $agency_id;
		$agency_id = ($haveSession && isset($_SESSION['AGENCY_ID']) && !empty($_SESSION['AGENCY_ID'])) ? $_SESSION['AGENCY_ID'] : 
					 (isset($_COOKIE['AGENCY_ID']) && !empty($_COOKIE['AGENCY_ID']) ? $_COOKIE['AGENCY_ID'] : 0);
		global $sampling;
		$sampling = $sampleView;
		global $author_id;
		$author_id = intval($userId);

		$this->log("premierAgents - agency_id:$agency_id");

		$Cities = new Cities();
		global $Sellers;
		$Sellers = $this;


		if (empty($city_id)) {
			$Listings = new Listings();
			$listing = $Listings->get((object)['where'=>['id'=>$listingId]]);
			if (!empty($listing) &&
				!empty($listing[0]->city) &&
				!empty($listing[0]->state)) {
				$city = $Cities->get((object)['like'=>['city'=>$listing[0]->city],
											  'where'=>['state'=>$listing[0]->state]]);
				if (!empty($city)) {
					$city_id = $city[0]->id;
					$this->log("premierAgents - found cityId: $city_id, using listingId: $listingId city: $listing[0]->city and state: $listing[0]->state");
					unset($city);
				}	
				unset($listing);		
			}
			// still empty?
			if (empty($city_id)) {
				$this->log("Exiting premierAgents, city id is 0.");
				return "0";
			}
		}

		$quizId = $quizId == 0 ? (isset($_COOKIE['QuizId']) ? $_COOKIE['QuizId'] : -1) : $quizId;
		$quizCompleted = isset($_COOKIE['QuizCompleted']) ? $_COOKIE['QuizCompleted'] : -1;
		$quizSession = isset($_COOKIE['QuizSession']) ? $_COOKIE['QuizSession'] : session_id();
		$quizTags = null;
		$location = null;
		$distance = 0;
		
		global $useCityScore, $searchAll;
		$service_area = null;
		$searchAll = false;
		$useCityScore = false;

		require_once(__DIR__.'/Options.class.php'); $Options = new Options();

		$opt = $Options->get((object)['where'=>['opt'=>'QuizGrabAllIntoCities']]);
		$quizGrabAllIntoCities = !empty($opt) ? is_true($opt[0]->value) : 0;

		$this->log("premierAgents - entered for quizId:$quizId, quizCompleted:$quizCompleted, quizSession:$quizSession, session_id:$session_id, id:$id, cityId: $city_id, listingId: $listingId, ".(!empty($portalAgent) ? "have PortalAgent - allow AM agents:".(($portalAgent->flags & SELLER_INACTIVE_EXCLUSION_AREA) == 0 ? "no" : "yes") : "no PortalAgent").", sampling:$sampling, author_id:$author_id, userId:$userId, quizGrabAllIntoCities: ".($quizGrabAllIntoCities ? "yes" : "no").", listing tags:".print_r($tags, true));

		// try to get the service area setup
		if ( gettype($portalAgent) == 'object') {
			$city = ''; $state = '';
			if (isset($portalAgent->service_areas) && // use the service area from the profile if defined.
				!empty($portalAgent->service_areas)) {
				$areas = explode(":", $portalAgent->service_areas);
				$areas = explode(',', $areas[0]);
				if (count($areas) == 2) {
					$city = trim($areas[0]);
					$state = trim($areas[1]);
				}
			}
			else if (isset($portalAgent->city) && // else use the city/state the agent is registered in
					 isset($portalAgent->state) &&
					 !empty($portalAgent->city) &&
					 !empty($portalAgent->state)) {
				$city = $portalAgent->city;
				$state = $portalAgent->state;
			}

			// try to get this city/state if we have the data
			if (!empty($city) &&
				!empty($state)) {
				$city = $Cities->get((object)['where'=>['city'=>$city,
														'state'=>$state]]);
				if (!empty($city)) {
					$service_area = array_pop($city);
					$service_area->range = 20; // default radius
				}
			}

			// if good so far, see if the extra exclusion option was purchased
			if (!empty($service_area) &&
				!empty($portalAgent->meta)) {
				$gotIt = false;
				foreach($portalAgent->meta as $info) {
					if ($info->action == SELLER_AGENT_ORDER &&
						!empty($info->item)) foreach($info->item as $item) {
							if ($item->type == ORDER_PORTAL_EXCLUSION_1 &&
								$item->mode == ORDER_BOUGHT) {
								$service_area->range = intval($item->range);
								$gotIt = true;
							}
							unset($item);
							if ($gotIt) break;
					}
					unset($info);
					if ($gotIt) break;
				}
			}
		}

		if (!$sampling &&
			isset($quizId) && isset($quizSession)) {
			$qa = new QuizActivity();
			if (!$quizGrabAllIntoCities)
				$a = $qa->get((object) array('where'=>array('session_id'=>$quizSession)));
			else
				$a = $qa->get((object) array('where'=>array('id'=>$quizId)));
			if (!empty($a)) {
				$activity = array_pop($a);
				$size = count($activity->data);
				if ($quizId == -1 ||
					$quizId >= $size)
					$last_quiz = array_pop($activity->data); // pop the last quiz
				else
					$last_quiz = $activity->data[$quizId];
				

				if (isset($last_quiz->results)) {
					$results = array_pop($last_quiz->results); // pop the last result set
					if (!empty($results)) {
						$quizTags = $results->tags;
						$location = $results->location;
						$distance = $agency_id == 0 ? ($results->distance < 10 ? 10 : $results->distance) : 2.5;
						$this->log("premierAgents - quizTags:".print_r($quizTags, true));
						if (empty($quizTags)) {
							$this->log("premierAgents - quizTags was empty, assigning listing's tags as quizTags");
							$searchAll = true;
							$quizTags = $tags;
							// if (!empty($quizTags)) foreach($quizTags as $tag)
							// 	$tag->percent = 100;
						}
						// elseif (!empty($tags)) {
						// 	$temp = [];
						// 	foreach($quizTags as $tag)
						// 		$temp[intval($tag->id)] = $tag;
						// 	unset($quizTags);
						// 	$keys = array_keys($temp);
						// 	foreach($tags as $tag)
						// 		if ( !in_array(intval($tag->id), $keys) )
						// 			$temp[intval($tag->id)] = $tag;
						// 	$quizTags = $temp;
						// 	$this->log("premierAgents - merged quiz and listing tags to:".print_r($quizTags, true));
						// }
					}
				}
			}
			else
				$this->log("premierAgents could not find any quiz for session:$quizSession");
		}
		
		if (empty($quizTags) ) {
			$this->log("premierAgents - using listing tags, no quiz found or used, sampling is $sampling");
			$quizTags = $tags; // use listing's tags
			$searchAll = true;
		}

		$googleGeoCoder = new GoogleLocation;
		$city = $Cities->get((object) array('where'=>array('id'=>$city_id)));
		if (!empty($city)) { // check if it needs geocoding
			$city = array_pop($city);
			if ($city->lng == -1 || $city->lng == 0) {
				// do getCode for this city
				$c = (object)['city'=>$city->city,
								 'state' => $city->state,
								 'country' => $city->country,
								 'lng' => null,
								 'lat' => null];
				// if (!empty($listing['zip']))
				// 	$city->zip = $listing['zip'];
				$lng = $lat = -1;
				$result_city = $google_result = null;
				try {
					$Cities->geocodeCity($googleGeoCoder, $c, $lng, $lat, $result_city, $google_result);
				}
				catch(\Exception $e) {
					parseException($e);
					return "0";
				}
				if ($lng == -1) 
					$this->log("premierAgents - failed geocode for $city->city, $city->state with id $city_id");
				$city->lng = $lng;
				$city->lat = $lat;
			}
		}
		else
			$this->log("Got empty city list @1.");

		if (!empty($service_area)) { // check if it needs geocoding
			if ($service_area->lng == -1 || $service_area->lng == 0) { // try to geocode it now...
				// do getCode for this city
				$c = (object)['city'=>$service_area->city,
								 'state' => $service_area->state,
								 'country' => $service_area->country,
								 'lng' => null,
								 'lat' => null];
				// if (!empty($listing['zip']))
				// 	$city->zip = $listing['zip'];
				$lng = $lat = -1;
				$result_city = $google_result = null;
				try {
					$Cities->geocodeCity($googleGeoCoder, $c, $lng, $lat, $result_city, $google_result);
				}
				catch(\Exception $e) {
					parseException($e);
					return;
				}
				$service_area->lng = $lng;
				$service_area->lat = $lat;
				if ($lng != -1)
					$Cities->set([(object)[ 'where'=>['id'=>$service_area->id],
											'fields'=>[ 'lng'=>$lng,
														'lat'=>$lat]]]);
			}
		}
		else
			$this->log("Got empty service_area list @1.");		

		// formulate the cities query that should fall within the search radius of the listing
		require_once(__DIR__.'/DistanceScale.php');
		$locationQuery = '';
		$extraInnerJoin = '';
		if (!empty($city)) { // this is the city of the listing
			if ($city->lng != -1) {
				$lat = (int)(abs( is_float($city->lat) ? $city->lat : floatval($city->lat) ) / 10);
				$deltaLng = $distance / $DistanceScale[$lat][0];
				$deltaLat = $distance / $DistanceScale[$lat][1]; // latitude remains pretty constant at about 60miles/degree
				$locationQuery .= "(c.lng >= ".($city->lng - $deltaLng)." AND c.lng <= ".($city->lng + $deltaLng)." AND c.lat >= ".($city->lat - $deltaLat)." AND c.lat <= ".($city->lat + $deltaLat).")";
			}
			else {
				$this->log("premierAgents - has no geocode for $city->city, $city->state with id $city_id");
				$locationQuery .= "(c.city = '".addslashes($city->city)."' AND c.state = '$city->state')";
			}
		}
		else
			$this->log("Got empty city list @2.");

		// basic query for the cities within range
		$sql = "SELECT c.id, c.city, c.state FROM ".$Cities->getTableName($Cities->table)." AS c WHERE ".$locationQuery;

		// if the portal agent was defined, exclude those cities that may fall within the exclusion area
		if (!empty($service_area) &&
			($portalAgent->flags & SELLER_INACTIVE_EXCLUSION_AREA) == 0 && //  exclude all AM agents
			$service_area->lat != -1 &&
			$service_area->lat != 0) {
			$lat = (int)(abs( is_float($service_area->lat) ? $service_area->lat : floatval($service_area->lat) ) / 10);
			$deltaLng = $service_area->range / $DistanceScale[$lat][0];
			$deltaLat = $service_area->range / $DistanceScale[$lat][1]; 
			$sql.= " AND !"."(c.lng >= ".($service_area->lng - $deltaLng)." AND c.lng <= ".($service_area->lng + $deltaLng)." AND c.lat >= ".($service_area->lat - $deltaLat)." AND c.lat <= ".($service_area->lat + $deltaLat).")";
		}

		// now go get all those cities that fall within the query
		$this->log("premierAgents - get cities sql: $sql");
		$cities = $Cities->rawQuery($sql);
		$temp = array();
		if (!empty($cities)) foreach($cities as $oneCity)
			$temp[$oneCity->id] = $oneCity;
		$cities = $temp;
		unset($temp);

		if (!array_key_exists($city_id, $cities) ) {// add original into the list
			if (!empty($service_area) && // if there isn't portalAgent and the city doesn't
				$service_area->lat != -1 && // fall within the portalAgent service area.
				$service_area->lat != 0 &&
				$city->lat != -1 &&
				$city->lat != 0) {
				$lat = (int)(abs( is_float($service_area->lat) ? $service_area->lat : floatval($service_area->lat) ) / 10);
				$deltaLng = $service_area->range / $DistanceScale[$lat][0];
				$deltaLat = $service_area->range / $DistanceScale[$lat][1]; 
				if (!($city->lng >= ($service_area->lng - $deltaLng) &&
					  $city->lng <= ($service_area->lng + $deltaLng) &&
					  $city->lat >= ($service_area->lat - $deltaLat) &&
					  $city->lat <= ($service_area->lat + $deltaLat)) )
					$cities[$city_id] = $city;
			}
			else if (!empty($service_area) && // then maybe the lng/lat wasn't defined, so check the city/state names directly
					 $service_area->city != $city->city &&
					 $service_area->state != $city->state)
				$cities[$city_id] = $city;
			else // go ahead and add it.
				$cities[$city_id] = $city;
		}

		// now with the completed cities list, create the query section to define the cities that
		// the agent match agents do business in.
		$first = true;
		$extraCities = '';
		if (!empty($cities)) {
			$extraCities = " AND (";
			foreach($cities as $city) {
				if (empty($city))
					continue;
				if ( ($pos = strpos($city->city, "(")) !== false ) {
					$city->city = trim(substr($city->city, 0, $pos));
				}
				$first ? $first = false : $extraCities .= ' OR ';
				// $extraCities .= '(b.city = "'.$city->city.'")';
				$extraCities .= ($agency_id == 0 ? '(a.city_id = '.$city->id.') OR ' : '').'(b.city = "'.addslashes($city->city).'")';
				unset($city);
			}
			$extraCities .= ")";
		}
		else {
			$this->log("Got empty city list @3.");
			return "0";
		}

		// now create the tags section of the query
		$agentTagTable = getTableName('sellers-tags');
		$tagQuery = '(';
		$first = true;
		$tagMatch = 0;
		$gotTag = false;
		$gotHeli = false;
		$gotAirstrip = false;
		$tagWeighted = [];
		global  $tag_city;
		global  $tag_listing;

		// before we go on, let's make sure that the $quizTags has some percentage on them and not 0'ed out.
		foreach($quizTags as &$tag) {
			if (!isset($tag->percent) ||
				empty($tag->percent) ||
				floatval($tag->percent) == 0)
				$tag->percent = 1;
			unset($tag);
		}

		$this->log("premierAgents - after setting empty percents:".print_r($quizTags, true));

		if (isset($quizTags) && !empty($quizTags)) foreach($quizTags as $tag) {
			if (in_array($tag->id, $tag_city) ||
				in_array($tag->id, $tag_listing)) {
				$first ? $first = false : $tagQuery .= " OR ";
				$tagQuery .= "`tag_id` = ".$tag->id;
				if ($tag->id == 44)
					$gotHeli = true;
				if ($tag->id == 76)
					$gotAirstrip = true;
				$gotTag = true;
				$tagMatch++;
				if (in_array($tag->id, $tag_city)) // it's a city tag, so give it more weight than a listing tag
					$tag->percent = $tag->percent * self::CITY_TAG_WEIGHT_MULTIPLIER;
				$tagWeighted[$tag->id] = $tag;
				$this->log("add to weighted:".print_r($tag, true));
				// $tagWeighted[$tag->id]->percent = isset($quizTags[$tag->id]->percent) ? floatval($quizTags[$tag->id]->percent) : 0;
			}
			unset($tag);
		}

		// add multi-lingual
		$first ? $first = false : $tagQuery .= " OR ";
		$tagQuery .= "`tag_id` = 170"; // "multi-lingual"
		$gotTag = true;
		$tagMatch++;
		$tagWeighted[170] = (object) ['tag'=>'multilingual',
										 'id'=>170,
										 'percent'=>50.0];

		if ($gotAirstrip &&
			!$gotHeli) {
			$first ? $first = false : $tagQuery .= " OR ";
			$tagQuery .= "`tag_id` = 44"; // force heli-pad to match with 'Aviation' in AgentMatch
			$gotTag = true;
			$tagMatch++;
			$tagWeighted[44] = (object) ['tag'=>'heli pad',
										 'id'=>44,
										 'percent'=>$tagWeighted[76]->percent];
		}

		if (!$gotAirstrip &&
			$gotHeli) {
			$first ? $first = false : $tagQuery .= " OR ";
			$tagQuery .= "`tag_id` = 76"; // force heli-pad to match with 'Aviation' in AgentMatch
			$gotTag = true;
			$tagMatch++;
			$tagWeighted[76] = (object) ['tag'=>'heli pad',
										 'id'=>76,
										 'percent'=>$tagWeighted[44]->percent];
		}

		$this->log("premierAgents - tagWeighted: ".print_r($tagWeighted, true));

		$tagQuery .= ")";
		if (!$gotTag)
			return "0";

		$sellersTable = $this->getTableName($this->table);
		$citiesTable = $Cities->getTableName($Cities->table);
		$tagsTable = getTableName('tags');

		if ($agency_id == 0) {
			// now construct the query to get all AM agents whose tags match the listing and are within the cities in range and not excluded by the portal agent
			// $sql = "SELECT a.author_id, b.id, b.first_name, b.last_name, b.photo, b.phone, b.city, b.state, b.company, b.meta, GROUP_CONCAT(DISTINCT a.tag_id,':',a.score,':',c.icon,':',c.tag,':',c.description,':',a.type SEPARATOR '|') as tags FROM ".$agentTagTable." AS a ";
			$sql = "SELECT a.author_id, b.id, b.first_name, b.last_name, b.photo, b.phone, b.city, b.state, b.company, b.meta, GROUP_CONCAT( CONCAT("."'".'{"tag_id":'."'".',a.tag_id,'."'".',"score":'."'".',a.score,'."',";
			$sql .= '"icon":'.'"'."',c.icon,'".'",';
			$sql .= '"tag":'.'"'."',c.tag,'".'",';
			$sql .= '"desc":'.'"'."',c.description,'".'",';
			$sql .= '"type"'.":',a.type,".'"}"'.") SEPARATOR ',') as tags FROM ".$agentTagTable." AS a ";
			$sql .= " INNER JOIN $sellersTable AS b ON a.author_id = b.author_id";
			$sql .= " INNER JOIN $tagsTable AS c on c.id = a.tag_id";
			$sql .= " WHERE ".$tagQuery." AND a.type = 1 AND a.city_id = $city_id AND b.author_id IS NOT NULL AND b.flags & ".(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)." AND !(b.flags & ".SELLER_NEEDS_BASIC_INFO.")"; //.$extraCities;
			$sql .= " GROUP BY a.author_id";
		}
		else {
			$sql = "SELECT b.author_id, b.id, b.first_name, b.last_name, b.photo, b.phone, b.city, b.state, b.company, b.meta FROM $sellersTable AS b ";
			$sql .= " WHERE b.association = $agency_id ".$extraCities;
			// $sql .= " GROUP BY b.author_id";
		}
		$this->log("premierAgents - sql:$sql");
		try {
			$x = $this->rawQuery($sql);
		}
		catch(\Exception $e) {
			return "0";
		}

		$normalizer = 1.0/$tagMatch;
		// did we get any agents?
		if (!empty($x)) {
			if ($agency_id != 0) {
				$ids = [];
				require_once(__DIR__.'/SellersTags.class.php'); $SellersTags = new SellersTags;
				foreach($x as $i=>&$seller) {
					$seller->phone = fixPhone($seller->phone);
					$seller->percentage = 0;
					$amStats = null;
					$seller->meta = !empty($seller->meta) ? json_decode($seller->meta) : [];
					$seller->author_id = isset($seller->author_id) ? intval($seller->author_id) : 0;
					$this->fixName($seller);
					$metas = [];
					if (!empty($seller->meta)) foreach($seller->meta as $meta) {
						if ($meta->action == SELLER_AGENT_MATCH_STATUS) {
							$amStats = $meta;
						}
						else
							$metas[] = $meta; // save it
					}

					if (!$amStats) {
						$amStats = new \stdClass();
						$amStats->action = SELLER_AGENT_MATCH_STATUS;
						$amStats->count = 1;
						$amStats->tags = [];
						$amStats->cities = [$city_id=>['count'=>1]];
						$amStats->listings = [$listingId=>['count'=>1]];
					}
					else {
						$amStats->count++;
						if ( property_exists($amStats->cities, $city_id) ) 
							$amStats->cities->$city_id->count++;
						else
							$amStats->cities->$city_id = ['count'=>1];

						if ( property_exists($amStats->listings, $listingId) ) 
							$amStats->listings->$listingId->count++;
						else
							$amStats->listings->$listingId = ['count'=>1];
					}

					$seller->percentage = 0;
					$seller->tagCount = 0;
					$seller->shown = $amStats->count;
					$seller->nickname = '';
					$seller->tags = [];
					if (!empty($seller->author_id)) {
						$info = get_userdata($seller->author_id);
						$nickname = !empty($info->nickname) ? $info->nickname : $info->user_nicename;
						$seller->nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
						// $tags = $SellersTags->get((object)['where'=>['author_id'=>$seller->author_id]]);
						$sql = "SELECT a.*, b.icon, b.description, b.tag FROM $agentTagTable as a ";
						$sql.= "INNER JOIN $tagsTable as b ON b.id = a.tag_id ";
						$sql.= "WHERE `author_id` = $seller->author_id AND a.type = 1 AND a.city_id = $city_id AND $tagQuery";
						$this->log("premierAgents - sql agency tags:$sql");
						try {
							$y = $this->rawQuery($sql);
						}
						catch(\Exception $e) {
							return "0";
						}
						if (!empty($y)) {
							$gotAviation = false;
							$z = [];
							foreach($y as &$tag) {
								$id = intval($tag->tag_id);						
								if ($id == 76 ||  // airstrip
									$id == 44) {  // heli-pad
									if ($gotAviation)
										continue; // don't double up
									$tag->tag = "Aviation Friendly";
									$gotAviation = true;
								}
								$z[] = $tag;
							}
							unset($y);
							$sellerTagList = $z;
							$seller->tagCount = count($z);	
							$this->processSellerStatsAndPercentages($seller, $amStats, $sellerTagList, $tagWeighted, $normalizer, $searchAll, $useCityScore);
						} // end if (!empty($x)) - got tags!
						$metas[] = $amStats;
						$this->set([(object)['where'=>['id'=>$seller->id],
														'fields'=>['meta'=>$metas]]]);
						$seller->meta = $metas;
					} // end if (!empty($seller->author_id)) - registered user
					unset($seller);
				} // end foreach($seller found)
			}
			else { // we are doing regular agent match stuff			
				$this->log("premierAgents - normalizer is $normalizer, with $tagMatch tags");
				foreach($x as $i=>&$seller) {
					$seller->phone = fixPhone($seller->phone);
					$info = get_userdata($seller->author_id);
					$nickname = !empty($info->nickname) ? $info->nickname : $info->user_nicename;
					$seller->nickname = stripos($nickname, 'unknown') === false ? $nickname : '';
					$seller->percentage = 0;
					$amStats = null;
					$seller->meta = !empty($seller->meta) ? json_decode($seller->meta) : [];
					$this->fixName($seller);
					$metas = [];
					if (!empty($seller->meta)) foreach($seller->meta as $meta) {
						if ($meta->action == SELLER_AGENT_MATCH_STATUS) {
							$amStats = $meta;
						}
						else
							$metas[] = $meta; // save it
					}

					if (!$amStats) {
						$amStats = new \stdClass();
						$amStats->action = SELLER_AGENT_MATCH_STATUS;
						$amStats->count = 1;
						$amStats->tags = [];
						$amStats->cities = [$city_id=>['count'=>1]];
						$amStats->listings = [$listingId=>['count'=>1]];
					}
					else {
						$amStats->count++;
						if ( property_exists($amStats->cities, $city_id) ) 
							$amStats->cities->$city_id->count++;
						else
							$amStats->cities->$city_id = ['count'=>1];

						if ( property_exists($amStats->listings, $listingId) ) 
							$amStats->listings->$listingId->count++;
						else
							$amStats->listings->$listingId = ['count'=>1];
					}

					$seller->percentage = 0;
					$seller->tagCount = 0;
					$seller->shown = $amStats->count;

					if (!empty($seller->tags)) {
						// $normalizer = count($seller->tags)/$tagMatch;
						$seller->tags = '['.$seller->tags.']';
						$seller->tags = json_decode($seller->tags);
						$sellerTagList = [];
						$gotAviation = false;

						foreach($seller->tags as $tag) {
							if ($tag->tag_id == 76 ||
								$tag->tag_id == 44) {
								if ($gotAviation)
									continue; // don't double up
								$tag->tag = "Aviation Friendly";
								$gotAviation = true;
							}
							$sellerTagList[$tag->tag_id] = $tag;
							unset($tag);
						}
						
						$seller->tags = [];
						// $rawTags = explode('|', $seller->tags);
						// $seller->tags = [];
						// $sellerTagList = [];
						// $gotAviation = false;
						// foreach($rawTags as $row) {
						// 	$elements = explode(':', $row);
						// 	$id = intval($elements[0]);
						// 	if ($id == 76 ||  // airstrip
						// 		$id == 44) {  // heli-pad
						// 		if ($gotAviation)
						// 			continue; // don't double up
						// 		$elements[3] = "Aviation Friendly";
						// 		$gotAviation = true;
						// 	}
						// 	$sellerTagList[$id] = (object)['tag_id'=>$id,
						// 									'score'=>$elements[1],
						// 									'icon'=>$elements[2],
						// 									'tag'=>$elements[3],
						// 									'desc'=>$elements[4],
						// 									'type'=>intval($elements[5])];
						// 	unset($row, $elements);					
						// }
						// unset($rawTags);
						$seller->tagCount = count($sellerTagList);				

						$this->processSellerStatsAndPercentages($seller, $amStats, $sellerTagList, $tagWeighted, $normalizer, $searchAll, $useCityScore, $city_id);
					}

					$metas[] = $amStats;
					$this->set([(object)['where'=>['id'=>$seller->id],
													'fields'=>['meta'=>$metas]]]);
					$seller->meta = $metas;
					unset($seller);
				}
			} // not agency_id

			usort( $x, function($a, $b) {
				global $useCityScore, $searchAll, $agency_id, $sampling, $author_id, $Sellers;
				if ($useCityScore || $searchAll) {
					if ($sampling) {
						if (isset($a->author_id) &&
							!empty($a->author_id) &&
							$a->author_id == $author_id) {
							// $Sellers->log("usort - @1-a author_id:$author_id matched");
							return -1;
						}
						elseif (isset($b->author_id) &&
							!empty($b->author_id) &&
							$b->author_id == $author_id) {
							// $Sellers->log("usort - @1-b author_id:$author_id matched");
							return 1;
						}
					}

					if ($b->percentage != $a->percentage) 
						return $b->percentage - $a->percentage;
					elseif ($agency_id == 0) 
						return $a->shown - $b->shown; // least number of shown agent comes first
					else {
						if ( (!empty($a->author_id) && !empty($b->author_id)) ||
							 (empty($a->author_id) && empty($b->author_id)))
							return $a->shown - $b->shown; // least number of shown agent comes first
						else
							return !empty($a->author_id) ? -1 : 1; // else registered agents come first
					}
				}
				else {
					if ($sampling) {
						if (isset($a->author_id) &&
							!empty($a->author_id) &&
							$a->author_id == $author_id) {
							// $Sellers->log("usort - @2-a author_id:$author_id matched");
							return -1;
						}
						elseif (isset($b->author_id) &&
							!empty($b->author_id) &&
							$b->author_id == $author_id) {
							// $Sellers->log("usort - @2-b author_id:$author_id matched");
							return 1;
						}
					}

					if ($b->percentage != $a->percentage)
						return $b->percentage - $a->percentage;
					else if ($b->tagCount != $a->tagCount)
						return $b->tagCount - $a->tagCount;
					elseif ($agency_id == 0) 
						return $a->shown - $b->shown; // least number of shown agent comes first
					else {
						if ( (!empty($a->author_id) && !empty($b->author_id)) ||
							 (empty($a->author_id) && empty($b->author_id)))
							return $a->shown - $b->shown; // least number of shown agent comes first
						else
							return !empty($a->author_id) ? -1 : 1; // else registered agents come first
					}
				}
			});

			$this->updateTopThreeAgents($x);
		} // if (!empty($x)) - found agents
		else
			$this->log("Got empty sellers list - sql:$sql");

		return empty($x) ? "0" : $x;
	}

	protected function updateTopThreeAgents(&$sellers) {
		$i = 0;
		global $agency_id;
		$goodOnes = [];
		if (!empty($sellers)) foreach($sellers as $seller) {
			$metas = [];
			$amStats = null;
			if (!empty($seller->meta)) foreach($seller->meta as $meta) {
				if ($meta->action == SELLER_AGENT_MATCH_STATUS) {
					$amStats = $meta;
				}
				else
					$metas[] = $meta; // save it
			}

			if (empty($amStats)) {
				$this->log("Failed to find SELLER_AGENT_MATCH_STATUS in updateTopThreeAgents() for seller:$seller->id, author_id:".(!empty($seller->author_id) ? $seller->author_id : "N/A")." agency_id:$agency_id");
				continue;
			}

			if ($i < 3) {
				$amStats->count++;
				$seller->shown = $amStats->count;
				$metas[] = $amStats;
				$this->set([(object)['where'=>['id'=>$seller->id],
												'fields'=>['meta'=>$metas]]]);
				$this->log("updateTopThreeAgents incremented seller:$seller->id count value in SELLER_AGENT_MATCH_STATUS");
				$i++;
			}
			$goodOnes[] = $seller;
			unset($seller);
		}
		$sellers = $goodOnes;
	}

	protected function processSellerStatsAndPercentages(&$seller, &$amStats, &$sellerTagList, $tagWeighted, $normalizer, $searchAll, $useCityScore, $city_id) {
		//$this->log("processSellerStatsAndPercentages - seller:$seller->id, normalizer:$normalizer, searchAll:".($searchAll ? 'yes' : 'no').", useCityScore:".($useCityScore ? 'yes' : 'no').", tagWeighted:".print_r($tagWeighted, true).', sellerTagList:'.print_r($sellerTagList, true));
		$amOrder = null;
		$city_id = intval($city_id);
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amOrder = $meta;
			}
			unset($meta);
			if ($amOrder) break;
		}

		if (!empty($amOrder) &&
			(empty($seller->city) ||
			 empty($seller->state)) ) { // then set the seller's city state to match a bought lifestyle's locationStr
			$break = false;
			foreach($amOrder->item as $order) {
				if ($order->type == ORDER_AGENT_MATCH &&
					$order->mode == ORDER_BOUGHT) {
					$parts = explode(',', $order->locationStr);
					$seller->city = $parts[0];
					$seller->state = trim($parts[1]);
					$break = true;
				}
				unset($order);
				if ($break) break;
			}
			unset($amOrder);
		}

		if ($searchAll) {
			$seller->percentage = $seller->tagCount;
			foreach($sellerTagList as $tag) {
				$seller->tags[$tag->tag_id] = $tag;
				$seller->tags[$tag->tag_id]->percent = isset($tagWeighted[$tag->tag_id]) ? $tagWeighted[$tag->tag_id]->percent : 0;
				$tagId = $tag->tag_id;
				$amStats->tags = (object)$amStats->tags;
				if ( property_exists($amStats->tags, $tagId) ) 
					$amStats->tags->$tagId->count++;
				else
					$amStats->tags->$tagId = ['count'=>1];
			}
		}
		else if ($useCityScore) {
			foreach($sellerTagList as $tag) {
				$this->log("premierAgents - for seller:{$seller->id}, tags:".print_r($tag, true) );
				$score = $tag->score == '-1' ? 1.0 : ($tag->score == '0' ? 0.8 : intval($tag->score)/10);
				$seller->tags[$tag->tag_id] = $tag;
				$seller->tags[$tag->tag_id]->percent = isset($tagWeighted[$tag->tag_id]) ? $tagWeighted[$tag->tag_id]->percent : 0;
				$seller->percentage += $normalizer * $score;
				$tagId = $tag->tag_id;
				$amStats->tags = (object)$amStats->tags;
				if ( property_exists($amStats->tags, $tagId) ) 
					$amStats->tags->$tagId->count++;
				else
					$amStats->tags->$tagId = ['count'=>1];
			}
			$seller->percentage = round($seller->percentage *100);
		}
		else { // using weights from quiz (user preference)
			foreach($sellerTagList as $tag) {
				if ( array_key_exists($tag->tag_id, $tagWeighted) ) {
					$seller->tags[$tag->tag_id] = $tag;
					$seller->tags[$tag->tag_id]->percent = $tagWeighted[$tag->tag_id]->percent;
					$seller->percentage += $normalizer * $tagWeighted[$tag->tag_id]->percent;
				}
				$tagId = $tag->tag_id;
				$amStats->tags = (object)$amStats->tags;
				if ( property_exists($amStats->tags, $tagId) ) 
					$amStats->tags->$tagId->count++;
				else
					$amStats->tags->$tagId = ['count'=>1];
			}
			$seller->percentage = round($seller->percentage);
		}
		unset($sellerTagList);

		uasort($seller->tags, function($a, $b) {
			return intval($b->percent) - intval($a->percent);
		});

		// $this->log("processSellerStatsAndPercentages - seller:$seller->id, tags:".print_r($seller->tags, true));

		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			$meta = (object)$meta;
			if ($meta->action == SELLER_AGENT_ORDER) {
				foreach($meta->item as $item) {
					$item = (object)$item;
					if ($item->type == ORDER_AGENT_MATCH &&
						$item->mode == ORDER_BOUGHT) {
						$break = false;
						foreach($seller->tags as $id=>&$tag) {
							$id = intval($id);
							// $this->log("processSellerStatsAndPercentages - seller:$seller->id, test tag:$id");
							if ($id == intval($item->specialty) &&
								$city_id == intval($item->location)) {
								// $this->log("processSellerStatsAndPercentages - seller:$seller->id, tag:$id matched item->specialty:$item->specialty, decs:".removeslashes($item->desc));
								$tag->user_profile = removeslashes($item->desc);
								$break = true;
							}
							unset($tag);
							if ($break) break;
						}
					}
					unset($item);
				}
			}
			unset($meta);
		}

		$this->log("processSellerStatsAndPercentages - seller:$seller->id, after user_profiles, tags:".print_r($seller->tags, true));
	}
}