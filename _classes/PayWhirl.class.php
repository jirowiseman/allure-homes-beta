<?php
namespace AH;
require_once (__DIR__  . '/../vendor/paywhirl/PayWhirl.php');
require_once(__DIR__.'/../_classes/Utility.class.php'); 
require_once(__DIR__.'/../_classes/States.php'); 
/*
	NOTES: 
		user_id appears to be our id for PayWhirl

	Sequence of events from actual sale:
	plan.updated - create row in paywhirl_plan table if needed with plan_id and sku
		- event->plan->sku

	subscription.created 
		- event->customer (user info set)
		- event->customer->id (same value as customer_id below)
		- event->subscription->customer_id

	invoice.created 
		- event->invoice->items[0]->sku

	charge.succeeded

	NOTE: we see the sku in the following events, but not in charge.succeeded as:
		- event->invoice->items[0]->sku
	invoice.created
	invoice.paid
	invoice.processed

	Followed by event from Stripe:
	charge.succeeded
*/

// event->type we need to be concernced with:
// customer.created
// 	event->customer
//			first_name
//			last_name
//			email
//			phone
//			address (street)
//			city
//			state
//			zip
//			country
//			user_id  (our paywhirl id)
//			id (this is also the customer_id referred elsewhere)

// card.created (I think we can ignore this one)
// 	event->card->customer_id seems to be interesting... this is the customer.created's event->customer->id

// subscription.created (maybe ignore this one too)
//	event->subscription
//			user_id  (paywhirl)
//			customer_id (should be the same one from card.created)
//			plan_id	  (not sure)

// invoice.created (point of payment)
// 	event->customer
//			user_id
//	event->invoice->subscription_id (maybe unique identifier this subscription event)
//	event->invoice->items[0]->customer_id
//	event->invoice->items[0]->sku
//	event->invoice->items[0]->invoice_id

// subscription.deleted
// 	event->customer
//			id (this is the customer_id)
//			first_name
//			last_name
//			email
//			phone
//			address (street)
//			city
//			state
//			zip
//			country
//			user_id  (paywhirl)
//	event->subscription
//			user_id
//			customer_id
//			plan_id
//			id (maybe the event->invoice->subscription_id from invoice.created)

/*
	events to really process:
	If running in sandbox, process
	invoice.created - deal same as invoice.paid.
	invoice.paid - this has the customer information, to either create a new user or to look up an existing one
	subscription.deleted - use information to reset flags as needed
	plan.created - process it for new/existing plan
	plan.updated - ditto
*/

define ('PORTAL_INITIAL_HELP_EMAIL', '<p>Hi %first_name%,<br/>
Welcome to lifestyled Listings!  Thank you for your patience while we set up your account. Below are the important pieces you’ll need to know to start working with the Lifestyled bundle.
<br/><strong>To learn more about how to get started visit</strong> our <a href="%help_center%">Lifestyled Listings Help Center</a>.

</strong>Your account login information:</strong>
Username: %user_name%
Password: %password%

<br/>You can change your password easily in the “My profile” tab of your Dashboard. Your dashboard will also provide you a step by step guide of how to fully set up your profile.
<br/><strong>To log into your Dashboard</strong> please go to www.lifestyledlistings.com/register. After signing in go to the “Dashboard” link on your navigation bar. 
<br/><strong>Your personal customer facing URL</strong> is www.lifestyledlistings.com/%nickname%
Note: This URL is for sharing with your clients and is not intended for you to log into. To access your agent tools and features log into your dashboard.
<br>Congratulations on adding our unique services to your business! 
<br/>Sincerely,</p>
<p>The Lifestyled Team</p>');

define( 'SALES_REGISTRATION', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com! Our sales team has registered you as an agent, and setting up your purchased products.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>We are striving to improve your experience and expanding our network daily!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>In case you forget, here is your login information.  You can change your password once you log in and go to the Sellers Dashboard->My Profile.  To get to the Seller's Dashboard, either click on 'Agent Dashboard' on the nav bar or the person icon in the upper right corner.
user name: %user_name%
password: %password%</p>");

class PayWhirl extends Controller {
	private $apiKey = 'pwpk_58499c8a79b4e58499c8a79bf5';
	private $secretKey = 'pwpsk_58499c8a79caa58499c8a79d4f';
	private $skuList = ['0000'=>SELLER_IS_PREMIUM_LEVEL_1,
						'0001'=>SELLER_IS_PREMIUM_LEVEL_2];

	public function __construct($logIt = 0){
		parent::__construct();
		set_time_limit(0);

		$this->payWhirl = new \PayWhirl\PayWhirl($this->apiKey,$this->secretKey);
		
		$x = $this->getClass('Options')->get((object)array('where'=>array('opt'=>'PayWhirlClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/.paywhirl.log') : null;
		$this->fromAdmin = false;
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

	public function handleEvent($event) {
		$this->log("entered handleEvent with $event->type");
		switch($event->type) {
			case 'customer.created':
				$this->createCustomer($event);
				break;

			case 'card.created': // extract event->card->customer_id ?
			case 'subscription.created': // extract event->plan_id ?
				$this->log("handleEvent - not processing $event->type");
				break;

			case 'plan.updated':
			case 'plan.created':
				return $this->processPlan($event);

			case 'invoice.created':
				if ($event->customer->gateway_type == 'StripeConnect') {// live transaction, deal with paid event only
					$this->log("handleEvent - for StripeConnect not processing $event->type");
					break;
				}
				return $this->createUser($event);

			case 'invoice.paid':
				if ($event->customer->gateway_type == 'PaywhirlTest') {// test transaction, deal with created event only
					$this->log("handleEvent - for PaywhirlTest not processing $event->type");
					break;
				}
				return $this->createUser($event);

			case 'subscription.deleted':
				return $this->refunded($event);

			default:
				$this->log("handleEvent - not processing $event->type");
				break;
		}
	}

	protected function sendMail($email, $subject, $msg, 
								$blogname = 'Welcome!', 
								$blogdescription = "You're now part of the best lifestyled listings revolution!") {
		$attachmentTriggers = ['Client Portal Notice',
									   'Successful Registration!'];
		$attachments = [];
		if (in_array($subject, $attachmentTriggers)) {
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'NewPortalAgentEmailAttachments']]);
			if (!empty($opt)) {
				$opt = json_decode($opt[0]->value);
				if (is_array($opt)) foreach($opt as $data) {
					if (file_exists(__DIR__."/../attachments/$data"))
						$attachments[] = __DIR__."/../attachments/$data";
					unset($data);
				}
				elseif (file_exists(__DIR__."/../attachments/$opt"))
					$attachments[] = __DIR__."/../attachments/$opt";
			}
		}

		$success = $this->getClass('Email')->sendMail($email, $subject, $msg, $blogname, $blogdescription, $attachments);
	  	$this->log("sendMail to $email about $subject: $msg was ".($success ? "succesful" : "failure"));
	  	return $success;
	}

	protected function constructMsgUser($first_name, $login, $password, $nickname, $flag = SELLER_IS_PREMIUM_LEVEL_1) {
		$msg = '';
		$msg .= '		<section>';
		$msg .= str_replace('%first_name%', ucwords(strtolower($first_name)),  $flag == SELLER_IS_PREMIUM_LEVEL_1 ? PORTAL_INITIAL_HELP_EMAIL : SALES_REGISTRATION);
		$msg = str_replace('%user_name%', $login, $msg);
		$msg = str_replace('%password%', $password, $msg);
		$msg = str_replace('%nickname%', $nickname, $msg);
		$msg = str_replace('%help_center%', HELP_CENTER, $msg);
		$msg .= '		</section>';
		return $msg;
	}

	protected function getUserLoginAndPassword($seller, &$login, &$password) {
		$author_id = $seller->author_id;
		$this->log("getUserLoginAndPassword - id:$seller->id, author_id: $author_id");
		$testPasswd = '';
		$magic = get_user_meta($author_id, 'user_key', true);
		$i = 0;
		if (!empty($magic)) foreach($magic as $value)
			$testPasswd .= chr( $value ^ ($i++ + ord('.')));
		else {
			if (!empty($seller->meta)) {
				foreach($seller->meta as $meta) {
					if ($meta->action == SELLER_KEY) {
						$key = json_decode($meta->key);
						$len = count($key); // strlen($meta->key);
						for($i = 0; $i < $len; $i++)
							$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
					}
				}
			}							
		}
		$login = '';
		if (!empty($testPasswd)) {
			$user = get_userdata($author_id);
			if ($user !== false )
				$login = $user->user_login;
		}
		$password = $testPasswd;
	}

	protected function createCustomer($event) {
		/*
		[customer] => stdClass Object
        (
            [first_name] => Jiro
            [last_name] => Wiseman
            [email] => Jiro.wiseman@gmail.com
            [phone] => 8312354638
            [address] => 877 cedar
            [city] => Santa Cruz
            [state] => California
            [zip] => 95060
            [country] => US
            [gateway_id] => 5755
            [user_id] => 3436
            [gateway_type] => StripeConnect
            [utm_source] => http://www.lifestyledlistingsproduct.com.usrfiles.com/html/9d8221_143c845812d6bcd7436bf8324be421e2.html
            [updated_at] => 2016-12-10 21:17:42
            [created_at] => 2016-12-10 21:17:42
            [id] => 33135
        )
        */
		$customer = $event->customer;

		$pwuser = $this->getClass('PaywhirlUser')->get((object)['where'=>['customer_id'=>$customer->id]]);
		if (!empty($pwuser)) {
			$this->log("createCustomer - customer with id:$customer->id already exists");
			return true;
		}

		$meta = new \stdClass();
		$meta->action = PW_IDENTITY;
		$meta->id = $customer;
		$metas = [$meta];

		$updated = $this->getClass('PaywhirlUser')->add(['customer_id'=>$customer->id,
											  			'first_name'=>$customer->first_name,
											  			'last_name'=>$customer->last_name,
											  			'flags'=>0,
											  			'meta'=>$metas]);
		return $updated;
	}

	protected function makePassCode() {
		$code = '';
		for($i = 0; $i < 6; $i++) {
			$what = rand(1, 1000) % 2; // 0 = number, 1 = Caps, 2 = letter
			switch($what) {
				case 1: $code .= chr(rand(0, 25) + ord('a')); break;
				case 2: $code .= chr(rand(0, 25) + ord('A')); break;
				case 0: $code .= number_format(rand(0, 9));
			}
		}
		return $code;
	}

	protected function findLoginToUse($customer) {
		$login = strtolower($customer->first_name[0]).strtolower($customer->last_name);
		$user = get_user_by('login', $login);
		if (!empty($user)) { // then make sure we have a unique login with the login
			$try = 1;
			$base = $login;
			while ( !empty($user = get_user_by('login', $login)) ) 
				$login = $base.$try++;
		}
		return $login;
	}

	protected function checkPortalName($portal) {
		$portal = strtolower($portal);
		$blogusers = get_users( array( 'search' => $portal ) );
		$havePortal = false;
		if (!empty($blogusers)) foreach($blogusers as $user) {
			if (strtolower($user->user_nicename) == $portal &&
				strtolower($user->nickname) == $portal) {
				return [$user];
			}
		}
		return $havePortal;
	}

	protected function getPortal($customer) {
		$nickname = strtolower($customer->first_name.$customer->last_name);
		$base = $nickname;
		$count = 1;
		while( !empty($users = $this->checkPortalName($nickname)) )
			$nickname = $base.$count++;

		return $nickname;
	}

	// this is called for an invoice.create/paid event only
	// so by this time, we need to create a seller with the right flags
	protected function createUser($event) {
		$this->log("entered createUser for $event->type");
		$PwUser = $this->getClass('PaywhirlUser');
		$plan = $this->getClass('PaywhirlPlan')->get((object)['where'=>['sku'=>$event->invoice->items[0]->sku]]);
		if (empty($plan)) {
			$msg = "createUser - failed to find plan for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to find plan from PayWhirl data",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
		}

		$flag = !empty($plan) && $plan[0]->flag ? $plan[0]->flag : 0;
		$this->log("createUser - flag:$flag");
		$seller = null;
		$updated = false;
		$invoice = new \stdClass();
		$invoice->action = PW_INVOICES;
		$invoice->list = [$event->invoice->items[0]];

		$pwuser = $PwUser->get((object)['where'=>['customer_id'=>$event->customer->id]]);
		if (empty($pwuser) ||
			empty($pwuser[0]->wp_user_id)) { // create new wp user, seller and PayWhirl user
			$bre = 'paywhirl_user';
			$password = strtolower($event->customer->last_name).$this->makePassCode();
			$login = $this->findLoginToUse($event->customer);
			$special = !empty($plan) && $plan[0]->flag ? ($plan[0]->flag == SELLER_IS_PREMIUM_LEVEL_1 ? CAMPAIGN_PORTAL_REGISTRATION : CAMPAIGN_LIFESTYLE_REGISTRATION) : SALES_REGISTRATION;
			$origin =  !empty($plan) && $plan[0]->flag ? ($plan[0]->flag == SELLER_IS_PREMIUM_LEVEL_1 ? ORDER_PORTAL : ORDER_AGENT_MATCH) : ORDER_IDLE;
			global $usStates;
			$state = array_key_exists($event->customer->state, $usStates) ? $usStates[$event->customer->state] : $event->customer->state;
			$out = $this->getClass('Register')->basicRegistry($event->customer->email, 
															  $event->customer->first_name, 
															  $event->customer->last_name, 
															  $password, 
															  $bre, 
															  $login, 
															  $state, 
															  '', 
															  $special,
															  0,
															  $origin);
			if ($out->status == 'OK') {
				if (empty($pwuser)) {
					$id = new \stdClass();
					$id->action = PW_IDENTITY;
					$id->id = $customer;
					$metas = [$id, $invoice];
					$updated = $PwUser->add(['wp_user_id'=>$out->data->id,
								  			'seller_id'=>$out->data->seller_id,
								  			'customer_id'=>$event->customer->id,
								  			'first_name'=>$event->customer->first_name,
								  			'last_name'=>$event->customer->last_name,
								  			'flags'=>!empty($plan) && $plan[0]->flag ? $plan[0]->flag : 0,
								  			'meta'=>$metas]);
				
					if (empty($updated)){
						$msg = "createUser - failed to create PayWhirl User for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   "Failed to create PayWhirl user from PayWhirl data",
													   $msg,
													   'Techincal Issue',
													   'Ecommerce needs attention!');
						return false;
					}
				}
				else {
					$pwuser = array_pop($pwuser);
					$metas = !empty($pwuser->meta) ? $pwuser->meta : [];
					$gotOne = false;
					foreach($metas as &$meta) {
						if ($meta->action == PW_INVOICES) {
							$gotOne = true;
							$meta->list[] = $event->invoice->items[0];
						}
						unset($meta);
						if ($gotOne)
							break;
					}
					if (!$gotOne)
						$metas[] = $invoice;
					$updated = $PwUser->set([(object)['where'=>['id'=>$pwuser->id],
													  'fields'=>['wp_user_id'=>$out->data->id,
								  								 'seller_id'=>$out->data->seller_id,
								  								 'flags'=>!empty($plan) && $plan[0]->flag ? $plan[0]->flag : 0,
								  								 'meta'=>$metas]]]);
					if (empty($updated)){
						$msg = "createUser - failed to update existing PayWhirl User customer_id:{$event->customer->id} with wp_user_id, seller_id, flags and meta, event:".print_r($event, true);
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   "Failed to update existing PayWhirl user from PayWhirl data when creating seller",
													   $msg,
													   'Techincal Issue',
													   'Ecommerce needs attention!');
					}
				}

				if (!isset($out->data->existing)) // then update with paywhirl data, for things that would be missing from this new seller (row)
					$this->getClass('Sellers')->set([(object)['where'=>['id'=>$out->data->seller_id],
															  'fields'=>['street_address'=>$event->customer->address,
															  			 'city'=>$event->customer->city,
															  			 'state'=>$state,
															  			 'country'=>$event->customer->country,
															  			 'zip'=>$event->customer->zip,
															  			 'phone'=>$event->customer->phone,
															  			 'mobile'=>$event->customer->phone]]]);
				else { // see if the existing one has the fields available from paywhirl
					$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$out->data->seller_id]]);
					if (empty($seller)) {
						$msg = "createUser - failed to find existing seller:{$out->data->seller_id} for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   "Failed to find existing seller from PayWhirl data",
													   $msg,
													   'Techincal Issue',
													   'Ecommerce needs attention!');
						return false;
					}
					$seller = array_pop($seller);
					$fields = [];
					if ( empty($seller->street_address) )
						$fields['street_address'] = $event->customer->address;
					if ( empty($seller->city) )
						$fields['city'] = $event->customer->city;
					if ( empty($seller->state) )
						$fields['state'] = $state;
					if ( empty($seller->country) )
						$fields['country'] = $event->customer->country;
					if ( empty($seller->zip) )
						$fields['zip'] = $event->customer->zip;
					if ( empty($seller->phone) )
						$fields['phone'] = $event->customer->phone;
					if ( empty($seller->mobile) )
						$fields['mobile'] = $event->customer->phone;
					if (count($fields)) {
						$this->getClass('Sellers')->set([(object)['where'=>['id'=>$out->data->seller_id],
															  	  'fields'=>$fields]]);
						$this->log("updated existing seller:$seller->id with fields:".print_r($fields, true));
					}
					unset($fields);
				}

				$seller_id = $out->data->seller_id;
				$first_name = $event->customer->first_name;
				$last_name = $event->customer->last_name;
				$this->log("createUser - ".(!isset($out->data->existing) ? 'new' : 'existing')." seller:$seller_id, $first_name $last_name, out:".print_r($out, true));
			}
			else {
				$customer_id = $event->customer->id;
				$first_name = $event->customer->first_name;
				$last_name = $event->customer->last_name;
				$email = $event->customer->email;
				$invoice_id = $event->invoice->id;
				$sku = $event->invoice->items[0]->sku;
				$msg = "createUser - failed to create WPUser for customer_id:$customer_id, $first_name $last_name, email: $email, invoice id:$invoice_id, sku:$sku, event:".print_r($event, true);
				$this->log($msg);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
											   "Failed to create WPuser from PayWhirl data",
											   $msg,
											   'Techincal Issue',
											   'Ecommerce needs attention!');
				return false;
			}

			$seller_id = $out->data->seller_id;

			if ($flag == SELLER_IS_PREMIUM_LEVEL_1) {
				$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$seller_id]]);
				if (!empty($seller)) {
					if ($seller[0]->flags & SELLER_IS_PREMIUM_LEVEL_1) {
						$id = $seller[0]->id;
						$customer_id = $event->customer->id;
						$first_name = $event->customer->first_name;
						$last_name = $event->customer->last_name;
						$email = $event->customer->email;
						$invoice_id = $event->invoice->id;
						$sku = $event->invoice->items[0]->sku;
						$msg = "createUser - seller:$id, {$seller[0]->first_name} {$seller[0]->last_name}, already has flag:$flag for portal ownership, paid for another one, apparently.  May need to refund this transaction - customer_id:$customer->id, $first_name $last_name, email:$email, invoice id:$invoice_id, sku:$sku";
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "Seller already has portal ownership",
												   $msg,
												   'Techincal Issue',
												   'Ecommerce needs attention!');
						return false;
					}
				}

				$nickname = $this->getPortal($event->customer);
				$this->log("createUser - about to call getPortalName with $nickname");
				$out = $this->getClass('SessionMgr')->getPortalName($nickname, 'paywhirl', $out->data->id, $event->customer->phone);
				if ($out->status != 'OK') {
					$id = $out->data->id;
					$customer_id = $event->customer->id;
					$first_name = $event->customer->first_name;
					$last_name = $event->customer->last_name;
					$email = $event->customer->email;
					$invoice_id = $event->invoice->id;
					$sku = $event->invoice->items[0]->sku;
					$msg = "createUser - failed to get portal:$nickname for seller:$id, for customer_id:$customer->id, $first_name $last_name, email:$email, invoice id:$invoice_id, sku:$sku, event:".print_r($event, true);
					$this->log($msg);
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "Failed get portal from PayWhirl data using portal:$nickname for WP user:".$out->data->id,
												   $msg,
												   'Techincal Issue',
												   'Ecommerce needs attention!');
				}
				else {
					$msg = "createUser - new portal:$nickname for seller:{$seller_id}, {$event->customer->first_name} {$event->customer->last_name}, email: {$event->customer->email}";
					$this->log($msg);
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "New seller with portal:$nickname from PayWhirl data",
												   $msg,
												   'PayWhirl transaction',
												   'Have paid agent!');
					// send this seller an email with the portal name ($nickname) and any helpful attachments
					$msg = $this->constructMsgUser($event->customer->first_name, $login, $password, $nickname);
					$this->sendMail($event->customer->email, 'Client Portal Notice', $msg,
									'Congratulation, here is your portal information',
									'Please visit our help center.');
					return true;

				}
			}
			else { // some other product besides portal
				$msg = "createUser - new seller:{$seller_id}, {$event->customer->first_name} {$event->customer->last_name} with flag:$flag, email: {$event->customer->email}";
				$this->log($msg);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
											   "New seller with product flag:$flag from PayWhirl data",
											   $msg,
											   'PayWhirl transaction',
											   'Have paid agent!');
			}
		}
		else { // we found an existing PayWhirl user
			$pwuser = array_pop($pwuser);
			$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$pwuser->seller_id]]);
			if (empty($seller)) {
				$msg = "createUser - failed to find existing seller for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
				$this->log($msg);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
											   "Failed to find existing seller from PayWhirl data",
											   $msg,
											   'Techincal Issue',
											   'Ecommerce needs attention!');
				return false;
			}
			$seller = array_pop($seller);
			if ($flag &&
				!($seller->flags & $flag)) {
				$updated = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
																	 'fields'=>['flags'=>($seller->flags | $flag)]]]);
				if (empty($updated)){
					$msg = "createUser - failed to update seller with flag:$flag for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
					$this->log($msg);
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "Failed to update seller flag from PayWhirl data",
												   $msg,
												   'Techincal Issue',
												   'Ecommerce needs attention!');
					return false;
				}

				$metas = [];
				$gotOne = false;
				foreach($pwuser->meta as $meta) {
					if ($meta->action == PW_INVOICES) {
						$meta->list[] = $event->invoice->items[0];
						$gotOne = true;
					}
					$metas[] = $meta;
				}
				if (!$gotOne)
					$metas[] = $invoice;

				$updated = $PwUser->set([(object)['where'=>['id'=>$pwuser->id],
												  'fields'=>['meta'=>$metas]]]);
				unset($metas);

				if (empty($updated)){
					$msg = "createUser - failed to update PayWhirl user's meta for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
					$this->log($msg);
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "Failed to update PayWhirl user's meta from PayWhirl data",
												   $msg,
												   'Techincal Issue',
												   'Ecommerce needs attention!');
				}

				if ($flag == SELLER_IS_PREMIUM_LEVEL_1) {
					$nickname = $this->getPortal($event->customer);
					$out = $this->getClass('SessionMgr')->getPortalName($nickname, 'paywhirl', $seller->author_id, $event->customer->phone);
					if ($out->status != 'OK') {
						$msg = "createUser - failed to get portal:$nickname for seller:$seller->id, customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, invoice id:{$event->invoice->id}, sku:{$event->invoice->items[0]->sku}, event:".print_r($event, true);
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   "Failed get portal from PayWhirl data using portal:$nickname for WP user:".$seller->author_id,
													   $msg,
													   'Techincal Issue',
													   'Ecommerce needs attention!');
					}
					else {
						$msg = "createUser - new portal:$nickname for existing seller:$seller->id, $seller->first_name $seller->last_name, email:$seller->email";
						$this->log($msg);
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
													   "Existing seller bought portal:$nickname from PayWhirl data",
													   $msg,
													   'PayWhirl transaction',
													   'Have paid agent!');
						// TODO: send this seller an email with the portal name ($nickname) and any helpful attachments
						$login = '';
						$password = '';
						$this->getUserLoginAndPassword($seller, $login, $password);
						$msg = $this->constructMsgUser($seller->first_name, $login, $password, $nickname);
						$this->sendMail($seller->email, 'Client Portal Notice', $msg,
										'Congratulation, here is your portal information',
										'Please visit our help center.');
					}
				}
				else { // some other product besides portal...
					$msg = "createUser - paywhirl transaction for flag:$flag for existing seller:$seller->id, $seller->first_name $seller->last_name, email:$seller->email";
					$this->log($msg);
					$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "Existing seller with product flag:$flag from PayWhirl data",
												   $msg,
												   'PayWhirl transaction',
												   'Have paid agent!');
				}
			}
			elseif ($flag &&
				    ($seller->flags & $flag) &&
				    $flag == SELLER_IS_PREMIUM_LEVEL_1) {
				$id = $seller->id;
				$customer_id = $event->customer->id;
				$first_name = $event->customer->first_name;
				$last_name = $event->customer->last_name;
				$email = $event->customer->email;
				$invoice_id = $event->invoice->id;
				$sku = $event->invoice->items[0]->sku;
				$msg = "createUser - seller:$id, {$seller->first_name} {$seller->last_name}, already has flag:$flag for portal ownership, paid for another one, apparently.  May need to refund this transaction - customer_id:$customer->id, $first_name $last_name, email:$email, invoice id:$invoice_id, sku:$sku";
				$this->log($msg);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
											   "Seller already has portal ownership",
											   $msg,
											   'Techincal Issue',
											   'Ecommerce needs attention!');
			}
			else
				$updated = true;
		}

		return $updated;
	}

	protected function getFlagFromSku($event) {
		$sku = !empty($event->plan->sku) ? explode("-", $event->plan->sku) : [];
		$flag = 0;
		if (!empty($sku)) {
			$flag = isset($this->skuList[$sku[0]]) ? $this->skuList[$sku[0]] : $flag;
			if (empty($flag)) {
				$msg = "Received $event->type, plan_id:{$event->plan->id}, updated at:{$event->plan->updated_at}, did not match with existing sku list, got sku:{$event->plan->sku}, event:".print_r($event, true);
				$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
												   "No SKU in Paywhirl Plan",
												   $msg,
												   'Techincal Issue',
												   'Ecommerce needs attention!');
			}
		}
		else {
			$msg = "Received $event->type, plan_id:{$event->plan->id}, updated at:{$event->plan->updated_at}, sku isn't there or doesn't match our format, sku:{$event->plan->sku}, event:".print_r($event, true);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
											   "Misconfigured SKU in Paywhirl Plan",
											   $msg,
											   'Techincal Issue',
											   'Ecommerce needs attention!');
		}
		return $flag;
	}

	protected function processPlan($event) {
		$pwPlan = $this->getClass('PaywhirlPlan');
		$plan = $pwPlan->get((object)['where'=>['plan_id'=>$event->plan->id]]);
		$updated = false;
		if (empty($plan)) {
			$sku = !empty($event->plan->sku) ? explode("-", $event->plan->sku) : [];
			$flag = $this->getFlagFromSku($event);
			$updated = $pwPlan->add(['plan_id'=>$event->plan->id,
								  	'sku'=>$event->plan->sku,
								  	'flag'=>$flag]);
			$plan_id = $event->plan->id;
			$sku = $event->plan->sku;
			$this->log("processPlan - adding row for $event->type, plan_id:$plan_id, sku:$sku was ".(empty($updated) ? 'failure' : 'success'));
		}
		else {
			$plan = array_pop($plan);
			if ($plan->sku != $event->plan->sku) {
				$sku = $event->plan->sku;
				$psku = $plan->sku;
				$plan_id = $event->plan->id;
				$this->log("processPlan - sku changed from $psku to sku:$sku for $event->type, plan_id:$plan_id");
				$flag = $this->getFlagFromSku($event);
				if ($flag && 
					$flag != $plan->flag) {
					$updated = $pwPlan->set([(object)['where'=>['id'=>$plan->id],
										   			  'fields'=>['flag'=>$flag]]]);
				}
				$this->log("processPlan - updating plan_id:{$event->plan->id}, sku:{$event->plan->sku} was ".(empty($updated) ? 'failure' : 'success'));
			}
		}
		return $updated;
	}

	protected function refunded($event) {
		$PwUser = $this->getClass('PaywhirlUser');
		$plan = $this->getClass('PaywhirlPlan')->get((object)['where'=>['plan_id'=>$event->subscription->plan_id]]);
		if (empty($plan)) {
			$msg = "refunded - failed to find plan for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to find plan from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
			return false;
		}

		$plan = array_pop($plan);
		if (empty($plan->flag)) {
			$msg = "refunded - plan has an empty flag for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Plan has an empty flag from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
			return false;
		}

		$flag = $plan->flag;
		$seller = null;
		$updated = false;
		$pwuser = $PwUser->get((object)['where'=>['customer_id'=>$event->customer->id]]);
		if (empty($pwuser)) {
			$msg = "refunded - failed to find PayWhirl user from customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to find existing PayWhirl user from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
			return false;
		}
		$pwuser = array_pop($pwuser);

		$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$pwuser->seller_id]]);
		if (empty($seller)) {
			$msg = "refunded - failed to find seller from PayWhirl user:$pwuser->id, customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to find existing seller from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
			return false;
		}

		$seller = array_pop($seller);
		if ( !($seller->flag & $flag)) {
			$msg = "refunded - Seller:$seller->id doesn't have this flag:$flag, from customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Seller doesn't have flag to unset from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
			return false;
		}

		$newFlags = $seller->flags & ~$flag;
		$updated = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
															 'fields'=>['flags'=>$newFlags]]]);
		if (empty($updated)){
			$msg = "refunded - failed to update seller:$seller->id to unset flag:$flag, for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to update seller's flag from PayWhirl data",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
		}

		if ($flag == SELLER_IS_PREMIUM_LEVEL_1) {
			$status = wp_update_user( array( 'ID' => $seller->author_id, 'nickname' => 'unknown' ) );
			$status = wp_update_user( array( 'ID' => $seller->author_id, 'user_nicename' => 'unknown' ) );
		}

		$refundMeta = null;
		$metas = [];
		foreach($pwuser->meta as $meta)
			if ($meta->action == PW_REFUNDS)
				$refundMeta = $meta;
			else
				$metas[] = $meta;

		if (!$refundMeta) {
			$refundMeta = new \stdClass();
			$refundMeta->action = PW_REFUNDS;
			$refundMeta->list = [$event->subscription];
		}
		else
			$refundMeta->list[] = $event->subscription;

		$metas[] = $refundMeta;
		$updated = $PwUser->set([(object)['where'=>['id'=>$pwuser->id],
										  'fields'=>['meta'=>$metas]]]);
		if (empty($updated)){
			$msg = "refunded - failed to update PayWhirl user's meta for customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}, event:".print_r($event, true);
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Failed to update PayWhirl user's meta from PayWhirl data during refund",
										   $msg,
										   'Techincal Issue',
										   'Ecommerce needs attention!');
		}
		else {
			$msg = "refunded - seller:$pwuser->seller_id, customer_id:{$event->customer->id}, {$event->customer->first_name} {$event->customer->last_name}, email:{$event->customer->email}, plan_id:{$event->subscription->plan_id}";
			$this->log($msg);
			$this->getClass('Email')->sendMail(SUPPORT_EMAIL,
										   "Processed PayWhirl refund for flag:$flag",
										   $msg,
										   'Product refunded',
										   'Oops product refunded!');
		}
		return $updated;
	}
}