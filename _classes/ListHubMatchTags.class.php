<?php
namespace AH;
require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');

class ListHubMatchTags extends Controller {
/*
private $tagsSet = array (
		'tags1' => array(
			array('find'=>array('2 car garage','2car garage','3 car garage','4 car garage','three car garage','two car garage','four car garage','2-car garage','3-car garage','4-car garage','three-car garage','two-car garage','four-car garage','2+ car garage','3+ car garage','4+ car garage'), 'tags'=>array('2-4 car garage')),
			array('find'=>array('5 car garage', '6 car garage', '7 car garage', '8 car garage','9 car garage','10 car garage','5-car garage','6-car garage','7-car garage','8-car garage','9-car garage','10-car garage','five car garage','five-car garage'), 'tags'=>array('5+ car garage'), 'override'=>array('1.5 car garage','2.5 car garage','3.5 car garage','1.5-car garage','2.5-car garage','3.5-car garage')),
			array('find'=>array('mountain bike','mountain biking','adventurous','hunting','hike','hiking','bike','biking','snowshoe','cross country ski','skiing','fishing','outdoor recreation','snowmobile','national park','state park','offroad','off-road','four wheel drive','4wd','federal land','kayaking','rafting','rock climbing','camping'),'tags'=>array('adventurous')),
			array('find'=>array('asian style','asian influenced architecture','asian influenced design'), 'tags'=>array('asian style')),
			array('find'=>array('hollywood actress','hollywood actor','hollywood producer','movie star','celebrity owned'), 'tags'=>array('celebrity')),
			array('find'=>array('classic decor','classical decor','classic style interior','classical style interior'), 'tags'=>array('classical style')),
			array('find'=>array('country style','country architecture','country decor'), 'tags'=>array('country style')),
			array('find'=>array('countryside'), 'tags'=>array('countryside')),
			array('find'=>array('craftsman'), 'tags'=>array('craftsman')),
			array('find'=>array('doorman','guard gated entrance','security guard','watchman','24 hour security','24-hour security','24 hr door staff'), 'tags'=>array('doorman'))
		),
	 	'tags2' => array(
			array('find'=>array('drive to casinos','drive to the casinos','driving distance to casinos','driving distance to several casino','driving distance to the casino'), 'tags'=>array('drive to casino')),
			array('find'=>array('drive to nearby lakes','drive to the lake','driving distance to the lake'), 'tags'=>array('drive to lake')),
			array('find'=>array('drive to major city','drive to san francisco','drive to los angeles','drive to sacramento','drive to san jose','drive to san diego','drive to fresno','near fresno','near san francisco','near san diego','near los angeles','near sacramento'), 'tags'=>array('drive to major city')),
			array('find'=>array('drive to ski slopes','drive to ski resorts','drive to skiing','drive to tahoe','driving distance to skiing','driving distance to ski resports','driving distance to tahoe','lake tahoe','mammoth mountain','near mammoth','mammoth resort'), 'tags'=>array('drive to ski')),
			array('find'=>array('driving distance to napa','drive to napa','driving distance to wine tasting','drive to winery','drive to wineries','driving distance to wineries'), 'tags'=>array('drive to winery')),
			array('find'=>array('solar power','solar panels','eco-home','eco home','green home','green-home','net zero home','net-zero home','zero energy home','zero-energy','low energy footprint','green technology','eco-friendly','eco friendly','evacuated tube solar'), 'tags'=>array('eco-friendly')),
			array('find'=>array('horse property','bring your horses','horse trails','stables','paddocks','roundpen','round pen','indoor arena','covered arena','equestrian property','equestrian','horse riding','horseback riding','horse back riding','horse-back riding'), 'tags'=>array('horse property')),
			array('find'=>array('estate property','estate home'), 'tags'=>array('estate')),
			array('find'=>array('famous architect','featured in','iconic family','renowned architect','celebrity architect','iconic architect'), 'tags'=>array('famous')),
			array('find'=>array('fast pace'), 'tags'=>array('fast pace')),
			array('find'=>array('fenced property','perimeter fence','completely fenced'), 'tags'=>array('fenced')),
			array('find'=>array('fireplace’,’fire place’), 'tags'=>array('fireplace')),
			array('find'=>array('gated community','private community'), 'tags'=>array('gated community')),
			array('find'=>array('gated entry','gated driveway','gates','gated property','gated estate','gated home','private gate'), 'tags'=>array('gated property'))
		),
	 	'tags3' => array(
			array('find'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway'), 'tags'=>array('golf front','golf')),
			array('find'=>array('state of the art appliances','top of the line appliances',"chef's kitchen",'gourmet kitchen','professional kitchen','open kitchen','top quality appliances','remodeled kitchen','dream kitchen','viking appliances','chefs kitchen','gourmet cook\'s delightful kitchen','highend appliances','subzero fridge'), 'tags'=>array('gourmet kitchen')),
			array('find'=>array('greenbelt view','pastoral backyard','lots of beautiful trees','wooded acres','lush landscaping'), 'tags'=>array('greenbelt view')),
			array('find'=>array('heliport','helipad','helicoptor pad','heli-pad'), 'tags'=>array('heli-pad')),
			array('find'=>array('high ceilings','tall ceilings','vaulted ceiling','vaulted beamed ceiling','cathedral ceilings','ceilings are over ten feet','soaring ceilings','ft ceiling','ft. ceiling',' ceiling'), 'tags'=>array('high ceilings')),
			array('find'=>array('historical property','historic estate','historical home','years of history','historic registry','historical structure'), 'tags'=>array('historical')),
			array('find'=>array('bar','bar-wet','wet bar','wetbar'), 'tags'=>array('home bar')),
			array('find'=>array('home theater','theater room','private theater','projection screening room','home theatre'), 'tags'=>array('home theater')),
			array('find'=>array('hot tub','spa'), 'tags'=>array('hot tub')),
			array('find'=>array('island style','polynesian style','island architecture','tropical architecture','tropical style'), 'tags'=>array('island architecture')),
			array('find'=>array('priviate island'), 'tags'=>array('island')),
			array('find'=>array('lake view','view of lake','view of the lake','overlooks compass lake','freshwater lake','premier lake','lake property','lake with docks'), 'tags'=>array('lake view')),
			array('find'=>array('marble floors','marble flooring','marble counter'), 'tags'=>array('marble interiors')),
			array('find'=>array('spanish villa','mediterranean villa','spanish home','spanish style home','tuscan home','tuscan style home','tuscan-style home','mediterranean style','spanish estate','mediterranean estate'), 'tags'=>array('mediterranean')),
		),
	 	'tags4' => array(
			array('find'=>array('modern interior','steel & glass','modern home','modern architecture','modern style and design'), 'tags'=>array('modern')),
			array('find'=>array('mountain view','view of mountains','views of mt.','magnificent mt','mountain views'), 'tags'=>array('mountain view')),
			array('find'=>array('museum'), 'tags'=>array('museum')),
			array('find'=>array('newly constructed','never lived in','newly built'), 'tags'=>array('new build')),
			array('find'=>array('no visible neighbors','no neighbors','no nearby neighbors'), 'tags'=>array('no visible neighbor','privacy')),
			array('find'=>array('ocean front','on the beach','oceanfront'), 'tags'=>array('ocean front'), 'override'=>array('why buy ocean front','ocean front path')),
			array('find'=>array('open floor plan','open-floor plan','open plan living','open floorplan','open concept'), 'tags'=>array('open floor plan')),
			array('find'=>array('bbq','barbecue','courtyard',"entertainer's yard",'outdoor space','patio','relaxing porches','custom deck','huge deck for entertaining','large covered patio','porches galore','large stone patio','incredible decks','outdoor living','multi-level back deck','out door kitchen','indoor/outdoor entertaining','outdoor entertaining','outdoor dining area','indoor-outdoor living','indoor/outdoor living','lanai'), 'tags'=>array('outdoor living space')),
			array('find'=>array('oversize closet','walk-in closet','walk in closet','closets are massive','his and hers closets','his/hers closets'), 'tags'=>array('oversize closet')),
			array('find'=>array('floor to ceiling windows','large windows','tall windows','walls of windows','stories of glass','huge windows','floor-to-ceiling windows','floor-to-ceiling glass','walls of glass','expansive windows'), 'tags'=>array('oversize windows')),
			array('find'=>array(' pool'), 'tags'=>array('pool')),
			array('find'=>array('privacy','private setting','far from neighbors'), 'tags'=>array('privacy')),
			array('find'=>array('private airstrip','airstrip','private hangar'), 'tags'=>array('private airstrip'))
		),
	 	'tags5' => array(
			array('find'=>array("' dock",'boathouse','boat dock','boat lift','private dock','deeded slip'), 'tags'=>array('private dock','boating','fishing')),
			array('find'=>array('ranch facilities','ranch property'), 'tags'=>array('ranch')),
			array('find'=>array('river front','river frontage'), 'tags'=>array('river front','river')),
			array('find'=>array('close to river','nearby river'), 'tags'=>array('river','fishing')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river view','river','fishing')),
			array('find'=>array('rooftop','roof-top terrace'), 'tags'=>array('rooftop terrace')),
			array('find'=>array('rural','wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('sauna','steam room'), 'tags'=>array('sauna')),
			array('find'=>array('secluded','far from neighbors','no nearby neighbors','dead-end road'), 'tags'=>array('secluded')),
			array('find'=>array('guest cottage','guest house','in-law unit','separate unit','in-law suite','separate rentals','mother-in-laws','detached cottage','in-law quarters','in law unit','in-law unit','guest studio','guesthouse','separate guest quarters','caretaker quarters','caretaker apt'), 'tags'=>array('separate guest space')),
			array('find'=>array('ski front','ski in/ski out','ski-in/ski-out','ski condo'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('skyline view','city views','views of the city','view of the city','view of the skyline','views of the skyline','incredible views of little rock'), 'tags'=>array('skyline view')),
			array('find'=>array('small town'), 'tags'=>array('small town')),
			array('find'=>array('smart home','state of the art technology'), 'tags'=>array('smart home')),
			array('find'=>array('stables','mulitple horses','stall barn','horse shelter','horse stall'), 'tags'=>array('stables','horse property')),
			array('find'=>array('lake front','lakefront'), 'tags'=>array('lake front'))
		),
	 	'tags6' => array(
			array('find'=>array('tennis club','tennis center'), 'tags'=>array('tennis club')),
			array('find'=>array('tennis court','tennis','tennis courts'), 'tags'=>array('tennis court')),
			array('find'=>array('updated interior','modern appliances','renovated'), 'tags'=>array('updated interior')),
			array('find'=>array('valet'), 'tags'=>array('valet')),
			array('find'=>array('valley view','views of the valley','view of the valley','scenic valley','valley wide views'), 'tags'=>array('Valley View')),
			array('find'=>array('commercial vineyard','vineyard property','established vineyard'), 'tags'=>array('vineyard')),
			array('find'=>array('wine cellar','wine storage','wine cave','wine room','tasting room'), 'tags'=>array('wine cellar')),
			array('find'=>array('victorian home','victorian style','classic victorian','typical victorian'), 'tags'=>array('victorian')),
			array('find'=>array('colonial design','colonial style','classic colonial','colonial architecture'), 'tags'=>array('colonial')),
			array('find'=>array('condo','condo/townhouse','condominium','townhome','townhouse'), 'tags'=>array('condo')),
		)
	); // end tags array

	private $unTagsSet = array(
		'tags1' => array(
			array('find'=>array('aspen'), 'tags'=>array('alpine','skiing','winter <40','mountain view'), 'override'=>array('ski resort','ski slopes','ski mountains','ski access')),
			array('find'=>array('country home'), 'tags'=>array('countryside')),
			array('find'=>array('jets ski','waterskiing','skiing equipment','own private dock-boating, skiing','own private dock - boating, skiing',"jet ski's",'water-skiing','jet-skiing'),'tags'=>array('skiing','winter >40','alpine','drive to ski')),
		),
	 	'tags2' => array(
			array('find'=>array(' gates'), 'tags'=>array('gated property')),
			array('find'=>array('poolhouse'), 'tags'=>array('separate guest space')),
			array('find'=>array('whirlpool','whirl pool'), 'tags'=>array('pool')),
			array('find'=>array('beach access'), 'tags'=>array('ocean front')),
		),
	 	'tags3' => array(
			array('find'=>array('golf course','country club'), 'tags'=>array('golf front'), 'override'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway')),
			array('find'=>array('media room'), 'tags'=>array('home theater')),
			array('find'=>array('hunter'), 'tags'=>array('hunting')),
		),
	 	'tags4' => array(
			array('find'=>array('contemporary','executive home'), 'tags'=>array('modern')),
			array('find'=>array('gulf to swim','stunning ocean'), 'tags'=>array('ocean front')),
			array('find'=>array('room for entertaining', 'great room','largest floor plans','great for entertaining'), 'tags'=>array('open floor plan')),
			array('find'=>array('roving security'), 'tags'=>array('privacy')),
		),
	 	'tags5' => array(
			array('find'=>array('harbor'), 'tags'=>array('private dock')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river front')),
			array('find'=>array('wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('cul de sac','end of the road'), 'tags'=>array('secluded')),
			array('find'=>array('ski resort development','on the slopes'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('on the slopes'), 'tags'=>array('Ski Front Home'), 'override'=>array('skiing')),
			array('find'=>array('equestrian'), 'tags'=>array('stables'), 'override'=>array('horse facility','horse facilities'))
		)
	);
	*/
	/* commented out June 3, 2016
	private $tagsSet = array (
		'tags1' => array(
			array('find'=>array('2 car garage','2car garage','3 car garage','4 car garage','three car garage','two car garage','four car garage','2-car garage','3-car garage','4-car garage','three-car garage','two-car garage','four-car garage','2+ car garage','3+ car garage','4+ car garage'), 'tags'=>array('2-4 car garage')),
			array('find'=>array('5 car garage', '6 car garage', '7 car garage', '8 car garage','9 car garage','10 car garage','5-car garage','6-car garage','7-car garage','8-car garage','9-car garage','10-car garage','five car garage','five-car garage'), 'tags'=>array('5+ car garage'), 'override'=>array('1.5 car garage','2.5 car garage','3.5 car garage','1.5-car garage','2.5-car garage','3.5-car garage')),
			array('find'=>array('mountain bike','mountain biking','adventurous','hunting','hike','hiking','bike','biking','snowshoe','cross country ski','skiing','fishing','outdoor recreation','snowmobile','national park','state park','offroad','off-road','four wheel drive','4wd','federal land','kayaking','rafting','rock climbing','camping'),'tags'=>array('adventurous')),
			array('find'=>array('asian style','asian influenced architecture','asian influenced design'), 'tags'=>array('asian style')),
			array('find'=>array('hollywood actress','hollywood actor','hollywood producer','movie star','celebrity owned'), 'tags'=>array('celebrity')),
			array('find'=>array('classic decor','classical decor','classic style interior','classical style interior'), 'tags'=>array('classical style')),
			array('find'=>array('country style','country architecture','country decor'), 'tags'=>array('country style')),
			array('find'=>array('countryside'), 'tags'=>array('countryside')),
			array('find'=>array('craftsman'), 'tags'=>array('craftsman')),
			array('find'=>array('doorman','guard gated entrance','security guard','watchman','24 hour security','24-hour security','24 hr door staff'), 'tags'=>array('doorman'))
		),
	 	'tags2' => array(
			array('find'=>array('drive to casinos','drive to the casinos','driving distance to casinos','driving distance to several casino','driving distance to the casino'), 'tags'=>array('drive to casino')),
			array('find'=>array('drive to nearby lakes','drive to the lake','driving distance to the lake'), 'tags'=>array('drive to lake')),
			array('find'=>array('drive to major city','drive to san francisco','drive to los angeles','drive to sacramento','drive to san jose','drive to san diego','drive to fresno','near fresno','near san francisco','near san diego','near los angeles','near sacramento'), 'tags'=>array('drive to major city')),
			array('find'=>array('drive to ski slopes','drive to ski resorts','drive to skiing','drive to tahoe','driving distance to skiing','driving distance to ski resports','driving distance to tahoe','lake tahoe','mammoth mountain','near mammoth','mammoth resort'), 'tags'=>array('drive to ski')),
			array('find'=>array('driving distance to napa','drive to napa','driving distance to wine tasting','drive to winery','drive to wineries','driving distance to wineries'), 'tags'=>array('drive to winery')),
			array('find'=>array('solar power','solar panels','eco-home','eco home','green home','green-home','net zero home','net-zero home','zero energy home','zero-energy','low energy footprint','green technology','eco-friendly','eco friendly','evacuated tube solar'), 'tags'=>array('eco-friendly')),
			array('find'=>array('horse property','bring your horses','stables','paddocks','roundpen','round pen','indoor arena','covered arena','equestrian property'), 'tags'=>array('horse property','ranch')),
			array('find'=>array('estate property','estate home'), 'tags'=>array('estate')),
			array('find'=>array('famous architect','featured in','iconic family','renowned architect','celebrity architect','iconic architect'), 'tags'=>array('famous')),
			array('find'=>array('fast pace'), 'tags'=>array('fast pace')),
			array('find'=>array('fenced property','perimeter fence','completely fenced'), 'tags'=>array('fenced')),
			array('find'=>array('fireplace'), 'tags'=>array('fireplace')),
			array('find'=>array('gated community','private community'), 'tags'=>array('gated community')),
			array('find'=>array('gated entry','gated driveway','gates','gated property','gated estate','gated home','private gate'), 'tags'=>array('gated property'))
		),
	 	'tags3' => array(
			array('find'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway'), 'tags'=>array('golf front','golf')),
			array('find'=>array('state of the art appliances','top of the line appliances',"chef's kitchen",'gourmet kitchen','professional kitchen','open kitchen','top quality appliances','remodeled kitchen','dream kitchen','viking appliances','chefs kitchen','gourmet cook\'s delightful kitchen','highend appliances','subzero fridge'), 'tags'=>array('gourmet kitchen')),
			array('find'=>array('greenbelt view','pastoral backyard','lots of beautiful trees','wooded acres','lush landscaping'), 'tags'=>array('greenbelt view')),
			array('find'=>array('heliport','helipad','helicoptor pad','heli-pad'), 'tags'=>array('heli-pad')),
			array('find'=>array('high ceilings','tall ceilings','vaulted ceiling','vaulted beamed ceiling','cathedral ceilings','ceilings are over ten feet','soaring ceilings','ft ceiling','ft. ceiling',' ceiling'), 'tags'=>array('high ceilings')),
			array('find'=>array('historical property','historic estate','historical home','years of history','historic registry','historical structure'), 'tags'=>array('historical')),
			array('find'=>array('bar','bar-wet','wet bar','wetbar'), 'tags'=>array('home bar')),
			array('find'=>array('home theater','theater room','private theater','projection screening room','home theatre'), 'tags'=>array('home theater')),
			array('find'=>array('hot tub','spa'), 'tags'=>array('hot tub')),
			array('find'=>array('island style','polynesian style','island architecture','tropical architecture','tropical style'), 'tags'=>array('island architecture')),
			array('find'=>array('priviate island'), 'tags'=>array('island')),
			array('find'=>array('lake view','view of lake','views of lake','view of the lake','views of the lake','overlooks compass lake','freshwater lake','premier lake','lake property','lake with docks'), 'tags'=>array('lake view')),
			array('find'=>array('marble floors','marble flooring','marble counter'), 'tags'=>array('marble interiors')),
			array('find'=>array('spanish villa','mediterranean villa','spanish home','spanish style home','tuscan home','tuscan style home','tuscan-style home','mediterranean style','spanish estate','mediterranean estate'), 'tags'=>array('mediterranean')),
		),
	 	'tags4' => array(
			array('find'=>array('modern interior','steel & glass','modern home','modern architecture','modern style and design'), 'tags'=>array('modern')),
			array('find'=>array('mountain view','view of mountains','views of mt.','magnificent mt','mountain views'), 'tags'=>array('mountain view')),
			array('find'=>array('museum'), 'tags'=>array('museum')),
			array('find'=>array('newly constructed','never lived in','newly built'), 'tags'=>array('new build')),
			array('find'=>array('no visible neighbors','no neighbors','no nearby neighbors'), 'tags'=>array('no visible neighbor','privacy')),
			array('find'=>array('ocean front','on the beach','oceanfront'), 'tags'=>array('ocean front'), 'override'=>array('why buy ocean front','ocean front path')),
			array('find'=>array('open floor plan','open-floor plan','open plan living','open floorplan','open concept'), 'tags'=>array('open floor plan')),
			array('find'=>array('bbq','barbecue','courtyard',"entertainer's yard",'outdoor space','patio','relaxing porches','custom deck','huge deck for entertaining','large covered patio','porches galore','large stone patio','incredible decks','outdoor living','multi-level back deck','out door kitchen','indoor/outdoor entertaining','outdoor entertaining','outdoor dining area','indoor-outdoor living','indoor/outdoor living','lanai'), 'tags'=>array('outdoor living space')),
			array('find'=>array('oversize closet','walk-in closet','walk in closet','closets are massive','his and hers closets','his/hers closets'), 'tags'=>array('oversize closet')),
			array('find'=>array('floor to ceiling windows','large windows','tall windows','walls of windows','stories of glass','huge windows','floor-to-ceiling windows','floor-to-ceiling glass','walls of glass','expansive windows'), 'tags'=>array('oversize windows')),
			array('find'=>array(' pool'), 'tags'=>array('pool')),
			array('find'=>array('privacy','private setting','far from neighbors'), 'tags'=>array('privacy')),
			array('find'=>array('private airstrip','airstrip','private hangar'), 'tags'=>array('private airstrip'))
		),
	 	'tags5' => array(
			array('find'=>array("' dock",'boathouse','boat dock','boat lift','private dock','deeded slip'), 'tags'=>array('private dock','fishing')),
			array('find'=>array('ranch facilities','ranch property','dude ranch','working ranch','private ranch property','livestock'), 'tags'=>array('ranch')),
			array('find'=>array('river front','river frontage'), 'tags'=>array('river front','river')),
			array('find'=>array('close to river','nearby river'), 'tags'=>array('river','fishing')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river view','river')),
			array('find'=>array('rooftop','roof-top terrace'), 'tags'=>array('rooftop terrace')),
			array('find'=>array('rural','wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('sauna','steam room'), 'tags'=>array('sauna')),
			array('find'=>array('secluded','far from neighbors','no nearby neighbors','dead-end road'), 'tags'=>array('secluded')),
			array('find'=>array('guest cottage','guest house','in-law unit','separate unit','in-law suite','separate rentals','mother-in-laws','detached cottage','in-law quarters','in law unit','in-law unit','guest studio','guesthouse','separate guest quarters','caretaker quarters','caretaker apt'), 'tags'=>array('separate guest space')),
			array('find'=>array('ski front','ski in/ski out','ski-in/ski-out','ski condo'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('skyline view','city views','views of the city','view of the city','view of the skyline','views of the skyline','incredible views of little rock'), 'tags'=>array('skyline view')),
			array('find'=>array('small town'), 'tags'=>array('small town')),
			array('find'=>array('smart home','state of the art technology'), 'tags'=>array('smart home')),
			array('find'=>array('stables','mulitple horses','stall barn','horse shelter','horse stall','horse facilities'), 'tags'=>array('stables','horse property')),
			array('find'=>array('lake front property','lakefront property','lake front home','lakefront home','lake front estate','lakefront estate'), 'tags'=>array('lake front'))
		),
	 	'tags6' => array(
			array('find'=>array('tennis club','tennis center'), 'tags'=>array('tennis club')),
			array('find'=>array('tennis court','tennis-court'), 'tags'=>array('tennis court')),
			array('find'=>array('updated interior','modern appliances','renovated'), 'tags'=>array('updated interior')),
			array('find'=>array('valet'), 'tags'=>array('valet')),
			array('find'=>array('valley view','views of the valley','view of the valley','scenic valley','valley wide views'), 'tags'=>array('Valley View')),
			// array('find'=>array('commercial vineyard','vineyard property','established vineyard'), 'tags'=>array('vineyard')),
			array('find'=>array('vineyard property','private vineyard','established vineyard','producing vineyard','potential vineyard','vineyard potential','for vineyard'), 'tags'=>array('vineyard'), 'override'=>array('nearby vineyard','close to vineyards')),			
			array('find'=>array('wine cellar','wine storage','wine cave','wine room','tasting room'), 'tags'=>array('wine cellar')),
			array('find'=>array('victorian home','victorian style','classic victorian','typical victorian'), 'tags'=>array('victorian')),
			array('find'=>array('colonial design','colonial style','classic colonial','colonial architecture'), 'tags'=>array('colonial')),
			array('find'=>array('condo','condo/townhouse','condominium','townhome','townhouse','towne home','row-style home'), 'tags'=>array('condo')),
		)
	); // end tags array

	private $unTagsSet = array(
		'tags1' => array(
			array('find'=>array('aspen'), 'tags'=>array('alpine','skiing','winter <40','mountain view'), 'override'=>array('ski resort','ski slopes','ski mountains','ski access')),
			array('find'=>array('country home'), 'tags'=>array('countryside')),
			array('find'=>array('jets ski','waterskiing','skiing equipment','own private dock-boating, skiing','own private dock - boating, skiing',"jet ski's",'water-skiing','jet-skiing'),'tags'=>array('skiing','winter >40','alpine','drive to ski')),
			array('find'=>array('ladera ranch property','lang ranch property','Nellie Gail Ranch facilities'), 'tags'=>array('ranch'))
		),
	 	'tags2' => array(
			array('find'=>array(' gates'), 'tags'=>array('gated property')),
			array('find'=>array('poolhouse'), 'tags'=>array('separate guest space')),
			array('find'=>array('whirlpool','whirl pool'), 'tags'=>array('pool')),
			array('find'=>array('almost ocean front','ocean front bluff top','beach access','walks on the beach','walk on the beach'), 'tags'=>array('ocean front'), 'override'=>array('ocean front','ocean-front','oceanfront'))
		),
	 	'tags3' => array(
			array('find'=>array('golf course','country club'), 'tags'=>array('golf front'), 'override'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway')),
			array('find'=>array('media room'), 'tags'=>array('home theater')),
			array('find'=>array('hunter'), 'tags'=>array('hunting')),
		),
	 	'tags4' => array(
			array('find'=>array('contemporary','executive home'), 'tags'=>array('modern')),
			array('find'=>array('gulf to swim','stunning ocean'), 'tags'=>array('ocean front')),
			array('find'=>array('room for entertaining', 'great room','largest floor plans','great for entertaining'), 'tags'=>array('open floor plan')),
			array('find'=>array('roving security'), 'tags'=>array('privacy')),
		),
	 	'tags5' => array(
			array('find'=>array('harbor'), 'tags'=>array('private dock')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river front')),
			array('find'=>array('wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('cul de sac','end of the road'), 'tags'=>array('secluded')),
			array('find'=>array('ski resort development','on the slopes'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('on the slopes'), 'tags'=>array('Ski Front Home'), 'override'=>array('skiing')),
			array('find'=>array('equestrian'), 'tags'=>array('stables'), 'override'=>array('horse facility','horse facilities'))
		)
	);
	*/

	private $tagsSet = array (
		'tags1' => array(
			array('find'=>array('2 car garage','2car garage','3 car garage','4 car garage','three car garage','two car garage','four car garage','2-car garage','3-car garage','4-car garage','three-car garage','two-car garage','four-car garage','2+ car garage','3+ car garage','4+ car garage'), 'tags'=>array('2-4 car garage')),
			array('find'=>array('5 car garage', '6 car garage', '7 car garage', '8 car garage','9 car garage','10 car garage','5-car garage','6-car garage','7-car garage','8-car garage','9-car garage','10-car garage','five car garage','five-car garage'), 'tags'=>array('5+ car garage'), 'override'=>array('1.5 car garage','2.5 car garage','3.5 car garage','1.5-car garage','2.5-car garage','3.5-car garage')),
			array('find'=>array('mountain bike','mountain biking','adventurous','hunting','hike','hiking','bike','biking','snowshoe','cross country ski','skiing','fishing','outdoor recreation','snowmobile','national park','state park','offroad','off-road','four wheel drive','4wd','federal land','kayaking','rafting','rock climbing','camping'),'tags'=>array('adventurous')),
			array('find'=>array('asian style','asian influenced architecture','asian influenced design'), 'tags'=>array('asian style')),
			array('find'=>array('hollywood actress','hollywood actor','hollywood producer','movie star','celebrity owned'), 'tags'=>array('celebrity')),
			array('find'=>array('classic decor','classical decor','classic style interior','classical style interior'), 'tags'=>array('classical style')),
			array('find'=>array('country style','country architecture','country decor'), 'tags'=>array('country style')),
			array('find'=>array('countryside'), 'tags'=>array('countryside')),
			array('find'=>array('craftsman'), 'tags'=>array('craftsman')),
			array('find'=>array('doorman','guard gated entrance','security guard','watchman','24 hour security','24-hour security','24 hr door staff'), 'tags'=>array('doorman')),
			array('find'=>array('heart of downtown','middle of downtown','in downtown','downtown location','downtown condo','heart of town'), 'tags'=>array('downtown'))
		),
	 	'tags2' => array(
			array('find'=>array('fireplace'), 'tags'=>array('fireplace')),
			array('find'=>array('drive to casinos','drive to the casinos','driving distance to casinos','driving distance to several casino','driving distance to the casino'), 'tags'=>array('drive to casino')),
			array('find'=>array('drive to nearby lakes','drive to the lake','driving distance to the lake'), 'tags'=>array('drive to lake')),
			array('find'=>array('drive to major city','drive to san francisco','drive to los angeles','drive to sacramento','drive to san jose','drive to san diego','drive to fresno','near fresno','near san francisco','near san diego','near los angeles','near sacramento'), 'tags'=>array('drive to major city')),
			array('find'=>array('drive to ski slopes','drive to ski resorts','drive to skiing','drive to tahoe','driving distance to skiing','driving distance to ski resports','driving distance to tahoe','lake tahoe','mammoth mountain','near mammoth','mammoth resort'), 'tags'=>array('drive to ski')),
			array('find'=>array('driving distance to napa','drive to napa','driving distance to wine tasting','drive to winery','drive to wineries','driving distance to wineries'), 'tags'=>array('drive to winery')),
			array('find'=>array('solar power','solar panels','eco-home','eco home','green home','green-home','net zero home','net-zero home','zero energy home','zero-energy','low energy footprint','green technology','eco-friendly','eco friendly','evacuated tube solar'), 'tags'=>array('eco-friendly')),
			array('find'=>array('horse property','bring your horses','stables','paddocks','roundpen','round pen','indoor arena','covered arena','equestrian property','for horses','equine'), 'tags'=>array('horse property','ranch')),
			array('find'=>array('estate property','estate home'), 'tags'=>array('estate')),
			array('find'=>array('famous architect','featured in','iconic family','renowned architect','celebrity architect','iconic architect'), 'tags'=>array('famous')),
			array('find'=>array('fast pace'), 'tags'=>array('fast pace')),
			array('find'=>array('fenced property','perimeter fence','completely fenced'), 'tags'=>array('fenced')),
			array('find'=>array('o community','private community'), 'tags'=>array('gated community')),
			array('find'=>array('gated entry','gated driveway','gates','gated property','gated estate','gated home','private gate'), 'tags'=>array('gated property'))
		),
	 	'tags3' => array(
			array('find'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway'), 'tags'=>array('golf front','golf')),
			array('find'=>array('state of the art appliances','top of the line appliances',"chef's kitchen",'gourmet kitchen','professional kitchen','open kitchen','top quality appliances','remodeled kitchen','dream kitchen','viking appliances','chefs kitchen','gourmet cook\'s delightful kitchen','highend appliances','subzero fridge','designer kitchen'), 'tags'=>array('gourmet kitchen')),
			array('find'=>array('greenbelt view','pastoral backyard','lots of beautiful trees','wooded acres','lush landscaping'), 'tags'=>array('greenbelt view')),
			array('find'=>array('heliport','helipad','helicoptor pad','heli-pad'), 'tags'=>array('heli-pad')),
			array('find'=>array('high ceilings','tall ceilings','vaulted ceiling','vaulted beamed ceiling','cathedral ceilings','ceilings are over ten feet','soaring ceilings','ft ceiling','ft. ceiling',' ceiling'), 'tags'=>array('high ceilings')),
			array('find'=>array('historical property','historic estate','historical home','years of history','historic registry','historical structure'), 'tags'=>array('historical')),
			array('find'=>array('bar','bar-wet','wet bar','wetbar'), 'tags'=>array('home bar')),
			array('find'=>array('home theater','theater room','private theater','projection screening room','home theatre'), 'tags'=>array('home theater')),
			array('find'=>array('hot tub','spa'), 'tags'=>array('hot tub')),
			array('find'=>array('island style','polynesian style','island architecture','tropical architecture','tropical style'), 'tags'=>array('island architecture')),
			array('find'=>array('private island'), 'tags'=>array('island')),
			array('find'=>array('lake view','view of lake','views of lake','view of the lake','views of the lake','overlooks compass lake','freshwater lake','premier lake','lake property','lake with docks'), 'tags'=>array('lake view')),
			array('find'=>array('marble floors','marble flooring','marble counter'), 'tags'=>array('marble interiors')),
			array('find'=>array('spanish villa','mediterranean villa','spanish home','spanish style home','tuscan home','tuscan style home','tuscan-style home','mediterranean style','spanish estate','mediterranean estate'), 'tags'=>array('mediterranean')),
		),
	 	'tags4' => array(
			array('find'=>array(' pool'), 'tags'=>array('pool')),
			array('find'=>array('modern interior','steel & glass','modern home','modern architecture','modern style and design'), 'tags'=>array('modern')),
			array('find'=>array('mountain view','view of mountains','views of mt.','magnificent mt','mountain views'), 'tags'=>array('mountain view')),
			array('find'=>array('ocean view','ocean views','view of the ocean','views of the ocean'), 'tags'=>array('ocean view')),
			array('find'=>array('museum'), 'tags'=>array('museum')),
			array('find'=>array('newly constructed','never lived in','newly built'), 'tags'=>array('new build')),
			array('find'=>array('no visible neighbors','no neighbors','no nearby neighbors'), 'tags'=>array('no visible neighbor','privacy')),
			array('find'=>array('ocean front','on the beach','oceanfront'), 'tags'=>array('ocean front'), 'override'=>array('why buy ocean front','ocean front path')),
			array('find'=>array('open floor plan','open-floor plan','open plan living','open floorplan','open concept','open layout'), 'tags'=>array('open floor plan')),
			array('find'=>array('bbq','barbecue','courtyard',"entertainer's yard",'outdoor space','patio','relaxing porches','custom deck','huge deck for entertaining','large covered patio','porches galore','large stone patio','incredible decks','outdoor living','multi-level back deck','out door kitchen','indoor/outdoor entertaining','outdoor entertaining','outdoor dining area','indoor-outdoor living','indoor/outdoor living','lanai'), 'tags'=>array('outdoor living space')),
			array('find'=>array('oversize closet','walk-in closet','walk in closet','closets are massive','his and hers closets','his/hers closets'), 'tags'=>array('oversize closet')),
			array('find'=>array('floor to ceiling windows','large windows','tall windows','walls of windows','stories of glass','huge windows','floor-to-ceiling windows','floor-to-ceiling glass','walls of glass','expansive windows'), 'tags'=>array('oversize windows')),
			array('find'=>array('privacy','private setting','far from neighbors'), 'tags'=>array('privacy')),
			array('find'=>array('private airstrip','airstrip','private hangar'), 'tags'=>array('private airstrip'))
		),
	 	'tags5' => array(
			array('find'=>array("' dock",'boathouse','boat dock','boat slip','boat lift','private dock','deeded slip'), 'tags'=>array('private dock','fishing')),
			array('find'=>array('ranch facilities','ranch property','dude ranch','working ranch','private ranch property','livestock'), 'tags'=>array('ranch')),
			array('find'=>array('river front','river frontage'), 'tags'=>array('river front','river')),
			array('find'=>array('close to river','nearby river'), 'tags'=>array('river','fishing')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river view','river')),
			array('find'=>array('rooftop','roof-top terrace'), 'tags'=>array('rooftop terrace')),
			array('find'=>array('rural','wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('sauna','steam room'), 'tags'=>array('sauna')),
			array('find'=>array('secluded','far from neighbors','no nearby neighbors','dead-end road'), 'tags'=>array('secluded')),
			array('find'=>array('guest cottage','guest house','in-law unit','separate unit','in-law suite','separate rentals','mother-in-laws','detached cottage','in-law quarters','in law unit','in-law unit','guest studio','guesthouse','separate guest quarters','caretaker quarters','caretaker apt'), 'tags'=>array('separate guest space')),
			array('find'=>array('ski front','ski in/ski out','ski-in/ski-out','ski condo'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('skyline view','city views','views of the city','view of the city','view of the skyline','views of the skyline','incredible views of little rock'), 'tags'=>array('skyline view')),
			array('find'=>array('small town'), 'tags'=>array('small town')),
			array('find'=>array('smart home','state of the art technology'), 'tags'=>array('smart home')),
			array('find'=>array('stables','mulitple horses','stall barn','horse shelter','horse stall','horse facilities'), 'tags'=>array('stables','horse property')),
			array('find'=>array('lake front property','lakefront property','lake front home','lakefront home','lake front estate','lakefront estate'), 'tags'=>array('lake front'))
		),
	 	'tags6' => array(
			array('find'=>array('tennis club','tennis center'), 'tags'=>array('tennis club')),
			array('find'=>array('tennis court','tennis-court'), 'tags'=>array('tennis court')),
			array('find'=>array('updated interior','modern appliances','renovated'), 'tags'=>array('updated interior')),
			array('find'=>array('valet'), 'tags'=>array('valet')),
			array('find'=>array('valley view','views of the valley','view of the valley','scenic valley','valley wide views'), 'tags'=>array('valley view')),
			// array('find'=>array('commercial vineyard','vineyard property','established vineyard'), 'tags'=>array('vineyard')),
			array('find'=>array('vineyard property','private vineyard','established vineyard','producing vineyard','potential vineyard','vineyard potential','for vineyard','vineyard estate'), 'tags'=>array('vineyard'), 'override'=>array('nearby vineyard','close to vineyards')),			
			array('find'=>array('wine cellar','wine storage','wine cave','wine room','tasting room'), 'tags'=>array('wine cellar')),
			array('find'=>array('victorian home','victorian style','classic victorian','typical victorian'), 'tags'=>array('victorian')),
			array('find'=>array('colonial design','colonial style','classic colonial','colonial architecture'), 'tags'=>array('colonial')),
			array('find'=>array('condo','condo/townhouse','condominium','townhome','townhouse','towne home','row-style home'), 'tags'=>array('condo')),
		)
	); // end tags array

	private $unTagsSet = array(
		'tags1' => array(
			array('find'=>array('aspen'), 'tags'=>array('alpine','skiing','winter <40','mountain view'), 'override'=>array('ski resort','ski slopes','ski mountains','ski access')),
			array('find'=>array('country home'), 'tags'=>array('countryside')),
			array('find'=>array('jets ski','waterskiing','skiing equipment','own private dock-boating, skiing','own private dock - boating, skiing',"jet ski's",'water-skiing','jet-skiing'),'tags'=>array('skiing','winter >40','alpine','drive to ski')),
			array('find'=>array('ladera ranch property','lang ranch property','Nellie Gail Ranch facilities'), 'tags'=>array('ranch'))
		),
	 	'tags2' => array(
			array('find'=>array(' gates'), 'tags'=>array('gated property')),
			array('find'=>array('poolhouse'), 'tags'=>array('separate guest space')),
			array('find'=>array('whirlpool','whirl pool'), 'tags'=>array('pool')),
			array('find'=>array('almost ocean front','ocean front bluff top','beach access','walks on the beach','walk on the beach'), 'tags'=>array('ocean front'), 'override'=>array('ocean front','ocean-front','oceanfront'))
		),
	 	'tags3' => array(
			array('find'=>array('golf course','country club'), 'tags'=>array('golf front'), 'override'=>array('golf front','golf course community','golf community','golf development','on the golf course','golf course in your backyard','golf course development','golf course property','golf property',' hole green','fairway')),
			array('find'=>array('media room'), 'tags'=>array('home theater')),
			array('find'=>array('hunter'), 'tags'=>array('hunting')),
		),
	 	'tags4' => array(
			array('find'=>array('contemporary','executive home'), 'tags'=>array('modern')),
			array('find'=>array('gulf to swim','stunning ocean'), 'tags'=>array('ocean front')),
			array('find'=>array('room for entertaining', 'great room','largest floor plans','great for entertaining'), 'tags'=>array('open floor plan')),
			array('find'=>array('roving security'), 'tags'=>array('privacy')),
		),
	 	'tags5' => array(
			array('find'=>array('harbor'), 'tags'=>array('private dock')),
			array('find'=>array('river view','view of river'), 'tags'=>array('river front')),
			array('find'=>array('wild life','wildlife'), 'tags'=>array('rural')),
			array('find'=>array('cul de sac','end of the road'), 'tags'=>array('secluded')),
			array('find'=>array('ski resort development','on the slopes'), 'tags'=>array('Ski Front Home')),
			array('find'=>array('on the slopes'), 'tags'=>array('Ski Front Home'), 'override'=>array('skiing')),
			array('find'=>array('equestrian'), 'tags'=>array('stables'), 'override'=>array('horse facility','horse facilities'))
		)
	);

	// advancedTagSet MUST have at least ONE find and mustHave elements and minMustHaveCount MUST be at least ONE.
	private $advancedTagSet = array (
		'adv1' => [//'listingTags' => ['lake front'],
					'find' => ['lake'],
					'mustHave' => ['shoreline','waterfront'],
					'minMustHaveCount' => 1,
					'tags' => ['lake front']
				   ],
	);

	/**
	 array(
'listingTags'=>array('list of listing tags found'), // leverage tags found for listings, comma separated tags, i.e.: condo, Ocean View, stables, golf front
'find'=>array('list of words to find'),                         // if listingTags is empty (defined as 'listingTags' => array() ), do a brute force search for these terms, i.e.: great surfing spot, four star restaurant, great hiking trails
'mustHave'=>array('list of words that must also be present beside those in "find"'),   // if 'listingTags' or 'find' had any hits, search for these terms
'minMustHaveCount'=>#),                                 // finally, we need at least this may mustHave's before be set any 'tags' to the city
'tags'=>array(list of trigger tags to assign) )           // city tags (type 1) to assign with a score of 1 if we have minMustHaveCount of matches, i.e.: hiking, fishing,

		'tags1' => ['listingTags' => [],
					'find' => [],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => []
				   ],

	***/
	
	private $triggerTags = [
		'tags1' => ['listingTags' => ['ocean front','ocean view'],
					'find' => ['beach frontage','by the ocean','to the ocean','near the ocean','maritime forest','intracoastal waterway','sounds of the ocean waves','sound of the ocean waves','right on the ocean'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['ocean']
				   ],
		'tags2' => ['listingTags' => ['ski front home'],
					'find' => ['mountain retreat','mountain air'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['mountain']
				   ],
		'tags3' => ['listingTags' => [],
					'find' => ['horse trails','equestrian','horse country','horse culture','horse people'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['equestrian']
				   ],
		'tags4' => ['listingTags' => ['vineyard'],
					'find' => ['wine country','wine trail'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['wine country']
				   ],
		'tags5' => ['listingTags' => ['drive to winery','vineyard'],
					'find' => [],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['wine culture']
				   ],
		'tags6' => ['listingTags' => ['golf front'],
					'find' => ['golf course','golfer','golfing'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['golf']
				   ],
		'tags7' => ['listingTags' => ['river front','river view'],
					'find' => [],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['river']
				   ],
		'tags8' => ['listingTags' => [],
					'find' => [' boat',' dock '],
					'mustHave' => ['ocean'],
					'minMustHaveCount' => 1,
					'tags' => ['ocean boating']
				   ],
		'tags9' => ['listingTags' => ['ski front home'],
					'find' => ['skiresort','ski-resort','ski resort','ski lifts'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['skiing']
				   ],
		'tags10' => ['listingTags' => [],
					'find' => ['to the beach','from the beach','private beach','beach access','beach trail','beach house','beach home'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['beach']
				   ],
		'tags11' => ['listingTags' => ['lake front','lake view'],
					'find' => ['nearby lake','lakefront','to the lake','lake front'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['lake']
				   ],
		'tags12' => ['listingTags' => [],
					'find' => ['lake'],
					'mustHave' => ['dock','boat'],
					'minMustHaveCount' => 2,
					'tags' => ['freshwater boating']
				   ],
		'tags13' => ['listingTags' => [],
					'find' => ['a concert','concerts','live music'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['music']
				   ],
		'tags14' => ['listingTags' => [],
					'find' => ['surf breaks','surfers','surfing','surf spots'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['surfing']
				   ],
		'tags15' => ['listingTags' => [],
					'find' => [' hunting ',' hunters',"hunter's"],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['hunting']
				   ],
		'tags16' => ['listingTags' => [],
					'find' => ['best shopping','high end shops','boutique shops','boutique shopping'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['shopping']
				   ],
		'tags17' => ['listingTags' => [],
					'find' => ['fishing','fisherman','fishermen','local fishing guides','great place to fish','execellent fishing'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['fishing']
				   ],
		'tags18' => ['listingTags' => [],
					'find' => ['casino','gambling'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['casinos']
				   ],
		'tags19' => ['listingTags' => [],
					'find' => ['hiking','state parks'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['hiking']
				   ],
		'tags20' => ['listingTags' => [],
					'find' => ['art community','art galleries','art district'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['artistic']
				   ],
		'tags21' => ['listingTags' => [],
					'find' => ['fine dining','restaurants','food options'],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['culinary']
				   ],
		'tags22' => ['listingTags' => [],
					'find' => ['state park','national park','wildlife area',"nat'l park"],
					'mustHave' => [],
					'minMustHaveCount' => 0,
					'tags' => ['major parks']
				   ],
	];

	public function __construct()
	{
		$this->tagList = [];
		$tags = $this->getClass("Tags")->get();
		foreach($tags as $tag) {
			$tag->tag = strtolower($tag->tag);
			$tag->id = intval($tag->id);
			$this->tagList[$tag->id] = $tag;
			unset($tag);
		}

		foreach($this->tagsSet as &$group) {
			foreach($group as &$set) {
				foreach($set['tags'] as &$tag) {
					$tag = strtolower($tag);
					unset($tag);
				}
				unset($set);
			}
			unset($group);
		}

		foreach($this->unTagsSet as &$group) {
			foreach($group as &$set) {
				foreach($set['tags'] as &$tag) {
					$tag = strtolower($tag);
					unset($tag);
				}
				unset($set);
			}
			unset($group);
		}

		foreach($this->triggerTags as &$trigger) {
			if (!empty($trigger['listingTags']))
				foreach($trigger['listingTags'] as &$ltags) {
					$ltags = strtolower($ltags);
					unset($ltags);
				}

			foreach($trigger['tags'] as &$tag) {
				$tag = strtolower($tag);
				unset($tag);
			}
			unset($trigger);
		}

		foreach($this->advancedTagSet as &$trigger) {
			if (!empty($trigger['listingTags']))
				foreach($trigger['listingTags'] as &$ltags) {
					$ltags = strtolower($ltags);
					unset($ltags);
				}

			foreach($trigger['tags'] as &$tag) {
				$tag = strtolower($tag);
				unset($tag);
			}
			unset($trigger);
		}
						 // 1     7       15     22      
		$this->tagRooms = " media servant in-law studio";
						 // 1      8     14       23   28   33    39     46   51          63    69    
		$this->viewType = " Canyon Ocean Mountain City Lake River Valley Park Golf Course Hills Bluff";
							  // 1     8            20        30       39    45     52        62        72      80     87          99      107    114    121      130     138    145             161           175   181       191      200    207 
		$this->architectType = " Ranch Contemporary Victorian Colonial Cabin Custom Craftsman Farmhouse Spanish Rustic Traditional Vintage French Chalet Bungalow English Modern New Traditional Mediterranean Tudor Cape Code Oriental Office Hospitality";
		$this->currentYear = (int)date("Y");
		$this->debugLevel = 3;
/*
		$this->mutex = \Mutex::create();
		$myThreads = array();
		foreach($this->tagsSet as $tags)
			$myThreads[] = new TagThread($tags,
										$this);
										*/
	}

	public function mutex()
	{
		return $this->mutex;
	}

	public function getTagId($look_for)
	{
		foreach($this->tagList as $tag)
			if ($tag->tag == $look_for)
				return $tag->id;

		$this->record("Failed to find matching tags for $look_for\n", 6);
		return null;
	}

	protected function record($buffer, $level = 1)
	{
		if ($level >= $this->debugLevel)
			echo $buffer;
		
		ob_flush();
		flush();
	}

	public function getTagList(&$listing, $field, $data)
	{
		if (!isset($listing['tags']))
			$listing['tags'] = array();
		switch($field)
		{
			case 'ExteriorType':
				break;
			case 'Room': 
				$tmp = explode(" ", $data);
				$tmp = strtolower($tmp[0]);
				$pos = strpos($this->tagRooms, $tmp);
				switch($pos){
					case 1: $listing['tags']['home theater'] = $this->getTagId('home theater'); break;
					case 7: $listing['tags']['estate'] = $this->getTagId('estate'); break;
					case 15:
					case 22: $listing['tags']['separate guest space'] = $this->getTagId('separate guest space');  break;
					default:
						break;
				} 
				break;
			case 'ArchitectureStyle':
				$pos = strpos($this->architectType, $data);
				switch($pos) {
					case 145:
					case 62:
					case 1: $listing['tags']['country style'] = $this->getTagId('country style'); break;
					case 45:
					case 8: $listing['tags']['modern'] = $this->getTagId('modern'); break;
					case 99:
					case 20: $listing['tags']['victorian'] = $this->getTagId('victorian'); break;
					case 175:
					case 130:
					case 30: $listing['tags']['colonial'] = $this->getTagId('colonial'); break;
					case 181:
					case 114:
					case 80:
					case 39: $listing['tags']['country style'] = $this->getTagId('country style'); break;
					case 52: $listing['tags']['craftsman'] = $this->getTagId('craftsman'); break;
					case 161:
					case 107:
					case 72: $listing['tags']['mediterranean'] = $this->getTagId('mediterranean'); break;
					case 87: $listing['tags']['classical style'] = $this->getTagId('classical style'); break;
					case 121: $listing['tags']['island architecture'] = $this->getTagId('island architecture'); break;
					case 191: $listing['tags']['asian style'] = $this->getTagId('asian style'); break;
					case 200:
					case 207: $listing['error'] |= COMMERCIAL; break;
				}
				break;
			case 'HasDoorman': if ($data == 'true') $listing['tags']['doorman'] = $this->getTagId('doorman'); 
				break;
			case 'HasDock': if ($data == 'true') $listing['tags']['private dock'] = $this->getTagId('private dock'); 
				break;
			case 'HasDoublePaneWindows': if ($data == 'true') $listing['tags']['eco-friendly'] = $this->getTagId('eco-friendly'); 
				break;
			case 'HasFireplace': if ($data == 'true') $listing['tags']['fireplace'] = $this->getTagId('fireplace'); 
				break;
			case 'HasGatedEntry': if ($data == 'true') $listing['tags']['gated community'] = $this->getTagId('gated community'); 
				break;
			case 'HasGreenhouse': 
				break;
			case 'HasJettedBathTub': 
			case 'HasHotTubSpa': if ($data == 'true') $listing['tags']['hot tub'] = $this->getTagId('hot tub'); 
				break;
			case 'HasMotherInLaw': if ($data == 'true') $listing['tags']['separate guest space'] = $this->getTagId('separate guest space'); 
				break;
			case 'IsNewConstruction': if ($data == 'true') $listing['tags']['new build'] = $this->getTagId('new build'); 
				break;
			case 'NumParkingSpaces':
				break;
			case 'HasPool': if ($data == 'true') $listing['tags']['pool'] = $this->getTagId('pool'); 
				break;
			case 'HasSauna': if ($data == 'true') $listing['tags']['sauna'] = $this->getTagId('sauna'); 
				break;
			case 'HasSportsCourt': if ($data == 'true') $listing['tags']['tennis court'] = $this->getTagId('tennis court'); 
				break;
			case 'HasVaultedCeiling': if ($data == 'true') $listing['tags']['high ceilings'] = $this->getTagId('high ceilings'); 
				break;
			case 'ViewType':
				$pos = strpos($this->viewType, $data);
				switch($pos){
					case 39:
					case 1: $listing['tags']['valley view'] = $this->getTagId('valley view'); break;
					case 8: $listing['tags']['ocean view'] = $this->getTagId('ocean view'); break;
					case 63:
					case 69:
					case 14: $listing['tags']['mountain view'] = $this->getTagId('mountain view'); break;
					case 23: $listing['tags']['skyline view'] = $this->getTagId('skyline view'); break;
					case 28: $listing['tags']['lake view'] = $this->getTagId('lake view'); break;
					case 33: $listing['tags']['river'] = $this->getTagId('river'); break;
					case 46: $listing['tags']['greenbelt view'] = $this->getTagId('greenbelt view'); break;
					case 51: $listing['tags']['golf front'] = $this->getTagId('golf front'); break;
				}
				break;
			case 'IsWaterfront': // hard to tell what kind of water...
				break;
			case 'HasWetBar': if ($data == 'true') $listing['tags']['home bar'] = $this->getTagId('home bar'); 
				break;
			case 'IsWired': if ($data == 'true') $listing['tags']['smart home'] = $this->getTagId('smart home'); 
				break;
			case 'YearUpdated': // remodel job
				break;
			case 'YearBuilt': if ( (intval($data) + 5) > $this->currentYear) $listing['tags']['new build'] = $this->getTagId('new build'); 
				break;
			case 'ListingDescription': // parse through desc and get as many tags as possible
				$tmp = strtolower($data);
				$this->searchKeyPhrases($listing, $tmp);
				break;

		}
	}

	protected function hasTag($id, $tagList) {
		foreach($tagList as $tag) {
			if ($tag->tag_id == $id)
				return true;
			unset($tag);
		}
		return false;
	}


	public function cleanTags($about, $tagList, &$badTags) {
		$tmp = strtolower($about);
		$len = strlen($tmp);
		foreach($this->unTagsSet as $tags) {
			foreach($tags as $keys) {
				foreach($keys['find'] as $key) {
					if ( ($pos = strpos($tmp, $key)) !== false &&
						  $pos < $len-1)
					{
						$untagIt = true;
						if (isset($keys['override'])) {
							foreach($keys['override'] as $key) {
								if ( ($pos = strpos($tmp, $key)) !== false &&
						  			  $pos < $len-1)
								{
									$untagIt = false;
									break;
								}
							}
						}
						if ($untagIt) {
							foreach($keys['tags'] as $tag) {
								$id = $this->getTagId($tag);
								if ($this->hasTag($id, $tagList))
									$badTags[$tag] = $id;
							}
						}
						unset($tags);
					} // if (strpos)
					unset($key);
				} // foreach($keys['find'] 
				unset($keys);
			} // foreach($tags
			unset($tags);
		} // foreach($this->unTagsSet
	}

	public function unTagList(&$listing, $orig) {
		$tmp = strtolower($orig);
		$len = strlen($tmp);
		foreach($this->unTagsSet as $tags) {
			foreach($tags as $keys) {
				foreach($keys['find'] as $key) {
					if ( ($pos = strpos($tmp, $key)) !== false &&
						  $pos < $len-1)
					{
						$untagIt = true;
						if (isset($keys['override'])) {
							foreach($keys['override'] as $key) {
								if ( ($pos = strpos($tmp, $key)) !== false &&
						  			  $pos < $len-1)
								{
									$untagIt = false;
									break;
								}
							}
						}
						if ($untagIt) {
							foreach($keys['tags'] as $tag) {
								if (isset($listing['tags'][$tag])) {
									unset($listing['tags'][$tag]);
								}
							}
						}
						break;
					}
					unset($key);
				}
				unset($keys);
			}
			unset($tags);
		}
	}

	protected function searchKeyPhrases(&$listing, &$data)
	{
		foreach($this->tagsSet as $tags)
		{
			$this->searchSet($tags, $listing, $data);
			unset($tags);
		}
		if ( isset($listing['tags']['condo']) ) {
			$listing['flags'] |= LISTING_IS_CONDO;
			if (isset($listing['tags']['house'])) unset($listing['tags']['house']);
		}

		$this->searchAdvTagSet($listing, $data);
		$this->searchTriggers($listing, $data);
	}

	protected function searchAdvTagSet(&$listing, &$data) {
		$len = strlen($data);
		foreach($this->advancedTagSet as $trigger) {
			$gotLTag = false;
			$keys = array_keys($listing['tags']); // all tags so far from the listing description
			foreach($trigger['tags'] as $ltag) {
				if (in_array($ltag, $keys)) {
					$gotLTag = true;
				}
				unset($ltag);
				if ($gotLTag) break;
			}

			if ($gotLTag) {
				unset($trigger);
				continue; // we already have this tag
			}

			$found = $this->findFromList($trigger['find'], $data, $len);
			if ($found) {
				$setTriggers = false;
				if (!empty($trigger['mustHave'])) {
					$found = $this->findFromList($trigger['mustHave'], $data, $len, $trigger['minMustHaveCount']);
					if ($found >= $trigger['minMustHaveCount'])
						$setTriggers = true;
				}
				// else - NOTE: this code cannot happen for listings, must have at least ONE mustHave element!
				// 	$setTriggers = true;

				if ($setTriggers) {
					foreach($trigger['tags'] as $tag) {
						$listing['tags'][$tag] = $this->getTagId($tag);
						unset($tag);
					}
				}
			}
			unset($trigger);
		}
	}

	protected function searchTriggers(&$listing, &$data) {
		$len = strlen($data);
		foreach($this->triggerTags as $trigger) {
			$gotLTag = false;
			if (!empty($trigger['listingTags'])) {
				if (!empty($listing['tags'])) {
					$keys = array_keys($listing['tags']);
					foreach($trigger['listingTags'] as $ltag) {
						if (in_array($ltag, $keys)) {
							$gotLTag = true;
						}
						unset($ltag);
						if ($gotLTag) break;
					}
				}
			}

			if ($gotLTag) {
				foreach($trigger['tags'] as $tag) {
					$listing['triggers'][$tag] = $this->getTagId($tag);
					unset($tag);
				}
			}
			else {
				$found = $this->findFromList($trigger['find'], $data, $len);
				if ($found) {
					$setTriggers = false;
					if (!empty($trigger['mustHave'])) {
						$found = $this->findFromList($trigger['mustHave'], $data, $len, $trigger['minMustHaveCount']);
						if ($found >= $trigger['minMustHaveCount'])
							$setTriggers = true;
					}
					else
						$setTriggers = true;

					if ($setTriggers) {
						foreach($trigger['tags'] as $tag) {
							$listing['triggers'][$tag] = $this->getTagId($tag);
							unset($tag);
						}
					}
				}
			}

			unset($trigger);
		}
	}

	protected function findFromList($keyList, &$data, $len, $minMustHaveCount = 0) {
		$found = 0;
		
		foreach($keyList as $key) {
			if ( ($pos = strpos($data, $key)) !== false &&
				  $pos < $len-1) {
				$found++;
			}
			unset($key);
			if ($minMustHaveCount &&
				$found >= $minMustHaveCount)
				break;
		}
		return $found;
	}

	protected function searchSet(&$myTags, &$listing, &$data)
	{
		$len = strlen($data);
		foreach($myTags as $keys)
		{
			$this->searchOneSet($keys, $listing, $data, $len);
			unset($keys);
		}
	}

	protected function searchOneSet($keys, &$listing, $data, $len, $tagList = 'tags') {
		$foundOne = false;
		foreach($keys['find'] as $key) {
			if ( ($pos = strpos($data, $key)) !== false &&
				  $pos < $len-1)
			{
				//Mutex::lock($this->owner->mutex());
				$foundOne = true;
				$tagIt = true;
				if (isset($keys['override'])) {
					foreach($keys['override'] as $key2) {
						if ( ($pos = strpos($tmp, $key2)) !== false &&
					  		  $pos < $len-1)
						{
							$tagIt = false;
						}
						unset($key2);
						if (!$tagIt) break;
					}
				}
				if ($tagIt) {
					foreach($keys['tags'] as $tag) {
						$listing[$tagList][$tag] = $this->getTagId($tag);
						unset($tag);
					}
				}
				//Mutex::unlock($this->owner->mutex());
			}
			unset($key);
			if ($foundOne) break;
		}
	}
/*
	protected function searchKeyPhrases(&$listing, $data)
	{
		
		foreach($this->myThreads as $thread)
		{
			$thread->setData($listing, $data);
			$thread->start();
		}

		foreach($this->myThreads as $thread)
			$thread->join();
	}
	*/
}

/*
use \Threaded;

class TagThread extends Threaded {
	public function __construct(&$tagsArray,
								&$owner)
	{
		$this->myTags = &$tagsArray();
		$this->owner = &$owner;
	}

	public function setData(&$searchThis,
							&$object)
	{
		$this->searchThis = &$searchThis;
		$this->object = &$object;		
	}

	public function run()
	{
		foreach($this->myTags as $keys)
		{
			foreach($keys['find'] as $key)
				if (strstr($this->searchThis, $key) !== false)
				{
					Mutex::lock($this->owner->mutex());
					foreach($keys['tags'] as $tag)
						$this->object['tags'][$tag] = $this->getTagId($tag);
					Mutex::unlock($this->owner->mutex());
					break;
				}
		}
	}
}
*/

