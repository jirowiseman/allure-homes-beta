<?php
namespace AH;

// Load WP components, no themes
if ( ! defined( 'WP_USE_THEMES' ) )
	define('WP_USE_THEMES', false);
// define('WP_ADMIN', true);2

require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/IPv6.php');

require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/pluggable.php');

require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );


// define( 'zohoAPIKey', 'e247c7877b1094422f2ed22409352db3');
// define( 'zohoAPIKey', '44dc430392f0dcb8e15bdf365bcdedfe');
define( 'zohoAPIKey', '63b7e5fe5b243036db5b18ad3fbe6cee');

define( 'zohoMAXLifestyles', 4);

class Zoho extends Controller {
	// comes from Controller now
	// protected $timezone_adjust = 7;

	protected $zohoInsertTags = [
		"seller_id"=>"Seller ID",
		'email'=>"Email",
		'first_name'=>"First Name",
		'last_name'=>"Last Name",
		"phone"=>"Phone",
		"company"=>"Company",
		"website"=>"Website",
		"mobile"=>"Mobile",
		"street_address"=>"Street",
		"city"=>"City",
		"state"=>"State",
		"zip"=>"Zip Code",
		"country"=>"Country",
		"about"=>"About",
		// stuff below here gets handled with the 'portal', going through all the reservation metas
		"portal"=>"Portal",
		"target_city"=>"Target City",
		"target_state"=>"Target State",
		"lifestyle1"=>"LifeStyle 1",
		"lifestyle2"=>"LifeStyle 2",
		"lifestyle3"=>"LifeStyle 3",
		"lifestyle4"=>"LifeStyle 4",
		"l1Desc"=>"L1 Description",
		"l2Desc"=>"L2 Description",
		"l3Desc"=>"L3 Description",
		"l4Desc"=>"L4 Description",
		"preferredName"=>"Preferred Display Name",
		"bre"=>"Real Estate License number",
		"yrLicensed"=>"Year Licensed",
		"yrsInServiceArea"=>"Years lived in Service Area",
		"transactions"=>"Transactions/year",
		"contact_email"=>"Secondary Email",
		"date"=>"Date",
		"notes"=>'Description'
	];

	protected $zohoLeadTags = [
		'email'=>"Email",
		'first_name'=>"First Name",
		'last_name'=>"Last Name",
		"phone"=>"Phone",
		"seller_id"=>"Seller ID",
		"portal"=>"Portal",
		"target_city"=>"Target City",
		"target_state"=>"Target State",
		"lifestyle1"=>"LifeStyle 1",
		"lifestyle2"=>"LifeStyle 2",
		"lifestyle3"=>"LifeStyle 3",
		"lifestyle4"=>"LifeStyle 4",
		"lifestyle5"=>"LifeStyle 5",
		"l1Desc"=>"L1 Description",
		"l2Desc"=>"L2 Description",
		"l3Desc"=>"L3 Description",
		"l4Desc"=>"L4 Description",
		"l5Desc"=>"L5 Description",
		'preferredName'=>"Preferred Display Name",
		"bre"=>"Real Estate License number",
		"yrLicensed"=>"Year Licensed",
		"yrsInServiceArea"=>"Years lived in Service Area",
		"transactions"=>"Transactions/year",
		"contact_email"=>"Secondary Email",
		"notes"=>"Description",
		"lead_id"=>"LEADID",
		// "contact_id"=>"CONTACTID",
		// extra data
		"company"=>"Company",
		"website"=>"Website",
		"mobile"=>"Mobile",
		"street_address"=>"Street",
		"city"=>"City",
		"state"=>"State",
		"zip"=>"Zip Code",
		"country"=>"Country",
		"about"=>"About"
	];

	protected $MapLeadToContacts = [
		"lead_id"=>"CONTACTID",
		"street_address"=>"Mailing Street",
		"city"=>"Mailing City",
		"state"=>"Mailing State",
		"zip"=>"Mailing Zip",
		"country"=>"Mailing Country",
	];

	public function __construct($logIt = 0){
		parent::__construct();
		set_time_limit(0);
		require_once(__DIR__.'/Options.class.php');
		$o = $this->getClass('Options');
		$x = $o->get((object)array('where'=>array('opt'=>'ZohoClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/zoho.log') : null;
		}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

	public function testAPI() {
		$retval = $this->getOnePage(1, 20);
		return new Out( empty($retval) ? 'fail' : 'OK', empty($retval) ? "No records" : count($retval)." records returned");
	}

	protected function getOnePage($start, $end) {
		$this->log("getOnePage - start:$start to end:$end");
		// defaults to getting first 20
		// fromIndex	Integer	Default value - 1
		// toIndex		Integer	Default value - 20
		// 				Maximum value - 200
		header("Content-type: application/xml");
		$token=zohoAPIKey;
		$url = "https://crm.zoho.com/crm/private/xml/Leads/getRecords";
		$param= "authtoken=".$token."&scope=crmapi"."&fromIndex=$start&toIndex=$end";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		$result = curl_exec($ch);
		curl_close($ch);

		$xml = new \XMLReader();
		if (($result = $xml->xml($result)) == false)
		{
			$this->log("Failed to open xml file: $file\n", 6);
			continue;
		}

		//$xml = simplexml_load_string($buffer);
		$retval = $this->parseXml($xml);
		$xml->close();
		return $retval;
	}

	protected function clearOutLead(&$reservation) {
		foreach($this->zohoLeadTags as $key=>$crm) {
			switch($key) {
				case 'email':
				case 'first_name':
				case 'last_name':
				case 'lead_id':
				case 'phone':
				case 'mobile':
				case 'portal': // if null, get rid of it
					if (!isset($reservation[$key]) &&
						$reservation[$key] === null)
						unset($reservation[$key]);
					elseif (($key == 'phone' ||
							 $key == 'mobile') &&
						    !empty($reservation[$key])) { // clean it up
						$reservation[$key] = str_replace('(', '', $reservation[$key]);
						$reservation[$key] = str_replace(')', '', $reservation[$key]);
						$reservation[$key] = str_replace('-', '', $reservation[$key]);
						$reservation[$key] = str_replace('.', '', $reservation[$key]);
						$reservation[$key] = str_replace(' ', '', $reservation[$key]);
						if (strlen($reservation[$key]) == 11 &&
							$reservation[$key][0] == '1')
							$reservation[$key] = substr($reservation[$key], 1); // strip off the 1
					}

				case 'seller_id': // set it to 0
					if (!isset($reservation['seller_id']) ||
						$reservation['seller_id'] == null) 
						$reservation['seller_id'] = 0;
					break;

				default: // get rid of no matter what
					if (isset($reservation[$key]) ||
						$reservation[$key] == null) 
						unset($reservation[$key]);
					break;
			}
		}
	}

	protected function buildFromLead(&$lead, &$reservation, $fromLeads = true) {
		$lead = (array)$lead;
		foreach($this->zohoLeadTags as $key=>$crm) {
			if (!$fromLeads &&
				isset($this->MapLeadToContacts[$key]))
				$crm = $this->MapLeadToContacts[$key];
			if (isset($lead[$crm]))
				$reservation[$key] = $lead[$crm];
		}

		$reservation['type'] = 0;
		// $this->log("buildFromLead - reservation: ".print_r($reservation, true));

		if (isset($reservation['portal']) && 
			!empty($reservation['portal'])) {
			if ($reservation['portal'] == 'N/A')
				$reservation['portal'] = null;
			else
				$reservation['type'] |= SELLER_IS_PREMIUM_LEVEL_1;
		}

		$reservation['meta'] = [];

		if (isset($reservation['notes']) &&
			!empty($reservation['notes'])) {
			if (strlen($reservation['notes']) > 150) {// just a guess, BTW
				$meta = new \stdClass();
				$meta->action = SELLER_NOTES_FROM_RESERVATION;
				$meta->notes = $reservation['notes'];
				$reservation['meta'][] = $meta;
				$this->log("Created SELLER_NOTES_FROM_RESERVATION for {$lead[Email]} - {$reservation['notes']}", 4);
			}
		}

		if (isset($reservation['bre']) &&
			!empty($reservation['bre'])) {
			$meta = new \stdClass();
			$meta->action = SELLER_BRE;
			$meta->BRE = $reservation['bre'];
			$meta->state = isset($reservation['state']) && !empty($reservation['state']) ? $reservation['state'] :
						   isset($reservation['target_state']) && !empty($reservation['target_state']) ? $reservation['target_state'] : '';
			$meta->userMode = isset($reservation['seller_id']) && !empty($reservation['seller_id']) ? AGENT_LISTHUB : AGENT_ORGANIC;
			$meta->needVerify = isset($reservation['seller_id']) && !empty($reservation['seller_id']) ? 0 : 1;
			$reservation['meta'][] = $meta;
		}

		if ( (isset($reservation['preferredName']) &&
			  !empty($reservation['preferredName'])) ||
			 (isset($reservation['yrLicensed']) &&
			  !empty($reservation['yrLicensed'])) ||
			 (isset($reservation['yrsInServiceArea']) &&
			  !empty($reservation['yrsInServiceArea'])) ||
			 (isset($reservation['transactions']) &&
			  !empty($reservation['transactions'])) ||
			 (isset($reservation['contact_email']) &&
			  !empty($reservation['contact_email'])) ) {
			$meta = new \stdClass();
			$meta->action = SELLER_PROFILE_DATA;

			if (isset($reservation['contact_email']) &&
			  	!empty($reservation['contact_email'])) {
				$meta->contact_email = $reservation['contact_email'];
			}

			$meta->first_name = '';
			$meta->last_name = '';
			if (isset($reservation['preferredName']) &&
			  	!empty($reservation['preferredName'])) {
				$names = explode(" ", $reservation['preferredName']);
				$meta->first_name = $names[0];
				if (count($names) > 1) {
					array_shift($names);
					$meta->last_name = implode(' ', $names);
				}
			}			

			if (isset($reservation['yrLicensed']) &&
			  	!empty($reservation['yrLicensed'])) {
				$meta->year = $reservation['yrLicensed'];
			}	
			else
				$meta->year = date("Y", time() + ($this->timezone_adjust*3600));			

			if (isset($reservation['yrsInServiceArea']) &&
			  	!empty($reservation['yrsInServiceArea'])) {
				$meta->inArea = number_format( intval(date("Y", time() + ($this->timezone_adjust*3600))) - intval($reservation['yrsInServiceArea']), 0, '.', '' );
			}
			else
				$meta->inArea = date("Y", time() + ($this->timezone_adjust*3600));
			
			if (isset($reservation['transactions']) &&
			  	!empty($reservation['transactions'])) 
			  	$meta->sold = $reservation['transactions'];
			else
				$meta->sold = "1";

			$reservation['meta'][] = $meta;
		}

		$lifestyleTags = ['lifestyle1','lifestyle2','lifestyle3','lifestyle4','lifestyle5'];
		$lifestyleDesc = ['l1Desc','l2Desc','l3Desc','l4Desc','l5Desc'];

		if (isset($reservation['target_city']) && 
			!empty($reservation['target_city']) &&
			// isset($reservation['target_state']) && 
			// !empty($reservation['target_state']) &&
			isset($reservation['lifestyle1']) && 
			!empty($reservation['lifestyle1'])) {
			$cityStr = trim($reservation['target_city']);
			$state = isset($reservation['target_state']) && !empty($reservation['target_state']) ? trim($reservation['target_state']) : '';
			if (!empty($state))
				$city = $this->getClass('Cities')->get((object)['like'=>['city'=>fixCity($cityStr, $state)],
																'like2'=>['state'=>$state]]);
			else
				$city = $this->getClass('Cities')->get((object)['like'=>['city'=>$cityStr]]);
			$this->log("buildFromLead - city:".(empty($city) ? " not found" : "found")." $cityStr");
			if (empty($city)) {
				$GeoCode = $this->getClass('GoogleLocation');
				$lng = -1; $lat = -1;
				$city = (object)['city'=>$cityStr,
								 'state'=>$state];
				$result = null;
				$result_city = null;
				if ( $this->getClass('Cities')->geocodeCity($GeoCode, $city, $lng, $lat, $result_city, $result)) {
					$city->lng = $lng;
					$city->lat = $lat;
				}
				$x = $this->getClass('Cities')->add($city);
				if (!empty($x)) {
					$city->id = $x;
					$city = [$city]; // for the array_pop below
				}
				else
					unset($city);
			}

			if (!empty($city)) {
				if (count($city) == 1)
					$city = array_pop($city);
				else { // oops! have more than 1, yikes!
					$gotOne = false;
					foreach($city as $checkIt) {
						if (strtolower($cityStr) == strtolower($checkIt->city)) { // get first full match
							$city = $checkIt;
							$gotOne = true;
							break;
						}
					}
					if (!$gotOne) // yikes!
						$city = array_pop($city);  // grab the first one then..
				}
				$reservation['type'] |= SELLER_IS_PREMIUM_LEVEL_2;
				$meta = new \stdClass();
				$meta->action = ORDER_AGENT_MATCH;
				$meta->item = [];
				foreach($lifestyleTags as $i=>$style) {
					if (isset($reservation[$style]) && 
						!empty($reservation[$style]) &&
						$reservation[$style] != 'none') {
						$tag = strtolower($reservation[$style]);
						$tag = $tag == 'multilingual' ? 'multi-lingual' : $tag;
						$tags = $this->getClass('Tags')->get((object)['like'=>['tag'=>$tag]]);
						if (!empty($tags)) {
							$shortest = 9999;
							$useTag = null;
							foreach($tags as $tag) {
								if ( ($diff = levenshtein($tag->tag, $reservation[$style])) < $shortest) {
									$useTag = $tag;
									$shortest = $diff;
								} 
							}
							$meta->item[] = (object)['location'=>$city->id,
													 'locationStr'=>$city->city.", ".$city->state,
													 'desc'=>isset($reservation[$lifestyleDesc[$i]]) ? $reservation[$lifestyleDesc[$i]] : '',
													 'specialty'=>$useTag->id,
													 'specialtyStr'=>ucwords($useTag->tag)];
						}
					}				
				}
				
				// $this->log("buildFromLead - lifestyles: ".print_r($meta, true));
				$reservation['meta'][] = $meta;
			}
		}

		$accessExtraData = false;
		global  $extraReservationData;
		foreach($extraReservationData as $extra) {
			switch($extra) {
				case 'company':
					if (isset($reservation[$extra]) &&
					    !empty($reservation[$extra]) &&
					    strtolower($reservation[$extra]) != 'unknown')
					    $accessExtraData = true;
					break;
				default: 
					if (isset($reservation[$extra]) &&
					    !empty($reservation[$extra]))
					    $accessExtraData = true;
					break;
			}
			
		}

		if ($accessExtraData) {
			$meta = new \stdClass();
			$meta->action = RESERVATION_EXTRA_SELLER_DATA;
			foreach($extraReservationData as $extra) {
				$meta->$extra = '';
				if (isset($reservation[$extra]) &&
				  	!empty($reservation[$extra])) {
					$meta->$extra = $reservation[$extra];
					$this->log("Adding extra data: {$meta->$extra}");
				}
			}
			
			$reservation['meta'][] = $meta;
		}

		$this->clearOutLead($reservation);
		if (count($reservation['meta']) == 0)
			unset($reservation['meta']);
	}

	protected function fixPhoneMobile(&$lead) {
		if ( isset($lead['mobile']) &&
			 !empty($lead['mobile'])) {
			if ( !isset($lead['phone']) ) 
				 $lead['phone'] = $lead['mobile'];
		}

		if (isset($lead['mobile']) ||
			$lead['mobile'] == null) 
			unset($lead['mobile']);
	}

	public function updateOneLead($lead, $fromLeads = true) {
		$retval = false;
		$reservationId = 0;
		$ourLead = [];
		$this->buildFromLead($lead, $ourLead, $fromLeads);
		$this->log("updateOneLead - ourLead:".print_r($ourLead, true));
		$this->fixPhoneMobile($ourLead);
		$lead = (object)$lead;
		if ( isset($lead->Email) &&
			!empty($lead->Email) )
			$reservation = $this->getClass('Reservations')->get((object)['like'=>['email'=>$lead->Email]]);
		else
			$reservation = $this->getClass('Reservations')->get((object)['like'=>['lead_id'=>$lead->LEADID]]);
		$this->log("updateOneLead - reservation:".(!empty($reservation) ? print_r($reservation[0], true) : "not found using $lead->Email nor $lead->LEADID"));
		if (!empty($reservation) ) {
			$reservation = array_pop($reservation);
			$reservationId = $reservation->id;
			foreach($reservation as $key=>$value) {
				if ($key == 'id')
					continue;
				if ($key != 'meta') {
					if (isset($reservation->$key)) {
						if ($reservation->$key == $ourLead[$key])
							unset($ourLead[$key]);
					}
					else
						$this->log("updateOneLead - reservation does not have $key data.");
				}
				else if (isset($ourLead[$key])) {
					// just test the first meta entry
					$needUpdate = false;
					$metas = [];
					$current = null;
					$currentBre = null;
					$currentProfileData = null;
					$currentExtra = null;
					if (!empty($value)) foreach($value as $meta)
						if ($current == null &&
							$meta->action == ORDER_AGENT_MATCH)
							$current = $meta; // get first one
						elseif ($meta->action == SELLER_BRE)
							$currentBre = $meta;
						elseif ($meta->action == SELLER_PROFILE_DATA)
							$currentProfileData = $meta;
						elseif ($meta->action == RESERVATION_EXTRA_SELLER_DATA)
							$currentExtra = $meta;
						else
							$metas[] = $meta; // any other meta data we are not tracking/comparing with CRM leads

					$zoho = null;
					$zohoBre = null;
					$zohoProfileData = null;
					$zohoExtra = null;
					foreach($ourLead[$key] as $meta)
						if ($meta->action == ORDER_AGENT_MATCH) // can now have SELLER_BRE and SELLER_PROFILE_DATA
							$zoho = $meta;
						elseif ($meta->action == SELLER_BRE)
							$zohoBre = $meta;
						elseif ($meta->action == SELLER_PROFILE_DATA)
							$zohoProfileData = $meta;
						elseif ($meta->action == RESERVATION_EXTRA_SELLER_DATA)
							$zohoExtra = $meta;

					// $zoho = $ourLead[$key][0];
					$this->log("updateOneLead - current: ".($current ? print_r($current, true) : "N/A").", zoho:".print_r($zoho, true));
					if ($zoho) {
						if ($current == null) { // no existing AM data
							$metas[] = $zoho;
							$needUpdate = true;
						}
						else {
							$gotMatch = 0;
							foreach($current->item as $existing) {
								foreach($zoho->item as $incoming) {
									if ($existing->location == $incoming->location &&
										$existing->specialty == $incoming->specialty) {
										if ( isset($incoming->desc) &&
											 isset($existing->desc) &&
											 strcmp( removeslashes($incoming->desc), removeslashes($existing->desc) ) == 0)
											$gotMatch++;
										elseif (!isset($incoming->desc) &&
											 	!isset($existing->desc))
											$gotMatch++;
										unset($incoming);
										break;
									}
									unset($incoming);
								}
								unset($existing);
							}
							$this->log("updateOneLead - gotMatch:$gotMatch, zohoCount:".count($zoho->item));
							if ($gotMatch == count($zoho->item))
								$metas[] = $current;
							else {
								$metas[] = $zoho;
								$needUpdate = true;
							}
						}
					}
					elseif ($current)
						$metas[] = $current;

					if ($zohoBre) {
						if ($currentBre == null) { // no existing AM data
							$metas[] = $zohoBre;
							$needUpdate = true;
						}
						else {
							if ($currentBre->BRE != $zohoBre->BRE ||
								$currentBre->state != $zohoBre->state ||
								$currentBre->userMode != $zohoBre->userMode ||
								$currentBre->needVerify != $zohoBre->needVerify) {
								$metas[] = $zohoBre;
								$needUpdate = true;
							}
							else
								$metas[] = $currentBre;	
						}
					}
					elseif ($currentBre)
						$metas[] = $currentBre;

					if ($zohoProfileData) {
						if ($currentProfileData == null) { // no existing PD data
							$metas[] = $zohoProfileData;
							$needUpdate = true;
						}
						else {
							if ($currentProfileData->contact_email != $zohoProfileData->contact_email ||
                                $currentProfileData->first_name != $zohoProfileData->first_name ||
                                $currentProfileData->last_name != $zohoProfileData->last_name ||
                                $currentProfileData->year != $zohoProfileData->year ||
                                $currentProfileData->inArea != $zohoProfileData->inArea ||
                                $currentProfileData->sold != $zohoProfileData->sold) {
                                $metas[] = $zohoProfileData;
                                $needUpdate = true;
                       	 	}
							else
								$metas[] = $currentProfileData;	
						}
					}
					elseif ($currentProfileData)
						$metas[] = $currentProfileData;

					if ($zohoExtra) {
						if ($currentExtra == null) { // no existing PD data
							$metas[] = $zohoExtra;
							$needUpdate = true;
						}
						else {
							$updateExtra = false;
							global  $extraReservationData;
							foreach($extraReservationData as $extra) {
								if ($currentExtra->$extra != $zohoExtra->$extra)
									$updateExtra = true;
							}

							if ($updateExtra) {
								$metas[] = $zohoExtra;
								$needUpdate = true;
							}
							else
								$metas[] = $currentExtra;	
						}
					}
					elseif ($currentExtra)
						$metas[] = $currentExtra;

					if ($needUpdate)
						$ourLead[$key] = $metas;
					else
						unset($ourLead[$key]);

					unset($metas);
				}
			}
			if (!$fromLeads) {
				if (!isset($reservation->flags) || ($reservation->flags & RESERVATION_LEADID_FROM_CONTACTS) == 0)
					$ourLead['flags'] = (isset($reservation->flags) ? $reservation->flags : 0) |  RESERVATION_LEADID_FROM_CONTACTS;
			}
			$this->log("updateOneLead - reservationId:".$reservationId.", fields count: ".count($ourLead).", ourLead:".print_r($ourLead, true).", reservation:".print_r($reservation, true));
			if (!empty($ourLead)) {
				$x = $this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservationId],
																	'fields' => $ourLead]]);
				unset($reservation);
				if (!empty($x)) 
					$retval = true;
			}
		}
		else {
			if (!$fromLeads)
				$ourLead['flags'] = RESERVATION_LEADID_FROM_CONTACTS;
			$reservationId = $this->getClass('Reservations')->add($ourLead);
			if ($reservationId)
				$retval = true;
			$this->log("updateOneLead - adding new reservation at row:".$reservationId);
		}

		$sellerId = 'Seller ID';
		if ( !empty($lead->$sellerId) &&
			 !empty($reservationId) &&
			 $retval ) { // check $retval.  It will be true if something worthy happened.
			$reservation = $this->getClass('Reservations')->get((object)['where'=>['id'=>$reservationId]])[0];
			$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$lead->$sellerId]]);
			if (!empty($seller))
				updateProfileFromReservation($reservation, $seller[0], $this);
		}

		unset($ourLead);
		return $retval;
	}

	public function updateLeadIds() {
		$start = 1;
		$working = true;
		$count = 0;
		while ($working) {
			$retval = $this->getOnePage($start, $start+19);
			$start += count($retval);
			if (empty($retval)) 
				$working = false;
			else foreach($retval as $lead) {
				if ($this->updateOneLead($lead))
					$count++;
			}
		}
		return new Out('OK', "Updated $count lead ids");
	}

	public function getLead($leadId, $fromLeads) {
		header("Content-type: application/xml");
		$token=zohoAPIKey;
		$url = "https://crm.zoho.com/crm/private/xml/".($fromLeads ? "Leads" : "Contacts")."/getRecordById";
		$param= "authtoken=".$token."&scope=crmapi"."&id=$leadId";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		$result = curl_exec($ch);
		curl_close($ch);

		$xml = new \XMLReader();
		if (($result = $xml->xml($result)) == false)
		{
			$this->log("Failed to open xml file: $file\n", 6);
			continue;
		}

		//$xml = simplexml_load_string($buffer);
		$retval = $this->parseXml($xml, $fromLeads);
		$xml->close();
		return $retval;
	}

	public function updateLead($leadId, $fromLeads = true) {
		$lead = $this->getLead($leadId, $fromLeads);
		if (empty($lead)) {
			$this->log("updateLead - Unable to get record for lead: $leadId");
			return new Out('fail', "Unable to get record for lead: $leadId");
		}

		if ($this->updateOneLead($lead[0], $fromLeads)) {
			$this->log("updateLead - Updated lead: $leadId");
			return new Out('OK', "Updated lead: $leadId");
		}
		else {
			$this->log("updateLead - Unable to update record for lead: $leadId");
			return new Out('fail', "Unable to update record for lead: $leadId");
		}
	}

	protected function parseXml($xml, $fromLeads = true) {
		$records = [];
		$type = $fromLeads ? 'Leads' : 'Contacts';
		while($xml->read()) {
			if ($xml->nodeType == \XMLReader::ELEMENT && $xml->name == $type) {
				$doc = new \DOMDocument("1.0", "UTF-8");
                $newRecord = simplexml_import_dom($doc->importNode($xml->expand(),true));
                $this->log("parseXml - doing $type");
                $lead = $this->parseLeads($newRecord, $records);
                if (!empty($lead)) {
                	$this->log("parseXml - got a ".($fromLeads ? 'lead' : 'contact'));
                }
                	
			}
		}
		$this->log("parseXml has ".count($records)." ".($fromLeads ? 'lead' : 'contact'));
		return $records;
	}

	/**  Sample xml dump:
[20150720.22:55:10] element count: 29, name: row
[20150720.22:55:10] parseElement sub: FL, value: 1584064000000088572
[20150720.22:55:10] parseElement - attr:val, value:LEADID
[20150720.22:55:10] parseElement sub: FL, value: 1584064000000084003
[20150720.22:55:10] parseElement - attr:val, value:SMOWNERID
[20150720.22:55:10] parseElement sub: FL, value: Lifestyled Listings
[20150720.22:55:10] parseElement - attr:val, value:Lead Owner
[20150720.22:55:10] parseElement sub: FL, value: Real Estate 1- Bill Scott
[20150720.22:55:10] parseElement - attr:val, value:Company
[20150720.22:55:10] parseElement sub: FL, value: Sandy
[20150720.22:55:10] parseElement - attr:val, value:First Name
[20150720.22:55:10] parseElement sub: FL, value: Scott
[20150720.22:55:10] parseElement - attr:val, value:Last Name
[20150720.22:55:10] parseElement sub: FL, value: sandy@midlandtexashomes.com
[20150720.22:55:10] parseElement - attr:val, value:Email
[20150720.22:55:10] parseElement sub: FL, value: 4326821111
[20150720.22:55:10] parseElement - attr:val, value:Phone
[20150720.22:55:10] parseElement sub: FL, value: Contact in Future
[20150720.22:55:10] parseElement - attr:val, value:Lead Status
[20150720.22:55:10] parseElement sub: FL, value: 0
[20150720.22:55:10] parseElement - attr:val, value:No of Employees
[20150720.22:55:10] parseElement sub: FL, value: 0
[20150720.22:55:10] parseElement - attr:val, value:Annual Revenue
[20150720.22:55:10] parseElement sub: FL, value: 1584064000000084003
[20150720.22:55:10] parseElement - attr:val, value:SMCREATORID
[20150720.22:55:10] parseElement sub: FL, value: Lifestyled Listings
[20150720.22:55:10] parseElement - attr:val, value:Created By
[20150720.22:55:10] parseElement sub: FL, value: 1584064000000084003
[20150720.22:55:10] parseElement - attr:val, value:MODIFIEDBY
[20150720.22:55:10] parseElement sub: FL, value: Lifestyled Listings
[20150720.22:55:10] parseElement - attr:val, value:Modified By
[20150720.22:55:10] parseElement sub: FL, value: 2015-07-20 13:42:49
[20150720.22:55:10] parseElement - attr:val, value:Created Time
[20150720.22:55:10] parseElement sub: FL, value: 2015-07-20 15:54:39
[20150720.22:55:10] parseElement - attr:val, value:Modified Time
[20150720.22:55:10] parseElement sub: FL, value: 3300 N. A St  Bldg 7 Ste 100
[20150720.22:55:10] parseElement - attr:val, value:Street
[20150720.22:55:10] parseElement sub: FL, value: Midland
[20150720.22:55:10] parseElement - attr:val, value:City
[20150720.22:55:10] parseElement sub: FL, value: TX
[20150720.22:55:10] parseElement - attr:val, value:State
[20150720.22:55:10] parseElement sub: FL, value: false
[20150720.22:55:10] parseElement - attr:val, value:Email Opt Out
[20150720.22:55:10] parseElement sub: FL, value: 2015-07-20 15:54:39
[20150720.22:55:10] parseElement - attr:val, value:Last Activity Time
[20150720.22:55:10] parseElement sub: FL, value: Midland
[20150720.22:55:10] parseElement - attr:val, value:Target City
[20150720.22:55:10] parseElement sub: FL, value: TX
[20150720.22:55:10] parseElement - attr:val, value:Target State
[20150720.22:55:10] parseElement sub: FL, value: Golf
[20150720.22:55:10] parseElement - attr:val, value:LifeStyle 1
[20150720.22:55:10] parseElement sub: FL, value: Estate
[20150720.22:55:10] parseElement - attr:val, value:LifeStyle 2
[20150720.22:55:10] parseElement sub: FL, value: Condo
[20150720.22:55:10] parseElement - attr:val, value:LifeStyle 3
[20150720.22:55:10] parseElement sub: FL, value: 2015-07-20 19:12:02
[20150720.22:55:10] parseElement - attr:val, value:Date
[20150720.22:55:10] parseElement sub: FL, value: 17458
[20150720.22:55:10] parseElement - attr:val, value:Seller ID
	**/

	protected function parseLeads($record, &$records) {
		try {
			$rec = null;
			foreach($record->children() as $element)
			{
				$name = $element->getName();
				$count = $element->count();
				$this->log( "element count: $count, name: $name" );
				if ($count) {
					$rec = new \stdClass();
					foreach($element->children() as $sub)
					{
						$field = '';
						$entry = $sub->__toString();
						foreach($sub->attributes() as $attr=>$value) 
							$field = $value;

						str_replace(" ", "_", $value);
						$rec->$value = $entry;
						$this->log("parseElement - $field: $value", 2);

						// $subName = $sub->getName();						
						// $this->log( "parseElement sub: $subName, value: ".$sub->__toString() );
						// foreach($sub->attributes() as $attr=>$value) 
						// 	$this->log("parseElement - attr:$attr, value:$value");
						
					} // end foreach($sub)
					$records[] = $rec;
				}
			}
			return $records;
		}
		catch( \Exception $e) {
			parseException($e);
		}
	}

/** Sample insertino data:
XMLDATA sample:
<Leads>
<row no="1">
<FL val="Lead Source">Web Download</FL>
<FL val="Company">Your Company</FL>
<FL val="First Name">Hannah</FL>
<FL val="Last Name">Smith</FL>
<FL val="Email">testing@testing.com</FL>
<FL val="Title">Manager</FL>
<FL val="Phone">1234567890</FL>
<FL val="Home Phone">0987654321</FL>
<FL val="Other Phone">1212211212</FL>
<FL val="Fax">02927272626</FL>
<FL val="Mobile">292827622</FL>
</row>
</Leads>
**/

	public function updateNewReservations() {
		$reservations = $this->getClass('Reservations')->get((object)['where'=>['lead_id'=>0]]);
		$count = 0;
		if (!empty($reservations)) foreach($reservations as $res) {
			$seller = null;
			if (!empty($res->seller_id))
				$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$res->seller_id]]);
			$out = $this->processOneZohoCrm(true, $res, empty($seller) ? null : $seller[0] );
			if ($out)
				$count++;
			unset($res, $seller);
		}
		return new Out('OK', "Updated $count reservations");
	}

	public function updateOneExistingReservation($id) {
		$seller = null;
		$retval = false;
		$msg = '';
		$res = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$id]]);
		if (!empty($res)) {
			$res = array_pop($res);
			$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$res->seller_id]]);
			if (!empty($seller)) {
				$retval = $this->processOneZohoCrm(false, $res, empty($seller) ? null : $seller[0] );
				if (!$retval)
					$msg = "Failed to process one zoho crm for sellerID:$id";
				else
					$msg = "Updated CRM for sellerId:$id";
			}
			else
				$msg = "Failed to find seller with id:$res->seller_id";
		}
		else
			$msg = "Failed to find reservation with sellerId:$id";
		return new Out($retval ? 'OK' : 'fail', $msg);
	}

	public function updateExistingReservations() {
		// $reservations = $this->getClass('Reservations')->get((object)['not'=>['lead_id'=>0]]);
		$reservations = $this->getClass('Reservations')->get();
		$count = 0;
		$this->log("updateExistingReservations has ".count($reservations)." to process.");
		if (!empty($reservations)) foreach($reservations as $res) {
			$seller = null;
			if (!empty($res->seller_id))
				$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$res->seller_id]]);
			$out = $this->processOneZohoCrm(false, $res, empty($seller) ? null : $seller[0] );
			if ($out)
				$count++;
			unset($res, $seller);
		}
		return new Out('OK', "Updated $count reservations");
	}

	public function createNewZohoCrm($seller_id) {
		$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$seller_id]]);
		if (empty($seller)) 
			throw new \Exception("Failed to get seller with id:$seller_id");
		
		$seller = array_pop($seller);
		$bre = null;
		foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_BRE)
				$bre = $meta;
		}

		if (!$bre)
			throw new \Exception("Failed to find BRE data for seller with id:$seller_id");

		$blankCity = ''; $blankState = '';
		$blank = '';
		$xml = '<Leads><row no="1">';
		$lead_id = 0;
		$didBre = false;
		$didPD = false;

		$xml.= '<FL val="Lead Source">Lifestyled Listings</FL>';
		$xml.= '<FL val="Lead Status">Not Contacted</FL>';

		foreach($this->zohoInsertTags as $key=>$tag) {
			switch($key) {
				case 'first_name':
				case 'last_name':
					$xml .= '<FL val="'.$tag.'">'.formatName($seller->$key).'</FL>';
					break;
				case 'email':
				case 'phone':
				case 'street_address':
				case 'city':
				case 'website':
				case 'mobile':
				case 'zip':
				case 'country':
				case 'about':
				case 'company':
				case 'bre':
				case 'about':
					if (isset($seller->$key) && !empty($seller->$key))
						$xml .= '<FL val="'.$tag.'">'.$seller->$key.'</FL>';
					break;
				case 'seller_id':
					$xml .= '<FL val="'.$tag.'">'.$seller->id.'</FL>';
					break;
				case 'state':
					$xml .= '<FL val="'.$tag.'">'.$bre->state.'</FL>';
					break;
				// case 'portal':
				// case 'notes':
				default: // do nothing
					break;
			}

		}
		$xml .= "</row></Leads>";

		$retval = $this->insertToCrm($xml);
		if (!$retval) 
			throw new \Exception("Failed to create new CRM for seller with id:$seller_id");

		$xml = new \XMLReader();
		if (($result = $xml->xml($retval)) == false)
		{
			$this->log("createNewZohoCrm - Failed to open xml result: $retval\n", 6);
			throw new \Exception("Failed to create xml reader for seller with id:$seller_id");
		}
		while($xml->read()) {
			if ($xml->nodeType == \XMLReader::ELEMENT && $xml->name == "result") {
				$doc = new \DOMDocument("1.0", "UTF-8");
                $newResult = simplexml_import_dom($doc->importNode($xml->expand(),true));
                // $this->log("parseXml - doing Leads");
                $result = $this->parseResult($newResult);
                if (!empty($result)) {
                	$this->log("createNewZohoCrm - seller with id:$seller_id, got a result:$result->message");
                	if (isset($result->Id) &&
            			!empty($result->Id)) {
                		$meta = new \stdClass();
                		$meta->action = SELLER_ZOHO_LEAD_ID;
                		$meta->lead_id = $result->Id;
                		$seller->meta[] = $meta;
            			$this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
            													  'fields'=>['meta'=>$seller->meta]]]);
            			$this->log("createNewZohoCrm - updated LEADID: $result->Id for seller with id: $seller->id");
            			unset($meta);
            		}
            		else
            			$this->log("createNewZohoCrm - did not update LEADID for seller with id: $seller->id, result has ".(!isset($result->Id) ? "NOT SET" : (empty($result->Id) ? 'EMPTY' : $result->Id))." for Id");

                	if (strpos($result->message, "success") === false) 
                		throw new \Exception("rcreateNewZohoCrm - result for seller with id:$seller_id: $result->message");
                } 
                else
                	throw new \Exception("createNewZohoCrm - Failed to parse xml result for seller with id:$seller_id");
			}
			else if ($xml->nodeType == \XMLReader::ELEMENT)
				$this->log("createNewZohoCrm - node: $xml->name");
		}
	}

	public function processOneZohoCrm($newReservation, $reservation, $seller) {
		$blankCity = ''; $blankState = '';
		$blank = '';
		$fromLeads = ($reservation->flags & RESERVATION_LEADID_FROM_CONTACTS) == 0;
		$xml = $fromLeads ? '<Leads><row no="1">' : '<Contacts><row no="1">';
		$lead_id = 0;
		$didBre = false;
		$didPD = false;
		if (!$newReservation)
			if (!empty($reservation->lead_id))
				$lead_id = $reservation->lead_id;
			else {
				$newReservation = true;
				$this->log("processOneZohoCrm had newReservation as false but there was no LEADID, treating reservation:$reservation->id as new");
			}

		if (!$fromLeads) {
			$xml .= '<FL val="Seller ID">'.$reservation->seller_id.'</FL>';
			$xml .= "</row></Contacts>";
			$retval = $this->updateToCrm($xml, $reservation->lead_id, $fromLeads);
			$retval = $this->processResult($retval, $reservation);
			$this->log("processOneZohoCrm - for Contacts - result: ".($retval ? 'true' : 'false'));
			return $retval;
		}

		$current = null;
		$currentBre = null;
		$currentProfileData = null;
		$currentExtra = null;
		$currentNotes = null;
		if (!empty($reservation->meta)) foreach($reservation->meta as $meta)
			if ($current == null &&
				$meta->action == ORDER_AGENT_MATCH)
				$current = $meta; // get first one
			elseif ($meta->action == SELLER_BRE)
				$currentBre = $meta;
			elseif ($meta->action == SELLER_PROFILE_DATA)
				$currentProfileData = $meta;
			elseif ($meta->action == RESERVATION_EXTRA_SELLER_DATA)
				$currentExtra = $meta;
			elseif ($meta->action == SELLER_NOTES_FROM_RESERVATION)
				$currentNotes = $meta;

		$xml.= '<FL val="Lead Source">Lifestyled Listings</FL>';

		if ($newReservation)  
			$xml.= '<FL val="Lead Status">Not Contacted</FL>';

		foreach($this->zohoInsertTags as $key=>$tag) {
			switch($key) {
				case 'first_name':
				case 'last_name':
					$xml .= '<FL val="'.$tag.'">'.formatName($reservation->$key).'</FL>';
					break;
				case 'email':
				case 'phone':
				case 'seller_id':
					$xml .= '<FL val="'.$tag.'">'.$reservation->$key.'</FL>';
					break;
				case 'street_address':
					$value = !empty($seller) && !empty($seller->street_address) ? ( (($pos = strpos($seller->street_address, ",")) !== false) ? $seller->street_address = substr($seller->street_address, 0, $pos) : $seller->street_address) : '';
					if (empty($value) &&
						$currentExtra &&
						!empty($currentExtra->$key))
						$value = $currentExtra->$key;
					$xml .= '<FL val="'.$tag.'">'.$value.'</FL>';
					break;
				case 'city':
					$value = !empty($seller) && !empty($seller->city) ? ( (($pos = strpos($seller->city, ",")) !== false) ? $seller->city = str_replace(",", "-", $seller->city) : $seller->city) : '';
					if (empty($value) &&
						$currentExtra &&
						!empty($currentExtra->$key))
						$value = $currentExtra->$key;
					$xml .= '<FL val="'.$tag.'">'.$value.'</FL>';
					break;
				case 'website':
				case 'mobile':
				case 'state':
				case 'zip':
				case 'country':
				case 'about':
					$value = isset($seller->$key) && !empty($seller->$key) ? $seller->$key : '';
					if (empty($value) &&
						$currentExtra &&
						!empty($currentExtra->$key))
						if ($key == 'zip') {
							if (gettype($currentExtra->$key) == 'string')
								$value = str_pad($currentExtra->$key, 5, '0', STR_PAD_LEFT);
							elseif (gettype($currentExtra->$key) == 'integer')
								$Rvalue = str_pad( number_format($currentExtra->$key), 5, '0', STR_PAD_LEFT);
						}
						$value = $currentExtra->$key;
					$xml .= '<FL val="'.$tag.'">'.$value.'</FL>';
					break;
				case 'company':
					if (!empty($seller)) {
						if (!empty($seller->company) && ($pos = strpos($seller->company, "&")) !== false ) {
							$frontCh = $seller->company[$pos-1] == ' ' ? '' : ' ';
							$backCh = $seller->company[$pos+1] == ' ' ? '' : ' ';
							$replacement = $frontCh.'and'.$backCh;
							$seller->company = str_replace("&", $replacement, $seller->company);
						}
					}
					$value = !empty($seller) && !empty($seller->company) ? ( (($pos = strpos($seller->company, ",")) !== false) ? $seller->company = str_replace(",", " ", $seller->company) : $seller->company) : 'Unknown';
					if ((empty($value) ||
						$value == 'Unknown') &&
						$currentExtra &&
						!empty($currentExtra->$key))
						$value = $currentExtra->$key;
					$xml .= '<FL val="'.$tag.'">'.$value.'</FL>';
					break;
				case 'portal':
					$xml .= '<FL val="'.$tag.'">'.(!empty($reservation->$key) ? $reservation->$key : 'N/A').'</FL>';
					$gotOne = false;
					if (!empty($reservation->meta)) foreach($reservation->meta as $meta) {
						if ($gotOne)
							break;
						$meta = (object)$meta;
						if ($meta->action == ORDER_AGENT_MATCH) {
							$nth = 0;
							if (!empty($meta->item)) foreach($meta->item as $item) {
								if ($nth == zohoMAXLifestyles)
									continue;
								$item = (object)$item;
								if (!$nth) {
									$loc = explode(",", $item->locationStr);
									$xml .= '<FL val="Target City">'.$loc[0].'</FL>';
									$xml .= '<FL val="Target State">'.(count($loc) == 2 ? $loc[1] : "").'</FL>';
								}
								$nth++;
								$xml .= '<FL val="LifeStyle '.$nth.'">'.$item->specialtyStr.'</FL>';	
								if (isset($item->desc) && !empty($item->desc))
									$xml .='<FL val="L'.$nth.' Description">'.removeslashes($item->desc).'</FL>';					
								unset($item);
							}
							// for($nth++ ; $nth <= 3; $nth++)
							// 	$xml .= '<FL val="LifeStyle '.$nth.'">'.$blank.'</FL>';
							$gotOne = true;
							break;
						}

						if ($meta->action == SELLER_BRE) {
							$xml .= '<FL val="Real Estate License number">'.$meta->BRE.'</FL>';	
							$didBre = true;
						}

						if ($meta->action == SELLER_PROFILE_DATA) {
							$didPD = true;
							if ( (isset($meta->first_name) && !empty($meta->first_name)) ||
								 (isset($meta->last_name) && !empty($meta->last_name)) ) {
								$name = (isset($meta->first_name) && !empty($meta->first_name) ? $meta->first_name. ' ' : '').(isset($meta->last_name) && !empty($meta->last_name) ? $meta->last_namen: '');
								$xml .= '<FL val="Preferred Display Name">'.$name.'</FL>';
							}
							if ( (isset($meta->year) && !empty($meta->year)) ) {
								$xml .= '<FL val="Year Licensed">'.$meta->year.'</FL>';
							}
							if ( (isset($meta->inArea) && !empty($meta->inArea)) ) {
								$xml .= '<FL val="Years lived in Service Area">'.(intval(date("Y", time() + ($this->timezone_adjust*3600))) - intval($meta->inArea) + 1).'</FL>';
							}
							if ( (isset($meta->sold) && !empty($meta->sold)) ) {
								$xml .= '<FL val="Transactions/year">'.$meta->sold.'</FL>';
							}
							if ( (isset($meta->contact_email) && !empty($meta->contact_email)) ) {
								$xml .= '<FL val="Secondary Email">'.$meta->contact_email.'</FL>';
							}
						}
						unset($meta);
					} // foreach($reservation->meta as $meta) 

					if ( (!$didBre ||
						  !$didPD) &&
						$seller &&
						$seller->meta) {
						foreach($seller->meta as $meta) {
							if ($meta->action == SELLER_BRE &&
								!$didBre) {
								$xml .= '<FL val="Real Estate License number">'.$meta->BRE.'</FL>';	
								$didBre = true;
							}

							if ($meta->action == SELLER_PROFILE_DATA &&
								!$didPD) {
								$didPD = true;
								if ( (isset($meta->first_name) && !empty($meta->first_name)) ||
									 (isset($meta->last_name) && !empty($meta->last_name)) ) {
									$name = (isset($meta->first_name) && !empty($meta->first_name) ? $meta->first_name. ' ' : '').(isset($meta->last_name) && !empty($meta->last_name) ? $meta->last_namen: '');
									$xml .= '<FL val="Preferred Display Name">'.$name.'</FL>';
								}
								if ( (isset($meta->year) && !empty($meta->year)) ) {
									$xml .= '<FL val="Year Licensed">'.$meta->year.'</FL>';
								}
								if ( (isset($meta->inArea) && !empty($meta->inArea)) ) {
									$xml .= '<FL val="Years lived in Service Area">'.(intval(date("Y", time() + ($this->timezone_adjust*3600))) - intval($meta->inArea) + 1).'</FL>';
								}
								if ( (isset($meta->sold) && !empty($meta->sold)) ) {
									$xml .= '<FL val="Transactions/year">'.$meta->sold.'</FL>';
								}
							}
							unset($meta);
						}
					}
					
					$xml .= '<FL val="Date">'.$reservation->added.'</FL>';
					break;
				case 'notes':
					$count = 0;
					$desc = '';
					if (!empty($reservation->meta)) 
						foreach($reservation->meta as $meta) {
							$meta = (object)$meta;
							if ($meta->action == ORDER_AGENT_MATCH) {
								$count++;
								if ($count == 1) // first one was done already
									continue;

								$nth = 0;
								if (!empty($meta->item)) foreach($meta->item as $item) {
									$item = (object)$item;
									$desc .= "$item->locationStr - $item->specialtyStr\n";												
									unset($item);
								}
							}
							unset($meta);
						}
					if (!empty($desc) &&
						$currentNotes == null)
						$xml .= '<FL val="Description">'.$desc.'</FL>';
					break;
				default: // do nothing
					break;
			}

		}
		$xml .= "</row></Leads>";

		$this->log("processOneZohoCrm - newReservation:$newReservation for $reservation->first_name $reservation->last_name, reservation id:$reservation->id, sellerId:".(empty($seller) ? '0' : $seller->id).", email:".(isset($reservation->email) && !empty($reservation->email) ? $reservation->email : "N/A").", didBre:".($didBre ? 'yes' : 'no').", didPD:".($didPD ? 'yes' : 'no'));
		if ($newReservation) {
			$retval = $this->insertToCrm($xml);
			if (!$retval) {
				$updatedMaybe = $this->getClass('Reservations')->get((object)['like'=>['email'=>$reservation->email]]);
				if (!empty($updatedMaybe) &&
					$updatedMaybe[0]->lead_id != $reservation->lead_id &&
					!empty($updatedMaybe[0]->lead_id)) {
					$this->log("processOneZohoCrm - insertToCrm() failed, but the reservation:$reservation->id now has a new LEADID: {$updatedMaybe[0]->lead_id}, so trying updateToCrm()");
					$retval = $this->updateToCrm($xml, $updatedMaybe[0]->lead_id);
				}
			}
		}
		else
			$retval = $this->updateToCrm($xml, $lead_id);

		$result = $this->processResult($retval, $reservation);
		$this->log("processOneZohoCrm - newReservation:$newReservation - result: ".($result ? 'true' : 'false'));
		if ($newReservation &&
			!$result &&
			gettype($retval) == 'object' &&
			isset($retval->Id) &&
            !empty($retval->Id) &&
            isset($retval->message) &&
            strpos($retval->message, 'already exists') !== false &&
			!empty($reservation->lead_id) &&
			$reservation->lead_id == $retval->Id) {
			$this->log("processOneZohoCrm - retrying as an existing CRM entry for sellerId:$seller->id, $seller->first_name $seller->last_name, lead_id:$retval->Id");
			$result = $this->processOneZohoCrm(false, $reservation, $seller);
		}

		return $result;
	}
	
	protected function processResult(&$result, &$reservation) {
		$xml = new \XMLReader();
		if (empty($result)) {
			$this->log("processResult - Failed as result if empty\n", 6);
			return false;
		}
		if (($result = $xml->xml($result)) == false)
		{
			$this->log("processResult - Failed to open xml result: $result\n", 6);
			return false;
		}
		while($xml->read()) {
			if ($xml->nodeType == \XMLReader::ELEMENT && $xml->name == "result") {
				$doc = new \DOMDocument("1.0", "UTF-8");
                $newResult = simplexml_import_dom($doc->importNode($xml->expand(),true));
                // $this->log("parseXml - doing Leads");
                $result = $this->parseResult($newResult);
                if (!empty($result)) {
                	$this->log("processResult - for reservation_id: $reservation->id, got a result:$result->message");
                	if (isset($result->Id) &&
            			!empty($result->Id) &&
            			$result->Id != $reservation->lead_id) {
            			$this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservation->id],
            															'fields'=>['lead_id'=>$result->Id]]]);
            			$this->log("processResult - updated LEADID: $result->Id for reservation_id: $reservation->id");
            			$reservation->lead_id = $result->Id;
            		}
            		else
            			$this->log("processResult - did not update LEADID for reservation_id: $reservation->id, result has ".(!isset($result->Id) ? "NOT SET" : (empty($result->Id) ? 'EMPTY' : $result->Id))." for Id");

                	if (strpos($result->message, "success") !== false) 
                		return true;
                	else
                		return false;
                } 
                else
                	$this->log("processResult - got empty from parseResult()");	
			}
			else if ($xml->nodeType == \XMLReader::ELEMENT)
				$this->log("processResult - node: $xml->name");
		}
		$this->log("processResult - failed, result:$result");
		return false;
	}

	protected function parseResult($record) {
		try {
			$rec = new \stdClass();
			foreach($record->children() as $element)
			{
				$name = $element->getName();
				$count = $element->count();
				// $this->log( "element count: $count, name: $name" );
				if ($count) {
					
					foreach($element->children() as $sub)
					{
						$field = '';
						$entry = $sub->__toString();
						foreach($sub->attributes() as $attr=>$value) 
							$field = $value;

						str_replace(" ", "_", $value);
						$rec->$value = $entry;
						// $this->log("parseResult - $field: $value");

						// $subName = $sub->getName();						
						// $this->log( "parseElement sub: $subName, value: ".$sub->__toString() );
						// foreach($sub->attributes() as $attr=>$value) 
						// 	$this->log("parseElement - attr:$attr, value:$value");
						
					} // end foreach($sub)
					return $rec;
				}
				else {
					$rec->$name = $element->__toString();
				}
			}
			return $rec;
		}
		catch( \Exception $e) {
			parseException($e);
		}
	}

	protected function insertToCrm($xml) {
		$this->log("insertToCrm entered xml: $xml");
		header("Content-type: application/xml");
		$token=zohoAPIKey;
		$url = "https://crm.zoho.com/crm/private/xml/Leads/insertRecords";
		$param= "newFormat=2&authtoken=".$token."&scope=crmapi"."&xmlData=$xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		$result = curl_exec($ch);
		curl_close($ch);

		$this->log("insertToCrm - result: $result");
		return $result;
	}

	protected function updateToCrm($xml, $leadId, $fromLeads = true) {
		$this->log("updateToCrm entered for $leadId, $xml");
		header("Content-type: application/xml");
		$token=zohoAPIKey;
		$url = "https://crm.zoho.com/crm/private/xml/".($fromLeads ? "Leads" : "Contacts")."/updateRecords";
		$param= "newFormat=2&authtoken=".$token."&scope=crmapi&id=$leadId"."&xmlData=$xml";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		$result = curl_exec($ch);
		curl_close($ch);

		$this->log("updateToCrm - result: $result");
		return $result;
	}
}