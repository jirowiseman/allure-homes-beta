<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');

class BingLocation {
	private $APIkey = array(
		'AglTWbaCQ4LHV3rtxmH3T4KxMZjLYcBmNIwAbUvZAn02EJzNWtr2X3WCXvQUhHNc', // alluretechnologies@gmail
		);

	public function __construct() {
		require_once(__DIR__.'/Options.class.php'); $this->Options = new Options();
		$this->apiIndex = 0;
		$this->timeToReset = 20; // in hours, this will guarantee at least one day cycle between feed parsing
		$this->startTime = microtime(true);
		$this->lockfile = null;

		$this->lockfile = fopen(__DIR__.'/_logs/geoCode.lock', 'c'); 

		$q = new \stdClass();
		$q->where = array('opt' => 'BingGeoCodeKeyIndex');
		$x = $this->Options->get($q);
		if (!empty($x)) {
			$data = json_decode($x[0]->value);
			$time = microtime(true);
			// time in seconds in float format.
			// 3600 secs/hr
			if ( ($time - floatval($data->time)) > (3600 * $this->timeToReset) )
				$this->apiIndex = 0;
			else
				$this->apiIndex = intval($data->index);
		}
		$this->recordIndex();
	}

	public function __destruct() {
		if ($this->lockfile);
			fclose($this->lockfile);
	}

	private function recordIndex() {
		//require_once(__DIR__.'/Options.class.php'); $options = new Options();
		$q = new \stdClass();
		$q->where = array('opt' => 'BingGeoCodeKeyIndex');
		$x = $this->Options->get($q);
		if (is_array($x)) // delete it
			$this->Options->delete($q->where);

		$data = array('index'=>(string)$this->apiIndex,
					  'time'=>microtime(true));
		$data = array('opt'=>'BingGeoCodeKeyIndex',
					  'value' => json_encode($data));
		$this->Options->add((object)$data);
	}

	protected function runGeocode($call, &$geocoded) {
		flock($this->lockfile, LOCK_EX);
		$lastTime = $this->Options->get((object)['where'=>['opt'=>'GeoCodeTimeStamp']]);
		$lastTime = json_decode($lastTime[0]->value);
		$curTime = microtime(true);
		$done = false;
		if ( ($curTime-$lastTime->time) < 1.0 ) {
			if ( $lastTime->count < 5 ) {
				$geocoded = json_decode(file_get_contents($call));
				$lastTime->count++;
				$this->Options->set([(object)['where'=>['opt'=>'GeoCodeTimeStamp'],
										'fields'=>['value'=>json_encode($lastTime)]]]);
				$done = true;
			}
			else
				time_nanosleep(0, ($curTime-$lastTime->time+0.01)*1000000000 );
		}
		
		if (!$done) {
			$geocoded = json_decode(file_get_contents($call));
			$lastTime->count = 1;
			$lastTime->time = microtime(true);
			$this->Options->set([(object)['where'=>['opt'=>'GeoCodeTimeStamp'],
									'fields'=>['value'=>json_encode($lastTime)]]]);
		}

		flock($this->lockfile, LOCK_UN);
	}

	

	/**
	 * Returns Geocoded result from Google Maps API
	 * @param  [string] func_get_arg(0) [address string to parse]
	 * @return [AH\Out] [status of Google response, result dataset]
	 */
	public function geocodeAddress(){ // requires address
		if (func_num_args() !== 2) return new Out(0, 'geocodeAddress: invalid number of arguments'); else {
			$address = func_get_arg(0);
			$nthTry = func_get_arg(1);
			$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
			if ($nthTry == count($this->APIkey))
				return new Out(0,  (object)['statusDescription'=>'OVER_QUERY_LIMIT'] );
			// try the call without a key...
			$call = 'http://dev.virtualearth.net/REST/v1/Locations/'.$address.'?o=json';
			// if ($this->apiIndex)
			$call .= '&key='.$this->APIkey[$this->apiIndex];
			$geocoded = null;
			$this->runGeocode($call, $geocoded);
			
			// if (property_exists($geocoded, 'status') &&
			// 	$geocoded->status == 'OVER_QUERY_LIMIT') {
			// 	if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
			// 		$this->apiIndex = 0; // reset
			// 	else 
			// 		$this->apiIndex++;
			// 	$this->recordIndex();
			// 	return $this->geocodeAddress(func_get_arg(0), $nthTry+1);
			// }

			if ($geocoded && $geocoded->statusDescription == 'OK') return new Out('OK', $geocoded->resourceSets[0]->resources);
			return new Out(0, $geocoded);
		}
	}

	// public function geocodeLatLng(){ // requires lat lng
	// 	if (func_num_args() !== 2) return new Out(0, 'geocodeAddress: invalid number of arguments'); else {
	// 		$address = str_replace(' ', '', func_get_arg(0));
	// 		$address = str_replace('+', ',', $address);
	// 		$nthTry = func_get_arg(1);
	// 		$nthTry = is_string($nthTry) ? intval($nthTry) : $nthTry;
	// 		if ($nthTry == count($this->APIkey))
	// 			return new Out(0,  (object)['status'=>'OVER_QUERY_LIMIT'] );
	// 		$call = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$address.'&sensor=false';
	// 		if ($this->apiIndex)
	// 			'&key='.$this->APIkey[$this->apiIndex];
	// 		$geocoded = null;
	// 		$this->runGeocode($call, $geocoded);
			
	// 		if ($geocoded->status == 'OVER_QUERY_LIMIT') {
	// 			if ($this->apiIndex == (count($this->APIkey)-1) ) // out of keys
	// 				$this->apiIndex = 0;  // reset
	// 			else 
	// 				$this->apiIndex++;
	// 			$this->recordIndex();
	// 			return $this->geocodeLatLng(func_get_arg(0), $nthTry+1);
	// 		}

	// 		if ($geocoded->status == 'OK') return new Out('OK', $geocoded->results[0]);
	// 		return new Out(0, $geocoded);
	// 	}
	// }
	/**
	 * Returns the distance between points as a float rounded to 2 decimal places
	 * @param  [float] $lat1 [latitude input 1]
	 * @param  [float] $lon1 [longitude input 1]
	 * @param  [float] $lat2 [latitude input 2]
	 * @param  [float] $lon2 [longitude input 2]
	 * @return [float]       [distance between points]
	 */
	public function getDistance($lat1, $lng1, $lat2, $lng2){
	  $theta = $lng1 - $lng2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    return round($dist * 60 * 1.1515, 2);
	}
	// public function nearbySearch($lat, $lng, $query = null){
	// 	$nearby = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$lat.','.$lng.'&rankby=distance&keyword='.$query.'&sensor=false&key='.$this->APIkey));
	// 	if ($nearby->status == 'OK'){
	// 		$results = [];
	// 		foreach ($nearby->results as $point){
	// 			$x = (object) [
	// 				'address'=> $point->vicinity,
	// 				'distance' => $this->getDistance(floatval($lat), floatval($lng), $point->geometry->location->lat, $point->geometry->location->lng),
	// 				'name' => $point->name,
	// 				'lat' => $point->geometry->location->lat,
	// 				'lng' => $point->geometry->location->lng,
	// 			];
	// 			// if ( is_array($point->photos) && count($point->photos) > 0){
	// 			// 	$x->photo = $point->photos[0]->photo_reference;
	// 			// 	// $x->photo = $this->APIurl.'photo?maxheight=71&photoreference='.$photoref.'&sensor=false&key='.$this->APIkey;
	// 			// 	// $x->photo = file_get_contents($this->APIurl.'photo?maxheight=71&photoreference='.$photoref.'&sensor=false&key='.$this->APIkey);
	// 			// }
	// 			// $x->icon = $results[$i]['nearby'][$j]->icon;
	// 			// $x->place_id = $results[$i]['nearby'][$j]->id;
	// 			// $x->place_ref = $results[$i]['nearby'][$j]->reference;
	// 			$results[] = $x;
	// 		}
	// 		return $results;
	// 	} else return $nearby;
	// }
}