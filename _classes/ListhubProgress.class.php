<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class ListhubProgress extends Base {
	public function __construct($logIt = 0){
		parent::__construct($logIt);
		// $this->log = new Log(__DIR__.'/_logs/QA.log');
	}

	public function forceEnableLog() {
		if (!$this->log) {
			$this->log = new Log(__DIR__."/_logs/".$this->table.".log");
			$this->log->add( get_class($this).':forceEnableLog()' );
			// $this->log->options->buffer_length = -1;
			$this->saved_log_to_file = $this->log_to_file;
			$this->log_to_file = 1;
		}
	}

	public function forceDisableLog() {
		if ($this->log != null) {
			$this->log->add( get_class($this).':forceDisableLog()' . "\r\n" );
			// fclose($this->lockfile);
			$this->flush();
			if (!$this->saved_log_to_file)
				$this->log = null;
		}
	}
}