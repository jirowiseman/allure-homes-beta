<?php
namespace AH;

require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
require_once(__DIR__.'/../../../../wp-config.php');
require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/formatting.php');
require_once(__DIR__.'/../../../../wp-includes/link-template.php');
require_once(__DIR__.'/../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/DynamicQuiz.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

define('MAX_LISTING_INDEXED', 12);
define('MAX_CITY_INDEXED', 3);
define('REDO_QUIZ_TIME', 1); //3600*24*5);

class SiteMapper extends DynamicQuiz {
	private $siteMapIndexHeader = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
	private $siteMapIndexEnd = '</sitemapindex>';
	// private $siteMapIndexHomeSite = '<sitemap><loc>'.get_home_url().'/sitemap.xml</loc><lastmod>2016-04-01T00:00:00+00:00</lastmod></sitemap>';
	private $sitemapHeader = '<?xml version="1.0" encoding="UTF-8" ?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
	private $sitemapEnd = '</urlset>';
	private $siteMaps = [];

	public function __construct($logIt = 0) {
		parent::__construct(); // don't pass $logIt, as Dynamicquiz will make a $log then
		if ($logIt)
			$this->log = new Log(__DIR__."/_logs/siteMapper.log");
		$this->siteMapIndexHomeSite = '<sitemap><loc>'.get_home_url().'/sitemap.xml</loc><lastmod>2016-04-01T00:00:00+00:00</lastmod></sitemap>';
	 	$this->siteMapIndex = $this->siteMapIndexHeader.$this->siteMapIndexHomeSite;
	 	// comes from DynamicQuiz->Controller now
		// $this->timezone_adjust = -7;

		$Quiz = $this->getClass('Quiz');
		$Quiz->setOption('allowPartialQuizResults', 0);
		$Quiz->setOption('max_cities', 10);
	}

	public function getParmeters($seo, $thisPage) {
		global $usStates;
		$url = explode('/', $seo);
		while(count($url))
			if ( empty($url[0]) )
				$url = array_slice($url, 1);
			else
				break;

		array_walk($url, function(&$ele, $i) { $ele = rawurldecode($ele); });
		$len = count($url) ;
		$isForListing = strpos($thisPage, 'listing') !== false;
		if ($len== 1) {// then nationwide search
			return "Nation wide search for a lifestyle, ".$url[0].($isForListing ? ', has found a listing in ' : '');
		}

		$param = new \stdClass();
		$param->optName = $url[0];
		$param->len = $len;
		$param->state = '';
		$param->cityName = '';
		$param->simpleStateQuiz = false;
		$param->nationwideQuiz = false;
		// $opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
		$param->minPrice = $this->minPrice;
		$this->log("minPrice - ".$param->minPrice);
		$param->fields = [];
		$value = 0;

		// is next word a state?
		$this->parseQuizParameters($url, 1, $param);

		return $param;
	}

	public function getDescription($seo, $thisPage) {
		global $usStates;
		$url = explode('/', $seo);
		while(count($url))
			if ( empty($url[0]) )
				$url = array_slice($url, 1);
			else
				break;

		array_walk($url, function(&$ele, $i) { $ele = rawurldecode($ele); });
		$len = count($url) ;
		$isForListing = strpos($thisPage, 'listing') !== false;
		if ($len== 1) {// then nationwide search
			return "Nation wide search for a lifestyle, ".$url[0].($isForListing ? ', has found a listing in ' : '');
		}

		$param = new \stdClass();
		$param->optName = $url[0];
		$param->len = $len;
		$param->state = '';
		$param->cityName = '';
		$param->simpleStateQuiz = false;
		$param->nationwideQuiz = false;
		// $opt = $Options->get((object)['where'=>['opt'=>'NO_ASK_ALLOW_MIN_PRICE']]);
		$param->minPrice = $this->minPrice;
		$this->log("minPrice - ".$param->minPrice);
		$param->fields = [];
		$value = 0;

		// is next word a state?
		$this->parseQuizParameters($url, 1, $param);
		$state = $param->state;
		foreach($usStates as $long=>$short)
			if ( $short == $state) {
				$state = $long;
				break;
			}
		if ( !empty($param->state) )
			$seoString = "State wide search in ".$state;
		else
			$seoString = "Nation wide search";
		if ( isset($param->fields['location']) ) {
			$seoString .= " with a radius of ".$param->fields['distance']." miles around";
			$seoString .= (count($param->fields['location']) == 1 ? ' the city of ' : ' cities including ').$param->cityName;

		}

		if ( isset($param->fields['price']))
			$seoString .= " for listings priced up to $".$param->fields['price'][1];
		if ( isset($param->fields['homeOptions']))
			$seoString .= " and ".$param->fields['homeOptions']['beds'].'beds';
		$seoString .= '.  ';

		if ( $isForListing )
			$seoString .= "This search for";
		else
			$seoString .= "This quiz features";

		$seoString .= " a lifestyle, ".$url[0];
		if ( isset($param->fields['tags'])) {
			$seoString .= ", along with these additional attributes: ";
			$index = 0;
			foreach($param->fields['tags'] as $tag) {
				$seoString .= ($index ? ", " : '').$tag->tag;
				$index++;
			}
		}

		if ($isForListing)
			$seoString .= ', has found a listing in ';
		else
			$seoString .= '.';

		return $seoString;
	}

	public function generateSiteMaps() {
		global $siteMapKeys;
		foreach($siteMapKeys as $key) {
			$optName = "Quiz_$key";
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>$optName]]);
			if (empty($opt))
				continue;

			$this->addToSitemapIndex($key);
			$quizSet = json_decode($opt[0]->value, true);
			$this->createSiteMap($optName, $quizSet, $key);

		}
		$this->siteMapIndex .= $this->siteMapIndexEnd;

		return $this->writeOutSiteMaps();
	}

	protected function writeOutSiteMaps() {
		$count = 0;
		$indexFile = fopen(__DIR__."/../../../../sitemap-index.xml", 'w');
		if ($indexFile) {
			fwrite($indexFile, $this->siteMapIndex);
			fclose($indexFile);
			$count++;
		}
		if (!empty($this->siteMaps)) foreach($this->siteMaps as $name=>$map) {
			$sitemap = fopen(__DIR__."/../../../../$name", 'w');
			if ($sitemap) {
				fwrite($sitemap, $map);
				fclose($sitemap);
				$count++;
			}
		}

		return $count;
	}

	protected function addToSitemapIndex($key) {
		$key = 'site-'.str_replace(' ','-', $key);
		$index = '<sitemap><loc>'.get_home_url().'/'.$key.'.xml</loc><lastmod>'.date(DATE_W3C).'</lastmod></sitemap>';
		$this->siteMapIndex .= $index;
	}

	protected function addOneListing($listing, $optName, $key, &$entry) {
		$listing = is_array($listing) ? array_pop($listing) : $listing;
		if (!isset($listing) ||
			empty($listing) ||
			!isset($listing->id) ||
			empty($listing->id))
			return;

		$listing->street_address = str_replace('/','-',$listing->street_address);
		$listing_key = $this->parseQuizKey($optName, $key);
		$listing_entry = get_home_url().'/'.$listing_key."/listing/{$listing->id}/";
		$listing_entry.= !empty($listing->street_address) ? rawurlencode($listing->street_address) : '';
		$listing_entry.= !empty($listing->city) ? rawurlencode(','.$listing->city) : '';
		$listing_entry.= !empty($listing->state) ? rawurlencode(' '.$listing->state) : '';
		$listing_entry.= !empty($listing->zip) ? rawurlencode(' '.$listing->zip) : '';
		$entry.= '<url><loc>'.$listing_entry.'</loc>';
		$timestamp = date(DATE_W3C, strtotime($listing->added));
		$entry.= '<lastmod>'.$timestamp.'</lastmod>';
		$entry.= '<changefreq>weekly</changefreq>';
		$entry.= '<priority>0.7</priority>';
		$entry.= '</url>';
	}

	protected function createSiteMap($optName, $quizSet, $activityType) {
		$siteMapName = 'site-'.str_replace(' ','-', $activityType).'.xml';
		$siteMap = $this->sitemapHeader;
		foreach($quizSet as $key=>$quiz_id) {
			$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quiz_id]]);
			if (empty($activity)) {
				$this->log("createSiteMap - failed to find quiz:$quiz_id for $key");
				continue;
			}


			$activity = array_pop($activity);
			// $data = array_pop($activity->data);
			// $results = array_pop($data->results);
			$diffTime = (time() - strtotime($activity->updated));
			if ( $diffTime > REDO_QUIZ_TIME) {
				$hours = $diffTime / 3600;
				$this->log("createSiteMap - running parseAndReturnResultSet for $optName, $key, quizId:$activity->id, it has been ".number_format($hours, 2)." hrs since last update.");
				// $new_results = $results;
				// if ( isset($new_results->city_sorted) ) unset($new_results->city_sorted);
				// if ( isset($new_results->listings) ) unset($new_results->listings);
				// if ( isset($new_results->cities) ) unset($new_results->cities);
				// if ( isset($new_results->total_listings_found) ) unset($new_results->total_listings_found);
				// if ( isset($new_results->admitted) ) unset($new_results->admitted);
				// if ( isset($new_results->allowPartialQuizResults) ) unset($new_results->allowPartialQuizResults);
				// if ( isset($new_results->startCityCountUsedForListings) ) unset($new_results->startCityCountUsedForListings);
				// if ( isset($new_results->currentMaxCityCountUsedForListings) ) unset($new_results->currentMaxCityCountUsedForListings);

				// $results = $this->getClass('Quiz')->parseAndReturnResultSet( $new_results );
				$results = $this->getClass('Quiz')->parseQuizActions(null, $quiz_id); 
				if ($results) {
					$activity = $this->getClass('QuizActivity')->get((object)['where'=>['id'=>$quiz_id]]);
					$activity = $activity[0];
					$data = $activity->data[0];
					$results = $data->results[0];
				}

				// unset($new_results);

				// $data->results[] = $results;
				// $activity->data[] = $data;
				// $query = (object) array( 'where'=>array('id'=>$activity->id) );
				// $query->fields = array('data'=>$activity->data,
				// 			   		   'status'=>'done');
				// $retval = $this->getClass('QuizActivity')->set(array($query)) ? true : false;
				// $this->log("createSiteMap - ran parseAndReturnResultSet, resetting QuizActivity:$activity->id was ".($retval ? "success" : "failure"));
				$activity->updated = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			}

			$entry = '<url><loc>'.get_home_url().'/'.$this->parseQuizKey($optName, $key).'</loc>';
			$timestamp = date(DATE_W3C, strtotime($activity->updated));
			$entry.= '<lastmod>'.$timestamp.'</lastmod>';
			$entry.= '<changefreq>weekly</changefreq>';
			$entry.= '<priority>0.6</priority>';
			$entry.= '</url>';


			unset($data);
			$extracted = 0;
			if (isset($results->city_sorted)) $results->city_sorted = (array) $results->city_sorted;
			if (isset($results->listings)) $results->listings = (array) $results->listings;

			if (!empty($results->city_sorted)) {
				$size = count($results->city_sorted);
				$this->log("getting listings for $optName, $key, with $size cities");
				// if ($size == 1) {// get it all from the one city
				// 	$this->log("getting listings for $optName, $key");
				// 	try {
				// 		$city = $results->city_sorted[0]; 
				// 		if (gettype($city) != 'object') {
				// 			$this->log("city is not an object! it is:".gettype($city));
				// 			$city = new \stdClass();
				// 		}
				// 		if (!isset($city->listings)) {
				// 			$this->log("city->listings is not set in $optName, $key");
				// 			$city->listings = [];
				// 		}
				// 		else
				// 			$city->listings = (array)$city->listings;
				// 		$this->log("$optName, $key has only one city, with ".(empty($city->listings) ? '0' : count($city->listings)));
				// 		if ( !empty($city->listings) ) foreach($city->listings as $listing) {
				// 			$data = $this->getClass("Listings")->get((object)['where'=>['id'=>$listing->id],
				// 																  'what'=>['street_address','city','state','zip','added','id']]);
				// 			if (empty($data)) {
				// 				$this->log("createSiteMap - failed to get listing: $listing->id");
				// 				unset($listing);
				// 				continue;
				// 			}
				// 			$this->addOneListing($data, $optName, $key, $entry);
				// 			unset($listing, $data);
				// 			$extracted++;
				// 			if ($extracted == MAX_LISTING_INDEXED)
				// 				break;
				// 		}
				// 		$this->log("extracted:$extracted listings for $optName, $key");
				// 	}
				// 	catch( \Excpetion $e) {
				// 		$this->log("Caught exception for $key: ".$e->getMessage());
				// 	}
				// }
				// else {
					$maxPerCity = MAX_LISTING_INDEXED / ($size > MAX_CITY_INDEXED ? MAX_CITY_INDEXED : $size);
					foreach($results->city_sorted as $i=>$city) {
						if (empty($city)) {
							$this->log("city:$i is empty!");
							continue;
						}
						$perCity = 0;
						foreach($city->listings as $listing) {
							$data = $this->getClass("Listings")->get((object)['where'=>['id'=>$listing->id],
																			  'what'=>['street_address','city','state','zip','added','id']]);
							if (empty($data)) {
								$this->log("createSiteMap - failed to get listing: $listing->id");
								unset($listing);
								continue;
							}
							$this->addOneListing($data, $optName, $key, $entry);
							unset($listing, $data);
							$extracted++;
							if ($extracted == MAX_LISTING_INDEXED)
								break;
							$perCity++;
							if ($perCity == $maxPerCity)
								break;
						}
						if ($extracted == MAX_LISTING_INDEXED)
							break;
					}
					$this->log("extracted:$extracted listings for $optName, $key");
				// }
			}
			elseif (!empty($results->listings)) {
				foreach($results->listings as $listing) {
					$data = $this->getClass("Listings")->get((object)['where'=>['id'=>$listing->id],
																		  'what'=>['street_address','added']]);
					if (empty($data)) {
						$this->log("createSiteMap - failed to get listing: $listing->id");
						unset($listing);
						continue;
					}
					$this->addOneListing($data, $optName, $key, $entry);
					unset($listing, $data);
					$extracted++;
					if ($extracted == MAX_LISTING_INDEXED)
						break;
				}
				$this->log("extracted:$extracted listings for $optName, $key");
			}
			else
				$this->log("createSiteMap - no listings in results for quiz:$quiz_id for $optName, $key");

			$siteMap .= $entry;
			unset($results, $activity);
		}
		$siteMap .= $this->sitemapEnd;
		$this->siteMaps["$siteMapName"] = $siteMap;
	}

}