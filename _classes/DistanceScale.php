<?php
namespace AH;

 // lng, lat arrays per 5 degrees starting from 5, 15, 25, etc...
global $DistanceScale;
$DistanceScale = array (
	array(68.91, 68.71), // 5
	array(66.83, 68.75), // 15
	array(62.73, 68.83), // 25
	array(56.72, 68.94), // 35
	array(48.99, 69.05), // 45
	array(39.76, 69.17), // 55
	array(29.31, 69.28), // 65
	array(17.96, 69.36), // 75
	array(6.05, 69.40)   // 85
);

global $DistanceScaleNautical;
$DistanceScaleNautical = array (
	array(59.88, 59.71), // 5
	array(58.07, 59.75), // 15
	array(54.51, 59.81), // 25
	array(49.29, 59.90), // 35
	array(42.57, 60.01), // 45
	array(34.55, 60.11), // 55
	array(25.27, 60.20), // 65
	array(15.61, 60.27), // 75
	array(5.26, 60.31)   // 85
);