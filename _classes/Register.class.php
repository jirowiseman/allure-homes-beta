<?php
namespace AH;

// Load WP components, no themes
if ( ! defined( 'WP_USE_THEMES' ) )
	define('WP_USE_THEMES', false);
// define('WP_ADMIN', true);2

require_once(__DIR__.'/_Controller.class.php');
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/IPv6.php');



require_once(__DIR__.'/../../../../wp-load.php');
require_once(__DIR__.'/../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'../../../../../wp-includes/l10n.php' );
// require_once(__DIR__.'../../../../../wp-includes/class-wp-walker.php');
// require_once(__DIR__.'../../../../../wp-includes/capabilities.php');
// require_once(__DIR__.'../../../../../wp-includes/user.php');
require_once(__DIR__.'/../../../../wp-admin/includes/admin.php' );
	
/** Load Ajax Handlers for WordPress Core */
require_once( __DIR__.'/../../../../wp-admin/includes/ajax-actions.php' );

global $wpdb;
global $wp_better_emails;


if ( ! defined( 'INVITATION_LIST' ) )
	define( 'INVITATION_LIST', 'invitationList');

define( 'USER_WELCOME', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com! Thank you for signing up with us, we’re excited to work day in and day out to help you find the best locations and homes for your lifestyle. We’ve been hard at work building this amazing new way to find properties, but we recognize we have a long way to go. Right now we are freshly in “live” mode, which means you are among the first few thousand people to use our site, that also means you are likely to run into occasional bugs or glitches. We appreciate your patience as we continue to improve.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>Happy house hunting, we hope you find what you’re looking for!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>In case you forget, here is your login information
user name: %user_name%
password: %password%</p>");

define( 'AGENT_WELCOME', "<p>Hi %first_name%,<br/>
You have successfully registered as your agent account on Lifestyled Listings. With this account you can:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&#183;enhance your listings with full resolution images
&nbsp;&nbsp;&nbsp;&nbsp;&#183;optimize your listings for our search engine
&nbsp;&nbsp;&nbsp;&nbsp;&#183;promote your expertise and personal qualities to interested buyers<br/>
In case you forget, here is your login information
user name: %user_name%
password: %password%<br/>
An email with helpful tips on getting started is coming soon but if you have any questions in the meantime please don’t hesitate to ask!<br/>
**If you are signing up for our Lifestyled Agent Program, you are not yet done. Please continue as prompted, or if you are picking up where you left off simply log in to your agent dashboard and click on Lifestyled Agent to finish the 2 easy steps.
You will receive confirmation email that your Lifestyled Agent profile is live once you have added your profile picture and 300-word+ description about why you are an expert in that lifestyle or property type.<br/>
Again, a big welcome from Team Lifestyled!"
);

define( 'SALES_REGISTRATION', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com! Our sales team has registered you as an agent, and setting up your purchased products.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>We are striving to improve your experience and expanding our network daily!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>In case you forget, here is your login information.  You can change your password once you log in and go to the Sellers Dashboard->My Profile.  To get to the Seller's Dashboard, either click on 'Agent Dashboard' on the nav bar or the person icon in the upper right corner.
user name: %user_name%
password: %password%</p>");

define( 'SALES_UPGRADE_REGISTRATION', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com! Our sales team has registered you as an agent, and setting up your purchased products.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>We are striving to improve your experience and expanding our network daily!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>You can change your password once you log in and go to the Sellers Dashboard->My Profile.  To get to the Seller's Dashboard, either click on 'Agent Dashboard' on the nav bar or the person icon in the upper right corner.
</p>");

define( 'CAMPAIGN_PORTAL_REGISTRATION', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com and thank you for registering on our site as a Portal Agent.
<br/>To get the most out of your portal, share it with your friends, family and clients and have them spread within their inner circles too.  You can go to your Seller's Dashboard->Tools, and click on share buttons to put it on your social media accounts.  You can also cut-and-paste an email signature image to put into your email (Chrome browser works best, iOS won't work).
<br/>In your Stats page, you can see the activities of your portal users, such as where and when they've logged in, what searches they have done and what listings they have viewed.
<br/>Also, please upload a photo of yourself from the Seller's Dashboard->My Profile page, so your portal can show it.  Filling in more information in your profile will also improve how you are presented when your Agent page is viewed by a client.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>We are striving to improve your experience and expanding our network daily!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>In case you forget, here is your login information.  You can change your password once you log in and go to the Sellers Dashboard->My Profile.  To get to the Seller's Dashboard, either click on 'Agent Dashboard' on the nav bar or the person icon in the upper right corner.
user name: %user_name%
password: %password%</p>");

define( 'CAMPAIGN_LIFESTYLE_REGISTRATION', "<p>Hi %first_name%,<br/>
Welcome to LifestyledListings.com and thank you for registering on our site as a Lifestyled Agent.
<br/>Please remember to update your photo and complete your description about the expertise you have chosen so you're account can be activated.  Filling in more information in your profile, Seller's Dashboard->My Profile page, will also improve how you are presented when your Agent page is viewed by a client.
<br/>If you would like to see something improved or a feature added we’d love to hear from you. Please use our Feedback tab to send us an email with any feedback or suggestions.
<br/>We are striving to improve your experience and expanding our network daily!
<br/>Sincerely,</p>
<p>The Lifestyled Team
<br/>In case you forget, here is your login information.  You can change your password once you log in and go to the Sellers Dashboard->My Profile.  To get to the Seller's Dashboard, either click on 'Agent Dashboard' on the nav bar or the person icon in the upper right corner.
user name: %user_name%
password: %password%</p>");
// $insertUser = "INSERT INTO $wpdb->users (user_login, user_pass, user_email, user_url) VALUES ('$user_login', '$user_pass', '$user_email', '$user_url')";
// $wpdb->query( $insertUser );

// $user_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM $wpdb->users WHERE user_email = %s", $user_email));

// $my_user = new WP_User( $user_id );
// $my_user->set_role( "Contributor" );

function set_html_content_type() {
	return 'text/html';
}

$feedbackType = [
	FB_GENERAL => 'Client Message',
	FB_IMPROVEMENT => 'Possible Improvement',
	FB_SUGGESTION => 'Client Suggestion',
	FB_QUESTION => 'Client Question',
	FB_BUGREPORT => 'Bug Report'
];

class Register extends Controller {
	protected $cassie = '';
	// comes from Controller now
	// protected $timezone_adjust = 7;

	public function __construct($logIt = 0){
		parent::__construct();
		set_time_limit(0);
		require_once(__DIR__.'/Options.class.php');
		$o = $this->getClass('Options');
		$x = $o->get((object)array('where'=>array('opt'=>'RegisterClassDebugLevel')));
		$this->log_to_file = empty($logIt) ? (empty($x) ? 0 : intval($x[0]->value)) : $logIt;
		$this->log = $this->log_to_file ? new Log(__DIR__.'/_logs/.registration.log') : null;

		$x = $o->get((object)array('where'=>array('opt'=>'RegisterTimeDurationAllowNoRegSessionSecs')));
		$this->allowNoRegDuration = empty($x) ? 172800 : intval($x[0]->value);
		$this->fromAdmin = false;
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}

	public function verifyInvite($code, $option, &$ic = null) {
		$userMode = 0;
		$ic = null;
		$this->log("verifyInvite - code:$code, option:$option");
		$retval = $this->checkInvite($code, $userMode, $ic);
		if ($retval !== true) // failed
			return $retval;

		if ($ic->flags & IC_FEATURE_LIFETIME) // unlimited features
			return new Out('OK', "Approved");

		$gotOne = $ic->flags & (intval($option) | IC_FEATURE_LIFETIME);
		if ($ic->number_usage == 0)
			return new Out('fail', "Code is used up");
		return new Out($gotOne ? 'OK' : 'fail', $gotOne ? "Approved" : "Code in invalid for this purpose.");
	}

	public function replyTo($magic) {
		$et = $this->getClass("EmailTracker");
		$x = $et->get((object)['where'=>['track_id'=>$magic]]);
		if (empty($x)) {
			$h = '<div id="primary" class="content-area"> 
					<main id="main" class="site-main" role="main">

						<section class="error-404 not-found">
							<header class="page-header">
								<h1 class="page-title">Oops! That page can&rsquo;t be found.</h1>
							</header><!-- .page-header -->

							<div class="page-content">
								<p>It looks like nothing was found at this location. Sorry from Lifestyled Listings.</p>

							</div><!-- .page-content -->
						</section><!-- .error-404 -->

					</main><!-- .site-main -->
				</div><!-- .content-area -->';
			echo $h;
			die();
		}

		$x = $x[0];

		header('Location: '.get_site_url()."/listing/T".$x->id.'-'.$x->listing_id);
		header("Connection: close");
		die;
		// echo '<script type="text/javascript">window.location="'.get_home_url()."/listing/T".$x->id.'-'.$x->listing_id.'";</script>';

	}

	public function contact($data, $primary, $listing) {
		global $feedbackType;
		$email = $primary->email;

		if (!empty($primary->meta)) foreach($primary->meta as $info) {
			if ($info->action == SELLER_PROFILE_DATA) {
				if (!empty($info->contact_email))
					$email = $info->contact_email;
				break;
			}
		}
		$listingAddr = !empty($listing) ? $listing->street_address.", ".$listing->state : '';
		$sellerName = $primary->first_name." ".$primary->last_name;
		$primaryEmail = $email;
		$email = $data->flags == MESSAGE_EMAIL_CLIENT_AS_AGENT ? $data->user_email : $email;
		
		$this->log("entered contact with data:".print_r($data, true)); //.", feedbackType:".print_r($feedbackType, true));

		// magic number
		$magic = microtime().$listingAddr.$email;
		$md5 = '';
		$et = $this->getClass("EmailTracker");
		$etData = [];
		$blogname = '';
		$blogdescription = '';
		$tellPortalAgent = false;
		$tellListingAgent = false;
		$listingAgent = isset($data->listing_agent) && !empty($data->listing_agent) ? 
						( !empty($temp = $this->getClass('Sellers')->get((object)['where'=>['id'=>$data->listing_agent]])) ? array_pop($temp) : null ) : null;

		if ($data->flags == MESSAGE_EMAIL_AGENT || // contact Lifestyled Agent or portal owner
			$data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL || // contact Lifestyled Agent when in portal
			$data->flags == MESSAGE_EMAIL_LISTING_AGENT ||
			$data->flags == MESSAGE_EMAIL_LISTING_OFFICE ||
			$data->flags == MESSAGE_EMAIL_CLIENT_AS_AGENT ||
			$data->flags == MESSAGE_EMAIL_PORTAL_AGENT) {
			$md5 = hash("sha256", $magic);
			$etData['track_id'] = $md5;					
			$etData['agent_id'] = $primary->id; // user id and not author_id since it can be from an unregistered agent
			$etData['agent_email'] = $email;
			$etData['user_id'] = $data->user_id; // this is always from wordpress user table, i.e. WP_User->ID
			$etData['user_email'] = $data->user_email;
			$etData['first'] = $data->first;
			if (isset($data->last)) $etData['last'] = $data->last;
			$etData['listing_id'] = !empty($listing) ? $listing->id : 0;
			$etData['type'] = $data->type; // general, improvement, suggestion, question, PROMO_FIRST, PROMO_THIRD, IMPROVE_LISTING, etc..
			$etData['flags'] = $data->flags; // MESSAGE_FEEDBACK, MESSAGE_EMAIL_AGENT, MESSAGE_REPLY_TO_AGENT, MESSAGE_REPLY_TO_USER, MESSAGE_SELLER, MESSAGE_EMAIL_CLIENT_AS_AGENT, etc..
			$etData['meta'] = [(object)['action'=>ET_CONTENT,
										'content'=>$data->comment]];
			$x = $et->add($etData);	

			$ip = $this->getClass('SessionMgr')->userIP();
			if ($ip != "::1")
				$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
			else
				$details = (object)['city'=>'local', 'region'=>'state'];

			$subject = ($data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_LISTING_OFFICE ? (!empty($listing) ? "Inquiry about " : "Message from client") : ($data->flags == MESSAGE_EMAIL_CLIENT_AS_AGENT ? "A great listing at " : "Potential referral on ")).$listingAddr;
			if ($data->flags != MESSAGE_EMAIL_CLIENT_AS_AGENT) {
				$msg = "<p>Hello ".$sellerName.",</p>";
				$msg .= "<p>You have a message from one of our users (".$data->first." ".$data->last." - sent from:{$details->city}, {$details->region})".(!empty($listing) ? (" about ".($data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_LISTING_OFFICE ? 'your': 'a')." listing at ".$listingAddr) : ".");
				$msg .= "&nbsp;&nbsp;The email entered for this user is $data->user_email.</p>";

				if (!empty($data->agent_id2)) { // this is the portal agent's id
					if ($data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_LISTING_OFFICE || $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL) { // tell them about the portal agent
						$portal = $this->getClass('Sellers')->get((object)['where'=>['id'=>$data->agent_id2]]);
						if (!empty($portal)) {
							$tellPortalAgent = true;
							$phone = isset($portal[0]->phone) && !empty($portal[0]->phone) ? fixPhone($portal[0]->phone) : '';
							$mobile = isset($portal[0]->mobile) && !empty($portal[0]->mobile) ? fixPhone($portal[0]->mobile) : '';
							$msg .= "<p>This buyer was referred to you by our portal agent, ".$portal[0]->first_name.' '.$portal[0]->last_name.".";
							if (!empty($phone)) {
								$msg .= " This agent can be reached at phone:$phone";
								if (!empty($mobile))
									$msg .= " or mobile:$mobile";
							}
							elseif (!empty($mobile))
								$msg .= " This agent can be reached at $mobile";
							$msg.= ", for additional information.";
							$msg .= "</p>";
							if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL) {
								$msg .= "<p>Please abide by the Agent Match Terms and Conditions, about the referal fees for the portal agent upon a successful transaction, to which you have agreed to.</p>";
								$tellListingAgent = true;
							}
						}
					}
					else { // $data->flags == MESSAGE_EMAIL_AGENT (Portal Agent!), so inform the listing agent!
						$tellListingAgent = true;
					}
				}
				elseif (!empty($data->listing_agent) &&
						$data->flags != MESSAGE_EMAIL_LISTING_AGENT) 
					$tellListingAgent = true;
				
				$msg .= '<div style="width: 80%"><p><strong>'.$data->comment.'</strong></p></div>';

				$quizTags = !empty($_COOKIE['quizTags']) ? str_replace("\\", '', $_COOKIE['quizTags']) : null;
				if ($quizTags &&
					($data->flags== MESSAGE_EMAIL_AGENT || // contact Lifestyled Agent or portal owner
					 $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL || // contact Lifestyled Agent when in portal
					 $data->flags == MESSAGE_EMAIL_PORTAL_AGENT) ) {
					$quizTags = json_decode($quizTags);
					$msg .= "<p>This buyer was interested in these home and area attributes:  ";
					$first = true;
					foreach($quizTags as $tag) {
						$first ? $first = false : $msg .= ", ";
						$msg .= $tag->tag;
					}
					$msg .= "</p>";
				}

				$blogname = ($data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_LISTING_OFFICE ? 'Inquiry about your listing!': $feedbackType[$data->type].'!');
				$blogdescription = "Please reply to this client when you have a moment!";
			}
			else {
				$msg = "<p>Hello ".$data->first.(!empty($data->last) ? ' '.$data->last : '').",</p>";
				$msg .= "<p>".$data->comment."</p>";
				$blogname = 'Great Listing!';
				$blogdescription = "I think you'll love this one!";
			}
		}
		elseif ($data->flags == MESSAGE_REPLY_TO_USER ||
				$data->flags == MESSAGE_REPLY_TO_AGENT) {
			$row = $et->get((object)['where'=>['id'=>$data->etId]]);
			if (!empty($row)) {
				$row = array_pop($row);
				$metas = $row->meta;
				$md5 = $row->track_id;
				$metas[] = (object)['action'=>ET_REPLY,
									'content'=>$data->comment];
				$x = $et->set([(object)['where'=>['id'=>$data->etId],
										'fields'=>['flags'=>$data->flags,
													'meta'=>$metas]]]);
				$this->log("contact - etId:{$data->etId}, mode:{$data->flags}, agent:{$row->agent_email}, user:{$data->user_email}, update was ".(empty($x) ? 'a failure' : 'successful'));
			}	
			$email = $data->flags == MESSAGE_REPLY_TO_USER ? $data->user_email : $row->agent_email;
			$subject = "Reply from ".($data->flags == MESSAGE_REPLY_TO_USER ? "agent" : "user")." about: ".$listingAddr;
			$msg = "<p>Hello ".($data->flags == MESSAGE_REPLY_TO_USER ? $data->first : $sellerName).",</p>";
			$msg .= "<p>You have a reply from the ".($data->flags == MESSAGE_REPLY_TO_USER ? "agent" : "user").", (".($data->flags == MESSAGE_REPLY_TO_USER ? $sellerName : $data->first)."), about the listing at ".$listingAddr."</p>";
			$msg .= '<div style="width: 80%"><p><strong>"'.$data->comment.'"</strong></p></div>';	
			$blogname = "Reply from the ".($data->flags == MESSAGE_REPLY_TO_USER ? "agent" : "user")."!";
			$blogdescription = "If you have other questions, please ask!";	
		}
		else
			return new Out('fail', "Unknown flags type: ".$data->flags);

								

		// $url = $_COOKIE['tp'].'/_pages/ajax-register.php?query=agent_reply&id='.$data->user_id.'&email='.$data->email;
		$url = get_template_directory_uri().'/_pages/ajax-register.php?query=agent-reply&data='.$md5;
		$msg .= '<p>Click on this <a href="'.$url.'" target="_blank">link</a> to reply.</p>';
		if (!empty($listing)) {
			$url2 = get_home_url().'/listing/LC-'.$listing->id;
			$msg .= '<p>Click <a href="'.$url2.'" target="_blank">here</a> to view the listing.</p>';
		}
		$msg .= '<p>Sincerely,</p>';
		if ($data->flags != MESSAGE_EMAIL_CLIENT_AS_AGENT) 
			$msg .= '<p>The Lifestyled Team</p>';
		else
			$msg .= '<p>'.$sellerName.', Lifestyled Portal Agent<br/>'."$primary->company<br/>$primary->city, $primary->state".'</p>';

	  	$success = $this->sendMail( $email, $subject, $msg, 
	  								$blogname,
	  								$blogdescription); 

	  	$this->sendMail( SUPPORT_EMAIL, $subject, $msg, 
	  	  								'Agent contact!',
	  									"Email Tracker id: $md5");

	  	if ($tellPortalAgent)
	  		$this->tellSecondaryAgent($data, $primary, $listing);
	  	if ($tellListingAgent)
	  		$this->tellListingAgent($data, $primary, $listing);
	  	if ($data->flags == MESSAGE_EMAIL_CLIENT_AS_AGENT)
	  		$this->sendMail( $primaryEmail, $subject, $msg,
	  						 'Copy',
	  						 "You sent this to your client at $email");

	  	return new Out($success, $success ? "Sent out contact email to $sellerName: ".$email : "Failed to send email to $sellerName: ".$email);
	}

	protected function tellSecondaryAgent($data, $primary, $listing) {
		$agent = $this->getClass('Sellers')->get((object)['where'=>['id'=>$data->agent_id2]]);
		$listingAddr = $listing->street_address.", ".$listing->state;
		$primaryName = $primary->first_name." ".$primary->last_name;
		$targetingPortalAgent = $data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_LISTING_OFFICE || $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL;
		$subject = ($targetingPortalAgent ? "Potential referral on " : "Activity on ").$listingAddr;
		$agentName = $agent[0]->first_name.' '.$agent[0]->last_name;
		$quizTags = !empty($_COOKIE['quizTags']) ? str_replace("\\", '', $_COOKIE['quizTags']) : null;
		if ($quizTags)
			$quizTags = json_decode($quizTags);
		//$userInfo = get_userdata($data->user_id);
		$url2 = get_home_url().'/listing/'.$listing->id;
		$email = $agent[0]->email;
		if ($agent[0]->meta) foreach($agent[0]->meta as $meta) {
			if ($meta->action == SELLER_PROFILE_DATA &&
				!empty($meta->contact_email) )
				$email = $meta->contact_email;
		}

		if (empty($email)) {
			$this->log("tellSecondaryAgent - seller id:".$agent[0]->id." has an empty email, cannot send out notification to $agentName");
			return;
		}

		$primaryEmail = $primary->email;
		if (!empty($primary->meta)) foreach($primary->meta as $info) {
			if ($info->action == SELLER_PROFILE_DATA) {
				if (!empty($info->contact_email))
					$primaryEmail = $info->contact_email;
				break;
			}
		}
		$phone = isset($primary->phone) && !empty($primary->phone) ? fixPhone($primary->phone) : '';
		$mobile = isset($primary->mobile) && !empty($primary->mobile) ? fixPhone($primary->mobile) : '';

		$ip = $this->getClass('SessionMgr')->userIP();
		if ($ip != "::1")
			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
	 	else
			$details = (object)['city'=>'local', 'region'=>'state'];

		$username = $data->first.(isset($data->last) ? " ".$data->last : '');
		$user_email = $data->user_email;

		$msg = "<p>Hello ".$agentName."</p>";
		$msg .= "<p>One of our users, ".$username." (sent from:{$details->city}, {$details->region}),";
		if ($targetingPortalAgent) {
			if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL) {
				$msg .= " who came through your portal, contacted one of our AgentMatch agents: $primaryName with email: $primaryEmail";
				if (!empty($phone))
					$msg .= ", phone: $phone";
				if (!empty($mobile))
					$msg .= ", mobile: $mobile";
				$msg .= ", about this ";
			}
			else
				$msg .= " who came through your portal, contacted a listing agent from our site about this ";
		}
		else
			$msg .= " contacted one of our portal agents about your ";
		$msg .= '<a href="'.$url2.'" target="_blank">listing</a>.  ';
		if ($targetingPortalAgent) {
			if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL) 
				$msg .= "Please contact the agent immediately to arrange your referral agreement, as has been agreed upon per our Terms and Conditions of AgentMatch.</p>";
			else
				$msg .= "Please contact the agent from this listing immediately to arrange your referral agreement.</p>";
			$msg .= "<p>The buyer's email is ".$user_email.".</p>";
			if (!empty($quizTags)) {
				$msg .= "<p>This buyer was interested in these home and area attributes:  ";
				$first = true;
				foreach($quizTags as $tag) {
					$first ? $first = false : $msg .= ", ";
					$msg .= $tag->tag;
				}
				$msg .= "</p>";
			}
		}
		else {
			$msg .= "	Thought you might want to know.  Please contact the portal agent at $primaryEmail";
			if (!empty($phone))
				$msg .= ", or phone: $phone";
			if (!empty($mobile))
				$msg .= ", or mobile: $mobile";
			$msg .= ".</p>";
		}

		$msg .= '<p>Sincerely,</p>';
		$msg .= '<p>The Lifestyled Team</p>';

		$this->sendMail( $email, $subject, $msg, 
						($targetingPortalAgent ? 'Possible referral!' : "Listing activity!"),
						($targetingPortalAgent ? "Please check the listing to see if you can help the client!" : "Please contact the portal agent for additional information!") );
	}

	protected function tellListingAgent($data, $primary, $listing) {
		// if $data->flags == MESSAGE_EMAIL_LISTING_AGENT, then we need to exit now, since $tellListingAgent should have been false
		if ($data->flags == MESSAGE_EMAIL_LISTING_AGENT) {
			$this->log("tellListingAgent was called with data->flags == MESSAGE_EMAIL_LISTING_AGENT");
			return;
		}

		if (!isset($data->listing_agent) || empty($data->listing_agent) ) {
			$this->log("tellListingAgent was called without proper listing_agent id");
			return;
		}
		// who is the $primary here?
		// if $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL or MESSAGE_EMAIL_AGENT, then it's the lifestyled agent
		// so if $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL, then $data->agent_id2 is the portal agent id
		$agent = $this->getClass('Sellers')->get((object)['where'=>['id'=>$data->listing_agent]]);
		if (empty($agent)) {
			$this->log("tellListingAgent could not find listing agent using $data->listing_agent");
			return;
		}

		$portalAgent = null;
		if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL) {
			if ( isset($data->agent_id2) ) {
				$portalAgent = $this->getClass('Sellers')->get((object)['where'=>['id'=>$data->agent_id2]]);
				if (empty($portalAgent)) {
					$this->log("tellListingAgent failed to find portal agent with id:$data->agent_id2");
					return;
				}
				$portalAgent = array_pop($portalAgent);
			}
			else {
				$this->log("tellListingAgent does not have portal agent id in agent_id2 fields");
				return;
			}
		}
		elseif ($data->flags == MESSAGE_EMAIL_PORTAL_AGENT)
			$portalAgent = $primary;

		$listingAddr = $listing->street_address.", ".$listing->state;
		$primaryName = $primary->first_name." ".$primary->last_name;
		// $targetingPortalAgent = $data->flags == MESSAGE_EMAIL_LISTING_AGENT || $data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL;
		$subject = "Activity on ".$listingAddr;
		$agentName = $agent[0]->first_name.' '.$agent[0]->last_name;
		
		$url2 = get_home_url().'/listing/'.$listing->id;
		$email = $agent[0]->email;
		if ($agent[0]->meta) foreach($agent[0]->meta as $meta) {
			if ($meta->action == SELLER_PROFILE_DATA &&
				!empty($meta->contact_email) )
				$email = $meta->contact_email;
		}

		if (empty($email)) {
			$this->log("tellListingAgent - listing agent with id:".$agent[0]->id." has an empty email, cannot send out notification to $agentName");
			return;
		}

		if (!empty($portalAgent)) {
			$portalAgentEmail = $portalAgent->email;
			if (!empty($portalAgent->meta)) foreach($portalAgent->meta as $info) {
				if ($info->action == SELLER_PROFILE_DATA) {
					if (!empty($info->contact_email))
						$portalAgentEmail = $info->contact_email;
					break;
				}
			}
			$portalAgentPhone = isset($portalAgent->phone) && !empty($portalAgent->phone) ? fixPhone($portalAgent->phone) : '';
			$portalAgentMobile = isset($portalAgent->mobile) && !empty($portalAgent->mobile) ? fixPhone($portalAgent->mobile) : '';
		}

		$primaryEmail = $primary->email;
		if (!empty($primary->meta)) foreach($primary->meta as $info) {
			if ($info->action == SELLER_PROFILE_DATA) {
				if (!empty($info->contact_email))
					$primaryEmail = $info->contact_email;
				break;
			}
		}
		$phone = isset($primary->phone) && !empty($primary->phone) ? fixPhone($primary->phone) : '';
		$mobile = isset($primary->mobile) && !empty($primary->mobile) ? fixPhone($primary->mobile) : '';

		$ip = $this->getClass('SessionMgr')->userIP();
		if ($ip != "::1")
			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
	 	else
			$details = (object)['city'=>'local', 'region'=>'state'];

		$username = $data->first.(isset($data->last) ? " ".$data->last : '');
		$user_email = $data->user_email;

		$msg = "<p>Hello ".$agentName."</p>";
		$msg .= "<p>One of our users, ".$username." ( from:{$details->city}, {$details->region})";
		if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL ||
			$data->flags == MESSAGE_EMAIL_PORTAL_AGENT) 
			$msg .= ", who came through one of our client portal (owned by $portalAgent->first_name $portalAgent->last_name)";

		if ($data->flags == MESSAGE_EMAIL_AGENT_VIA_PORTAL ||
			$data->flags == MESSAGE_EMAIL_AGENT) {
			$msg .= ", contacted one of our Lifestyled AgentMatch agents: $primaryName with email: $primaryEmail";
			if (!empty($phone))
				$msg .= ", phone: $phone";
			if (!empty($mobile))
				$msg .= ", mobile: $mobile";
		}
		$msg .= ", about this ";
		$msg .= '<a href="'.$url2.'" target="_blank">listing</a> at '.$listingAddr.'.  ';
		$msg .= "<p>The buyer's email is ".$user_email.".</p>";
		
		$msg .= "<p>Thought you might want to know.";
		if (!empty($portalAgent)) {
			$msg .="  Please contact the portal agent, $portalAgent->first_name, for any additional information or how to get your own client portal at $portalAgentEmail";
			if (!empty($portalAgentPhone))
				$msg .= ", phone: $portalAgentPhone";
			if (!empty($portalAgentMobile))
				$msg .= ", mobile: $portalAgentMobile";
			$msg .= ".</p>";
		}

		$msg .= '<p>Sincerely,</p>';
		$msg .= '<p>The Lifestyled Team</p>';

		$this->sendMail( $email, $subject, $msg, 
						"Listing activity!",
						(empty($portalAgent) ? "Please check the listing to see if you can help the client!" : "Please contact the portal agent for additional information!") );
	}

	protected function checkInvite($invite, &$userMode, &$ic) {
		$o = $this->getClass('Invitations');
		$q = new \stdClass();
		$ic = null;
		if (!empty($invite)) {
			//$q->where = array('opt'=>INVITATION_LIST);
			$invites = $o->get();
			if (empty($invites))
				return new Out('fail', "Sorry, there are no invitations available.");
			//$invites = json_decode($invites[0]->value);
			if ( ($ic = $this->inviteInList($invite, $invites, 'code')) === false )
				return new Out('fail', "Sorry, this invitation is not recognized.");
			$userMode = AGENT_INVITE;
			return true;
		}		
		return false;
	}

	public function updateInvite($ic, $sellerId) {
		if (!isset($ic->seller_id))
			$ic->seller_id = [];
		$fields = [];
		if (!in_array($sellerId, $ic->seller_id)) {
			$ic->seller_id[] = $sellerId;
			$fields['seller_id'] = $ic->seller_id;
		}
		$ic->number_usage--;
		$fields['number_usage'] = $ic->number_usage;
		if (!($ic->flags & IC_USED)) {
			$ic->flags |= IC_USED;
			$fields['flags'] = $ic->flags;
		}
		$ic->count_usage++;
		$fields['count_usage'] = $ic->count_usage;
		$x = $this->getClass('Invitations')->set([(object)['where'=>['code'=>$ic->code],
											  			   'fields'=>$fields]]);
		unset($fields);
		return !empty($x) ? true : false;
	}

	
	public function sendMail($email, $subject, $msg, 
								$blogname = 'Welcome!', 
								$blogdescription = "You're now part of the best lifestyled listings revolution!") {
		$newAgentAttachmentTriggers = ['Email Verification',
									   'Successful Registration!'];
		$user = wp_get_current_user();
		$attachments = [];
		if (in_array($subject, $newAgentAttachmentTriggers)) {
			$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'NewAgentEmailAttachments']]);
			if (!empty($opt)) {
				$opt = json_decode($opt[0]->value);
				if (is_array($opt)) foreach($opt as $data) {
					if (file_exists(__DIR__."/../attachments/$data"))
						$attachments[] = __DIR__."/../attachments/$data";
					unset($data);
				}
				elseif (file_exists(__DIR__."/../attachments/$opt"))
					$attachments[] = __DIR__."/../attachments/$opt";
			}
		}

		update_option( 'blogurl', get_home_url());
		update_option( 'blogname', $blogname);
		update_option( 'blogdescription', $blogdescription);
		update_option( 'admin_email', SUPPORT_EMAIL); 

		$headers = ['From: Lifestyled Listings <'.SUPPORT_EMAIL.'>' . "\r\n"];
	  	$success = wp_mail( $email, $subject, $msg, $headers, $attachments); 

	  	update_option( 'blogname', 'Welcome!');
	  	update_option( 'blogdescription', 'The best lifestyled listing site!');

	  	$this->log("sendMail to $email about $subject: $msg was ".($success ? "successful" : "failure"));
	  	return $success;
	}

	public function loginUser($login, $password) {
		wp_logout(); // just in case...
		wp_set_current_user(0);

		$creds = array();
		$creds['user_login'] = $login;
		$creds['user_password'] = $password;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to login seller</head>';
			$h .= '<body><h1>Seller with author id: '.$keys[1].', failed to login with error: '.$user->get_error_message().'.<br/>Please contact administrator</h1></body>';
			$h .= '</html>';
			return $h;
		}
		wp_set_current_user( $user->ID, $login );
		return true;
	}

	public function getUserData($id) {
		if ($id == -1) {// then this might be from the feedback dialog
			$user = wp_get_current_user();
			$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		}
		elseif ($id == 0)
			$userId = 0;
		else
			$userId = $id;

		if ($userId !== 0) {
			$user_info = get_userdata($userId);
			$info = new \stdClass;
			$info->user_id = $user_info->ID;
			$info->session_id = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : session_id();
			$info->first = $user_info->first_name;
			$info->last = $user_info->last_name;
			$info->email = $user_info->user_email;
			return new Out('OK', $info);
		}
		return new Out('fail');
	}

	// public function logout($redirect) {
	// 	$this->wp_logout(); // this will force the cqll to $this->wp_logout();
	// 	//wp_redirect($redirect);
	// 	// exit;
	// 	return new Out('OK', array('redirect'=>$redirect));
	// }

	// // 
	// public function wp_logout() {
	// 	$ip = $this->userIP();
	// 	$session = session_id();

	// 	$msg = "wp_logout - ip:$ip - session:$session";
	// 	$this->log($msg);

	// 	setcookie('ProfileData', '', time() + (86400 * 1), "/"); // 86400 = 1 day

	// 	// setcookie('PROFILE_AGENT_NAME', "Unknown", time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// setcookie('PROFILE_AGENT_ID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// setcookie('TRACK_PREMIER_AGENT_USAGE', 'false', time() + (86400 * 1), "/"); // 86400 = 1 day
	
	// 	// if ($session) {
	// 	// 	session_destroy(); // get rid of old session data
	// 	// 	// session_abort(); // finish session
	// 	// 	// // session_commit();
	// 	// 	// if (!empty($_COOKIE['PHPSESSID'])) {
	// 	// 		setcookie('PHPSESSID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// 		$this->log('wp-logout set PHPSESSID to 0');
	// 	// 	// }
	// 	// 	// else
	// 	// 	// 	$this->log('wp-logout finds cookie empty');
	// 	// 	setcookie('PROFILE_AGENT_NAME', 'unknown', time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// 	setcookie('PROFILE_AGENT_ID', "0", time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// 	setcookie('TRACK_PREMIER_AGENT_USAGE', 'false', time() + (86400 * 1), "/"); // 86400 = 1 day
	// 	// 	$_SESSION['PROFILE_AGENT_NAME'] = 'unknown';
	// 	// 	$_SESSION['PROFILE_AGENT_ID'] = "0";
	// 	// 	$_SESSION['TRACK_PREMIER_AGENT_USAGE'] = 'false';
	// 	// }
	// }

	// called from function.php's: add_action( 'password_reset', 'password_reset', 10, 2 );
	public function userResetPassword($user, $password, $oldPassword = '', $fromAdmin = 0) {
		$s = $this->getClass('Sellers');
		$q = new \stdClass();
		$seller = null;
		$q->where = array('email'=>$user->user_email);
		$x = $s->get($q);
		if (!empty($x))
			$seller = $x[0];

		$updatePswd = false;
		$activationKey = null;
		$magic = null;

		$this->log("userResetPassword - entered for {$user->ID} with $password, old:".(!empty($oldPassword) ? $oldPassword : 'N/A'));
		if (!empty($password)) {
			// check if the password has changed
			$testPasswd = '';
			$magic = get_user_meta($user->ID, 'user_key', true);
			$i = 0;
			if (!empty($magic)) foreach($magic as $value)
				$testPasswd .= chr( $value ^ ($i++ + ord('.')));
			else {
				if (!empty($seller->meta)) {
					foreach($seller->meta as $meta) {
						if ($meta->action == SELLER_KEY) {
							$key = json_decode($meta->key);
							$len = count($key); // strlen($meta->key);
							for($i = 0; $i < $len; $i++)
								$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
						}
					}
					$this->log("userResetPassword - on record: $testPasswd");
				}
				elseif ($user->ID > 8) {
					$this->log("userResetPassword - Cannot find current password for verification for $user->ID");
					return new Out('fail', "Cannot find current password for verification!");
				}
			}

			if ($user->ID > 8 &&
				!empty($oldPassword) &&
				$oldPassword != $testPasswd) {
				$this->log("userResetPassword - Current password is incorrect for $user->ID");
				return new Out('fail', "Current password is incorrect!");
			}

			if ($testPasswd != $password) {
				// $change['user_pass'] = $password;
				$activationKey = array();
				$len = strlen($password);
				for($i = 0; $i < $len; $i++) {
					$left = ord($password[$i]);
					$right = ($i + ord('.'));
					$val = $left ^ $right;
					$activationKey[] = $val;
				}
				$updatePswd = true;		
			}
		}

		$msg = '';
		$retval = true;
		if ($updatePswd) {
			if (!empty($magic)) {
				update_user_meta( $user->ID, 'user_key', $activationKey, $magic);
				$this->log("userResetPassword - updated user_key for $user->ID");
			}
			else {
				add_user_meta(	$user->ID, 
								'user_key', 
								$activationKey, 
								true);
				$this->log("userResetPassword - added user_key for $user->ID");
			}
			// if $oldPassword is there, then this call came from seller's page, vs as add_action( 'password_reset', 'password_reset', 10, 2 ) when user 
			// changes the password from "Did you forget the password?", so update wp explictly
			if (!empty($oldPassword)) {
				wp_set_password($password, $user->ID);
				wp_logout(); // just in case...
				wp_set_current_user(0);
				$creds = array();
				$creds['user_login'] = $user->user_login;
				$creds['user_password'] = $password;
				$creds['remember'] = true;
				$user = wp_signon( $creds, false );
			}

			$msg = "Updated password:$password for user with id $user->ID.";
			if ($seller) {
				// update email
				$q->where = array('id'=>$seller->id);
				$q->fields = array('email'=>$email);
				if ($updatePswd) {
					$meta = array();
					foreach($seller->meta as $data) {
						if ($data->action != SELLER_KEY)
							$meta[] = $data;
					}
					$newMeta = new \stdClass();
					$newMeta->action = SELLER_KEY;
					$newMeta->key = JSON_encode($activationKey);
					$newMeta->updated = date("Y-m-d H:i:s", time() + (-7*3600));
					$meta[] = clone $newMeta;
					$q->fields['meta'] = $meta;
				}
				$a = array();
				$a[] = $q;
				$x = $s->set($a);
				if (!$x) {
					$msg .= "  But failed to update seller db's meta data.";
					$retval = false;
				}
				else
					$msg .= " And also updated seller db's meta data";

			}
		}
		else {
			$msg = "Password: $password, is the same as before.";
			$retval = false;
		}

		$this->log("userResetPassword - $msg");
		// if (!empty($oldPassword)) {
		if ($updatePswd) {
			$msg = "Password has been changed on your account to $password.  If this was not you, please contact us right away!";
			if (!$fromAdmin)
				$this->sendMail($user->user_email, "Password Changed", $msg,
													"Password Change",
													"Please take action if you are not aware of this action.");
			$this->sendMail(SUPPORT_EMAIL, "Password Changed", "User:$user->ID - Password has been changed on account to $password by".($fromAdmin ? 'admin' : 'user'),
												"Password Change",
												"Record for prosperity.");
		}

		return new Out($retval ? 'OK' : 'fail', $msg);
	}

	public function updateUser($login, $email, $password) {
		$user = wp_get_current_user();
		$change = array();
		if (!empty($login) &&
			$user->user_login != $login)
			$change['user_login'] = $login;

		$s = $this->getClass('Sellers');
		$q = new \stdClass();
		$seller = null;
		if (!empty($email) &&
			$user->user_email != $email) {
			// check if this email is in wp already
			if (get_user_by( 'email', $email ) )
				return new Out('fail', "This email address: $email, already exists.  Try another.");

			// check if another seller has this email
			$q->likeonlycase = array('email'=>$email);
			$x = $s->get($q);
			if (!empty($x))
				return new Out('fail', "This email address: $email, already exists.  Try another.");

			$change['user_email'] = $email;
			// see if we have a seller with the same one as current wp
			$q->likeonlycase = array('email'=>$user->user_email);
			$x = $s->get($q);
			if (!empty(x)) 
				$seller = $x[0];
		}

		$this->log("updateUser - entered for $login with $email and $password");

		$updatePswd = false;
		$activationKey = null;
		$magic = null;
		if (!empty($password)) {
			// check if the password has changed
			$testPasswd = '';
			$magic = get_user_meta($user->ID, 'user_key', true);
			$i = 0;
			foreach($magic as $value)
				$testPasswd .= chr( $value ^ ($i++ + ord('.')));

			if ($testPasswd != $password) {
				$change['user_pass'] = $password;
				$activationKey = array();
				$len = strlen($password);
				for($i = 0; $i < $len; $i++) {
					$left = ord($password[$i]);
					$right = ($i + ord('.'));
					$val = $left ^ $right;
					$activationKey[] = $val;
				}
				$updatePswd = true;		
			}
		}
		if (count($change)) {
			$change['ID'] = $user->ID;
			$retval = wp_update_user($change);
			if ($retval == $user->ID) {
				if ($updatePswd)
					update_user_meta( $user->ID, 'user_key', $activationKey, $magic);
				$msg = "Changed ".(count($change)-1)." items.";
				$this->log("updateUser - $smg for $login");
				if ($seller) {
					// update email
					$q->where = array('id'=>$seller->id);
					$q->fields = array('email'=>$email);
					if ($updatePswd) {
						$meta = array();
						foreach($seller->meta as $data) {
							if ($data->action != SELLER_KEY)
								$meta[] = $data;
						}
						$newMeta = new \stdClass();
						$newMeta->action = SELLER_KEY;
						// $len = strlen($activationKey);
						// $key = array();
						// for($i = 0; $i < $len; $i++)
						// 	$key[] = ord($activationKey[$i]) ^ ($i + ord('.'));
						$newMeta->key = JSON_encode($activationKey);
						$meta[] = clone $newMeta;
						$q->fields['meta'] = $meta;
					}
					$a = array();
					$a[] = $q;
					$x = $s->set($a);
					if (!$x)
						$msg .= "  But failed to update seller db's email.";
				}
				$out = new Out('OK', $msg);
				return $out;
			}
			else
				return new Out('fail', "Could not change user data for user: $user->ID");
		}
		else
			return new Out('OK', "Nothing was changed.");
	}

	public function getUser() {
		$user = wp_get_current_user();
		$bre = '';
		$magic = get_user_meta($user->ID, 'user_key', true);
		$s = $this->getClass('Sellers');
		$q = new \stdClass();
		$q->likeonlycase = array('email'=>$user->user_email);
		$x = $s->get($q);
		if (!empty($x)) { // get their bre
			$x[0] = (object)$x[0];
			foreach($x[0]->meta as $data) {
				if ($data->action == SELLER_BRE) {
					$bre = $data->BRE;
					break;
				}
			}
		}

		$data = array();
		$data['login'] = $user->user_login;
		$data['bre'] = $bre;
		$data['email'] = $user->user_email;
		$data['first_name'] = $user->first_name;
		$data['last_name'] = $user->last_name;

		return new Out('OK', $data);
	}

	public function resendVerificaton() {
		$user = wp_get_current_user();
		$s = $this->getClass('Sellers');
		$q = new \stdClass();
		$q->likeonlycase = array('email'=>$user->user_email);
		$x = $s->get($q);
		if (empty($x))
			return new Out('fail', 'Failed to find seller with email: '.$user->user_email);

		$color_key= get_user_meta($user->ID, 'color_key', true);
		if (empty($color_key)) {
			$key = null;
			$password = '';
			$x[0] = (object)$x[0];
			foreach($x[0]->meta as $meta) {
				if ($meta->action == SELLER_KEY) {
					$key = json_decode($meta->key);
					break;
				}
			}
			if ($key != null) {
				$len = count($key); // strlen($meta->key);
				for($i = 0; $i < $len; $i++)
					$password .= chr( $key[$i] ^ ($i + ord('.')) );

				$color_key = hash("sha256", $password);
				$activationKey = array();
				$len = strlen($password);
				for($i = 0; $i < $len; $i++) {
					$left = ord($password[$i]);
					$right = ($i + ord('.'));
					$val = $left ^ $right;
					$activationKey[] = $val;
				}
				update_user_meta(	$user->ID, 
									'user_key', 
									$activationKey, 
									true);

				update_user_meta(	$user->ID, 
									'color_key', 
									$color_key, 
									true);

				update_user_meta(	$user->ID, 
									'login_magic', 
									$color_key, 
									true);
			}
		}
		$msg = $this->constructMsg($x[0], $user->ID, $color_key);
		$mailed = $this->sendMail($user->user_email, "Verification Email", $msg);
		$msg = $mailed ? "Your verification email was sent to ".$user->user_email."<br/><br/><span id='spam_notice'><strong>Please make sure your spam filters allow emails from ".SUPPORT_EMAIL."</strong></span>" : "Failed to send verification email";
		$this->log("Resent verification email to {$user->user_email}");
		return new Out('OK', $msg);
	}

	public function upgradeRegistry($realtor_id, $state, $invite, $special = REGISTRATION_SPECIAL_NONE) {
		sleep(2); // just to give time for the ahtb to close...
		$id = wp_get_current_user()->ID;
		if ($id == 0)
			return new Out('fail',"Failed to retrieve current user's id");

		$userMode = AGENT_ORGANIC;
		$ic = null;
		$hasInvite = $this->checkInvite($invite, $userMode, $ic);
		// if ( ($out = $this->checkInvite($invite, $userMode, $ic)) !== true)
		// 	return $out;	

		if ($ic && $ic->number_usage == 0)
			return new Out('fail', "This invitation's usage count has been used up.");

		// this piece of data must exist for the upgrade, since the caller is now logged in as a simple user
		// and during the simple user registration, 'user_key' was assigned to this $id
		$magic = get_user_meta($id, 'user_key', true);
		$password = '';
		$len = count($magic); // strlen($magic);
		for($i = 0; $i < $len; $i++) {
			$c = chr( $magic[$i] ^ ($i + ord('.')) );
			$password .= $c;
		}

		if ($password == '' ||
			empty($password))
			return new Out('fail', "Failed to find the user key from the original user registration.");

		$this->log("upgradeRegistry - entered with BRE:$realtor_id, state:$state, invite:$invite");

		$seller = null;

		add_user_meta(	$id, 
						'realtor_id', 
						$realtor_id,
						true);

		$my_user = new \WP_User( $id );
		$my_user->set_role( "contributor" );

		if ($special)
			$userMode = AGENT_INVITE;

		//$from = get_option('admin_email');

		switch($this->createSeller($seller, $my_user->user_email, $my_user->user_firstname, $my_user->user_lastname, $id, $userMode)) {
			case 0: return new Out('fail', "Failed to add to our user database");
			case -1: return new Out('fail', "Failed to retrieve from our user database after creation");
		}

		$color_key= get_user_meta($id, 'color_key', true);
		update_user_meta(	$id, 
							'login_magic', 
							$color_key, 
							true);		
  		$this->addSellerMeta($seller, $id, $realtor_id, $password, $userMode, $state, $ic);

  		$this->log("upgradeRegistry - succeeded for {$my_user->user_firstname} {$my_user->user_lastname} id:$id, email:{$my_user->user_email}");

		if (!$special &&
			 $userMode == AGENT_ORGANIC) {
		  	$msg = $this->constructMsg($seller, $id, $color_key);
		  	if ($this->sendMail($my_user->user_email, 'Successful Registration!', $msg)) {
		  		$this->sendMail( SUPPORT_EMAIL, "Upgrade to Agent!", $msg, 
	  	  								'Upgrade!',
	  									"Agent:$id upgraded!");
	  			$out = new Out('OK', '<div style="text-align: left;"><span>Sent email to you at <strong>'.$my_user->user_email.'</strong> with your login id as: '.$my_user->user_login.' and your password as: '.$password.'.'.
	  								 "<br>Please keep this information in a secure place so that you can find it again.".
	  								 "<br>Please check and click on the link in the email to complete registration.".
	  								 "<br>If you don't see it, please check your spam folder too.</span></div><br/>
	  								 <strong>Please make sure your spam filters allow emails from ".SUPPORT_EMAIL."</strong>");
		  	}
		  	else 
		  		$out = new Out('OK', "Failed to send out confirmation email, with code: $mailed!!");
		}
		else {
			if (!$special)
				$out = new Out('OK', '<div style="text-align: left;"><span>Sent email to you at '.$my_user->user_email.' with your login id as: '.$my_user->user_login.' and your password as: '.$password.'.'.
									 "<br>Please keep this information in a secure place so that you can find it again.".
									 "<br>Please hit OK, to enter the seller's page.</span></div>");
			else {
				$msg = '';
				$msg .= '		<section>';
				$msg .= str_replace('%first_name%', ucwords(strtolower($my_user->user_firstname)), SALES_UPGRADE_REGISTRATION);
				$msg .= '		</section>';
		  		if ($this->sendMail($my_user->user_email, 'Successful Upgrade!', $msg)) {
		  			$this->sendMail( SUPPORT_EMAIL, "Upgrade to Agent by Sales!", $msg, 
	  	  								'Upgrade!',
	  									"Agent:$id upgraded!");
		  		}
		  		$out = new Out('OK', (object)['id'=>$seller->id]);
		  	}
			$this->updateListings($seller, $id);
		}
      	return $out;
	}

	public function basicRegistry($email, $first_name, $last_name, $password, $userIdentifier, $login, $state = '', $invite = '', $special = REGISTRATION_SPECIAL_NONE, $fromAdmin = 0, $origin = ORDER_IDLE, $optIn = 0) {
		sleep(2); // just to give time for the ahtb to close...
		$email = trim($email);
		$first_name = trim($first_name);
		$last_name = trim($last_name);
		$my_user =  null;
		if (!empty($userIdentifier) && !is_numeric($userIdentifier)) $userIdentifier = trim($userIdentifier);
		if (!empty($login)) $login = trim($login);
		
		if ( ($my_user = get_user_by( 'email', $email )) ) {
			if ($userIdentifier != 'paywhirl_user')
				return new Out('fail', "This email address: $email, already exists.  Please sign in.");
			else {
				$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id' => $my_user->ID]]);
				if (!empty($seller))
					return new Out('OK', (object)['id'=>$my_user->ID,
												  'seller_id'=>$seller[0]->id,
												  'existing'=>1]);
				else
					$this->log("Failed to find seller with email:$email and WP id:$my_user->ID, but found WPUser with the same email, going to create seller");
			}
		}

		if (get_user_by( 'login', $login ) )
			return new Out('fail', "This login id: $login, already exists.  Please sign in.");

		$this->fromAdmin = $fromAdmin; // if true, then don't send email to registrar
		$added = false;
		$userMode = NON_AGENT;
		$out = null;
		$seller_id = 0;
		if (!empty($userIdentifier) &&
			isset($state) &&
			!empty($state))
		{
			$out = $this->createAgent($email, $first_name, $last_name, $password, $userIdentifier, $login, $state, $invite, $userMode, $seller_id, $special, $origin, $optIn);
			if ($out->status == 'fail') {
				$this->log("basicRegistry - FAILED to created agent: $first_name $last_name, BRE:$userIdentifier, state:$state, login:$login, userMode:$userMode, invite:$invite, special:$special");
				return $out;
			}
			$added = true;
			$this->log("basicRegistry - created agent: $first_name $last_name, email:$email, BRE:$userIdentifier, state:$state, login:$login, userMode:$userMode, invite:$invite, special:$special, seller_id:$seller_id");
			$user = get_user_by('email', $email);
			$status = $user->ID;

			if (!isset($out->data->exisiting)) { // if isset(), then found existing registered seller for paywhirl, so don't reset nickname here
				$status = wp_update_user( array( 'ID' => $user->ID, 'nickname' => 'unknown' ) );
				if ( is_wp_error($status) ) 
					return new Out('fail', "Failed to update user, status: ".$status->get_error_message());
			}
			$my_user = $user;
		}

		$retval = new \stdClass();
		// this block if it's a simple user and not an agent (seller)
		if (!$added) {
			// create new wp user
			$md5 = hash("sha256", $password);
			$status = wp_insert_user(array(
				'user_login'=>$login,
				'user_email'=>$email,
				'user_pass'=>$password,
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'user_nicename'=>'Unknown',
				//'user_activation_key'=>$md5,
				'display_name'=>$first_name,
				'show_admin_bar_front'=> 'false'
			));

			if ( is_wp_error($status) ) 
				return new Out('fail', "Failed to add to user database, status: ".$status->get_error_message());

			$status = wp_update_user( array( 'ID' => $status, 'nickname' => 'unknown' ) );

			$my_user = new \WP_User( $status );
			$my_user->set_role( "subscriber" );
			$retval->id = $status;

			// save encrypted password to be used later if this same user tries to become an agent
			$activationKey = array();
			$len = strlen($password);
			for($i = 0; $i < $len; $i++) {
				$left = ord($password[$i]);
				$right = ($i + ord('.'));
				$val = $left ^ $right;
				$activationKey[] = $val;
			}
			add_user_meta(	$status, 
							'user_key', 
							$activationKey, 
							true);
			add_user_meta(	$status, 
							'color_key', 
							$md5, 
							true);
			add_user_meta(	$status, 
							'login_magic', 
							$md5, 
							true);	
			add_user_meta(	$status, 
							'opt-in', 
							$optIn, 
							true);	
			$this->log("basicRegistry - subscriber: $first_name $last_name, login:$login");
		}

		// grab current session's portalUser information before re-login
		$id = 0;
		$SessionMgr = $this->getClass('SessionMgr');
		$session = null;
		$session_id = $session_id_pre = $SessionMgr->getCurrentSession($id, $session);
		$ip = $SessionMgr->userIP();
		$portalUserID = 0;
		$agentID = 0;
		$nickname = '';
		$lastQuizId = !empty($session) ? $session->quiz_id : 0;
		$this->getSessionData($session, $portalUserID, $agentID);
		// if ($portalUserID &&
		// 	$agentID) 
			$this->log("basicRegistry - portalUser:$portalUserID, agentID:$agentID, lastQuizId:$lastQuizId, sessionId:$id");

		if ( ($h = $this->loginUser($login, $password)) !== true) {
			return new Out('fail', $h);
		}
		else
			$this->log("basicRegistry - logged in $login, userID:$status");

		if (!$added) {
		  	$msg = $this->constructMsgUser($first_name, $login, $password);

		  	// associate current session with ths user
		  	$session_id = session_id();
		  	$this->log("basicRegistry - ip:".$ip." session:".$session_id.", userID:".$status.", userMode:$userMode");
		  	$SessionMgr->setIPAssoc($ip, $session_id, $status);

		  	if (!empty($email) &&
		  		!$this->sendMail($email, 'Successful Registration!', $msg)) {
		  		$retval->failure_msg = "Failed to send out confirmation email!!";
		  		$this->log("Failed to send out confirmation email to userId:$status");
		  		return new Out('OK', $retval);
		  	}
		  		
		  	$this->sendMail( SUPPORT_EMAIL, "Basic Registration!", $msg,
		  					'Basic Registration!',
		  					"New user: $login, id: {$my_user->ID}");
		  	// employ overloaded use of $userIdentifier to assign portalUserID
		  	if ( !empty($userIdentifier) &&
		  		 !$portalUserID )
		  		$portalUserID = $userIdentifier;
		}

		$session_id_post = $SessionMgr->getCurrentSession($id, $session);
		$user = wp_get_current_user();

		if ($agentID) {
	  		$agent = get_userdata($agentID);
	  		if ($agent) {
	  			$nickname = $agent->nickname;
	  			if ( strpos($nickname, 'unknown') === false ) {
		  			$SessionMgr->setPortalAgent($nickname, $agentID, true, headers_sent()); // associate portal agent to new session
		  			$session_id_post = $SessionMgr->getCurrentSession($id, $session);
		  			if ($portalUserID) {
			  			$this->log("basicRegistry - after setting portalAgent for new user:$user->ID, portalUser:$portalUserID, nickname:$nickname, agentID:$agentID, sessionID:$id, session_id:$session_id_post, system's session_id:$session_id, session:user_id:$session->user_id");
			  			$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserID]]);
			  			$fields = [];
			  			$portalUser = array_pop($portalUser);
			  			// update fields as necessary
			  			if ($portalUser->wp_user_id != $user->ID)
			  				$fields['wp_user_id'] = $user->ID;
			  			if ($portalUser->first_name != $first_name)
			  				$fields['first_name'] = $first_name;
			  			if ($portalUser->last_name != $last_name)
			  				$fields['last_name'] = $last_name;
			  			if ($portalUser->email != $email)
			  				$fields['email'] = $email;
			  			if ($session_id_pre != $session_id_post)
			  				$fields['session_id'] = $id;
			  			if (count($fields))
				  			$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserID],
				  														  'fields'=>$fields]]); // update with new user ID
				  		$this->log("basicRegistry - calling updateSession() for portalUserID:$portalUserID, agentID:$agentID, id:$id");
			  			$this->getClass('PortalUsersMgr')->updateSession($portalUserID, $agentID, $id); // set SESSION_PORTAL_USER meta data
			  			unset($fields, $portalUser);
			  		}
			  		else
			  			$this->getClass('PortalUsersMgr')->addPortalUserFromWP($id, $user->ID, 'basic-registration', $agentID);
		  		}
		  		else
		  			$this->log("basicRegistry - agentID:$agentID has invalid nickname:$nickname");
	  		}
	  		else
	  			$this->log("basicRegistry - failed to get userdata for agentID:$agentID");
	  	}
	  	else 
	  		$this->log("basicRegistry - after registry no portalUser/agent on sessionID:$id");
	  		
		if ($session_id_pre != $session_id_post &&
  			$lastQuizId) {
  			$this->getClass('Sessions')->set([(object)['where'=>['id'=>$id],
  													   'fields'=>['quiz_id'=>$lastQuizId]]]);
  			$session_id = $session_id_post;
  		}

	  	$msg = '<div style="margin:0 auto;width:92%"><br/><span><span style="font-family:Perpetua;font-size:2.25em">Welcome to LifeStyled Listings!</span><br/><span>We have successfully registered you as '.$login.'.</span><br/><br/>';
	  	if ($added) 
	  		if ($userMode != AGENT_ORGANIC)
	  			$msg .= "You will now be directed to your seller's page.  Please take time to update your profile and add or modify your listings.";
	  		else {
	  			$extra = $origin == ORDER_PORTAL ? ($special != REGISTRATION_SPECIAL_NONE ? ", and upload a photo for your portal." : ", and complete the portal purchase and edit your profile and use your portal.") :
	  					 ($origin == ORDER_AGENT_MATCH ? ($special != REGISTRATION_SPECIAL_NONE ? ", and activate yourself as a LifeStyle Agent by supplying some basic information about yourself.  Click on the LifeStyled Agent tab in your seller's dashboard." :
	  					 																		  ", and purchase LifeStyle Agent product by supplying some basic information about yourself.  Click on the LifeStyled Agent tab in your seller's dashboard.") :
	  					 								".");
	  			$msg .= "Please check your email at <strong>$email</strong> and click the link to verify.  Once that is done, you will be able to access your agent page".$extra."<br/>";
	  			$msg .= '<span style="display:block;font-size:.7em;font-style:italic;margin:2em auto 1.5em;line-height:1.5em;width:70%;font-family:open sans">If you do not see any email from us, please make sure your spam filters allow emails from support@lifestyledlistings.com</span>';
	  		}
	  	else
	  		$msg .= "Have fun finding amazing locations and homes that fit your lifestyle!  Let us know how you like it.";
	  	$msg .= "</span></div>";

	  	$retval->seller_id = $seller_id;
	  	$retval->id = !empty($my_user) ? $my_user->ID : 0;
	  	$retval->msg = $msg;
	  	$retval->userMode = $userMode;
	  	$retval->session_id = $session_id;
	  	$retval->session = $id;
	  	$retval->nickname = $nickname;
	  	if ( !$special || !$added || empty($out) ) 
	  		return new Out('OK', $retval);
	  	else {
	  		$out->data->seller_id = $seller_id;
	  		$out->data->id = !empty($my_user) ? $my_user->ID : 0;
	  		$out->data->msg = $msg;
	  		$out->data->userMode = $userMode;
	  		return $out;
	  	}
	}

	protected function getSellerEmailDb($seller) {
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'host'=>strtolower($email[1])];
				$msg = "getSellerEmailDb - added sellerId: {$seller->id}, email:{$seller->email}";
				$this->log($msg);
				$id = $EmailDb->add($db);
				unset($db);
				return $EmailDb->get((object)['where'=>['id'=>$id]])[0];
			}
			else
				return false;
		}
		else if (!empty($seller->email))
			return $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
		else
			return false;
	}

	protected function checkSellerEmailDb($seller) {
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'host'=>strtolower($email[1])];
				$msg = "checkSellerEmailDb - added sellerId: {$seller->id}, email:{$seller->email}";
				$this->log($msg);
				$EmailDb->add($db);
				unset($db);
			}
		}
	}

	public function createAgent($email, $first_name, $last_name, $password, $realtor_id, $login, $state, $invite, &$userMode, &$seller_id, $special = REGISTRATION_SPECIAL_NONE, $origin = ORDER_IDLE, $optIn = 0) {
		$o = $this->getClass('Options');
		// first verify that this agent's email address is valid
		$s = $this->getClass('Sellers');
		$q = new \stdClass();
		$x = false;
		$seller_id = 0;

		// check for email match first
		$x = $s->get((object)['likeonlycase'=>['email'=>$email],
							  'keepListHubNaming'=>true]);
		if (empty($x) &&
		   !empty($last_name)) {
			$q->like = array('last_name'=>$last_name);
			$q->like2 = array('first_name'=>$first_name);
			$q->where = ['state'=>$state];
			$q->keepListHubNaming = true;
			$x = $s->get($q);
			unset($q->where);
		}

		$seller = null;
		$gotOne = false;
		$userMode = AGENT_LISTHUB;

		if (!empty($x)) {
			if (count($x) == 1 &&
				$x[0]->state == $state) {
				$seller = $x[0];
				$gotOne = true;
			}
		// now test for every seller with this last_name.
		// and test each email address, since the seller's incoming email
		// address case may not match the one in db, make them lower and test each one
	 	// then at least one selle in db had the last_name
			else foreach($x as $seller) {
				$test = strtolower($seller->email); // check to see if this seller's email matches the caller's
				$sellerlastName = strtolower($seller->last_name);
				if ( $test == strtolower($email) &&
					 $sellerlastName == strtolower($last_name) ) {
					$gotOne = true;
					break;
				}
			}
		}

		$ic = null;
		if (!$gotOne) { // then no seller with this last_name in our db.
			$userMode = AGENT_ORGANIC; // default to ORGANIC
			if (!empty($invite)) { // then check to see if this invite is valid
				if ( ($out = $this->checkInvite($invite, $userMode, $ic)) !== true)
					return $out; // failed to match or find the invite code
			}
			else if ($special) {
				$userMode = AGENT_INVITE; // technically, we are inviting them since we are doing this registration on their behalf if $special is true
			}
		}
		elseif (!empty($seller->author_id)) {
			if ( (!empty($seller->email) &&
				  $seller->email == $email) ||
				($realtor_id != 'paywhirl_user' &&
				 $seller->bre == $realtor_id) ) {
				if ($realtor_id != 'paywhirl_user')
					return new Out('fail', "This email address: $email and last name: $last_name is already registered by seller with realtor id:$realtor_id.  Did you forget the password?");
				else {
					$this->log("Found existing seller:$seller->id with email address: $email and last name: $last_name");
					$seller_id = $seller->id;
					return new Out('OK', (object)['seller_id'=>$seller->id,
												  'existing'=>1]);
				}
			}
		}

		if ($ic && $ic->number_usage == 0)
			return new Out('fail', "This invitation's usage count has been used up.");

		// this can happen if an existing WP_USER existed, but not a seller
		$existing_wp_user =  get_user_by( 'email', $email ); 
		if (empty($existing_wp_user)) {
			// create new wp user
			$md5 = hash("sha256", $password);
			$status = wp_insert_user(array(
				'user_login'=>$login,
				'user_email'=>$email,
				'user_pass'=>$password,
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'user_url'=>(!empty($seller) && isset($seller->website) ? $seller->website : ''),
				'user_nicename'=>'Unknown',
				//'user_activation_key'=>$md5,
				'display_name'=>$first_name,
				'show_admin_bar_front'=> 'false'
			));
		}
		else {
			$status = $existing_wp_user->ID;
			$my_user = $existing_wp_user;
		}

		if ( is_wp_error($status) ) 
			$out = new Out('fail', "Failed to add to user database, status: ".$status->get_error_message());
		else {
			$id = $status;
			add_user_meta(	$id, 
							'realtor_id', 
							$realtor_id,
							true);

			if (empty($existing_wp_user)) {
				$activationKey = array();
				$len = strlen($password);
				for($i = 0; $i < $len; $i++) {
					$left = ord($password[$i]);
					$right = ($i + ord('.'));
					$val = $left ^ $right;
					$activationKey[] = $val;
				}
				add_user_meta(	$id, 
								'user_key', 
								$activationKey, 
								true);

				$my_user = new \WP_User( $id );
			}
			$my_user->set_role( "contributor" );

			$status = wp_update_user( array( 'ID' => $id, 'nickname' => 'unknown' ) );

			//$from = get_option('admin_email');

			if ($userMode != AGENT_LISTHUB) 
				switch($this->createSeller($seller, $email, $first_name, $last_name, $id, $userMode, $optIn)) {
					case 0: return new Out('fail', "Failed to add to our user database");
					case -1: return new Out('fail', "Failed to retrieve from our user database after creation");
				}
			else { // set the author_id for AGENT_LISTHUB
				unset($q->where);
				$q->where = array('id'=>$seller->id);
				$fields = array('author_id'=>$id);
				if ($optIn)
					$fields['flags'] = SELLER_HAS_OPTED_IN;
				$q->fields = $fields;
				$a = array();
				$a[] = $q;
				$x = $s->set($a);
				if (empty($x))
					return new Out('fail', "Failed to update author_id for $seller->id in the user database");
				$this->checkSellerEmailDb($seller);
				$this->log("createAgent - updated existing listhub agent with ".print_r($fields, true));
			}
			// at this point, $seller is a valid entity

			add_user_meta(	$id, 
							'color_key', 
							$md5, 
							true);
			add_user_meta(	$id, 
							'login_magic', 
							$md5, 
							true);
			add_user_meta(	$id, 
							'opt-in', 
							$optIn, 
							true);	
	  		$this->addSellerMeta($seller, $id, $realtor_id, $password, $userMode, $state, $ic);

	  		$seller_id = $seller->id;
	  		$this->log("createAgent - $first_name $last_name key:$md5, userMode:$userMode, invite:$invite, special:$special, seller_id: $seller_id, optIn:$optIn");

	  		// new agent registers... so register with Zoho CRM
	  		try {
	  			$this->getClass('Zoho')->createNewZohoCrm($seller->id);
	  		}
	  		catch(\Exception $e) {
	  			$this->log("createAgent - $first_name $last_name, failed to create new Zoho CRM:".$e->getMessage());
	  		}

		  	if ($userMode == AGENT_ORGANIC)
		  		$msg = $this->constructMsg($seller, $id, $md5);
		  	else {
		  		// check to see if we need to specialize the email to send to the agent
		  		$specialTemp = $special == REGISTRATION_SPECIAL_NONE ? ($origin == ORDER_PORTAL ? REGISTRATION_SPECIAL_CAMPAIGN_PORTAL : 
		  														   	   ($origin == ORDER_AGENT_MATCH ? REGISTRATION_SPECIAL_CAMPAIGN_LIFESTYLE : $special)) : 
		  															    $special;
		  		$msg = $this->constructMsgUser($seller->first_name, $login, $password, $specialTemp);
		  		$this->updateListings($seller, $id);
		  	}
		  	if ($this->fromAdmin ||
		  		$this->sendMail($email, $userMode == AGENT_ORGANIC ? 'Email Verification' : 'Successful Registration!', $msg)) {
		  	// $status is what you might want to look at

		  		$this->sendMail( SUPPORT_EMAIL, !$special ? "New Agent!" : "Sales Dept", $msg,
		  						!$special ? 'Agent Registration!' : 'Sales Registration',
		  						"ActivationKey: $md5");
		  		
		  		if ($userMode == AGENT_ORGANIC) 
		  			$out = new Out('OK', '<div style="text-align: left;"><span>Sent email to you at <strong>'.$email.'</strong> with your login id as: '.$login.' and your password as: '.$password.'.'.
		  								 "<br>Please keep this information in a secure place so that you can find it again.".
		  								 "<br>Please check and click on the link in the email to complete registration.".
		  								 "<br>If you don't see it, please check your spam folder too.</span></div><br/>
		  								 <strong>Please make sure your spam filters allow emails from ".SUPPORT_EMAIL."</strong>");
		  		else if (!$special)
		  			$out = new Out('OK', '<div style="text-align: left;"><span>Sent email to you at '.$email.' with your login id as: '.$login.' and your password as: '.$password.'.'.
		  								 "<br>Please keep this information in a secure place so that you can find it again.".
		  								 "<br>Please hit OK, to enter the seller's page.</span></div>");
		  		else
		  			$out = new Out('OK', (object)['seller_id'=>$seller->id]);
		  	}
		  	else if (!$special)
		  	// $mailed is the error code
		  		$out = new Out('OK', "Failed to send out confirmation email, with code: $mailed!!");
		  	else
		  		$out = new Out('OK', (object)['seller_id'=>$seller->id]);
		}
		return $out;
	}

	// reset all listings owned by this seller to new author_id
	protected function updateListings($seller, $author_id) {
		$Listings = $this->getClass('Listings');
		$q = new \stdClass();
		$q->where = array('author'=>$seller->id,
						  'author_has_account'=>0);
		$listings = $Listings->get($q);
		// seller may not have any listings yet...
		if (!empty($listings)) {
			$this->log("updateListings - converting ".count($listings)." listings to author id:{$author_id} for seller id:{$seller->id}");
			foreach($listings as $listing) {
				$this->log("updateListings - listing:{$listing->id} has for author:{$listing->author}, changing it to {$author_id}");
				$q2 = new \stdClass();
				$q2->fields = array('author'=>$author_id,
									'author_has_account'=>1);
				$q2->where = array('id'=>$listing->id);
				$a = array();
				$a[] = $q2;
				$didIt = $Listings->set($a);
				if ($didIt)
					$this->log("updateListings - listing:{$listing->id} now has author:{$author_id} and author_has_account set to 1");
				unset($a, $q2);
			}
		}
	}
	// this would only get called when the intro email's link was used.
	// the activation key is made of md5__wpID__sellerID
	public function agentVerify($activationKey) {
		$keys = explode("__", $activationKey);
		$userId = intval($keys[1]);
		$magic = get_user_meta($userId, 'login_magic', true);
		if ($keys[0] != $magic) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to match seller</head>';
			$h .= '<body><h1>Invalid authorization key: '.$keys[0].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}
		// login in the user
		// get the seller using $keys[1] as 'author_id' in Sellers
		// then use the 'id' from returned seller and find all the listings that as 'author' set to this 'id'
		// then update all the listings' 'author' to the new 'author_id' value and set 'author_has_account' to 1 (true).
		// after all that, set page to seller.
		// code to retrieve password from SELLER_KEY meta database
		// for($i = 0; $i < $len; $i++)
		// $activationKey[$i] = chr( ord($activationKey[$i]) ^ ($i + ord('.')) );

		$Sellers = $this->getClass('Sellers');
		$q = new \stdClass();
		$q->where = array('id'=>$keys[2]);
		$seller = $Sellers->get($q);
		if (empty($seller)) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to find seller</head>';
			$h .= '<body><h1>Failed to find with id: '.$keys[2].'.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}
		$seller = array_pop($seller);
		if (empty($seller->meta)) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to find seller</head>';
			$h .= '<body><h1>Seller with id: '.$keys[2].', does not have the correct meta data to login.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}

		$msg = "Agent - id:".$seller->id.", name:".$seller->first_name.' '.$seller->last_name.', email:'.$seller->email.', author_id:'.(empty($seller->author_id) ? "N/A (expected)" : $seller->author_id." (not expected)").', key:'.$activationKey;
		$sent = $this->sendMail( SUPPORT_EMAIL, "Verified Agent!", $msg,
						"Verification!",
						"Another agent went live!");
		$this->log("agentVerify - $msg, sent:$sent");

		// add author_id to seller
		$q->where = array('id'=>$keys[2]);
		$q->fields = array('author_id'=>$keys[1]);
		$a = array();
		$a[] = $q;
		$updateAuthorId = $Sellers->set($a);
		if (empty($updateAuthorId)) {
			$h = '<!DOCTYPE html>';
			$h .= '<html><head>Failed to update seller</head>';
			$h .= '<body><h1>Seller with id: '.$keys[2].', could not be updated with the author_id.  Please contact administrator</h1></body>';
			$h .= '</html>';
			echo $h;
			return;
		}
		$this->log("agentVerify - updated seller_id:{$keys[2]} with author_id:{$keys[1]}");

		$password = '';
		foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_KEY) {
				$key = json_decode($meta->key);
				$len = count($key); // strlen($meta->key);
				for($i = 0; $i < $len; $i++)
					$password .= chr( $key[$i] ^ ($i + ord('.')) );
			}
		}

		$user_info = get_userdata(intval($keys[1]));
		if ( ($h = $this->loginUser($user_info->user_login, $password)) !== true) {
			echo $h;
			return;
		}

		$this->checkSellerEmailDb($seller);
			
		// convert all 'author' values to the WP userID
		$this->updateListings($seller, $keys[1]);
		// $Listings = $this->getClass('Listings');
		// $q->where = array('author'=>$seller->id,
		// 				  'author_has_account'=>0);
		// $listings = $Listings->get($q);
		// // seller may not have any listings yet...
		// if (!empty($listings)) {
		// 	$this->log("agentVerify - converting ".count($listings)." listings to author id:{$keys[1]} for seller id:{$seller->id}");
		// 	foreach($listings as $listing) {
		// 		$this->log("agentVerify - listing:{$listing->id} has for author:{$listing->author}, changing it to {$keys[1]}");
		// 		$q2 = new \stdClass();
		// 		$q2->fields = array('author'=>$keys[1],
		// 							'author_has_account'=>1);
		// 		$q2->where = array('id'=>$listing->id);
		// 		$a = array();
		// 		$a[] = $q2;
		// 		$didIt = $Listings->set($a);
		// 		if ($didIt)
		// 			$this->log("agentVerify - listing:{$listing->id} now has author:{$keys[1]} and author_has_account set to 1");
		// 		unset($a, $q2);
		// 	}
		// }
		// $h = '<!DOCTYPE html>';
		// $h .= '<html><head></head>';
		// $h .= '<body><script>window.location = '.get_home_url().'/sellers; </script></body>';
		// $h .= '</html>';
		//echo $h;

		if (headers_sent())
			echo '<script type="text/javascript"> window.location = "'.get_home_url().'/sellers"; </script>';
		else {
			header('Location: '.get_site_url()."/sellers");
			header("Connection: close");
		}
		die;
	}

	protected function inviteInList($invite, $invites, $key = 'code') {
		foreach($invites as &$data) {
			if (strtolower($data->$key) == strtolower($invite))
				return $data;
		}
		return false;
	}

	// author_id is not set until agent is verified.
	protected function createSeller(&$seller, $email, $first_name, $last_name, $wpId, $userMode, $optIn = 0) {
		$s = $this->getClass('Sellers');
		$x = $s->get((object)['likeonlycase'=>['email'=>$email]]);
		if (empty($x)) {
			$q = new \stdClass();
			// don't assign author_id until verified, if AGENT_ORGANIC
			if ($userMode == AGENT_INVITE)
				$q->author_id = $wpId;
			$q->first_name = $first_name;
			$q->last_name = $last_name;
			$q->email = $email;
			if ($optIn)
				$q->flags = SELLER_HAS_OPTED_IN;
			$x = $s->add($q);
			if (empty($x))
				return 0;
		}
		else { // wow, got one!
			if ($userMode == AGENT_INVITE &&
				empty($x[0]->author_id)) {
				$fields = ['author_id'=>$wpId];
				if ($optIn)
					$fields['flags'] = SELLER_HAS_OPTED_IN;
				$s->set([(object)['where'=>['id'=>$x[0]->id],
								  'fields'=>$fields]]);
			}
			$x = $x[0]->id;
		}

		$this->log("createSeller - added to db for $first_name $last_name with $email, row:$x"); 

		unset($q);
		$q = new \stdClass();
		$q->where = array('id'=>$x);
		$x = $s->get($q);
		if (empty($x))
			return -1;

		$this->log("createSeller - succeeded for $first_name $last_name, sellerID:{$x[0]->id} authorID:$wpId");

		$seller = $x[0];
		$this->getSellerEmailDb($seller);
		return 1;
	}

	protected function addSellerMeta($seller, $author_id, $realtor_id, $activationKey, $userMode, $state, $ic) {
		$o = $this->getClass('Invitations');
		$s = $this->getClass('Sellers');
		$q = new \stdClass();

		$meta = array();
		if (!empty($seller->meta)) foreach($seller->meta as $data) {
			$data = (object)$data;
			if ($data->action != SELLER_BRE &&
				$data->action != SELLER_KEY)
				$meta[] = $data;
		}

		$newMeta = new \stdClass();
		$newMeta->action = SELLER_BRE;
		$newMeta->BRE = $realtor_id;
		$newMeta->state = $state;
		$newMeta->userMode = $userMode;
		$newMeta->needVerify = $userMode == AGENT_ORGANIC ? 1 : 0;
		$meta[] = clone $newMeta;
		unset($newMeta->BRE);
		unset($newMeta->state);
		unset($newMeta->userMode);
		unset($newMeta->needVerify);

		$newMeta->action = SELLER_KEY;
		$len = strlen($activationKey);
		$key = array();
		for($i = 0; $i < $len; $i++)
			$key[] = ord($activationKey[$i]) ^ ($i + ord('.'));
		$newMeta->key = JSON_encode($key);
		$meta[] = $newMeta;

		if (!empty($ic) &&
			$userMode == AGENT_INVITE) {
			$newMeta = new \stdClass();
			$newMeta->action = SELLER_INVITE_CODE;
			$newMeta->key = $ic->code;
			$newMeta->owner = false;
			$newMeta->date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
			$meta[] = $newMeta;
			//$q->where = array('opt'=>INVITATION_LIST);
			$invites = $o->get();
			if (!empty($invites)) { // just to be sane, even though it must be true if the user is AGENT_INVITE
				$ic = $this->inviteInList($ic->code, $invites);			
				$this->updateInvite($ic, $seller->id);
				add_user_meta(	$author_id, 
								'inviteCodeUsed', 
								date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600)),
								true);
			}
		}

		$q->fields = array( 'meta'=>$meta );
		if (empty($seller->bre) ||
			$seller->bre != $realtor_id)
			$q->fields['bre'] = $realtor_id;

		if ($userMode != AGENT_ORGANIC) 
			$q->fields['author_id'] = $author_id;

		$flags = 0;
		if (!empty($ic)) {
			$flags = $seller->flags;
			if ($ic->flags & IC_FEATURE_PORTAL)
				$flags |= SELLER_IS_PREMIUM_LEVEL_1;
			if ($ic->flags & IC_FEATURE_LIFESTYLE)
				$flags |= SELLER_IS_PREMIUM_LEVEL_2;
			if ($ic->flags & IC_FEATURE_LIFETIME)
				$flags |= SELLER_IS_LIFETIME;

			if ($flags != $seller->flags)
				$q->fields['flags'] = $flags;

			$this->log('addSellerMeta - userId:$author_id, {$seller->first_name} {$seller->last_name}, flags:$flags, ic -  sellerId:{$ic->seller_id}, number_usage left:{$ic->number_usage-1}, count_usage:{$ic->count_usage+1}');
		}
		$q->where = array('id'=>$seller->id);
		$a = array();
		$a[] = $q;
		$x = $s->set($a);
		unset($newMeta, $meta, $a, $q);
		return $x;

	}

	protected function constructMsgUser($first_name, $login, $password, $special = REGISTRATION_SIMPLE_USER) {
		$msg = '';
		$msg .= '		<section>';
		$msg .= str_replace('%first_name%', ucwords(strtolower($first_name)), $special == REGISTRATION_SIMPLE_USER ? USER_WELCOME : 
																			  ($special == REGISTRATION_SPECIAL_NONE ? AGENT_WELCOME :
																			  ($special == REGISTRATION_SPECIAL_ADMIN ? SALES_REGISTRATION :
																			  ($special == REGISTRATION_SPECIAL_CAMPAIGN_PORTAL ? CAMPAIGN_PORTAL_REGISTRATION :
																			  													CAMPAIGN_LIFESTYLE_REGISTRATION))) );
		$msg = str_replace('%user_name%', $login, $msg);
		$msg = str_replace('%password%', $password, $msg);
		$msg .= '		</section>';
		return $msg;
	}

	protected function constructMsg($seller, $wpid, $md5) {
		$md5 .= "__".strval($wpid)."__".strval($seller->id);
		$url = get_template_directory_uri().'/_pages/ajax-register.php?query=agent-verified&data='.$md5;
		$msg = '';
		// $msg = '<!DOCTYPE html>';
		// $msg .= '<html>';
		// $msg .= '	<head>';
		// $msg .= '		<title><h2>Welcome to Allure Homes!!<h2></title>';
		// // $msg .= '	<script>';
		// // $msg .= '		function sendReply() {';
		// // $msg .= '			xmlhttp.open("POST","'.$url.'",true);';
		// // $msg .= '			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");';
		// // $msg .= '			xmlhttp.send('.'"query=agent-verified?activate='.$md5.'");';
		// // $msg .= '		}';
		// // $msg .= '	</script>';
		// $msg .= '	</head>';
		// $msg .= '	<body>';
		$msg .= '		<section>';
		$msg .= '			<span><strong>Hello '.ucfirst($seller->first_name).' '.ucfirst($seller->last_name).' </strong><br/>';
		$msg .= '			<strong>Your registration to LifeStyled Listings was successful!</strong><br/><br/>';
		$msg .= '			Please click on this </span><a href="'.$url.'" target="_blank">link</a>, to verify your email account to complete the registration.<br/>';
		$msg .= '          We will be verifying your BRE with your state of registry, but please feel free to use our site and add your listings.</span>';
		$msg .= '		</section>';
		// $msg .= '';
		// $msg .= '	</body>';
		// $msg .= '</html>';
		return $msg;
	}

	protected function getSessionData($session, &$portalUserID, &$agentID) {
		$portalUserID = 0;
		$agentID = 0;
		if (empty($session))
			return;
		$this->log("getSessionData for $session->id");
		if ($session &&
			!empty($session->value)) {
			foreach($session->value as $meta) {
				$meta = (object)$meta;
				if ($meta->type == SESSION_PORTAL_USER) {
					$portalUserID = intval($meta->portalUser);
				}
				elseif ($meta->type == SESSION_PREMIER_AGENT) {
					$agentID = $meta->agentID;
				}
			}
		}
	}
}

// new Register();
