<?php
namespace AH;
require_once(__DIR__.'/_Base.class.php');
class Invitations extends Base {
	public function __construct($logIt = 0) {
		parent::__construct($logIt);
	}

	public function __destruct() {
		if ($this->log !== null)
			$this->log->writeStringToFile();
	}
}
