<?php
namespace AH;
require_once(__DIR__.'/Utility.class.php');
require_once(__DIR__.'/Logger.class.php');

function image_dir(){return __DIR__.'/../_img/';}
class Image extends Logger {
	public function __construct($query = null, $data = null){
		parent::__construct( 1, substr(get_class($this), 3) );
		set_time_limit(0);
		
		$this->clean_file = image_dir() . 'imagecleaner.json';
		$this->clean_rows_per_type = 75; // number of rows to process at once for cleaning commands
		$this->logIt = true;
		$this->log = $this->logIt ? new Log(__DIR__.'/_logs/images.log') : null;
		if (!empty($query)){
			$out = null;
			switch($query){
				case 'generate-clean-file': case 'run-clean-file':
					// deprecated
					$out = new Out(0, 'not set');
					break;
				case 'get-image-sizes': // called by imageCleaner, returns sizes array
					$out = new Out('OK', $this->sizes);
					break;
				case 'get-type-counts':
					$out = new Out('OK', $this->getTypeCounts($data));
					break;
				case 'query-from-cleaner':
					if (empty($data['status']))
						$out = new Out(0, 'No data provided.');
					else try {
						$out = new Out('OK', $this->imageCleaner($data));
					} catch (\Exception $e) {
						$out = new Out(0, $e->getMessage());
					}
					break;
 				case 'resize': // Called by Jcrop
					if (empty($data)) $out = new Out(0, 'Image::resize: no arguements provided');
					else{
						try {
							$d = $data;
							if (empty($d['file'])) $out = new Out(0, 'Image::resize: file not set');
							elseif (empty($d['src_path'])) $out = new Out(0, 'Image::resize: src_path not set');
							elseif (empty($d['dst_path'])) $out = new Out(0, 'Image::resize: dst_path not set');
							elseif (empty($d['src_width'])) $out = new Out(0, 'Image::resize: src_width not set');
							elseif (empty($d['src_height'])) $out = new Out(0, 'Image::resize: src_height not set');
							elseif (!isset($d['dst_x'])) $out = new Out(0, 'Image::resize: dst_x not set');
							elseif (!isset($d['dst_y'])) $out = new Out(0, 'Image::resize: dst_y not set');
							elseif (!isset($d['dst_x2'])) $out = new Out(0, 'Image::resize: dst_x2 not set');
							elseif (!isset($d['dst_y2'])) $out = new Out(0, 'Image::resize: dst_y2 not set');

							$this->log("resize - data:".print_r($d, true));
							$out = $this->resize($d['file'],$d['src_path'],$d['dst_path'],$d['src_width'],$d['src_height'],$d['dst_x'],$d['dst_y'],$d['dst_x2'],$d['dst_y2'],(isset($d['logo']) ? 1 : 0));
						}
						catch( \Exception $e) {
							$out = new Out(0, 'Caught exception: '.$e->getMessage());
						}
					}
					break;
				case 'remove-image':
					require_once(__DIR__.'/Sellers.class.php'); $Sellers = new Sellers();
					if (empty($data)) throw new \Exception("No data sent");
					if (empty($data['type'])) throw new \Exception("No type sent");
					if (empty($data['id'])) throw new \Exception("No id sent");
					if (empty($data['name'])) throw new \Exception("No name sent");

					$type = $data['type'];
					$name = $data['name'];
					$id = $data['id'];
					$this->removeImage($type, $name);

					$Sellers->set([(object)['where'=>['id'=>$id],
											'fields'=>['photo'=>'NULL']]]);
					$out = new Out('OK', "removed $name from $type folders");
					break;
				default: $out = new Out(0, __FILE__.': Invalid query "'.$query.'"'); break;
			}
			if (!empty($out)) echo json_encode($out);
		}
	}

	// public function log($message, $hide_timestamp = false) {
	// 	if ($this->log !== null)
	// 		$this->log->add($message, $hide_timestamp);
	// }

	public function removeImage($type, $name) {
		if (!isset($this->sized[$type])) {
			$path = image_dir().'_'.$type.'/'.$name;
			if (file_exists($path)) {
				unlink($path);
				return true;
			}
			else
				return false;
		}
		$retval = false;
		foreach($this->sizes[$type] as $loc) {
			$path = image_dir().'_'.$type.'/'.$loc['dir'].'/'.$name;
			if (file_exists($path)) {
				unlink($path);
				$retval = true;
			}
			unset($loc);
		}
		$path = image_dir().'_'.$type.'/uploaded/'.$name;
		if (file_exists($path)) {
			unlink($path);
			$retval = true;
		}
		return $retval;
	}

	/**
	 * returns the number of rows in each "type" of image (quiz, listings, etc)
	 * @param  [type] $in (object) [ 'types' => [ 'listings' => [ '50x50', '125x500' ], 'quiz' => [ ... ], ... ] ]
	 * @return [type]     [description]
	 */
	public function getTypeCounts($in, $time_it = true){
		require_once(__DIR__.'/Options.class.php');
		$Opt = new Options();
		$in = empty($in) ? null : (object) $in;

		$types = [];

		if (!empty( $in->types ))
			foreach ($in->types as $type => $size_dirs){
				if ( !empty($size_dirs) && isset( $this->sizes[$type] ) )
					$types[] = $type;
				unset($type, $size_dirs);
			}

		$out = new \stdClass();

		foreach ($types as $type){
			$out->$type = new \stdClass();

			switch($type){
				case 'cities':
					$sql = "SELECT COUNT(1) as count FROM {$Opt->getTableName($type)} WHERE image IS NOT NULL";
					break;
				case 'listings':
					$sql = "SELECT COUNT(1) as count FROM {$Opt->getTableName($type)} WHERE images IS NOT NULL AND active = 1";
					break;
				case 'authors':
					$sql = "SELECT COUNT(1) as count FROM {$Opt->getTableName('sellers')} WHERE photo IS NOT NULL";
					break;
				case 'quiz':
					$sql = "SELECT COUNT(1) as count FROM {$Opt->getTableName('quiz-slides')} as slides "
						. "INNER JOIN {$Opt->getTableName('quiz-questions')} as questions "
						. "ON questions.id = slides.question_id "
						. "WHERE slides.file IS NOT NULL AND questions.enabled = 1";
					break;
			}

			$out->$type->time_for_query = microtime(true);
			$count = $Opt->rawQuery($sql);
			if (!empty($count))
				$out->$type->count = intval($count[0]->count);

			if ($time_it)
				$out->$type->time_for_query = round(microtime(true) - $out->$type->time_for_query, 3);
			else
				$out->$type = $out->$type->count;
		}
		return $out;
	}

	/**
	 * stores in the Options DB the list of files needed, handles all file / DB read interaction with the imageCleaner JS
	 * @param  [type] $in (object) [ 'types' => [ 'listings' => [ '50x50', '125x500' ], 'quiz' => [ ... ], ... ] ]
	 * @return [type]        results
	 */
	public function imageCleaner($in){
		require_once(__DIR__.'/Options.class.php');
		$Opt = new Options();
		$in = (object) $in;
		$out = new \stdClass();

		if ($in->status == 'start'){
			// delete old cln rows
			$Opt->rawQuery("DELETE FROM {$Opt->getTableName('options')} WHERE opt LIKE 'icn_cln_%'");

			$in->id = $out->id = 'icn_cln_' . microtime(true);
			$in->status = 'started';
			$totals = $this->getTypeCounts($in, false);

			if (!isset($in->per_page))
				$in->per_page = $this->clean_rows_per_type;

			$in->current_type = null;
			$in->force_regenerate = empty($in->force_regenerate) ? false : true;
			$in->missing_originals = [];

			if (!empty($in->types))
				foreach ($in->types as $type => $type_info){

					if ($in->current_type === null)
						$in->current_type = $type;

					$type_info = (object)[ 'sizes' => $type_info ];

					if (isset( $totals->$type )){
						$type_info->total_pages = ceil( $totals->$type / $in->per_page );
						$type_info->current_page = 0;
						$type_info->results = [];
						$type_info->status = 'waiting';
						$type_info->page_times = [];
					}

					$in->types[$type] = $type_info;

					unset($type, $type_sizes);
				}

			// add new row to DB
			if ( !$Opt->add([ 'opt' => $in->id, 'value' => json_encode($in) ]) )
				throw new \Exception('Unable to add row to DB');

			$out = (object) [
				'id' => $in->id,
				'status' => 'running',
				'message' => "Working on {$in->current_type}:  0%, calulating time remaining...",
			];

		} else {
			$parse_time = microtime(true);
			if ( empty($in->id) )
				throw new \Exception('No ID sent to image cleaner.');

			$in_db = $Opt->get((object)[ 'what' => ['value'], 'where' => [ 'opt' => $in->id ] ]);

			if (!$in_db)
				throw new \Exception('No image cleaner found with ID: ' . $in->id);
			else
				$in = json_decode($in_db[0]->value);

			unset($in_db);

			if ($in->current_type === null){
				foreach( $in->types as $type => $type_info )
					if ($type_info->status == 'running' || $type_info->status == 'waiting')
						$in->current_type = $type;

				if ($in->current_type === null){
					$in->status = 'done';
					$out->status = 'done';
					$out->data = $in;
				}
			}

			$current_type = $in->current_type;

			if (!isset($out->status)){
				$in->types->$current_type->status = 'running';
				$current_page = &$in->types->$current_type->current_page;
				$total_pages = &$in->types->$current_type->total_pages;

				switch($current_type){
					case 'listings':
						$sql = "SELECT id,images as image FROM {$Opt->getTableName('listings')} WHERE images IS NOT NULL AND active = 1";
						break;
					case 'cities':
						$sql = "SELECT id,image FROM {$Opt->getTableName('cities')} WHERE image IS NOT NULL";
						break;
					case 'authors':
						$sql = "SELECT id,photo as image FROM {$Opt->getTableName('sellers')} WHERE photo IS NOT NULL";
						break;
					case 'quiz':
						$sql = "SELECT slides.id,slides.question_id,slides.file as image FROM {$Opt->getTableName('quiz-slides')} as slides "
							. "INNER JOIN {$Opt->getTableName('quiz-questions')} as questions "
							. "ON questions.id = slides.question_id "
							. "WHERE slides.file IS NOT NULL AND (questions.enabled & 1 OR questions.enabled & 2)";
						break;
				}

				$sql.= ' LIMIT ' . ( $current_page * $in->per_page ) . ', ' . $in->per_page;

				$this->log("sql:$sql");
				$results = $Opt->rawQuery( $sql );

				// listings has many images per row, exclude the http:// images
				if (!empty($results) && $current_type == 'listings'){
					$temp = [];
					foreach ($results as $row){
						$row->image = json_decode($row->image);
						if (!empty($row->image))
							foreach ($row->image as $image)
								if ( isset($image->file) && substr($image->file, 0, 7) != 'http://' ) {
									$temp[] = (object)[
										'id' => $row->id,
										'image' => $image->file,
									];
								}

						unset($image);
					}
					$results = $temp;
				}

				if ($current_page === 0)
					array_unshift( $results, (object)[ 'id' => null, 'image' => '_blank.jpg' ]);

				if (!empty($results))
					foreach ($results as $row){
						$res = (object)[ 'id' => intval($row->id), 'status' => null, 'image' => $row->image ];
						$res->results = [];
						if ($current_type == 'quiz' && isset( $row->question_id ))
							$res->question_id = $row->question_id;

						// check if original file exists in uploaded folder
						if ( !file_exists(image_dir() . "_{$current_type}/uploaded/{$row->image}") ){
							// "uploaded" file doesn't exists
							$res->status = 404;
							$in->missing_originals[] = "{$current_type}/uploaded/{$row->image}";
							$res->results[] = "{$current_type}/uploaded/{$row->image} -- skipped, original does not exist.";
						} else {
							// "uploaded" file exists
							$res->status = 'OK';
							$res->results[] = "{$current_type}/uploaded/{$row->image} -- exists, generating sizes...";
							// set $file_path to original image, unless $type is authors, then check for cropped image first
							if ($current_type == 'authors' && file_exists( image_dir() . "_{$current_type}/cropped/{$row->image}" ) )
								$file_path = image_dir() . "_{$current_type}/cropped/{$row->image}";
							else
								$file_path = image_dir() . "_{$current_type}/uploaded/{$row->image}";

							// cycle through the sizes for this type
							foreach ($in->types->$current_type->sizes as $size_dir)
								foreach ($this->sizes[$current_type] as $size_info)
									// match the size from $this->sizes
									if ($size_dir == $size_info['dir']){

										// check if the new size image already exists, delete if force_regenerate == true
										if ( $in->force_regenerate && file_exists(image_dir() . "_{$current_type}/{$size_dir}/{$row->image}") ){
											$res->results[] = "{$current_type}/{$size_dir}/{$row->image} -- exists, deleting...";
											unlink(image_dir() . "_{$current_type}/{$size_dir}/{$row->image}");
										}

										// create the file if it doesn't exist
										if ( file_exists(image_dir() . "_{$current_type}/{$size_dir}/{$row->image}") )
											// file already exists, skip
											$res->results[] = "{$current_type}/{$size_dir}/{$row->image} -- exists, skipped.";
										else {

											try {
												// generate the single size
												$gen_size = $this->generateSingleSize( $file_path, $current_type, $size_info );
											} catch (\Exception $e) {
												print_r($e);
												die();
											}

											if ($gen_size->status == 'OK')
												$res->results[] = "{$current_type}/{$size_dir}/{$row->image} -- created.";
											else {
												$res->status = 'fail';
												$res->results[] = "ERROR: {$current_type}/{$size_dir}/{$row->image} -- unable to create size.";
											}
										}

										break;
									}
						}

						$in->types->$current_type->results = array_merge($in->types->$current_type->results, $res->results);

						unset($res, $row);
					}

				$current_page++;

				$parse_time = microtime(true) - $parse_time;
				$in->types->$current_type->page_times[] = $parse_time;

				if ($current_page === $total_pages){
					$in->types->$current_type->status = 'done';
					$in->current_type = null;
					$out->status = 1;
				} elseif ($in->types->$current_type->page_times > 0) {
					if ( count($in->types->$current_type->page_times) > 5 )
						array_slice( $in->types->$current_type->page_times, -5);
					$t = 0;
					foreach ($in->types->$current_type->page_times as $time)
						$t += $time;
					$t = $t / count($in->types->$current_type->page_times);
					$in->types->$current_type->time_remaining = round( $t * ( $total_pages - $current_page ) / 60, 1);
				}
			}

			if ( !$Opt->set([ (object)[ 'fields' => [ 'value' => json_encode($in) ], 'where' => [ 'opt' => $in->id ] ] ]) )
				throw new \Exception('Unable to update row.');


			if ( !isset($out->status) ){
				$current_percent = round(100 * $in->types->$current_type->current_page / $in->types->$current_type->total_pages, 2);
				$out = (object) [
					'id' => $in->id,
					'status' => 'running',
					'message' => "Working on {$current_type}:  {$current_percent}%, ~{$in->types->$current_type->time_remaining} minutes remaining",
				];
			} elseif ($out->status == 'done')
				$out = (object)[
					'id' => $in->id,
					'status' => 'done',
					'data' => $in,
				];
			else
				$out = (object)[
					'id' => $in->id,
					'status' => 'running',
					'message' => "{$current_type} finished, querying server for next action...",
				];
		}

		return $out;
	}

	public function generateSizes($type = null, $filepath = null, $force = false, $origName = '', $activeMode = 1){
		try {
			if ($type == null)
				throw new \Exception("No image caterory specified.");
			if ($filepath == null)
				throw new \Exception("No input file specified.");
			if (!file_exists( $filepath ))
				throw new \Exception("Input file does not exist: {$filepath}");

			$results = [];

			if (!isset($this->sizes[$type]))
				$results[] = new Out(0, "Sizes for '$type' do not exist.");
			else
				foreach($this->sizes[$type] as $size) {
					if (isset($size['active']) &&
						$size['active'] != $activeMode)
						continue;

					$results[] = $this->generateSingleSize($filepath, $type, $size, $force, $origName);
				}

			$fail = false;
			foreach ($results as $r)
				if($r->status != 'OK'){
					$fail = true;
					break;
				}

			if ($fail || count($results) < 1)
				return new Out(0, $results);
			else
				return new Out('OK', $results);
		} catch (\Exception $e) {
			return new Out(0, $e->getMessage());
		}
	}

	protected function tryImagick($orig, $scaled, $quality) {
		try {
			$this->log("About to Imagick to create size:{$scaled->size[0]}x{$scaled->size[1]} for $scaled->filename");
			$image = new \Imagick($orig->file);
			if ( $image->scaleImage($scaled->size[1], $scaled->size[0]) !== true) {
				throw new \Exception("Failed to Imagick::scaleImage for size:{$scaled->size[0]}x{$scaled->size[1]}");
			}
			$compression_type = \Imagick::COMPRESSION_JPEG;
			$image->setImageCompression($compression_type); 
			$image->setImageCompressionQuality($quality); 
			$image->stripImage();
			if (!$image->writeImage($scaled->path.$scaled->filename))
				throw new \Exception("Unable to create image: " . $scaled->path . $scaled->filename);
			$image->destroy();
			$this->log("Used Imagick to create size:{$scaled->size[0]}x{$scaled->size[1]} for $scaled->filename");

			return new Out('OK', $scaled->filename);
		}
		catch(\Exception $e) {
			throw $e;
		}
		catch(\ImagickException $e) {
			throw new \Exception($e->getMessage());
		}
	}

	public function generateSingleSize($input = null, $type = null, $size = null, $regenerate = false, $origName = ''){
		try {
			if ( !file_exists($input) )
				throw new \Exception("File does not exist: {$input}");
			if (!$size)
				throw new \Exception('Image::generateSingleSize: size does not exist.');

			// create the original (input) object
			$orig = (object)[
					'file' => $input,
					'filename' => empty($origName) ? basename($input) : basename($origName),
					'size' => [],
				];

			// create the scaled (output) object
			$scaled = (object)[
				'size' => [ $size['width'], $size['height'] ],
				'path' => image_dir() . '_'.$type.'/' . $size['dir'].'/',
				'filename' => $orig->filename,
			];

			// jpeg compression quality
			$quality = 80;

			// try to create destination directory if does not exist
			if ( !is_dir( $scaled->path ) && !mkdir( $scaled->path ) )
				throw new \Exception("Unable to create directory: {$scaled->path}");

			// check if input file has a valid size
			if (!filesize( $orig->file ))
				throw new \Exception("{$orig->file} - Unable to read.");

			if ( file_exists( $scaled->path . $scaled->filename ) ) {//&& $regenerate )
				if ( $regenerate )
					unlink( $scaled->path . $scaled->filename );
				else
					return new Out('OK', $scaled->filename);
			}
			// elseif ( file_exists( $scaled->path . $scaled->filename ) )
			// 	throw new \Exception("File already exists: " . $scaled->path . $scaled->filename);

			// get input image info
			$imageData = @getimagesize($orig->file);
			if (!empty($imageData))
				list($orig->size[0], $orig->size[1], $orig->mime) = $imageData;
			else
				throw new \Exception("{$orig->file} - Unable to read.");

			// check input valid MIME type
			switch ($orig->mime){
				case 1:
					$orig->type = 'gif';
					$orig->img = imagecreatefromgif($orig->file);
					break;
				case 2:
					$orig->type = 'jpg';
					// look for EOF
					// $orig->resource = fopen($orig->file, 'r');
					// if (0 !== fseek($orig->resource, -2, SEEK_END) || "\xFF\xD9" !== fread($orig->resource, 2)) {
				 //        fclose($orig->resource);
				 //        throw new \Exception("Invalid jpeg file: {$orig->file}");
				 //    }
				 //    fclose($orig->resource);
					if ( empty($orig->img = imagecreatefromjpeg($orig->file)) ||
						 gettype($orig->img) != 'resource' ) {
						return $this->tryImagick($orig, $scaled, $quality);
						// throw new \Exception("Failed to create jpeg from file: {$orig->file}");
					}
					break;
				case 3:
					$orig->type = 'png';
					$orig->img = imagecreatefrompng($orig->file);
					break;
				default:
					throw new \Exception("Invalid mime type: {$orig->mime}");
					break;
			}

			// scale up before cropping
			$temp = (object)[
				'size' => $orig->size[0]/$scaled->size[0] < $orig->size[1]/$scaled->size[1] ?
						[ $scaled->size[0], (int) $orig->size[1] * ( $scaled->size[0] / $orig->size[0] ) ] :
						[ (int) $orig->size[0] * ( $scaled->size[1] / $orig->size[1] ), $scaled->size[1] ],
			];
			$temp->img = imagecreatetruecolor($temp->size[0], $temp->size[1]);

			if ( !$temp->img )
				throw new \Exception("Unable to created temp image.");

			// copy from original -> temp and scale up, throw on fail
			if ( !imagecopyresampled($temp->img, $orig->img, 0, 0, 0, 0, $temp->size[0], $temp->size[1], $orig->size[0], $orig->size[1]) ) {
				if ($orig->mime != 2)
					throw new \Exception("Unable to copy original image to temp image.");

				imagedestroy($orig->img);
				imagedestroy($temp->img);
				return $this->tryImagick($orig, $scaled, $quality);
			}

			imagedestroy($orig->img);

			// create an empty image for the scaled size
			if ( !$scaled->img = imagecreatetruecolor($scaled->size[0], $scaled->size[1]) )
				throw new \Exception("Unable to create scaled image.");

			if ( !imagecopy($scaled->img, $temp->img, 0, 0, ($temp->size[0] - $scaled->size[0])/2, ($temp->size[1] - $scaled->size[1])/2, $scaled->size[0], $scaled->size[1]) )
				throw new \Exception("Unable to copy temp image to scaled image.");

			imagedestroy($temp->img);

			// setup image filetype
			
			switch ($orig->mime) {
				case 2:
					if ( !imagejpeg($scaled->img, $scaled->path.$scaled->filename, $quality) )
						throw new \Exception("Unable to create image: " . $scaled->path . $scaled->filename);
					break;
				case 3:
					if ( !imagepng($scaled->img, $scaled->path.$scaled->filename, 0) )
						throw new \Exception("Unable to create image: " . $scaled->path . $scaled->filename);
					break;
				case 4:
					if ( !imagegif($scaled->img, $scaled->path.$scaled->filename) )
						throw new \Exception("Unable to create image: " . $scaled->path . $scaled->filename);
					break;
			}

			imagedestroy($scaled->img);

			// $this->log("generateSingleSize - ".$scaled->path.$scaled->filename);

			return new Out('OK', $scaled->filename);
		} catch (\Exception $e) {
			return new Out(0, $e->getMessage());
		}
	}

	protected function getImageType(&$mime, $name) {
		$attr = 0;
		$width = $height = 0;
		if ( !($mime = exif_imagetype($name)) ) {
			list($width, $height, $mime, $attr) = getimagesize($name);
			$this->log("exif_imagetype() returned false, getimagesize() returned $mime");
		}
		$img = null;
		switch($mime) {
			//case IMG_JPEG:
			case IMAGETYPE_JPEG: $img = imagecreatefromjpeg($name); break;
			case IMAGETYPE_GIF: $img = imagecreatefromgif($name); break;
			case 3:
			case IMAGETYPE_PNG: $img = imagecreatefrompng($name); break;
			case IMAGETYPE_WBMP: $img = imagecreatefromwbmp($name); break;
			// case IMAGETYPE_XPM: $img = imagecreatefromxpm($name); break;
			case IMAGETYPE_XBM: $img = imagecreatefromxbm($name); break;
			default:
				$this->log("Invalid mime type: ".$mime." for ".$name." = JPEG:".IMAGETYPE_GIF.", GIF:".IMAGETYPE_GIF.", PNG:".IMAGETYPE_PNG.", WBMP:".IMAGETYPE_WBMP);
				throw new \Exception("Invalid mime type: ".$mime." for ".$name." = JPEG:".IMAGETYPE_GIF.", GIF:".IMAGETYPE_GIF.", PNG:".IMAGETYPE_PNG.", WBMP:".IMAGETYPE_WBMP);
				break;
		}
		return $img;
	}

	public function resize($file=null,$src_path=null,$dst_path=null,$src_width=null,$src_height=null,$dst_x=null,$dst_y=null,$dst_x2=null,$dst_y2=null, $logo=false){
		$type = 0;
		try {
			$img_r = $this->getImageType($type, image_dir().$src_path.$file);
		}
		catch( \Exception $e) {
			throw $e;
		}
		try {
			$dst_r = imagecreatetruecolor( $dst_x2-$dst_x, $dst_y2-$dst_y );
		}
		catch( \Exception $e ) {
				throw $e;
		}

		try {
			imagecopyresampled($dst_r,$img_r,0,0,$dst_x,$dst_y,
			($dst_x2-$dst_x),($dst_y2-$dst_y),($dst_x2-$dst_x),($dst_y2-$dst_y));
		}
		catch( \Exception $e ) {
				throw $e;
		}

		// try to create destination directory if does not exist
		if ( !is_dir( image_dir().$dst_path ) && !mkdir( image_dir().$dst_path ) )
			throw new \Exception("Unable to create directory: {image_dir().$dst_path}");

		try {
			if (!imagejpeg($dst_r, image_dir().$dst_path.$file, 95)) return new Out(0, 'Image::resize: unable to resize image.');
			else {
				$filename = $this->generateFilename($file);
				rename(image_dir().$src_path.$file, image_dir().$src_path.$filename);
				rename(image_dir().$dst_path.$file, image_dir().$dst_path.$filename);
				try {
					if (!$logo)
						$x = $this->generateSizes('authors', image_dir().$dst_path.$filename, true);
					else {
						unlink(image_dir().$src_path.$filename); // get rid of old
						rename(image_dir().$dst_path.$filename, image_dir().$src_path.$filename); // move from cropped folder to destination
						$x = (object)['status'=>'OK'];
					}
				}
				catch( \Exception $e ) {
						throw $e;
				}
				if ($x->status != 'OK') return $x; else return new Out('OK', $filename);
			}
		}
		catch( \Exception $e ) {
				throw $e;
		}
	}
	/**
	 * called by Dropzone.js on image upload
	 * @param  [array] $files [list of files]
	 * @param  [string] $dir   [directory under _img/ to store]
	 * @return [type]        [description]
	 */
	public function exitNow($out) {
		ignore_user_abort(true); // just to be safe
		set_time_limit(0);
		ob_start();
		echo json_encode($out);
		$size = ob_get_length();
        header("Connection: close\r\n");
		header("Content-Length: $size\r\n");
		header("Content-Encoding: none\r\n" );//disable apache compressed

		ob_end_flush(); // Strange behaviour, will not work
		ob_flush();
		flush(); // Unless both are called !
	}

	public function uploadCallback($files = null, $dir = null){ // called by Dropzone on image upload
		try {
			if ( empty($files) ) {
				$this->log("uploadCallback - No files sent to Image.class.php");
				throw new \Exception("No files sent to Image.class.php");
			}

			if ( empty($dir) ) {
				$this->log("uploadCallback - No directory to Image.class.php");
				throw new \Exception("No directory to Image.class.php");
			}

			// make directory if it doesn't exist
			if (!is_dir( image_dir() . $dir ))
				mkdir( image_dir() . $dir );

			$destDir = realpath( rtrim(image_dir() . $dir, '/') );
			$filenames = [];

			$this->log("uploadCallback - ".count($files)." files to process");
			foreach ($files as $file){
				// generate the filename
				if ($file['error'] == 1)
					throw new \Exception("File is too big, please reduce the size");

				$file['name'] = preg_replace('/[^a-zA-Z0-9_.]/', '', $file['name']);
				$filename = $this->generateFilename( $file['name'] );
				$this->log("uploadCallback - processing {$file['name']}, generated filename:$filename, files:".print_r($files, true));


				// move the file, throw on fail
				$dest = ($destDir . '/' . $filename);
				$this->log("about to move file:".$file['tmp_name']." to dest:$dest");
				if ( !move_uploaded_file($file['tmp_name'], $dest ) ) {
					$this->log("Failed to move file to $dest");
					throw new \Exception("Unable to move file to ".$dest);
				}
				$filenames[] = $filename;
			}

			// treat listing images special, so we process the image displayed in the new-listings page first and return
			$doingFastExit = false;
			if (strpos($dir,'listings') !== false) {
				foreach($filenames as $filename) {
					try {
						$gen = $this->generateSizes('listings', image_dir().$dir.$filename, true, '', 2);
						if ( !($gen && $gen->status == 'OK') ) {
							$this->log("failed in generateSizes for $filename:".print_r($gen, true));
							return $gen;
						}
					}
					catch( \Exception $e) {
						return parseException($e);
					}
				}
				$out = new Out('OK', $filenames);
				$this->log("uploadCallback - exitNow for listings");
				$this->exitNow($out);
				$doingFastExit = true;
			}

			foreach($filenames as $filename) {
				// figure out the type from the directory
				$gotOne = false;
				foreach (['quiz', 'listings', 'authors', 'cities'] as $type)
					if (strpos($dir,$type) !== false){
						$gotOne = true;
						try {
							$gen = $this->generateSizes($type, image_dir().$dir.$filename, true);
							if ($gen && $gen->status == 'OK') {
								$this->log("uploadCallback - returning $filename");
								if ($doingFastExit)
									die;
								return new Out('OK', [ $filename ]);
							}
							else {
								$this->log("failed in generateSizes for $filename:".print_r($gen, true));
								if ($doingFastExit)
									die;
								return $gen;
							}
						}
						catch( \Exception $e) {
							if ($doingFastExit)
								die;
							return new Out(0, $e->getMessage());
						}
						break;
					}

				if (!$gotOne) // then just moving it..
					return new Out('OK', [ $filename ]);

			}
		} catch (\Exception $e) {
			$this->log("uploadCallback - exception:".$e->getMessage());
			if ($doingFastExit)
				die;
			return new Out(0, $e->getMessage());
		}
	}

	public function sameColor($pix1, $pix2, $tolerance = 0.1) {
		$r1 = ($pix1 >> 16) & 0xFF;
		$g1 = ($pix1 >> 8) & 0xFF;
		$b1 = $pix1 & 0xFF;

		$r2 = ($pix2 >> 16) & 0xFF;
		$g2 = ($pix2 >> 8) & 0xFF;
		$b2 = $pix2 & 0xFF;

		$ok = true;
		$denominator = $r1 != 0 ? $r1 : ($r2 != 0 ? $r2 : false); // safe guard against pure black
		if ($denominator !== false)
			$ok = abs($r1 - $r2)/$denominator <= $tolerance;

		if ($ok) {
			$denominator = $g1 != 0 ? $g1 : ($g2 != 0 ? $g2 : false); // safe guard against pure black
			if ($denominator !== false)
				$ok = abs($g1 - $g2)/$denominator <= $tolerance;
		}
		if ($ok) {
			$denominator = $b1 != 0 ? $b1 : ($b2 != 0 ? $b2 : false); // safe guard against pure black
			if ($denominator !== false)
				$ok = abs($b1 - $b2)/$denominator <= $tolerance;
		}
		return $ok;
	}
							// current avg, new pixel
	public function makeAverage(&$pix1, $pix2) {
		if ($pix1 == 0) {
			$pix1 = $pix2;
			return;
		}
		$r1 = ($pix1 >> 16) & 0xFF;
		$g1 = ($pix1 >> 8) & 0xFF;
		$b1 = $pix1 & 0xFF;

		$r2 = ($pix2 >> 16) & 0xFF;
		$g2 = ($pix2 >> 8) & 0xFF;
		$b2 = $pix2 & 0xFF;

		$r1 = ($r1 + $r2)/2;
		$g1 = ($g1 + $g2)/2;
		$b1 = ($b1 + $b2)/2;

		$pix1 = ($r1 << 16)+($g1 << 8)+$b1;
	}

	public function hasWhiteBorder($tmpName, &$img, $origName, &$top, &$bottom) {
	  	list($width, $height, $type, $attr) = getimagesize($tmpName);
	  	switch($type){
	  		case IMG_JPEG:
			case IMG_JPG: $img = imagecreatefromjpeg($tmpName); break;
			case IMG_GIF: $img = imagecreatefromgif($tmpName); break;
			case IMG_PNG: $img = imagecreatefrompng($tmpName); break;
			case IMG_WBMP: $img = imagecreatefromwbmp($tmpName); break;
			case IMG_XPM: $img = imagecreatefromxpm($tmpName); break;
		}

		// if ( ($handle = fopen($name, 'rb')) == false)
		// 	return false;

		$haveBorder = false;
		$different = false;
		$failures = 0;
		$average = 0;
		$avgCounter = 0;
		$closeEnoughToEdge = floor( $width / 50 );
		for($h = 0; $h < $height; $h++) {
			$prevColor = null;
			$failures = 0;
			for($w = 0; $w < $width; $w++) {
				$color = imagecolorat($img, $w, $h);
				if ($prevColor) {
					$checkColor = $avgCounter == 5 ? $average : $prevColor;
					if (!$this->sameColor($checkColor,$color)) {
						$failures++;
						if ($failures == 5) {
							if ( $w <= $closeEnoughToEdge ) // if the error is within allowable tolerance to edge, let it slide
								$prevColor = 0;
							else {
								$different = true;
								break;
							}
						}
					}
					elseif ($avgCounter < 5) {
						$this->makeAverage($average, $prevColor);
						$avgCounter++; // don't want it to go over 5
						$failures = 0;
					}
					else
						$failures = 0;
				}
				//$this->copyArray($color, $prevColor);
				$prevColor = $color;
			}
			if ($different)
				break;
		}
		if ($h != 0) // different pixel in first row, no band
			$haveBorder = true;

		$top = $h; // let's assume here that we have $h rows of same color
		// now check from bottom
		$different = false;
		$failures = 0;
		$average = 0;
		$avgCounter = 0; // in case the other side is a different border color
		for($h = $height-1; $h >= 0; $h--) {
			$prevColor = null;
			$failures = 0;
			for($w = 0; $w < $width; $w++) {
				$color = imagecolorat($img, $w, $h);
				if ($prevColor) {
					$checkColor = $avgCounter == 5 ? $average : $prevColor;
					if (!$this->sameColor($checkColor,$color)) {
						$failures++;
						if ($failures == 5) {
							if ( ($width - $w) <= $closeEnoughToEdge ) // if the error is within allowable tolerance to edge, let it slide
								$prevColor = 0;
							else {
								$different = true;
								break;
							}
						}
					}
					elseif ($avgCounter < 5) {
						$this->makeAverage($average, $prevColor);
						$avgCounter++; // don't want it to go over 5
						$failures = 0;
					}
					else
						$failures = 0;
				}
				//$this->copyArray($color, $prevColor);
				$prevColor = $color;
			}
			if ($different)
				break;
		}
		if ($h != $height-1) // different pixel in first row, no band
			$haveBorder = true;

		$bottom = $height - $h - 1; // let's assume here that we have $h rows of same color

		// if ($haveBorder)
		// 	$this->log("Has side borders: top:$top, bottom:$bottom");
		//$imagick = $img;
		//fclose($handle);
		return $haveBorder;
	}

	public function hasWhiteBorderSides($tmpName, &$img, &$origName, &$left, &$right) {
	  	list($width, $height, $type, $attr) = getimagesize($tmpName);
	  	switch($type){
	  		case IMG_JPEG:
			case IMG_JPG: $img = imagecreatefromjpeg($tmpName); break;
			case IMG_GIF: $img = imagecreatefromgif($tmpName); break;
			case IMG_PNG: $img = imagecreatefrompng($tmpName); break;
			case IMG_WBMP: $img = imagecreatefromwbmp($tmpName); break;
			case IMG_XPM: $img = imagecreatefromxpm($tmpName); break;
		}

		// if ( ($handle = fopen($name, 'rb')) == false)
		// 	return false;

		$haveBorder = false;
		$different = false;
		$failures = 0;
		$average = 0;
		$avgCounter = 0;
		$closeEnoughToEdge = floor( $height / 50 );
		for($w = 0; $w < $width; $w++) {
			$prevColor = null;
			$failures = 0;
			for($h = 0; $h < $height; $h++) {
				$color = imagecolorat($img, $w, $h);
				if ($prevColor ) {
					$checkColor = $avgCounter == 5 ? $average : $prevColor;
					if (!$this->sameColor($checkColor,$color)) {
						$failures++;
						if ($failures == 5) {
							if ( $h <= $closeEnoughToEdge ) // if the error is within allowable tolerance to edge, let it slide
								$prevColor = 0;
							else {
								$different = true;
								break;
							}
						}
					}
					elseif ($avgCounter < 5) {
						$this->makeAverage($average, $prevColor);
						$avgCounter++; // don't want it to go over 5
						$failures = 0;
					}
					else
						$failures = 0;
				}
				//$this->copyArray($color, $prevColor);
				$prevColor = $color;
			}
			if ($different)
				break;
		}
		if ($w != 0) // different pixel in first row, no band
			$haveBorder = true;

		$left = $w; // let's assume here that we have $h rows of same color
		// now check from bottom
		$different = false;
		$failures = 0;
		$average = 0;
		$avgCounter = 0; // in case the other side is a different border color
		for($w = $width-1; $w >= 0; $w--) {
			$prevColor = null;
			$failures = 0;
			for($h = 0; $h < $height; $h++) {
				$color = imagecolorat($img, $w, $h);
				if ($prevColor ) {
					$checkColor = $avgCounter == 5 ? $average : $prevColor;
					if (!$this->sameColor($checkColor,$color)) {
						$failures++;
						if ($failures == 5) {
							if ( ($height - $h) <= $closeEnoughToEdge ) // if the error is within allowable tolerance to edge, let it slide
								$prevColor = 0;
							else {
								$different = true;
								break;
							}
						}
					}
					elseif ($avgCounter < 5) {
						$this->makeAverage($average, $prevColor);
						$avgCounter++; // don't want it to go over 5
						$failures = 0;
					}
					else
						$failures = 0;
				}
				//$this->copyArray($color, $prevColor);
				$prevColor = $color;
			}
			if ($different)
				break;
		}
		if ($w != $width-1) // different pixel in first row, no band
			$haveBorder = true;

		$right = $width - $w - 1; // let's assume here that we have $h rows of same color

		// if ($haveBorder)
		// 	$this->log("Has side borders: left:$left, right:$right");
		//$imagick = $img;
		//fclose($handle);
		return $haveBorder;
	}


	public function trimTopBottom($img, $top, $bottom) {
		$height = imagesy($img);
		$width = imagesx($img);

		$rect = array('x'=>0,
					  'y'=> $top,
					  'width'=>$width,
					  'height'=>($height - $top - $bottom));
		$cropped = imagecrop($img, $rect);
		return $cropped;
	}

	public function blackOutTopBottom(&$img, $top, $bottom) {
		$height = imagesy($img);
		$width = imagesx($img);

		$fill = imagecolorallocate($img, 0, 0, 0);
		if ($top)
			imagefilledrectangle($img, 0, 0, $width-1, $top -1, $fill);
		if ($bottom)
			imagefilledrectangle($img, 0, $height-$bottom, $width-1, $height-1, $fill);
	}

	public function trimLeftRight($img, $left, $right) {
		$height = imagesy($img);
		$width = imagesx($img);

		$rect = array('x'=>$left,
					  'y'=> 0,
					  'width'=>$width - $left - $right,
					  'height'=>$height - 1);
		$cropped = imagecrop($img, $rect);
		return $cropped;
	}

	public function blackOutLeftRight(&$img, $left, $right) {
		$height = imagesy($img);
		$width = imagesx($img);

		$fill = imagecolorallocate($img, 0, 0, 0);
		if ($left)
			imagefilledrectangle($img, 0, 0, $left, $height -1, $fill);
		if ($right)
			imagefilledrectangle($img, $width - $right, 0, $width-1, $height-1, $fill);
	}

	public function unlinkListingImages(&$images) {
		if (!empty($images)) foreach($images as $image) {
			$this->unlinkListingImage($image, true);
			unset($image);
		}
	}

	public function unlinkListingImage($image, $logIt = false) {
		if (!empty($image)) {
			if (isset($image->file) && 
				!empty($image->file)  && 
				substr($image->file, 0, 4 ) != 'http') {// then have a processed image
				foreach($this->sizes['listings'] as $loc) {
					$path = image_dir().'_listings/'.$loc['dir'].'/'.$image->file;
					if (file_exists($path)) {
						unlink($path);
						if ($logIt) $this->log("unlinkListingImage - $path");
					}
					unset($loc);
				}
				$path = image_dir().'_listings/uploaded/'.$image->file;
				if (file_exists($path)) {
					unlink($path);
					if ($logIt) $this->log("unlinkListingImage - $path");
				}
			}
		}
	}

	public function unlinkOldImages() {
		// close connection
		ignore_user_abort(true); // just to be safe
		ob_start();
		$out = array("status" => 'OK',
					"data"=>"Begin cleaning out img folder.");
		echo( json_encode($out) );
		$size = ob_get_length();
		header("Connection: close\r\n");
		header("Content-Length: $size\r\n");
		header("Content-Encoding: none\r\n");//disable apache compressed
		ob_end_flush(); // Strange behaviour, will not work
		flush(); // Unless both are called !

		for($i = 1; $i < 100; $i++) {
			foreach($this->sizes['listings'] as $loc) {
				$path = image_dir().'_listings/'.$loc['dir'].'/'.$i."_lm_*";
				$handle = popen('rm '.$path, "r");
				if ($handle) {
					$this->log("unlinkOldImages - $path");
					pclose($handle);
				}
			}
			break;
		}
		$this->log("unlinkOldImages done");
		die();
	}

	private function generateFilename($filename){
		$maxFilenameLength = 32;
		$input = pathinfo($filename);
		$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\]]{2,})", '', $input['filename']);
		$filename = substr($filename, 0, strlen($filename)>$maxFilenameLength?$maxFilenameLength:strlen($filename));
		$extension = strtolower($input['extension']);
		if ($extension == 'jpeg') $extension = 'jpg';
		$time = time();
		$filename = str_replace(" ", "_", $filename.'_'.$time.'_'.strlen((string)$time).'.'.$extension);
		return $filename;
	}
	private $sizes = array(
		'authors'=>array(
			array('dir'=>'450x450','width'=>450,'height'=>450), // seller admin - my profile new page
			array('dir'=>'420x260','width'=>420,'height'=>260), // seller admin - my profile old page
			array('dir'=>'272x272','width'=>272,'height'=>272), // profile
			array('dir'=>'250x250','width'=>250,'height'=>250), // listing
			array('dir'=>'55x55','width'=>55,'height'=>55) // nav bar
		),
		'cities'=>array(
			array('dir'=>'845x350','width'=>845,'height'=>350), // quiz results + setup wizard bg images
			array('dir'=>'300x300','width'=>300,'height'=>300, 'active' => 0), // default
			array('dir'=>'290x180','width'=>290,'height'=>180, 'active' => 0), // quiz-results popup
			array('dir'=>'100x70','width'=>100,'height'=>70), // quiz-results infoWindow popup
			array('dir'=>'15x15','width'=>15,'height'=>15), // admin
		),
		// active: 0 = don't use, 1 = use, 2 = do first (for uploadCallback when _listing images are uploaded so we can return right after this one is done)
		"listings" => array(
			array('dir'=>'1440x714','width'=>1440, 'height'=>714, 'active' => 1), // home page
			array('dir'=>'1440x575','width'=>1440,'height'=>575, 'active' => 2), // main profile + setup wizard + listings main
			array('dir'=>'900x500','width'=>900,'height'=>500, 'active' => 1), // listing gallery bg image
			//array('dir'=>'918x350','width'=>918,'height'=>350), // quiz results + setup wizard bg images
			array('dir'=>'845x350','width'=>845,'height'=>350, 'active' => 1), // quiz results + setup wizard bg images
			array('dir'=>'210x120','width'=>210,'height'=>120, 'active' => 1), // listing gallery thumbs
			array('dir'=>'158x64','width'=>158, 'height'=>64, 'active' => 1), // home page thumb
			array('dir'=>'100x70','width'=>100,'height'=>70, 'active' => 1), // seller admin - photos page
			array('dir'=>'80x80','width'=>80,'height'=>80, 'active' => 1) // admin backend
		),
		'quiz' => array(
			array('dir'=>'600x250','width'=>600,'height'=>250), // quiz admin large
			array('dir'=>'280x715','width'=>280,'height'=>715, 'active' => 0), // 3-type
			array('dir'=>'280x325','width'=>280,'height'=>325, 'active' => 0), // 6-type
			array('dir'=>'280x175','width'=>280,'height'=>175, 'active' => 0), // 12-type
			array('dir'=>'350x450','width'=>350,'height'=>450, 'active' => 1), // 3-type new
			array('dir'=>'325x265','width'=>325,'height'=>265, 'active' => 1), // 6-type new
			array('dir'=>'265x170','width'=>265,'height'=>170, 'active' => 1), // 12-type new
			array('dir'=>'25x50','width'=>25,'height'=>50), // quiz admin small
			array('dir'=>'285x275','width'=>285,'height'=>275, 'active' => 0), // mobile-type
			array('dir'=>'280x215','width'=>280,'height'=>215, 'active' => 0), // mobile-type
			array('dir'=>'325x340','width'=>325,'height'=>340, 'active' => 0), // mobile-type
			array('dir'=>'350x520','width'=>350,'height'=>520, 'active' => 0), // mobile-type
			array('dir'=>'290x275','width'=>290,'height'=>275, 'active' => 0), // mobile-type new
			array('dir'=>'580x550','width'=>580,'height'=>550, 'active' => 1), // mobile-type new
			array('dir'=>'700x900','width'=>700,'height'=>900, 'active' => 1), // 3-type retina
			array('dir'=>'650x530','width'=>650,'height'=>530, 'active' => 1), // 6-type retina
			array('dir'=>'530x340','width'=>530,'height'=>340, 'active' => 1), // 12-type retina

			array('dir'=>'534x400','width'=>534,'height'=>400, 'active' => 1) // predefined quiz
		)
	);
}
if ( empty($_POST['query']) ) {
	if (!empty($_FILES) && 
		(!empty($_SERVER['HTTP_IMAGE_DIR']) ||
		 !empty($_COOKIE['IMAGE_DIR']))) {
		$x = new Image();
		$dir = !empty($_SERVER['HTTP_IMAGE_DIR']) ? $_SERVER['HTTP_IMAGE_DIR'] : 
				(isset($_COOKIE['IMAGE_DIR']) ? $_COOKIE['IMAGE_DIR'] :
				(isset($_POST['image_dir']) ? $_POST['image_dir'] :
				(isset($_GET['image_dir']) ? $_GET['image_dir'] : '')));
		$x->log("calling uploadCallback for $dir");
		try {
			$out = $x->uploadCallback($_FILES, $dir);
			// $x->log("returning from uploadCallback:".print_r($out, true));
			echo json_encode($out);
		}
		catch( \Exception $e) {
			echo parseException($e);
		}
		die;
	}
	// else {
	// 	if (empty($_FILES))
	// 		$out = new Out('fail', "FILES is empty!");
	// 	else
	// 		$out = new Out('fail', "HTTP_IMAGE_DIR is missing - ".print_r($_SERVER, true));

	// 	echo json_encode($out);
	// }
	// else echo json_encode(new Out(0, array(
	// 	'server' => $_SERVER,
	// 	'message' => 'Unable to upload files.',
	// 	'files' => $_FILES,
	// 	'files_is_empty' => empty($_FILES),
	// 	'image_dir' => $_SERVER['HTTP_IMAGE_DIR'],
	// 	'image_dir_is_empty' => empty($_SERVER['HTTP_IMAGE_DIR'])
	// 	)) );
} else {
	new Image( $_POST['query'], (!empty($_POST['data'])?$_POST['data']:null) );
}
