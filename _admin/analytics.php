<?php
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Analytics.class.php'); $Analytics = new AH\Analytics(); 

$eventTypes = (object)[
	ANALYTICS_TYPE_CLICK => 'click',
	ANALYTICS_TYPE_EVENT => 'event',
	ANALYTICS_TYPE_PAGE_ENTRY_INITIAL => 'siteEntry',
	ANALYTICS_TYPE_PAGE_ENTRY => 'pageEntry',
	ANALYTICS_TYPE_PAGE_EXIT => 'pageExit',
	ANALYTICS_TYPE_SLIDER => 'slider',
	ANALYTICS_TYPE_ERROR => 'error'
];

$originTypes = [
	'home',
	'about',
	'contact',
	'terms',
	'portal-landing',
	'portal-landing-one',
	'quiz',
	'quiz-results',
	'listing',
	'explore-the-area',
	'explore-the-area-new',
	'register',
	'seller-create',
	'faq',
	'agent',
	'cart',
	'product',
	'my-account',
	'checkout',
	'agent-benefits',
	'agent-reservation',
	'agent-reserve-portal',
	'agent-purchase-portal',
	'directory',
	'sellers',
	'new-listing',
];

$whatTypes = [
	'new-portal-user',
	'email-only-captured',
	'Quiz Start',
	'Quiz Search',
	'QuizStartNationwide',
	'QuizStartStatewide',
	'QuizStartLocalized',
	'SEO',
	'Next',
	'Prev',
	'Skip',
	'SelectSlide',
	'DeselectSlide',
	'SelectTag',
	'DeselectTag',
	'DeselectSlide',
	'SlideTag',
	'RemoveTag',
	'SetMin',
	'SetMax',
	'Listing',
	'SignIn Register',
	'Agent Benefits'
];

$getWhat = [
	'id',
	'session_id',
	'added',
	'type',
	'origin',
	'what',
	'value_str',
	'value_int',
	'referer',
	'browser',
	'ip'
];

$customSQL = [
//	'visitAllSql',
	'visitAllSqlWithSessionId',
	'pageInitialEntrySql',
	'portalUsage'
];

$visitPages = [
	'visitAll',
	'visitPortal',
	'visitPortalOne',
	'visitQuiz',
	'visitQuizResults',
	'pageInitialEntrySql',
	'portalUsage'
];

$browser = AH\getBrowser();

?>
<script>
var eventTypes = <?php echo json_encode($eventTypes); ?>;
var originTypes = <?php echo json_encode($originTypes); ?>;
var whatTypes = <?php echo json_encode($whatTypes); ?>;
var isDst = <?php echo AH\isDST() ? 1 : 0; ?>;
var browser = <?php echo json_encode( $browser ); ?>;
var visitPages = <?php echo json_encode( $visitPages ); ?>;
</script>

<div id="overlay-bg" style="display: none;">
	<div class="spin-wrap"><div class="spinner sphere"></div></div>
</div>

<section id="selections">
	<div id="dates">
		<label id="from">From:</label><input type="text" id="date_from" value="">&nbsp;&nbsp;<span>-</span>&nbsp;&nbsp;<label id="from">To:</label><input type="text" id="date_to" value="">
		&nbsp;&nbsp;<button id="resetToNow">Set to Now</button>
	</div>
	<form id="selections" onsubmit="analytics.submitRequest(); return false;">
		<div id="elementsDiv">
			<span id="instructions">Choose All for all types in the category, or select types you want to see.  Use Alt-click or Command-click for multiple selections</span>
			<div id="eventDiv">
				<table>
					<tbody>
						<th>
							Events:
						</th>
						<tr>
							<!-- <td><input id="allEvents" type="checkbox">All</input></td> -->
							<td>
								<span id="label">Select</span>&nbsp;
								<select id="events" multiple></select>
							</td>
					</tbody>
				</table>
			</div>
			<div id="originDiv">
				<table>
					<tbody>
						<th>
							Origins:
						</th>
						<tr>
							<!-- <td><input id="allOrigins" type="checkbox">All</input></td> -->
							<td>
								<span id="label">Select</span>&nbsp;
								<select id="origins" multiple></select>
							</td>
					</tbody>
				</table>
			</div>
			<div id="whatDiv">
				<table>
					<tbody>
						<th>
							Actions:
						</th>
						<tr>
							<!-- <td><input id="allWhats" type="checkbox">All</input></td> -->
							<td>
								<span id="label">Select</span>&nbsp;
								<select id="whats" multiple></select>
							</td>
					</tbody>
				</table>
			</div>
			<div id="getWhat">
				<span id="label">Select columns to retrieve:</span>
				<?php
				foreach($getWhat as $what) {
					echo '<div class="item" id="'.$what.'">';
					echo '<span>'.$what.'</span>';
					echo '<input type="checkbox" id="'.$what.'" ></input>';
					echo '</div>';
				}

				?>
			</div>
		</div>
		<input type="submit" value="Submit">
	</form>
	<div id="customSQLs">
		<span>Custom SQL (define time range!!)</span>
		<?php
		foreach($customSQL as $sql)
			echo '<button id="'.$sql.'">'.$sql.'</button>';
		?>
		<div id="visitPages">
			<span>View individual pages from Custom SQL</span>
			<?php
			foreach($visitPages as $sql)
				echo '<button id="'.$sql.'" class="hidden">'.$sql.'</button>';
			?>
		</div>
	</div>
	<div id="tableDiv">
		<table>
			<thead></thead>
			<tbody></tbody>
		</table>
	</div>
</section>
