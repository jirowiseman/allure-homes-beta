<?php
require_once(__DIR__.'/../_classes/Tables.class.php');
global $wpdb;
$curPage = 0;
$pageSize = 40;
//$last = $wpdb->get_results("SELECT * FROM ".getTableName('listings')." ORDER BY `id` DESC LIMIT 1");
//$x = $wpdb->get_results('SELECT id, title, author, price, beds, baths FROM '.getTableName('listings-rejected')." LIMIT $pageSize");
//$authors = $wpdb->get_results('SELECT * FROM '.getTableName('sellers'));
// usort($authors, function($a, $b){
// 	return $a->last_name > $b->last_name;
// });
// echo get_templete_directory_uri();
?>
<script type="text/javascript">ah_local = {
	tp: '<?php echo get_template_directory_uri(); ?>',
	wp: '<?php echo get_home_url(); ?>',
	user: '<?php echo wp_get_current_user()->ID; ?>'
}</script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/_js/jquery.bxslider.min.js" type="text/javascript"></script>

<style>

</style>
<!-- <p><a href="<?php bloginfo('wpurl'); ?>/sellers/new-listing/">Create New Listing</a></p> -->
<div id="activeChooser">
	<label>InActive:</label> <input type="radio" name="table" id="inactive" /><span> </span>
	<label>Active:</label> <input type="radio" name="table" id="live" /><span> </span>
	<label>Waiting:</label> <input type="radio" name="table" id="waiting" style="margin-top: -4px"/><span> </span>
	<label>Rejected:</label> <input type="radio" name="table" id="rejected" /><span> </span>
	<label>TooCheap:</label> <input type="radio" name="table" id="tooCheap" /><span> </span>
	<label>Never:</label> <input type="radio" name="table" id="never" /><span> </span>
	<label>Average:</label> <input type="radio" name="table" id="average" /><span><br/></span>
</div>
<div id="optionWrapper">
	<div class="options">
		<div id="optionChooser">
			<label>Use specifier</label><span> </span><input type="checkbox" class="optionActive" />
			<div id="sellerOption">
				<label>Seller:</label> <input type="radio" name="option" id="seller" /><span> </span><input type="button" id="submitName" value="Submit" />
				<table class="sellerID">
					<thead>
						<tr>
							<th scope="col" class="manage-column column-first-name">First</th>
							<th scope="col" class="manage-column column-last-name">Last</th>
						</tr>
						<tr>
							<td><input type="text" id="firstName"/></td>
							<td><input type="text" id="lastName"/> </td>
						</tr>
					</thead>
				</table>
			</div>
			<div id="addressOption">
				<label>Address:</label> <input type="radio" name="option" id="address" /><span> </span><input type="button" id="submitAddress" value="Submit" />
				<table class="address">
					<thead>
						<tr>
							<th scope="col" class="manage-column column-street">Street</th>
							<th scope="col" class="manage-column column-city">City</th>
							<th scope="col" class="manage-column column-state">State</th>
						</tr>
						<tr>
							<td><input type="text" id="street"/></td>
							<td><input type="text" id="city"/> </td>
							<td><input type="text" id="state"/> </td>
						</tr>
					</thead>
				</table>
			</div>
		</div>

		<div id="keywordOption">
			<label id="keywordLabel">Enter Keywords (separated by commas)</label><br/>
			<input type="text" placeHolder="Enter a keyword to search in the description" id="keyword"><span>  </span><button id="applyKeyword">Apply</button><br/>
		    <div id="container"><label id="untagLabel">Remove undesired tags using defined list: </label><button id="cleanTags">Clean tags</button></div>
		    <div id="container"><label id="geocode">Geocode non-listhub active listings: </label><button id="geocode">Geocode them</button></div>
			<div class="spin-wrap">
		        <div class="spinner">
		        <div class="cube1"></div>
		        <div class="cube2"></div>
		    </div></div>
		</div>
	</div>
</div>

<div id="priceOption">
	<!-- <label>Min Price:</label> <input type="checkbox" name="price" id="price" /> -->
	<span id="priceValue"></span><span> </span><div id="priceSlider"></div>
</div>

<div id="infoBar">
	<span class="tally"></span>
	<span id="legend">   Error Code Legend: 1 = Too Cheap, 2 = Lack Desc, 3 = No Images, 4 = No Bed/Baths, 5 = Commercial, 6 = No Tags</span>
</div>

<table class="widefat">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-listing-id"><a href="javascript:listingAdmin.sortId()" target="_blank">ID</a></th>
			<th scope="col" class="manage-column column-price"><a href="javascript:listingAdmin.sortPrice()" target="_blank">Price</a></th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-error">Error</th>
			<th scope="col" class="manage-column column-tags"># Tags</th>
			<th scope="col" class="manage-column column-tags">Home Img</th>
			<th scope="col" class="manage-column column-image">Image</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-price">Price</th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-error">Error</th>
			<th scope="col" class="manage-column column-tags"># Tags</th>
			<th scope="col" class="manage-column column-tags">Home Img</th>
			<th scope="col" class="manage-column column-image">Image</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</tfoot>
	<tbody>

	</tbody>
</table>
<div id="controls"> 
	<input type="button" id="prevButton" value="Previous"><span> </span><input type="button" id="nextButton" value="Next"><span> </span><input type="button" id="applyChanges" value="Apply"><span> </span><span id="sliderValue"></span><span> </span><div id="pageSlider"></div>
</div>

