<?php
require_once(__DIR__.'/../_classes/Tables.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/Reservations.class.php'); $Reservations = new AH\Reservations(); 
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(1);
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options;
require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new AH\Cities;
require_once(__DIR__.'/../_classes/CitiesTags.class.php'); $CitiesTags = new AH\CitiesTags;
global $Tags;
require_once(__DIR__.'/../_classes/Tags.class.php'); $Tags = new AH\Tags;
// require_once(__DIR__.'./../_classes/TagsCategories.class.php'); $TagsCategories = new AH\TagsCategories(); $TagsCategories = $TagsCategories->get();
// require_once(__DIR__.'./../_classes/TagsTaxonomy.class.php'); $TagsTaxonomy = new AH\TagsTaxonomy(); $TagsTaxonomy = $TagsTaxonomy->get();
require_once(__DIR__.'/../_classes/RadialDistance.php');

global $wpdb;
global $RadialDistance;
global $browser;
$browser = AH\getBrowser();
// $curPage = 0;
// $pageSize = 40;
global $amTags;
$amTags = $Sellers->getAMTagList();

function makeTagSet(&$cities, $city_id, $city, $lat, $lng) {
	global $amTags;
	$set = [];
	
	$cities[$city_id] = (object)['locationStr'=>AH\removeslashes($city),
								 'city_id'=>$city_id,
								 'lat'=>$lat,
								 'lng'=>$lng,
								 'tags'=>$set];
	unset($set);
}

$Sellers->log("amTags:".count($amTags).", RadialDistance:".count($RadialDistance[0]));
if ( (count($amTags) != count($RadialDistance[0])) ) { // remake it!
	require_once(__DIR__.'/../_classes/DistanceScale.php');
	$steps = count($amTags);
	$RadialDistance = [];
	$radialListStr = '[';
	foreach($DistanceScale as $index=>$scale) {
		$radials = [];
		$radialsStr = '[';
		$deltaLng = 1/$DistanceScale[$index][0];
		$deltaLat = 1/$DistanceScale[$index][1]; // pretty constant
		for($i = 0; $i < $steps; $i++) {
			$rad = (360/$steps) * (M_PI/180) * $i;
			$x = cos( $rad ) * $deltaLng;
			$y = sin( $rad ) * $deltaLat;
			$radials[] = [$x, $y];
			$radialsStr .= "[$x,$y],";
		}
		$radialsStr .= ']';
		$RadialDistance[] = $radials;
		$radialListStr .= $radialsStr.",\n";
		unset($scale, $radials);
	}
	$radialListStr .= '];';
	// $out = new Out('OK', ['txt'=>print_r($radialList, true)]);
	$file = __DIR__.'/../_classes/RadialDistance.php';
	$fileContents = "<?php\nnamespace AH;\n// lng, lat arrays per 5 degrees starting from 5, 15, 25, etc...,  each containing stepped radial offsets for # of agent match tags we have from Sellers::getAMTagList()\nglobal ".'$'."RadialDistance;\n".'$'."RadialDistance = $radialListStr";
	$Sellers->log("sales.php - whoami:".\get_current_user().", file:$file");
	@file_put_contents($file, $fileContents);
}



// get all reservations that still ha
// 
$sql = "SELECT * FROM {$Reservations->getTableName()} WHERE `type` & ".(SELLER_IS_PREMIUM_LEVEL_2).' AND (!(`flags` & '.(RESERVATION_CONVERTED_LIFESTYLE | RESERVATION_CONVERTED_LIFESTYLE_CODE).') OR `flags` IS NULL)';
$Sellers->log("Sales page get reservations sql: $sql");
$reservations = $Reservations->rawQuery($sql);
$reservationCollation = [];

$x = $reservations;
$reservations = [];
foreach($x as $res)
	$reservations[$res->id] = $res;
unset($x);

foreach($reservations as &$res) {
	if (!empty($res->meta)) {
		$res->meta = json_decode($res->meta);
		$metas = [];
		$needUpdate = false;
		$lastBadLocation = -1;
		$lastFixedLocation = -1;
		foreach($res->meta as &$meta) {
			if ($meta->action == ORDER_AGENT_MATCH) {
				if (!isset($res->city))
					$res->city = [];
				foreach($meta->item as &$item) {
					// verify correct location value
					if ($item->location == $lastBadLocation) {
						$item->location = $lastFixedLocation;
						$Sellers->log("Sales - reconcile bad location:$lastBadLocation to location:$item->location, $item->locationStr on $item->specialtyStr for reservationId:$res->id");
					}
					else {
						$parts = explode(',',AH\removeslashes($item->locationStr));
						$city = $Cities->get((object)['where'=>['id'=>$item->location]]);
						if (empty($city) ||
							$city[0]->city != $parts[0]) {// uh oh
							$lastBadLocation = $item->location;
							$state = trim($parts[1]);
							if ( ($pos = strpos($state, ':')) !== false) {
								$state = substr($state, 0, $pos); // strip off an ":8" or ":16", etc..
							}
							$city = $Cities->get((object)['like'=>['city'=>$parts[0]],
														  'like2'=>['state'=>$state]]);
							if (!empty($city)) {
								$item->location = $city[0]->id;
								$lastFixedLocation = $item->location;
								$needUpdate = true;
								$Sellers->log("Sales - reconcile bad location:$lastBadLocation to location:$item->location, $item->locationStr on $item->specialtyStr for reservationId:$res->id");
							}
							else {
								$Sellers->log("Sales - could not reconcile location:$item->location, $item->locationStr - skipping $item->specialtyStr for reservationId:$res->id!");
								continue;
							}
						}
						unset($parts, $city);
					}
					if (!isset($res->city[$item->location]))
						$res->city[$item->location] = (object)["locationStr"=>AH\removeslashes($item->locationStr),
															   "specialty"=>[]];
					$res->city[$item->location]->specialty[] = (object)["specialty"=>$item->specialty,
																	    "specialtyStr"=>$item->specialtyStr];
					// now build up the city based collation
					if (!isset($reservationCollation[$item->location]))		
						makeTagSet($reservationCollation, $item->location, $item->locationStr, -1, -1);						
				 	if (!isset($reservationCollation[$item->location]->tags[$item->specialty]))	
				 		$reservationCollation[$item->location]->tags[$item->specialty] = (object)["specialtyStr"=>$item->specialtyStr,
				 																				  "reservations"=>[],
				 																				  "agents"=>[]];	

				 	$reservationCollation[$item->location]->tags[$item->specialty]->reservations[] = $res->id;
				}
				$metas[] = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}
		if ($needUpdate) 
			$Reservations->set([(object)['where'=>['id'=>$res->id],
										 'fields'=>['meta'=>$metas]]]);
		unset($metas);
	}
}


// SELECT a.first_name, a.last_name, a.email, a.company, a.street_address, a.city, a.state, a.phone, a.flags, a.meta , GROUP_CONCAT( b.tag_id,  ':', b.score,  ':', b.city_id, ':', d.city, ':', d.lat, ':', d.lng, ':', c.tag
// SEPARATOR  '|' ) AS tags
// FROM  `icn_alr_sellers` AS a
// INNER JOIN icn_alr_sellers_tags AS b ON a.author_id = b.author_id
// INNER JOIN icn_alr_tags AS c ON b.tag_id = c.id
// INNER JOIN icn_alr_cities AS d ON b.city_id = d.id
// WHERE b.type =1 AND a.flags & ((1 << 4) | (1 << 5))
// GROUP BY a.author_id
																																								//0				1 			2 				3 				4 				5 			6 			7
$sql = "SELECT a.id, a.author_id, a.first_name, a.last_name, a.email, a.company, a.street_address, a.city, a.state, a.phone, a.mobile, a.flags, GROUP_CONCAT( b.tag_id, ':', c.tag, ':', b.score,  ':', b.city_id, ':', d.city, ':', d.state, ':', d.lat, ':', d.lng  SEPARATOR  '|' ) AS tags ";
$sql.= "FROM  {$Sellers->getTableName()} AS a ";
$sql.= "INNER JOIN {$Sellers->getTableName('sellers-tags')} AS b ON a.author_id = b.author_id ";
$sql.= "INNER JOIN {$Sellers->getTableName('tags')} AS c ON b.tag_id = c.id ";
$sql.= "INNER JOIN {$Sellers->getTableName('cities')} AS d ON b.city_id = d.id ";
$sql.= "WHERE b.type =1 AND a.flags & (".(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME).") GROUP BY a.author_id";

// $activeAgents = $Sellers->get((object)['bitand'=>['flags'=>(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)],
// 									   'where'=>['meta'=>'notnull']);
$activeAgents = $Sellers->rawQuery( $sql );
$cityAgentsActive = [];
if (!empty($activeAgents)) {
	$agents = [];
	// rearrange array to be associative
	foreach($activeAgents as $agent) {
		$agents[$agent->id] = $agent;
		unset($agent);
	}

	unset($activeAgents);
	$activeAgents = $agents;
}

if (!empty($activeAgents)) {
	foreach($activeAgents as $agent_id=>&$agent) {
		$agent->tags = explode('|', $agent->tags);
		$new_tags = [];
		$city_tags = [];
		foreach($agent->tags as $tag_info) {
			if (!empty($tag_info)) {
				$tag_info = explode(':', $tag_info);
				$tag_id = intval($tag_info[0]);
				$tag = $tag_info[1];
				$city_id = intval($tag_info[3]);
				$city = $tag_info[4];
				$state = $tag_info[5];
				$lat = (isset($tag_info[6]) && !empty($tag_info[6]) ? floatval($tag_info[6]) : -1);
				$lng = (isset($tag_info[7]) && !empty($tag_info[7]) ? floatval($tag_info[7]) : -1);
				$score = ($tag_info[2] == '-1' ? 10 : intval($tag_info[2]));
				if ( !array_key_exists($tag_id, $new_tags) )
					$new_tags[$tag_id] = (object)[	'tag_id'=>$tag_id,
													'tag'=>$tag,
													'score'=>$score,
													'loc'=>[]];
				$loc = (object)['city_id'=>$city_id,
								'city'=>$city.', '.$state,
								'lat'=>$lat,
								'lng'=>$lng
								];
				$tag = (object)['tag_id'=>$tag_id,
								'tag'=>$tag,
								'score'=>$score];
				$new_tags[$tag_id]->loc[] = $loc;

				if ( !array_key_exists($city_id, $city_tags) )
					$city_tags[$city_id] = (object)['city'=>$city.', '.$state,
													'tags'=>[]];
				$city_tags[$city_id]->tags[] = $tag;
				unset($tag);
			}
		}
		unset($agent->tags);
		$agent->tags = $new_tags;
		$agent->city_tags = $city_tags;

		foreach($agent->tags as $tag_id=>$tagLoc) {
			foreach($tagLoc->loc as $loc) {
				if ( !array_key_exists($loc->city_id, $reservationCollation) )
					makeTagSet($reservationCollation, $loc->city_id, $loc->city, $loc->lat, $loc->lng);
				if ( !isset($reservationCollation[$loc->city_id]->tags[$tag_id]) )
					$reservationCollation[$loc->city_id]->tags[$tag_id] = (object)["specialtyStr"=>$tagLoc->tag,
 																				  "reservations"=>[],
 																				  "agents"=>[]];	
				$reservationCollation[$loc->city_id]->tags[$tag_id]->agents[] = $agent;				
			}
		}
		unset($new_tags, $city_tags);
	}
}

$cityList = $Cities->get((object)['where'=>['id'=>array_keys($reservationCollation)]]);
$minPos = ['lng'=>181,
		   'lat'=>181];
$maxPos = ['lng'=>-181,
		   'lat'=>-181];

foreach($cityList as $city) {
	$reservationCollation[$city->id]->lng = $city->lng;
	$reservationCollation[$city->id]->lat = $city->lat;

	if ($city->lng < $minPos['lng'])
		$minPos['lng'] = $city->lng;
	if ($city->lat < $minPos['lat'])
		$minPos['lat'] = $city->lat;
	if ($city->lng > $maxPos['lng'])
		$maxPos['lng'] = $city->lng;
	if ($city->lat > $maxPos['lat'])
		$maxPos['lat'] = $city->lat;
}

$mapCenter = ['lng'=>(($minPos['lng']+$maxPos['lng'])/2),
			  'lat'=>(($minPos['lat']+$maxPos['lat'])/2)];
$Sellers->log("minPos:".print_r($minPos, true).", maxPos:".print_r($maxPos, true).", mapCentereewr:".print_r($mapCenter, true));

global $tag_listing; // reservation's listing tags
global $tag_city;    // reservation's cit tags
$all_cityTags = $tag_city;
$totalAdded = 0;
foreach($reservationCollation as $city_id=>$city) {
	// $Sellers->log("City_id:$city_id, ".print_r($city->tags, true));
	// $city->tags = (array)$city->tags;
	$city = (object)$city;
	if (isset($city->tags)) {
		$city_keys = array_keys((array)$city->tags);
		$test_keys = array_intersect($all_cityTags, $city_keys);
	}
	// $this->log("City_id:$city_id, all_cityTags:".print_r($all_cityTags, true));
	// $this->log("City_id:$city_id, city_keys:".print_r($city_keys, true));
	// $this->log("City_id:$city_id, test_keys:".print_r($test_keys, true));
	if (empty($test_keys)) { // then none of them are city tags, but other lifestyle tags
		if (isset($city->locationStr) &&
			isset($city->tags)) {
			$city->tag_state = TAGS_ALL_LISTINGS;
			$dat[] = json_encode($city);
		}
		else
			$Sellers->log("Failed to find a good city - ".print_r($city, true));
		continue;
	}

	$startSize = count($test_keys);
	$city_tags = [];
	$tags = $CitiesTags->get((object)['where'=>['city_id'=>$city_id],
									  'what'=>['tag_id', 'score']]);
	if (!empty($tags)) {
		// $this->log("City_id:$city_id, found ".count($tags)." tags");
		// $this->log("City_id:$city_id, test_keys:".print_r($test_keys, true));
		// $this->log("City_id:$city_id, city_tags:".print_r($tags, true));
		$needScoring = false;
		$hasScoring = false;
		foreach($tags as $tag) {
			$city_tags[] = $tag->tag_id;
			if ($tag->score == 1) {
				$needScoring = true;
				if (in_array($tag->tag_id, $test_keys)) {
					// $Sellers->log("update-city-tags - City_id:$city_id - assign score:$tag->score to $tag->tag_id");
					// $city->tags[strval($tag->tag_id)]->score = $tag->score;
					$city->tags[$tag->tag_id]->score = $tag->score;
				}
			}
			elseif (in_array($tag->tag_id, $test_keys)) {
				$hasScoring = true;
				// $Sellers->log("update-city-tags - City_id:$city_id - assign score:$tag->score to $tag->tag_id");
				// $city->tags[strval($tag->tag_id)]->score = $tag->score;
				$city->tags[$tag->tag_id]->score = $tag->score;
			}
		}

		$test_keys = array_diff($test_keys, $city_tags);
		// $Sellers->log("update-city-tags - City_id:$city_id, needScoring:$needScoring, hasScoring:$hasScoring");
		// $this->log("City_id:$city_id, diff_keys:".print_r($test_keys, true));
		if (count($test_keys) == 0) {
			if ($needScoring)
				$city->tag_state = $hasScoring ? TAGS_HAVE_SOME : TAGS_NEED_ALL;
			else
				$city->tag_state = TAGS_HAVE_ALL;
		}
		else
			$city->tag_state = TAGS_HAVE_SOME;
	}
	else
		$city->tag_state = TAGS_NEED_ALL;

	$added = 0;
	// $this->log("update-city-tags - before City_id:$city_id, ".print_r($city->tags, true));
	if (count($test_keys) > 0) {
		foreach($test_keys as $tag) {
			$x = $CitiesTags->add(['tag_id'=>$tag,
									'city_id'=>$city_id,
									'score'=>1,
									'clicks'=>0]);
			$Sellers->log("update-city-tags - City_id:$city_id, added tag: $tag, assign score:1");
			gettype($city->tags) == 'array' ? $city->tags[strval($tag)]->score = 1 :
											  $city->tags->$tag->score = 1;
			if (!empty($x))
				$added++;
		}
	}
	$Sellers->log("update-city-tags - for $city->locationStr, id:$city_id, tag_state:$city->tag_state, added:$added tags, pre-exiting in CitiesTags:".count($city_tags)." tags.");
	// $this->log("update-city-tags - after City_id:$city_id, ".print_r($city->tags, true));
	$dat[] = json_encode($city);
	unset($city_tags, $test_keys, $city);
}

$lifestyleStatusFile = __DIR__.'/_data/lifestyledStatus.dat';
$msg = '';
$dat = [];
foreach($reservationCollation as $data) {
	$data->locationStr = addslashes($data->locationStr);
	$dat[] = json_encode($data);
}
$wrote = file_put_contents($lifestyleStatusFile, implode('||',$dat), LOCK_EX);
unset($dat);
$msg = !empty($wrote) ? "Wrote $wrote bytes to $lifestyleStatusFile." : "Failed to write to $lifestyleStatusFile.";
$Sellers->log("Sales - $msg");

global $Logger;
require_once(__DIR__.'/../_classes/Logger.class.php'); $Logger = new AH\Logger(1, 'sales-page');

function getCityCategories(&$cityCategories, &$allCityTags) {
	require_once(__DIR__.'./../_classes/TagsCategories.class.php'); $TagsCategories = new AH\TagsCategories(); $TagsCategories = $TagsCategories->get();
	require_once(__DIR__.'./../_classes/TagsTaxonomy.class.php'); $TagsTaxonomy = new AH\TagsTaxonomy(); $TagsTaxonomy = $TagsTaxonomy->get();

	global $all_tag_city;
	global $Tags;

	$allCityTags = $Tags->get((object)['where'=>['id'=>$all_tag_city]]);
	$x = [];
	foreach($allCityTags as $tag)
		$x[$tag->id] = $tag;
	$allCityTags = $x;

	foreach ($TagsCategories as $category){
		$cityCategories[$category->id] = $category;
		$cityCategories[$category->id]->tags = [];
		foreach ($TagsTaxonomy as $tax) {
			if ($tax->category_id == $category->id)
				foreach ($allCityTags as $tag) 
					if ($tag->id == $tax->tag_id && $tag->type <= 1){ $cityCategories[$category->id]->tags[$tag->id] = $tag; break; }
			unset($tax);
		}
		usort($cityCategories[$category->id]->tags,function($a,$b){ return strcmp($a->tag, $b->tag); });
		unset($category);
	}
	// $Logger->log("cityCategories before culling:".print_r($cityCategories, true));
	foreach($cityCategories as $id=>$cat)
		if ( empty($cat->tags) )
			unset($cityCategories[$id]);
	
	unset($TagsCategories, $TagsTaxonomy);
}

$allCityTags = [];
$cityCategories = [];
getCityCategories($cityCategories, $allCityTags);

function geListingsCategories(&$listingCategories, &$allListingTags) {
	require_once(__DIR__.'./../_classes/TagsCategories.class.php'); $TagsCategories = new AH\TagsCategories(); $TagsCategories = $TagsCategories->get();
	require_once(__DIR__.'./../_classes/TagsTaxonomy.class.php'); $TagsTaxonomy = new AH\TagsTaxonomy(); $TagsTaxonomy = $TagsTaxonomy->get();

	global $Tags;
	global $tag_listing_for_sales_admin;
	$allListingTags = $Tags->get((object)['where'=>['id'=>$tag_listing_for_sales_admin]]);
	$x = [];
	foreach($allListingTags as $tag)
		$x[$tag->id] = $tag;
	$allListingTags = $x;

	foreach ($TagsCategories as $category){
		$listingCategories[$category->id] = $category;
		$listingCategories[$category->id]->tags = [];
		foreach ($TagsTaxonomy as $tax) {
			if ($tax->category_id == $category->id)
				foreach ($allListingTags as $tag) 
					if ($tag->id == $tax->tag_id && $tag->type <= 1){ $listingCategories[$category->id]->tags[$tag->id] = $tag; break; }
			unset($tax);
		}
		usort($listingCategories[$category->id]->tags,function($a,$b){ return strcmp($a->tag, $b->tag); });
		unset($category);
	}	
	foreach($listingCategories as $id=>$cat)
		if ( empty($cat->tags) )
			unset($listingCategories[$id]);
	unset($TagsCategories, $TagsTaxonomy);
}

$allListingTags = [];
$listingCategories = [];
geListingsCategories($listingCategories, $allListingTags);

global $tag_listing_for_sales_admin;

$Logger->log("cityCategories after culling:".print_r($cityCategories, true));

if (!empty($_COOKIE['PHPSESSID'])) $session_id = $_COOKIE['PHPSESSID'];
elseif (session_id()) { $session_id = session_id(); }
?>
<script type="text/javascript">

var reservations = <?php echo (empty($reservations) ? '0' : json_encode($reservations)); ?>;
var reservationCollation = <?php echo (empty($reservationCollation) ? '0' : json_encode($reservationCollation)); ?>;
var activeAgents = <?php echo (empty($activeAgents) ? '0' : json_encode($activeAgents)); ?>;
var amTagList = <?php echo json_encode($amTags); ?>;
var mapCenter = <?php echo json_encode($mapCenter); ?>;
var allCityTags = <?php echo json_encode($allCityTags); ?>;
var cityCategories = <?php echo json_encode($cityCategories); ?>;
var allListingTags = <?php echo json_encode($allListingTags); ?>;
var listingCategories = <?php echo json_encode($listingCategories); ?>;
var reservationCityTags = <?php echo json_encode($tag_city); ?>;
var reservationListingTags = <?php echo json_encode($tag_listing); ?>;
var tag_listing_for_sales_admin = <?php echo json_encode($tag_listing_for_sales_admin); ?>;
var Browser = <?php echo json_encode($browser); ?>;
/*
var reservations = [];
var reservationCollation = [];
var activeAgents = [];
var amTagList = <?php echo json_encode($amTags); ?>;
var mapCenter = [];
var RadialDistance = <?php echo json_encode($RadialDistance); ?>;
*/
</script>
<?php if ( current_user_can('create_users') ) :// adminstrator ?>

<div id="adminDiv">
	<h3>Simulate CRM Access:</h3>
	<form id="leads" onsubmit="salesAdmin.submitCRMContact('leads'); return false;">
		<label>Lead Id:</label>&nbsp;<input type="text" class='crm_id' name="leads" placeholder="enter CRM LEAD id" />
		<label>Seller Id:</label>&nbsp;<input type="text" class='seller_id' name="leads" placeholder="enter seller id" />
		<label>Seller Email:</label>&nbsp;<input type="text" class='email' name="leads" placeholder="enter email" />
		<input type="submit" value="Submit">
	</form>
	<form id="contacts" onsubmit="salesAdmin.submitCRMContact('contacts'); return false;">
		<label>Lead Id:</label>&nbsp;<input type="text" class='crm_id' name="contacts" placeholder="enter CRM CONTACT id" />
		<label>Seller Id:</label>&nbsp;<input type="text" class='seller_id' name="contacts" placeholder="enter seller id" />
		<label>Seller Email:</label>&nbsp;<input type="text" class='email' name="contacts" placeholder="enter email" />
		<input type="submit" value="Submit">
	</form>
</div>
<?php endif; ?>

<div id="optionWrapper">
	<div class="options">
		<div id="optionChooser">
			<label>Use specifier</label>&nbsp;<input type="checkbox" class="optionActive" /></br>
			<div id="sellerOption">
				<label>Name:</label> <!-- <input type="radio" name="option" id="name" /><span> </span> --><input type="button" id="submitName" value="Submit" />
				<table class="sellerID">
					<thead>
						<tr>
							<th scope="col" class="manage-column column-first-name">First</th>
							<th scope="col" class="manage-column column-last-name">Last</th>
							<th scope="col" class="manage-column column-email">Email</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" id="firstName"/></td>
							<td><input type="text" id="lastName"/> </td>
							<td><input type="text" id="email"/> </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="addressOption">
				<label>City:</label> <!-- <input type="radio" name="option" id="city" /><span> </span> --><input type="button" id="submitAddress" value="Submit" />
				<input type="text" class="city" placeholder="City" value="" />
			</div>
			<!--
			<div id="idOption">
				<label>ID:</label> <input type="radio" name="option" id="ids" />&nbsp;<input type="button" id="submitID" value="Submit" /></br>
				<div id="idSpecifier">
					<input type="radio" name="id-option" id="range" /><span>Specify range of IDs</span></br>	
					<label>From:</label>&nbsp;<input type="text" id="from-id" />&nbsp;&nbsp;<label>To:</label>&nbsp;<input type="text" id="to-id" /></br>
					<input type="radio" name="id-option" id="specify" /><span>Select IDs</span></br>
					<input type="text" id="id-specifier" placeholder="Enter specific and/or sequence of IDs: 1,2,5-9,13" />
				</div>
			</div>
			-->	
		</div>
	</div>
</div>

<div id="legend">
	<span>Map Legend</span>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>Only Listing Tags</th>
				<th>No Scoring</th>
				<th>Partial Scores</th>
				<th>All Scored</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Only Reservations</td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/GrayWhite.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/GrayRed.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/GrayYellow.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/GrayBlack.png'; ?>"></td>
			</tr>
			<tr>
				<td>Has Agent</td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BlueWhite.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BlueRed.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BlueYellow.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BlueBlack.png'; ?>"></td>
			</tr>
			<tr>
				<td>Maxed Agent</td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BigBlueWhite.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BigBlueRed.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BigBlueYellow.png'; ?>"></td>
				<td><img src="<?php echo get_template_directory_uri().'/_img/sales/map-markers/BigBlueBlack.png'; ?>"></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="spin-wrap">
    <div class="spinner">
        <div class="cube1"></div>
        <div class="cube2"></div>
	</div>
</div>

<div class="context-menu">
	<div id="container">
		<span id="instructions">This applies only to last zoomed area, which can be different from last area selected for cities and listings.</span>
		<ul id="options">
			<li><a href="javascript:salesAdmin.applyCityTagsToZone();">Apply city tags to last zoomed area</a></li>
			<li><a href="javascript:salesAdmin.applyListingTagsToZone();">Apply listing tags to last zoomed area</a></li>
		</ul>
		<button id="close">Close</button>
	</div>
</div>


<section class="upper reservations">
	<span id="title"><strong>LifeStyle Status</strong></span>&nbsp;&nbsp;
	<div id="zipCodeDiv" class="topOption"><input type="checkbox" id="zipCodes" checked /><span>Show Zip Codes</span></div>&nbsp;&nbsp;
	<div id="kmlDiv" class="topOption"><input type="checkbox" id="kml" checked /><span>Overlay KML</span></div>&nbsp;&nbsp;
	<div id="zoomInstructions">
		<div id="container">
			<h4>Mouse Click-Drag</h4>
			<span>Shift only - zoom, mark area</span>
			<span>Shift-c - select cities, mark area</span>
			<span>Shift-l - select cities and all listings for those cities, mark area</span>
			<span>Shift-x - select listings only within the area, along with their cities, mark area</span>
		</div>
		<div id="container2">
			<h4>Marked Area</h4>
			<span>Right click - popup menu to assign tags only to last zoomed area</span>
		</div>
	</div>
	<!-- <button id="updateTags">Update City Tags</button> -->
	<ul class="view selector" id="view">
      	<li class="left list active"><a class="top list"><span style="display: block; margin: 3px 0 0 0;">List View</span></a></li>
      	<li class="right map"><a class="top map"><span style="display: block; margin: 3px 0 0 0;">Map View</span></a></li>
	</ul>
	<div id="reservations-list-div" class="main"></div>
	<div id="tag-selections">
		<div id="tag-window"></div>
		<div id="controls">
			<div id="dozone">
				<input type="checkbox" id="dozone" /><span>&nbsp;Also apply to last zoomed area</span>
			</div>
			<button id="accept">Accept</button>
			<button id="cancel">Cancel</button>
		</div>
	</div>
	<div id="reservation-map"></div>
	<ul class="list selector" id="list">
      	<li class="left data"><a class="bottom left"><span class="entypo-left-open-big"></span></a></li>
      	<li class="right data"><a class="bottom right"><span class="entypo-right-open-big"></span></a></li>
	</ul>
</section>

<!--
<section class="mid agents">
	<h1 id="title">Active Agents</h1>
	<ul class="view-selector">
      	<li class="list active"><a class="list"><span style="display: block; margin: 3px 0 0 0;">List View</span></a></li>
      	<li class="map"><a class="map"><span style="display: block; margin: 3px 0 0 0;">Map View</span></a></li>
	</ul>
	<div id="agent-table">
		<table id="agents-list" class="scrollbar">
			<thead>
				<tr style="text-align: center;">
					<th>ID</th>
					<th>First</th>
					<th>Last</th>
					<th>Email</th>
					<th>Expertise</th>
				</tr>
			</thead>
			<tbody>
		<?php
		if (!empty($activeAgents)) foreach($activeAgents as $id=>$agent) { ?>
						<tr>
							<td><?php echo $id; ?></td>
							<td><?php echo $agent->first_name; ?></td>
							<td><?php echo $agent->last_name; ?></td>
							<td><?php echo $agent->email; ?></td>
							<td>
								<table>
									<thead>
										<tr>
											<td>City</td>
											<td>Lifestyles</td>
										</tr>
									</thead>
									<tbody>
									<?php foreach($agent->city_tags as $city_id=>$tags) { ?>
										<tr><td><?php echo $tags->city; ?></td>
											<td><ul> <?php
											foreach($tags->tags as $tag) { ?>
												<li style="float: left; margin-left: 10px"><?php echo $tag->tag; ?></li>
									<?php 		unset($tag);
											} ?>
											</ul></td>
									<?php 	unset($tags);
										  } ?>
										</tr>

									</tbody>
								</table>
							</td>
						</tr>
		<?php 
			unset($agent);
		} ?>
			</tbody>
		</table>
	</div>
	<div id="agents-map"></div>
</section>
-->
<!--
<section class="lower waiting">
	<h1 id="title">Waiting Agents</h1>
	<ul class="view-selector">
      	<li class="list active"><a class="list"><span style="display: block; margin: 3px 0 0 0;">List View</span></a></li>
      	<li class="map"><a class="map"><span style="display: block; margin: 3px 0 0 0;">Map View</span></a></li>
	</ul>
	<ul id="reservations-list" class="scrollbar"></ul>
	<div id="reservation-map"></div>
</section>
-->
