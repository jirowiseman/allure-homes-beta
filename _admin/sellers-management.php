<?php
require_once(__DIR__.'/../_classes/Tables.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
//require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers(); $Sellers = $Sellers->get((object)[ 'what' => ['id','author_id','first_name','last_name','email','meta'], 'where'=>array('meta'=>'notnull') ]);
require_once(__DIR__.'/../_classes/Invitations.class.php'); $Invitations = new AH\Invitations(); 
require_once(__DIR__.'/../_classes/Sellers.class.php'); $Sellers = new AH\Sellers;
require_once(__DIR__.'/../_classes/Options.class.php'); $Options = new AH\Options;
$invites = $Invitations->get();
$opt = $Options->get((object)['where'=>['opt'=>'EmailTestStats']]);
if (!empty($opt)) $testStat = $opt[0]->value == "1" ? true : false;

$csvAgentCount = 5000;
$opt = $Options->get((object)['where'=>['opt'=>'CSVAgentCountPerFile']]);
if (!empty($opt)) $csvAgentCount = $opt[0]->value;

$emailDbMetaTypes = [
		(object)["value"=>0,"name"=>"SPECIAL ZERO"],
		(object)["value"=>SENT_FIRST_EMAIL,"name"=>"SENT_FIRST_EMAIL"],
		(object)["value"=>RESPONSE_FIRST_EMAIL,"name"=>"RESPONSE_FIRST_EMAIL"],
		(object)["value"=>SENT_SECOND_EMAIL,"name"=>"SENT_SECOND_EMAIL"],
		(object)["value"=>RESPONSE_SECOND_EMAIL,"name"=>"RESPONSE_SECOND_EMAIL"],
		(object)["value"=>SENT_THIRD_EMAIL ,"name"=>"SENT_THIRD_EMAIL"],
		(object)["value"=>RESPONSE_THIRD_EMAIL ,"name"=>"RESPONSE_THIRD_EMAIL"],
		(object)["value"=>SENT_FOURTH_EMAIL ,"name"=>"SENT_FOURTH_EMAIL"],
		(object)["value"=>RESPONSE_FOURTH_EMAIL ,"name"=>"RESPONSE_FOURTH_EMAIL"],
		(object)["value"=>IMPROVE_LISTING_SENT ,"name"=>"IMPROVE_LISTING_SENT"],
		(object)["value"=>IMPROVE_LISTING_RESPONSE,"name"=>"IMPROVE_LISTING_RESPONSE"],
		(object)["value"=>VIEWED_LISTING,"name"=>"VIEWED_LISTING"],
		(object)["value"=>RESERVED_PORTAL,"name"=>"RESERVED_PORTAL"],
		(object)["value"=>RESERVED_AGENT_MATCH ,"name"=>"RESERVED_AGENT_MATCH"],
		(object)["value"=>RESERVED_SOMETHING ,"name"=>"RESERVED_SOMETHING"],
		(object)["value"=>EMAIL_UNSUBSCRIBED_DATA ,"name"=>"EMAIL_UNSUBSCRIBED_DATA"],
];

$chimpCampaigns = [
		(object)["key"=>'7-7-M',"cid"=>'67e0c47b82',"name"=>"Batch 1 (7/7)"],
		(object)["key"=>'7-8-M',"cid"=>'6679381340',"name"=>"New Mass Test"],
		(object)["key"=>'7-8-A',"cid"=>'37dfe403dc',"name"=>"Evening Send 7/8"],
		(object)["key"=>'7-8-E',"cid"=>'301bc70bf3',"name"=>"Evening Send 7/8 (Copy 01)"],
		(object)["key"=>"7-9-A","cid"=>'7573b76f66',"name"=>"7-9 12:36PM"]
];
$opt = $Options->get((object)array('where'=>array('opt'=>'ChimpCampaigns')));
if (!empty($opt)) $chimpCampaigns = json_decode($opt[0]->value);


global $wpdb;
$curPage = 0;
$pageSize = 40;

$wpId = wp_get_current_user()->ID;
$seller = $Sellers->get((object)['where'=>['author_id'=>$wpId]]);
$sellerId = empty($seller) ? 0 : $seller[0]->id;
//$last = $wpdb->get_results("SELECT * FROM ".getTableName('listings')." ORDER BY `id` DESC LIMIT 1");
//$x = $wpdb->get_results('SELECT id, title, author, price, beds, baths FROM '.getTableName('listings-rejected')." LIMIT $pageSize");
//$authors = $wpdb->get_results('SELECT * FROM '.getTableName('sellers'));
// usort($authors, function($a, $b){
// 	return $a->last_name > $b->last_name;
// });
// echo get_templete_directory_uri();
$session_id = '0';
if (!empty($_COOKIE['PHPSESSID'])) $session_id = $_COOKIE['PHPSESSID'];
elseif (session_id()) { $session_id = session_id(); }
?>
<script type="text/javascript">ah_local = {
	tp: '<?php echo get_template_directory_uri(); ?>',
	wp: '<?php echo get_home_url(); ?>',
	sessionID: '<?php echo $session_id; ?>',
	inviteList: <?php echo json_encode($invites); ?>
}
var sellerId = <?php echo $sellerId; ?>;
var testStats = <?php echo $testStat; ?>;
var csvAgentCount = <?php echo $csvAgentCount; ?>;
var chimpCampaigns = <?php echo json_encode($chimpCampaigns); ?>;
</script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/_js/jquery.bxslider.min.js" type="text/javascript"></script>


<div id="activityChooser">
	<label style="display: none;">Assign Invitation Code:</label> <input type="radio" name="table" id="invites" style="display: none;"/><span> </span>
	<label>Check Improvements:</label> <input type="radio" name="table" id="improvements" /><span><br/></span>
	<label>Send Mail:</label> <input type="radio" name="table" id="mail" />
	<div id="mail-control">
		<select id="mail-type">
			<option value="8">First Email</option>
			<option value="16">Second Email</option>
			<option value="32">Third Email</option>
			<option value="128">Fourth Email</option>
			<option value="256">First Portal Promo Email</option>
		</select>
		<button id="preview">Preview</button><button id="testEmail">Send Test Email</button><button id="clear-flags">Clear Seller Flags</button>
		<div "id=emails">
			<button id="sendEmail">Send Email</button>&nbsp;
			<input type="checkbox" id="mailchimp">Create MailChimp CSV file</input>&nbsp;
			<input type="checkbox" id="test-run">Testing, fake trackers and no tripping flags</input>&nbsp;
			<span id="doingStats"></span>
		</div>
		<br/>
		<div id="stats-div">
			<span>Chimp Stat Section</span><br/>
			<button id="totals">Get Total Sent</button>
			<button id="stats">Email DNS Stats</button>
			<button id="stats-range">Range of DNS Stats</button>&nbsp;<span>Works only with City/State (Address) option</span>
			
			<div id="chimp-stats">
				<button id="activity-stats">Report Email Activity</button>&nbsp;<span>Get stats and chart on email activity</span><br/>
				<table>
					<tbody>
						<tr>
							<td><button id="update-sentTo">Update SentTo</button></td>
							<td><div>Update Flags to indicate Agents that were sent emails.<br/>Update Options Table's ChimpCampaigns when new campaigns are added.<br/>Do this before getting accurate email activity stats.</div><td/>
						</tr>
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
							<td><button id="update-complaints">Update Complaints</button></td>
							<td><div>Update Flags to indicate Agents that who unsubscribed.<br/>Update Options Table's ChimpCampaigns when new campaigns are added.<br/>Do this before getting accurate email activity stats.</div><td/>
						</tr>
					</tbody>
				</table>
				<span><br/>Get count of complaints/unsubscribes/opens on selected Chimp Campaigns<span><br/>
				<button id="get-chimp-complaints">Chimp Complaints</button>
				<button id="get-chimp-unsubscribes">Chimp Unsubscribes</button>
				<button id="get-chimp-open">Chimp Opens</button>
				<select id="mail-chimp">
					<?php foreach($chimpCampaigns as $campaign) { ?>
					<option value="<?php echo $campaign->key; ?>"><?php echo $campaign->name; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<br/>
		<div id="crmDiv">
			<span>CRM Section</span><br/>
			<button id="get-zoho-crm-csv">Zoho CRM CSV</button>&nbsp;
			<button id="zoho-update-lead-ids">Zoho Update Lead IDs</button>&nbsp;
			<button id="zoho-udpate-new-reservations">Zoho Update New Reservations</button>
			<button id="zoho-udpate-existing-reservations">Zoho Update Existing Reservations</button>
		</div>
		<br/>
		<div id="maintenace">
			<span>DO NOT MESS WITH THESE BELOW!!<br/></span>
			<button id="clean-emaildb">Clean EmailDb (Expert Mode!)</button>
			<select id="mail-meta">
				<?php foreach($emailDbMetaTypes as $type) { ?>
				<option value="<?php echo $type->value; ?>"><?php echo $type->name; ?></option>
				<?php } ?>
			</select>
			<!-- these two checkboxes go with clean-emaildb!  -->
			<input type="checkbox" id="reset-flag-also">Reset flag</input>&nbsp;
			<input type="checkbox" id="remove-meta">Remove meta</input>&nbsp;<br/>
			<button id="rebuild-flags">Rebuild Flags From Existing Meta Data (Expert Mode!)</button><br/>
			<button id="rebuild-meta">Rebuild Seller EmailDb Meta From EmailTracker and Reservations Meta data (Expert Mode!)</button><br/>
			<button id="radian-array">Make Radial Array</button><br/>
			<button id="scrub-seller-tags">Scrub Seller Tags</button><br/>
			<!-- <button id="update-seller-portal-visits">Update Seller Portal Visits</button><br/> -->
			<button id="test-ajax-paywhirl">Test Ajax Paywhirl</button><br/>
			<label>Seller ID:</label>&nbsp;<input type="text" id="seller-id" />&nbsp;<button id="delete-seller">Delete Seller</button><br/>
			<label>User name</label>&nbsp;<input type="text" id="user-name" />&nbsp;<button id="unlock-user">Unlock User</button><br/>
			<!-- <button id="fix-email-response">Repair Mail Receive meta data (Expert Mode!)</button><br/> -->
			<span>END SCARY PART!<br/></span>
		</div>
	</div>
</div>
<div id="emailStats"></div>
<div id="sellerSelection">
	<table>
		<tbody>
			<tr>
				<td><label>Sellers with active and waiting listings</label></td>
				<td><input type="radio" name="sellers" class="optionDefaultSeller" checked for="0"/></td>
				<td><span>Presorted sellers based on the sellers email database - USE THIS!	</span></td>
			</tr>
			<tr>
				<td><label>Only sellers with active listings</label></td>
				<td><input type="radio" name="sellers" class="optionOnlyActiveSeller" for="1"/></td>
				<td><span>It will only reduce the selection by small percentage</span></td>
			</tr>
			<tr>
				<td><label>Select Sellers that has these types</label></td>
				<td><input type="radio" name="sellers" class="optionAllSeller" for="2"/></td>
				<td>
					<div id="listing_type">
						<input type="checkbox" name="listing_type" id="inactive" for="0">Inactive</input>
						<input type="checkbox" name="listing_type" id="active" for="1">Active</input>
						<input type="checkbox" name="listing_type" id="waiting" for="2">Waiting</input>
						<input type="checkbox" name="listing_type" id="rejected" for="3">Rejected</input>
						<input type="checkbox" name="listing_type" id="toocheap" for="4">TooCheap</input>
						<input type="checkbox" name="listing_type" id="average" for="6">Average</input>
						<input type="checkbox" name="listing_type" id="never" for="5">Never</input>
						<input type="checkbox" name="listing_type" id="none" for="7">No Listing at all</input>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div id="previewDiv">
	<div id="content"></div>
	<button id="hide">Done</button>
</div>
<div id="optionWrapper">
	<div class="options">
		<div id="optionChooser">
			<label>Use specifier</label>&nbsp;<input type="checkbox" class="optionActive" /></br>
			<div id="sellerOption">
				<label>Name:</label> <input type="radio" name="option" id="name" /><span> </span><input type="button" id="submitName" value="Submit" />
				<table class="sellerID">
					<thead>
						<tr>
							<th scope="col" class="manage-column column-first-name">First</th>
							<th scope="col" class="manage-column column-last-name">Last</th>
							<th scope="col" class="manage-column column-email">Email</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" id="firstName"/></td>
							<td><input type="text" id="lastName"/> </td>
							<td><input type="text" id="email"/> </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="addressOption">
				<label>Address:</label> <input type="radio" name="option" id="address" /><span> </span><input type="button" id="submitAddress" value="Submit" />
				<select id="distance"></select>
				<table class="address">
					<thead>
						<tr>
							<th scope="col" class="manage-column column-city">City</th>
							<th scope="col" class="manage-column column-state">State</th>
							<th scope="col" class="manage-column column-zip">ZIP</th>
						</tr>
						<tr>
							<td><input type="text" id="city"/> </td>
							<td><input type="text" id="state"/> </td>
							<td><input type="text" id="zip"/></td>
						</tr>
					</thead>
				</table>
			</div>
			<div id="idOption">
				<label>ID:</label> <input type="radio" name="option" id="ids" />&nbsp;<input type="button" id="submitID" value="Submit" /></br>
				<div id="idSpecifier">
					<input type="radio" name="id-option" id="range" /><span>Specify range of IDs</span></br>	
					<label>From:</label>&nbsp;<input type="text" id="from-id" />&nbsp;&nbsp;<label>To:</label>&nbsp;<input type="text" id="to-id" /></br>
					<input type="radio" name="id-option" id="specify" /><span>Select IDs</span></br>
					<input type="text" id="id-specifier" placeholder="Enter specific and/or sequence of IDs: 1,2,5-9,13" />
				</div>
			</div>
		</div>
	</div>
	<div id="createCodesDiv">
		<span>Create more invite codes:&nbsp;</span><button name="createCodes" id="createCodes">Create Codes</button>
		<span>Show codes:&nbsp;</span><button name="createCodes" id="showCodes">Show Codes</button>
		<span>Check codes:&nbsp;</span><button name="createCodes" id="checkCodes">Check Codes</button>
	</div>
	<div class="spin-wrap">
        <div class="spinner">
	        <div class="cube1"></div>
	        <div class="cube2"></div>
		</div>
	</div>
</div>

<div id="sellerSingle">
	<label>Seller Info:</label>
	<table class="widefat sellerSingle">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-id">ID</th>
				<th scope="col" class="manage-column column-author-id">AuthorID</th>
				<th scope="col" class="manage-column column-first-name">First</th>
				<th scope="col" class="manage-column column-last-name">Last</th>
				<th scope="col" class="manage-column column-email">Email</th>
				<th scope="col" class="manage-column column-city">City</th>
				<th scope="col" class="manage-column column-state">State</th>
				<th scope="col" class="manage-column column-zip">ZIP</th>
				<th scope="col" class="manage-column column-invitation-code">Code</th>
				<th scope="col" class="manage-column column-improvements">Improvements</th>
				<th scope="col" class="manage-column column-assign">Action</th>
				<th scope="col" class="manage-column column-send-email">Action</th>
				<th scope="col" class="manage-column column-types">Distribution</th>
				<th scope="col" class="manage-column column-listings">Listings</th>
				<th scope="col" class="manage-column column-password">Password</th>
				<th scope="col" class="manage-column column-reservation">CRM</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
			</tr>
		</tbody>
	</table>
	<br/>
</div>

<br/>
<div id="infoBarSeller">
	<span class="tally seller"></span>
	<span id="legend">   Distribution Legend: I = Inactive, A = Active, W = Waiting, R = Rejected, C = TooCheap, N = Never, Avg = Average</span>
</div>

<div class="sellerListDiv">
	<div id="selectAllDiv"><input type="checkbox" id="selectAll" /><span> Select All </span></div>
		<table class="widefat sellerList">
			<thead>
				<tr>
					<th scope="col" class="manage-column column-select">Select</th>
					<th scope="col" class="manage-column column-id"><a href="javascript:sellerAdmin.sortId()" target="_blank">ID</a></th>
					<th scope="col" class="manage-column column-author-id"><a href="javascript:sellerAdmin.sortAuthorId()" target="_blank">AuthorID</a></th>
					<th scope="col" class="manage-column column-first-name">First</th>
					<th scope="col" class="manage-column column-last-name">Last</th>
					<th scope="col" class="manage-column column-email"><a href="javascript:sellerAdmin.sortEmail()" target="_blank">Email</a></th>
					<th scope="col" class="manage-column column-city"><a href="javascript:sellerAdmin.sortCity()" target="_blank">City</a></th>
					<th scope="col" class="manage-column column-state"><a href="javascript:sellerAdmin.sortState()" target="_blank">State</a></th>
					<th scope="col" class="manage-column column-zip"><a href="javascript:sellerAdmin.sortZip()" target="_blank">ZIP</a></th>
					<th scope="col" class="manage-column column-invitation-code">Code</th>
					<th scope="col" class="manage-column column-improvements">Improvements</th>
					<th scope="col" class="manage-column column-assign">Action</th>
					<th scope="col" class="manage-column column-send-email">Action</th>
					<th scope="col" class="manage-column column-types">Distribution</th>
					<th scope="col" class="manage-column column-listings">Listings</th>
					<th scope="col" class="manage-column column-password">Password</th>
					<th scope="col" class="manage-column column-reservation">CRM</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="controls"> 
			<input type="button" id="prevButtonSeller" value="Previous"><span> </span><input type="button" id="nextButtonSeller" value="Next"><span> </span><input type="button" id="sendImproveEmailAll" value="ImproveIt Email" disabled="true"><span> </span><input type="button" id="sendIntroEmailAll" value="Intro Email" disabled="true"><span> </span><span id="sliderValueSeller"></span><span> </span><div id="pageSliderSeller"/>
		</div>
	</div>
</div>

<br/><br/>
<div id="infoBar">
	<span class="tally listing"></span>
	<span id="legend">   Error Code Legend: 1 = Too Cheap, 2 = Lack Desc, 3 = No Images, 4 = No Bed/Baths, 5 = Commercial, 6 = No Tags</span>
</div>

<table class="widefat listings">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-listing-id"><a href="javascript:sellerAdmin.sortId()" target="_blank">ID</a></th>
			<th scope="col" class="manage-column column-price"><a href="javascript:sellerAdmin.sortPrice()" target="_blank">Price</a></th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-error">Error</th>
			<th scope="col" class="manage-column column-tags"># Tags</th>
			<th scope="col" class="manage-column column-image">Image</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-active">Current</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-price">Price</th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-error">Error</th>
			<th scope="col" class="manage-column column-tags"># Tags</th>
			<th scope="col" class="manage-column column-image">Image</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-active">Current</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</tfoot>
</table>
<div class="controls"> 
	<input type="button" id="prevButton" value="Previous"><span> </span><input type="button" id="nextButton" value="Next"><span> </span><input type="button" id="applyChanges" value="Apply"><span> </span><span id="sliderValue"></span><span> </span><div id="pageSlider"/>
</div>

