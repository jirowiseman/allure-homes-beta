<?php
namespace AH;
require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
require_once(__DIR__.'../../../../../wp-config.php');
require_once(__DIR__.'../../../../../wp-load.php');
require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Email.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

class AJAX_invitations extends Controller {
	// comes from Controller now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	private $logIt = true;
	protected $allowed = [3,4,5,8];
	private $activeModification = true;
	private $sendEmailToSeller = true;
	protected $todayDate = '';

	public function __construct(){
		$in = parent::__construct();
		set_time_limit(0);

		$this->todayDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
		try {
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-invitations.log') : null;
			$this->log("ajax-invitations doing ".(isset($in->query) ? $in->query : "no query sent").", activeModification:".($this->activeModification ? 'true' : 'false').", sendEmailToSeller:".($this->sendEmailToSeller ? 'true' : 'false'));
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			switch ($in->query){
				case 'check-invites':
					$opt = $this->getClass('Options', 1)->get((object)['where'=>['opt'=>'DirectoryPermittedUsers']]);
					if (!empty($opt)) {
						$this->getClass('Options')->log("DirectoryPermittedUsers - {$opt[0]->value}");
						$optAr = json_decode($opt[0]->value);
						if (count($optAr)) {
							foreach($optAr as $i=>$val)
								$optAr[$i] = intval($val);
							$this->allowed = array_merge($this->allowed, $optAr);
						}
					}
					// $llcEmployees = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$this->allowed]]);
					// if (!empty($llcEmployees)) {
					// 	$temp = [];
					// 	foreach($llcEmployees as $emp)
					// 		$temp[$emp->id] = $emp;
					// 	$llcEmployees = $temp;
					// }
					$processed = 0;
					$invitations = $this->getClass('Invitations', 1)->get((object)['bitand'=>['flags'=>IC_USED],
																				'bitor'=>['flags'=>(IC_EXPIRE_IN_30 | IC_EXPIRE_IN_180 | IC_EXPIRE_IN_365)],
																				'notand'=>['flags'=>IC_FEATURE_LIFETIME]]);
					$this->log("got ".count($invitations)." from table");
					if (!empty($invitations)) {
						$today = time();
						$added = strtotime($invite->added);
						$day = 3600*24;
						$day7 = ($day * 7);
						$day14 = ($day7 + $day7);
						$day30 = ($day * 30);
						$day180 = ($day30*6)+($day*3);
						$day365 = ($day * 365);
						$day2year = ($day365 * 2);
						$day3year = ($day365 * 3);
						$expireDay30 = $today - $day30;
						$expireDay180 = $today - $day180;
						$expireDay365 = $today - $day365;
						$expireDay2yr = $today - $day2year;
						$expireDay3yr = $today - $day3year;

						$warningSet = [	$day=>IC_WITHIN_1, 
										$day7=>IC_WITHIN_7, 
										$day14=>IC_WITHIN_14, 
										$day30=>IC_WITHIN_30];

						$corporateAttention = [];
						foreach($invitations as $invite) {
							if (!empty($invite->seller_id)) {
								$processed++;
								$this->log("code:$invite->code - flags:$invite->flags, added:$invite->added");
								foreach($invite->seller_id as $seller_id) {								
									$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$seller_id]]);
									if (!empty($seller)) {
										$seller = array_pop($seller);
										$this->log("sellerId: $seller_id, flags:$seller->flags");
										if (!empty($seller->author_id) &&
											in_array($seller->author_id, $this->allowed)) {
											unset($seller);
											continue;
										}
										// $expirationDate = $invite->flags & IC_EXPIRE_IN_30 ? $expireDay30 : ($invite->flags & IC_EXPIRE_IN_180 ? $expireDay180 : $expireDay365);
										$expirationDate = $today;
										$duration = $invite->flags & IC_EXPIRE_IN_30 ? $day30 : 
													$invite->flags & IC_EXPIRE_IN_180 ? $day180 : 
													$invite->flags & IC_EXPIRE_IN_365 ? $day365 :
													$invite->flags & IC_EXPIRE_IN_2_YR ? $day2year :
													$invite->flags & IC_EXPIRE_IN_3_YR ? $day3year : $day3year*10; // no expiration?
										$duration += $invite->flags & IC_EXTRA_1_MONTH ? $day30 : 
													$invite->flags & IC_EXTRA_2_MONTH ? $day30*2 : 
													$invite->flags & IC_EXTRA_3_MONTH ? $day30*3 : 0; 
										$meta = null;
										$needUpdate = false;
										$needAttention = [];
										$metas = [];
										$deflag = 0;
										$alreadyBought = 0;
										if ($invite->flags & IC_FEATURE_PORTAL) {
											if ($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) {
												// default start date is when the seller was added
												$endDate = ($invite->flags & IC_CORPORATE_WIDE) == 0  ? strtotime($seller->added) + $duration :
																										strtotime($invite->added) + $duration ;
												$meta = null;
												$haveNicknameMeta = false;
												if (!empty($seller->meta)) foreach($seller->meta as $metaData) {
													if ($metaData->action == SELLER_NICKNAME) {
														if ($metaData->flags & IC_BOUGHT_PORTAL)
															$alreadyBought = true;
														if (!isset($metaData->inviteCode) ||
														 	$metaData->inviteCode == $invite->code) {
															if (isset($metaData->inviteUsedDate) &&
																($invite->flags & IC_CORPORATE_WIDE) == 0) { 
																$endDate = strtotime($metaData->inviteUsedDate) + $duration;
															}
															$meta = $metaData;
														}
														$haveNicknameMeta = true;
													}
													else
														$metas[] = $metaData;
												}

												if (!$meta &&
													$haveNicknameMeta) {
													$this->log("SellerId:$seller->id - has a SELLER_NICKNAME meta, but not from this invite:$invite->code, alreadyBought:".($alreadyBought ? 'yes' : 'no'));
												}
												else {
													$user_info = get_userdata($seller->author_id);
													if (!$meta) {
														$meta = new \stdClass();
														$meta->action = SELLER_NICKNAME;
														$meta->nickname = strpos($user_info->nickname, 'unknown') === false ? $user_info->nickname :
																		  (strpos($user_info->user_nicename, 'unknown') === false ? $user_info->user_nicename : 'unknown');
														$needUpdate = true;
													}
													if (!isset($meta->flags)) {
														$meta->flags = 0;
														$needUpdate = true;
													}
													if ($endDate < $expirationDate) {
														$this->log("Portal has expired for sellerId:$seller->id");
														if (($meta->flags & IC_EXPIRED) == 0) {
															// first time we need to deal with expiration of this item
															$meta->flags |= IC_EXPIRED;
															$meta->expireDate = $this->todayDate;
															$deflag |= SELLER_IS_PREMIUM_LEVEL_1;
															$needUpdate = true;
															if ( ($meta->flags & IC_IGNORE_ALL_WARNINGS) == 0) {
																if ( ($invite->flags & IC_CORPORATE_WIDE) == 0 )
																	$needAttention[] = (object)['what'=>'portal',
																								'state'=>IC_EXPIRED];
																else {
																	if (!array_key_exists($invite->association_id, $corporateAttention))
																		$corporateAttention[$invite->association_id] = [];
																	if (!isset($corporateAttention[$invite->association_id]['portal']))
																		$corporateAttention[$invite->association_id]['portal'] = IC_EXPIRED;
																}
															}
															// need to deactivate
															if ($this->activeModification) {
																if (!empty($seller->author_id)) {
																	$status = wp_update_user( array( 'ID' => $seller->author_id, 'user_nicename' => 'unknown' ) );
																	if ( is_wp_error($status) ) 
																		$this->log("SellerId:$seller->id - Failed to update user_nicename, status: ".$status->get_error_message());
																	else
																		$this->log("SellerId:$seller->id - portal:$user_info->user_nicename is deactivated");
																}
																else
																	$this->log("SellerId:$seller->id has an empty author_id!!");
															}
														}										
													}
													else {
														$delta = $endDate - $expirationDate;
														$this->log("SellerId:$seller->id - portal delta:".($delta/$day)." days");
														foreach($warningSet as $timeleft=>$flag) {
															if ( $delta < $timeleft ) {
																if (($meta->flags & $flag) == 0) {
																	$meta->flags |= $flag;
																	$needUpdate = true;
																	if ( ($meta->flags & IC_IGNORE_ALL_WARNINGS) == 0) {
																		if ( ($invite->flags & IC_CORPORATE_WIDE) == 0 ) {
																			$needAttention[] = (object)['what'=>'portal',
																										'state'=>$flag];
																			$this->log("Portal is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id");	
																		}
																		else {
																			if (!array_key_exists($invite->association_id, $corporateAttention))
																				$corporateAttention[$invite->association_id] = [];
																			if (!isset($corporateAttention[$invite->association_id]['portal'])) {
																				$corporateAttention[$invite->association_id]['portal'] = $flag;
																				$this->log("Portal is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id with company:$invite->association_id");	
																			}
																		}	
																	}	
																	else	
																		$this->log("No report - Portal is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id".(($invite->flags & IC_CORPORATE_WIDE) ? " with company:$invite->association_id" : ''));				
																	break;
																}
																else
																	$this->log("Flag tripped - Portal is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id".(($invite->flags & IC_CORPORATE_WIDE) ? " with company:$invite->association_id" : ''));
															}
														}
													}

													$metas[] = $meta;
													if ($needUpdate) { // otherwise, seller->meta should be intact											
														$seller->meta = $metas;
													}
												} // end else if (!$meta && $haveNicknameMeta), so $meta matched invite code or just didn't exist yet
											} // end if ($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) 
											else {
											// check to see how long it's expired.  If over IC_KEEP_EXPIRED_PORTAL_DAYS period, remove it from user's nicknmae too
												if (!empty($seller->meta)) foreach($seller->meta as &$meta) {
													if ($meta->action == SELLER_NICKNAME &&
														$meta->flags & IC_EXPIRED && 
														($meta->flags & IC_REMOVED) == 0) {
														$whenExpired = strtotime($meta->expireDate);
														if ( ($today - $whenExpired) >= (IC_KEEP_EXPIRED_PORTAL_DAYS * $day)) {
															$needUpdate = true;
															$status = wp_update_user( array( 'ID' => $seller->author_id, 'nickname' => 'unknown' ) );
															if ( is_wp_error($status) ) 
																$this->log("SellerId:$seller->id - Failed to update nickname, status: ".$status->get_error_message());
															else {
																$this->log("SellerId:$seller->id - portal:$user_info->nickname is removed completely, it was ".(($today - $whenExpired)/$day)." days since it had expired.");
																$meta->flags |= IC_REMOVED;
															}
														}
													}											
												}													
											}
										} // end if ($invite->flags & IC_FEATURE_PORTAL)

										$needUpdate2 = false;
										$metas = [];
										if ($invite->flags & IC_FEATURE_LIFESTYLE &&
											$seller->flags & SELLER_IS_PREMIUM_LEVEL_2) {
											$endDate = ($invite->flags & IC_CORPORATE_WIDE) == 0  ? strtotime($seller->added) + $duration :
																									strtotime($invite->added) + $duration ;
											$meta = null;
											if (!empty($seller->meta)) foreach($seller->meta as $metaData) {
												if ($metaData->action == SELLER_AGENT_ORDER) {
													if (!empty($metaData->item)) foreach($metaData->item as &$sellerItem) {
														if ($sellerItem->type == ORDER_AGENT_MATCH &&
															$sellerItem->mode == ORDER_BOUGHT &&
															$sellerItem->order_id == 999999999) { // then it was from invite
															// if used date is not there, default to added date
															if (isset($sellerItem->inviteCode)) {
																if ($sellerItem->inviteCode != $invite->code)
																	continue;
																if (($invite->flags & IC_CORPORATE_WIDE) == 0 &&
																	isset($sellerItem->inviteUsedDate)) {
																	$endDate = strtotime($sellerItem->inviteUsedDate) + $duration;
																}
															}

															if (!isset($sellerItem->flags)) {
																$sellerItem->flags = 0;
																$needUpdate2 = true;
															}

															if ($endDate < $expirationDate) {
																$this->log("Lifestyle $sellerItem->specialtyStr:$sellerItem->locationStr has expired for sellerId:$seller->id");
																if (($sellerItem->flags & IC_EXPIRED) == 0) {
																	// first time we need to deal with expiration of this item
																	$sellerItem->flags |= IC_EXPIRED;
																	$sellerItem->expireDate = $this->todayDate;
																	$needUpdate2 = true;
																	if ( ($sellerItem->flags & IC_IGNORE_ALL_WARNINGS) == 0) {
																		if ( ($invite->flags & IC_CORPORATE_WIDE) == 0 )
																			$needAttention[] = (object)['what'=>'lifestyle',
																										'item'=>$sellerItem,
																										'state'=>IC_EXPIRED];
																		else {
																			if (!array_key_exists($invite->association_id, $corporateAttention))
																				$corporateAttention[$invite->association_id] = [];
																			if (!isset($corporateAttention[$invite->association_id]['lifestyle']))
																				$corporateAttention[$invite->association_id]['lifestyle'] = IC_EXPIRED;
																			// 	$corporateAttention[$invite->association_id]['lifestyle'] = [];
																			// $corporateAttention[$invite->association_id]['lifestyle'][] = (object)['seller'=>$seller,
																			// 																	   'item'=>$sellerItem,
																			// 																	   'state'=>IC_EXPIRED];
																		}
																	}
																	// need to deactivate
																	if ($this->activeModification) {
																		$sellerItem->mode = ORDER_IDLE;
																		if ( $this->getClass('SellersTags')->exists(['author_id'=>$seller->author_id,
																													'tag_id'=>$sellerItem->specialty,
																													'city_id'=>$sellerItem->location]) ) {
																			$x = $this->getClass('SellersTags')->set([(object)['where'=>['author_id'=>$seller->author_id,
																																		 'tag_id'=>$sellerItem->specialty,
																																		 'city_id'=>$sellerItem->location],
																															   'fields'=>['type'=>0]]]);
																			if (!empty($x))
																				$this->log("SellerId:$seller->id - lifestyle:$sellerItem->specialtyStr at $sellerItem->locationStr is deactivated");
																			else
																				$this->log("SellerId:$seller->id - lifestyle:$sellerItem->specialtyStr at $sellerItem->locationStr failed to deactivate");
																		}
																	}
																}
															}
															else {
																$delta = $endDate - $expirationDate;
																$this->log("SellerId:$seller->id - lifestyle($sellerItem->specialtyStr:$sellerItem->locationStr) delta:".($delta/$day)." days");
																foreach($warningSet as $timeleft=>$flag) {
																	if ( $delta < $timeleft &&
																		 ($sellerItem->flags & $flag) == 0) {
																		$sellerItem->flags |= $flag;
																		$needUpdate2 = true;
																		if ( ($sellerItem->flags & IC_IGNORE_ALL_WARNINGS) == 0) {
																			if ( ($invite->flags & IC_CORPORATE_WIDE) == 0 ) {
																				$needAttention[] = (object)['what'=>'lifestyle',
																											'item'=>$sellerItem,
																											'state'=>$flag];
																				$this->log("Lifestyle $sellerItem->specialtyStr is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id");	
																			}
																			else {
																				if (!array_key_exists($invite->association_id, $corporateAttention))
																					$corporateAttention[$invite->association_id] = [];
																				if (!isset($corporateAttention[$invite->association_id]['lifestyle'])) 
																					$corporateAttention[$invite->association_id]['lifestyle'] = $flag;
																				// 	$corporateAttention[$invite->association_id]['lifestyle'] = [];
																				// $corporateAttention[$invite->association_id]['lifestyle'][]  = (object)['seller'=>$seller,
																				// 																		'item'=>$sellerItem,
																				// 																 		'state'=>$flag];
																				$this->log("Lifestyle $sellerItem->specialtyStr is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id with company:$invite->association_id");	
																				
																			}
																		}	
																		else
																			$this->log("No report - Lifestyle $sellerItem->specialtyStr is within ".($timeleft/$day)." days of expiring for sellerId:$seller->id".(($invite->flags & IC_CORPORATE_WIDE) ? " with company:$invite->association_id" : ''));	
																		break;
																	}
																}
															}													
														} // end if $sellerItem->order_id == 999999999
													}// end foreach ($meta->item as &$sellerItem)	
													//check to see if the seller has any lifestyle left, if not, deflag it
													if ($needUpdate2) {
														$haveLifestyle = false;
														foreach($metaData->item as &$sellerItem) 
															if ($sellerItem->type == ORDER_AGENT_MATCH &&
																$sellerItem->mode == ORDER_BOUGHT) {
																$haveLifestyle = true;
																break;
															}

														if (!$haveLifestyle)
															$deflag |= SELLER_IS_PREMIUM_LEVEL_2;
													}	
													$meta = $metaData;									
												} // end if ($meta->action == SELLER_AGENT_ORDER)
												else
													$metas[] = $metaData;
											}	

											if ($meta) $metas[] = $meta;
											if ($needUpdate2) { // otherwise, seller->meta should be intact											
												$seller->meta = $metas;
											}											
										}
										// update the seller table...
										if ($this->activeModification &&
											($needUpdate || $needUpdate2)) {
											$fields = ['meta'=>$seller->meta];
											if ($deflag) {
												$fields['flags'] = $seller->flags & ~$deflag;
												$this->log("SellerId:$seller->id - deflagged: $deflag");
											}
											$this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller_id],
																					  'fields'=>$fields]]);
										}
										if (!empty($needAttention)) {
											$msg = '<span>Seller Info:</span><br/>';
											$msg .= "<span>$seller->first_name $seller->last_name, $seller->email, id:$seller->id".(!empty($seller->phone) ? ", phone:".fixPhone($seller->phone) : '').(!empty($seller->mobile) ? ", mobile:".fixPhone($seller->mobile) : '')."</span><br/><br/>";

											$msg .= "<span>Options for sales dept.</span><br/>";
											$bare_url = get_template_directory_uri()."/_admin/ajax_invitations.php?query=%query%&author=$seller->author_id&code=$invite->code";

											$key = "grant-extension"."_".$seller->author_id;
											$this->log("SellerId:$seller->id - nonce key: $key");
											$complete_url = wp_nonce_url( str_replace('%query%', 'grant-extension', $bare_url), $key);
											$msg .= '<a href="'.$complete_url.'">Grant Extention</a> |';

											$key = "ignore-all-warnings"."_".$seller->author_id;
											$this->log("SellerId:$seller->id - nonce key: $key");
											$complete_url = wp_nonce_url( str_replace('%query%', 'ignore-all-warnings', $bare_url), $key);
											$msg .= '&nbsp;&nbsp;<a href="'.$complete_url.'">Stop all further warnings for this seller</a> |';

											$key = "send-email-to-seller"."_".$seller->author_id;
											$this->log("SellerId:$seller->id - nonce key: $key");
											$complete_url = wp_nonce_url( str_replace('%query%', 'send-email-to-seller', $bare_url), $key);
											$msg .= '&nbsp;&nbsp;<a href="'.$complete_url.'">Email seller about current state of invite code</a>';

											$msg .= '<br/><br/>';
											$msg .= '<span>Current status of products:</span><br/>';

											foreach($needAttention as $warning) {												
												$msg .= '<span>';
												switch($warning->what) {
													case 'portal':
														$msg .= $warning->what;
														break;
													case 'lifestyle':
														$msg .= " for ".$warning->item->specialtyStr." at ".$warning->item->locationStr;
														break;
												}
												switch($warning->state) {
													case IC_WITHIN_30: $msg.= " will expire WITHIN_30"; break;
													case IC_WITHIN_14: $msg.= " will expire WITHIN_14"; break;
													case IC_WITHIN_7: $msg.= " will expire WITHIN_7"; break;
													case IC_WITHIN_1: $msg.= " will expire WITHIN_1"; break;
													case IC_EXPIRED: $msg.= " EXPIRED"; break;
												}
																							
												$msg .= '</span><br/>';
												unset($warning);
											}
											$sent = $this->getClass('Email')->sendMail(SALES_EMAIL,
																						'Invitation Expiration Alert',
																						$msg,
																						'Take Action',
																						'Alert Seller');
											$this->log("sent mail:".($sent ? 'success' : 'fail')." - warning count:".count($needAttention).", $seller->first_name $seller->last_name, $seller->email, id:$seller->id to ".SALES_EMAIL);
										}
										unset($seller, $metas, $needAttention);
									} // end if (!empty($seller))
								} // end foreach($invite->seller_id as $seller_id)
							} // end if (!empty($invite->seller_id))
							unset($invite);
						} // end foreach($invitations as $invite)
						if (!empty($corporateAttention)) {
							foreach($corporateAttention as $id=>$corporate) {
								$company = $this->getClass('Associations')->get((object)['where'=>['id'=>$id]]);
								if (!empty($company)) {
									$company = array_pop($company);

									$msg = '<span>Corporate Info:</span><br/>';
									$msg .= "<span>$company->company, id:$company->id, located $company->city, $company->state".(!empty($company->phone) ? ", phone:".fixPhone($company->phone) : '').(!empty($company->phone2) ? ", mobile:".fixPhone($company->phone2) : '')."</span><br/><br/>";

									$msg .= "<span>Options for sales dept.</span><br/>";
									$bare_url = get_template_directory_uri()."/_admin/ajax_invitations.php?query=%query%&company=$company->id&code=$invite->code";

									$key = "grant-corporate-extension"."_".$company->id;
									$this->log("Company:$company->id - nonce key: $key");
									$complete_url = wp_nonce_url( str_replace('%query%', 'grant-corporate-extension', $bare_url), $key);
									$msg .= '<a href="'.$complete_url.'">Grant Extention</a>';

									$msg .= '<br/><br/>';
									$msg .= '<span>Current status of products:</span><br/>';

									foreach($corporate as $what=>$flag) {
										$msg .= "<span>- ".ucfirst($what)." product";							
										switch($flag) {
											case IC_WITHIN_30: $msg.= " will expire WITHIN_30";
											case IC_WITHIN_14: $msg.= " will expire WITHIN_14";
											case IC_WITHIN_7: $msg.= " will expire WITHIN_7";
											case IC_WITHIN_1: $msg.= " will expire WITHIN_1";
											case IC_EXPIRED: $msg.= " EXPIRED";
										}
										$msg .= '</span><br/>';
									}
									$this->getClass('Email')->sendMail(SALES_EMAIL,
																		'Corporate Invitation Expiration Alert',
																		$msg,
																		'Take Action',
																		'Alert Company');
								}
								else
									$this->log("Failed to find company with $id in Associations table, cannot send email to Sales");
							}
						} // end if (!empty($corporateAttention))
						unset($corporateAttention);
					} // end if (!empty($invitations))
					$out = new Out('OK', ['count'=>count($invitations),
										  'processed'=>$processed]);
					break;

				case 'grant-corporate-extension':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$company = '0';
					$nonce = '';
					$code = '';
					if ($type == 'POST') {
						if (isset($_POST['company']))
							$company = $_POST['company'];	
						if (isset($_POST['_wpnonce']))
							$nonce = $_POST['_wpnonce'];	
						if (isset($_POST['code']))
							$code = $_POST['code'];				
					}
					else {
						if (isset($_GET['company']))
							$company = $_GET['company'];
						if (isset($_GET['_wpnonce']))
							$nonce = $_GET['_wpnonce'];
						if (isset($_GET['code']))
							$code = $_GET['code'];	
					}

					$this->log("grant-corporate-extension type:$type got $company, code:$code, nonce:$nonce");
					$key = 'grant-corporate-extension_'.$company;
					$passed = check_ajax_referer($key, false, false);
					// $passed = wp_verify_nonce($key, $nonce);
					$this->log("grant-corporate-extension - checking nonce($key) passed:".($passed ? "yes" : "no"));
					if (!$passed) {
						$h = '<div><span>Sorry, you are a hacker, goodbye!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					else {
						$company = $this->getClass('Associations')->get((object)['where'=>['id'=>$company]]);
						$invite = $this->getClass('Invitations')->get((object)['where'=>['code'=>$code]]);
						$status = '';
						if (!empty($company) &&
							!empty($invite) &&
							$this->activeModification) {
							$company = array_pop($company);
							$invite = array_pop($invite);
							$sellers = $invite->seller_id;
							foreach($sellers as $seller)
								$this->grantSeller($seller, $invite, $code);
						}
						$h = '<div><span>Welcome, processing your corporate grant!</span><br/>';
						$h.= '<span>'.$status.'</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					break;

				case 'grant-extension':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$author = '0';
					$nonce = '';
					$code = '';
					if ($type == 'POST') {
						if (isset($_POST['author']))
							$author = $_POST['author'];	
						if (isset($_POST['_wpnonce']))
							$nonce = $_POST['_wpnonce'];	
						if (isset($_POST['code']))
							$code = $_POST['code'];				
					}
					else {
						if (isset($_GET['author']))
							$author = $_GET['author'];
						if (isset($_GET['_wpnonce']))
							$nonce = $_GET['_wpnonce'];
						if (isset($_GET['code']))
							$code = $_GET['code'];	
					}

					$this->log("grant-portal type:$type got $author, code:$code, nonce:$nonce");
					$key = 'grant-extension_'.$author;
					$passed = check_ajax_referer($key, false, false);
					// $passed = wp_verify_nonce($key, $nonce);
					$this->log("grant-extension - checking nonce($key) passed:".($passed ? "yes" : "no"));
					if (!$passed) {
						$h = '<div><span>Sorry, you are a hacker, goodbye!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					else {
						$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author]]);
						$invite = $this->getClass('Invitations')->get((object)['where'=>['code'=>$code]]);
						$status = '';
						if (!empty($seller) &&
							!empty($invite) &&
							$this->activeModification) {
							$seller = array_pop($seller);
							$invite = array_pop($invite);

							$this->grantSeller($seller, $invite, $code);
						}
						$h = '<div><span>Welcome, processing your grant!</span><br/>';
						$h.= '<span>'.$status.'</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					break;

				case 'ignore-all-warnings':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$author = '0';
					$nonce = '';
					$code = '';
					if ($type == 'POST') {
						if (isset($_POST['author']))
							$author = $_POST['author'];	
						if (isset($_POST['_wpnonce']))
							$nonce = $_POST['_wpnonce'];	
						if (isset($_POST['code']))
							$code = $_POST['code'];				
					}
					else {
						if (isset($_GET['author']))
							$author = $_GET['author'];
						if (isset($_GET['_wpnonce']))
							$nonce = $_GET['_wpnonce'];
						if (isset($_GET['code']))
							$code = $_GET['code'];	
					}

					$this->log("ignore-all-warnings type:$type got $author, code:$code, nonce:$nonce");
					$key = 'ignore-all-warnings_'.$author;
					$passed = check_ajax_referer($key, false, false);
					// $passed = wp_verify_nonce($key, $nonce);
					$this->log("ignore-all-warnings - checking nonce($key) passed:".($passed ? "yes" : "no"));
					if (!$passed) {
						$h = '<div><span>Sorry, you are a hacker, goodbye!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					else {
						$h = '<div><span>Welcome, processing your ignore all warnings!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						// set IC_IGNORE_ALL_WARNINGS flag on seller meta
						$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author]]);
						if (!empty($seller) &&
							$this->activeModification) {
							$seller = array_pop($seller);
							$needUpdate = false;
							if (!empty($seller->meta)) foreach($seller->meta as &$meta) {
								if ($meta->action == SELLER_NICKNAME &&
									!($meta->flags & IC_IGNORE_ALL_WARNINGS)) {
									$meta->flags |= IC_IGNORE_ALL_WARNINGS;
									$needUpdate = true;
								}
								elseif ($meta->action == SELLER_AGENT_ORDER) foreach($meta->item as &$sellerItem) {
									if ($sellerItem->type == ORDER_AGENT_MATCH &&
										$sellerItem->mode != ORDER_BUYING &&
										$sellerItem->order_id == 999999999 &&
										!($sellerItem->flags & IC_IGNORE_ALL_WARNINGS)) {
										if (isset($sellerItem->inviteCode) &&
											$sellerItem->inviteCode != $code)
										 	continue;

										$sellerItem->flags |= IC_IGNORE_ALL_WARNINGS;
										$needUpdate = true;
									}
								}
							}
							if ($needUpdate)
								$this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
																		  'fields'=>['meta'=>$seller->meta]]]);
						}
						echo $h; die;
					}
					break;

				case 'goto-dashboard':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$author = '0';
					$nonce = '';
					$code = '';
					if ($type == 'POST') {
						if (isset($_POST['author']))
							$author = $_POST['author'];	
						if (isset($_POST['_wpnonce']))
							$nonce = $_POST['_wpnonce'];	
						if (isset($_POST['code']))
							$code = $_POST['code'];				
					}
					else {
						if (isset($_GET['author']))
							$author = $_GET['author'];
						if (isset($_GET['_wpnonce']))
							$nonce = $_GET['_wpnonce'];
						if (isset($_GET['code']))
							$code = $_GET['code'];	
					}

					$this->log("goto-dashboard type:$type got $author, code:$code, nonce:$nonce");
					$key = 'goto-dashboard_'.$author;
					$passed = check_ajax_referer($key, false, false);
					// $passed = wp_verify_nonce($key, $nonce);
					$this->log("goto-dashboard - checking nonce($key) passed:".($passed ? "yes" : "no"));
					if (!$passed) {
						$h = '<div><span>Sorry, you are a hacker, goodbye!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					else {
						$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author]]);
						if (!empty($seller)) {
							$seller = array_pop($seller);
							wp_logout(); // just in case...
							wp_set_current_user(0);

							$user = get_userdata( $author ); // if we get this, the agent has at least did a basic registration
							$testPasswd = '';

							$magic = get_user_meta($user->ID, 'user_key', true);
							$i = 0;
							if (!empty($magic)) {
								$this->log("Found user_key for sellerId:$seller->id, userID:$user->ID - ".print_r($magic, true));
								foreach($magic as $value)
									$testPasswd .= chr( $value ^ ($i++ + ord('.')));
							}
							else {
								if (!empty($seller->meta)) {
									// $seller->meta = json_decode($seller->meta);
									foreach($seller->meta as $meta) {
										if ($meta->action == SELLER_KEY) {
											$this->log("Found SELLER_KEY for $seller->id, meta:$meta->key");
											$key = json_decode($meta->key);
											$len = count($key); // strlen($meta->key);
											for($i = 0; $i < $len; $i++)
												$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
										}
									}
								}
							}
								
							if (empty($testPasswd)) {
								$this->log("Failed seller: ".print_r($seller, true));
								$h = '<!DOCTYPE html>';
								$h .= '<html><head><h2>Failed to retrieve seller password</h2></head>';
								$h .= '<body><h1>Seller with author id: '.$seller->author_id.', could not be logged in.<br/>Please contact administrator</h1></body>';
								$h .= '</html>';
								echo $h;
								die;
							}								
							
							$this->log($testPasswd." ".$user->user_login);
							$creds = array();
							$creds['user_login'] = $user->user_login;
							$creds['user_password'] = $testPasswd;
							$creds['remember'] = true;
							$user = wp_signon( $creds, false );
							if ( is_wp_error($user) ) {
								$this->log("Failed seller: ".print_r($seller, true));
								$h = '<!DOCTYPE html>';
								$h .= '<html><head><h2>Failed to login seller</h2></head>';
								$h .= '<body><h1>Seller with author id: '.$seller->author_id.', failed to login with error: '.$user->get_error_message().'.<br/>Please contact administrator</h1></body>';
								$h .= '</html>';
								echo $h;
								die;
							}

								$user = wp_set_current_user($user->ID, $user->user_login);
							$this->log("UserId: $user->ID after wp_set_current_user()");
							// $user = wp_get_current_user();
							wp_set_auth_cookie( $user->ID );
							do_action('wp_login', $user->user_login, $user);
							$this->log("Seller:$seller->id, with userID:$seller->author_id is logged in with $testPasswd as {$creds['user_login']}, actual user is $user->ID, is logged in:".is_user_logged_in());
							$redirect_to = get_site_url()."/sellers";
							if (headers_sent())
								echo '<script type="text/javascript"> window.location = "'.$redirect_to.'"; </script>';
							else {
								header('Location: '.$redirect_to);
								header("Connection: close");
							}
							exit;
						}

						$h = '<div><span>Sorry, we could not find your account. Please contact our adminstrator.</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					break;

				case 'send-email-to-seller':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$author = '0';
					$nonce = '';
					$code = '';
					if ($type == 'POST') {
						if (isset($_POST['author']))
							$author = $_POST['author'];	
						if (isset($_POST['_wpnonce']))
							$nonce = $_POST['_wpnonce'];	
						if (isset($_POST['code']))
							$code = $_POST['code'];				
					}
					else {
						if (isset($_GET['author']))
							$author = $_GET['author'];
						if (isset($_GET['_wpnonce']))
							$nonce = $_GET['_wpnonce'];
						if (isset($_GET['code']))
							$code = $_GET['code'];	
					}

					$this->log("send-email-to-seller type:$type got $author, code:$code, nonce:$nonce");
					$key = 'send-email-to-seller_'.$author;
					$passed = check_ajax_referer($key, false, false);
					// $passed = wp_verify_nonce($key, $nonce);
					$this->log("send-email-to-seller - checking nonce($key) passed:".($passed ? "yes" : "no"));
					if (!$passed) {
						$h = '<div><span>Sorry, you are a hacker, goodbye!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						echo $h; die;
					}
					else {
						$h = '<div><span>Welcome, processing your send email to seller!</span><br/>';
						$h.= '<a href="javascript:window.close();">Close Window</a>';
						$h.= '</div>';
						$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author]]);
						if (!empty($seller) &&
							$this->sendEmailToSeller) {
							$seller = array_pop($seller);
							$needAttention = [];
							$icFlags = [IC_REMOVED, IC_EXPIRED, IC_WITHIN_1, IC_WITHIN_7, IC_WITHIN_14, IC_WITHIN_30];
							if (!empty($seller->meta)) foreach($seller->meta as &$meta) {
								if ($meta->action == SELLER_NICKNAME) {
									foreach($icFlags as $flag) {
										if ($meta->flags & $flag) {
											$needAttention[] = (object)['what'=>'portal',
																		'state'=>$flag];
											break;
										}
									}
								}
								elseif ($meta->action == SELLER_AGENT_ORDER) foreach($meta->item as &$sellerItem) {
									if ($sellerItem->type == ORDER_AGENT_MATCH &&
										$sellerItem->mode == ORDER_BOUGHT &&
										$sellerItem->order_id == 999999999) {
										if (isset($sellerItem->inviteCode) &&
											$sellerItem->inviteCode != $code)
										 	continue;

										foreach($icFlags as $flag) {
											if ($sellerItem->flags & $flag) {
												$needAttention[] = (object)['what'=>'lifestyle',
																			'item'=>$sellerItem,
																			'state'=>$flag];
												break;
											}
										}
									}
								}
							}
							if (!empty($needAttention)) {
								$bare_url = get_template_directory_uri()."/_admin/ajax_invitations.php?query=%query%&author=$seller->author_id&code=$invite->code";
								$key = "goto-dashboard"."_".$seller->author_id;
								$this->log("SellerId:$seller->id - nonce key: $key");
								$complete_url = wp_nonce_url( str_replace('%query%', 'goto-dashboard', $bare_url), $key);

								$msg = "<span>Hello $seller->first_name $seller->last_name</span><br/><br/>";
								$msg.= '<span>The following products you have are nearing expiration or have already expired.</span>';
								$msg.= '<span>You may go to our <a href="'.$complete_url.'";>site</a> and purchase the products to continue getting benefits from them.  Please log in and go to the Sellers Dashboard.</span><br/><br/>';
								foreach($needAttention as $warning) {
									$msg .= '<span>';
									switch($warning->what) {
										case 'portal':
											$msg .= "- ".ucfirst($warning->what);
											break;
										case 'lifestyle':
											$msg .= "- For ".$warning->item->specialtyStr." at ".$warning->item->locationStr;
											break;
									}
									switch($warning->state) {
										case IC_WITHIN_30: $msg.= " will expire within 30 days."; break;
										case IC_WITHIN_14: $msg.= " will expire within 14 days."; break;
										case IC_WITHIN_7: $msg.= " will expire within 7 days."; break;
										case IC_WITHIN_1: $msg.= " will expire tomorrow."; break;
										case IC_EXPIRED: $msg.= " has already expired."; break;
									}
									$msg .= '</span><br/>';
									unset($warning);
								}
								$msg .= '<br/>';
								$msg .= '<span>Sincerely,</span><br/><span>The LifeStyled Team</span>';

								$sent = $this->getClass('Email')->sendMail(	$seller->email,
																			'Invitation Expiration Alert',
																			$msg,
																			'Take Action',
																			"Don't let your power tools expire!");
								$this->log("sent mail:".($sent ? 'success' : 'fail')." - warning count:".count($needAttention).", $seller->first_name $seller->last_name, $seller->email, id:$seller->id");
							}
							else
								$this->log("send-email-to-seller had empty needAttention");
						}
						echo $h; die;
					}
					break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}

	protected function grantSeller($seller, $invite, $code) {
		$user_info = get_userdata($seller->author_id);
		if ($invite->flags & IC_FEATURE_PORTAL) {
			$needUpdate = false;
			$metas = [];
			$meta = null;
			$wasRemoved = false;
			$hadExpired = false;
			if (!empty($seller->meta)) foreach($seller->meta as $metaData) {
				if ($metaData->action == SELLER_NICKNAME &&
					(!isset($metaData->inviteCode) ||
					 $metaData->inviteCode == $code)) {
					$needUpdate = true;
					// reset start date
					// $metaData->inviteCode = $code;
					if (isset($metaData->expireDate)) unset($metaData->expireDate);
					$metaData->inviteUsedDate = $this->todayDate;
					$wasRemoved = $metaData->flags & IC_REMOVED;
					$hadExpired = $metaData->flags & IC_EXPIRED;
					// reset any IC flags
					$metaData->flags &= ~(IC_WITHIN_30 | IC_WITHIN_14 | IC_WITHIN_7 | IC_WITHIN_1 | IC_EXPIRED | IC_REMOVED | IC_IGNORE_ALL_WARNINGS);
					$meta = $metaData;
				}
				else
					$metas[] = $metaData;
			}
			if ($meta) {
				$metas[] = $meta;								

				$this->log("SellerId:$seller->id - meta nickname:".(isset($meta->nickname) ? $meta->nickname : '').", user_info:".(isset($user_info->nickname) ? $user_info->nickname : ''));
				$nickname = isset($meta->nickname) ? $meta->nickname : (isset($user_info->nickname) ? $user_info->nickname : '');
				$status = false;
				if ($hadExpired || $wasRemoved) {
					if (!empty($nickname) &&
						strpos($nickname, "unknown") === false) {
						if ($hadExpired) {
							$status = wp_update_user( array( 'ID' => $seller->author_id, 'user_nicename' =>  $nickname) );
							if ( is_wp_error($status) ) 
								$this->log("SellerId:$seller->id - Failed to update user_nicename, status: ".$status->get_error_message());
							else
								$this->log("SellerId:$seller->id - portal - user_nicename:$nickname is activated");
						}

						if ($wasRemoved) {
							$status = wp_update_user( array( 'ID' => $seller->author_id, 'nickname' =>  $nickname) );
							if ( is_wp_error($status) ) 
								$this->log("SellerId:$seller->id - Failed to update nickname, status: ".$status->get_error_message());
							else
								$this->log("SellerId:$seller->id - portal - nickname:$nickname is activated");
						}
					}
					else
						$this->log("SellerId:$seller->id - portal not activated, nickname is ".(!empty($nickname) ? $nickname : "empty!!"));
				}
				else
					$this->log("SellerId:$seller->id - portal, nickname is ".(!empty($nickname) ? $nickname : 'N/A')." and it's inviteUsedDate has been modified.");

				$fields = [];
				if ( ($seller->flags & SELLER_IS_PREMIUM_LEVEL_1) == 0) 
					$fields['flags'] = ($seller->flags | SELLER_IS_PREMIUM_LEVEL_1);
				$fields['meta'] = $metas;
				$status = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
														  			'fields'=>$fields]]);
				$this->log("SellerId:$seller->id - updating portal flags & meta was ".(!empty($status) ? "successful" : " a failure"));
				unset($fields, $metas);		
				$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$seller->id]])[0];		
			}
		} // end if ($invite->flags & IC_FEATURE_PORTAL) 

		if ($invite->flags & IC_FEATURE_LIFESTYLE) {
			$needUpdate = false;
			$metas = [];
			$meta = null;
			if (!empty($seller->meta)) foreach($seller->meta as $metaData) {
				if ($metaData->action == SELLER_AGENT_ORDER) {
					if (!empty($metaData->item)) foreach($metaData->item as &$sellerItem) {
						if ($sellerItem->type == ORDER_AGENT_MATCH &&
							$sellerItem->mode != ORDER_BUYING &&
							$sellerItem->order_id == 999999999) { // then it was from invite
							if (isset($sellerItem->inviteCode) &&
								$sellerItem->inviteCode != $code)
							 	continue;
							$needUpdate = true;
							if (isset($sellerItem	->expireDate)) unset($sellerItem->expireDate);
							$sellerItem->flags &= ~(IC_WITHIN_30 | IC_WITHIN_14 | IC_WITHIN_7 | IC_WITHIN_1 | IC_EXPIRED | IC_REMOVED | IC_IGNORE_ALL_WARNINGS);
							$sellerItem->mode = ORDER_BOUGHT;
							$sellerItem->inviteUsedDate = $this->todayDate;
							$x = $this->getClass('SellersTags')->set([(object)['where'=>['author_id'=>$seller->author_id,
																						 'tag_id'=>$sellerItem->specialty,
																						 'city_id'=>$sellerItem->location],
																			   'fields'=>['type'=>1]]]);
							$this->log("SellerId:$seller->id - updating lifestyle($sellerItem->specialtyStr:$sellerItem->locationStr) was ".(!empty($x) ? "successful" : "a failure"));
						}
					}
					$meta = $metaData;
				}
				else
					$metas[] = $metaData;
			}
			if ($meta) $metas[] = $meta;
			if ($needUpdate) {
				$fields = [];
				if ( ($seller->flags & SELLER_IS_PREMIUM_LEVEL_2) == 0) 
					$fields['flags'] = ($seller->flags | SELLER_IS_PREMIUM_LEVEL_2);
				$fields['meta'] = $metas;
				$status = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
														  			'fields'=>$fields]]);
				$this->log("SellerId:$seller->id - updating lifestyle flags & meta was ".(!empty($status) ? "successful" : " a failure"));
				unset($fields);
			}
			unset($metas);
		}
	}

	
}
new AJAX_invitations();
