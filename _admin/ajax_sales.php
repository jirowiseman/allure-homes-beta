<?php
namespace AH;
require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
require_once(__DIR__.'../../../../../wp-config.php');
require_once(__DIR__.'../../../../../wp-load.php');
require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');
require_once(__DIR__.'/../_classes/RadialDistance.php');

define('zohoToLLCKey', 'efc098ksf8312wiesl746264llczoho');
define('zohoToLLCKey_AccessSeller', 'ielwo3820ldioqlaie8okd1830wilszohollc88');
define('zohoToLLCKey_DeleteReservation', '3i894792idkjf729djuy19hgxvncmsbdkqpei3');
define('zohoToLLCKey_UpdateReservation', 'mwidhiwd9827fhsleu273792laiue7328hfyqw');

global $haveActiveParallelProcessing;
$haveActiveParallelProcessing = 0;

class AJAX_sales extends Controller {
	// comes from Controller now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	private $logIt = true;
	protected $lifestyleStatusFile = '';
	protected $lifestyleReservationsFile = '';
	protected $lifestyleAgentFile = '';
	protected $lifestyleMapCenterFile = '';
	protected $lifestyleAMTagListile = '';

	private $defaults = array(
		'show_inactive_listings' => true,
		'show_parser_listings' => false,
		'imgPath' => '/../_img/_listings/210x120'
	);

	public function __construct(){
		$in = parent::__construct();
		set_time_limit(0);
		$this->lifestyleStatusFile = __DIR__.'/_data/lifestyledStatus.dat';
	 	$this->lifestyleReservationsFile = __DIR__.'/_data/lifestyleReservationsFile.dat';
	 	$this->lifestyleAgentFile = __DIR__.'/_data/lifestyleAgentFile.dat';
	 	$this->lifestyleMapCenterFile = __DIR__.'/_data/lifestyleMapCenterFile.dat';
	 	$this->lifestyleAMTagListile = __DIR__.'/_data/lifestyleAMTagListFile.dat';
	 	$this->defaults['imgPath'] = realpath(__DIR__.'/../_img/_listings/210x120');
	 	global $haveActiveParallelProcessing;
	 	$haveActiveParallelProcessing = $this->haveActiveParallelProcessing();
		try {
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-sales.log') : null;
			$this->log("ajax-sales doing ".(isset($in->query) ? $in->query : "no query sent"));
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			$this->log("Status files: $this->lifestyleStatusFile, $this->lifestyleAgentFile, $this->lifestyleMapCenterFile, $this->lifestyleReservationsFile, $this->lifestyleAMTagListile");

			switch ($in->query){
				case 'gather-agents':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					$bounds = (object)$in->data['bounds'];
					$cities = $this->getBoundedCities($bounds);

					$this->getAgentsInCities($cities);

					$out = new Out('OK', $cities);
					break;

				case 'gather-locations':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					$bounds = (object)$in->data['bounds'];
					$cities = $this->getBoundedCities($bounds);
					$out = new Out('OK', $cities);
					break;

				case 'gather-listings':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					$bounds = (object)$in->data['bounds'];
					$cities = $this->getBoundedCities($bounds);

					$this->log("gather-listings - cities:".print_r($cities, true));
					
					$this->getListingsInCities($cities);

					$out = new Out('OK', $cities);
					break;

				case 'gather-listings-exact':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					$bounds = (object)$in->data['bounds'];

					$cities = $this->getBoundedListings($bounds);
					$out = new Out('OK', $cities);
					break;

				// update all gathered cities' tags with a specific single tag/score
				case 'update-listings-tag':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['listing_ids'])) throw new \Exception('No listing_ids sent.');
					if (empty($in->data['tag_id'])) throw new \Exception('No tag_id sent.');
					if (empty($in->data['tag'])) throw new \Exception('No tag sent.');
					if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');

					$ids = $in->data['listing_ids'];
					$tagId = intval($in->data['tag_id']);
					$tag = $in->data['tag'];
					$city_id = intval($in->data['city_id']);
					$what = $in->data['what'];
					$bounds = (object)$in->data['bounds'];
					$added = 0;

					$this->applyToTagsSpace([$tagId=>(object)['tag_id'=>$tagId,
															  'tag'=>$tag,
															  'type'=>0]], $bounds, 0, $in->query);

					$listingTags = $this->getClass('ListingsTags')->get((object)['where'=>['listing_id'=>$ids,
																						   'tag_id'=>$tagId]]);
					
					$alreadyHave = [];
					if (!empty($listingTags)) foreach($listingTags as $tag)
						$alreadyHave[] = intval($tag->listing_id);

					$x = [];
					foreach($ids as $id)
						$x[] = intval($id);
					$ids = $x;

					$needUpdating = array_diff($ids, $alreadyHave);
					if (!empty($needUpdating)) foreach($needUpdating as $id)
						if ($this->getClass('ListingsTags')->add(['listing_id'=>$id,
															  	  'tag_id'=>$tagId]))
							$added++;

					$cities = $this->getCitiesByIds([$city_id]);
					$this->getListingsInCities($cities);

					$out = new Out('OK', ['added'=>$added,
										  'expectedToAdd'=>count($needUpdating),
										  'unchanged'=>count($alreadyHave),
										  'city'=>$cities[intval($city_id)]]);
					break;

				// update single city score from sales map from a gathered city
				case 'update-city-tag':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
					if (empty($in->data['tag_id'])) throw new \Exception('No tag_id sent.');
					if (empty($in->data['score'])) throw new \Exception('No score sent.');
					$x = $this->getClass('CitiesTags')->set([(object)['where'=>['city_id'=>$in->data['city_id'],
																				'tag_id'=>$in->data['tag_id']],
																	  'fields'=>['score'=>intval($in->data['score'])] ]]);
					$out = new Out(!empty($x) ? 'OK' : 'fail');
					break;

				// update all gathered cities' tags with a specific single tag/score
				case 'update-cities-tag':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_ids'])) throw new \Exception('No city_ids sent.');
					if (empty($in->data['tag_id'])) throw new \Exception('No tag_id sent.');
					if (empty($in->data['tag'])) throw new \Exception('No tag sent.');
					if (empty($in->data['score'])) throw new \Exception('No score sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');

					$ids = $in->data['city_ids'];
					$tagId = intval($in->data['tag_id']);
					$tag = $in->data['tag'];
					$score = intval($in->data['score']);
					$what = $in->data['what'];
					$bounds = (object)$in->data['bounds'];

					$this->applyToTagsSpace([$tagId=>(object)['tag_id'=>$tagId,
															  'tag'=>$tag,
															  'type'=>1,
															  'score'=>$score]], $bounds, 1, $in->query);

					$x = $this->getClass('CitiesTags')->get((object)['where'=>['city_id'=>$ids,
																				'tag_id'=>$tagId]]);
					$alreadyHave = [];
					$updated = 0;
					$unchanged = 0;
					if (!empty($x)) foreach($x as $tag) {
						$tag->score = $tag->score == -1 ? 10 : $tag->score;
						$alreadyHave[] = $tag->city_id;
						$fields = [];
						if ($tag->score < $score) {
							$fields['score'] = $score;
							if ( !($tag->flags & CITY_TAG_MODIFIED_BY_SALES_ADMIN) )
								$fields['flags'] = $tag->flags | CITY_TAG_MODIFIED_BY_SALES_ADMIN;
						}
						if (count($fields)) {
							$updated++;
							$this->getClass('CitiesTags')->set([(object)['where'=>[	'city_id'=>$tag->city_id,
																					'tag_id'=>$tagId ],
																	  	'fields'=>$fields ]]);
						}
						else
							$unchanged++;
						unset($fields, $tag);
					}

					$notHaveYet = array_diff($ids, $alreadyHave);
					if (!empty($notHaveYet)) foreach($notHaveYet as $city_id)
						$this->getClass('CitiesTags')->add([ 'city_id' => $city_id,
															 'tag_id' => $tagId,
															 'score' => $score,
															 'flags' => CITY_TAG_ASSIGNED_BY_SALES_ADMIN]);

					$cities = $this->getCitiesByIds($ids);
					switch($what) {
						case 'gather-locations': break;  // done
						case 'gather-listings': $this->getListingsInCities($cities); break;
						case 'gather-agents': $this->getAgentsInCities($cities); break;
					}

					$out = new Out('OK', ['updated'=>$updated,
										  'unchanged'=>$unchanged,
										  'added'=>count($notHaveYet),
										  'cities' => $cities]);
					break;

				case 'add-city-tags':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
					if (empty($in->data['tags'])) throw new \Exception('No tags sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					$city_id = $in->data['city_id'];
					$tags = $in->data['tags'];
					$what = $in->data['what'];
					$x = [];
					foreach($tags as $tag) {
						$tag = (object)$tag;
						$tag->id = intval($tag->id);
						$tag->tag_id = intval($tag->tag_id);
						$x[$tag->tag_id] = $tag;
					}
					$tags = $x;
					$this->log("add-city-tags for city:$city_id, for $what got tags:".print_r($tags, true));

					unset($x);
					$out = $this->applyTagsToCity($tags, $city_id);
					break;

				case 'add-cities-tags':
				case 'add-specific-cities-tags':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_ids'])) throw new \Exception('No city_ids sent.');
					if (empty($in->data['tags'])) throw new \Exception('No tags sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					if (empty($in->data['dozone'])) throw new \Exception('No dozone sent.');

					$city_ids = $in->data['city_ids'];
					$tags = $in->data['tags'];
					$what = $in->data['what'];
					$bounds = (object)$in->data['bounds'];
					$dozone = is_true($in->data['dozone']);

					$x = [];
					foreach($tags as $tag) {
						$tag = (object)$tag;
						$tag->id = intval($tag->id);
						$tag->tag_id = intval($tag->tag_id);
						$x[$tag->tag_id] = $tag;
					}
					$tags = $x;
					$results = ['updated'=>0,
							  	'unchanged'=>0,
							  	'added'=>0,
							  	'removed'=>0,
							  	'changes'=>[]];
					$this->log("add-city-tags for city:$city_id, for $what got tags:".print_r($tags, true));

					if ($dozone)
						$this->applyToTagsSpace($tags, $bounds, 1, $in->query);

					foreach($city_ids as $city_id) {
						if ($in->query == 'add-cities-tags')
							$out = $this->applyTagsToCity($tags, $city_id); // this will remove tags and make all cities the same
						else
							$out = $this->addTagsToCity($tags, $city_id); // this will just add the tag
						$results['changes'][] = $out->data;
						$results['updated'] += $out->data['updated'];
						$results['unchanged'] += $out->data['unchanged'];
						$results['added'] += $out->data['added'];
						$results['removed'] += $out->data['removed'];
					}
					unset($x, $tags);
					$cities = $this->getCitiesByIds($city_ids);
					$results['cities'] = $cities;
					$out = new Out('OK', $results);
					break;

				case 'add-listing-tags':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
					if (empty($in->data['listing_id'])) throw new \Exception('No listing_id sent.');
					if (empty($in->data['tags'])) throw new \Exception('No tags sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					$city_id = $in->data['city_id'];
					$listing_id = $in->data['listing_id'];
					$tags = $in->data['tags'];
					$what = $in->data['what'];
					$x = [];
					foreach($tags as $tag) {
						$tag = (object)$tag;
						$tag->id = intval($tag->id);
						$tag->tag_id = intval($tag->tag_id);
						$x[$tag->tag_id] = $tag;
					}
					$tags = $x;
					$this->log("add-city-tags for city:$city_id, for $what got tags:".print_r($tags, true));

					unset($x);
					$out = $this->applyTagsListing($tags, $listing_id);
					break;

				case 'add-listings-tags':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
					if (empty($in->data['listing_ids'])) throw new \Exception('No listing_ids sent.');
					if (empty($in->data['tags'])) throw new \Exception('No tags sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');
					if (empty($in->data['dozone'])) throw new \Exception('No dozone sent.');

					$city_id = $in->data['city_id'];
					$listing_ids = $in->data['listing_ids'];
					$tags = $in->data['tags'];
					$what = $in->data['what'];
					$bounds = (object)$in->data['bounds'];
					$dozone = is_true($in->data['dozone']);

					$x = [];
					foreach($tags as $tag) {
						$tag = (object)$tag;
						$tag->id = intval($tag->id);
						$tag->tag_id = intval($tag->tag_id);
						$x[$tag->tag_id] = $tag;
					}
					$tags = $x;

					if ($dozone)
						$this->applyToTagsSpace($tags, $bounds, 0, $in->query);

					$results = ['unchanged'=>0,
							  	'added'=>0,
							  	'removed'=>0,
							  	'propertyTags'=>0,
							  	'cityListing'=>0,
							  	'changes'=>[]];
					$this->log("add-listings-tags for city:$city_id, for $what got tags:".print_r($tags, true));

					foreach($listing_ids as $listing_id) {
						$out = $this->addToListingTags($tags, $listing_id); //  nonthing removed, only added
						$results['changes'][] = $out->data;
						$results['unchanged'] += $out->data['unchanged'];
						$results['added'] += $out->data['added'];
						$results['removed'] += $out->data['removed'];
						$results['propertyTags'] += $out->data['propertyTags'];
						$results['cityListing'] += $out->data['cityListing'];
					}
					unset($x, $tags);
					$out = new Out('OK', $results);
					break;

				case 'apply-zone-city':
				case 'apply-zone-listing':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['tags'])) throw new \Exception('No tags sent.');
					if (empty($in->data['what'])) throw new \Exception('No what sent.');
					if (empty($in->data['bounds'])) throw new \Exception('No bounds sent.');

					$tags = $in->data['tags'];
					$what = $in->data['what'];
					$bounds = (object)$in->data['bounds'];

					$x = [];
					foreach($tags as $tag) {
						$tag = (object)$tag;
						// $tag->id = intval($tag->id);
						$tag->tag_id = intval($tag->tag_id);
						$x[$tag->tag_id] = $tag;
					}
					$tags = $x;

					$this->applyToTagsSpace($tags, $bounds, $in->query == 'apply-zone-listing' ? 0 : 1, $in->query);
					$out = new Out('OK');
					break;

				case 'save-reservation':
					// $out = new Out('fail', ['msg'=>"testing"]);
					// break;

					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['id'])) throw new \Exception('No id sent.');
					if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
					$fields = $in->data['fields'];
					$id = $in->data['id'];
					$x = $this->getClass('Reservations')->set([(object)['where'=>['id'=>$id],
																		'fields'=>$fields]]);
					$baseMsg = "reservation at id:$id";
					$outMsg = !empty($x) ? "Updated $baseMsg" : "Failed to $baseMsg";
					if (!empty($x)) {
						$reservation = $this->getClass('Reservations')->get((object)['where'=>['id'=>$id]]);
						if (!empty($reservation)) {
							if (!empty($reservation[0]->seller_id)) {
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$reservation[0]->seller_id]]);
								if (!empty($seller)) {
									if (updateProfileFromReservation($reservation[0], $seller[0], $this))
										$outMsg .= ", updated seller:{$reservation[0]->seller_id} from reservation information.";
								}
								else
									$outMsg .= ", failed to get seller with id: {$reservation[0]->seller_id}.";
							}
							$this->getClass('Zoho')->processOneZohoCrm(false, $reservation[0], null);
						}
						else
							$outMsg .= ", but failed to retrieve the reservation back.";
					}
					$out = new Out(!empty($x) ? 'OK' : 'fail', $outMsg);
					break;

				// this from the sales map directly, not gathered cities
				case 'update-city-tags':
					if (file_exists($this->lifestyleStatusFile)) {
						$data = file_get_contents($this->lifestyleStatusFile);
						if ($data === false) {
							$out = new Out('fail', 'Could not open'.$this->lifestyleStatusFile);
							break;
						}
						$data = explode('||', $data);
						$reservationCollation = [];
						foreach($data as $row) {
							if (!empty($row)) {
								$row = json_decode(removeslashes($row));
								if ($row &&
									isset($row->city_id)) {
									$reservationCollation[$row->city_id] = $row;
									$this->log("Read in for $row->locationStr:$row->city_id");
								}
								else
									$this->log("Failed json: $row");
							}
						}

						$dat = [];
						$Sellers = $this->getClass('Sellers', 1);
						global $tag_city;
						$all_cityTags = $tag_city;
						$totalAdded = 0;
						foreach($reservationCollation as $city_id=>$city) {
							// $Sellers->log("City_id:$city_id, ".print_r($city->tags, true));
							// $city->tags = (array)$city->tags;
							$city = (object)$city;
							if (isset($city->tags)) {
								$city_keys = array_keys((array)$city->tags);
								$test_keys = array_intersect($all_cityTags, $city_keys);
							}
							// $this->log("City_id:$city_id, all_cityTags:".print_r($all_cityTags, true));
							// $this->log("City_id:$city_id, city_keys:".print_r($city_keys, true));
							// $this->log("City_id:$city_id, test_keys:".print_r($test_keys, true));
							if (empty($test_keys)) { // then none of them are city tags, but other lifestyle tags
								if (isset($city->locationStr) &&
									isset($city->tags)) {
									$city->tag_state = TAGS_ALL_LISTINGS;
									$dat[] = json_encode($city);
								}
								else
									$this->log("Failed to find a good city - ".print_r($city, true));
								continue;
							}

							$startSize = count($test_keys);
							$city_tags = [];
							$tags = $this->getClass('CitiesTags')->get((object)['where'=>['city_id'=>$city_id],
																				'what'=>['tag_id', 'score']]);
							if (!empty($tags)) {
								// $this->log("City_id:$city_id, found ".count($tags)." tags");
								// $this->log("City_id:$city_id, test_keys:".print_r($test_keys, true));
								// $this->log("City_id:$city_id, city_tags:".print_r($tags, true));
								$needScoring = false;
								$hasScoring = false;
								foreach($tags as $tag) {
									$city_tags[] = $tag->tag_id;
									if ($tag->score == 1) {
										$needScoring = true;
										if (in_array($tag->tag_id, $test_keys)) {
											$this->log("update-city-tags - City_id:$city_id - assign score:$tag->score to $tag->tag_id");
											// $city->tags[strval($tag->tag_id)]->score = $tag->score;
											$city->tags->{$tag->tag_id}->score = $tag->score;
										}
									}
									elseif (in_array($tag->tag_id, $test_keys)) {
										$hasScoring = true;
										$this->log("update-city-tags - City_id:$city_id - assign score:$tag->score to $tag->tag_id");
										// $city->tags[strval($tag->tag_id)]->score = $tag->score;
										$city->tags->{$tag->tag_id}->score = $tag->score;
									}
								}

								$test_keys = array_diff($test_keys, $city_tags);
								$this->log("update-city-tags - City_id:$city_id, needScoring:$needScoring, hasScoring:$hasScoring");
								// $this->log("City_id:$city_id, diff_keys:".print_r($test_keys, true));
								if (count($test_keys) == 0) {
									if ($needScoring)
										$city->tag_state = $hasScoring ? TAGS_HAVE_SOME : TAGS_NEED_ALL;
									else
										$city->tag_state = TAGS_HAVE_ALL;
								}
								else
									$city->tag_state = TAGS_HAVE_SOME;
							}
							else
								$city->tag_state = TAGS_NEED_ALL;

							$added = 0;
							// $this->log("update-city-tags - before City_id:$city_id, ".print_r($city->tags, true));
							if (count($test_keys) > 0) {
								foreach($test_keys as $tag) {
									$x = $this->getClass('CitiesTags')->add(['tag_id'=>$tag,
																			'city_id'=>$city_id,
																			'score'=>1,
																			'clicks'=>0]);
									$this->log("update-city-tags - City_id:$city_id, added tag: $tag, assign score:1");
									// $city->tags[strval($tag)]->score = 1;
									$city->tags->$tag->score = 1;
									if (!empty($x))
										$added++;
								}
							}
							$totalAdded += $added;
							$this->log("update-city-tags - for $city->locationStr, id:$city_id, tag_state:$city->tag_state, added:$added tags, pre-exiting in CitiesTags:".count($city_tags)." tags.");
							// $this->log("update-city-tags - after City_id:$city_id, ".print_r($city->tags, true));
							$dat[] = json_encode($city);
							unset($city_tags, $test_keys, $city);
						}

						$this->log("update-city-tags - returning OK ");
						$out = new Out('OK',(object)['msg'=>"Retrieved $this->lifestyleStatusFile",
													 'count'=>$totalAdded,
													 'data'=>implode('||',$dat)]);
					}
					else
						$out = new Out('fail', "Could not find $this->lifestyleStatusFile");
					break;
				case 'get-sales-info':
					$first = isset($in->data['first']) ? $in->data['first'] : '';
					$last = isset($in->data['last']) ? $in->data['last'] : '';
					$email = isset($in->data['email']) ? $in->data['email'] : '';

					$Sellers = $this->getClass('Sellers', 1);
					$Reservations = $this->getClass('Reservations');
					$sql = "SELECT * FROM {$Reservations->getTableName()} WHERE `type` & ".(SELLER_IS_PREMIUM_LEVEL_2).' AND (!(`flags` & '.(RESERVATION_CONVERTED_LIFESTYLE | RESERVATION_CONVERTED_LIFESTYLE_CODE).') OR `flags` IS NULL) ';
					$sql.= !empty($first) ? "AND `first_name` LIKE '%$first%' " : '';
					$sql.= !empty($last) ? "AND `last_name` LIKE '%$last%' " : '';
					$sql.= !empty($email) ? "AND `email` LIKE '%$email%' " : '';

					$Sellers->log("get-sales-info page get reservations sql: $sql");
					$reservations = $Reservations->rawQuery($sql);
					$reservationCollation = [];

					$x = $reservations;
					$reservations = [];
					foreach($x as $res)
						$reservations[$res->id] = $res;
					unset($x);

					$this->buildReservationCollation($reservationCollation, $reservations);

					$sql = "SELECT a.id, a.author_id, a.first_name, a.last_name, a.email, a.company, a.street_address, a.city, a.state, a.phone, a.mobile, a.flags, GROUP_CONCAT( b.tag_id, ':', c.tag, ':', b.score,  ':', b.city_id, ':', d.city, ':', d.state, ':', d.lat, ':', d.lng  SEPARATOR  '|' ) AS tags ";
					$sql.= "FROM  {$Sellers->getTableName()} AS a ";
					$sql.= "INNER JOIN {$Sellers->getTableName('sellers-tags')} AS b ON a.author_id = b.author_id ";
					$sql.= "INNER JOIN {$Sellers->getTableName('tags')} AS c ON b.tag_id = c.id ";
					$sql.= "INNER JOIN {$Sellers->getTableName('cities')} AS d ON b.city_id = d.id ";
					$sql.= "WHERE b.type =1 AND a.flags & (".(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME).") ";
					$sql.= !empty($first) ? "AND a.first_name LIKE '%$first%' " : '';
					$sql.= !empty($last) ? "AND a.last_name LIKE '%$last%' " : '';
					$sql.= !empty($email) ? "AND a.email LIKE '%$email%' " : '';
					$sql.= "GROUP BY a.author_id";

					// $activeAgents = $Sellers->get((object)['bitand'=>['flags'=>(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)],
					// 									   'where'=>['meta'=>'notnull']);
					$activeAgents = $Sellers->rawQuery( $sql );
					$cityAgentsActive = [];
					if (!empty($activeAgents)) {
						$agents = [];
						// rearrange array to be associative
						foreach($activeAgents as $agent) {
							$agents[$agent->id] = $agent;
							unset($agent);
						}

						unset($activeAgents);
						$activeAgents = $agents;
					}

					$this->buildReservationCollationWithAgents($reservationCollation, $activeAgents);

					$mapCenter = null;
					$this->getMapCenter($mapCenter, $reservationCollation, $Sellers);

					$out = new Out('OK', (object)['reservationCollation'=>$reservationCollation,
												  'reservations'=>$reservations,
												  'activeAgents'=>$activeAgents,
												  'mapCenter'=>$mapCenter]);
					break;
				case 'create-lifestyle-status-file':
					global $RadialDistance;
					$curPage = 0;
					$pageSize = 40;
					global $amTags;
					$Sellers = $this->getClass('Sellers', 1);
					$Reservations = $this->getClass('Reservations');

					$amTags = $Sellers->getAMTagList();

					// get all reservations that still ha
					// $sql = "SELECT a.* FROM {$Reservations->getTableName()} AS a ";
					// $sql.= "INNER JOIN {$Reservations->getTableName('sellers')} AS b ";
					// $sql.= "ON (a.seller_id = b.id AND (b.flags & ".(SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME).") = 0  ) OR a.seller_id = 0 ";
					// $sql.= "GROUP BY a.id";
					$sql = "SELECT * FROM {$Reservations->getTableName()} WHERE `type` & ".(SELLER_IS_PREMIUM_LEVEL_2).' AND (!(`flags` & '.(RESERVATION_CONVERTED_LIFESTYLE | RESERVATION_CONVERTED_LIFESTYLE_CODE).') OR `flags` IS NULL)';
					$Sellers->log("Sales page get reservations sql: $sql");
					$reservations = $Reservations->rawQuery($sql);
					$reservationCollation = [];

					$this->buildReservationCollation($reservationCollation, $reservations);

					$Sellers->log("amTags:".count($amTags).", RadialDistance:".count($RadialDistance[0]));
					if ( (count($amTags) != count($RadialDistance[0])) ) { // remake it!
						require_once(__DIR__.'/../_classes/DistanceScale.php');
						global $DistanceScale;
						$steps = count($amTags);
						$RadialDistance = [];
						$radialListStr = '[';
						foreach($DistanceScale as $index=>$scale) {
							$radials = [];
							$radialsStr = '[';
							$deltaLng = 1/$DistanceScale[$index][0];
							$deltaLat = 1/$DistanceScale[$index][1]; // pretty constant
							for($i = 0; $i < $steps; $i++) {
								$rad = (360/$steps) * (M_PI/180) * $i;
								$x = cos( $rad ) * $deltaLng;
								$y = sin( $rad ) * $deltaLat;
								$radials[] = [$x, $y];
								$radialsStr .= "[$x,$y],";
							}
							$radialsStr .= ']';
							$RadialDistance[] = $radials;
							$radialListStr .= $radialsStr.",\n";
							unset($scale, $radials);
						}
						$radialListStr .= '];';
						// $out = new Out('OK', ['txt'=>print_r($radialList, true)]);
						$file = __DIR__.'/../_classes/RadialDistance.php';
						$fileContents = "<?php\nnamespace AH;\n// lng, lat arrays per 5 degrees starting from 5, 15, 25, etc...,  each containing stepped radial offsets for # of agent match tags we have from Sellers::getAMTagList()\nglobal ".'$'."RadialDistance;\n".'$'."RadialDistance = $radialListStr";
						$Sellers->log("sales.php - whoami:".\get_current_user().", file:$file");
						@file_put_contents($file, $fileContents);
					}


					/*
					SELECT a.first_name, a.last_name, a.email, a.company, a.street_address, a.city, a.state, a.phone, a.flags, a.meta , GROUP_CONCAT( b.tag_id,  ':', b.score,  ':', b.city_id, ':', d.city, ':', d.lat, ':', d.lng, ':', c.tag
					SEPARATOR  '|' ) AS tags
					FROM  `icn_alr_sellers` AS a
					INNER JOIN icn_alr_sellers_tags AS b ON a.author_id = b.author_id
					INNER JOIN icn_alr_tags AS c ON b.tag_id = c.id
					INNER JOIN icn_alr_cities AS d ON b.city_id = d.id
					WHERE b.type =1 AND a.flags & ((1 << 4) | (1 << 5))
					GROUP BY a.author_id
					*/																																		//0				1 			2 				3 				4 				5 			6 			7
					$sql = "SELECT a.id, a.author_id, a.first_name, a.last_name, a.email, a.company, a.street_address, a.city, a.state, a.phone, a.mobile, a.flags, GROUP_CONCAT( b.tag_id, ':', c.tag, ':', b.score,  ':', b.city_id, ':', d.city, ':', d.state, ':', d.lat, ':', d.lng  SEPARATOR  '|' ) AS tags ";
					$sql.= "FROM  {$Sellers->getTableName()} AS a ";
					$sql.= "INNER JOIN {$Sellers->getTableName('sellers-tags')} AS b ON a.author_id = b.author_id ";
					$sql.= "INNER JOIN {$Sellers->getTableName('tags')} AS c ON b.tag_id = c.id ";
					$sql.= "INNER JOIN {$Sellers->getTableName('cities')} AS d ON b.city_id = d.id ";
					$sql.= "WHERE b.type =1 AND a.flags & (".(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME).") GROUP BY a.author_id";

					// $activeAgents = $Sellers->get((object)['bitand'=>['flags'=>(SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME)],
					// 									   'where'=>['meta'=>'notnull']);
					$activeAgents = $Sellers->rawQuery( $sql );
					$cityAgentsActive = [];
					if (!empty($activeAgents)) {
						$agents = [];
						// rearrange array to be associative
						foreach($activeAgents as $agent) {
							$agents[$agent->id] = $agent;
							unset($agent);
						}

						unset($activeAgents);
						$activeAgents = $agents;
					}

					$this->buildReservationCollationWithAgents($reservationCollation, $activeAgents);

					$mapCenter = null;
					$this->getMapCenter($mapCenter, $reservationCollation, $Sellers);

					$msg = '';
					$dat = [];
					foreach($reservationCollation as $data)
						$dat[] = json_encode($data);
					$wrote = file_put_contents($this->lifestyleStatusFile, implode('||',$dat), LOCK_EX);
					unset($dat);
					$msg = !empty($wrote) ? "Wrote $wrote bytes to $this->lifestyleStatusFile." : "Failed to write to $this->lifestyleStatusFile.";

					$dat = [];
					foreach($activeAgents as $data)
						$dat[] = json_encode($data);
					$wrote2 = file_put_contents($this->lifestyleAgentFile, implode('||',$dat), LOCK_EX);
					unset($dat);
					$msg = !empty($wrote2) ? "  Wrote $wrote bytes to $this->lifestyleAgentFile." : "  Failed to write to $this->lifestyleAgentFile.";

					$dat = json_encode($mapCenter);
					$wrote3 = file_put_contents($this->lifestyleMapCenterFile, $dat, LOCK_EX);
					unset($dat);
					$msg = !empty($wrote3) ? "  Wrote $wrote bytes to $this->lifestyleMapCenterFile." : "  Failed to write to $this->lifestyleMapCenterFile.";

					$dat = [];
					foreach($reservations as $data)
						$dat[] = json_encode($data);
					$wrote4 = file_put_contents($this->lifestyleReservationsFile, implode('||',$dat), LOCK_EX);
					unset($dat);
					$msg = !empty($wrote4) ? "  Wrote $wrote bytes to $this->lifestyleReservationsFile." : "  Failed to write to $this->lifestyleReservationsFile.";

					$dat = [];
					foreach($amTags as $data)
						$dat[] = json_encode($data);
					$wrote5 = file_put_contents($this->lifestyleAMTagListile, implode('||',$dat), LOCK_EX);
					unset($dat);
					$msg = !empty($wrote5) ? "  Wrote $wrote bytes to $this->lifestyleAMTagListile." : "  Failed to write to $this->lifestyleAMTagListile.";

					$this->log("create-lifestyle-status-file - $msg");

					$success = !empty($wrote) && !empty($wrote2) && !empty($wrote3) && !empty($wrote4);
					$out = new Out($success ? 'OK' : 'fail', $success ? "Wrote files: $this->lifestyleStatusFile, $this->lifestyleAgentFile, $this->lifestyleMapCenterFile, $this->lifestyleReservationsFile, $this->lifestyleAMTagListile" : $msg);
					break;
				case 'get-lifestyle-status-file':
					if (file_exists($this->lifestyleStatusFile)) {
						$data = file_get_contents($this->lifestyleStatusFile);
						if ($data === false) {
							$out = new Out('fail', 'Could not open'.$this->lifestyleStatusFile);
							break;
						}
						$out = new Out('OK',(object)['msg'=>"Retrieved $this->lifestyleStatusFile",
													 'data'=>$data]);
					}
					else
						$out = new Out('fail', "Could not find $this->lifestyleStatusFile");
					break;
				case 'get-map-center-file':
					if (file_exists($this->lifestyleMapCenterFile)) {
						$data = file_get_contents($this->lifestyleMapCenterFile);
						if ($data === false) {
							$out = new Out('fail', 'Could not open'.$this->lifestyleMapCenterFile);
							break;
						}
						$out = new Out('OK',(object)['msg'=>"Retrieved $this->lifestyleMapCenterFile",
													 'data'=>$data]);
					}
					else
						$out = new Out('fail', "Could not find $this->lifestyleMapCenterFile");
					break;
				case 'get-agent-data-file':
					if (file_exists($this->lifestyleAgentFile)) {
						$data = file_get_contents($this->lifestyleAgentFile);
						if ($data === false) {
							$out = new Out('fail', 'Could not open'.$this->lifestyleAgentFile);
							break;
						}
						$out = new Out('OK',(object)['msg'=>"Retrieved $this->lifestyleAgentFile",
													 'data'=>$data]);
					}
					else
						$out = new Out('fail', "Could not find $this->lifestyleAgentFile");
					break;
				case 'get-reservations-data-file':
					if (file_exists($this->lifestyleReservationsFile)) {
						$data = file_get_contents($this->lifestyleReservationsFile);
						if ($data === false) {
							$out = new Out('fail', 'Could not open'.$this->lifestyleReservationsFile);
							break;
						}
						$out = new Out('OK',(object)['msg'=>"Retrieved $this->lifestyleReservationsFile",
													 'data'=>$data]);
					}
					else
						$out = new Out('fail', "Could not find $this->lifestyleAgentFile");
					break;
				case 'zoho-test-api':
					$zoho = $this->getClass('Zoho');
					$out = $zoho->testAPI();
					break;

				case 'zoho-lead-ids':
					$zoho = $this->getClass('Zoho');
					$out = $zoho->updateLeadIds();
					break;

				case 'zoho-udpate-new-reservations':
					$zoho = $this->getClass('Zoho');
					$out = $zoho->updateNewReservations();
					break;
				case 'zoho-udpate-existing-reservations':
					$zoho = $this->getClass('Zoho');
					$what = isset($in->data['what']) ? $in->data['what'] : 'all';
					if ($what === 'all')
						$out = $zoho->updateExistingReservations();
					else
						$out = $zoho->updateOneExistingReservation($what);
					break;

				case 'update-lead':
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$lead = [];
					if ($type == 'POST') {
						if (isset($_POST['key']))
							$key = $_POST['key'];
						$leadId = isset($_POST['leadID']) ? $_POST['leadID'] : 0;
					}
					else {
						if (isset($_GET['key']))
							$key = $_GET['key'];
						$leadId = isset($_GET['leadID']) ? $_GET['leadID'] : 0;
					}
					$out = $this->getClass('Zoho')->updateLead($leadId);
					$xml = "Reservation update for leadId:$leadId was ".($out->status == 'OK' ? 'successful.' : ' a failure :(').", msg: $out->data";
					$xml.= "You can close this tab now.";
					echo $xml;
					die;
					break;

				case 'update-reservation':
					// $this->log("access-seller - call_type: $in->call_type");
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					$lead = [];
					if ($type == 'POST') {
						if (isset($_POST['key']))
							$key = $_POST['key'];

						$lead['Seller ID'] = isset($_POST['sellerID']) ? $_POST['sellerID'] : 0;
						$lead['Email'] = isset($_POST['email']) ? $_POST['email'] : '';
						$lead['LEADID'] = isset($_POST['leadID']) ? $_POST['leadID'] : 0;
						$lead['Portal'] = isset($_POST['portal']) ? $_POST['portal'] : '';
						$lead['Phone'] = isset($_POST['phone']) ? $_POST['phone'] : '';
						$lead['First Name'] = isset($_POST['first_name']) ? $_POST['first_name'] : '';
						$lead['Last Name'] = isset($_POST['last_name']) ? $_POST['last_name'] : '';
						$lead['Target City'] = isset($_POST['target_city']) ? $_POST['target_city'] : '';
						$lead['Target State'] = isset($_POST['target_state']) ? $_POST['target_state'] : '';
						$lead['State'] = isset($_POST['address_state']) ? $_POST['address_state'] : '';
						$lead['LifeStyle 1'] = isset($_POST['lifestyle1']) ? $_POST['lifestyle1'] : '';
						$lead['LifeStyle 2'] = isset($_POST['lifestyle2']) ? $_POST['lifestyle2'] : '';
						$lead['LifeStyle 3'] = isset($_POST['lifestyle3']) ? $_POST['lifestyle3'] : '';
						$lead['LifeStyle 4'] = isset($_POST['lifestyle4']) ? $_POST['lifestyle4'] : '';
						$lead['L1 Description'] = isset($_POST['lifestyle1desc']) ? $_POST['lifestyle1desc'] : '';
						$lead['L2 Description'] = isset($_POST['lifestyle2desc']) ? $_POST['lifestyle2desc'] : '';
						$lead['L3 Description'] = isset($_POST['lifestyle3desc']) ? $_POST['lifestyle3desc'] : '';
						$lead['L4 Description'] = isset($_POST['lifestyle4desc']) ? $_POST['lifestyle4desc'] : '';
						$lead['Preferred Display Name'] = isset($_POST['preferredName']) ? $_POST['preferredName'] : '';
						$lead['Real Estate License number'] = isset($_POST['bre']) ? $_POST['bre'] : '';
						$lead['Year Licensed'] = isset($_POST['yrLicensed']) ? $_POST['yrLicensed'] : '';
						$lead['Years lived in Service Area'] = isset($_POST['yrsInServiceArea']) ? $_POST['yrsInServiceArea'] : '';
						$lead['Transactions/year'] = isset($_POST['transactions']) ? $_POST['transactions'] : '';
					}
					else {
						if (isset($_GET['key']))
							$key = $_GET['key'];
						$lead['Seller ID'] = isset($_GET['sellerID']) ? $_GET['sellerID'] : 0;
						$lead['Email'] = isset($_GET['email']) ? $_GET['email'] : '';
						$lead['LEADID'] = isset($_GET['leadID']) ? $_GET['leadID'] : 0;
						$lead['Portal'] = isset($_GET['portal']) ? $_GET['portal'] : '';
						$lead['Phone'] = isset($_GET['phone']) ? $_GET['phone'] : '';
						$lead['First Name'] = isset($_GET['first_name']) ? $_GET['first_name'] : '';
						$lead['Last Name'] = isset($_GET['last_name']) ? $_GET['last_name'] : '';
						$lead['Target City'] = isset($_GET['target_city']) ? $_GET['target_city'] : '';
						$lead['Target State'] = isset($_GET['target_state']) ? $_GET['target_state'] : '';
						$lead['State'] = isset($_GET['address_state']) ? $_GET['address_state'] : '';
						$lead['LifeStyle 1'] = isset($_GET['lifestyle1']) ? $_GET['lifestyle1'] : '';
						$lead['LifeStyle 2'] = isset($_GET['lifestyle2']) ? $_GET['lifestyle2'] : '';
						$lead['LifeStyle 3'] = isset($_GET['lifestyle3']) ? $_GET['lifestyle3'] : '';
						$lead['LifeStyle 4'] = isset($_GET['lifestyle4']) ? $_GET['lifestyle4'] : '';
						$lead['L1 Description'] = isset($_GET['lifestyle1desc']) ? $_GET['lifestyle1desc'] : '';
						$lead['L2 Description'] = isset($_GET['lifestyle2desc']) ? $_GET['lifestyle2desc'] : '';
						$lead['L3 Description'] = isset($_GET['lifestyle3desc']) ? $_GET['lifestyle3desc'] : '';
						$lead['L4 Description'] = isset($_GET['lifestyle4desc']) ? $_GET['lifestyle4desc'] : '';
						$lead['Preferred Display Name'] = isset($_GET['preferredName']) ? $_GET['preferredName'] : '';
						$lead['Real Estate License number'] = isset($_GET['bre']) ? $_GET['bre'] : '';
						$lead['Year Licensed'] = isset($_GET['yrLicensed']) ? $_GET['yrLicensed'] : '';
						$lead['Years lived in Service Area'] = isset($_GET['yrsInServiceArea']) ? $_GET['yrsInServiceArea'] : '';
						$lead['Transactions/year'] = isset($_GET['transactions']) ? $_GET['transactions'] : '';
					}
					if ($key != zohoToLLCKey_UpdateReservation) {
						$this->log("update-reservation - incorrect key!");
						throw new \Exception (__FILE__.':('.__LINE__.') - bad key sent');
					}

					$lead['Last Name'] = html_entity_decode( filter_var($lead['Last Name'], FILTER_SANITIZE_STRING), ENT_QUOTES);

					$this->log("update-reservation got ".print_r($lead, true));
					$out = $this->getClass('Zoho')->updateLead($lead['LEADID']);
					$this->log("$in->query - Reservation update for leadId:$leadId was ".($out->status == 'OK' ? 'successful.' : ' a failure :(').", msg: $out->data");
					// $out = $this->getClass('Zoho')->updateOneLead($lead);
					// $xml = "Reservation update for {$lead['First Name']} {$lead['Last Name']} was ".($out ? 'successful.' : ' a failure :(');
					$xml = '<!DOCTYPE html>';
					$xml .= '<html>';
					$xml .= '<head><style>';
					$xml .= 	'body {background-color:lightgrey}';
					$xml .= 	'h2   {color:blue}';
					$xml .=		'h1   {color:green}';
					$xml .=		'</style>';
					$xml .= 	'<title>Message</title>';
					$xml .= '</head>';
					$xml .= '<body><h2 style="color:blue">You can close this tab now.</h2>';
					$xml .= '<h1 style="color:red">Reservation has been updated.</h1></body>';
					$xml .= '</html>';
					echo $xml;
					die;
					break;

				case 'delete-reservation':
					if (isset($_POST['key']))
						$key = $_POST['key'];
					$this->log("delete-reservation - call_type: $in->call_type, key: $key");
					if (trim($key) != zohoToLLCKey_DeleteReservation) {
						$this->log("delete-reservation - incorrect key, expected:".zohoToLLCKey_DeleteReservation);
						throw new \Exception (__FILE__.':('.__LINE__.') - bad key sent');
					}
					$sellerId = isset($_POST['sellerID']) ? $_POST['sellerID'] : 0;
					$email = isset($_POST['email']) ? $_POST['email'] : '';
					$this->log("delete-reservation - sellerID:$sellerId, email:$email");
					$res = $this->getClass('Reservations')->get((object)['like'=>['email'=>$email]]);
					if (!empty($res))
						$x = $this->getClass('Reservations')->delete((object)['id'=>$res[0]->id]);
					$this->log("delete-reservation - got from delete: $x");
					break;

				case 'change-status':
					if (isset($_POST['key']))
						$key = $_POST['key'];
					$this->log("change-status - call_type: $in->call_type, key: $key");
					if ($key != zohoToLLCKey) {
						$this->log("change-status - incorrect key!");
						throw new \Exception (__FILE__.':('.__LINE__.') - bad key sent');
					}
					$sellerId = isset($_POST['sellerID']) ? $_POST['sellerID'] : 0;
					$email = isset($_POST['email']) ? $_POST['email'] : '';
					$this->log("change-status - sellerID:$sellerId, email:$email");
					break;

				case 'access-seller':
				case 'access-seller-contact':
					// $this->log("access-seller - call_type: $in->call_type");
					$type = $in->call_type == 'post' ? 'POST' : 'GET';
					if ($type == 'POST') {
						if (isset($_POST['key']))
							$key = $_POST['key'];
						$sellerId = isset($_POST['sellerID']) ? $_POST['sellerID'] : 0;
						$email = isset($_POST['email']) ? $_POST['email'] : '';
						$leadId = isset($_POST['leadID']) ? $_POST['leadID'] : 0;
						$fromLocal = isset($_POST['fromLocal']) ? $_POST['fromLocal'] : 0;
					}
					else {
						if (isset($_GET['key']))
							$key = $_GET['key'];
						$sellerId = isset($_GET['sellerID']) ? $_GET['sellerID'] : 0;
						$email = isset($_GET['email']) ? $_GET['email'] : '';
						$leadId = isset($_GET['leadID']) ? $_GET['leadID'] : 0;
						$fromLocal = isset($_GET['fromLocal']) ? $_GET['fromLocal'] : 0;
					}
					if ($key != zohoToLLCKey_AccessSeller) {
						$this->log("$in->query - type:$type, incorrect key:$key");
						throw new \Exception (__FILE__.':('.__LINE__.') - bad key sent,'.print_r($_GET, true));
					}
					$this->log("$in->query - type:$type, using sellerId:".$sellerId.", email:".$email.", leadId:$leadId, query: $in->query, fromLocal:".($fromLocal ? 'yes' : 'no'));

					// if ($in->query == 'access-seller') then from LEADS, else from CONTACTS
					$out = $this->getClass('Zoho')->updateLead($leadId, ($in->query == 'access-seller'));
					$this->log("$in->query - Reservation update for leadId:$leadId was ".($out->status == 'OK' ? 'successful' : ' a failure :(').", msg: $out->data");
					unset($out); $out = null;
					// }
					// $this->log("access-seller - sellerID:$sellerId, email:$email,  key: $key");

					// fix zoho's fucked up email formatting
					$email = removeslashes($email);
					$this->log("email after removeslashes: $email");				
					$fixedEmail = preg_replace('/\xFE\xFF\x30/', '', $email);
					// for($i = 0; $i < strlen($fixedEmail); $i++)
					// 	$this->log($i.":".ord($fixedEmail[$i]));
					$this->log("email:$email, fixedEmail:$fixedEmail");
					$email = $fixedEmail;

					$Sellers = $this->getClass('Sellers');
					$seller = null;
					if (empty($sellerId)) {
						$seller = $this->getClass('Sellers', 1)->get((object)['like'=>['email'=>$email]]);
						// $sql = "SELECT * FROM {$Sellers->getTableName()} WHERE `email` = $email";
						// $seller = $Sellers->rawQuery($sql);
					}
					else {
						$seller = $this->getClass('Sellers', 1)->get((object)['where'=>['id'=>$sellerId]]);
						// $sql = "SELECT * FROM {$Sellers->getTableName()} WHERE `id` = $sellerId";
						// $seller = $Sellers->rawQuery($sql);
					}

					$this->log("After get - SellerId:".$sellerId.", email:".$email);
					$this->log(" was - ".(!empty($seller) ? "found" : "not found") );

					// try to find the reservation using the email, and get it's id, and check for proper LEADID value
					$reserveId = 0;
					$reservation = $this->getClass('Reservations')->get((object)['like'=>['email'=>$email]]);
					// if ($in->query == 'access-seller') {
						if (!empty($reservation)) {
							$reserveId = $reservation[0]->id;
							if ($reservation[0]->lead_id != $leadId) {
								$this->log("$in->query - updating LEADID from {$reservation[0]->lead_id} to $leadId for $email");
								$reservation[0]->lead_id = $leadId;
								$this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservation[0]->id],
																			   'fields'=>['lead_id'=>$leadId]]]);
							}
						}
						else if (!empty($seller)) {
							$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller[0]->id]]);
							if (!empty($reservation)) {
								$reserveId = $reservation[0]->id;
								if ($reservation[0]->lead_id != $leadId) {
									$this->log("$in->query - updating LEADID from {$reservation[0]->lead_id} to $leadId for $email");
									$reservation[0]->lead_id = $leadId;
									$this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservation[0]->id],
																				   'fields'=>['lead_id'=>$leadId]]]);
								}
							}
						}
					// }

					// I believe the 3 lines below are redundant, as Zoho::updateOneLoead() calls it already
					// if (!empty($reservation) &&
					// 	!empty($seller))
					// 	updateProfileFromReservation($reservation[0], $seller[0], $this);


					// if (empty($reservation)) {
					// 	$h = '<!DOCTYPE html>';
					// 	$h .= '<html><head><h2>Failed to find reservation</h2></head>';
					// 	$h .= '<body><h1>Reservation for email:'.$email.'could not be found.<br/>Please contact administrator</h1></body>';
					// 	$h .= '</html>';
					// 	echo $h;
					// 	die;
					// }

					wp_logout(); // just in case...
					wp_set_current_user(0);
					wp_set_auth_cookie(0);
					$this->log("$in->query - logged out current user, userID is now:".wp_get_current_user()->ID);

					$user = get_user_by_email( $email ); // if we get this, the agent has at least did a basic registration
					$testPasswd = '';

					// USE CASES
					// 1: Listhub Agent, but not registered in any way - has seller, but no author_id
					// 2: Registered ($user is not empty), but is not a $seller - have user, empty seller
					// 3: Not Listhub and not registered - empty both $seller and $user
					// 4: Registered as agent - seller with author_id
					//
					// Use case 1 and 3 - need to register completely as agent
					// Use case 2 - need to upgrade to agent
					// Use case 4 - login and go to agent-match-sales page

					
					if ( (!empty($seller) &&
						  empty($seller[0]->author_id)) ||
						 (empty($seller) &&
						  empty($user)) ) { // need to register as user first

						if (empty($sellerId) &&
							!empty($reserveId)) 
							$sellerId = 'T-'.$reserveId; // user reservation details to populate into
														 // else $sellerId is the Listhub agent's id in Sellers table, so use that

						$this->log("{$in->query} - calling seller-create for new user");
						$url = get_home_url().'/seller-create/'.$sellerId;
						if (!$fromLocal) {
							if (headers_sent())
								echo '<script type="text/javascript"> window.location = "'.$url.'"; </script>';
							else {
								header('Location:'.$url);
								header("Connection: close\r\n", true);
								header("Content-Encoding: none\r\n");
								header("Content-Length: 0", true);
								session_write_close();
								flush();
								ob_flush();	
								die;
							}
						}
						else
							$out = new Out('OK', $url);
					}
					elseif ( empty($seller) &&
							 !empty($user) ) {
						$sellerId = 'U-'.$user->ID; // this will tell the seller-create page that we are needing to upgrade to agent
						$magic = get_user_meta($user->ID, 'user_key', true);
						$i = 0;
						if (!empty($magic)) foreach($magic as $value)
							$testPasswd .= chr( $value ^ ($i++ + ord('.')));

						$this->log("{$in->query} - calling seller-create for user:$sellerId, password:$testPasswd");
						if (empty($testPasswd)) {
							$msg = '<h2>Failed to retrieve user password.</h2>';
							$msg .= '<h1>User with ID: '.$user->ID.', could not be logged in.<br/>Please contact administrator</h1>';
							if (!$fromLocal) {
								$xml = '<!DOCTYPE html>';
								$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
								$xml .= '</html>';
								echo $xml;
								die;
							}
							else
								$out = new Out('fail', $msg);
						}	

						$creds = array();
						$creds['user_login'] = $user->user_login;
						$creds['user_password'] = $testPasswd;
						$creds['remember'] = true;
						$user = wp_signon( $creds, false );
						if ( is_wp_error($user) ) {
							$msg = '<h2>Failed to login seller.</h2>';
							$msg .= '<h1>Seller with author id: '.$seller[0]->author_id.', failed to login with error: '.$user->get_error_message().'.<br/>Please contact administrator</h1>';
							if (!$fromLocal) {
								$xml = '<!DOCTYPE html>';
								$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
								$xml .= '</html>';
								echo $xml;
								die;
							}
							else
								$out = new Out('fail', $msg);
						}

						$user = wp_set_current_user($user->ID, $user->user_login);
						$this->log("UserId: $user->ID after wp_set_current_user()");
						// $user = wp_get_current_user();
						wp_set_auth_cookie( $user->ID );
						do_action('wp_login', $user->user_login, $user);

						$url = get_home_url().'/seller-create/'.$sellerId;

						if (!$fromLocal) {
							if (headers_sent())
								echo '<script type="text/javascript"> window.location = "'.$url.'"; </script>';
							else {
								header('Location:'.$url);
								header("Connection: close\r\n", true);
								header("Content-Encoding: none\r\n");
								header("Content-Length: 0", true);
								session_write_close();
								flush();
								ob_flush();	
								die;
							}
						}
						else
							$out = new Out('OK', $url);
					}
					else {
						// now log in this seller
						$seller = array_pop($seller);
						$user = get_userdata($seller->author_id);
						$magic = get_user_meta($user->ID, 'user_key', true);
						$i = 0;
						if (!empty($magic)) {
							$this->log("$in->query - Found user_key for sellerId:$seller->id, userID:$user->ID - ".print_r($magic, true));
							foreach($magic as $value)
								$testPasswd .= chr( $value ^ ($i++ + ord('.')));
						}
						else {
							if (!empty($seller->meta)) {
								// $seller->meta = json_decode($seller->meta);
								foreach($seller->meta as $meta) {
									if ($meta->action == SELLER_KEY) {
										$this->log("Found SELLER_KEY for $seller->id, meta:$meta->key");
										$key = json_decode($meta->key);
										$len = count($key); // strlen($meta->key);
										for($i = 0; $i < $len; $i++)
											$testPasswd .= chr( $key[$i] ^ ($i + ord('.')) );
									}
								}
							}
						}
						
						$this->log("$in->query - calling seller-create for existing seller with userID:$seller->author_id, password:$testPasswd");
						$msg = '<h2>Failed to retrieve seller password.</h2>';
						$msg .= '<h1>Seller with author id: '.$seller->author_id.', could not be logged in.<br/>Please contact administrator</h1>';
						if (empty($testPasswd)) {
							if (!$fromLocal) {
								$this->log("Failed seller: ".print_r($seller, true));
								$xml = '<!DOCTYPE html>';
								$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
								$xml .= '</html>';
								echo $xml;
								die;
							}
							else
								$out = new Out('fail', $msg);
						}								
						
						$this->log($testPasswd." ".$user->user_login);

						$creds = array();
						$creds['user_login'] = $user->user_login;
						$creds['user_password'] = $testPasswd;
						$creds['remember'] = true;
						$user = wp_signon( $creds, false );
						if ( is_wp_error($user) ) {
							$this->log("Failed seller: ".print_r($seller, true));
							$msg = '<h2>Failed to login seller.</h2>';
							$msg .= '<h1>Seller with author id: '.$seller->author_id.', failed to login with error: '.$user->get_error_message().'.<br/>Please contact administrator</h1>';
							if (!$fromLocal) {
								$xml = '<!DOCTYPE html>';
								$xml .= '<html><title>Message</title><body>'.$msg.'</body>';
								$xml .= '</html>';
								echo $xml;
								die;
							}
							else
								$out = new Out('fail', $msg);
						}
						
						$user = wp_set_current_user($user->ID, $user->user_login);
						$this->log("UserId: $user->ID after wp_set_current_user()");
						wp_set_auth_cookie( $user->ID );
						do_action('wp_login', $user->user_login, $user);
						
						// $url = get_home_url()."/sellers/Z-{$user->ID}_agent-match-sales";
						$wpId = wp_get_current_user()->ID;
						global $current_user;
      					get_currentuserinfo();
      					$this->log("$in->query - login:$current_user->user_login, id:$current_user->ID");

						$url = get_home_url()."/sellers/L-$wpId";
						$this->log("$in->query - Seller:$seller->id, with userID:$seller->author_id is logged in with $testPasswd as {$creds['user_login']}, actual user is $wpId, is logged in:".is_user_logged_in().", header_sent:".(headers_sent() ? "yes" : 'no').", url:$url");
						if (!$fromLocal) {
							if (headers_sent() ) 
								echo '<script type="text/javascript"> window.location = "'.$url.'"; </script>';
							else {
								header('Location:'.$url, true, 302);
								header("Connection: close\r\n", true);
								// header("Content-Encoding: none\r\n");
								// header("Content-Length: 0", true);
								// session_write_close();
								// flush();
								// ob_flush();								
								die;
							}
						}
						else {
							$out = new Out('OK', $url);
						}
					}
					break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}

	protected function applyTagsToCity($tags, $city_id) {
		$existing = $this->getClass('CitiesTags')->get((object)['where'=>['city_id'=>$city_id]]);
		$x = [];
		if (!empty($existing)) foreach($existing as $tag) {
			$x[$tag->tag_id] = $tag;
			unset($tag);
		}
		$existing = $x;
		unset($x);

		$incoming = array_keys($tags);
		$alreadyHave = [];
		$removeFromExisting = [];
		$updated = 0;
		$unchanged = 0;
		if (!empty($existing)) foreach($existing as $i=>$tag) {
			if (in_array($tag->tag_id, $incoming)) {
				$tag->score = $tag->score == -1 ? 10 : $tag->score;
				$fields = [];
				if ($tag->score != $tags[$tag->tag_id]->score) {
					$fields['score'] = $tags[$tag->tag_id]->score;
					if ( !($tag->flags & CITY_TAG_MODIFIED_BY_SALES_ADMIN) )
						$fields['flags'] = $tag->flags | CITY_TAG_MODIFIED_BY_SALES_ADMIN;
				}
				if (count($fields)) {
					$updated++;
					$this->log("applyTagsToCity for city:$city_id, updating tag:$tag->tag_id with score:".$tags[$tag->tag_id]->score);
					$this->getClass('CitiesTags')->set([(object)['where'=>[	'city_id'=>$tag->city_id,
																			'tag_id'=>$tagId ],
															  	'fields'=>$fields ]]);
				}
				else {
					$this->log("applyTagsToCity for city:$city_id, unchanged tag:$tag->tag_id with score:$tag->score");
					$unchanged++;
				}
				$alreadyHave[] = $tag->tag_id;
				unset($fields);
			}
			else
				$removeFromExisting[] = $tag->tag_id; // since it wasn't in incoming, these had a score of 0 in the sales page
			unset($tag);
		}

		$notHaveYet = array_diff($incoming, $alreadyHave);
		$this->log("removeFromExisting: ".(count($removeFromExisting) ? print_r($removeFromExisting, true) : "none"));
		$this->log("notHaveYet: ".(count($notHaveYet) ? print_r($notHaveYet, true) : "none"));

		if ( !empty($removeFromExisting) ) foreach($removeFromExisting as $tagId) {
			$this->log("applyTagsToCity for city:$city_id, deleting tag:$tagId, id:".$existing[$tagId]->id);
			$result = $this->getClass('CitiesTags')->delete((object)['id'=>$existing[$tagId]->id]);
			$this->log("deleted CitiesTag, id:".$existing[$tagId]->id.", tagId:$tagId, city:$city_id, result:".($result ? "success" : "failure"));
		}

		if ( !empty($notHaveYet) ) foreach($notHaveYet as $tagId) {
			$this->log("applyTagsToCity for city:$city_id, adding tag:$tagId, score:".$tags[$tagId]->score);
			$result = $this->getClass('CitiesTags')->add(['tag_id'=>$tagId,
														  'city_id'=>$city_id,
														  'score'=>$tags[$tagId]->score,
														  'clicks'=>0,
														  'flags'=>CITY_TAG_ASSIGNED_BY_SALES_ADMIN]);
			$this->log("added CitiesTag, tagId:$tagId, city:$city_id, score:".$tags[$tagId]->score.", result:".($result ? "success" : "failure"));
		}

		$out = new Out('OK', ['city_id'=>$city_id,
							  'updated'=>$updated,
							  'unchanged'=>$unchanged,
							  'added'=>count($notHaveYet),
							  'removed'=>count($removeFromExisting)]);
		unset($removeFromExisting, $notHaveYet, $alreadyHave, $incoming, $existing);
		return $out;
	}

	protected function addTagsToCity($tags, $city_id) {
		$existing = $this->getClass('CitiesTags')->get((object)['where'=>['city_id'=>$city_id]]);
		$x = [];
		if (!empty($existing)) foreach($existing as $tag) {
			$x[$tag->tag_id] = $tag;
			unset($tag);
		}
		$existing = $x;
		unset($x);

		$incoming = array_keys($tags);
		$incomingTags =  $tags;
		$alreadyHave = [];
		$removeFromExisting = [];
		$updated = 0;
		$unchanged = 0;
		if (!empty($existing)) foreach($existing as $i=>$tag) {
			if (in_array($tag->tag_id, $incoming)) {
				$tag->score = $tag->score == -1 ? 10 : $tag->score;
				$fields = [];
				if ($tag->score != $tags[$tag->tag_id]->score) {
					$fields['score'] = $tags[$tag->tag_id]->score;
					if ( !($tag->flags & CITY_TAG_MODIFIED_BY_SALES_ADMIN) )
						$fields['flags'] = $tag->flags | CITY_TAG_MODIFIED_BY_SALES_ADMIN;
				}
				if (count($fields)) {
					$updated++;
					$this->log("addTagsToCity for city:$city_id, updating tag:$tag->tag_id with score:".$tags[$tag->tag_id]->score);
					$this->getClass('CitiesTags')->set([(object)['where'=>[	'city_id'=>$tag->city_id,
																			'tag_id'=>$tagId ],
															  	'fields'=>$fields ]]);
				}
				else {
					$this->log("addTagsToCity for city:$city_id, unchanged tag:$tag->tag_id with score:$tag->score");
					$unchanged++;
				}
				$alreadyHave[] = $tag->tag_id;
				unset($incomingTags[$tag->tag_id]); // remove it..
				unset($fields);
			}
			// else
			// 	$removeFromExisting[] = $tag->tag_id; // since it wasn't in incoming, these had a score of 0 in the sales page
			unset($tag);
		}

		$notHaveYet = !empty($incomingTags) ? array_keys($incomingTags) : [];
		$this->log("removeFromExisting: ".(count($removeFromExisting) ? print_r($removeFromExisting, true) : "none"));
		$this->log("notHaveYet: ".(count($notHaveYet) ? print_r($notHaveYet, true) : "none"));

		if ( !empty($removeFromExisting) ) foreach($removeFromExisting as $tagId) {
			$this->log("addTagsToCity for city:$city_id, deleting tag:$tagId, id:".$existing[$tagId]->id);
			$result = $this->getClass('CitiesTags')->delete((object)['id'=>$existing[$tagId]->id]);
			$this->log("deleted CitiesTag, id:".$existing[$tagId]->id.", tagId:$tagId, city:$city_id, result:".($result ? "success" : "failure"));
		}

		if ( !empty($notHaveYet) ) foreach($notHaveYet as $tagId) {
			$this->log("addTagsToCity for city:$city_id, adding tag:$tagId, score:".$tags[$tagId]->score);
			$result = $this->getClass('CitiesTags')->add(['tag_id'=>$tagId,
														  'city_id'=>$city_id,
														  'score'=>$tags[$tagId]->score,
														  'clicks'=>0,
														  'flags'=>CITY_TAG_ASSIGNED_BY_SALES_ADMIN]);
			$this->log("added CitiesTag, tagId:$tagId, city:$city_id, score:".$tags[$tagId]->score.", result:".($result ? "success" : "failure"));
		}

		$out = new Out('OK', ['city_id'=>$city_id,
							  'updated'=>$updated,
							  'unchanged'=>$unchanged,
							  'added'=>count($notHaveYet),
							  'removed'=>count($removeFromExisting)]);
		unset($removeFromExisting, $notHaveYet, $alreadyHave, $incoming, $existing);
		return $out;
	}

	protected function applyTagsListing($tags, $listing_id) {
		$sql = 'SELECT a.*, b.type, b.tag FROM `'.$this->getClass('ListingsTags')->getTableName('listings-tags').'` AS a ';
		$sql.= 'INNER JOIN '.$this->getClass('ListingsTags')->getTableName('tags').' AS b ON a.tag_id = b.id ';
		$sql.= 'WHERE a.listing_id = '.$listing_id;

		$existing = $this->getClass('ListingsTags')->rawQuery($sql);
		$x = [];
		if (!empty($existing)) foreach($existing as $tag) {
			$x[$tag->tag_id] = $tag;
			unset($tag);
		}
		$existing = $x;
		unset($x);

		global $tag_listing_for_sales_admin;
		$incoming = array_keys($tags);
		$alreadyHave = [];
		$removeFromExisting = [];
		$unchanged = 0;
		$cityListing = 0;
		$propertyTags = 0;
		if (!empty($existing)) foreach($existing as $i=>$tag) {
			if (in_array($tag->tag_id, $incoming)) {
				$this->log("applyTagsListing for listing:$listing_id, unchanged tag:$tag->tag_id");
				$unchanged++;
				$alreadyHave[] = $tag->tag_id;
			}
			else if ($tag->type == 0) {// remove only listing type tags, leave along everything else
				if (in_array($tag->tag_id, $tag_listing_for_sales_admin)) // but only if it's part of $tag_listing_for_sales_admin list of tags
					$removeFromExisting[] = $tag->tag_id; // since it wasn't in incoming, these were unchecked in the sales page
				else
					$propertyTags++;
			}
			else
				$cityListing++;
			unset($tag);
		}

		$notHaveYet = array_diff($incoming, $alreadyHave);
		$this->log("removeFromExisting: ".(count($removeFromExisting) ? print_r($removeFromExisting, true) : "none"));
		$this->log("notHaveYet: ".(count($notHaveYet) ? print_r($notHaveYet, true) : "none"));

		if ( !empty($removeFromExisting) ) foreach($removeFromExisting as $tagId) {
			$this->log("applyTagsListing for listing:$listing_id, deleting tag:$tagId, id:".$existing[$tagId]->id);
			$result = $this->getClass('ListingsTags')->delete((object)['id'=>$existing[$tagId]->id]);
			$this->log("deleted ListingsTags, id:".$existing[$tagId]->id.", tagId:$tagId, listing:$listing_id, result:".($result ? "success" : "failure"));
		}

		if ( !empty($notHaveYet) ) foreach($notHaveYet as $tagId) {
			$this->log("applyTagsListing for listing:$listing_id, adding tag:$tagId,");
			$result = $this->getClass('ListingsTags')->add(['tag_id'=>$tagId,
														  'listing_id'=>$listing_id,
														  'clicks'=>0,
														  'flags'=>LISTING_TAG_ASSIGNED_BY_SALES_ADMIN]);
			$this->log("added ListingsTags, tagId:$tagId, listing:$listing_id, result:".($result ? "success" : "failure"));
		}

		$x = [];
		$tags = $this->getClass('ListingsTags')->rawQuery($sql);
		if (!empty($tags)) foreach($tags as $tag)
			if ($tag->type == 0) 
				$x[$tag->tag_id] = $tag;
			
		$tags = $x;

		$out = new Out('OK', ['listing_id'=>$listing_id,
							  'tags'=>$tags,
							  'unchanged'=>$unchanged,
							  'added'=>count($notHaveYet),
							  'removed'=>count($removeFromExisting),
							  'propertyTags'=>$propertyTags,
							  'cityListing'=>$cityListing]);
		unset($removeFromExisting, $notHaveYet, $alreadyHave, $incoming, $existing, $x);
		return $out;
	}

	protected function addToListingTags($tags, $listing_id) {
		$sql = 'SELECT a.*, b.type, b.tag FROM `'.$this->getClass('ListingsTags')->getTableName('listings-tags').'` AS a ';
		$sql.= 'INNER JOIN '.$this->getClass('ListingsTags')->getTableName('tags').' AS b ON a.tag_id = b.id ';
		$sql.= 'WHERE a.listing_id = '.$listing_id;

		$existing = $this->getClass('ListingsTags')->rawQuery($sql);
		$x = [];
		if (!empty($existing)) foreach($existing as $tag) {
			$x[$tag->tag_id] = $tag;
			unset($tag);
		}
		$existing = $x;
		unset($x);

		global $tag_listing_for_sales_admin;
		$incoming = array_keys($tags);
		$incomingTags =  $tags;
		$alreadyHave = [];
		$removeFromExisting = [];
		$unchanged = 0;
		$cityListing = 0;
		$propertyTags = 0;
		if (!empty($existing)) foreach($existing as $i=>$tag) {
			if (in_array($tag->tag_id, $incoming)) {
				$this->log("applyTagsListing for listing:$listing_id, unchanged tag:$tag->tag_id");
				$unchanged++;
				$alreadyHave[] = $tag->tag_id;
				unset($incomingTags[$tag->tag_id]); // remove it..
			}
			else if ($tag->type == 0) {// remove only listing type tags, leave along everything else
				// if (in_array($tag->tag_id, $tag_listing_for_sales_admin)) // but only if it's part of $tag_listing_for_sales_admin list of tags
				// 	$removeFromExisting[] = $tag->tag_id; // since it wasn't in incoming, these were unchecked in the sales page
				// else
					$propertyTags++;
			}
			else
				$cityListing++;
			unset($tag);
		}

		$notHaveYet = !empty($incomingTags) ? array_keys($incomingTags) : [];
		$this->log("removeFromExisting: ".(count($removeFromExisting) ? print_r($removeFromExisting, true) : "none"));
		$this->log("notHaveYet: ".(count($notHaveYet) ? print_r($notHaveYet, true) : "none"));

		if ( !empty($removeFromExisting) ) foreach($removeFromExisting as $tagId) {
			$this->log("applyTagsListing for listing:$listing_id, deleting tag:$tagId, id:".$existing[$tagId]->id);
			$result = $this->getClass('ListingsTags')->delete((object)['id'=>$existing[$tagId]->id]);
			$this->log("deleted ListingsTags, id:".$existing[$tagId]->id.", tagId:$tagId, listing:$listing_id, result:".($result ? "success" : "failure"));
		}

		if ( !empty($notHaveYet) ) foreach($notHaveYet as $tagId) {
			$this->log("applyTagsListing for listing:$listing_id, adding tag:$tagId,");
			$result = $this->getClass('ListingsTags')->add(['tag_id'=>$tagId,
														  'listing_id'=>$listing_id,
														  'clicks'=>0,
														  'flags'=>LISTING_TAG_ASSIGNED_BY_SALES_ADMIN]);
			$this->log("added ListingsTags, tagId:$tagId, listing:$listing_id, result:".($result ? "success" : "failure"));
		}

		$x = [];
		$tags = $this->getClass('ListingsTags')->rawQuery($sql);
		if (!empty($tags)) foreach($tags as $tag)
			if ($tag->type == 0) 
				$x[$tag->tag_id] = $tag;
			
		$tags = $x;

		$out = new Out('OK', ['listing_id'=>$listing_id,
							  'tags'=>$tags,
							  'unchanged'=>$unchanged,
							  'added'=>count($notHaveYet),
							  'removed'=>count($removeFromExisting),
							  'propertyTags'=>$propertyTags,
							  'cityListing'=>$cityListing]);
		unset($removeFromExisting, $notHaveYet, $alreadyHave, $incoming, $incomingTags, $existing, $x);
		return $out;
	}


	protected function getAgentsInCities(&$cities) {
		if (!empty($cities)) foreach($cities as &$city) {
			$list = $this->getClass("Sellers")->get((object)['where'=>['city'=>$city->city,
																	   'state'=>$city->state],
															 'what'=>['id','author_id','first_name','last_name','email','flags']]);
			if (!empty($list)) {
				// $list = array_map( function($ele) { $ele->city_id = $city->id; return $ele; }, $list);
				// $agents = array_merge($agents, $list);
				$city->agents = $list;
			}
			unset($city);
		}
		return $cities;
	}

	protected function getListingsInCities(&$cities) {
		require_once(__DIR__.'/../_classes/DistanceScale.php');
		global $DistanceScale;

		$Listings = $this->getClass("Listings", 1);
		if (!empty($cities)) foreach($cities as &$city) {
			$list = $Listings->get((object)['where'=>['city'=>$city->city,
													  'state'=>$city->state],
											'or'=>['active'=>[1,2,3,4,6]],
											'what'=>['id','street_address','images','first_image','about','beds','baths','active']]);
			if (!empty($list)) {
				// $list = array_map( function($ele) { $ele->city_id = $city->id; return $ele; }, $list);
				// $agents = array_merge($agents, $list);
				$ids = [];
				$x = [];
				foreach($list as $l) {
					$ids[] = $l->id;
					$l->image = $this->getFirstImage($l);
					unset($l->images, $l->first_image);
					$x[$l->id] = $l;
					$x[$l->id]->lat = -1; // default
					$x[$l->id]->lng = -1; // default
					unset($l);
				}
				unset($list);
				$list = $x;

				$geo = $this->getClass('ListingsGeoinfo')->get((object)['where'=>['listing_id'=>$ids]]);
				foreach($geo as $g) {
					$list[$g->listing_id]->lat = floatval($g->lat);
					$list[$g->listing_id]->lng = floatval($g->lng);
					unset($g);
				}

				$noGeo = 0;
				$steps = 15;

				$index = (int)(abs( is_float($city->lat) ? $city->lat : (float)$city->lat ) / 10);
				if ($index > 0) {
					$startLng = 0.2 / $DistanceScale[$index][0]; // 0.1 miles is about 520ft
					$startLat = 0.2 / $DistanceScale[$index][1]; // pretty constant of about 68miles
					$deltaLng = $startLng / $steps; // increase radius each step by
					$deltaLat = $startLat / $steps; // increase radius each step by

					foreach($list as &$listing) {
						if ($listing->lat == -1) {
							if ($city->lat != -1) {
								$rad = (360/$steps) * (M_PI/180) * $noGeo;
								$lng = cos( $rad ) * ($startLng + ($deltaLng * $noGeo));
								$lat = sin( $rad ) * ($startLat + ($deltaLat * $noGeo));
								$noGeo++;
								$listing->lat = $lat + $city->lat;
								$listing->lng = $lng + $city->lng;
								$listing->isRadial = 1;
							}
						}
						unset($listing);
					}
				}

				$sql = 'SELECT b.id, b.tag, b.type, a.listing_id, a.tag_id FROM `'.$Listings->getTableName('listings-tags').'` AS a ';
				$sql.= 'INNER JOIN '.$Listings->getTableName('tags').' AS b ON a.tag_id = b.id ';
				$sql.= 'WHERE a.listing_id IN ('.implode(',', $ids).')';
				$tags = $Listings->rawQuery($sql);
				$this->log("getListingsInCities - sql:$sql");

				if (!empty($tags)) foreach($tags as $tag) {
					if ( !isset($list[$tag->listing_id]->tags))
						$list[$tag->listing_id]->tags = [];
					if (!empty($tag->tag_id) &&
						$tag->type == 0)
						$list[$tag->listing_id]->tags[$tag->id] = $tag;
					unset($tag);
				}
				unset($tags);

				$city->listings = $list;
			}
			unset($city);
		}
		return $cities;
	}

	protected function getCitiesByIds($ids) {
		$Cities = $this->getClass('Cities');
		$cities = $Cities->get((object)['where'=>['id'=>$ids]]);

		$x = [];
		if (!empty($cities)) foreach($cities as $city) {
			$city->id = intval($city->id);
			$x[$city->id] = $city;
			$x[$city->id]->locationStr = $city->city.", ".$city->state;
			unset($city);
		}
		unset($cities);
		$cities = $x;

		$sql = 'SELECT b.id, b.tag, b.type, a.score, a.city_id, a.tag_id FROM `'.$Cities->getTableName('cities-tags').'` AS a ';
		$sql .= 'INNER JOIN '.$Cities->getTableName('tags').' AS b ON b.id = a.tag_id ';
		$sql .= 'WHERE a.city_id in ('.implode(',',$ids).')';

		$tags = $this->getClass('CitiesTags')->rawQuery($sql);
		foreach($tags as $tag) {
			if ( !isset($cities[$tag->city_id]->tags) )
				$cities[$tag->city_id]->tags = [];
			$tag->score = !empty($tag->score) ? ($tag->score == -1 ? 10 : $tag->score) : 1;
			if (!empty($tag->tag_id))
				$cities[$tag->city_id]->tags[$tag->id] = $tag;
			unset($tag);
		}
		unset($ids, $tags);

		return $cities;
	}

	protected function getBoundedCities($bounds) {
		$Cities = $this->getClass('Cities');
		$cities = $Cities->get((object)['between'=>['lng'=> [$bounds->west,
														     $bounds->east]],
									   'between2'=>['lat'=>[$bounds->south,
									  					    $bounds->north]]]);
		$x = [];
		$ids = [];
		if (!empty($cities)) foreach($cities as $city) {
			$x[$city->id] = $city;
			$x[$city->id]->locationStr = $city->city.", ".$city->state;
			$ids[] = $city->id;
			unset($city);
		}
		else
			return null;

		unset($cities);
		$cities = $x;

		$sql = 'SELECT b.id, b.tag, b.type, a.score, a.city_id, a.tag_id FROM `'.$Cities->getTableName('cities-tags').'` AS a ';
		$sql .= 'INNER JOIN '.$Cities->getTableName('tags').' AS b ON b.id = a.tag_id ';
		$sql .= 'WHERE a.city_id in ('.implode(',',$ids).')';

		$tags = $this->getClass('CitiesTags')->rawQuery($sql);
		foreach($tags as $tag) {
			if ( !isset($cities[$tag->city_id]->tags) )
				$cities[$tag->city_id]->tags = [];
			$tag->score = !empty($tag->score) ? ($tag->score == -1 ? 10 : $tag->score) : 1;
			if (!empty($tag->tag_id))
				$cities[$tag->city_id]->tags[$tag->id] = $tag;
			unset($tag);
		}
		unset($ids, $tags);

		return $cities;
	}

	protected function getBoundedListings($bounds) {
		$ListingsGeoinfo = $this->getClass('ListingsGeoinfo');
		$geoInfo = $ListingsGeoinfo->get((object)['between'=>['lng'=> [$bounds->west,
														     		   $bounds->east]],
											      'between2'=>['lat'=>[$bounds->south,
											  					       $bounds->north]]]);
		$this->log("getBoundedListings - geoInfo:".!empty($geoInfo) ? print_r($geoInfo, true) : 'empty');

		$x = [];
		$ids = [];
		if (!empty($geoInfo)) {
			foreach($geoInfo as $geo) {
				$x[$geo->listing_id] = $geo;
				$ids[] = $geo->listing_id;
				unset($geo);
			}
		}
		else
			return $this->getBoundedCities($bounds);

		unset($geoInfo);
		$geoInfo = $x;

		$Listings = $this->getClass('Listings', 1);
		$listings = $Listings->get((object)['where'=>['id'=>$ids],
											'or'=>['active'=>[1,2,3,4,6]],
										    'what'=>['id','street_address','images','first_image','about','beds','baths','active','city_id']]);

		if (!empty($listings)) {
			$ids = [];
			foreach($listings as $l)
				$ids[] = $l->id;
			$sql = 'SELECT b.id, b.tag, b.type, a.listing_id, a.tag_id FROM `'.$Listings->getTableName('listings-tags').'` AS a ';
			$sql.= 'INNER JOIN '.$Listings->getTableName('tags').' AS b ON a.tag_id = b.id ';
			$sql.= 'WHERE a.listing_id IN ('.implode(',', $ids).')';
			$tags = $Listings->rawQuery($sql);
		}

		$ids = [];
		$x = [];
		if (!empty($listings)) {
			foreach($listings as $l) {
				if (!in_array($l->city_id, $ids))
					$ids[] = $l->city_id;
				$l->image = $this->getFirstImage($l);
				unset($l->images, $l->first_image);
				$x[$l->id] = $l;
				$x[$l->id]->lat = $geoInfo[$l->id]->lat; 
				$x[$l->id]->lng = $geoInfo[$l->id]->lng; 
				unset($l);
			}
			$listings = $x;
		}
		else
			return $this->getBoundedCities($bounds);

		$this->log("getBoundedListings - list of city ids:".print_r($ids, true));

		if (!empty($tags)) foreach($tags as $tag) {
			if ( !isset($listings[$tag->listing_id]) ) {
				$this->log("getBoundedListings - missing listing_id:$tag->listing_id in listings array");
				continue;
			}
			if ( !isset($listings[$tag->listing_id]->tags))
				$listings[$tag->listing_id]->tags = [];
			if (!empty($tag->tag_id) &&
				$tag->type == 0)
				$listings[$tag->listing_id]->tags[$tag->id] = $tag;
			unset($tag);
		}
		unset($tags);

		$Cities = $this->getClass('Cities');
		$cities = $Cities->get((object)['where'=>['id'=>$ids]]);
		$x = [];
		foreach($cities as $city) {
			$city->locationStr = $city->city.', '.$city->state;
			$city->listings = [];
			foreach($listings as $l) {
				if ($l->city_id == $city->id)
					$city->listings[$l->id] = $l;
				unset($l);
			}
			$x[$city->id] = $city;
			unset($city);
		}
		$cities = $x;

		$sql = 'SELECT b.id, b.tag, b.type, a.score, a.city_id, a.tag_id FROM `'.$Cities->getTableName('cities-tags').'` AS a ';
		$sql .= 'INNER JOIN '.$Cities->getTableName('tags').' AS b ON b.id = a.tag_id ';
		$sql .= 'WHERE a.city_id in ('.implode(',',$ids).')';

		$tags = $this->getClass('CitiesTags')->rawQuery($sql);
		foreach($tags as $tag) {
			if ( !isset($cities[$tag->city_id]->tags) )
				$cities[$tag->city_id]->tags = [];
			$tag->score = !empty($tag->score) ? ($tag->score == -1 ? 10 : $tag->score) : 1;
			if (!empty($tag->tag_id))
				$cities[$tag->city_id]->tags[$tag->id] = $tag;
			unset($tag);
		}
		unset($ids, $tags, $x);

		return $cities;
	}

	protected function applyToTagsSpace($tags, $bounds, $type, $query) {
		$tagSpaces = $this->getClass('TagsSpace', 1)->get((object)['lessthanequal'=>['north'=>$bounds->north],
															    	'greaterthanequal'=>['south'=>$bounds->south],
															    	'lessthanequal2'=>['west'=>$bounds->west],
															    	'greaterthanequal2'=>['east'=>$bounds->east],
															        'where'=>['type'=>$type]]); // specify types
		if (!empty($tagSpaces)) {
			$incoming = array_keys($tags);
			foreach($tagSpaces as $ts) {
				$metas = [];
				$needUpdate = false;
				if (!empty($ts->meta)) {
					foreach($ts->meta as $meta) {
						if ($meta->action == TAG_SPACE_LIST) {
							$tsList = (array)$meta->tags;
							$existing = array_keys($tsList);
							foreach($incoming as $id)
								if (!in_array($id, $existing))
									$tsList[$id] = $tags[$id];									

							if(count($tsList) != count($meta->tags)) {
								$meta->tags = (array)$tsList;
								$needUpdate = true;
							}
						}
						$metas[] = $meta;
						unset($meta);
					}
				}
				if ($needUpdate) {
					$result = $this->getClass('TagsSpace')->set([(object)['where'=>['id'=>$ts->id],
																		  'fields'=>['meta'=>$metas]]]);
					$this->log("applyToTagsSpace - for $query updating TagsSpace id:$ts->id tags to:".print_r($tsList, true).' was:'.($result ? 'success' : 'failure'));
				}
				else
					$this->log("applyToTagsSpace - for $query no change for TagsSpace id:$ts->id");
				unset($metas, $ts);
			}
		}
		else {
			$meta = new \stdClass();
			$meta->action = TAG_SPACE_LIST;
			$meta->tags = $tags;
			$result = $this->getClass('TagsSpace')->add(['north'=>$bounds->north,
												   		 'south'=>$bounds->south,
												   		 'west'=>$bounds->west,
												   		 'east'=>$bounds->east,
												   		 'type'=>$type,
												   		 'meta'=>[$meta]]);
			$this->log("applyToTagsSpace - for $query add TagsSpace id:$result to:".print_r($meta->tags, true).' was:'.($result ? 'success' : 'failure'));
		}
	}

	private function haveActiveParallelProcessing() {
		$Options = $this->getClass('Options');
		$x = $Options->get((object)['or'=>['opt'=>['activePP','activeIP','activeIPCustom']]]);
		if (empty($x))
			$x = $Options->get((object)['where'=>['opt'=>'activeParseToDb']]);
		return !empty($x);
	}

	private function getFirstImage($listing) {
		global $haveActiveParallelProcessing;
		$haveFile = false;
		if ( isset($listing->first_image) &&
			($haveFile = !empty($listing->first_image)) )
			return $listing->first_image->file;
		else {
			if (!empty($listing->images)) {
				foreach($listing->images as $i=>$img) {
					if (!empty($img->file)) { // can be a discard
						$isHttp = substr($img->file, 0, 4 ) == 'http';
						if ($isHttp) {
							return $img->file;
						}
						elseif ($haveActiveParallelProcessing) {
							if (isset($img->url) &&
								!empty($img->url)) {
								return $img->url;
							}
						}
						elseif (!file_exists($this->defaults['imgPath'].'/'.$img->file)) {
							if (isset($img->url)) {
								return $img->url; // revert to url...
							}
						}
						else {
							return $img->file;
						}
					}
					unset($img);
				}
			}
			return null;
		}					
	}

	protected function makeTagSet(&$cities, $city_id, $city, $lat, $lng) {
		// global $amTags;
		$set = [];
		// foreach($amTags as $tag_id=>$tag) {
		// 	$tag->agents = [];
		// 	$set[$tag_id] = $tag;
		// 	unset($tag);
		// }
		$cities[$city_id] = (object)['locationStr'=>$city,
									 'city_id'=>$city_id,
									 'lat'=>$lat,
									 'lng'=>$lng,
									 'tags'=>$set];
		unset($set);
	}

	protected function buildReservationCollation(&$reservationCollation, &$reservations) {
		if (!empty($reservations)) foreach($reservations as &$res) {
			if (!empty($res->meta)) {
				$res->meta = json_decode($res->meta);
				$metas = [];
				$needUpdate = false;
				$lastBadLocation = -1;
				$lastFixedLocation = -1;
				foreach($res->meta as $meta) {
					if ($meta->action == ORDER_AGENT_MATCH) {
						if (!isset($res->city))
							$res->city = [];
						foreach($meta->item as $item) {
							// verify correct location value
							if ($item->location == $lastBadLocation) {
								$item->location = $lastFixedLocation;
								$this->getClass('Sellers', 1)->log("Sales - reconcile bad location:$lastBadLocation to location:$item->location, $item->locationStr on $item->specialtyStr for reservationId:$res->id");
							}
							else {
								$parts = explode(',',removeslashes($item->locationStr));
								$city = $this->getClass('Cities')->get((object)['where'=>['id'=>$item->location]]);
								if (empty($city) ||
									$city[0]->city != $parts[0]) {// uh oh
									$lastBadLocation = $item->location;
									$city = $this->getClass('Cities')->get((object)['like'=>['city'=>$parts[0]],
																  					'like2'=>['state'=>trim($parts[1])]]);
									if (!empty($city)) {
										$item->location = $city[0]->id;
										$lastFixedLocation = $item->location;
										$needUpdate = true;
										$this->getClass('Sellers', 1)->log("Sales - reconcile bad location:$lastBadLocation to location:$item->location, $item->locationStr on $item->specialtyStr for reservationId:$res->id");
									}
									else {
										$this->getClass('Sellers', 1)->log("Sales - could not reconcile location:$item->location, $item->locationStr - skipping $item->specialtyStr for reservationId:$res->id!");
										continue;
									}
								}
								unset($parts, $city);
							}
							if (!isset($res->city[$item->location]))
								$res->city[$item->location] = (object)["locationStr"=>removeslashes($item->locationStr),
																	   "specialty"=>[]];
							$res->city[$item->location]->specialty[] = (object)["specialty"=>$item->specialty,
																			    "specialtyStr"=>$item->specialtyStr];
							// now build up the city based collation
							if (!isset($reservationCollation[$item->location]))		
								$this->makeTagSet($reservationCollation, $item->location, $item->locationStr, -1, -1);						
						 	if (!isset($reservationCollation[$item->location]->tags[$item->specialty]))	
						 		$reservationCollation[$item->location]->tags[$item->specialty] = (object)["specialtyStr"=>$item->specialtyStr,
						 																				  "reservations"=>[],
						 																				  "agents"=>[]];	

						 	$reservationCollation[$item->location]->tags[$item->specialty]->reservations[] = $res->id;
						}
						$metas[] = $meta;
					}
					else
						$metas[] = $meta;
					unset($meta);
				}
				if ($needUpdate) 
					$this->getClass('Reservations')->set([(object)['where'=>['id'=>$res->id],
												 				   'fields'=>['meta'=>$metas]]]);
				unset($metas);
			}
		}
	}

	protected function buildReservationCollationWithAgents(&$reservationCollation, &$activeAgents) {
		if (!empty($activeAgents)) {
			foreach($activeAgents as $agent_id=>&$agent) {
				$agent->tags = explode('|', $agent->tags);
				$new_tags = [];
				$city_tags = [];
				foreach($agent->tags as $tag_info) {
					if (!empty($tag_info)) {
						$tag_info = explode(':', $tag_info);
						$tag_id = intval($tag_info[0]);
						$tag = $tag_info[1];
						$city_id = intval($tag_info[3]);
						$city = $tag_info[4];
						$state = $tag_info[5];
						$lat = (isset($tag_info[6]) && !empty($tag_info[6]) ? floatval($tag_info[6]) : -1);
						$lng = (isset($tag_info[7]) && !empty($tag_info[7]) ? floatval($tag_info[7]) : -1);
						$score = ($tag_info[2] == '-1' ? 10 : intval($tag_info[2]));
						if ( !array_key_exists($tag_id, $new_tags) )
							$new_tags[$tag_id] = (object)[	'tag_id'=>$tag_id,
															'tag'=>$tag,
															'score'=>$score,
															'loc'=>[]];
						$loc = (object)['city_id'=>$city_id,
										'city'=>$city.', '.$state,
										'lat'=>$lat,
										'lng'=>$lng
										];
						$tag = (object)['tag_id'=>$tag_id,
										'tag'=>$tag,
										'score'=>$score];
						$new_tags[$tag_id]->loc[] = $loc;

						if ( !array_key_exists($city_id, $city_tags) )
							$city_tags[$city_id] = (object)['city'=>$city.', '.$state,
															'tags'=>[]];
						$city_tags[$city_id]->tags[] = $tag;
						unset($tag);
					}
				}
				unset($agent->tags);
				$agent->tags = $new_tags;
				$agent->city_tags = $city_tags;

				foreach($agent->tags as $tag_id=>$tagLoc) {
					foreach($tagLoc->loc as $loc) {
						if ( !array_key_exists($loc->city_id, $reservationCollation) )
							$this->makeTagSet($reservationCollation, $loc->city_id, $loc->city, $loc->lat, $loc->lng);
						if ( !isset($reservationCollation[$loc->city_id]->tags[$tag_id]) )
							$reservationCollation[$loc->city_id]->tags[$tag_id] = (object)["specialtyStr"=>$tagLoc->tag,
		 																				  "reservations"=>[],
		 																				  "agents"=>[]];	
						$reservationCollation[$loc->city_id]->tags[$tag_id]->agents[] = $agent;				
					}
				}
				unset($new_tags, $city_tags);
			}
		}
	}

	protected function getMapCenter(&$mapCenter, &$reservationCollation, $Sellers) {
		$cityList = $this->getClass('Cities')->get((object)['where'=>['id'=>array_keys($reservationCollation)]]);
		$minPos = ['lng'=>181,
				   'lat'=>181];
		$maxPos = ['lng'=>-181,
				   'lat'=>-181];

		foreach($cityList as $city) {
			$reservationCollation[$city->id]->lng = $city->lng;
			$reservationCollation[$city->id]->lat = $city->lat;

			if ($city->lng < $minPos['lng'])
				$minPos['lng'] = $city->lng;
			if ($city->lat < $minPos['lat'])
				$minPos['lat'] = $city->lat;
			if ($city->lng > $maxPos['lng'])
				$maxPos['lng'] = $city->lng;
			if ($city->lat > $maxPos['lat'])
				$maxPos['lat'] = $city->lat;
		}

		$mapCenter = ['lng'=>(($minPos['lng']+$maxPos['lng'])/2),
					  'lat'=>(($minPos['lat']+$maxPos['lat'])/2)];
		$Sellers->log("minPos:".print_r($minPos, true).", maxPos:".print_r($maxPos, true).", mapCenter:".print_r($mapCenter, true));

	}
}
new AJAX_sales();
