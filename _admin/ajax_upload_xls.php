<?php
namespace AH;
require_once(__DIR__.'/../_classes/Utility.class.php');
class uploadXLS {
	public function __construct($query = null, $files = null){
		$out = null;
		if (!empty($query)) switch($query){
			/* disabled */
			// case 'city-tag': // adds tags to cities
			// 	$file = array_pop($files);
			// 	unset($files);

			// 	require_once(__DIR__.'/../_classes/Cities.class.php'); $Cities = new Cities();
			// 	require_once(__DIR__.'/../_classes/CitiesTags.class.php'); $CitiesTags = new CitiesTags();
			// 	require_once(__DIR__.'/../_classes/ExcelReader.class.php');

			// 	$Reader = new ExcelReader($file['tmp_name']);
			// 	$cities = [];
			// 	$tag_id_ref = [
					// 'small town' => 27,
					// 'metropolitan' => 16,
					// 'urban' => 29,
					// 'airport within 15 miles' => 104,
					// 'airport within 40 miles' => 142,
					// 'airport within 80 miles' => 105,
					// 'winter <40' => 33,
					// 'winter 40-70' => 32,
					// 'winter >70' => 34,
					// 'summer 71-85' => 115,
					// 'summer 60-70' => 114,
					// 'summer 86+' => 116,
					// 'ocean' => 19,
					// 'alpine' => 1,
					// 'countryside' => 4,
					// 'wine country' => 31,
					// 'lake' => 166,
					// 'city outskirts' => 159,
					// 'rural' => 24,
					// 'urban' => 29,
			// 	];
			// 	foreach ($Reader->read() as $i=>$row)
			// 		if ($row['B'] != 'City' && $row['W'] == 'x'){ // ignore header row
			// 			if ($row['B'] == 'Los Angeles (City)') $row['B'] = 'Los Angeles';
			// 			$row['C'] = strtoupper(trim($row['C']));
			// 			$row['B'] = str_replace('-',' ',strtolower(trim($row['B'])));
			// 			$row['B'] = str_replace('  ',' ',$row['B']);

			// 			$t = explode(' ', $row['B']);
			// 			$row['B'] = '';
			// 			$doNotUpperFirst = ['the','for','by','on'];
			// 			foreach ($t as $i=>$str){
			// 				if ($i > 0) $row['B'] .= ' ';
			// 				if ($i < 1 || !in_array($str, $doNotUpperFirst)) $str = ucfirst($str);
			// 				$row['B'] .= $str;
			// 			} unset($i, $t, $str);

			// 			$city = $Cities->get((object)['what' => ['id'], 'where' => [ 'city' => $row['B'], 'state' => $row['C']] ])[0];
			// 			if ($city && is_array($city)) $city = $city['id'];
			// 			else if ($city && is_object($city)) $city = $city->id;

			// 			if ($city) $cities[] = (object)[
			// 				'city_id' => $city,
			// 				'city' => $row['B'],
			// 				'state' => $row['C'],
			// 				'tags' => (object)[
			// 					42 => $row['D'], // golf
			// 					123 => $row['E'], // boating
			// 					163 => $row['F'], // beach
			// 					18 => $row['G'], // nightlife
			// 					164 => $row['H'], // wine culture
			// 					6 => $row['I'], // equestrian
			// 					66 => $row['J'], // culinary
			// 					49 => $row['K'], // music
			// 					145 => $row['L'], // surfing
			// 					72 => $row['M'], // hunting
			// 					143 => $row['N'], // skiing
			// 					25 => $row['O'], // shopping
			// 					40 => $row['P'], // fishing
			// 					165 => $row['Q'],// casinos
			// 					'townsize' => $row['R'],
			// 					'airport' => $row['S'],
			// 					'winter' => $row['T'],
			// 					'summer' => $row['U'],
			// 					'region' => $row['V'],
			// 				]
			// 			];
			// 		}

			// 	$results = [];

			// 	foreach ($cities as &$city){
			// 		foreach ($city->tags as $tag_id=>&$tag_score) if ($tag_score){
			// 			if (is_numeric($tag_id)){
			// 				$tag = $CitiesTags->get((object)[ 'what' => ['score'], 'where' => [ 'city_id' => $city->city_id, 'tag_id' => $tag_id ] ]);
			// 				if (!empty($tag)){ // row exists with city_id && tag_id
			// 					$tag = array_pop($tag);
			// 					if (!empty($tag->score) && $tag->score != $tag_score){
			// 						// $results[] = (object)['set-city' => (object)[ 'where'=>[ 'city_id' => $city->city_id, 'tag_id' => $tag_id ], 'fields'=>[ 'score'=>$tag_score ] ]];
			// 						// $CitiesTags->set([(object)[ 'where'=>[ 'city_id' => $city->city_id, 'tag_id' => $tag_id ], 'fields'=>[ 'score'=>$tag_score ] ]]);
			// 					}
			// 				} else { // add row
			// 					$results[] = $CitiesTags->add((object)['score'=>$tag_score, 'city_id' => $city->city_id, 'tag_id' => $tag_id ]);
			// 					// $results[] = (object)['add' => (object)[ 'fields'=>[ 'score'=>$tag_score, 'city_id' => $city->city_id, 'tag_id' => $tag_id ] ]];
			// 				}
			// 			} else if (array_key_exists(strtolower($tag_score), $tag_id_ref)){
			// 				$tag_id = $tag_id_ref[strtolower($tag_score)];
			// 				$tag = $CitiesTags->get((object)[ 'where'=>[ 'score'=>-1, 'city_id' => $city->city_id, 'tag_id' => $tag_id ] ]);
			// 				if (!$tag) // doesn't exist, add it
			// 					// $results[] = (object)['add' => (object)[ 'fields'=>[ 'score'=>-1, 'city_id' => $city->city_id, 'tag_id' => $tag_id ] ]];
			// 					$results[] = $CitiesTags->add((object)[ 'score'=>-1, 'city_id' => $city->city_id, 'tag_id' => $tag_id ]);
			// 			}
			// 		}
			// 	}

			// 	$out = new Out('OK', $results);
			// 	break;
			default: $out = new Out(0, 'Invalid query: '.$query); break;
		}
		if (empty($out)) $out = new Out(0, 'Query was not set.');
		echo json_encode( $out );
	}
}

if (!empty($_FILES))
	new uploadXLS( (!empty($_SERVER['HTTP_XLSTYPE']) ? $_SERVER['HTTP_XLSTYPE'] : null), (!empty($_FILES) ? $_FILES : null) );
else echo new Out(0, 'No files uploaded.');