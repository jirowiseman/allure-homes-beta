<?php
namespace AH;
// require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
// require_once(__DIR__.'../../../../../wp-config.php');
// require_once(__DIR__.'../../../../../wp-load.php');
// require_once(__DIR__.'../../../../../wp-includes/wp-db.php');
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

class AJAX_Analytics extends Controller {
	// comes from Controller now
	// global $timezone_adjust;
	// protected $timezone_adjust = $timezone_adjust;
	private $logIt = true;

	public function __construct(){
		$in = parent::__construct();
		set_time_limit(0);
		try {
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-analytics.log') : null;
			if (empty($in->query)) {
				// $msg = __FILE__.':('.__LINE__.') - invalid request sent from ip:'.$this->userIP().", browser: ".print_r(getBrowser(), true).", GET:".print_r($_GET, true).", POST:".print_r($_POST, true);
				$msg = 'invalid request sent from ip:'.$this->userIP().", browser: ".print_r(getBrowser(), true).", GET:".print_r($_GET, true).", POST:".print_r($_POST, true);
				$this->log($msg);
				$out = new Out('OK', $msg); //throw new \Exception (__FILE__.':('.__LINE__.') - no query sent from ip:'.$this->userIP().", broswer: ".print_r(getBrowser(), true));
				echo json_encode($out);
				die;
				// if (!isset($in->query))
				// 	$in->query = "bust";
			}
			// $this->log("ajax-analytics doing $in->query");
			switch ($in->query){
				case 'visitAll':
				case 'visitPortal':
				case 'visitPortalOne':
				case 'visitQuiz':
				case 'visitQuizResults':
					$table = '';
					switch($in->query) {
						case 'visitAll': $table = 'visits_all'; break;
						case 'visitPortal': $table = 'visits_portal_landing'; break;
						case 'visitPortalOne': $table = 'visits_portal_landing_one'; break;
						case 'visitQuiz': $table = 'visits_quiz'; break;
						case 'visitQuizResults': $table = 'visits_quiz_results'; break;
					}

					$sql = 'SELECT * FROM '.$table." WHERE 1";
					$this->log("$in->query - sql:$sql");

					$result = $this->getClass('Analytics')->rawQuery($sql);
					$retval = !empty($result);

					if ($retval) {
						// $row = (array)$result[0];
						$x = array_keys((array)$result[0]);
						$keys = [];
						$i = 0;
						foreach($x as $key)
							$keys[$i++] = $key;

						$result = (object)['results'=>$result,
											'keys'=>$keys];
					}
					$out = new Out($retval ? 'OK' : 'fail', $result);
					break;
	
				case 'initialEntry':
					$in->query = 'pageInitialEntrySelectSql'; // change to point to correct .dat file.  It's initalEntry so the button on FE looks more in synch with other ones
				case 'visitAllSql':
				case 'visitAllSqlWithSessionId':
				case 'pageInitialEntrySql':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (!isset($in->data['from'])) throw new \Exception('No from sent.');
					if (!isset($in->data['to'])) throw new \Exception('No to sent.');
					$sql = file_get_contents(__DIR__.'/_data/'.$in->query.'.dat');
					// $sql = file_get_contents(__DIR__.'/_data/test.dat');
					if (empty($sql))
						throw new \Exception('No sql found.');

					$sql = trim(preg_replace('/\s+/', ' ', $sql));

					$sqlList = json_decode($sql);
					$result = null;
					$retval = $this->runSqlQueryList($sqlList, $in->data['from'], $in->data['to'], $result);
					if ($retval &&
						!empty($result)) {
						// $row = (array)$result[0];
						$x = array_keys((array)$result[0]);
						$keys = [];
						$i = 0;
						foreach($x as $key)
							$keys[$i++] = $key;

						$result = (object)['results'=>$result,
											'keys'=>$keys];
					}
					$out = new Out($retval ? 'OK' : 'fail', $retval ? $result : (empty($result) ? "None found" : $result));
					break;

				case 'portalUsage':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (!isset($in->data['from'])) throw new \Exception('No from sent.');
					if (!isset($in->data['to'])) throw new \Exception('No to sent.');
					$sql = file_get_contents(__DIR__.'/_data/'.$in->query.'.dat');
					// $sql = file_get_contents(__DIR__.'/_data/test.dat');
					if (empty($sql))
						throw new \Exception('No sql found.');

					$sql = trim(preg_replace('/\s+/', ' ', $sql));

					$sqlList = json_decode($sql);
					$result = null;
					$retval = $this->runSqlQueryList($sqlList, $in->data['from'], $in->data['to'], $result);
					if ($retval &&
						!empty($result)) {
						$this->extractPortalUsageData($result);
						$x = array_keys((array)$result[0]);
						$keys = [];
						$i = 0;
						foreach($x as $key)
							$keys[$i++] = $key;

						$result = (object)['results'=>$result,
											'keys'=>$keys];
					}
					$out = new Out($retval ? 'OK' : 'fail', $retval ? $result : (empty($result) ? "None found" : $result));
					break;

				case 'general-SQL':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (!isset($in->data['events'])) throw new \Exception('No events sent.');
					if (!isset($in->data['origins'])) throw new \Exception('No origins sent.');
					if (!isset($in->data['whats'])) throw new \Exception('No whats sent.');
					if (!isset($in->data['retrieve'])) throw new \Exception('No retrieve sent.');
					if (!isset($in->data['from'])) throw new \Exception('No from sent.');
					if (!isset($in->data['to'])) throw new \Exception('No to sent.');

					$events = $in->data['events'];
					$origins = $in->data['origins'];
					$whats = $in->data['whats'];
					$retrieve = $in->data['retrieve'];
					$from = $in->data['from'];
					$to = $in->data['to'];

					if (empty($events) &&
						empty($origins) &&
						empty($whats))
						throw new \Exception('Nothing to query.');


					$retrieve = empty($retrieve) ? '*' : $retrieve;
					$analytics = $this->getClass('Analytics');
					$sql = 'SELECT '.$retrieve.' FROM '.$analytics->getTableName().' WHERE ';
					$sql .= "added BETWEEN '$from' AND '$to' ";
					if (!empty($events))
						$sql .= "AND type IN ($events) ";
					if (!empty($origins))
						$sql .= "AND origin IN ('$origins') ";
					if (!empty($whats))
						$sql .= "AND what IN ('$whats') ";

					$this->log("general-SQL - sql:$sql");

					$result = $analytics->rawQuery($sql);
					$retval = !empty($result);

					if ($retval) {
						// $row = (array)$result[0];
						$x = array_keys((array)$result[0]);
						$keys = [];
						$i = 0;
						foreach($x as $key)
							$keys[$i++] = $key;

						$result = (object)['results'=>$result,
											'keys'=>$keys];
					}
					$out = new Out($retval ? 'OK' : 'fail', $retval ? $result : (empty($result) ? "None found" : $result));
					break;

				case 'record-analytic':
					if (empty($in->data)) throw new \Exception('No data sent.');
					if (empty($in->data['type'])) throw new \Exception('No type sent.');
					if (empty($in->data['origin'])) throw new \Exception('No origin sent.');
					// if (empty($in->data['session_id'])) throw new \Exception("No session_id sent for type:.{$in->data['type']}, origin:{$in->data['origin']}, what:".(isset($in->data['what']) ? $in->data['what'] : ''.", value_str:".(isset($in->data['value_str']) ? $in->data['value_str'] : '').", value_int:".(isset($in->data['value_int']) ? intval($in->data['value_int']) : 0)));

					$this->exitNow("record-analytic is running");

					$session_id = isset($in->data['session_id']) ? intval($in->data['session_id']) : 0;
					$referer = isset($in->data['referer']) ? $in->data['referer'] : '';
					$what = isset($in->data['what']) ? $in->data['what'] : '';
					$value_str = isset($in->data['value_str']) ? $in->data['value_str'] : '';
					$value_int = isset($in->data['value_int']) ? intval($in->data['value_int']) : 0;
					$date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
					$core = "type:{$in->data['type']}, origin:{$in->data['origin']}, referer:$referer, session_id:$session_id, what:$what, value_str:$value_str, value_int:$value_int, date:$date";
					$this->log("record-analytic - $core");
					$browser = getBrowser();
					$ip = $this->userIP();

					$x = $this->getClass('Analytics', 1)->add(['type'=>$in->data['type'],
															'origin'=>$in->data['origin'],
															'session_id'=>$session_id,
															'referer'=>$referer,
															'what'=>$what,
															'value_str'=>$value_str,
															'value_int'=>$value_int,
															'added'=>$date,
															'browser'=>$browser['shortname'],
															'connection'=>$browser,
															'ip'=>$ip]);

					if ($what == 'portalUserReachOut') 
						$this->getClass('Email')->sendMail(SUPPORT_EMAIL, "PortalAgent Activity", $value_str, "Portal Agent reach out", "");

					$ignoreIPs = ['63.249.70.15','107.202.64.174','50.131.133.194','96.86.188.225','127.0.0.1','0','::1'];
					if ( intval($in->data['type']) == ANALYTICS_TYPE_PAGE_ENTRY_INITIAL &&
						 !empty($ip) &&
						 !in_array($ip, $ignoreIPs)) {
						$city = $this->getClass('IpList')->getCity($ip);
						if ($city)
							$this->log("PAGE_ENTRY_INITIAL to {$in->data['origin']} from {$city['city']}, {$city['state']}, ip:$ip, session_id:$session_id");
					}

					$this->log(!empty($x) ? "Analytics recorded $core" : "Failed to add $core");
					// $out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Analytics for $core" : "Failed to add $core");
					die;
			}
			if ($out) echo json_encode($out);
			else echo json_encode(new Out('fail', "No handler for $in->query"));
		}
		catch (\Exception $e) { parseException($e, true); die(); }
	}

	protected function runSqlQueryList($sqlList, $from, $to, &$result) {
		$ignoreIPs = '63.249.70.15,107.202.64.174,67.169.51.50, 50.131.133.194,96.86.188.225';
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'ignoreIPs']]);
		if (!empty($opt))
			$ignoreIPs = $opt[0]->value;

		$timeSpentInQuiz = '00:06:00';
		$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'timeSpentInQuiz']]);
		if (!empty($opt))
			$timeSpentInQuiz = $opt[0]->value;

		global $wpdb;
		$prefix = $wpdb->prefix;

		$retval = true;
		foreach($sqlList as $sql) {
			$sql = str_replace("%start%", $from, $sql);
			$sql = str_replace("%end%", $to, $sql);
			$sql = str_replace("%ignoreIPS%", $ignoreIPs, $sql);
			$sql = str_replace("%timeSpentInQuiz%", $timeSpentInQuiz, $sql);
			$sql = str_replace("%prefix%", $prefix, $sql);

			$this->log("runSqlQueryList - sql:$sql");

			$result = $this->getClass('Analytics')->rawQuery($sql);
			if (empty($result)) {
				$result = "Failed query:$sql";
				$this->log($result);
				$retval = false;
				break;
			}
			usleep(200);
		}
		return $retval;
	}

	protected function extractPortalUsageData (&$input) {
		foreach($input as &$user) {
			$summary = null;
			$user->meta = !empty($user->meta) ? json_decode($user->meta) : [];
			foreach($user->meta as $meta) {
				if ($meta->action == DAILY_SUMMARY)
					$summary = $meta;
				unset($meta);
				if ($summary)
					break;
			}
			// $this->log("extractPortalUsageData - user:$user->id, summary:".(!empty($summary) ? 'got one' : 'no summary').", user:".print_r($user, true));
			unset($user->meta); // all gone, bye bye...
			$user->quiz_id = 0;
			$user->price = '';
			$user->city_tags = '';
			$user->listing_tags = '';
			$user->location = '';
			$user->number_visits = 0;
			$user->number_view_listings = 0;
			$lastDate = '';
			$quizDate = '';
			$quizIp = '';
			if ($summary) {
				foreach($summary->list as $daysActivity) {
					$lastDate = $daysActivity->date;
					foreach($daysActivity->list as $activity) {
						switch($activity->action) {
							case 'Quiz': 
								$user->quiz_id = $activity->what; 
								$quizDate = $activity->date;
								$quizIp = $activity->ip;
								break;
							case 'Page': $user->number_visits++; break;
							case 'Listing': $user->number_view_listings++; break;

						}
					}
				}

				if ($user->quiz_id) {
					if ($user->quiz_id == -1) {
						// ugghh.. need to go find it now..
						$sql = 'SELECT quiz_id FROM '.$this->getClass('Sessions')->getTableName().' WHERE ';
						$sql.= 'user_id = '.$user->wp_user_id.' AND ';
						$sql.= 'ip_dot = '.$quizIp.' AND ';
						$sql.= "updated >= SUBTIME('$quizDate','00:01:00')";
						$ses = $this->getClass('Sessions')->rawQuery($sql);
						if (!empty($ses)) {
							$user->quiz_id = $ses[0]->quiz_id;
							$this->log("extractPortalUsageData - found quizId:$user->quiz_id for portal user:$user->id with WPUserId:".(!empty($user->wp_user_id) ? $user->wp_user_id : '0')." on ip:$quizIp, $quizDate");
						}
						else {
							$user->quiz_id = 0;
							$this->log("extractPortalUsageData -failed to find quiz_id for portal user:$user->id using sql:$sql");
						}
					}
				}

				if ($user->quiz_id) { // ok, we have something
					$this->getQuizData($user);
				}
			}

			unset($user);
		}
	}

	protected function getQuizData(&$user) {
		$quiz = $this->getClass('QuizActivity')->getDetails($user->quiz_id);
		if ($quiz->status == 'OK') {
			if ( empty($quiz->data['location']) && empty($quiz->data['state']) )
				$user->location = "nationwide";
			elseif ( !empty($quiz->data['state']) ) 
				$user->location = "statewide:{$quiz->data['state']}";
			else 
				$user->location = $quiz->data['location'];
						 
			$user->price = "$".$quiz->data['price'][0].' to '.($quiz->data['price'][1] == -1 ? "MAX" : '$'.$quiz->data['price'][1]);
			$this->getTags($user, $quiz->data['tags']);
		}
	}

	protected function getTags(&$user, $tags) {
		if (empty($tags))
			return;

		$tagList = [];
		foreach($tags as $tag)
			$tagList[] = $tag->id;

		$tagList = $this->getClass('Tags')->get((object)['where'=>['id'=>$tagList]]);
		foreach($tagList as $tag)
			switch($tag->type) {
				case 0: $user->listing_tags .= (!empty($user->listing_tags) ? ',' : '').$tag->tag; break;
				case 1: $user->city_tags .= (!empty($user->city_tags) ? ',' : '').$tag->tag; break;
			}

		unset($tagList);
	}
}

new AJAX_Analytics();
