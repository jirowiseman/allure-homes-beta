var parser;
jQuery(document).ready(function($){
parser = new function(){
	this.sites = {
		luxuryrealestate: {title:'Luxury Real Estate.com', url:'http://www.luxuryrealestate.com/residential/ajax_search?', options: 'area=US-CA&kind=Sale&price_min=1800000&sort=shared_desc', options_formatted: '>$1.8m, US-CA, sale',  pages:40}
	}
	this.menu = [
		{title:'Parse From Sites',page:'parse-summaries'},
		{title:'View Un-Detailed Listings',page:'view-summary-listings'},
		{title:'Parsed Listings',page:'view-parsed-listings'}
	];
	this.parsed = {};
	this.init = function(){
		$('div.wrap > h2').html('Parser <span class="page-sub-title"></span>');
		for (var i in this.menu) {
			$('#parser-nav').append('<li menu-page="'+this.menu[i].page+'" class="menu-item"><a href="javascript:parser.getPage('+"'"+parser.menu[i].page+"'"+');">'+this.menu[i].title+'</a></li>');
		}
		this.getPage('start');
	}
	this.getPage = function(thePage){
		if (this.currentPage == thePage) return false;
		this.currentPage = thePage;
		$('span.page-sub-title').fadeOut({duration:250, queue:false});
		$('#parser-page').fadeOut(250, function(){
			$('#parser-nav > li').each(function(){$(this).removeClass('selected')});
			$(this).empty();
			var p = parser;
			var onLoad = function(){};
			var h = '';
			switch(thePage){
				case 'start':
					h+= '<p>Select a menu item above to get started.</p>';
					break;
				case 'parse-summaries':
					h+= '<select id="parse-site">';
					for (var i in p.sites) h+= '<option value="'+i+'" selected>'+p.sites[i].title+' ('+p.sites[i].options_formatted+', pages: '+p.sites[i].pages+')</option>';
					h+= '</select><p><a id="parse-start" href="javascript:;">Start Parsing</a></p>';
					onLoad = function(){
						$('#parse-start').on('click',function(){ p.pullSummaryPages(p.sites[$('#parse-site').val()]); });
					}
					break;
				case 'view-summary-listings':
					p.getListings({onDone: function(d){
						p.listings = d;
						$('#listings thead').empty().append('<th><input type="checkbox" id="check-all" /></th><th>Thumb</th><th>Address / Price</th><th>Parse Site / Time</th>');
						var h = '';
						for (var i in d) {
							h+= '<tr listing-id="'+d[i].id+'" '+(i%2 === 1 ? ' class="alternate"' : '')+'>';
							h+= '<td class="listing-check"><input type="checkbox" /></td>';
							h+= '<td class="listing-thumbnail"><img src="'+d[i].thumb_url+'" /></td>';
							h+= '<td><a target="_blank" class="listing-address" href="'+d[i].url+'">'+d[i].address+'</a><br/><span class="listing-price">$'+d[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span></td>';
							h+= '<td class="listing-site"><strong>'+p.sites[d[i].site].title+'</strong><br/>'+d[i].added_obj+'</td>';
							h+= '</tr>';
						}
						$('#listings tbody').empty().append(h);
						$('#check-all').change(function(){
							if ( $(this).is(':checked') ) $('td input[type=checkbox]').each(function(){ $(this).prop('checked', true); });
							else $('td input[type=checkbox]').each(function(){ $(this).prop('checked', false); });
						});
					}});
					h+= '<p><a id="get-details" href="javascript:;">Get Details for Checked Listings</a></p><table id="listings" class="widefat"><thead></thead><tbody><tr><td colspan="5">Loading listings..</td></tr></tbody></table>';
					onLoad = function(){
						$('#get-details').on('click',function(){p.pullDetails()});
					}
					break;
				case 'view-raw-listings':
					h+= '<table class="widefat raw-listings"><thead><tr><th style="width:25px">ID</th><th># of Photos</th><th>Title/Address</th><th>Price/Beds/Baths</th><th>Realtor</th><th>Tags:</th></tr></thead><tbody><tr><td><strong>Loading...</strong></td></tbody></table>';
					onLoad = function(){
						// parser.DB({ 
						// 	query:'parse-raw-listings', 
						// 	onDone:function(d){
						// 		$('.raw-listings tbody').empty();
						// 		for (var i in d){
						// 			var l = d[i].data;
						// 			if (l){
						// 				// console.log(l.realtor_info);
						// 				var h = '<tr class="listing'+(i % 2 === 1 ? ' alternate' : '')+'" listing-id="'+l.id+'"><td>'+l.id+'</td><td>';
						// 				// h+='<ul class="thumbnails">';
						// 				if (l.images && l.images.length > 0) {
						// 					h+= l.images.length;
						// 					// for (var j in l.images){
						// 					// 	if (l.images[j].thumb && l.images[j].thumb != '') h+= '<li><a href="'+l.images[j].full+'" title="'+(l.images[j].caption==null?'':l.images[j].caption)+'"  target="_blank"><img src="'+l.images[j].thumb+'" alt="'+(l.images[j].caption==null?'':l.images[j].caption)+'" /></a></li>'
						// 					// }	
						// 				}
						// 				// h+= '</ul>';
						// 				h+= '</td><td><a href="'+l.url+'" target="_blank">'+(l.title == null ? '' : l.title+'<br/>')+(l.street_address == null ? '' : l.street_address+'<br/>')+(l.city == null ? '' : l.city+(l.state == null? '' : ', '))+(l.state == null ? '' : l.state+(l.zip == null? '' : ', '))+(l.zip == null ? '' : l.zip)+'</a></td>'+
						// 				'<td>$'+(l.price ? l.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0)+'<br/>Beds: '+(l.beds == null ? 0 : l.beds)+' / Baths: '+(l.baths == null ? 0 : l.baths)+'</td>'+
						// 				'<td>';
						// 				if (l.realtor_info) {
						// 					h+= '<span><strong>Company:</strong> '+(l.realtor_info.company != null && l.realtor_info.company != '' ? l.realtor_info.company : '(no company)')+'</span><br/><span><strong>Realtor:</strong> ';
						// 					if (l.realtor_info.realtor && l.realtor_info.realtor.length > 0) for (var j in l.realtor_info.realtor){
						// 						if (j > 0) h+= ', ';
						// 						h+= (l.realtor_info.realtor[j][1] ? '<a title="'+l.realtor_info.realtor[j][1]+'" href="mailto:'+l.realtor_info.realtor[j][1]+'">'+l.realtor_info.realtor[j][0]+'</a>' : l.realtor_info.realtor[j][0]);
						// 					} else h+= '(no realtor)';
						// 					h+= '</span>';
						// 				} else h+= '(no realtor info)';
						// 				h+= '</td>'+
						// 				'<td><a href="javascript:;" title="'+(l.tags ? l.tags : '')+'">'+(l.tags ? l.tags.length : 0)+'</a></td></tr>';
						// 				$('.raw-listings tbody').append(h);
						// 			}
						// 		}
						// 	} 
						// });
					}
					break;
				case 'view-parsed-listings':
					h+= '<table class="widefat parsed-listings"><thead><tr><th style="width:25px">ID</th><th># of Photos</th><th>Title/Address</th><th>Price/Beds/Baths</th><th>Realtor</th><th>Tags:</th></tr></thead><tbody><tr><td><strong>Loading...</strong></td></tbody></table>';
					onLoad = function(){
						parser.DB({
							query: 'get-parsed-listings',
							onDone: function(d){
								$('.parsed-listings tbody').empty();
								for (var i in d){
									var l = d[i];
									var json = ['images','tags','other','baths'];
									for (var j in json) if (l[json[j]]) l[json[j]] = $.parseJSON(l[json[j]]);
									var hh = '<tr'+(i%2?' class="alternate"':'')+'>';
									hh+= '<td>'+l.id+'</td><td>'+l.images.length+'</td>';
									hh+= '<td><a href="'+ah_local.wp+'/listing/-'+l.id+'">'+(l.title ? l.title : '(no title)')+'</a><br/>'+l.street_address+(l.city ? '<br/>'+l.city : '')+(l.state ? ', '+l.state : '')+(l.zip ? ' '+l.zip : '')+'</td>';
									hh+= '<td>$'+l.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'<br/>'+l.beds+' / '+l.baths[0]+','+l.baths[1]+'</td>';
									hh+= '<td>';
									if (l.other.realtors) for (var j in l.other.realtors.realtor) {
										if (j > 0) hh+= ', ';
										hh+= l.other.realtors.realtor[j][0];
									}
									hh+= '</td>';
									hh+= '</tr>';
									$('.parsed-listings tbody').append(hh);
									console.log(l);	
								}
								
							}
						});
					}
					break;
				default:
					h+= '<p>Page Not Found: '+thePage+'</p>';
					break;
			}
			$(this).html(h);
			onLoad();
			$(this).fadeIn({duration:250,queue:false});
			if (thePage != 'start') $('span.page-sub-title').html(' - '+thePage).fadeIn({duration:250,queue:false});;
			$('#parser-nav [menu-page="'+thePage+'"]').addClass('selected');
		});
	}
	this.pullSummaryPages = function(site, page){
		if (page == null) page = 1;
		if (page > site.pages) {
			$('#parser-page').html('Parsing complete.');
		} else {
			var parseURL = site.url+site.options;
			parseURL += '&page='+page;
			$('#parser-page').html('Storing summary page '+page+' results from '+site.title);
			parser.DB({ query: 'pull-summary-pages', data: {URL: parseURL}, onDone: function(d){ parser.pullSummaryPages(site, page+1); } });
		}
	}
	this.getUngeocoded = function(){
		parser.DB({
			query:'get-ungeocoded',
			onDone: function(d){
				// console.log(d);
				parser.thickbox({
					title: 'Geocode Listings',
					width: 300,
					height: 100,
					html: '<p>Geocode '+d.length+' listings?</p>',
					onSubmit: function(){
						parser.geocode(d, 0);
					}
				});
			}
		});
	}
	this.geocode = function(ids, i){
			$('#tb-submit').html('<p>Geocoding '+(i+1)+' of '+ids.length+'...</p>');
			parser.DB({
				query:'geocode-by-id',
				data: {id: ids[i]},
				onDone: function(d){
					if (i >= ids.length) tb_remove();
					else parser.geocode(ids, i+1);
				}
			});
	}
	this.pullDetails = function(){
		var ids = [];
		$('.listing-check input').each(function(){ $(this).prop('checked') ? ids.push(parseInt($(this).parent().parent().attr('listing-id'))) : null; });
		if (ids.length > 0) this.thickbox({
			title: 'Parsing Listing Details',
			width: 300,
			height: 100,
			html: '<p>Load details for '+(ids.length === 1 ? ids.length+' listing' : ids.length+' listings')+'?</p>',
			onSubmit: function(){ parser.thickboxResize(300, 50); parser.pullDetailSingle(0, ids); }
		});
	}
	this.pullDetailSingle = function(i, ids){
		i = parseInt(i);
		$('#tb-submit').fadeOut({duration:250,queue:false,complete:function(){
			if (i >= ids.length){
				$(this).html('<p>Pulled raw data successfully. Parsing...</p>').fadeIn({duration:250,queue:false});
				parser.DB({
					query:'parse-raw-listings',
					onDone: function(d){
						console.log(d);
						$('#tb-submit').fadeOut({duration:250,queue:false,complete:function(){
							$(this).html('<p>Listings parsed successfully. Geocoding...</p>');
							parser.DB({
								query:'geocode-all',
								onDone: function(d){
									console.log(d);
								}
							});
							$(this).fadeIn({duration:250,queue:false});
						}});
						tb_remove();
						parser.alert('Parsing completed.', 'Server Response');
					}
				});
			} else {
				$(this).html('<p>Pulling raw data for listing '+(i+1)+' of '+ids.length+'</p>').fadeIn({duration:250,queue:false});
				parser.DB({
					query:'pull-single-detail',
					data:{id:ids[i]},
					onDone: function(d){
						console.log(d);
						parser.pullDetailSingle(i+1, ids);
					}
				});
			}
		}});
	}
	this.getListings = function(x){
		if (x == null) { x = {}; x.type = null; }
		if (x.onDone == null) x.onDone = function(){};
		if (x.type == 'details') null;
		else this.DB({ query:'get-unparsed-listings', onDone:function(d){x.onDone(parser.parseListingsFromDB({listings:d}))} });
	}
	this.getListingByID = function(listing_id){
		for (var i in this.listings){
			if (this.listings[i].id == listing_id){ return this.listings[i]; break; }
		}
	}
	this.parseListingsFromDB = function(x){
		var res = [];
		for (var i in x.listings){
			var l = x.listings[i];
			var ints = ['price','parsed','id'];
			if (x.type == 'details'){

			}
			var json = ['site_vars'];
			for (var j in ints) l[ints[j]] = parseInt(l[ints[j]]);
			for (var j in json) l[json[j]] = $.parseJSON(l[json[j]]);
			l.added_obj = new Date(Date.parse(l.added));
			res.push(l);
		}
		return res;
	}
	this.parseCSV = function(){
		this.DB({ 
			query:'parse-email-csv',
			dataType: 'html',
			onDone:function(d){
				console.log(d);
			} 
		});
	}
	this.DB = function(x){
		if (x == null || x.query == null) { parser.alert('Query not sent.','Error: parser.DB'); return false; }
		if (x.dataType == null) x.dataType == 'json';
		x.data == null ? x.headers = {query:x.query} : x.headers = {query:x.query,data:x.data};
		if (x.onDone == null) x.onDone = function(){};
		if (x.onError == null) x.onError = function(d){ parser.alert(d, 'Error: Parser Script Error', {width:650, height: 200}) };
		$.ajax({ url:ah_local.wp+'/_admin/parser_ajax.php', data: x.headers, dataType: x.dataType, type:'POST'}).done(function(d){
			if (d.charAt(0) == '{') d = $.parseJSON(d);
			else d = {status:'fail', data:d.toString()};
			if (d.status == 'OK') x.onDone(d.data);
			else x.onError(d.data); 
		});
	}
	this.thickboxResize = function(width, height){
		var displayWidth =  width * 0.9;
		var displayHeight = height * 0.9;
		$("#TB_ajaxContent").animate({ height: displayHeight, width: displayWidth }, {duration:250,queue:false});
		$("#TB_window").animate({
			marginLeft: 0 - (displayWidth + 50) / 2,
		  marginTop: 0 - (displayHeight + 50) / 2,
		  height: displayHeight+50,
		  width: displayWidth+30
		},{duration:250,queue:false});
	}
	this.thickbox = function(x){
		if (parser.tbCount == null) parser.tbCount = 0;
		if (x == null) x = {};
		if (x.html == null) x.html = '';
		if (x.width == null) x.width = 200;
		if (x.buttonText == null) x.buttonText = 'OK';
		x.height == null ? x.height = 200 : null;
		if (x.title == null) x.title = '';
		if (x.onSubmit == null) x.onSubmit = function(){ tb_remove(); };
		if (x.onOpen == null) x.onOpen = function(){};
		if (x.onClose == null) x.onClose = function(){};
		x.close = function(){tb_remove()};
		var h = '<div id="thickbox-window-'+(parser.tbCount+1)+'" style="display:none;"><div style="text-align:center;"><form id="tb-submit">'+x.html+'<button>'+x.buttonText+'</button></form></div></div><a href="#TB_inline?width='+x.width+'&height='+x.height+'&inlineId=thickbox-window-'+(parser.tbCount+1)+'" id="thick-click-'+(parser.tbCount+1)+'" class="thickbox" style="display:none;"></a>';
		$('body').append(h);
		$('#thick-click-'+(parser.tbCount+1)).on('click',function(){
			parser.tbCount++;
			var old_remove = tb_remove;
			tb_remove = function(){
				x.onClose();
				old_remove();
				$('#thickbox-window-'+parser.tbCount).remove();
				$('#thick-click-'+parser.tbCount).remove();
				parser.tbCount--;
				return false;
			}
		}).click();
		$('#TB_ajaxWindowTitle').html(x.title);
		$('#tb-submit').submit(function(e){ e.preventDefault(); x.onSubmit(); });
		x.onOpen();
		var displayWidth =  x.width * 0.9;
		var displayHeight = x.height * 0.9;
		$("#TB_ajaxContent").css({ height: displayHeight, width: displayWidth });
		$("#TB_window").css({
		  'margin-left': 0 - (displayWidth + 50) / 2,
		  'margin-top': 0 - (displayHeight + 50) / 2,
		  opacity: 0,
		  height: displayHeight+50,
		  width: displayWidth+30
		}).animate({opacity:1},{duration:250,queue:false});
		if ($('#tb-submit button').length == 1) $(document).keypress(function(e){ if (e.which==13) $("#tb-submit").submit() });
	}
	this.alert = function(mess, title, size){ this.thickbox({title:(typeof title == 'undefined' ? 'Alert' : title), height: (size != null && size.height != null) ? size.height : 100, width: (size != null && size.width != null) ? size.width : 400, html:'<p>'+mess+'</p>'}); }
}
parser.init();
});