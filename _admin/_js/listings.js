var listingAdmin;
jQuery(document).ready(function($){
listingAdmin = new function(){
	this.ajaxUrl = ah_local.tp+"/_admin/ajax.php";
	this.authors = ah_local.authors;

	this.cleanUpImages = function(){
		$.ajax({type: "POST", url: ah_local.tp+'/_classes/Image.class.php', 
			data: {query: 'clean', data: 'listings'}, 
		success: function(d){ console.log(d) }, dataType: "JSON"});
	}
	this.editAuthor = function(listingID, authorID, authorHasAccount){
		$('#author-window').remove();
		var h = '<div style="text-align:center;"><form id="tb-submit"><p>Select new author:</p><select id="tb-author"><option disabled value="null"';
		if (authorID < 1) h+= ' selected';
		h+='>Select new author</option>';
		for (var i in this.authors){ 
			h+= '<option value="'+this.authors[i].author_id+'"';
			if (authorHasAccount &&
				this.authors[i].author_id == authorID) h+= ' selected';
			else if (!authorHasAccount &&
					 this.authors[i].id == authorID) h+= ' selected';
			h+= '>'+this.authors[i].first_name+' '+this.authors[i].last_name+'</option>';
		}
		h+='</select><button>Submit</button></form></div>';
		$('body').append('<div id="author-window" style="display:none;"></div><a href="#TB_inline?width=200&height=100&inlineId=category-window" id="thick-click" class="thickbox" style="display:none;"></a>');
		$('#thick-click').click();
		$('#TB_ajaxWindowTitle').html('Choose an Author');
		$('#TB_ajaxContent').html(h);
		$('#tb-submit').submit(function(event){
			event.preventDefault();
			if ( $('#tb-author').val() != null ) {
				var newAuthor = $('#tb-author').val();
				$('#TB_ajaxContent').html('Saving...');
				$.post(listingAdmin.ajaxUrl,{ 'query':'update-listing','data':{id:listingID, fields:{author:newAuthor}} },function(){},'json').done(function(data){
					if (data.status == 'OK'){ $('#TB_closeWindowButton').click(); location.reload(); }
					else $('#TB_ajaxContent').html('Unable to update listing author.<br/><br/>'+data.data);
				});
			}
		});
	}
	this.deleteListing = function(listingID){
		if (confirm("Are you sure you want to delete listing: "+listingID)) {
			$.post(listingAdmin.ajaxUrl,{'query':'delete-listing','data':{id:listingID}},function(){},'json').done(function(data){
				if (data.status == 'OK'){ location.reload(); }
				else console.log(data.data);
			});
		}
	}
}
});