var listingAdmin;

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(what) {
    var user = getCookie(what);
    if (user != "") {
        return true;
    } else {
        return false;
    }
}

jQuery(document).ready(function($){
	listingAdmin = new function(){
		this.ajaxUrl = ah_local.tp+"/_admin/ajax_rejected.php";
		this.authors = ah_local.authors;
		this.curPage = 0;
		this.pageSize = 10;
		this.listings = [];
		this.rowCount = 0;
		this.gotRowCount = false;

		this.init = function() {
			if (!checkCookie('myCurrentPage')) {
				listingAdmin.curPage = 0;
				setCookie('myCurrentPage', listingAdmin.curPage, 2);
			}
			else {
				listingAdmin.curPage = parseInt(getCookie('myCurrentPage'));
			}

			listingAdmin.getPage(listingAdmin.curPage);
			listingAdmin.getRowCount();

			$('#prevButton').on('click', function() {
				console.log("Previous selected, curPage:"+listingAdmin.curPage);
				if (listingAdmin.curPage > 0) {
					listingAdmin.getPage(listingAdmin.curPage - 1);
					listingAdmin.curPage--;
					setCookie('myCurrentPage', listingAdmin.curPage, 2);
					$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
				}
			});

			$('#nextButton').on('click', function() {
				console.log("Next selected, curPage:"+listingAdmin.curPage+", last:"+listingAdmin.rowCount);
				if ( (listingAdmin.curPage + 1)*listingAdmin.pageSize < listingAdmin.rowCount ) {
					listingAdmin.getPage(listingAdmin.curPage + 1);
					listingAdmin.curPage++;
					setCookie('myCurrentPage', listingAdmin.curPage, 2);
					$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));
				}
			});

			listingAdmin.setSlider();
			
		}

		this.retrySetSlider = function() {
			listingAdmin.setSlider();
		}

		this.setSlider = function() {
			if (!listingAdmin.gotRowCount) {
				window.setTimeout(function(){
					listingAdmin.retrySetSlider();
				}, 250);
				return;
			}

			$('#sliderValue').html("Current Page: "+(listingAdmin.curPage+1));

			if ( (listingAdmin.rowCount / listingAdmin.pageSize) > 2) {
				console.log("Max page is "+(listingAdmin.rowCount / listingAdmin.pageSize));
				var maxi = Math.floor(listingAdmin.rowCount / listingAdmin.pageSize) - 1;
				maxi = (listingAdmin.rowCount % listingAdmin.pageSize) ? maxi + 1 : maxi;
			    $( "#pageSlider" ).slider({
			      range: false,
			      min: 0,
			      max: maxi,
			      value: listingAdmin.curPage,
			      stop: function( event, ui ) {
			        listingAdmin.curPage = ui.value;
			        listingAdmin.getPage(listingAdmin.curPage);
			        setCookie('myCurrentPage', listingAdmin.curPage, 2);
			        $('#sliderValue').html("Current Page: "+(ui.value+1));
			      },
			      slide: function( event, ui) {
			      	$('#sliderValue').html("Current Page: "+(ui.value+1));
			      }
			    });
			}
		}

		this.getPage = function(page) {
			var startAt = listingAdmin.listings.length ? listingAdmin.listings[listingAdmin.listings.length - 1].id +1 : 0;
			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: { query: 'getpage',
						page: page,
						perPage: listingAdmin.pageSize,
						startId: startAt },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getPage: "+msg);
				},					
			  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any PP starting point data</p>' });
				  	}
				  	else {
				  		if (data.data) {
					  		listingAdmin.listings = data.data;
					  		listingAdmin.showListings();
					  	}
		  			}
		  		}
	  		})
		}

		this.showListings = function() {
			var body = '';
			for(var x = 0; x < listingAdmin.listings.length; x++) {
				body += '<tr';
				if ( (x % 2) == 1)
					body += " class='alternate'";
				body += '>';
				body += "<td>"+listingAdmin.listings[x].id+"</td>";
				body += "<td>"+listingAdmin.listings[x].price+"</td>";
				body += "<td>"+listingAdmin.listings[x].beds+"/"+listingAdmin.listings[x].baths+"</td>";
				listingAdmin.listings[x].images = JSON.parse(listingAdmin.listings[x].images);
				if (listingAdmin.listings[x].images != 'undefined' &&
					listingAdmin.listings[x].images != null &&
					listingAdmin.listings[x].images.length) {
					var galleryName = 'gallery'+x;
					body += "<td style='width: 220px'><ul class='listing-"+galleryName+"'>";
					for(var i = 0; i < 6 && i < listingAdmin.listings[x].images.length; i++) {
						if (listingAdmin.listings[x].images[i].file.indexOf("http") != -1) {
							if (listingAdmin.listings[x].images.length == 1)
								body += "<li class='listing-image-single' style='width: 210px; height: 120px; margin-left: 140px'><img src="+listingAdmin.listings[x].images[i].file+" style='width: 210px; height: 120px'/></li>";
							else
								body += "<li class='listing-image'><img src="+listingAdmin.listings[x].images[i].file+" style='width: 100%; height: 100%'/></li>";
						}
						else
							body += "<li class='listing-image'><img src="+ah_local.wp+"/_img/_listings/210x120/"+listingAdmin.listings[x].images[i].file+" style='width: 100%; height: 100%'/></li>";
					}
					body += "</ul></td>";				
				}
				else
					body += "<td style='padding-left: 180px'>No Image</td>";
				body += "<td><a href="+ah_local.wp+"/listing-rejected/"+listingAdmin.listings[x].id+" target='_blank'>"+listingAdmin.listings[x].title+"</a></td>";
				body += "<td><a href=javascript:listingAdmin.transfer("+listingAdmin.listings[x].id+")>Transfer</a></td>"
				body += "</tr>";
			}
			$('.widefat tbody').html(body);
			var slider;

			$('.listing-image-single').hover(
				function() {
					console.log("entered hover");
					$(this).animate({
						width: "800px",
						height: "640px"
					}, 250, function(){});
					$(this).children().animate({
						width: "800px",
						height: "640px"
					}, 250, function(){});
				},
				function() {
					console.log("leave hover");
					$(this).animate({
						width: "210px",
						height: "120px"
					}, 250, function(){});
					$(this).children().animate({
						width: "210px",
						height: "120px"
					}, 250, function(){});
				}
			);
			
			for(var x = 0; x < listingAdmin.listings.length; x++) {
				var slider;
				var targetName = 'ul.listing-gallery'+x;
				if ($(targetName).children('li').length > 1){
					slider = $(targetName).bxSlider({
					  auto: true,
					  pause: 3000,
					  mode: 'fade',
					  responsive: 'false',
					  controls: 'false',
					  autoHover: 'true',
					  onSlideBefore: function($element, oldIndex, newIndex) {
					  		$element.hover(
								function() {
									console.log("entered hover:"+newIndex);
									$(this).parent().parent().parent().animate({
										width: "800px",
										height: "640px"
									}, 250, function(){});
									$(this).parent().parent().animate({
										width: "800px",
										height: "640px"
									}, 250, function(){});
									$(this).animate({
										width: "800px",
										height: "640px"
									}, 250, function(){});
								},
								function() {
									console.log("leave hover:"+newIndex);
									$(this).parent().parent().parent().animate({
										width: "210px",
										height: "120px"
									}, 250, function(){});
									$(this).parent().parent().animate({
										width: "210px",
										height: "120px"
									}, 250, function(){});
									$(this).animate({
										width: "210px",
										height: "120px"
									}, 250, function(){});
								})
					  }
					});
				}
			}
		}

		this.getRowCount = function() {
			listingAdmin.gotRowCount = false;
			$.ajax({
				url: listingAdmin.ajaxUrl,
				data: { query: 'row-count'},
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getRowCount: "+msg);
				},					
			  	success: function(data){
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Failed to get any PP starting point data</p>' });
				  	}
				  	else {
				  		if (data.data) {
					  		listingAdmin.rowCount = parseInt(data.data);
					  	}
		  			}
					listingAdmin.gotRowCount = true;
		  		}
	  		})
		}

		this.transfer = function(listingID){
			if (confirm("Are you sure you want to transfer listing: "+listingID+" to the main listing table?")) {
				$.post(listingAdmin.ajaxUrl,
						{'query':'transfer-listing',
						'data':{id:listingID}},
						function(){},
						'json')
					.done(function(data){
						if (data.status == 'OK'){ 
							istingAdmin.rowCount--;
							location.reload(); 
						}
						else console.log(data.data);
					});
			}	
		}
	}

	listingAdmin.init();
});