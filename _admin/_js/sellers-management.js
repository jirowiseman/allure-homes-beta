
var sellerAdmin;

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie(what) {
    var user = getCookie(what);
    if (user != "") {
        return true;
    } else {
        return false;
    }
}

var SellerTasks = {
  MORE_TAGS: 0,
  HIRES_PHOTOS: 1,
  BETTER_DESC: 2,
  ADD_BEDS: 3,
  ADD_BATHS: 4,
  ADD_SELLER_PHOTO: 5,
  WRITE_AREA_BLOG: 6
}

var MetaDataType = {
	SELLER_IMPROVEMENT_TASKS: (1 << 0),
	SELLER_KEY: (1 << 1),
	SELLER_BRE: (1 << 2),
	SELLER_INVITE_CODE: (1 << 3),
	SELLER_VISITATIONS: 16,
	SELLER_PROFILE_DATA: 32,
	SELLER_AGENT_MATCH_DATA: 64,
	SELLER_MODIFIED_PROFILE_DATA: 128,
	SELLER_NICKNAME: 256,
	SELLER_NEW_ORDER: 512,
	SELLER_AGENT_ORDER: 1024
}

var SellerFlags = {
	SELLER_AGREED_TAG_TERMS: 1,
	SELLER_UNUSED_1: 2,
	SELLER_UNUSED_2: 4,
	SELLER_IS_PREMIUM_LEVEL_1: 8,
	SELLER_IS_PREMIUM_LEVEL_2: 16,
	SELLER_IS_LIFETIME: 32,
	SELLER_INACTIVE_EXCLUSION_AREA: 64
}

var SellerImprovements = [
  {visible: "Tags",
   element: 'tags',
   action: SellerTasks.MORE_TAGS},

  {element: 'images',
   visible: "HiRes Photos",
   action: SellerTasks.HIRES_PHOTOS},

  {element: 'about',
   visible: 'Better description',
   action: SellerTasks.BETTER_DESC},

  {element: 'beds',
   visible: 'Beds',
   action: SellerTasks.ADD_BEDS},

  {element: 'baths',
   visible: 'Baths',
   action: SellerTasks.ADD_BATHS},

  {element: 'seller-photo',
   visible: "Seller Photo",
   action: SellerTasks.ADD_SELLER_PHOTO},

  {element: 'area-blog',
   visible: 'Write Area Blog',
   action: SellerTasks.WRITE_AREA_BLOG}
]


var ActivityState = {
	INVITATION: 0,
	IMPROVEMENT: 1,
	MAIL: 2
}

var ListingState = {
	INACTIVE: 0,
	ACTIVE: 1,
	WAITING:   2,
	REJECTED: 3,
	TOOCHEAP: 4,
	NEVER: 5,
	AVERAGE: 6,
	NONE: 7
}

var SellerEmailDbFlags = {
	INACTIVE: (1 << 20),
	ACTIVE: (1 << 21),
	WAITING:   (1 << 22),
	REJECTED: (1 << 23),
	TOOCHEAP: (1 << 24),
	NEVER: (1 << 25),
	AVERAGE: (1 << 26),
	NONE: (1 << 27),
	EMAIL_UNSUBSCRIBED: (1 << 31)
}


var EmailTypes = {
	PROMO_FIRST: 8,
	PROMO_SECOND: 16,
	PROMO_THIRD: 32,
	IMPROVE_LISTING: 64,
	PROMO_FOURTH: 128,
	PORTAL_PROMO_1: 256
}


var ActivityStateNames = [
	'invitation',
	'IMPROVEMENT'
]

var OptionState = {
	SELLER: 0,
	ADDRESS: 1,
	IDS: 2
}

var ActiveListingNames = [
	'inactive',
	'active',
	'waiting',
	'rejected',
	'toocheap',
	'never',
	'average'
]

var ButtonOptions = [ [1,5], [2,3,5], [1,3,5], [1,2,5], [1,2,3,5,6], [2], [2,5] ];

jQuery(document).ready(function($){
	sellerAdmin = new function(){
		//this.ajaxUrl = ah_local.tp+"/_admin/ajax_viewer.php";
		this.ajaxUrl = ah_local.tp+"/_admin/ajax_sellers_management.php";
		this.ajaxSalesUrl = ah_local.tp+"/_admin/ajax_sales.php";
		this.authors = 0;
		this.inviteList = null; //$.parseJSON(ah_local.inviteList);
		this.curSellerPage = 0;
		this.curPage = 0;
		this.pageSize = 10;
		this.listings = [];
		this.rowCount = 0;
		this.sellerCount = 0;
		this.gotRowCount = false;
		this.gotSellerCount = false;
		this.activitySelected = 1; // 0 is invite code and was deprecated
		this.idSpecifier = 0;
		this.option = 0;
		this.lastOption = null;
		this.animate = false;
		this.showReminderRefresh = 1;
		//this.price = 800000;
		this.errorMax = 6;
		this.keywordsApplied = false;
		this.idSort = 0;
		this.priceSort = 0;
		this.authorIdSort = 0;
		this.citySort = 0;
		this.stateSort = 0;
		this.zipSort = 0;
		this.emailSort = 0;
		this.singleSeller = null;
		this.curSellerAuthorId = 0;
		this.myMountain = 0;
		this.myEverest = 0;
		this.sellerAdmin = false;
		this.gotCodes = false;
		this.gettingListingStat = 0;
		this.mailType = EmailTypes.PROMO_FIRST;
		this.onlyActiveSellers = 0;
		this.gettingEmailStats = false;
		this.waitingForEmailStats = false;
		this.distance = 0;
		this.mailchimp = 1;
		this.emailDbFlags = (SellerEmailDbFlags.INACTIVE | SellerEmailDbFlags.ACTIVE | SellerEmailDbFlags.WAITING | SellerEmailDbFlags.REJECTED | SellerEmailDbFlags.NEVER | SellerEmailDbFlags.TOOCHEAP | SellerEmailDbFlags.AVERAGE);
		this.pageClearEmailDb = 0;
		this.pagePerClearEmailDb = 1000;
		this.clearedEmailDbCount = 0;
		this.pageSendEmail = 0;
		this.pagePerSendEmail = 1000;
		this.sentEmailCount = 0;
		this.haltSendEmail = false;
		this.csvDumpCsvLimit = csvAgentCount;
		this.csv = '';
		this.chart = null;
		this.mailchimpCampaign = "7-7-M";
		this.testingEmail = false;
		this.mailMetaType = (1 << 0);
		this.resetFlag = 0;
		this.removeMeta  = 0;
		this.campaignIndex = 0;
		this.sentToCount = 0;
		this.complaintCount = 0;

		this.init = function() {
			if (!checkCookie('everest_sm') ||
				!checkCookie('mountain_sm')) {
				sellerAdmin.myMountain = ah_local.sessionID+"-"+Math.round(Math.random()*10000).toString();
				sellerAdmin.myEverest = 'everest'+"-"+Math.round(Math.random()*10000).toString();
				setCookie('mountain_sm', this.myMountain, 1);
				setCookie('everest_sm', this.myEverest, 1);
			}
			else {
				sellerAdmin.myMountain = getCookie('mountain_sm');
				sellerAdmin.myEverest = getCookie('everest_sm');
			}

			sellerAdmin.sendTrickOrTreat();
			sellerAdmin.createCodes(0);

			$('#doingStats').html( testStats ? "&nbsp;NOTICE::Testing stats, no emails sent out" : "&nbsp;NOTICE::Live email mode");


			$('#previewDiv').hide();
			$('#activityChooser #clear-flags').hide();
			$('#activityChooser #totals').hide();
			$('#activityChooser #stats').hide();

			if (!checkCookie('Activity')) {
				sellerAdmin.activitySelected = 1; // check invites
				setCookie('Activity', sellerAdmin.activitySelected, 2);
			}
			else {
				var value = getCookie('Activity') == "NaN" || getCookie('Activity') == 'false' ? '1' : getCookie('Activity');
				sellerAdmin.activitySelected = parseInt(value) > 1 ? 1 : (parseInt(value) == 0 ? 1 : parseInt(value)); 
			}

			if (!checkCookie('ReminderRefresh')) {
				sellerAdmin.showReminderRefresh = 1;
				setCookie('ReminderRefresh', sellerAdmin.showReminderRefresh, 7);
			}
			else {
				var value = getCookie('ReminderRefresh') == "NaN" ? '1' : getCookie('ReminderRefresh');
				sellerAdmin.showReminderRefresh = parseInt(value);
			}

			//('#optionWrapper .spinner').fadeOut(1, function(){});

			$('#sliderValueSeller').show();
			$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));
		    $("#pageSliderSeller").show();
		    $( "#pageSliderSeller" ).slider({
		      range: false,
		      min: 0,
		      max: sellerAdmin.curSellerPage,
		      value: sellerAdmin.curSellerPage,
		      stop: function( event, ui ) {
		      	if (sellerAdmin.gettingListingStat) {
					ahtb.alert("Please wait until the current listing stats for agents have completed.",
						{height: 160});
					$( "#pageSliderSeller" ).slider('value', sellerAdmin.curSellerPage);
					$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));
					return;
				}
		        sellerAdmin.curSellerPage = ui.value;
		        sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		        var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'SellerPage';
		        setCookie(target, sellerAdmin.curSellerPage, 2);
		        $('#sliderValueSeller').html("Current Page: "+(ui.value+1));
		      },
		      slide: function( event, ui) {
		      	$('#sliderValueSeller').html("Current Page: "+(ui.value+1));
		      }
		    });
			

		    $('#sliderValue').show();
			$('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));
		    $("#pageSlider").show();
		    $( "#pageSlider" ).slider({
		      range: false,
		      min: 0,
		      max: sellerAdmin.curPage,
		      value: sellerAdmin.curPage,
		      stop: function( event, ui ) {
		        sellerAdmin.curPage = ui.value;
		        sellerAdmin.getPage();
		        var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'Page';
		        setCookie(target, sellerAdmin.curPage, 2);
		        $('#sliderValue').html("Current Page: "+(ui.value+1));
		      },
		      slide: function( event, ui) {
		      	$('#sliderValue').html("Current Page: "+(ui.value+1));
		      }
		    });

		    var h = '';
		    for(i = 0; i < 21; i++)
		    	h += '<option value="'+(i*5)+'">'+(i == 0 ? "City Limit" : i*5)+'</option>';
		    $('#optionChooser #addressOption #distance').html(h);
		    $('#optionChooser #addressOption #distance').on('change', function() {
		    	var val = $(this,'option:selected').val();
		    	console.log("Distance is "+(val));
		    	sellerAdmin.distance = val;
		    })

		    // $('#optionChooser .optionOnlyActiveSeller').on('click', function() {
		    // 	var val = $(this).prop('checked');
		    // 	console.log("optionOnlyActiveSeller is now "+val);
		    // 	sellerAdmin.onlyActiveSellers = val ? 1 : 0;
		    // 	sellerAdmin.getSellerCount();
		    // })

		    $("#sellerSelection input[name='sellers']").on('click', function() {
		    	var val = $(this).attr('for');
		    	console.log("optionOnlyActiveSeller is now "+val);
		    	sellerAdmin.onlyActiveSellers = parseInt(val);
		    	if (val == '2' &&
		    		sellerAdmin.emailDbFlags == 0) {
		    		ahtb.alert("Please pick at least one listing type", {height: 150});
		    		return;
		    	}
		    	sellerAdmin.setCookies(sellerAdmin.activitySelected);
		    	sellerAdmin.getSellerCount();
		    });

		    $("#sellerSelection input[type='checkbox']").on('change', function() {
		    	var val = parseInt($(this).attr('for'));
		    	var checked = $(this).prop('checked');
		    	var id = $(this).attr('id');
		    	if (checked)
		    		sellerAdmin.emailDbFlags |= (1 << (20+val));
		    	else
		    		sellerAdmin.emailDbFlags &= ~(1 << (20+val));
		    	console.log(id + " is now checked:"+checked+", for:"+val+", emailDbFlags:"+sellerAdmin.emailDbFlags);
		    	sellerAdmin.setCookies(sellerAdmin.activitySelected);
		    	if (sellerAdmin.emailDbFlags != 0 &&
		    		sellerAdmin.onlyActiveSellers == 2)
		    		sellerAdmin.getSellerCount();
		    });

			sellerAdmin.getCookies(sellerAdmin.activitySelected);
			$("#activityChooser #"+sellerAdmin.getActiveName(sellerAdmin.activitySelected)).prop("checked", true);
			sellerAdmin.setVisibility();
			sellerAdmin.getRowCount();

			// $("#activityChooser #live").prop("checked", true);
			// $("#optionChooser #seller").prop("checked", true);
			// sellerAdmin.lastOption = $("#optionChooser #seller");
			// $("#optionChooser .optionActive").prop("checked", false);
			// sellerAdmin.disable($('#sellerOption'));
			// sellerAdmin.disable($('#addressOption'));
			// $('#sellerOption').children().prop("disabled", !sellerAdmin.option);
			// $('#addressOption').children().prop("disabled", !sellerAdmin.option);

			$('#activityChooser input[name="table"]').on('change', function() {
				if ($(this).prop('checked'))
				{
					var oldSelection = sellerAdmin.activitySelected;
					sellerAdmin.getActiveSelection();
					sellerAdmin.getSetPageCookies(oldSelection);
					sellerAdmin.setRowCountCookies(oldSelection);
					sellerAdmin.keywordsApplied = false;
					sellerAdmin.getRowCount();
					//sellerAdmin.showSellers();
					sellerAdmin.setVisibility();
					setCookie('Activity', sellerAdmin.activitySelected, 2);
					// $('#keyword').val("");
					// $('#keyword').css('placeHolder', "Enter a keyword to search in the description");
					// if (sellerAdmin.activitySelected == 1 ||
					// 	sellerAdmin.activitySelected == 2) 
					// 	$('#applyKeyword').prop("disabled", false);
					// else
					// 	$('#applyKeyword').prop("disabled", true);
				}
			});

			$('#activityChooser #preview').on('click', function() {
				$('#emailStats').hide();
				sellerAdmin.getPreview(sellerAdmin.mailType);
			})

			$('#activityChooser #testEmail').on('click', function() {
				if (sellerId == 0) {
					ahtb.alert("Please create a sellers account to send a test email.", {height: 180});
					return;
				}
				var oldchimp = sellerAdmin.mailchimp;
				sellerAdmin.mailchimp = 0;
				sellerAdmin.sendEmail(sellerAdmin.mailType, 'email-test', [{id:sellerId}]);
				sellerAdmin.mailchimp = oldchimp;
			})

			$('#activityChooser #sendEmail').on('click', function() {
				// if (!sellerAdmin.mailchimp)
				// 	sellerAdmin.sendEmail(sellerAdmin.mailType, 'email-sellers');
				// else
					sellerAdmin.getSellerCount(false, true);
			})

			$('#activityChooser #mailchimp').on('click', function() {
				var val = $(this).prop('checked');
				console.log("Mailchimp is now "+val);
				sellerAdmin.mailchimp = val ? 1 : 0;
				sellerAdmin.setCookies(sellerAdmin.activitySelected);
			})

			$('#activityChooser #test-run').on('click', function() {
				var val = $(this).prop('checked');
				console.log("TestRun is now "+val);
				sellerAdmin.testingEmail = val ? 1 : 0;
				sellerAdmin.setCookies(sellerAdmin.activitySelected);
			})

			$('#activityChooser #totals').on('click', function() {
				sellerAdmin.getEmailStats();
			})

			$('#activityChooser #stats').on('click', function() {
				sellerAdmin.sendEmail(sellerAdmin.mailType, 'email-stats');
			})

			$('#activityChooser #stats-range').on('click', function() {
				sellerAdmin.sendEmail(sellerAdmin.mailType, 'email-stats-range');
			})

			$('#activityChooser #activity-stats').on('click', function() {
				sellerAdmin.getEmailActivityStats();
			})

			$('#activityChooser #update-sentTo').on('click', function() {
				sellerAdmin.startUpdateChimpSentTo();
			})

			$('#activityChooser #update-complaints').on('click', function() {
				sellerAdmin.startUpdateChimpCompliants();
			})

			$('#activityChooser #get-chimp-complaints').on('click', function() {
				sellerAdmin.getChimpComplaints();
			})

			$('#activityChooser #get-chimp-unsubscribes').on('click', function() {
				sellerAdmin.getChimpUnsubscribes();
			})

			$('#activityChooser #get-chimp-open').on('click', function() {
				sellerAdmin.getChimpOpen();
			})

			$('#activityChooser #clean-emaildb').on('click', function() {
				sellerAdmin.cleanMeta();
			})

			$('#activityChooser #rebuild-flags').on('click', function() {
				sellerAdmin.rebuildFlags();
			})

			$('#activityChooser #rebuild-meta').on('click', function() {
				sellerAdmin.rebuildMeta();
			})

			$('#activityChooser #radian-array').on('click', function() {
				sellerAdmin.makeRadialArray();
			})

			$('#activityChooser #scrub-seller-tags').on('click', function() {
				sellerAdmin.scrubSellerTags();
			})

			$('#activityChooser #update-seller-portal-visits').on('click', function() {
				sellerAdmin.updateSellerPortalVisits();
			})

			$('#activityChooser #test-ajax-paywhirl').on('click', function() {
				sellerAdmin.testAjaxPaywhirl();
			})

			$('#activityChooser #delete-seller').on('click', function() {
				sellerAdmin.deleteSeller();
			})

			$('#activityChooser #unlock-user').on('click', function() {
				sellerAdmin.unlockUser();
			})

			$('#activityChooser #fix-email-response').on('click', function() {
				sellerAdmin.fixEmailResponse();
			})

			$('#activityChooser #get-zoho-crm-csv').on('click', function() {
				sellerAdmin.getZohoCrmCsv();
			})

			$('#activityChooser #zoho-update-lead-ids').on('click', function() {
				sellerAdmin.updateLeadIds();
			})

			$('#activityChooser #zoho-udpate-new-reservations').on('click', function() {
				sellerAdmin.updateNewReservations();
			})

			$('#activityChooser #zoho-udpate-existing-reservations').on('click', function() {
				sellerAdmin.updateExistingReservations();
			})

			$('#activityChooser #clear-flags').on('click', function() {
				ahtb.open({html: "<p>Are you sure?  It can mess up a lot of things!<br/>May lose all email campaign data!</p>",
							width: 500,
							height: 180,
							buttons: [{text:'Go Ahead Do it', action: function() {
								console.log("First step in clearing all flags taken");
								ahtb.open({html: "<p>Really clear all flags?!!</p>",
											width: 450,
											height: 150,
											buttons: [{text:'Kill it', action: function() {
												var x = {query: 'get-sellers-email-db-count',
														 done: function(data) {
														 	sellerAdmin.pageClearEmailDb = 0;
														 	sellerAdmin.clearedEmailDbCount = 0;
														 	sellerAdmin.totalSellersEmailDb = parseInt(data);
														 	var h = '<div id="emailDb">';
																h+= 	'<label id="message">Remaining sellers to process</label>&nbsp;';
																h+= 	'<span id="sellerCount"></span>';
																h+= '</div>';
														 	ahtb.open({html: h,
																	   width: 450,
																		height: 150,
																		opened: function() {
																			var ele = $('#emailDb #sellerCount');
																			ele.html(sellerAdmin.totalSellersEmailDb.toString());
																			sellerAdmin.clearOutAllFlags();
																		}});
														 },
														 error: function(data) {
														 	console.log(data);
														 	var msg = typeof data == 'undefined' ? "Unknown server error" : data;
														 	ahtb.alert(	msg,
														 				{height: 150});
														 } };
												sellerAdmin.DB(x);
											}},
											{text:'Cancel', action: function() {
												ahtb.close();
											}}]
								});
							}},
							{text:'Cancel', action: function() {
								ahtb.close();
							}}]});
			})


			$('#previewDiv #hide').on('click', function() {
				$('#previewDiv').hide();
				$('#emailStats').show();
			})

			$("#optionChooser .optionActive").change(function(){
				sellerAdmin.option = $(this).prop("checked");
				console.log("Option is now "+sellerAdmin.option );
				sellerAdmin.setOptions();

				var oldSelection = sellerAdmin.activitySelected;
				sellerAdmin.getActiveSelection();
				sellerAdmin.getSetPageCookies(oldSelection);
				sellerAdmin.setRowCountCookies(oldSelection);
				if (!sellerAdmin.option) {
					sellerAdmin.getSellerCount();
					sellerAdmin.getRowCount();
				}
			});

			$("#optionChooser input[name='option']").on('click', function() {
				if ($(this).prop('checked'))
				{
					if ($(this).attr('id') == "name") {
						console.log("name checked");
						sellerAdmin.lastOption = $(this);
						$('#submitName').prop("disabled", false);
						$('#submitAddress').prop("disabled", true);
						$('#submitID').prop("disabled", true);
						//$('#sellerSingle').css('display','visible');
						$('#sellerSingle').show();
						$('#activityChooser #stats-range').prop('disabled', true);
					}
					else if ( $(this).attr('id') == "address") {
						console.log("address checked");
						sellerAdmin.lastOption = $(this);
						$('#submitName').prop("disabled", true);
						$('#submitAddress').prop("disabled", false);
						$('#submitID').prop("disabled", true);
						//$('#sellerSingle').css('display','none');
						$('#sellerSingle').hide();
						$('#activityChooser #stats-range').prop('disabled', false);
				 	}
				 	else if ( $(this).attr('id') == "ids") {
				 		console.log("id checked");
				 		sellerAdmin.lastOption = $(this);
				 		$('#submitName').prop("disabled", true);
						$('#submitAddress').prop("disabled", true);
						$('#submitID').prop("disabled", false);
						//$('#sellerSingle').css('display','none');
						$('#sellerSingle').hide();
						$('#activityChooser #stats-range').prop('disabled', true);
				 	}
				}
			});

			$("#optionChooser input[name='id-option']").on('click', function() {
				var id = $(this).attr('id');
				console.log("IdOption - selected "+id);
				sellerAdmin.idSpecifier = id;
			});

			$('#optionChooser #submitName').on('click', function() {
				sellerAdmin.curPage = 0;
				sellerAdmin.setCookies(sellerAdmin.activitySelected);
				sellerAdmin.getSingleSeller();
			});

			$('#optionChooser #submitAddress').on('click', function() {
				sellerAdmin.curPage = 0;
				sellerAdmin.setCookies(sellerAdmin.activitySelected);
				sellerAdmin.getSellerCount();
			});

			$('#optionChooser #submitID').on('click', function() {
				sellerAdmin.curPage = 0;
				sellerAdmin.setCookies(sellerAdmin.activitySelected);
				sellerAdmin.getSellerCount();
			});

			$('#activityChooser #mail-type').off().on('change', function() {
				var val = $(this,'option:selected').val();
				console.log("mail type is "+val);
				sellerAdmin.mailType = parseInt(val);
			});

			$('#activityChooser #mail-chimp').off().on('change', function() {
				var val = $(this,'option:selected').val();
				console.log("mail campaign type is "+val);
				sellerAdmin.mailchimpCampaign = val;
			});

			$('#activityChooser #mail-meta').off().on('change', function() {
				var val = $(this,'option:selected').val();
				console.log("mail meta type is "+val);
				sellerAdmin.mailMetaType = val;
				// sellerAdmin.setCookies(sellerAdmin.activitySelected);
			});

			$('#activityChooser #reset-flag-also').on('click', function() {
				var val = $(this).prop('checked');
				console.log("Reset flag is now "+val);
				sellerAdmin.resetFlag = val ? 1 : 0;
				// sellerAdmin.setCookies(sellerAdmin.activitySelected);
			})

			$('#activityChooser #remove-meta').on('click', function() {
				var val = $(this).prop('checked');
				console.log("Remove meta is now "+val);
				sellerAdmin.removeMeta = val ? 1 : 0;
				// sellerAdmin.setCookies(sellerAdmin.activitySelected);
			})

			sellerAdmin.finishInit();
		}

		this.finishInit = function() {
			if (!sellerAdmin.gotCodes) {
				window.setTimeout(function() {
					sellerAdmin.finishInit();
				}, 500);
				return;
			}
			sellerAdmin.getSellerCount(true);

			$('#prevButtonSeller').on('click', function() {
				if (sellerAdmin.gettingListingStat) {
					ahtb.alert("Please wait until the current listing stats for agents have completed.",
						{height: 160});
					return;
				}
				console.log("Previous selected, curPage:"+sellerAdmin.curSellerPage);
				if (sellerAdmin.curSellerPage > 0) {
					sellerAdmin.getSellerPage(sellerAdmin.curSellerPage - 1);
					sellerAdmin.curSellerPage--;
					var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'SellerPage';
					setCookie(target, sellerAdmin.curSellerPage, 2);
					console.log("Set Seller page for "+target+" to "+sellerAdmin.curSellerPage);
					$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));
					$( "#pageSliderSeller" ).slider('value', sellerAdmin.curSellerPage);
				}
			});

			$('#nextButtonSeller').on('click', function() {
				if (sellerAdmin.gettingListingStat) {
					ahtb.alert("Please wait until the current listing stats for agents have completed.",
						{height: 160});
					return;
				}
				console.log("Next selected, curPage:"+sellerAdmin.curSellerPage+", last:"+sellerAdmin.sellerCount);
				if ( (sellerAdmin.curSellerPage + 1)*sellerAdmin.pageSize < sellerAdmin.sellerCount ) {
					sellerAdmin.getSellerPage(sellerAdmin.curSellerPage + 1);
					sellerAdmin.curSellerPage++;
					var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'SellerPage';
					setCookie(target, sellerAdmin.curSellerPage, 2);
					console.log("Set Seller page for "+target+" to "+sellerAdmin.curSellerPage);
					$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));
					$( "#pageSliderSeller" ).slider('value', sellerAdmin.curSellerPage);
				}
			});
			

			$('#prevButton').on('click', function() {
				console.log("Previous selected, curPage:"+sellerAdmin.curPage);
				if (sellerAdmin.curPage > 0) {
					sellerAdmin.curPage--;
					sellerAdmin.getPage();
					var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'Page';
					setCookie(target, sellerAdmin.curPage, 2);
					console.log("Set page for "+target+" to "+sellerAdmin.curPage);
					$('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));
					$( "#pageSlider" ).slider('value', sellerAdmin.curPage);
				}
			});

			$('#nextButton').on('click', function() {
				console.log("Next selected, curPage:"+sellerAdmin.curPage+", last:"+sellerAdmin.rowCount);
				if ( (sellerAdmin.curPage + 1)*sellerAdmin.pageSize < sellerAdmin.rowCount ) {
					sellerAdmin.curPage++;
					sellerAdmin.getPage();
					var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'Page';
					setCookie(target, sellerAdmin.curPage, 2);
					console.log("Set page for "+target+" to "+sellerAdmin.curPage);
					$('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));
					$( "#pageSlider" ).slider('value', sellerAdmin.curPage);
				}
			});

			$('#optionWrapper #createCodes').on('click', function() {
				console.log("Create codes clicked.");
				sellerAdmin.createCodes(12);
			})

			$('#optionWrapper #showCodes').on('click', function() {
				console.log("Show codes clicked.");
				sellerAdmin.showCodes();
			})

			$('#optionWrapper #checkCodes').on('click', function() {
				console.log("Check codes clicked.");
				sellerAdmin.checkCodes();
			})
		}

		this.setOptions = function() {
			if (sellerAdmin.option) {
				sellerAdmin.enable($('#sellerOption'));
				// $('#sellerSingle').css('display','visible');
				//$('.sellerList tr *:nth-child(1)').css('display','visible');
				sellerAdmin.enable($('#addressOption'));
				sellerAdmin.enable($('#idOption'));
				$('#activityChooser #stats-range').prop('disabled', sellerAdmin.lastOption.attr('id') != 'address');
			}
			else {
				sellerAdmin.disable($('#sellerOption'));
				// $('#sellerSingle').css('display','none');
				//$('.sellerList tr *:nth-child(1)').css('display','none');
				sellerAdmin.disable($('#addressOption'));
				sellerAdmin.disable($('#idOption'));
				$('#activityChooser #stats-range').prop('disabled', true);
			}
			sellerAdmin.lastOption.prop("checked", true);
		}

		this.setVisibility = function() {
			// NOTE: using *:nth-child() did not work properly.  Had to separate the 'td' from 'th'
			if (sellerAdmin.activitySelected) {
				// $('.sellerList tr td:nth-child(1)').hide();
				// $('.sellerList tr th:nth-child(1)').hide();
				$('.sellerList tr td:nth-child(10)').hide();
				$('.sellerList tr th:nth-child(10)').hide();
				$('.sellerList tr td:nth-child(12)').hide();
				$('.sellerList tr th:nth-child(12)').hide();
				$('#selectAllDiv').show();
				// $('#selectAllDiv').hide();
				$('.sellerList tr td:nth-child(1)').show();
				$('.sellerList tr th:nth-child(1)').show();
				$('.sellerList tr td:nth-child(11)').show();
				$('.sellerList tr th:nth-child(11)').show();
				$('.sellerList tr td:nth-child(13)').show();
				$('.sellerList tr th:nth-child(13)').show();
				$('.sellerListDiv #sendImproveEmailAll').hide();
				$('.sellerListDiv #sendIntroEmailAll').hide();
			}
			else {
				$('#selectAllDiv').hide();
				$('.sellerList tr td:nth-child(1)').hide();
				$('.sellerList tr th:nth-child(1)').hide();
				$('.sellerList tr td:nth-child(11)').hide();
				$('.sellerList tr th:nth-child(11)').hide();
				$('.sellerList tr td:nth-child(13)').hide();
				$('.sellerList tr th:nth-child(13)').hide();
				$('.sellerListDiv #sendImproveEmailAll').hide();
				$('.sellerListDiv #sendIntroEmailAll').hide();
				$('.sellerList tr td:nth-child(10)').show();
				$('.sellerList tr th:nth-child(10)').show();
				$('.sellerList tr td:nth-child(12)').show();
				$('.sellerList tr th:nth-child(12)').show();
			}


			// NOTE: using *:nth-child() did not work properly.  Had to separate the 'td' from 'th'
			if (sellerAdmin.activitySelected) {
				$('.sellerSingle tr td:nth-child(9)').hide();
				$('.sellerSingle tr th:nth-child(9)').hide();
				$('.sellerSingle tr td:nth-child(11)').hide();
				$('.sellerSingle tr th:nth-child(11)').hide();
				$('.sellerSingle tr td:nth-child(10)').show();
				$('.sellerSingle tr th:nth-child(10)').show();
				$('.sellerSingle tr td:nth-child(12)').show();
				$('.sellerSingle tr th:nth-child(12)').show();
			}
			else {
				$('.sellerSingle tr td:nth-child(10)').hide();
				$('.sellerSingle tr th:nth-child(10)').hide();
				$('.sellerSingle tr td:nth-child(12)').hide();
				$('.sellerSingle tr th:nth-child(12)').hide();
				$('.sellerSingle tr td:nth-child(9)').show();
				$('.sellerSingle tr th:nth-child(9)').show();
				$('.sellerSingle tr td:nth-child(11)').show();
				$('.sellerSingle tr th:nth-child(11)').show();
			}

			if (sellerAdmin.option &&
				sellerAdmin.lastOption.attr('id') == 'name')
				$('#sellerSingle').show();
			else
				$('#sellerSingle').hide();

		}

		this.enable = function(parent)
		{
			$.each( parent.children(), function() {
				$(this).prop("disabled", false);
				sellerAdmin.enable($(this));
			});
		}
		this.disable = function(parent)
		{
			$.each( parent.children(), function() {
				$(this).prop("disabled", true);
				sellerAdmin.disable($(this));
			});
		}

		this.getActiveSelection = function() {
			var selected = 0;
			var i = 0;
			$.each( $("#activityChooser input[name='table']"), function() {
				if ($(this).prop('checked') ) {
					selected = i;
				}
				i++;
			});
			console.log("getActiveSelection:"+selected);
			sellerAdmin.activitySelected = selected;
			setCookie('Activity', sellerAdmin.activitySelected, 2);
		}

		this.getActiveName = function(active) {
			switch(active) {
				case ActivityState.INVITATION: return 'invites';
				case ActivityState.IMPROVEMENT: return 'improvements';
				case ActivityState.MAIL: return 'mail';
				default: return 'unknown';
			}
		}

		this.getListingState = function(active) {
			switch(active) {
				case ListingState.INACTIVE: return 'inactive';
				case ListingState.ACTIVE: return 'live';
				case ListingState.WAITING: return 'waiting';
				case ListingState.REJECTED: return 'rejected';
				case ListingState.TOOCHEAP: return 'tooCheap';
				case ListingState.NEVER: return 'never';
				case ListingState.AVERAGE: return 'average';
				// case ActiveState.SELLER: return 'seller';
				// case ActiveState.ADDRESS: return 'address';
				default: return 'unknown';
			}
		}


		this.getOptionName = function(opt) {
			switch(opt) {
				case OptionState.SELLER: return 'seller';
				case OptionState.ADDRESS: return 'address';
			}
		}

		this.setCookies = function(which) {
			var target = sellerAdmin.getActiveName(which)+'Page';
			switch (which) {
				case ActivityState.INVITATION:
				case ActivityState.IMPROVEMENT:
				case ActivityState.MAIL:
					setCookie(target, sellerAdmin.curPage, 2); 

					target = sellerAdmin.getActiveName(which)+'MailChimp';
					setCookie(target, sellerAdmin.mailchimp, 2); 

					target = sellerAdmin.getActiveName(which)+'TestingEmail';
					setCookie(target, sellerAdmin.testingEmail, 2); 

					target = sellerAdmin.getActiveName(which)+'SellerPage';
					setCookie(target, sellerAdmin.curSellerPage, 2); 

					target = sellerAdmin.getActiveName(which)+'Option';
					setCookie(target, sellerAdmin.option ? "Active" : "Inactive", 2); 
					var option = sellerAdmin.lastOption.attr('id');
					target += "Which";
					setCookie(target, option);
					console.log("setCookies for "+sellerAdmin.getActiveName(which)+" - option:"+(sellerAdmin.option ? "Active" : "Inactive")+", which:"+option);

					target = sellerAdmin.getActiveName(which)+'OnlyActive';
					setCookie(target, sellerAdmin.onlyActiveSellers, 2);

					target = sellerAdmin.getActiveName(which)+'EmailDbFlags';
					setCookie(target, sellerAdmin.emailDbFlags, 2);

					target = sellerAdmin.getActiveName(which)+'MailChimpCampaign';
					setCookie(target, sellerAdmin.mailchimpCampaign, 2);

					target = sellerAdmin.getActiveName(which)+'First';
					setCookie(target, $('#optionChooser #firstName').val(), 2);
					target = sellerAdmin.getActiveName(which)+'Last';
					setCookie(target, $('#optionChooser #lastName').val(), 2);
					target = sellerAdmin.getActiveName(which)+'Email';
					setCookie(target, $('#optionChooser #email').val(), 2);

					target = sellerAdmin.getActiveName(which)+'Zip';
					setCookie(target, $('#optionChooser #zip').val(), 2);
					target = sellerAdmin.getActiveName(which)+'City';
					setCookie(target, $('#optionChooser #city').val(), 2);
					target = sellerAdmin.getActiveName(which)+'State';
					setCookie(target, $('#optionChooser #state').val(), 2); 
					target = sellerAdmin.getActiveName(which)+'Distance';
					setCookie(target, sellerAdmin.distance, 2); 

					target = sellerAdmin.getActiveName(which)+'Min';
					setCookie(target, $('#optionChooser #from-id').val(), 2);
					target = sellerAdmin.getActiveName(which)+'Max';
					setCookie(target, $('#optionChooser #to-id').val(), 2);
					target = sellerAdmin.getActiveName(which)+'IDSpecifier';
					setCookie(target, $('#optionChooser #id-specifier').val(), 2); 
					target = sellerAdmin.getActiveName(which)+'IDSelector';
					setCookie(target, sellerAdmin.idSpecifier, 2); 

					target = sellerAdmin.getActiveName(which)+'IdSort';
					setCookie(target, sellerAdmin.idSort, 2);
					target = sellerAdmin.getActiveName(which)+'PriceSort';
					setCookie(target, sellerAdmin.priceSort, 2);
					target = sellerAdmin.getActiveName(which)+'AuthorIdSort';
					setCookie(target, sellerAdmin.authorIdSort, 2);
					target = sellerAdmin.getActiveName(which)+'CitySort';
					setCookie(target, sellerAdmin.citySort, 2);
					target = sellerAdmin.getActiveName(which)+'StateSort';
					setCookie(target, sellerAdmin.stateSort, 2);
					target = sellerAdmin.getActiveName(which)+'ZipSort';
					setCookie(target, sellerAdmin.zipSort, 2);
					target = sellerAdmin.getActiveName(which)+'EmailSort';
					setCookie(target, sellerAdmin.emailSort, 2);

					target = sellerAdmin.getActiveName(which)+'SellerAuthorId';
					setCookie(target, sellerAdmin.curSellerAuthorId, 2);
					
					break;
			}
		}

		this.getCookie = function(target) {
			var retval = ''
			if (!checkCookie(target)) {
				setCookie(target, '', 2);
			}
			else {
				retval = getCookie(target);
				console.log("Got value for "+target+" is "+retval);
			}
			return retval;
		}


		this.getNumCookie = function(target) {
			if (!checkCookie(target)) {
				//sellerAdmin.curPage = 0;
				setCookie(target, 0, 2);
				return 0;
			}
			else {
				var cookie = getCookie(target);
				var value = cookie == '' || cookie == "NaN" ? '0' : cookie;
				// sellerAdmin.curPage = parseInt(value);
				console.log("Got page for "+target+" is "+value);
				return value;
				// $('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));
				// $( "#pageSlider" ).slider('value', sellerAdmin.curPage);
			}
		}

		this.getStrCookie = function(target, element){
			if (!checkCookie(target)) {			
				$(element).val("");		
				setCookie(target, "", 2);
			}
			else {
				var value = getCookie(target);
				$(element).val(value);
				console.log("Got for "+target+" is "+value);
			}
		}

		this.getCookies = function(which) {
			var target = sellerAdmin.getActiveName(which)+'Page';
			var value = '';
			switch (sellerAdmin.activitySelected) {
				case ActivityState.INVITATION:
				case ActivityState.IMPROVEMENT:
				case ActivityState.MAIL:
					value = sellerAdmin.getNumCookie(target);
					sellerAdmin.curPage = parseInt(value);
					$('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));
					$( "#pageSlider" ).slider('value', sellerAdmin.curPage);

					target = sellerAdmin.getActiveName(which)+'MailChimp';
					value = sellerAdmin.getNumCookie(target);
					$('#activityChooser #mailchimp').prop('checked', parseInt(value) );
					sellerAdmin.mailchimp = parseInt(value);

					target = sellerAdmin.getActiveName(which)+'TestingEmail';
					value = sellerAdmin.getNumCookie(target);
					$('#activityChooser #test-run').prop('checked', parseInt(value) );
					sellerAdmin.testingEmail = parseInt(value);

					target = sellerAdmin.getActiveName(which)+'SellerPage';
					value = sellerAdmin.getNumCookie(target);
					sellerAdmin.curSellerPage = parseInt(value);
					$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));
					$( "#pageSliderSeller" ).slider('value', sellerAdmin.curSellerPage);

					target = sellerAdmin.getActiveName(which)+'EmailDbFlags';
					value = parseInt(sellerAdmin.getNumCookie(target));
					for(var i = 0; i <= ListingState.NONE; i++) {
						if ( (value & (1 << (20+i))) != 0 )
							$("#sellerSelection input[type='checkbox'][for="+i+"]").prop('checked', true);
						else
							$("#sellerSelection input[type='checkbox'][for="+i+"]").prop('checked', false);
					}
					sellerAdmin.emailDbFlags = value;

					target = sellerAdmin.getActiveName(which)+'MailChimpCampaign';
					value = sellerAdmin.getCookie(target);
					if (value && value.length) {
						sellerAdmin.mailchimpCampaign = value;
						$('#activityChooser #mail-chimp option[value="'+sellerAdmin.mailchimpCampaign+'"]').prop('selected', true);
					}

					target = sellerAdmin.getActiveName(which)+'Option';
					value = sellerAdmin.getCookie(target);
					sellerAdmin.option = (value && value == 'Active') ? 1 : 0;
					$("#optionChooser .optionActive").prop("checked", sellerAdmin.option ? true : false);

					target = sellerAdmin.getActiveName(which)+'OnlyActive';
					value = sellerAdmin.getCookie(target);
					sellerAdmin.onlyActiveSellers = (value ? parseInt(value) : 0);
					$('#sellerSelection input[name="sellers"][for="'+sellerAdmin.onlyActiveSellers+'"]' ).prop('checked', true);

					target += "Which";
					value = sellerAdmin.getCookie(target);
					if ( value && value == 'address' ) {
						sellerAdmin.lastOption = $("#optionChooser #address");
						$('#sellerSingle').css('display','none');
					}
					else if (value && value == 'name') {
						sellerAdmin.lastOption = $("#optionChooser #name");
						$('#sellerSingle').css('display','visible');
					}
					else {
						sellerAdmin.lastOption = $("#optionChooser #ids");
						$('#sellerSingle').css('display','none');
					}

					console.log("getCookies for "+sellerAdmin.getActiveName(which)+" - option:"+sellerAdmin.option+", which:"+value);
					sellerAdmin.lastOption.prop('checked', true);

					sellerAdmin.setOptions();
					
					target = sellerAdmin.getActiveName(which)+'First';
					sellerAdmin.getStrCookie(target, '#optionChooser #firstName');

					target = sellerAdmin.getActiveName(which)+'Last';
					sellerAdmin.getStrCookie(target, '#optionChooser #lastName');
					
					target = sellerAdmin.getActiveName(which)+'Email';
					sellerAdmin.getStrCookie(target, '#optionChooser #email');

					target = sellerAdmin.getActiveName(which)+'Zip';
					sellerAdmin.getStrCookie(target, '#optionChooser #zip');

					target = sellerAdmin.getActiveName(which)+'City';
					sellerAdmin.getStrCookie(target, '#optionChooser #city');

					target = sellerAdmin.getActiveName(which)+'State';
					sellerAdmin.getStrCookie(target, '#optionChooser #state');

					target = sellerAdmin.getActiveName(which)+'Distance';
					value = sellerAdmin.getCookie(target);
					value = value ? value : "0";
					$('#optionChooser #addressOption #distance option[value="'+value+'"]').prop('selected', true);
					sellerAdmin.distance = value;

					target = sellerAdmin.getActiveName(which)+'Min';
					sellerAdmin.getStrCookie(target, '#optionChooser #from-id');

					target = sellerAdmin.getActiveName(which)+'Max';
					sellerAdmin.getStrCookie(target, '#optionChooser #to-id');

					target = sellerAdmin.getActiveName(which)+'IDSpecifier';
					sellerAdmin.getStrCookie(target, '#optionChooser #id-specifier');

					target = sellerAdmin.getActiveName(which)+'IDSelector';
					sellerAdmin.idSpecifier = sellerAdmin.getCookie(target);
					if (!sellerAdmin.idSpecifier) sellerAdmin.idSpecifier = "range";
					console.log("getCookies - idSpecifier:"+sellerAdmin.idSpecifier);
					var ele = $("#optionChooser #"+sellerAdmin.idSpecifier);
					ele.prop('checked', true);


					target = sellerAdmin.getActiveName(which)+'IdSort';
					sellerAdmin.idSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.idSort !== 0) {
						sellerAdmin.idSort = !sellerAdmin.idSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortIdArrow();
					}
					target = sellerAdmin.getActiveName(which)+'PriceSort';
					sellerAdmin.priceSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.priceSort !== 0) {
						sellerAdmin.priceSort = !sellerAdmin.priceSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortPriceArrow();
					}
					target = sellerAdmin.getActiveName(which)+'AuthorIdSort';
					sellerAdmin.authorIdSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.authorIdSort !== 0) {
						sellerAdmin.authorIdSort = !sellerAdmin.authorIdSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortAuthorIdArrow();
					}
					target = sellerAdmin.getActiveName(which)+'CitySort';
					sellerAdmin.citySort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.citySort !== 0) {
						sellerAdmin.citySort = !sellerAdmin.citySort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortCityArrow();
					}
					target = sellerAdmin.getActiveName(which)+'StateSort';
					sellerAdmin.stateSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.stateSort !== 0) {
						sellerAdmin.stateSort = !sellerAdmin.stateSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortStateArrow();
					}
					target = sellerAdmin.getActiveName(which)+'ZipSort';
					sellerAdmin.zipSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.zipSort !== 0) {
						sellerAdmin.zipSort = !sellerAdmin.zipSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortZipArrow();
					}
					target = sellerAdmin.getActiveName(which)+'EmailSort';
					sellerAdmin.emailSort = sellerAdmin.setOrderValue( sellerAdmin.getCookie(target) );
					if (sellerAdmin.emailSort !== 0) {
						sellerAdmin.emailSort = !sellerAdmin.emailSort; // will revert correctly in setSortXXArrow()
						sellerAdmin.setSortEmailArrow();
					}

					target = sellerAdmin.getActiveName(which)+'SellerAuthorId';
					sellerAdmin.curSellerAuthorId = sellerAdmin.getCookie(target) == '' ? 0 : parseInt(sellerAdmin.getCookie(target));
		        	// $('#priceValue').html("Current min price: "+sellerAdmin.price);
		        	// $( "#priceSlider" ).slider('value', sellerAdmin.price);
					break;
			}
		}

		this.setOrderValue = function(value) {
			switch(value) {
				case '0': return 0;
				case 'true': return true;
				case 'false': return false;
				default: return 0;
			}
		}

		this.getSetPageCookies = function(oldSelection)
		{
			sellerAdmin.setCookies(oldSelection);
			sellerAdmin.getCookies(sellerAdmin.activitySelected);
		}

		this.setRowCountCookies = function(oldSelection)
		{
			var old = sellerAdmin.getActiveName(oldSelection)+'RowCount';
			setCookie(old, sellerAdmin.rowCount, 2);
		}

		this.setSliderSeller = function() {
			if (!sellerAdmin.gotSellerCount) {
				window.setTimeout(function(){
					sellerAdmin.setSliderSeller();
				}, 100);
				return;
			}

			$('#sliderValueSeller').html("Current Page: "+(sellerAdmin.curSellerPage+1));

			if ( (sellerAdmin.sellerCount / sellerAdmin.pageSize) > 2) {
				console.log("Max Seller page is "+(sellerAdmin.sellerCount / sellerAdmin.pageSize));
				var maxi = Math.floor(sellerAdmin.sellerCount / sellerAdmin.pageSize) - 1;
				maxi = (sellerAdmin.sellerCount % sellerAdmin.pageSize) ? maxi + 1 : maxi;
				$( "#pageSliderSeller" ).slider('option', 'max', maxi);
				$( "#pageSliderSeller" ).slider('value', sellerAdmin.curSellerPage);	
				$("#pageSliderSeller").show();			
			}
			else
				$("#pageSliderSeller").hide();
		}

		this.setSlider = function() {
			if (!sellerAdmin.gotRowCount) {
				window.setTimeout(function(){
					sellerAdmin.setSlider();
				}, 100);
				return;
			}

			$('#sliderValue').html("Current Page: "+(sellerAdmin.curPage+1));

			if ( (sellerAdmin.rowCount / sellerAdmin.pageSize) > 2) {
				console.log("Max page is "+(sellerAdmin.rowCount / sellerAdmin.pageSize));
				var maxi = Math.floor(sellerAdmin.rowCount / sellerAdmin.pageSize) - 1;
				maxi = (sellerAdmin.rowCount % sellerAdmin.pageSize) ? maxi + 1 : maxi;
				$( "#pageSlider" ).slider('option', 'max', maxi);
				$( "#pageSlider" ).slider('value', sellerAdmin.curPage);	
				$("#pageSlider").show();			
			}
			else
				$("#pageSlider").hide();
		}

		this.resetSortArrows = function(current) {
			var arr = ['.column-id','.column-price','.column-city','.column-state','.column-zip','.column-author-id','.column-email'];
			var id = ['idSort','priceSort','citySort','startSort','zipSort','authorIdSort','emailSort' ];
			for(var i in arr) {
				if (arr[i] == current)
					continue;
				if ($('.sellerList '+arr[i]).hasClass("entypo-up-open"))
					$('.sellerList '+arr[i]).removeClass("entypo-up-open");
				if ($('.sellerList '+arr[i]).hasClass("entypo-down-open"))
					$('.sellerList '+arr[i]).removeClass("entypo-down-open");
				sellerAdmin[id[i]] = 0;
			}
		}

		this.setSortIdArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.idSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.idSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.idSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.column-id').removeClass(removeClass);
			$('.column-id').addClass(myClass);

			sellerAdmin.idSort = !sellerAdmin.idSort;
			sellerAdmin.resetSortArrows('.column-id');
			// if ($('.column-price').hasClass("entypo-up-open"))
			// 	$('.column-price').removeClass("entypo-up-open");
			// if ($('.column-price').hasClass("entypo-down-open"))
			// 	$('.column-price').removeClass("entypo-down-open");
			// sellerAdmin.priceSort = 0;
		}
		this.sortId = function() {
			console.log ("sorId called cur dir: ", sellerAdmin.idSort);
			sellerAdmin.setSortIdArrow();
			// sellerAdmin.getPage();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.setSortPriceArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.priceSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.priceSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.priceSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.column-price').removeClass(removeClass);
			$('.column-price').addClass(myClass);
			sellerAdmin.priceSort = !sellerAdmin.priceSort;

			sellerAdmin.resetSortArrows('.column-price');
			// if ($('.column-listing-id').hasClass("entypo-up-open"))
			// 	$('.column-listing-id').removeClass("entypo-up-open");
			// if ($('.column-listing-id').hasClass("entypo-down-open"))
			// 	$('.column-listing-id').removeClass("entypo-down-open");
			// sellerAdmin.idSort = 0;
		}
		this.sortPrice = function() {
			console.log ("sortPrice called cur dir: ", sellerAdmin.priceSort);
			sellerAdmin.setSortPriceArrow();
			// sellerAdmin.getPage();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.setSortAuthorIdArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.authorIdSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.authorIdSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.authorIdSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.sellerList .column-author-id').removeClass(removeClass);
			$('.sellerList .column-author-id').addClass(myClass);

			sellerAdmin.authorIdSort = !sellerAdmin.authorIdSort;
			sellerAdmin.resetSortArrows('.column-author-id');
			// if ($('.sellerList .column-city').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-up-open");
			// if ($('.sellerList .column-city').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-down-open");
			// sellerAdmin.citySort = 0;
			// if ($('.sellerList .column-state').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-up-open");
			// if ($('.sellerList .column-state').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-down-open");
			// sellerAdmin.stateSort = 0;
			// if ($('.sellerList .column-zip').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-up-open");
			// if ($('.sellerList .column-zip').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-down-open");
			// sellerAdmin.zipSort = 0;
		}
		this.sortAuthorId = function() {
			console.log ("sortCity called cur dir: ", sellerAdmin.authorIdSort);
			sellerAdmin.setSortAuthorIdArrow();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}


		this.setSortCityArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.citySort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.citySort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.citySort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.sellerList .column-city').removeClass(removeClass);
			$('.sellerList .column-city').addClass(myClass);

			sellerAdmin.citySort = !sellerAdmin.citySort;
			sellerAdmin.resetSortArrows('.column-city');
			// if ($('.sellerList .column-author-id').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-up-open");
			// if ($('.sellerList .column-author-id').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-down-open");
			// sellerAdmin.authorIdSort = 0;
			// if ($('.sellerList .column-state').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-up-open");
			// if ($('.sellerList .column-state').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-down-open");
			// sellerAdmin.stateSort = 0;
			// if ($('.sellerList .column-zip').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-up-open");
			// if ($('.sellerList .column-zip').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-down-open");
			// sellerAdmin.zipSort = 0;
		}
		this.sortCity = function() {
			console.log ("sortCity called cur dir: ", sellerAdmin.citySort);
			sellerAdmin.setSortCityArrow();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.setSortStateArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.stateSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.stateSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.stateSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.sellerList .column-state').removeClass(removeClass);
			$('.sellerList .column-state').addClass(myClass);

			sellerAdmin.stateSort = !sellerAdmin.stateSort;
			sellerAdmin.resetSortArrows('.column-state');
			// if ($('.sellerList .column-author-id').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-up-open");
			// if ($('.sellerList .column-author-id').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-down-open");
			// sellerAdmin.authorIdSort = 0;
			// if ($('.sellerList .column-city').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-up-open");
			// if ($('.sellerList .column-city').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-down-open");
			// sellerAdmin.citySort = 0;
			// if ($('.sellerList .column-zip').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-up-open");
			// if ($('.sellerList .column-zip').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-zip').removeClass("entypo-down-open");
			// sellerAdmin.zipSort = 0;
		}
		this.sortState = function() {
			console.log ("sortCity called cur dir: ", sellerAdmin.stateSort);
			sellerAdmin.setSortStateArrow();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.setSortZipArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.zipSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.zipSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.zipSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.sellerList .column-zip').removeClass(removeClass);
			$('.sellerList .column-zip').addClass(myClass);

			sellerAdmin.zipSort = !sellerAdmin.zipSort;
			sellerAdmin.resetSortArrows('.column-zip');
			// if ($('.sellerList .column-author-id').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-up-open");
			// if ($('.sellerList .column-author-id').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-author-id').removeClass("entypo-down-open");
			// sellerAdmin.authorIdSort = 0;
			// if ($('.sellerList .column-state').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-up-open");
			// if ($('.sellerList .column-state').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-state').removeClass("entypo-down-open");
			// sellerAdmin.stateSort = 0;
			// if ($('.sellerList .column-city').hasClass("entypo-up-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-up-open");
			// if ($('.sellerList .column-city').hasClass("entypo-down-open"))
			// 	$('.sellerList .column-city').removeClass("entypo-down-open");
			// sellerAdmin.citySort = 0;
		}
		this.sortZip = function() {
			console.log ("sortZip called cur dir: ", sellerAdmin.zipSort);
			sellerAdmin.setSortZipArrow();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.setSortEmailArrow = function() {
			removeClass = "entypo-down-open"
			if (sellerAdmin.emailSort === 0) {// first time
				myClass = "entypo-down-open";
			}
			else {
				myClass = sellerAdmin.emailSort === true ? "entypo-up-open" : "entypo-down-open" ;
				removeClass = sellerAdmin.emailSort === true ? "entypo-down-open" : "entypo-up-open" ;
			}
			$('.sellerList .column-email').removeClass(removeClass);
			$('.sellerList .column-email').addClass(myClass);

			sellerAdmin.emailSort = !sellerAdmin.emailSort;
			sellerAdmin.resetSortArrows('.column-email');
		}

		this.sortEmail = function() {
			console.log ("sortEmail called cur dir: ", sellerAdmin.emailSort);
			sellerAdmin.setSortEmailArrow();
			sellerAdmin.getSellerPage(sellerAdmin.curSellerPage);
		}

		this.getPage = function() {
			// sellerAdmin.getSellerPage(sellerAdmin.curPage);
			// return;

			if (typeof sellerAdmin.listings == 'object')
				sellerAdmin.listings.splice(0, sellerAdmin.listings.length); // clear out
			
			$('#optionWrapper .spinner').fadeIn(250, function(){});
			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: { query: 'getpage',
						data: {	page: sellerAdmin.curPage,
								perPage: sellerAdmin.pageSize,
								id: sellerAdmin.curSellerAuthorId,
								sortid: sellerAdmin.idSort,
								sortprice: sellerAdmin.priceSort  }
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getPage: "+msg);
					sellerAdmin.getPage(page); // try again
					$('#optionWrapper .spinner').fadeOut(250, function(){});
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status != 'OK') {
				  		var msg = 'Failed to get any page of data';
				  		if (data.data != null)
				  			msg += ': '+data.data;
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+msg+'</p>' });
				  	}
				  	else {
				  		if (data.data) {
					  		sellerAdmin.listings = data.data;
					  	}
						sellerAdmin.showListings();
	  				}
		  		}
	  		})
		}

		this.showListings = function() {
			$('#controls #applyChanges').prop('disabled', true);
			var body = '';
			for(var x = 0; typeof sellerAdmin.listings == 'object' && x < sellerAdmin.listings.length; x++) {
				body += '<tr';
				if ( (x % 2) == 1)
					body += " class='alternate'";
				body += '>';
				body += "<td style='width: 5%'>"+sellerAdmin.listings[x].id+"</td>";
				body += "<td style='width: 8%'>"+sellerAdmin.listings[x].price+"</td>";
				body += "<td style='width: 5%'>"+sellerAdmin.listings[x].beds+"/"+sellerAdmin.listings[x].baths+"</td>";
				var err = '';
				for(var j = 0; j < sellerAdmin.errorMax; j++) {
					if (sellerAdmin.listings[x].error & (1 << j)) {
						if (err.length)
							err += ",";
						err += j+1;
					}
				}
				body += "<td style='width: 5%'>"+err+"</td>";
				body += "<td style='width: 5%'>"+sellerAdmin.listings[x].tagCount+"</td>";
				// if (!sellerAdmin.listings[x].images.isArray() &&
				// 	typeof sellerAdmin.listings[x].images == 'string')
				// 	sellerAdmin.listings[x].images = JSON.parse(sellerAdmin.listings[x].images);

				var imgCount = 0;
				if (sellerAdmin.listings[x].images != 'undefined' &&
					sellerAdmin.listings[x].images != null &&
					sellerAdmin.listings[x].images.length) {
					var galleryName = 'gallery'+x;
					body += "<td style='width: 220px'><ul class='listing-"+galleryName+"'>";
					var item = '';
					for(var i = 0; i < 6 && i < sellerAdmin.listings[x].images.length; i++) {
						if (typeof sellerAdmin.listings[x].images[i].file != 'undefined' &&
							sellerAdmin.listings[x].images[i].file != null) {
							if (sellerAdmin.listings[x].images[i].file.indexOf("http") != -1) {
								if (sellerAdmin.listings[x].images.length == 1) {
									item = "<li class='listing-image-single' style='width: 210px; height: 120px; margin-left: 40px'>";
											//"<div style='display: inline;'><img src='"+sellerAdmin.listings[x].images[i].file+"' style='width: 210px; height: 120px'/>";
									if (sellerAdmin.listings[x].images[i].width != 'undefined' && sellerAdmin.listings[x].images[i].width != null) {
										item += '<span>('+sellerAdmin.listings[x].images[i].width+'x'+sellerAdmin.listings[x].images[i].height+')</span>';
									}
									item += "<img src='"+sellerAdmin.listings[x].images[i].file+"' style='width: 210px; height: 120px'/>";
									//body += "</div></li>"; style="position: absolute; right: 15px; top: -15px; z-index: 10;"
									item += "</li>";
									body += item;
								}
								else {
									item = "<li class='listing-image'>";
											//"<div style='display: inline;'><img src='"+sellerAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
									if (sellerAdmin.listings[x].images[i].width != 'undefined' && sellerAdmin.listings[x].images[i].width != null) {
										item += '<span >('+sellerAdmin.listings[x].images[i].width+'x'+sellerAdmin.listings[x].images[i].height+')</span>';
									} // style="position: absolute; right: 15px; top: -15px; z-index: 10;"
									item += "<img src='"+sellerAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
									//body += "</div></li>";
									item += "</li>";
									body += item;
								}
							}
							else {
								item = "<li class='listing-image'>";
										//"<div style='display: inline;'><img src='"+ah_local.tp+"/_img/_listings/210x120/"+sellerAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
								if (sellerAdmin.listings[x].images[i].width != 'undefined' && sellerAdmin.listings[x].images[i].width != null) {
									item += '<span >('+sellerAdmin.listings[x].images[i].width+'x'+sellerAdmin.listings[x].images[i].height+')</span>';
								}		// style="position: absolute; right: 15px; top: -15px; z-index: 10;"
								item += "<img src='"+ah_local.tp+"/_img/_listings/210x120/"+sellerAdmin.listings[x].images[i].file+"' style='width: 100%; height: 100%'/>";
								//body += "</div></li>";
								item += "</li>";
								body += item;
							}
							imgCount++;
						} else if (typeof sellerAdmin.listings[x].images[i].discard != 'undefined' &&
								   sellerAdmin.listings[x].images[i].discard != null ) {
							if (sellerAdmin.listings[x].images[i].discard.indexOf("http") != -1) {
								if (sellerAdmin.listings[x].images.length == 1)
									body += "<li class='listing-image-single' style='width: 210px; height: 120px; margin-left: 10px'>"+                                      
											"<span>DISCARD</span><img src='"+sellerAdmin.listings[x].images[i].discard+"' style='width: 210px; height: 120px'/></li>";
								else
									body += "<li class='listing-image'>"+																									
											"<span>DISCARD</span><img src='"+sellerAdmin.listings[x].images[i].discard+"' style='width: 100%; height: 100%'/></li>";
							}
							else
								body += "<li class='listing-image'>"+																																		
										"<span>DISCARD</span><img src='"+ah_local.tp+"/_img/_listings/210x120/"+sellerAdmin.listings[x].images[i].discard+"' style='width: 100%; height: 100%'/></span></li>";
							imgCount++;
						} 

					}
					body += "</ul></td>";				
				}
				if (imgCount == 0)
					body += "<td style='padding-left: 180px'>No Image</td>";
				//body += "<td><a href="+ah_local.wp+"/listing/1-"+sellerAdmin.listings[x].id+" target='_blank'>"+sellerAdmin.listings[x].title+"</a></td>";
				body += '<td><a href="javascript:sellerAdmin.gotoListing('+sellerAdmin.listings[x].id+')" target="_blank">'+sellerAdmin.listings[x].title+"</a></td>";
				body += '<td>'+ActiveListingNames[ sellerAdmin.listings[x].active ]+'</td>';
				body += "<td>";
				var optCount = 0;
				var index = sellerAdmin.listings[x].active != null && sellerAdmin.listings[x].active >= 0 && sellerAdmin.listings[x].active < ButtonOptions.length ? sellerAdmin.listings[x].active : 2;
				if (ButtonOptions[index].length) {					
					for(j = 0; j < ButtonOptions[index].length; j++) {
						var name = ActiveListingNames[ ButtonOptions[index][j] ];
						if (j > 0)
							body += "/";
						body += "<a href=javascript:sellerAdmin.transfer("+sellerAdmin.listings[x].id+","+ButtonOptions[index][j]+")>"+name+"</a>"+
								'<span> <span><input type="radio" name="active'+sellerAdmin.listings[x].id+'" id="'+ActiveListingNames[ButtonOptions[index][j]]+'-'+sellerAdmin.listings[x].id+'">';
					}
					body += "</td>"
				}
				else
					body += "No Option</td>";
				//body += "<td><a href=javascript:sellerAdmin.transfer("+sellerAdmin.listings[x].id+")>Transfer</a></td>"
				body += "</tr>";
			}
			var table = $('.listings tbody');
			table.html(body);

			var slider;

			$('.widefat .listings input[type="radio"]').on('change',function() {
				if ($(this).prop('checked'))
					$('#controls #applyChanges').prop('disabled', false);
			});

			$('.controls #applyChanges').on('click', function() {
				sellerAdmin.transferAll();
			})

			// $('.listing-image-single').hover(
			// 	function() {
			// 		console.log("entered hover");
			// 		$(this).animate({
			// 			width: "800px",
			// 			height: "640px"
			// 		}, 100, function(){});
			// 		$(this).children().animate({
			// 			width: "800px",
			// 			height: "640px"
			// 		}, 100, function(){});
			// 	},
			// 	function() {
			// 		console.log("leave hover");
			// 		$(this).animate({
			// 			width: "210px",
			// 			height: "120px"
			// 		}, 100, function(){});
			// 		$(this).children().animate({
			// 			width: "210px",
			// 			height: "120px"
			// 		}, 100, function(){});
			// 	}
			// );
			
			for(var x = 0; x < sellerAdmin.listings.length; x++) {
				var slider;
				var targetName = 'ul.listing-gallery'+x;
			//$.each('ul.listing-gallery',function() {
				if ($(targetName).children('li').length > 1){
					slider = $(targetName).bxSlider({
					  auto: true,
					  pause: 3000,
					  mode: 'fade',
					  responsive: 'false',
					  controls: 'false',
					  autoHover: 'true' //,
					  // onSlideAfter: function($element, oldIndex, newIndex) {
					  // 		$element.hover(
							// 	function() {
							// 		console.log("entered hover:"+newIndex);
							// 		sellerAdmin.animate = true;
							// 		element = $(this);
							// 		window.setTimeout(function() {
							// 			sellerAdmin.expand(element);
							// 		}, 200);
							// 	},
							// 	function() {
							// 		console.log("leave hover:"+newIndex);
							// 		sellerAdmin.animate = false;
							// 		$(this).parent().parent().parent().animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 		$(this).parent().parent().animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 		$(this).animate({
							// 			width: "210px",
							// 			height: "120px"
							// 		}, 100, function(){});
							// 	})
					  // }
					});
				}
			}
		}

		this.showSellers = function() {
			var body = '';
			for(var x in sellerAdmin.authors) {
				body += '<tr';
				if ( (x % 2) == 1)
					body += " class='alternate'";
				body += '>';
				var authorId = sellerAdmin.authors[x].author_id == null || sellerAdmin.authors[x].author_id == 'undefined' ? "N/A" : sellerAdmin.authors[x].author_id;
				//if (sellerAdmin.activitySelected)
				body += '<td ><input type="checkbox" id="select" data="'+sellerAdmin.authors[x].id+'""></td>'
				body += "<td >"+sellerAdmin.authors[x].id+"</td>";
				body += "<td >"+authorId+"</td>";
				body += "<td >"+sellerAdmin.authors[x].first_name+"</td>";
				body += "<td >"+sellerAdmin.authors[x].last_name+"</td>";
				body += "<td >"+sellerAdmin.authors[x].email+"</td>";
				body += "<td >"+sellerAdmin.authors[x].city+"</td>";
				body += "<td >"+sellerAdmin.authors[x].state+"</td>";
				body += "<td >"+sellerAdmin.authors[x].zip+"</td>";
				// begin invite codes
				body += '<td><select name="invitecodes" class="codes" id="codes-'+sellerAdmin.authors[x].id+'" data="'+sellerAdmin.authors[x].id+'">';
				for(var c in sellerAdmin.inviteList) {
			  		body += '<option value="'+sellerAdmin.inviteList[c].code+'" owner="'+(sellerAdmin.inviteList[c].seller_id ? sellerAdmin.inviteList[c].seller_id[0]: "N/A")+'">'+sellerAdmin.inviteList[c].code+' - owned by: '+sellerAdmin.inviteList[c].seller_id+'</option>';
			  	}
				body+= '</select></td>';
				// end invite codes
				// begin improvements
				var countImprovementMeta = 0;
				var seller = sellerAdmin.authors[x];
				if (seller.meta != null) {
					for(var i in seller.meta) {
			          if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS)
			            countImprovementMeta++;
			    	}
			    	if (countImprovementMeta) {
						body += '<td><select name="improvements" class="improvements" id="improvements-'+sellerAdmin.authors[x].id+'" data="'+sellerAdmin.authors[x].id+'">';
						for(var i in seller.meta) {
			          		if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS) {
			          			body += '<option>'+seller.meta[i].listhub_key+' :';
			          			var k = 0;
			          			for(var j in seller.meta[i]) {
                					if (j != 'listhub_key' &&
                						j != 'action') {
                						if (k)
                							body += ',';
                						body += j;
                						k++;
                					}
                				}
			          			body += '</option>';
			          		}
			          	}
						body+= '</select></td>';
					}
					else
						body += '<td>None found</td>';
				}
				else
					body += '<td>None found</td>';
				// end improvements
				body += '<td><button name="assign" id="assign" data="'+sellerAdmin.authors[x].id+'">Assign</button></td>';
				if (!countImprovementMeta &&
					!(sellerAdmin.authors[x].author_id && sellerAdmin.authors[x].author_id <= 8) &&
					seller.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME))
					body += '<td>All good</td>';
				else {
					body += '<td>';
					if (countImprovementMeta)
						body += '<button name="improve" id="improve" data="'+sellerAdmin.authors[x].id+'">Improve It Email</button>';
					if ( !(seller.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) ||
						  (sellerAdmin.authors[x].author_id && sellerAdmin.authors[x].author_id <= 8) )
						body += '<button name="intro" id="intro" data="'+sellerAdmin.authors[x].id+'">Intro Email</button>';
					if (typeof sellerAdmin.authors[x].flags != 'undefined' &&
						 sellerAdmin.authors[x].flags != '0') {
						body += '<button name="clear" id="clear" data="'+sellerAdmin.authors[x].id+'">Clear Email Flags</button>';
						if ( (sellerAdmin.authors[x].flags & SellerEmailDbFlags.EMAIL_UNSUBSCRIBED) == 0)
							body += '<button name="unsubscribe" id="unsubscribe" data="'+sellerAdmin.authors[x].id+'">Unsubscribe</button>';
					}
					body += '</td>';
				}
				
				// var types = '';
				// for(var t in sellerAdmin.authors[x].listingTypes)
				// 	switch(parseInt(t)) {
				// 		case ListingState.INACTIVE: types += "I:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.ACTIVE: types += "A:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.WAITING: types += "W:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.REJECTED: types += "R:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.TOOCHEAP: types += "C:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.NEVER: types += "N:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 		case ListingState.AVERAGE: types += "Avg:"+sellerAdmin.authors[x].listingTypes[t]+" "; break;
				// 	}
				// body += '<td>'+types+'</td>';

			  	var author_id = sellerAdmin.authors[x].author_id == null || sellerAdmin.authors[x].author_id == 'undefined' ? sellerAdmin.authors[x].id : sellerAdmin.authors[x].author_id;
				// body += '<td><span>Count: '+(sellerAdmin.authors[x].listingCount ? sellerAdmin.authors[x].listingCount : 0)+'  </span><button name="viewButton" id="view" data="'+author_id+'"';
				// if (sellerAdmin.authors[x].listingCount == null || sellerAdmin.authors[x].listingCount == 0)
				// 	body += 'disabled="true"';
				// body += '>View</button></td>';
				// body += '</tr>';
				sellerAdmin.getSellerListingStat(sellerAdmin.authors[x].id, x, author_id);
			}

			$('.sellerList tbody').html(body);
			
			for(var x in sellerAdmin.authors) {
				var match = '';
				for(var c in sellerAdmin.inviteList) {
					// if (sellerAdmin.inviteList[c].seller_id == sellerAdmin.authors[x].id)
					if (sellerAdmin.inviteList[c].seller_id &&
					 	sellerAdmin.inviteList[c].seller_id.indexOf(sellerAdmin.authors[x].id) != -1)
			  			match = sellerAdmin.inviteList[c].code;
				}
				if (match != '') {
					var id = '#codes-'+sellerAdmin.authors[x].id;
					$('.sellerList '+id+' option[value="'+match+'"').prop('selected', true);
				}

				var seller = sellerAdmin.authors[x];
				var id = '#improve-'+seller.id;
				if (seller.meta != null) {
					var countImprovementMeta = 0;
					for(var i in seller.meta) {
			          if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS)
			            countImprovementMeta++;
			    	}
					$('.sellerList '+id).prop('disabled', countImprovementMeta ? false : true);
				}
				else
					$('.sellerList '+id).prop('disabled', true);
			}

			$('.sellerListDiv #selectAll').on('change', function() {
				var val = $(this).prop('checked');
				console.log("Select All clicked: "+val);
				$('.sellerListDiv #sendImproveEmailAll').prop('disabled', !val);
				$('.sellerListDiv #sendIntroEmailAll').prop('disabled', !val);
				$.each( $('.sellerList #select'), function() {
					var val = $('.sellerListDiv #selectAll').prop('checked');
					console.log('set select to '+val);
					$(this).prop('checked', val);
				})
			})

			$('.sellerList tbody #select').change(function() {
				var val = $(this).prop('checked');
				var id = $(this).attr('data');
				console.log("Id:"+id+" is now checked:"+val);

				if (val == false) {
					$('.sellerListDiv #sendImproveEmailAll').prop('disabled', !val);
					$('.sellerListDiv #sendIntroEmailAll').prop('disabled', !val);
					$('.sellerListDiv #selectAll').prop('checked', false);
				}
			});

			$('.sellerList .codes').on('change', function() {
				var val = '';
				var owner = '';
				$.each($(this).children(), function() {
					if ($(this).prop("selected")) {
						val = $(this).attr('value');
						owner = $(this).attr('owner');
					}
				});
				//var val = $("this option:selected").val();
				var id = $(this).attr('data');
				console.log("Code:"+val+", owner:"+owner+" for seller id:"+id);
				if (owner == '0') {
					$('.sellerList tbody #assign[data="'+id+'"]').prop('disabled', false);
					for(var x in sellerAdmin.authors) {
						if (sellerAdmin.authors[x].id == id) {
							sellerAdmin.authors[x].invite = val;
							break;
						}
					}
				}
				else
					$('.sellerList tbody #assign[data="'+id+'"]').prop('disabled', true);
				
			});
			$.each($('.sellerList tbody #assign'), function() {
				$(this).prop('disabled', true);
			});
			$('.sellerList tbody #assign').on('click', function() {
				var id = $(this).attr('data');
				console.log("Assign clicked for id:"+id);
				// need to get the code registered in the seller's meta
				for(var x in sellerAdmin.authors) {
					if (sellerAdmin.authors[x].id == id &&
						sellerAdmin.authors[x].invite) {
						sellerAdmin.addSellerCode(sellerAdmin.authors[x]);
						break;
					}
				}
				// need to update the codes in Options
				sellerAdmin.updateInviteCodes(sellerAdmin.authors[x]);
				// need to update the select menus of all sellers on page
				sellerAdmin.updateAllSelectMenus(sellerAdmin.authors[x]);
				// now disable this assign button again
				$(this).prop('disabled', true);
			});

			$('.sellerList tbody #improve').on('click', function() {
				var id = $(this).attr('data');
				var name = $(this).attr('name');
				console.log(name+" clicked "+name+" for id:"+id);
				sellerAdmin.sendEmail(EmailTypes.IMPROVE_LISTING, 'improve',[{id:id}]);
			});

			$('.sellerList tbody #intro').on('click', function() {
				var id = $(this).attr('data');
				var name = $(this).attr('name');
				console.log(name+" clicked "+name+" for id:"+id);
				sellerAdmin.sendEmail(EmailTypes.PROMO_FIRST, 'intro',[{id:id}]);
			});

			$('.sellerList tbody #clear').on('click', function() {
				var id = $(this).attr('data');
				var name = $(this).attr('name');
				console.log(name+" clicked "+name+" for id:"+id);
				var x = {query: 'clear-flags',
						 data: {id: id},
						 done: function(data) {
						 	console.log(data);
						 },
						 error: function(data) {
						 	console.log(data);
						 }
						};
				sellerAdmin.DB(x);
			});

			$('.sellerList tbody #unsubscribe').on('click', function() {
				var id = $(this).attr('data');
				var name = $(this).attr('name');
				console.log(name+" clicked "+name+" for id:"+id);
				var x = {query: 'unsubscribe-agent',
						 data: {id: id},
						 done: function(data) {
						 	console.log(data);
						 },
						 error: function(data) {
						 	console.log(data);
						 }
						};
				sellerAdmin.DB(x);
			});

			// $('.sellerList tbody #view').on('click', function() {
			// 	var id = $(this).attr('data');
			// 	console.log("View clicked for id:"+id);
			// 	sellerAdmin.curPage = 0;
			// 	sellerAdmin.curSellerAuthorId = id;
			// 	sellerAdmin.getRowCount();
			// });
		}

		this.getSellerPassword = function(id, author_id) {
			var x = {query: 'get-seller-password',
					 data: {id: id,
					 		author_id: author_id },
					 done: function(data) {			 
					 	ahtb.open({html: '<p>User password is: '+data.pwd+'<br/>User login is: '+data.login+'</p>',
								   width: 450,
									height: 150,
						});
					 },
					 error: function(data) {
					 	console.log(data);
					 	var msg = typeof data == 'undefined' ? "Unknown server error" : data;
					 	ahtb.alert(	msg,
					 				{height: 150});
					 } };
			sellerAdmin.DB(x);
		}

		this.updateSellerPassword = function(id, author_id) {
			var h = '<div id="updatePasswordDiv">' +
						'<span id="seller">Name:'+sellerAdmin.singleSeller.first_name+' '+sellerAdmin.singleSeller.last_name+'</span>' +
						'<span id="seller">Email:'+(typeof sellerAdmin.singleSeller.email != 'undefined' && sellerAdmin.singleSeller.email ? sellerAdmin.singleSeller.email : "no email")+'</span>' +
						'<input type="text" id="password" placeholder="Type in new password"/>' +
					'</div>';
			ahtb.open({	html: h,
					  	width: 300,
						height: 160,
						buttons: [{text:'OK', action: function() {
							var password = $('input#password').val();
							if (password.length == 0 ||
								password.indexOf('Enter a password') != -1)
								$('input#password').val("Enter a password");
							else if (password.length < 8)
								$('input#password').val("Enter a password at least 8 chars");
							else {
								var x = {query: 'update-seller-password',
										 data: {id: id,
										 		author_id: author_id,
										 		password: password },
										 done: function(data) {			 
										 	ahtb.open({html: '<p>Password updated.</p>',
													   width: 450,
														height: 150,
											});
										 },
										 error: function(data) {
										 	console.log(data);
										 	var msg = typeof data == 'undefined' ? "Unknown server error" : data;
										 	ahtb.alert(	msg,
										 				{height: 150});
										 } };
								sellerAdmin.DB(x);
							}
						}},
						{text:'Cancel', action: function() {
							ahtb.close();
						}}]});

		}

		this.expand = function(what) {
			if (sellerAdmin.animate) {
				sellerAdmin.animate = false;
			}
			what.parent().parent().parent().animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			what.parent().parent().animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			what.animate({
				width: "800px",
				height: "640px"
			}, 100, function(){});
			sellerAdmin.animate = false;
		}

		this.gotoListing = function(id) {
			if (sellerAdmin.showReminderRefresh) {
				h = '<div id="informUserToRefresh">'+
						"<p>Refresh page to update listing's info.</p>"+
						'<div id="userStopReminder" style="display: inline">'+
							"<span>Don't remind me again: </span>"+
							'<input type="checkbox" id="stopReminder" />';
				var opened = function() {
					$('#informUserToRefresh #stopReminder').prop('checked', !sellerAdmin.showReminderRefresh)
					$('#informUserToRefresh #stopReminder').on('click', function() {
					 	sellerAdmin.showReminderRefresh = $(this).prop('checked') ? 0 : 1;
					 	setCookie('ReminderRefresh', sellerAdmin.showReminderRefresh, 7);
					})
				}
		      	ahtb.open({ hideSubmit: false, height: 150, width: 300, title: 'Reminder', html: h, opened: opened });
				// ahtb.alert("Refresh page to update listing's info.",
				// 	{height: 150});
			}
			window.open(ah_local.wp+"/listing/1-"+id, '_blank');
		}

		this.getRowCount = function() {
			if (sellerAdmin.curSellerAuthorId == 0)
				return;

			sellerAdmin.gotRowCount = false;
			sellerAdmin.rowCount = 0;
			$('#optionWrapper .spinner').fadeIn(250, function(){});
			
			console.log("getRowCount - id:"+sellerAdmin.curSellerAuthorId);
			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: { query: 'row-count',
						data: {	id: sellerAdmin.curSellerAuthorId } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getRowCount: "+msg);
					sellerAdmin.gotRowCount = true;
					$('#optionWrapper .spinner').fadeOut(250, function(){});
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
				  	if (data == null || data.status == null ||
				  		typeof data == 'undefined' || typeof data.status == 'undefined') 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else if (data.status == 'fail') {
				  		ahtb.open({ title: 'No rows detected', height: 150, html: '<p>'+data.data+'</p>' });
				  	}
				  	else {
				  		sellerAdmin.rowCount = data.data ? parseInt(data.data) : 0;
				  		$('#infoBar .listing').html("Author ID: "+sellerAdmin.curSellerAuthorId+" - # of listings: "+ sellerAdmin.rowCount);
				  		console.log("RowCount for seller id:"+sellerAdmin.curSellerAuthorId+" is "+sellerAdmin.rowCount);
				  		if ( ((sellerAdmin.curPage+1)*sellerAdmin.pageSize) > sellerAdmin.rowCount)  // reset then
				  			sellerAdmin.curPage = 0;
				  		
				  		sellerAdmin.getPage();
				  		var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'SellerAuthorId';
						setCookie(target, sellerAdmin.curSellerAuthorId, 2);

					  	if (!sellerAdmin.rowCount) {
					  		var msg = 'For this agent, there appears to be no listings';
					  		ahtb.open({ title: 'No listings detected', height: 180, html: '<p>'+msg+'</p>' });
					  	}
		  			}
					sellerAdmin.gotRowCount = true;
		  		}
	  		});
			
			sellerAdmin.setSlider();
		}

		this.transfer = function(listingID, dest){
			if (confirm("Are you sure you want to transfer listing: "+listingID+" to the "+sellerAdmin.getListingState(dest)+" category?")) {
				$.post(sellerAdmin.ajaxUrl,
						{'query':'transfer-listing',
						 'data':{id:listingID,
						 		 active: dest,
						 		 user: ah_local.user }},
						function(){},
						'json')
					.done(function(data){
						if (data.status == 'OK'){ 
							sellerAdmin.rowCount--;
							location.reload(); 
						}
					 	console.log(data.data);
					});
			}	
		}

		this.okToTransfer = function(id, dest) {
			for(var x in sellerAdmin.listings) {
				if (sellerAdmin.listings[x].id == id) {
					if (sellerAdmin.listings[x].active != dest)
						return true;
					else
						return false;
				}
			}
		}

		this.transferAll = function() {
			var didTransfer = false;
			$('.listings input[type="radio"]').each(function() {
				if ($(this).prop('checked')) {
					var attr;
					attr = $(this).attr('id');
					var pair = attr.split("-");
					console.log("Got "+pair[0]+", id:"+pair[1]);
					var id = parseInt(pair[1]);
					var dest = ActiveListingNames.indexOf(pair[0]);
					$(this).prop('checked', false);
					if (sellerAdmin.okToTransfer(id, dest)) {					
						$.post(sellerAdmin.ajaxUrl,
							{'query':'transfer-listing',
							 'data':{id: id,
							 		 active: dest,
							 		 user: ah_local.user }},
							function(){},
							'json')
						.done(function(data){
							if (data.status == 'OK'){ 
								sellerAdmin.rowCount--;
								didTransfer = true;
							}
						 	console.log(data.data);
						});
					}
				}
			})

			window.setTimeout(function() {
				if (didTransfer)
					location.reload(); 
			}, 500);
		}

		this.applyKeyword = function(val) {
			sellerAdmin.keywordsApplied = false;
			if (val.length) {
				ahtb.open({	html: '<div><span>Are you sure you want to search using keywords:'+val+'?</span></div>',
						   	width: 450,
							height: 150,
							buttons:[{text:'OK', action: function() {
								ahtb.close();
								$('#optionWrapper .spinner').fadeIn(250, function(){});
								var words = val.split(",");
								for(var i in words)
									words[i] = words[i].trim().toLowerCase();

								var data = {
									query: 'applykeyword',
									data: { key: words,
											active: sellerAdmin.activitySelected }
								};

								$.ajax({
									url: sellerAdmin.ajaxUrl,
									data: data,
									dataType: 'JSON',
									type: 'POST',
									error: function($xhr, $status, $error){
										var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
										console.log("Error during applyKeyword: "+msg);
										$('#optionWrapper .spinner').fadeOut(250, function(){});
										//sellerAdmin.getRowCount();  // try again
									},					
								  	success: function(data){
								  		$('#optionWrapper .spinner').fadeOut(250, function(){});
								  		if (data == null || data.status == null ||
								  			typeof data == 'undefined' || typeof data.status == 'undefined') 
								  			ahtb.open({ title: 'You Have Encountered an Error in applyKeyword', height: 150, html: '<p>Got a return of NULL</p>' });
								  		else if (data.status == 'OK'){ 
								  			var count = parseInt(data.data);
								  			if (count) {
								  				ahtb.alert(count+" listings matched the keywords, but actual number displayed will be limited by price and options.",
								  						   {height: 180});
								  				sellerAdmin.keywordsApplied = true;
								  				sellerAdmin.getRowCount();
								  			}
								  			else
								  				ahtb.alert("None of the listings matched the keyword(s)",
								  					 {height: 150});
								  		}
								  		else if (data.data)
								  			ahtb(data.data, {height: 150});
								  		else
								  			console.log("Some sort of failure in applyKeyword, no info.");

								  	}
								  });
							}},
							{text:'Cancel', action: function() {
								ahtb.close();
							}}]})
			}
			else {
				$('#optionWrapper .spinner').fadeOut(250, function(){});
				sellerAdmin.getRowCount();
				ahtb.alert("Please enter a keyword(s) or phrase(s) separated by commas.",
					{height: 150});
			}
		}

		this.cleanTags = function() {
			var data = {
				query: 'cleanTags',
				data: { active: sellerAdmin.activitySelected }
			};

			$('#optionWrapper .spinner').fadeIn(250, function(){});

			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: data,
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during cleanTags: "+msg);
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					//sellerAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in cleanTags', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.status == 'OK'){ 
			  			var count = 0;
			  			if ( !(typeof data.data == 'undefined' || data.data == null) )
			  				count = parseInt(data.data);
			  			if (count) {
			  				ahtb.alert(count+" listings cleaned.",
			  						   {height: 180});
			  				sellerAdmin.getPage();
			  			}
			  			else
			  				ahtb.alert("None of the listings had any tags to be cleaned",
			  					 {height: 150});
			  		}
			  		else if (data.data)
			  			ahtb.alert(data.data, {height: 150});
			  		else
			  			console.log("Some sort of failure in cleanTags, no info.");

			  	}
			});
		}

		this.getSellerListingStat = function(id, rowId, authorId) {
			if (sellerAdmin.gettingListingStat == 0)
				$('#optionWrapper .spinner').fadeIn(250, function(){});
			sellerAdmin.gettingListingStat++;

			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: {query: 'getsellerlistingstat',
						data: {	author: authorId,
								id: id } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getSellerListingStat: "+msg);
					//$('#optionWrapper .spinner').fadeOut(250, function(){});
					sellerAdmin.gettingListingStat--;
					if (sellerAdmin.gettingListingStat == 0)
						$('#optionWrapper .spinner').fadeOut(250, function(){});
				},					
			  	success: function(data){
			  		sellerAdmin.gettingListingStat--;
			  		//$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in getSellerListingStat', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.data == null || data.data == 'undefined')
			  			console.log("Some sort of failure in getSellerListingStat, no info.");
			  		else {
			  			sellerAdmin.showSellerListingStat(parseInt(rowId)+1, data.data, authorId, id);
			  			
			  			if (sellerAdmin.gettingListingStat == 0) {
			  				$('.sellerList tbody #view').on('click', function() {
								var id = $(this).attr('data');
								console.log("View clicked for id:"+id);
								sellerAdmin.curPage = 0;
								sellerAdmin.curSellerAuthorId = id;
								sellerAdmin.getRowCount();
							});
			  				$('#optionWrapper .spinner').fadeOut(250, function(){});
			  			}
			  		}	
			  	}
			});
		}

		this.showSellerListingStat = function(rowId, seller, author_id, id) {
			var types = '';
			var body = '';
			for(var t in seller.listingTypes)
				switch(parseInt(t)) {
					case ListingState.INACTIVE: types += "I:"+seller.listingTypes[t]+" "; break;
					case ListingState.ACTIVE: types += "A:"+seller.listingTypes[t]+" "; break;
					case ListingState.WAITING: types += "W:"+seller.listingTypes[t]+" "; break;
					case ListingState.REJECTED: types += "R:"+seller.listingTypes[t]+" "; break;
					case ListingState.TOOCHEAP: types += "C:"+seller.listingTypes[t]+" "; break;
					case ListingState.NEVER: types += "N:"+seller.listingTypes[t]+" "; break;
					case ListingState.AVERAGE: types += "Avg:"+seller.listingTypes[t]+" "; break;
				}

			body += '<td>'+types+'</td>';
			$('.sellerList tbody tr:nth-child('+rowId+')').append(body);

			body = '<td><span>Count: '+(seller.listingCount ? seller.listingCount : 0)+'  </span><button name="viewButton" id="view" data="'+author_id+'"';
			if (seller.listingCount == null || seller.listingCount == 0)
				body += 'disabled="true"';
			body += '>View</button></td>';
			body += '<td>';
			if (typeof author_id != 'undefined' &&
				author_id != null &&
				author_id > 8 &&
				author_id != id)
			body += 	'<button name="password" id="password" data="'+id+'" data2="'+author_id+'">Get Pswd</button>';
			body += '</td>';
			body += '<td>';
			if (typeof seller.reservation != 'undefined')
				body += 	'<button name="reservation" id="reservation" data="'+id+'">Update CRM</button>';
			body += '</td>';

			$('.sellerList tbody tr:nth-child('+rowId+')').append(body);

			$('.sellerList tbody #password').off().on('click', function() {
				var id = $(this).attr('data');
				var author_id = $(this).attr('data2');
				sellerAdmin.getSellerPassword(id, author_id);
			});

			$('.sellerList tbody #reservation').off().on('click', function() {
				var id = $(this).attr('data');
				sellerAdmin.updateExistingReservations(id);
			});
		}

		this.getSellerPage = function(page, isInit) {
			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			isInit = typeof isInit == 'undefined' ? false : isInit;

			if (typeof sellerAdmin.authors == 'object')
				sellerAdmin.authors.splice(0, sellerAdmin.authors.length); // clear out

			if (sellerAdmin.option) {
				if (sellerAdmin.lastOption.attr('id') == 'address') {
					one = $('#optionChooser #city').val().trim();
					two = $('#optionChooser #state').val().trim();
					three = $('#optionChooser #zip').val().trim();
					if (!one && !two) {
						if (!isInit) {
							ahtb.open({ title: 'No address detected', height: 150, html: '<p>Please enter either city, state or zip.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.ADDRESS;
				}
				else if (sellerAdmin.lastOption.attr('id') == 'ids') {
					if (sellerAdmin.idSpecifier == 'range') {
						one = $('#optionChooser #from-id').val().trim();
						if (one.length == 0)
							one = '1';
						two = $('#optionChooser #to-id').val().trim();
						if (two.length == 0)
							two = '1000000';
					}
					else {
						one = $('#optionChooser  #id-specifier').val().trim();
					}
					whichOne = OptionState.IDS;
					if ( (sellerAdmin.idSpecifier == 'range' && !one && !two) ||
						 !one) { // && !three) {
						sellerAdmin.gotSellerCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No range detected', height: 150, html: '<p>Please enter min/max or specified ids.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.IDS;
				}
			}

			$('#optionWrapper .spinner').fadeIn(250, function(){});

			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: {query: 'getsellers',
						data: {	page: page,
								perPage: sellerAdmin.pageSize,
								which: whichOne,
								first: one,
								second: two,
								third: three,
								distance: sellerAdmin.distance,
								sortcity: sellerAdmin.citySort,
								sortstate: sellerAdmin.stateSort,
								sortzip: sellerAdmin.zipSort,
								sortid: sellerAdmin.idSort,
								sortemail: sellerAdmin.emailSort,
								sortauthorid: sellerAdmin.authorIdSort,
								active: sellerAdmin.onlyActiveSellers,
								emailDbFlags: sellerAdmin.emailDbFlags } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during getSellerPage: "+msg);
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					//sellerAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in getSellerPage', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.status == 'OK'){ 
			  			var count = 0;
			  			if ( !(typeof data.data == 'undefined' || data.data === null) )
			  				sellerAdmin.authors = data.data;
			  			if (sellerAdmin.authors != 0) 
			  				sellerAdmin.showSellers();
			  			else
			  				ahtb.alert("Failed to get sellers list.",
			  					 {height: 150});

			  			if (sellerAdmin.option) 
			  				sellerAdmin.getSingleSeller(isInit);

			  			sellerAdmin.setVisibility();
			  			
			  		}
			  		else if (data.data)
			  			ahtb.alert(data.data, {height: 150});
			  		else
			  			console.log("Some sort of failure in getSellerPage, no info.");

			  	}
			});
		}

		this.getSingleSeller = function(isInit) {
			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			isInit = typeof isInit == 'undefined' ? false : isInit;

			if (sellerAdmin.option &&
				sellerAdmin.lastOption.attr('id') == 'name') {
					one = $('#optionChooser #firstName').val().trim();
					two = $('#optionChooser #lastName').val().trim();
					three = $('#optionChooser #email').val().trim();
					if (!one && !two && sellerAdmin.option) {
						sellerAdmin.gotRowCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No name detected', height: 150, html: '<p>Please enter either first/last name or both.</p>' });
							return;
						}
					}
					console.log("Seller option - first:"+one+", last:"+two+", email:"+three);

				$.ajax({
					url: sellerAdmin.ajaxUrl,
					data: {query: 'getsingleseller',
							data: { first: one,
									second: two,
									third: three } 
						  },
					dataType: 'JSON',
					type: 'POST',
					error: function($xhr, $status, $error){
						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						console.log("Error during getSingleSeller: "+msg);
						$('#optionWrapper .spinner').fadeOut(250, function(){});
						//sellerAdmin.getRowCount();  // try again
					},					
				  	success: function(data){
				  		$('#optionWrapper .spinner').fadeOut(250, function(){});
				  		if (data == null || data.status == null ||
				  			typeof data == 'undefined' || typeof data.status == 'undefined') 
				  			ahtb.open({ title: 'You Have Encountered an Error in getSingleSeller', height: 150, html: '<p>Got a return of NULL</p>' });
				  		else if (data.status == 'OK'){ 
				  			var count = 0;
				  			sellerAdmin.singleSeller = data.data;
				  			if (sellerAdmin.singleSeller != null) {
				  				// populate singleSeller table
				  				var body = '<tr>';
								var authorId = sellerAdmin.singleSeller.author_id == null || sellerAdmin.singleSeller.author_id == 'undefined' ? "N/A" : sellerAdmin.singleSeller.author_id;
				
								body += "<td >"+sellerAdmin.singleSeller.id+"</td>";
								body += "<td >"+authorId+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.first_name+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.last_name+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.email+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.city+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.state+"</td>";
								body += "<td >"+sellerAdmin.singleSeller.zip+"</td>";
								//begin invite codes
								body += '<td><select name="codes" class="codes" data="'+sellerAdmin.singleSeller.id+'">';
								for(var c in sellerAdmin.inviteList) {
							  		body += '<option value="'+sellerAdmin.inviteList[c].code+'">'+sellerAdmin.inviteList[c].code+' - owned by: '+(sellerAdmin.inviteList[c].seller_id ? sellerAdmin.inviteList[c].seller_id[0] : "N/A")+'</option>';
							  	}
								body+= '</select></td>';
								//end invite codes
								//begin improvements
								var seller = sellerAdmin.singleSeller;
								var countImprovementMeta = 0;
								if (seller.meta != null) {								
									for(var i in seller.meta) {
							          if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS)
							            countImprovementMeta++;
							    	}
							    	if (countImprovementMeta) {
										body += '<td><select name="improvements" class="improvements" id="improvements-'+sellerAdmin.singleSeller.id+'" data="'+sellerAdmin.singleSeller.id+'">';
										for(var i in seller.meta) {
							          		if (parseInt(seller.meta[i].action) == MetaDataType.SELLER_IMPROVEMENT_TASKS) {
							          			body += '<option>'+seller.meta[i].listhub_key+' :';
							          			var k = 0;
							          			for(var j in seller.meta[i]) {
				                					if (j != 'listhub_key' &&
				                						j != 'action') {
				                						if (k)
				                							body += ',';
				                						body += j;
				                						k++;
				                					}
				                				}
							          			body += '</option>';
							          		}
							          	}
										body+= '</select></td>';
									}
									else
										body += '<td>None found</td>';
								}
								else
									body += '<td>None found</td>';
								// end improvements
								body += '<td><button name="assign" id="assign" data="'+sellerAdmin.singleSeller.id+'">Assign</button></td>';
								if (!countImprovementMeta &&
									!(sellerAdmin.singleSeller.author_id && sellerAdmin.singleSeller.author_id <= 8) &&
									sellerAdmin.singleSeller.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME))
									body += '<td>All good</td>';
								else {
									body += '<td>';
									if (countImprovementMeta)
										body += '<button name="improve" id="improve" data="'+sellerAdmin.singleSeller.id+'">Improve It Email</button>';
									if ( !(sellerAdmin.singleSeller.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) ||
										  (sellerAdmin.singleSeller.author_id && sellerAdmin.singleSeller.author_id <= 8) )
										body += '<button name="intro" id="intro" data="'+sellerAdmin.singleSeller.id+'">Intro Email</button>';
									body += '<button name="intro" id="test-email" data="'+sellerAdmin.singleSeller.id+'">Test Email</button>';
									if (typeof sellerAdmin.singleSeller.flags != 'undefined' &&
										 sellerAdmin.singleSeller.flags != '0')
										body += '<button name="clear" id="clear" data="'+sellerAdmin.singleSeller.id+'">Clear Email Flags</button>';
									body += '</td>';
								}

								var types = '';
								for(var t in sellerAdmin.singleSeller.listingTypes)
									switch(parseInt(t)) {
										case ListingState.INACTIVE: types += "I:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.ACTIVE: types += "A:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.WAITING: types += "W:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.REJECTED: types += "R:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.TOOCHEAP: types += "C:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.NEVER: types += "N:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
										case ListingState.AVERAGE: types += "Avg:"+sellerAdmin.singleSeller.listingTypes[t]+" "; break;
									}
								body += '<td>'+types+'</td>';

			 					var author_id = sellerAdmin.singleSeller.author_id == null || sellerAdmin.singleSeller.author_id == 'undefined' ? sellerAdmin.singleSeller.id : sellerAdmin.singleSeller.author_id;
								body += '<td><span>Count: '+(sellerAdmin.singleSeller.listingCount ? sellerAdmin.singleSeller.listingCount : 0)+'  </span><button name="view" id="view" data="'+author_id+'"';
								if (sellerAdmin.singleSeller.listingCount == null || sellerAdmin.singleSeller.listingCount == 0)
									body += 'disabled="true"';
								body += '>View</button></td>';
								body += '<td>';
								if (typeof sellerAdmin.singleSeller.author_id != 'undefined' &&
									sellerAdmin.singleSeller.author_id != null &&
									sellerAdmin.singleSeller.author_id > 8 &&
									sellerAdmin.singleSeller.author_id != sellerAdmin.singleSeller.id)
									body += 	'<button name="password" id="password" data="'+sellerAdmin.singleSeller.id+'" data2="'+sellerAdmin.singleSeller.author_id+'">Get Pswd</button>';
									body += 	'<button name="update-password" id="password" data="'+sellerAdmin.singleSeller.id+'" data2="'+sellerAdmin.singleSeller.author_id+'">Update Pswd</button>';
								body += '</td>';
								body += '<td>';
								if (typeof sellerAdmin.singleSeller.reservation != 'undefined')
									body += 	'<button name="reservation" id="reservation" data="'+sellerAdmin.singleSeller.id+'">Update CRM</button>';
								body += '</td>';
								body += '</tr>';
								$('.sellerSingle tbody').html(body);

								var match = '';
								for(var c in sellerAdmin.inviteList) {
									// if (sellerAdmin.inviteList[c].seller_id == sellerAdmin.singleSeller.id)
									if (sellerAdmin.inviteList[c].seller_id &&
										sellerAdmin.inviteList[c].seller_id.indexOf(sellerAdmin.singleSeller.id) != -1)
							  			match = sellerAdmin.inviteList[c].code;
								}
								if (match != '') 
									$('.sellerSingle .codes option[value="'+match+'"').prop('selected', true);

								$('.sellerSingle .codes').on('change', function() {
									var val = '';
									var owner = '';
									$.each($(this).children(), function() {
										if ($(this).prop("selected")) {
											val = $(this).attr('value');
											owner = $(this).attr('owner');
										}
									});
									//var val = $("this option:selected").val();
									var id = $(this).attr('data');
									console.log("Code:"+val+", owner:"+owner+" for seller id:"+id);
									if (owner == '0') {
										$('.sellerSingle tbody #assign[data="'+id+'"]').prop('disabled', false);
										sellerAdmin.singleSeller.invite = val;
									}
									else
										$('.sellerSingle tbody #assign[data="'+id+'"]').prop('disabled', true);
									
								});
								$('.sellerSingle tbody #assign').prop('disabled', true);
								$('.sellerSingle tbody #assign').on('click', function() {
									var id = $(this).attr('data');
									console.log("Assign clicked for id:"+id);
									// need to get the code registered in the seller's meta
									if (sellerAdmin.singleSeller.id == id &&
										sellerAdmin.singleSeller.invite) {
										sellerAdmin.addSellerCode(sellerAdmin.singleSeller);
									}
									// need to update the codes in Options
									sellerAdmin.updateInviteCodes(sellerAdmin.singleSeller);
									// need to update the select menus of all sellers on page
									sellerAdmin.updateAllSelectMenus(sellerAdmin.singleSeller);
									// now disable this assign button again
									$(this).prop('disabled', true);
								});

								$('.sellerSingle tbody #clear').on('click', function() {
									var id = $(this).attr('data');
									console.log("Clear clicked for id:"+id);
									var x = {query: 'clear-flags',
											 data: {id: id},
											 done: function(data) {
											 	console.log(data);
											 },
											 error: function(data) {
											 	console.log(data);
											 }
											};
									sellerAdmin.DB(x);
								});

								$('.sellerSingle  #improve').on('click', function() {
									var id = $(this).attr('data');
									var name = $(this).attr('name');
									console.log(name+" clicked for id:"+id);
									sellerAdmin.sendEmail(EmailTypes.IMPROVE_LISTING, 'improve',[{id:id}]);
								});

								$('.sellerSingle  #intro').on('click', function() {
									var id = $(this).attr('data');
									var name = $(this).attr('name');
									console.log(name+" clicked for id:"+id);
									sellerAdmin.sendEmail(EmailTypes.PROMO_FIRST, 'intro',[{id:id}]);
								});

								$('.sellerSingle  #test-email').on('click', function() {
									var id = $(this).attr('data');
									var name = $(this).attr('name');
									console.log(name+" clicked for id:"+id);
									sellerAdmin.sendEmail(sellerAdmin.mailType, 'email-test',[{id:id}]);
								});

								$('.sellerSingle tbody #view').on('click', function() {
									var id = $(this).attr('data');
									console.log("View clicked for id:"+id);
									sellerAdmin.curPage = 0;
									sellerAdmin.curSellerAuthorId = id;
									sellerAdmin.getRowCount();
								});

								$('.sellerSingle tbody #password').on('click', function() {
									var name = $(this).attr('name');
									var id = $(this).attr('data');
									var author_id = $(this).attr('data2');

									console.log("clicked "+name);

									switch(name) {
										case 'password':
											sellerAdmin.getSellerPassword(id, author_id);
											break;
										case 'update-password':
											sellerAdmin.updateSellerPassword(id, author_id);
											break;
									}
									
								});

								$('.sellerSingle tbody #reservation').on('click', function() {
									var id = $(this).attr('data');
									sellerAdmin.updateExistingReservations(id);
								});

								sellerAdmin.setVisibility();
				  			}
				  			else
				  				ahtb.alert("Failed to get single seller with First:"+(one.length?one:"N/A")+", Last:"+(two.length?two:"N/A")+", email:"+(three.length?three:"N/A"),
				  					 {height: 150});
				  		}
				  		else if (data.data)
				  			ahtb.alert(data.data, {height: 150});
				  		else
				  			console.log("Some sort of failure in getSingleSeller, no info.");

				  	}
				});
			}
		}

		// do this once for all time...
		this.getSellerCount = function(isInit, doSendEmailPerPage) {
			sellerAdmin.gotSellerCount = false;
			sellerAdmin.sellerCount = 0;
			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			isInit = typeof isInit == 'undefined' || !isInit ? false : isInit;

			if (sellerAdmin.option) {
				if (sellerAdmin.lastOption.attr('id') == 'address') {
					one = $('#optionChooser #city').val().trim();
					two = $('#optionChooser #state').val().trim();
					three = $('#optionChooser #zip').val().trim();
					if (!one && !two) { // && !three) {
						sellerAdmin.gotSellerCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No address detected', height: 150, html: '<p>Please enter either city, state or zip.</p>' });
							return;
						}
						else {
							$("#optionChooser .optionActive").prop('checked', false);
							sellerAdmin.option = false;
							sellerAdmin.setOptions();
							sellerAdmin.setCookies(sellerAdmin.activitySelected);
							sellerAdmin.setRowCountCookies(sellerAdmin.activitySelected);
							// $("#optionChooser .optionActive").click();
						}
					}
					else
						whichOne = OptionState.ADDRESS;
				}
				else if (sellerAdmin.lastOption.attr('id') == 'ids') {
					if (sellerAdmin.idSpecifier == 'range') {
						one = $('#optionChooser #from-id').val().trim();
						if (one.length == 0)
							one = '1';
						two = $('#optionChooser #to-id').val().trim();
						if (two.length == 0)
							two = '1000000';
					}
					else {
						one = $('#optionChooser  #id-specifier').val().trim();
					}
					whichOne = OptionState.IDS;
					if ( (sellerAdmin.idSpecifier == 'range' && !one && !two) ||
						 !one ) {
						sellerAdmin.gotSellerCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No ids detected', height: 150, html: '<p>Please enter either range or selected ids.</p>' });
							return;
						}
						else {
							$("#optionChooser .optionActive").prop('checked', false);
							sellerAdmin.option = false;
							sellerAdmin.setOptions();
							sellerAdmin.setCookies(sellerAdmin.activitySelected);
							sellerAdmin.setRowCountCookies(sellerAdmin.activitySelected);
							// $("#optionChooser .optionActive").click();
						}
					}
					else
						whichOne = OptionState.IDS;
				}
			}

			var x = { query: 'getsellercount',
					  data: { which: whichOne,
								first: one,
								second: two,
								third: three,
								distance: sellerAdmin.distance,
								active: sellerAdmin.onlyActiveSellers,
								emailDbFlags: sellerAdmin.emailDbFlags },
					 done: function(data) {
			  			var count = data ? parseInt(data) : 0;
				  		if (count) {
					  		sellerAdmin.sellerCount = count;
					  		$('#infoBarSeller .seller').html("# of Sellers: "+ sellerAdmin.sellerCount);
					  		var target = sellerAdmin.getActiveName(sellerAdmin.activitySelected)+'SellerPage';
							var value = parseInt(sellerAdmin.getNumCookie(target));
							value = (value * sellerAdmin.pageSize) > count ? Math.floor(count/sellerAdmin.pageSize) : value;
							sellerAdmin.curSellerPage = value;
						}			

				  		console.log("SellerCount is "+sellerAdmin.sellerCount);
				  		
				  		//sellerAdmin.getPageSeller(sellerAdmin.curSellerPage);

					  	if (!sellerAdmin.sellerCount) {
					  		var msg = "Major problem!  No sellers found!";
					  		console.log(msg);
					  		ahtb.alert("No sellers found with the current setting.", {height: 150});
					  		return;
					  	}

					  	sellerAdmin.gotSellerCount = true;
					  	if (typeof doSendEmailPerPage == 'undefined') {
							sellerAdmin.setSliderSeller();
							sellerAdmin.getSellerPage(sellerAdmin.curSellerPage, isInit);
						}
						else
							sellerAdmin.startSendEmailPerPage();
					 },
					 error: function(data) {
					 	console.log(data);
					 	ahtb.alert(data, {height: 150});
					 	sellerAdmin.gotSellerCount = true;
					 	if (typeof doSendEmailPerPage == 'undefined') {
							sellerAdmin.setSliderSeller();
							sellerAdmin.getSellerPage(sellerAdmin.curSellerPage, isInit);
						}
					 } }; 

			sellerAdmin.DB(x);
		}

		this.startSendEmailPerPage = function() {
			var h = '<div id="sendEmailPerPage">';
			h+= 	'<label id="message">Remaining sellers to process</label>&nbsp;';
			h+= 	'<span id="sellerCount"></span>';
			h+= '</div>';
			sellerAdmin.pageSendEmail = 0;
			// sellerAdmin.pagePerSendEmail = 1000;
			sellerAdmin.sentEmailCount = 0;
			sellerAdmin.haltSendEmail = false;
			sellerAdmin.csv = '';

			if (!sellerAdmin.mailchimp) {
				ahtb.alert("This must only be called for chimp services", {height: 150});
				return;
			}

			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			isInit = false;

			if (typeof sellerAdmin.authors == 'object')
				sellerAdmin.authors.splice(0, sellerAdmin.authors.length); // clear out

			if (sellerAdmin.option) {
				if (sellerAdmin.lastOption.attr('id') == 'address') {
					one = $('#optionChooser #city').val().trim();
					two = $('#optionChooser #state').val().trim();
					three = $('#optionChooser #zip').val().trim();
					if (!one && !two) {
						if (!isInit) {
							ahtb.open({ title: 'No address detected', height: 150, html: '<p>Please enter either city, state or zip.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.ADDRESS;
				}
				else if (sellerAdmin.lastOption.attr('id') == 'ids') {
					if (sellerAdmin.idSpecifier == 'range') {
						one = $('#optionChooser #from-id').val().trim();
						if (one.length == 0)
							one = '1';
						two = $('#optionChooser #to-id').val().trim();
						if (two.length == 0)
							two = '1000000';
					}
					else {
						one = $('#optionChooser  #id-specifier').val().trim();
					}
					whichOne = OptionState.IDS;
					if ( (sellerAdmin.idSpecifier == 'range' && !one && !two) ||
						 !one) { // && !three) {
						sellerAdmin.gotSellerCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No range detected', height: 150, html: '<p>Please enter min/max or specified ids.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.IDS;
				}
			}

			ahtb.open({html: h,
					   width: 450,
						height: 150,
						buttons: [{text: "Stop it", action: function() {
							sellerAdmin.haltSendEmail = true;
						}}],
						opened: function() {
							var ele = $('#sendEmailPerPage #sellerCount');
							ele.html(sellerAdmin.sellerCount.toString());
							sellerAdmin.doOnePageEmailDb(whichOne, one, two, three);
						}});
		}

		this.doOnePageEmailDb = function(whichOne, one, two, three) {
			console.log("doOnePageEmailDb - start which:"+whichOne+", page: "+sellerAdmin.pageSendEmail+", pagePer:"+sellerAdmin.pagePerSendEmail);
			var x = {query: 'sendemailperpage',
					 data: {
					 		id: sellerAdmin.mailType,
							what: 'email-sellers',
							target: [],
							which: whichOne,
							first: one,
							second: two,
							third: three,
							distance: sellerAdmin.distance,
							active: sellerAdmin.onlyActiveSellers,
							mailchimp: sellerAdmin.mailchimp,
							emailDbFlags: sellerAdmin.emailDbFlags,
							page: sellerAdmin.pageSendEmail,
							pagePer: sellerAdmin.pagePerSendEmail,
							noTrip: sellerAdmin.testingEmail // trip or not trip flags
					 },
					 done: function(data) {
					 	// sellerAdmin.gettingListingStats = false;
					 	if (sellerAdmin.mailchimp)
					 		sellerAdmin.csv += data.csv;
					 	sellerAdmin.pageSendEmail++;
					 	var ele = $('#sendEmailPerPage #sellerCount');
					 	var remaining = (sellerAdmin.sellerCount - (sellerAdmin.pageSendEmail * sellerAdmin.pagePerSendEmail)) >= 0 ? (sellerAdmin.sellerCount - (sellerAdmin.pageSendEmail * sellerAdmin.pagePerSendEmail)) : 0;
					 	console.log("doOnePageEmailDb -  completed which:"+whichOne+", page: "+sellerAdmin.pageSendEmail+", remaining:"+remaining.toString());
						ele.html(remaining.toString());
						if (!sellerAdmin.haltSendEmail) {
							sellerAdmin.doOnePageEmailDb(whichOne, one, two, three);
							if ( ((sellerAdmin.pageSendEmail * sellerAdmin.pagePerSendEmail) % sellerAdmin.csvDumpCsvLimit) == 0 &&
								 sellerAdmin.mailchimp ){
								var fname = (one == '' ? 'AllCurrent' : one)+'-'+(sellerAdmin.pageSendEmail * sellerAdmin.pagePerSendEmail).toString()+'.csv';
								saveAs( new Blob([sellerAdmin.csv], 
					  								{type: "plain/text;charset=" + document.characterSet} ), 
					  						fname);
								sellerAdmin.csv = ''; // blank out
							}
						}
						else {
							var msg = "Stopped process after processing "+(sellerAdmin.pageSendEmail * sellerAdmin.pagePerSendEmail).toString()+" users";
							ahtb.alert(msg, {height: 150});
							if (sellerAdmin.mailchimp) {
								if (one == '')
			  						one = 'AllCurrent';
				  				saveAs( new Blob([sellerAdmin.csv], 
				  								{type: "plain/text;charset=" + document.characterSet} ), 
				  						one+"-final.csv");	
				  			}
				  			else
				  				sellerAdmin.getEmailStats(); // last time
						}
					 },
					 error: function(data) {
					 	console.log("doOnePageEmailDb -  errored out, which:"+whichOne+", page: "+sellerAdmin.pageSendEmail);
					 	// console.log(data);
					 	var h = '<div id="content" style="width: 450px; max-height:250px; overflow-y: scroll;"><span></span></div>';
			  			ahtb.open({
			  				html: h, 
			  				height: 250,
			  				width: 450,
			  				opened: function() {
			  					$('#content span').html(data.status);
			  				}});
			  			if (sellerAdmin.mailchimp) {
			  				if (one == '')
			  					one = 'AllCurrent';
			  				saveAs( new Blob([sellerAdmin.csv], 
			  								{type: "plain/text;charset=" + document.characterSet} ), 
			  						one+".csv");		
			  			}
			  			else
			  				sellerAdmin.getEmailStats(); // last time  		
					}};
			sellerAdmin.DB(x);
		}

		this.getZohoCrmCsv = function() {
			var x = {query: 'get-zoho-crm-csv',
					 data: {
							what: 'all'
					 },
					 done: function(data) {
						var fname = 'zoho.csv';
						saveAs( new Blob([data.csv], 
			  								{type: "plain/text;charset=" + document.characterSet} ), 
			  						fname);
					
					},
					error: function(data) {
					 	console.log("returned fail for getZohoCrmCsv");
					}
				};
			sellerAdmin.DB(x);
		}

		this.makeRadialArray = function() {
			var x = {query: 'make-radian-array',
					 done: function(data) {
						var fname = 'radial.txt';
						saveAs( new Blob([data.txt], 
			  								{type: "plain/text;charset=" + document.characterSet} ), 
			  						fname);
					
					},
					error: function(data) {
					 	console.log("returned fail for makeRadialArray");
					}
				};
			sellerAdmin.DB(x);
		}

		this.scrubSellerTags = function() {
			var x = {query: 'scrub-seller-tags',
					 done: function(data) {
					 	var msg = "Tags updated "+data.tagMods+", Seller items updated "+data.sellerMods;
						ahtb.alert(msg, {width: 450, height: 150});
					},
					error: function(data) {
					 	console.log("returned fail for scrubSellerTags");
					}
				};
			sellerAdmin.DB(x);
		}

		this.updateSellerPortalVisits = function() {
			ahtb.alert("Please wait, updating seller portal visits...", {height: 150});
			var x = {query: 'update-seller-portal-visits',
					 done: function(data) {
					 	console.log("returned OK for updateSellerPortalVisits");
					 	ahtb.alert("Updated "+data.total+" seller portals", {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for updateSellerPortalVisits");
					 	ahtb.close();
					 }};
			sellerAdmin.DB(x);
		}

		this.testAjaxPaywhirl = function() {
			// ahtb.alert("Please wait, updating seller portal visits...", {height: 150});
			var x = {query: 'testMe',
					 url: ah_local.tp+"/_admin/ajax_paywhirl.php",
					 done: function(data) {
					 	console.log("returned OK for testAjaxPaywhirl");
					 	ahtb.alert(data, {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for testAjaxPaywhirl");
					 	ahtb.alert("returned fail for testAjaxPaywhirl");
					 }};
			sellerAdmin.DB(x);
		}

		this.deleteSeller = function() {
			var id = $('input#seller-id').val();
			if (id.length == 0) {
				ahtb.alert("Enter Seller ID");
				return;
			}
			ahtb.open({	html:"Are you sure?!",
					   	width: 350,
					   	height: 150,
					   	buttons: [
					   	{text:'OK', action: function() {
					   		var x = {query: 'delete-seller',
									 data: {id: id},
									 done: function(data) {
									 	console.log("returned OK for deleteSeller");
									 	ahtb.alert("Deleted sellerId:"+id+", "+data.first_name+" "+data.last_name+".", {height: 150});
									 },
									 error: function(data) {
									 	console.log("returned fail for deleteSeller");
									 	if (typeof data == 'string')
									 		ahtb.alert(data);
									 	else
									 		ahtb.alert("Failure in deleteSeller");
									 }};
							sellerAdmin.DB(x);
					   	}},
					   	{text:'Cancel', action: function() {
					   		ahtb.close();
					   	}}]});
		}

		this.unlockUser = function() {
			var id = $('input#user-name').val();
			if (id.length == 0) {
				ahtb.alert("Enter User Name");
				return;
			}
			ahtb.open({	html:"Are you sure?!",
					   	width: 350,
					   	height: 150,
					   	buttons: [
					   	{text:'OK', action: function() {
					   		var x = {query: 'unlock-user',
									 data: {user: id},
									 done: function(data) {
									 	console.log("returned OK for unlockUser");
									 	var msg = "Unlocked user:"+id+".  ";
									 	if (typeof data == 'object' ||
									 		Array.isArray(data))
									 		for(var i in data)
									 			msg += data[i]+'<br/>';
									 	ahtb.alert(msg, {width: 500,
									 					height: 150 + (msg.length/100)*30});
									 },
									 error: function(data) {
									 	console.log("returned fail for unlockUser");
									 	if (typeof data == 'string')
									 		ahtb.alert(data);
									 	else
									 		ahtb.alert("Failure in deleteSeller");
									 }};
							sellerAdmin.DB(x);
					   	}},
					   	{text:'Cancel', action: function() {
					   		ahtb.close();
					   	}}]});
		}


		this.updateLeadIds = function() {
			var x = {query: 'zoho-lead-ids',
					 data: {
							what: 'all'
					 },
					 done: function(data) {
						ahtb.alert(data, {height: 150});
					},
					error: function(data) {
					 	console.log("returned fail for zohoTestApi");
					}
				};
			sellerAdmin.DBsales(x);
		}

		this.updateNewReservations = function() {
			var x = {query: 'zoho-udpate-new-reservations',
					 data: {
							what: 'all'
					 },
					 done: function(data) {
						ahtb.alert(data, {height: 150});
					},
					error: function(data) {
					 	console.log("returned fail for zohoTestApi");
					}
				};
			sellerAdmin.DBsales(x);
		}

		this.updateExistingReservations = function(id) {
			var what = 'all';
			if (typeof id != 'undefined')
				what = id;
			var x = {query: 'zoho-udpate-existing-reservations',
					 data: {
							what: what
					 },
					 done: function(data) {
						ahtb.alert(data, {height: 150});
					},
					error: function(data) {
					 	console.log("returned fail for zohoTestApi");
					}
				};
			sellerAdmin.DBsales(x);
		}

		this.addSellerCode = function(seller) {
			var x = {query: 'addsellercode',
					 data: {id: seller.id,
							code: seller.invite },
					 done: function(data) {
					 	console.log(data);
					 },
					 error: function(data) {
					 	console.log(data);
					 } };
			sellerAdmin.DB(x);
				  
			// $('#optionWrapper .spinner').fadeIn(250, function(){});
			// $.ajax({
			// 	url: sellerAdmin.ajaxUrl,
			// 	data: {query: 'addsellercode',
			// 			data: {	id: seller.id,
			// 					code: seller.invite } 
			// 		  },
			// 	dataType: 'JSON',
			// 	type: 'POST',
			// 	error: function($xhr, $status, $error){
			// 		var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
			// 		console.log("Error during addSellerCode: "+msg);
			// 		$('#optionWrapper .spinner').fadeOut(250, function(){});
			// 		//sellerAdmin.getRowCount();  // try again
			// 	},					
			//   	success: function(data){
			//   		$('#optionWrapper .spinner').fadeOut(250, function(){});
			//   		if (data == null || data.status == null ||
			//   			typeof data == 'undefined' || typeof data.status == 'undefined') 
			//   			ahtb.open({ title: 'You Have Encountered an Error in addSellerCode', height: 150, html: '<p>Got a return of NULL</p>' });
			//   		else if (data.data == null || data.data == 'undefined')
			//   			console.log("Some sort of failure in addSellerCode, no info.");
			//   		else
			//   			console.log(data.data);

			//   	}
			// });
		}

		this.getEmailActivityStats = function() {
			if (sellerAdmin.waitingForEmailStats)
				return;

			ahtb.alert("Please wait, collating data...", {height: 150});
			var x = {query: 'get-email-activity-stats',
  				 done: function(data) {
  				 	sellerAdmin.waitingForEmailStats = false;
  				 	// $('#emailStats').html(data);
  				 	var h = '<div id="activity-stats">'+data.table+'</div>';
  				 	h +=    '<div id="chartDiv">';
					h +=		'<canvas id="chart" height="150" width="600"></canvas>';
					h +=		'<div id="legend"></div>';
					h += 	'</div>';
					h +=	'<div id="controls">';
					h +=    	'<input type="radio" name="option" id="table" checked >Table</input>';
					h +=		'<input type="radio" name="option" id="chart">Chart</input>';
					h +=	'</div>';
  				 	ahtb.open({html: h,
  				 			   width: 900,
  				 			   height: 500,
  				 			   opened: function() {
  				 			   		
  				 			   		var ctx = $('canvas#chart').get(0).getContext("2d");
									sellerAdmin.chart = null;
									sellerAdmin.chart = new Chart(ctx).Bar(data.chart, {
											responsive: true,
											// scaleSteps: 5
									});
									sellerAdmin.chart.update();
									$('#chartDiv').hide();
									$('#chartDiv #legend').html(sellerAdmin.chart.generateLegend());
  				 			   		$('#controls input').on('change', function() {
  				 			   			var id = $(this).attr('id');
  				 			   			console.log("Control hit for "+id);
  				 			   			var ele = $('div#activity-stats');
  				 			   			switch(id) {
  				 			   				case 'table': $('#chartDiv').hide();
  				 			   							  $('div#activity-stats').show();
  				 			   							  break;
  				 			   				case 'chart': $('#chartDiv').show();
  				 			   							  $('div#activity-stats').hide();
  				 			   							  break;
  				 			   			}
  				 			   		})
  				 			   }})
  				 },
		  		 error: function(data) {
		  		 	sellerAdmin.waitingForEmailStats = false;
				 	console.log(data);
				 } };
			sellerAdmin.waitingForEmailStats = true;
			sellerAdmin.DB(x);
		}

		this.getChimpComplaints = function() {
			ahtb.alert("Please wait, collating data...", {height: 150});
			var x = {query: 'get-chimp-complaints',
					 data: { index: sellerAdmin.mailchimpCampaign },
					 done: function(data) {
					 	console.log("returned OK for getChimpComplaints");
					 	ahtb.alert("Found "+data.total+" complaints for "+sellerAdmin.mailchimpCampaign, {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for getChimpComplaints");
					 }};
			sellerAdmin.DB(x);
		}

		this.getChimpUnsubscribes = function() {
			ahtb.alert("Please wait, collating data...", {height: 150});
			var x = {query: 'get-chimp-unsubscribes',
					 data: { index: sellerAdmin.mailchimpCampaign },
					 done: function(data) {
					 	console.log("returned OK for getChimpUnsubscribes");
					 	ahtb.alert("Found "+data.total+" unsubscribes for "+sellerAdmin.mailchimpCampaign, {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for getChimpUnsubscribes");
					 }};
			sellerAdmin.DB(x);
		}

		this.getChimpOpen = function() {
			ahtb.alert("Please wait, collating data...", {height: 150});
			var x = {query: 'get-chimp-open',
					 data: { index: sellerAdmin.mailchimpCampaign },
					 done: function(data) {
					 	console.log("returned OK for getChimpOpen");
					 	ahtb.alert("Found "+data.total+" opens for "+sellerAdmin.mailchimpCampaign, {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for getChimpOpen");
					 }};
			sellerAdmin.DB(x);
		}

		this.startUpdateChimpSentTo = function() {
			var h = '<div id="updateChimpSentDo">';
			h+= 	'<label id="message">Working on campaign</label>&nbsp;';
			h+= 	'<span id="campaign"></span>';
			h+= 	'<span id="sentTo"></span>';
			h+= '</div>';
			sellerAdmin.campaignIndex = 0;
			sellerAdmin.sentToCount = 0;
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						// buttons: [{text: "Stop it", action: function() {
						// 	sellerAdmin.haltSendEmail = true;
						// }}],
						opened: function() {
							var ele = $('#updateChimpSentDo #campaign');
							ele.html(chimpCampaigns[0].name);
							sellerAdmin.doOneCampaignSentTo();
						}});
		}

		this.doOneCampaignSentTo = function() {
			console.log("doOnePageEmailDb - for "+chimpCampaigns[sellerAdmin.campaignIndex].name);
			var x = {query: 'update-campaign-sentto-perpage',
					 data: {
					 	index: chimpCampaigns[sellerAdmin.campaignIndex].key
					 },
					 done: function(data) {
					 	if (data.working == 0 ||
					 		data.working == false ||
					 		data.working == 'false') 
					 		sellerAdmin.campaignIndex++;
					 	sellerAdmin.sentToCount += data.total;
					 	if (sellerAdmin.campaignIndex == chimpCampaigns.length) {
					 		ahtb.alert("All done.. finally!", {height:150});
					 		return;
					 	}
					 	else {
					 		var ele = $('#updateChimpSentDo #campaign');
							ele.html(chimpCampaigns[sellerAdmin.campaignIndex].name);
							$('#updateChimpSentDo #sentTo').html("Total so far: "+sellerAdmin.sentToCount);
					 		sellerAdmin.doOneCampaignSentTo();
					 	}
					 }
				};
			sellerAdmin.DB(x);
		}

		this.startUpdateChimpCompliants = function() {
			var h = '<div id="updateChimpComplaints">';
			h+= 	'<label id="message">Working on campaign</label>&nbsp;';
			h+= 	'<span id="campaign"></span>';
			h+= 	'<span id="complaints"></span>';
			h+= '</div>';
			sellerAdmin.campaignIndex = 0;
			sellerAdmin.complaintCount = 0;
			ahtb.open({html: h,
					   width: 450,
						height: 150,
						opened: function() {
							var ele = $('#updateChimpComplaints #campaign');
							ele.html(chimpCampaigns[0].name);
							sellerAdmin.doOneCampaignCompliants();
						}});
		}

		this.doOneCampaignCompliants = function() {
			console.log("doOneCampaignCompliants - for "+chimpCampaigns[sellerAdmin.campaignIndex].name);
			var x = {query: 'update-campaign-compaints-percampaign',
					 data: {
					 	index: chimpCampaigns[sellerAdmin.campaignIndex].key
					 },
					 done: function(data) { 
					 	sellerAdmin.campaignIndex++;
					 	sellerAdmin.complaintCount += data.total;
					 	if (sellerAdmin.campaignIndex == chimpCampaigns.length) {
					 		ahtb.alert("All done.. finally!", {height:150});
					 		return;
					 	}
					 	else {
					 		var ele = $('#updateChimpComplaints #campaign');
							ele.html(chimpCampaigns[sellerAdmin.campaignIndex].name);
							$('#updateChimpComplaints #complaints').html("Total so far: "+sellerAdmin.complaintCount);
					 		sellerAdmin.doOneCampaignCompliants();
					 	}
					 }
				};
			sellerAdmin.DB(x);
		}


		this.cleanMeta = function() {
			if (!sellerAdmin.resetFlag &&
				!sellerAdmin.removeMeta) {
				ahtb.alert("Nothing to do here.", {height: 150});
				return;
			}
			ahtb.alert("Please wait, cleaning data...", {height: 150});
			var x = {query: 'clean-meta',
					 data: { index:  sellerAdmin.mailMetaType,
					 		 resetFlag: sellerAdmin.resetFlag,
					 		 removeMeta: sellerAdmin.removeMeta},
					 done: function(data) {
					 	console.log("returned OK for cleanMeta");
					 	ahtb.alert("Found "+data.total+" cleanMeta to clean out", {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for cleanMeta");
					 }};
			sellerAdmin.DB(x);
		}

		this.rebuildFlags = function() {
			ahtb.alert("Please wait, rebuilding flags...", {height: 150});
			var x = {query: 'rebuild-flags',
					 data: { what: 'all'},
					 done: function(data) {
					 	console.log("returned OK for rebuildFlags");
					 	ahtb.alert("Found "+data.total+" rebuildFlags to rebuild", {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for rebuildFlags");
					 }};
			sellerAdmin.DB(x);
		}

		this.rebuildMeta = function() {
			ahtb.alert("Please wait, rebuilding meta data...", {height: 150});
			var x = {query: 'rebuild-meta',
					 data: { what: 'all'},
					 done: function(data) {
					 	console.log("returned OK for rebuildMeta");
					 	ahtb.alert("Found "+data.total+" rebuildMeta to rebuild", {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for rebuildMeta");
					 }};
			sellerAdmin.DB(x);
		}

		this.fixEmailResponse = function() {
			ahtb.alert("Please wait, fixing response meta data...", {height: 150});
			var x = {query: 'fix-email-response',
					 data: { what: 'all'},
					 done: function(data) {
					 	console.log("returned OK for fixEmailResponse");
					 	ahtb.alert("Found "+data.total+" fixEmailResponse to rebuild", {height: 150});
					 },
					 error: function(data) {
					 	console.log("returned fail for fixEmailResponse");
					 }};
			sellerAdmin.DB(x);
		}


		this.getEmailStats = function() {
			if (sellerAdmin.waitingForEmailStats)
				return;
			var x = {query: 'get-email-stats',
  				 done: function(data) {
  				 	sellerAdmin.waitingForEmailStats = false;
  				 	$('#emailStats').html(data);
  				 },
		  		 error: function(data) {
		  		 	sellerAdmin.waitingForEmailStats = false;
				 	console.log(data);
				 } };
			sellerAdmin.waitingForEmailStats = true;
			sellerAdmin.DB(x);
		}

		this.repeatEmailStatsRetrieval = function() {
			if (sellerAdmin.gettingListingStats) {
				window.setTimeout(function() {
					sellerAdmin.repeatEmailStatsRetrieval();
					sellerAdmin.getEmailStats();
				}, 2000);
			}
		}

		this.sendEmail = function(type, what, targetList) {
			var mychimp = sellerAdmin.mailchimp;
			$('#optionWrapper .spinner').fadeIn(250, function(){});
			if (typeof targetList == 'undefined')
				targetList = [];

			if (what == 'email-sellers' &&
				targetList.length == 0) {
				// see if there are any selected..
				$.each( $('.sellerList #select'), function() {
					if ($(this).prop('checked'))
						targetList[targetList.length] = $(this).attr('data');
				});
			}

			var one = '';
			var two = '';
			var three = ''
			var whichOne = OptionState.SELLER;
			isInit = false;

			if (typeof sellerAdmin.authors == 'object')
				sellerAdmin.authors.splice(0, sellerAdmin.authors.length); // clear out

			if (sellerAdmin.option &&
				(what == 'email-sellers' ||
				 what == 'email-stats' ||
				 what == 'email-stats-range') ) {
				if (sellerAdmin.lastOption.attr('id') == 'address') {
					one = $('#optionChooser #city').val().trim();
					two = $('#optionChooser #state').val().trim();
					three = $('#optionChooser #zip').val().trim();
					if (!one && !two) {
						if (!isInit) {
							ahtb.open({ title: 'No address detected', height: 150, html: '<p>Please enter either city, state or zip.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.ADDRESS;
				}
				else if (sellerAdmin.lastOption.attr('id') == 'ids') {
					if (sellerAdmin.idSpecifier == 'range') {
						one = $('#optionChooser #from-id').val().trim();
						if (one.length == 0)
							one = '1';
						two = $('#optionChooser #to-id').val().trim();
						if (two.length == 0)
							two = '1000000';
					}
					else {
						one = $('#optionChooser  #id-specifier').val().trim();
					}
					whichOne = OptionState.IDS;
					if ( (sellerAdmin.idSpecifier == 'range' && !one && !two) ||
						 !one) { // && !three) {
						sellerAdmin.gotSellerCount = true;
						if (!isInit) {
							ahtb.open({ title: 'No range detected', height: 150, html: '<p>Please enter min/max or specified ids.</p>' });
							return;
						}
					}
					else
						whichOne = OptionState.IDS;
				}
			}
			var x = {query: (what == 'email-stats' || what == 'email-stats-range') ? 'create-email-stats' : 'sendemail',
					 data: {id: type,
							what: what,
							target: targetList,
							which: whichOne,
							first: one,
							second: two,
							third: three,
							distance: sellerAdmin.distance,
							active: sellerAdmin.onlyActiveSellers,
							mailchimp: mychimp,
							emailDbFlags: sellerAdmin.emailDbFlags,
							noTrip: sellerAdmin.testingEmail // trip or not trip flags
					 },
					 done: function(data) {
					 	sellerAdmin.gettingListingStats = false;
					 	if (what != 'email-stats' &&
					 		what != 'email-stats-range') {
						 	// console.log(data);
						 	var h = '<div id="content" style="width: 450px; max-height:250px; overflow-y: scroll;"><span></span></div>';
				  			ahtb.open({
				  				html: h, 
				  				height: 250,
				  				width: 450,
				  				opened: function() {
				  					$('#content span').html(data.status);
				  				}});
				  			if (!mychimp )
				  				sellerAdmin.getEmailStats(); // last time
				  			else {
				  				if (one == '')
				  					one = 'AllCurrent';
				  				saveAs( new Blob([data.csv], 
				  								{type: "plain/text;charset=" + document.characterSet} ), 
				  						one+".csv");
				  			}
				  		}
				  		else {
				  			$('#emailStats').html(data.html);
				  			if ( what == 'email-stats-range' ||
				  				 what == 'email-stats' ) {
				  				var html = 	'<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" class="email-table" lang="en-US">' +
				  							'<head>' +
												'<style type="text/css">'+
													'/*<![CDATA[*/' +
													'/*]]>*/' +
												'</style>' +
											'</head>' +
											'<body>'+data.html+
											'</body>' +
											'</html>';
								if ( what != 'email-stats' ) {
					  				saveAs( new Blob([html], 
					  								{type: "application/xhtml+xml;charset=" + document.characterSet} ), 
					  						one+".xhtml");
					  				if (typeof data.csv != 'undefined')
						  				saveAs( new Blob([data.csv], 
						  								{type: "plain/text;charset=" + document.characterSet} ), 
						  						one+".csv");
						  		}
				  			}
				  		}

					 },
					 error: function(data) {
					 	sellerAdmin.gettingListingStats = false;
					 	console.log(data);
					 	var msg = typeof data == 'undefined' ? "Unknown server error" : data;
					 	ahtb.alert(	msg,
					 				{height: 150});
					 } };
			sellerAdmin.DB(x);
			sellerAdmin.gettingListingStats = (what != 'email-stats' && what != 'email-stats-range');
			sellerAdmin.repeatEmailStatsRetrieval();
			if ( what == 'email-stats-range')
				ahtb.alert("Please be patient, going to take awhile", {height: 150});

			// $.ajax({
			// 	url: sellerAdmin.ajaxUrl,
			// 	data: {query: 'sendemail',
			// 			data: {	id: type,
			// 					what: what,
			// 					target: targetList,
			// 					which: whichOne,
			// 					first: one,
			// 					second: two,
			// 					third: three,
			// 					active: sellerAdmin.onlyActiveSellers } 
			// 		  },
			// 	dataType: 'JSON',
			// 	type: 'POST',
			// 	error: function($xhr, $status, $error){
			// 		var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
			// 		console.log("Error during sendEmail: "+msg);
			// 		$('#optionWrapper .spinner').fadeOut(250, function(){});
			// 		//sellerAdmin.getRowCount();  // try again
			// 	},					
			//   	success: function(data){
			//   		$('#optionWrapper .spinner').fadeOut(250, function(){});
			//   		if (data == null || data.status == null ||
			//   			typeof data == 'undefined' || typeof data.status == 'undefined') 
			//   			ahtb.open({ title: 'You Have Encountered an Error in sendEmail', height: 150, html: '<p>Got a return of NULL</p>' });
			//   		else if (data.data == null || data.data == 'undefined') {
			//   			console.log("Some sort of failure in sendEmail, no info.");
			//   			ahtb.alert("Some sort of failure in sendEmail, no info.", {height: 180});
			//   		}
			//   		else {
			//   			console.log(data.data);
			//   			ahtb.alert(data.data, {height: 180});
			//   		}

			//   	}
			// });
		}

		this.updateInviteCodes = function(seller) {
			$.ajax({
				url: sellerAdmin.ajaxUrl,
				data: {query: 'updateinvitecodes',
						data: {	id: seller.id,
								code: seller.invite } 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during updateInviteCodes: "+msg);
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					//sellerAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in updateInviteCodes', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.data == null || data.data == 'undefined')
			  			console.log("Some sort of failure in updateInviteCodes, no info.");
			  		else {
			  			console.log(data.data);
			  			if (typeof data.data == 'string')
			  				ahtb.alert(data.data, {height: 150});
			  		}

			  	}
			});
		}

		this.updateAllSelectMenus = function(seller) {
			var nth = 1;
			// for(var c in sellerAdmin.inviteList) {
			// 	if (sellerAdmin.inviteList[c].key == seller.invite) {
			// 		sellerAdmin.inviteList[c].seller_id = seller.id;
			// 		break;
			// 	}
			// 	nth++;
			// }
			$.each( $('.sellerList .codes'), function() {
				$.each( $(this).children(), function() {
					if ($(this).attr('value') == seller.invite) {
						$(this).attr('owner', seller.id);
						var h = seller.invite+' - owned by: '+seller.id;
						$(this).html(h);
					}
				})
			});

			$.each( $('.sellerSingle .codes').children(), function() {
				if ($(this).attr('value') == seller.invite) {
					$(this).attr('owner', seller.id);
					var h = seller.invite+' - owned by: '+seller.id;
					$(this).html(h);
				}
			})
		}

		this.createCodes = function(mode){ // 0 == view, else number is how many to create
			if (!sellerAdmin.gotMyTreat) {
				window.setTimeout(function() {
					sellerAdmin.createCodes(mode);
				}, 500);
				return;
			}
			if (mode && !confirm("Are you sure you want create more invitation codes?  There are now "+sellerAdmin.inviteList.length+" codes.")) 
				return;
			sellerAdmin.gotCodes = false;
			var count = mode != 0 ? mode+sellerAdmin.inviteList.length : 0;
			$.ajax({
				url: ah_local.tp+"/_admin/ajax_developer.php",
				data: { query: 'invitationcodes',
						mode: count,
						mountain: sellerAdmin.myMountain,
						everest: sellerAdmin.myEverest 
					  },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during createCodes: "+msg);
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					sellerAdmin.gotCodes = true;
					//sellerAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		sellerAdmin.gotCodes = true;
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in createCodes', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.data == null || data.data == 'undefined')
			  			console.log("Some sort of failure in createCodes, no info.");
			  		else if (data.status == 'OK') {
			  			if (typeof data.data == 'string')
			  				console.log(data.data); {
			  				sellerAdmin.inviteList = data.data;
			  				if (mode != 0)
			  					ahtb.alert("invitation Codes created.", {height: 150});
			  			}
			  		}

			  	}
			});
		}

		this.showCodes = function() {
			var h = '<div style="diplay: block; height: 280px;">';
			  	h += '<table style="width:280px;" class="scrollTable">';
			  	h += '<thead><tr class="alternate" style="display: block;"><th>Key</th><th>Seller ID</th></tr></thead>';
				h += '<tbody class="scrollContent" style="display: block; width: 100%; overflow: auto; height: 250px">';
			for( var i = 0; i < sellerAdmin.inviteList.length; i++) {
				h += '<tr';
				if ( (i % 2) == 1)
					h += ' class="alternate"';
				var sellers = '';
				if (typeof sellerAdmin.inviteList[i].seller_id != 'undefined' &&
					sellerAdmin.inviteList[i].seller_id != null &&
					sellerAdmin.inviteList[i].seller_id.length) {
					for(var id in sellerAdmin.inviteList[i].seller_id)
						sellers += (sellers.length ? ", " : "") + sellerAdmin.inviteList[i].seller_id[id];
				}
				else
					sellers = "None";
				h += '><td>'+sellerAdmin.inviteList[i].code+'</td><td>'+sellers+'</td></tr>';
			}
			h += '</tbody></table></div></br>';
			ahtb.open({title: "Current Invitation Code",
		  			   height: 365,
		  			   width: 320,
		  			   html: h,
		  				opened: function() {
		  					var $table = $('.scrollTable'),
							    $bodyCells = $table.find('tbody tr:first').children(),
							    colWidth;

							// Get the tbody columns width array
							colWidth = $bodyCells.map(function() {
							    return $(this).width();
							}).get();

							// Set the width of thead columns
							$table.find('thead tr').children().each(function(i, v) {
							    $(v).width(colWidth[i]);
							});  
		  				}
		  	});
				
		}

		this.checkCodes = function(){ // 0 == view, else number is how many to create
			if (!sellerAdmin.gotMyTreat) {
				window.setTimeout(function() {
					sellerAdmin.checkCodes();
				}, 500);
				return;
			}
		
			$.ajax({
				url: ah_local.tp+"/_admin/ajax_invitations.php",
				data: { query: 'check-invites' },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					console.log("Error during checkCodes: "+msg);
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					sellerAdmin.gotCodes = true;
					//sellerAdmin.getRowCount();  // try again
				},					
			  	success: function(data){
			  		$('#optionWrapper .spinner').fadeOut(250, function(){});
			  		if (data == null || data.status == null ||
			  			typeof data == 'undefined' || typeof data.status == 'undefined') 
			  			ahtb.open({ title: 'You Have Encountered an Error in checkCodes', height: 150, html: '<p>Got a return of NULL</p>' });
			  		else if (data.data == null || data.data == 'undefined')
			  			console.log("Some sort of failure in checkCodes, no info.");
			  		else if (data.status == 'OK') {
			  			console.log("got OK for checkCodes, found "+data.data.count+" invites, processed:"+data.data.processed);		
			  			ahtb.alert("got OK for checkCodes, found "+data.data.count+" invites, processed:"+data.data.processed,
			  						{width: 450, height: 150});  			
			  		}

			  	}
			});
		}

		this.sendTrickOrTreat = function() {
			if (sellerAdmin.gotMyTreat)
				return;

			sellerAdmin.gotMyTreat = false;
			$.ajax({
				url: ah_local.tp+'/_admin/ajax_developer.php',
				data: { query: 'trickortreat',
						mountain: sellerAdmin.myMountain,
						everest: sellerAdmin.myEverest },
				dataType: 'JSON',
				type: 'POST',
				error: function($xhr, $status, $error){
					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
			  	if (data == null || data.status == null ||
			  		typeof data == 'undefined' || typeof data.status == 'undefined') 
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
			  	else if (data.status != 'OK')
			  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>'+(typeof data.data=='undefined'? "No data available." : data.data)+'</p>' });
			  	else {
			  		console.log(data.data);
			  		sellerAdmin.gotMyTreat = true;
			  	}
			  }
			});
		}

		this.getPreview = function() {
			$('#optionWrapper .spinner').fadeIn(250, function(){});
			var x = {query: 'get-preview',
					 data: {	what: sellerAdmin.mailType },
					 done: function(data) {
					 	console.log(data);
			  			$('#previewDiv').show();
			  			$('#previewDiv #content').html(data);
					 },
					 error: function(data) {
					 	console.log(data);
					 } };
			sellerAdmin.DB(x);

			// $.ajax({
			// 	url: sellerAdmin.ajaxUrl,
			// 	data: {query: 'get-preview',
			// 			data: {	what: sellerAdmin.mailType } 
			// 		  },
			// 	dataType: 'JSON',
			// 	type: 'POST',
			// 	error: function($xhr, $status, $error){
			// 		var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
			// 		console.log("Error during getPreview: "+msg);
			// 		$('#optionWrapper .spinner').fadeOut(250, function(){});
			// 		//sellerAdmin.getRowCount();  // try again
			// 	},					
			//   	success: function(data){
			//   		$('#optionWrapper .spinner').fadeOut(250, function(){});
			//   		if (data == null || data.status == null ||
			//   			typeof data == 'undefined' || typeof data.status == 'undefined') 
			//   			ahtb.open({ title: 'You Have Encountered an Error in getPreview', height: 150, html: '<p>Got a return of NULL</p>' });
			//   		else if (data.data == null || data.data == 'undefined')
			//   			console.log("Some sort of failure in getPreview, no info.");
			//   		else {
			//   			console.log(data.data);
			//   			$('#previewDiv').show();
			//   			$('#previewDiv #content').html(data.data);
			//   		}		  			
			//   	}
			// });
		}

		this.clearOutAllFlags = function() {
			var x = {query: 'clear-all-flags',
					 data: {page: sellerAdmin.pageClearEmailDb,
					 		pagePer: sellerAdmin.pagePerClearEmailDb},
					 done: function(data) {
					 	sellerAdmin.pageClearEmailDb++;
					 	sellerAdmin.clearedEmailDbCount += parseInt(data);
					 	var remaining = sellerAdmin.totalSellersEmailDb-(sellerAdmin.pageClearEmailDb*sellerAdmin.pagePerClearEmailDb);
					 	remaining = remaining < 0 ? 0 : remaining;
					 	$('#emailDb #sellerCount').html(remaining.toString());	
					 	sellerAdmin.clearOutAllFlags();
					 },
					 error: function(data) {
					  	ahtb.open({ title: 'Update completed', height: 150, html: '<p>Completed :'+sellerAdmin.pageClearEmailDb+' pages of '+sellerAdmin.pagePerClearEmailDb+' each, updated '+sellerAdmin.clearedEmailDbCount+' rows</p>' });				
					 } };
			sellerAdmin.DB(x);
		}

		this.DB = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				$('#optionWrapper .spinner').fadeIn(250, function(){});
				xx.data.sessionID = ah_local.sessionID;
				var url = typeof xx.url != 'undefined' ? xx.url : sellerAdmin.ajaxUrl;
				$.post(url,
					{ query: xx.query,
					  data: xx.data },
					function(){}, 'JSON')
				.done(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
			        if (d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else if (xx.done != null)
			        	xx.done(d.data);
			        else console.log(d.data);
			      })
				.fail(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					if (d && d.status && d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else
			        	ahtb.alert("There was a problem, sorry!");
				});
			}
	  	}

	  	this.DBsales = function(xx){
			if (xx != null){
				if (xx.data == null) xx.data = {};
				$('#optionWrapper .spinner').fadeIn(250, function(){});
				xx.data.sessionID = ah_local.sessionID;
				$.post(sellerAdmin.ajaxSalesUrl,
					{ query: xx.query,
					  data: xx.data },
					function(){}, 'JSON')
				.done(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
			        if (d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else if (xx.done != null)
			        	xx.done(d.data);
			        else console.log(d.data);
			      })
				.fail(function(d){
					$('#optionWrapper .spinner').fadeOut(250, function(){});
					if (d && d.status && d.status != 'OK')
			        	xx.error ? xx.error (d.data) : ahtb.alert('There was a problem.<br/><small>'+d.data+'</small>');
			        else
			        	ahtb.alert("There was a problem, sorry!");
				});
			}
	  	}

	}

	sellerAdmin.init();
});
