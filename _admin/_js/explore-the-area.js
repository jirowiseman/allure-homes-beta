var exploreAdmin;

function explore_loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false&callback=explore_initialize';
	document.body.appendChild(script);
}

function explore_initialize() {
	exploreAdmin.init();
}

jQuery(document).ready(function ($) {
	exploreAdmin = {
		ajaxURL: ah_local.tp + "/_admin/ajax.php",
		init: function () {
			var self = this;
			$('#nearby-search').on('click', function () {
				self.nearby_search.init();
			});
			$('#add-manual').on('click', function () {
				self.add_manual.init();
			});
		},
		nearby_search: {
			init: function () {
				var self = exploreAdmin;

				$("#the-menu .active").removeClass('active');
				$("#the-menu #nearby-search").addClass('active');
				var $html = '<h2>Nearby Search</h2>' +
					'<p class="content">' +
					'Enter a location to search nearby:<br/>' +
					'<input type="text" id="nearloc" placeholder="Enter a location" style="width: 40em; display: block; margin: 0 0 .25em" />' +
					'<button id="location_confirm">Next</button>' +
					'</p>';
				$("#alr-content").html($html);

				self.autocomplete = new google.maps.places.Autocomplete(
					/* @type {HTMLInputElement} */
					(document.getElementById('nearloc')), {
						types: ['geocode']
					}
				);

				google.maps.event.addListener(self.autocomplete, 'place_changed', function () {
					$('#location_confirm').click();
				});

				$('#location_confirm').one('click', function () {
					self.nearby_search.get_search_query(self.autocomplete.getPlace());
				});
			},
			get_search_query: function (place) {
				self = exploreAdmin;
				$('#alr-content p').fadeOut(125, function () {
					$html = 'Enter a search query:<br/>' +
						'<div id="autocomplete-map" style="width: 450px;"></div>'+
						'<input type="text" id="nearsearch" placeholder="Enter a search query" style="width: 40em; display: block; margin: 0 0 .25em" />' +
						'<button id="location_confirm">Next</button>';
					$(this).html($html).fadeIn(125, function(){

						self.map = new google.maps.Map(document.getElementById('autocomplete-map'), {
							center: new google.maps.LatLng(place.geometry.location.k, place.geometry.location.B),
						});
						self.places_service = new google.maps.places.PlacesService(self.map);

						$("#alr-content").keypress(function(e) {
						  if ( e.which == 13 ) {
						     e.preventDefault();
						     $('#location_confirm').click();
						  }
						});

						$('#location_confirm').one('click', function () {
							var search_query = $.trim($('#nearsearch').val());

							self.places_service.nearbySearch({
								location: place.geometry.location,
								radius: 50000,
								keyword: search_query,
							}, function (results, status) {
								if (status != 'OK')
									ahtb.edit({
										height: 75,
										width: 450,
										html: '<p><strong>Google Error:</strong></p><p>' + status + '</p>'
									});
								else
									self.nearby_search.parse_results(results);
							});
						});
					});
				});
			},
			parse_results: function (results) {
				self = exploreAdmin;
				$('#alr-content p').fadeOut(125, function () {
					self.nearby_search.results = [];

					var $html = '<select style="margin-right: 1em;" class="place-category">';
					for (var pi in point_categories)
						$html += '<option value="' + point_categories[pi].id + '">' + point_categories[pi].category + '</option>';
					$html += '</select> <button id="location_confirm">Save Checked</button>'+
						'<table class="places-list widefat" style="width:100%;">' +
						'<thead>'+
						'<th class="check-column"><input type="checkbox" /></th>'+
						'<th>Name</th>'+
						'<th>Address</th>'+
						'</thead>'+
						'<tbody>';
					for (var i in results) {
						self.nearby_search.results[i] = {
							id: parseInt(i),
							name: results[i].name,
							lat: results[i].geometry.location.lat(),
							lng: results[i].geometry.location.lng(),
							address: results[i].vicinity,
							meta: {
								google: {
									place_id: results[i].place_id
								}
							}
						};
						$html += '<tr place-index="' + i + '" class="place' + (i % 2 ? ' alternate' : '') + '">'+
						'<td><input type="checkbox" /></td>'+
						'<td><strong>' + results[i].name + '</strong></td>'+
						'<td>' + results[i].vicinity + '</td>'+
						'</tr>';
					}
					$html += '</tbody>'+
					'</table>';

					$(this).html(self.nearby_search.results.length ? $html : '<strong><p>No results.</p></strong>').fadeIn(125);

					$('#location_confirm').one('click', function () {
						var $new_poi = [];
						$('td input[type=checkbox]').each(function(){
							if ($(this).prop('checked')){
								var $p = self.nearby_search.results[parseInt($(this).parent().parent().attr('place-index'))];
								$p.category = parseInt($('select.place-category').val());
								$new_poi.push( $p );
							}
						});

						self.add_to_db($new_poi);
					});

					$('th input[type=checkbox]').change(function(){
						var checked = $(this).prop('checked');
						$('td input[type=checkbox]').prop('checked', checked);
					});
				});
			}
		},
		add_manual: {
			init: function(){
				var self = exploreAdmin;

				$("#the-menu .active").removeClass('active');
				$("#the-menu #nearby-search").addClass('active');
				var $html = '<h2>Add Manually</h2>' +
					'<form class="content">' +
					'<table>'+
					'<tr><td>Name:</td><td><input type="text" placeholder="Point Name" name="name" /></td></tr>' +
					'<tr><td>Address:</td><td><input type="text" placeholder="Address" name="address" /></td></tr>' +
					'<tr><td>Latitude:</td><td><input type="text" placeholder="Latitude" name="lat" /></td></tr>' +
					'<tr><td>Longitude:</td><td><input type="text" placeholder="Longitude" name="lng" /></td></tr>' +
					'<tr><td>Category:</td><td><select name="category"><option value="" selected>(choose a category)</option>';
					for (var pi in point_categories)
						$html += '<option value="' + point_categories[pi].id + '">' + point_categories[pi].category + '</option>';

				$html+= '</select></td></tr>' +
					'</table>'+
					'<button id="location_confirm">Next</button>' +
					'</form>';
				$("#alr-content").html($html);

				$("form.content").on( "submit", function(e) {
					e.preventDefault();
					var $a = $(this).serializeArray();
					var $poi = {};
					for (var ai in $a)
						$poi[$a[ai].name] = $a[ai].value === "" ? null : $a[ai].value;

					$('form.content input, form.content select').css('border', '1px solid #ddd');
					$err = false;
					for (var i in $poi){
						if (i === 'lat' || i === 'lng')
							$poi[i] = parseFloat($poi[i]);
						else if ( i === 'category' )
							$poi[i] = parseInt($poi[i]);

						if ((i === 'name' || i === 'lat' || i === 'lng' || i === 'category') && !$poi[i]){
							$err = true;
							$('[name='+i+']').css('border', '1px solid red');
						}
					}

					if (!$err)
						self.add_to_db( [$poi] );
				});
			}
		},
		add_to_db: function($poi){
			var self = this;
			ahtb.loading('Saving...',{
				height: 125,
				opened: function(){
					self.DB({
						query: 'add-points',
						data: $poi,
						done: function (d) {
							$('#alr-content').html('<p>Added '+d.length+' point'+(d.length > 1 ? 's' : '')+'.</p>');
							ahtb.close();
						}
					});
				}
			});
		},
		DB: function (x) {
			var self = this;
			self.dbQueue = self.dbQueue || [];
			self.dbRunning = self.dbRunning || false;
			if (x) {
				if (self.dbRunning) {
					x.id = self.dbQueue.length;
					self.dbQueue.push(x);
				} else {
					self.dbRunning = true;
					if (x.before) x.before();
					$.post(self.ajaxURL, {
						query: x.query,
						data: (x.data ? x.data : null)
					}, function () {}, 'json').done(function (d) {
						self.DBdone();
						if (d.status != 'OK') {
							ahtb.alert(d.data, 'Error', {
								height: 60 + (16 * Math.round(d.data.length / 75))
							});
							console.log(d);
						} else if (x.done) x.done(d.data);
						else console.log(d);
					});
				}
			}
		},
		DBdone: function () {
			this.dbRunning = false;
			this.DB(self.dbQueue.shift());
		}
	};
	explore_loadScript();
});