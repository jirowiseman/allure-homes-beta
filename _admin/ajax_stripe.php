<?php
// 1. Autoload the SDK Package. This will include all the files and classes to your autoloader
// Used for composer based installation
require_once (__DIR__  . '/../../../plugins/vendor/autoload.php');
require_once(__DIR__.'/../_classes/Logger.class.php');
require_once(__DIR__.'/../../../../wp-load.php');

global $sLogger;
$sLogger = new AH\Logger(1, 'ajax-stripe');
$sLogger->log("ajax_stripe entered, uri:".$_SERVER['REQUEST_URI']);

$homeURL = get_home_url();
$isSandBox = false;
if (strpos($homeURL, 'alpha') !== false)
	$isSandBox = true;
// your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
if ($isSandBox)
	\Stripe\Stripe::setApiKey("sk_test_09ic10X7SOuFSlgSwUgPcDYa");
else
	\Stripe\Stripe::setApiKey("sk_live_4YjHoMQXbq448BPh17zQVDbB");


require_once(__DIR__.'/../_classes/Stripe.class.php'); $Stripe = new AH\Stripe(1);

function decodeCustomerData(&$customerInfo) {
	if (empty($customerInfo))
		return;

	foreach($customerInfo as $id=>$data)
		if (is_numeric($data))
			$customerInfo->$id = is_float($data) ? floatval($data) : intval($data);
		else
			$customerInfo->$id = json_decode($data);
}

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input", true);
$sLogger->log("input received:".print_r($input, true));
$event_json = json_decode($input);

// Do something with $event_json
$sLogger->log("isSandBox:".($isSandBox ? 'yes' : 'no').", event_json received:".print_r($event_json, true));

if (!$isSandBox) {
	// $event = \Stripe\Event::retrieve($event_json->id);
	// $sLogger->log("event received:".print_r($event, true));
	if (!isset($event_json->data->object->metadata) || empty($event_json->data->object->metadata)) {
		$sLogger->log("ajax_stripe find customerInfo emtpy");
		http_response_code(400); // PHP 5.4 or greater
	}
	$customerInfo = $event_json->data->object->metadata;
	decodeCustomerData($customerInfo);

	// add charge id to customerInfo

	$sLogger->log("customerInfo received:".print_r($customerInfo, true));

	switch($event_json->type) {
		case 'charge.succeeded':
			$Stripe->bought($customerInfo);
			break;

		case 'charge.refunded':
			$Stripe->refunded($customerInfo);
			break;

		default:
			$sLogger->log("ajax_stripe not handling $event_json->type");
			break;
	}
}

http_response_code(200); // PHP 5.4 or greater