<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
class AJAX_TomDev extends Controller {
	public function __construct(){
		$in = parent::__construct();
		$out = 'WAS_NEVER_SET';
		try {
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			switch ($in->query){
					case 'clear-base-log':
						$log = new Log(__DIR__.'/../_classes/_logs/base_class_log');
						$log->clear();
						unset($log);
						$out = new Out('OK');
						break;
					case 'clear-exception-log':
						$log = new Log(__DIR__.'/../_classes/_logs/exception_log');
						$log->clear();
						unset($log);
						$out = new Out('OK');
						break;
					case 'check-all-tables':
						require_once(__DIR__.'/../_classes/Tables.class.php'); $t = new Tables();
						$out = $t->checkAllTables();
						$out = new Out($out ? 'OK' : 0, $out);
						break;
					case 'generate-clean-images':
						unset($_POST);
						if ( empty($in->data) ){
							$out = $this->getClass('Image')->generateCleanFile();
							$out = new Out($out ? 'OK' : 0, $out);
						} else
							$out = new Out('OK',
								$this->getClass('Image')->generateCleanFileType(
									(isset($in->data['type']) ? $in->data['type'] : null),
									(isset($in->data['dir']) ? $in->data['dir'] : null),
									(isset($in->data['page']) ? $in->data['page'] : null),
									(isset($in->data['rows']) ? $in->data['rows'] : 500)
								)
							);
						break;
					case 'run-clean-images':
						unset($_POST);
						$out = $this->getClass('Image')->runCleanFile();
						$out = new Out($out ? 'OK' : 0, $out);
						break;
				/* Listings */
					case 'geocode-listings':
						$start = isset($in->data['start']) ? intval($in->data['start']) : 0;
						$size = (isset($in->data['size']) && intval($in->data['size']) > 0 ? intval($in->data['size']) : 10);
						$x = $this->getClass('Listings', 1)->geocodeListings($start, $size);
						$out = new Out('OK', ['remaining' => $this->getClass('Listings')->countListingsWithoutGeoinfo(), 'results' => $x ]);
						break;
					case 'listings-cities-pivot':
						$out = new Out('OK',
							[
								'results' => $this->getClass('Cities')->createListingsPivot(0, (isset($in->data)&&intval($in->data)>0 ? intval($in->data) : 100)),
								'remaining' => $this->getClass('Listings')->count((object)[ 'where' => [ 'city_id' => null ] ]),
							]
						);
						break;
				/* Cities */
					case 'geocode-cities':
						$x = $this->getClass('Cities', 1)->geocodeCities(0, (isset($in->data) ? intval($in->data) : 10));
						if (!isset($x->status))
							$x->remaining = $this->getClass('Cities')->count((object)[ 'where' => [ 'lat' => null, 'lng' => null ] ]);
						$out = new Out('OK', $x );
						break;
					case 'import-cities-from-listings':
						$per_page = 1500;
						$page = (round($this->getClass('Listings')->count() / $per_page) < $in->data) ? 'done' : ($in->data == null ? 0 : $in->data);
						if ($page === 'done')
							$out = new Out('OK', 'done');
						else $out = ( $x = $this->getClass('Cities')->importFromListings($page, $per_page) ) ?
							new Out('OK', round( $page * 100/ round($this->getClass('Listings')->count() / $per_page), 2) ) :
							new Out(0, $x);
						break;
					case 'count-cities':
						$out = new Out('OK', $this->getClass('Cities')->countCities());
						break;
				/* DB Cleaner */
					case 'clean-extra-rows':
						$run_it = true; // actually delete the rows

						$out = [];
						$Listings = $this->getClass('Listings')->get( (object)['what'=>array('id')] ); $Listings = wp_list_pluck($Listings, 'id');
						$Tags = $this->getClass('Tags')->get( (object)['what'=>array('id')] ); $Tags = wp_list_pluck($Tags, 'id');
						$Points = $this->getClass('Points')->get( (object)['what'=>array('id')] ); $Points = wp_list_pluck($Points, 'id');
						$PointsCategories = $this->getClass('PointsCategories')->get( (object)['what'=>array('id')] ); $PointsCategories = wp_list_pluck($PointsCategories, 'id');
						$QuizQuestions = $this->getClass('QuizQuestions')->get( (object)['what'=>array('id')] ); $QuizQuestions = wp_list_pluck($QuizQuestions, 'id');
						$extra = (object) [];

						/* Listings */
						$ListingsTags = $this->getClass('ListingsTags')->get((object)[ 'what'=>['tag_id','listing_id'] ]);
						if (!empty($ListingsTags))
						$extra->ListingsTags = [
							'tag_id'=>array_values(array_diff(array_unique(wp_list_pluck($ListingsTags, 'tag_id')), $Tags)),
							'listing_id'=>array_values(array_diff(array_unique(wp_list_pluck($ListingsTags, 'listing_id')), $Listings))
						];
						$ListingsGeoinfo = $this->getClass('ListingsGeoinfo')->get((object)[ 'what'=>['listing_id'] ]);
						if (!empty($ListingsGeoinfo))
						$extra->ListingsGeoinfo = [
							'listing_id'=>array_values(array_diff(array_unique(wp_list_pluck($ListingsGeoinfo, 'listing_id')), $Listings))
						];
						$ListingsPoints = $this->getClass('ListingsPoints')->get((object)[ 'what'=>['point_id','listing_id'] ]);
						if (!empty($ListingsPoints)) $extra->ListingsPoints = [
							'point_id'=>array_values(array_diff(array_unique(wp_list_pluck($ListingsPoints, 'point_id')), $Points)),
							'listing_id'=>array_values(array_diff(array_unique(wp_list_pluck($ListingsPoints, 'listing_id')), $Listings))
						];
						/* Points */
						$PointsTaxonomy = $this->getClass('PointsTaxonomy')->get((object)[ 'what'=>['point_id','category_id'] ]);
						if (!empty($PointsTaxonomy))
						$extra->PointsTaxonomy = [
							'point_id'=>array_values(array_diff(array_unique(wp_list_pluck($PointsTaxonomy, 'point_id')), $Points)),
							'category_id'=>array_values(array_diff(array_unique(wp_list_pluck($PointsTaxonomy, 'category_id')), $PointsCategories))
						];
						/* Quiz */
						$QuizSlides = $this->getClass('QuizSlides')->get((object)[ 'what'=>['id','question_id'] ]);
						if (!empty($QuizSlides))
						$extra->QuizSlides = [
							'question_id'=>array_values(array_diff(array_unique(wp_list_pluck($QuizSlides, 'question_id')), $QuizQuestions))
						];
						$QuizTags = $this->getClass('QuizTags')->get((object)[ 'what'=>['slide_id','tag_id'] ]);
						if (!empty($QuizTags))
						$extra->QuizTags = [
							'slide_id'=>array_values(array_diff(array_unique(wp_list_pluck($QuizTags, 'slide_id')), wp_list_pluck($QuizSlides, 'id'))),
							'tag_id'=>array_values(array_diff(array_unique(wp_list_pluck($QuizTags, 'tag_id')), $Tags)),
						];
						/* Tags */
						$TagsCategories = $this->getClass('TagsCategories')->get( (object)[ 'what'=>['id'] ] ); $TagsCategories = wp_list_pluck($TagsCategories, 'id');
						$TagsTaxonomy = $this->getClass('TagsTaxonomy')->get( (object)[ 'what'=>['tag_id','category_id'] ] );
						if (!empty($TagsTaxonomy))
						$extra->TagsTaxonomy = [
							'category_id'=>array_values(array_diff(array_unique(wp_list_pluck($TagsTaxonomy, 'category_id')), $TagsCategories)),
							'tag_id'=>array_values(array_diff(array_unique(wp_list_pluck($TagsTaxonomy, 'tag_id')), $Tags)),
						];

						foreach ($extra as $class_name=>$to_remove_by) if (!empty($to_remove_by) && count($to_remove_by) > 0) {
							$results[$class_name] = [];
							foreach ($to_remove_by as $field=>$value)
								if (!empty($value) && $value !== 0 && $value !== null)
									$results[$class_name][$field] = $value;
							if (empty($results[$class_name])) unset($results[$class_name]);
						}

						if ($run_it) foreach ($results as $class_name=>&$to_remove_by){
							foreach ($to_remove_by as $field=>&$values){
								foreach ($values as &$value)
									$value = [ $value, $this->getClass($class_name)->delete((object)[$field=>$value]) ];
							}
						}

						$out = new Out('OK', $results);
						break;
 				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out != 'WAS_NEVER_SET') echo json_encode($out);
			else echo json_encode(new Out(0, 'Output was never set.'));
		} catch (\Exception $e) { parseException($e); die(); }
	}
}
new AJAX_TomDev();