<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'/../_classes/Utility.class.php');

define('OPT_SELLER',   0);
define('OPT_ADDRESS',  1);

function saveFile(&$img, &$loc, &$needUpdate) {
	if (substr($img->file, 0, 4 ) == 'http') {
		$url = $img->file;
		list($width, $height, $type, $attr) = getimagesize($url);
		if ((int)$width < (int)$height ||
			(int)$width < 600) {
			$img->discard = $url;
			unset($img->file);
			$needUpdate = true;
			return false;
		}
		$base = basename($url);
		$base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);
		if (strpos('.', $base) == false)
			switch($type){
				case IMG_JPG: $base .= '.jpg'; break;
				case IMG_JPEG: $base .= '.jpeg'; break;
				case IMG_GIF: $base .= '.gif'; break;
				case IMG_PNG: $base .= '.png'; break;
				case IMG_WBMP: $base .= '.wbmp'; break;
				case IMG_XPM: $base .= '.xpm'; break;
			}
			
		$dest = __DIR__.'/../_img/_listings/uploaded/'.$base;
		if (!copy($url, $dest)){
			//$this->record( "Failed to copy image: $url, to $dest\n", 4 ); 
			return false; 
		}
		$img->url = $url;
		$img->file = $base;
		$needUpdate = true;
		$loc = __DIR__.'/../_img/_listings/uploaded/'.$img->file;
		return true;
	}
	elseif (isset($img->file))
		return true;
	
	return false;
}

class AJAX_wpAdmin extends Controller {
	private $logIt = true;

	public function __construct(){
		$in = parent::__construct();
		try {
			// if (empty($in->query)) throw new \Exception (__FILE__.':('.__LINE__.') - no query sent');
			if (empty($in->query)) throw new \Exception ('no query sent');
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax-viewer.log') : null;
			$this->log("ajax-viewer doing $in->query");
			switch ($in->query){
				/* Listings */
					case 'geocode-nonlisthub':
						$Listings = $this->getClass('Listings', 1);
						$sql = "SELECT b.id, b.street_address, b.city, b.state, b.zip, b.country, b.listhub_key, b.city_id FROM {$Listings->getTableName('listings-geoinfo')} AS a ";
						$sql.= "INNER JOIN {$Listings->getTableName()} AS b ON a.listing_id = b.id ";
						$sql.= "WHERE  `lat` = -1 AND  `lng` = -1 AND `active` =1 AND `listhub_key` IS NULL AND `street_address` IS NOT NULL";
						$listings = $Listings->rawQuery($sql);
						if (empty($listings)) { $out = new Out('fail', "No listings found, sql:$sql"); break; }

						$good = 0;
						$bad = 0;
						$this->log("geocode-nonlisthub found ".count($listings)." listings to process.");
						foreach($listings as $listing) {
							try {
								$google = null; // not used really
								$geo = new \stdClass();
								$geo->listing_id = $listing->id;
								$this->log("geocode-nonlisthub - id:$listing->id - $listing->street_address, $listing->city, $listing->state $listing->zip $listing->country");
								if ( $this->getClass('Listings')->geocodeListing($this->getClass("GoogleLocation"), $listing, $google)) {
									$good++;
									$geo->lat = $listing->result->query->lat;
									$geo->lng = $listing->result->query->lng;
									if (isset($listing->result->query->address))
										$geo->address = $listing->result->query->address;
									else
										$geo->address = $listing->street_address.', '.$listing->city.', '.$listing->state.(isset($listing->zip) ? ', '.$listing->zip : '').(isset($listing->country) ? ', '.$listing->country : '');
									if (isset($listing->update)) {
										$c = $this->getClass('Cities');
										$q = new \stdClass();
										$q->where = array('city' => $listing->city,
									  					  'state' => $listing->state);
										$x = $c->get($q);
										unset($q->where);
										$q->fields = array();
										if (!empty($x)) {
											if ((isset($listing->city_id) &&
											     $listing->city_id != $x[0]->id) ||
											   !isset($listing->city_id))
											$q->fields['city_id'] = $x[0]->id;
										} // empty!
										else { // so make a new one if possible
											if ( ($city_id = $this->getCityGeoCode($listing->city, $listing->state, $listing->zip, $listing->country, $cityGeo)) != null)
												$q->fields['city_id'] = $city_id;
										}
										if ($listing->update & 1)
											$q->fields['city'] = $listing->city;
										if ($listing->update & 2)
											$q->fields['state'] = $listing->state;
										$q->where = array('id'=>$listing->id);
										$a = array($q);
										try {
											$this->getClass('Listings')->set($a); // update city/state as needed.
										}
										catch(\Exception $e) {
											parseException($e);
										}
										unset($q, $a);
									}

								}
								else {
									$this->log("Failed geocodeListing for listing:".$listing->listhub_key." with ".$listing->street_address.", ".$listing->city." ".$listing->state." with status: ".$listing->result->status);
									$geo->address = $listing->street_address.', '.$listing->city.', '.$listing->state.(isset($listing->zip) ? ', '.$listing->zip : '').(isset($listing->country) ? ', '.$listing->country : '');
									$geo->lat = -1;
									$geo->lng = -1;
									$bad++;
									// $checkCityId = true;
								}
							}
							catch(\Exception $e) {
								parseException($e);
								$this->record("Caught exception from geocodeListing - ".$e->getMessage()."\n", 3);
							}
							if ($geo) {
								$this->createGeoCode($geo);
								// unset($geo);
							}
						}
						$out = new Out('OK', "Total listings found:".count($listings)." good:$good, bad:$bad");
						break;

					case 'home-page-img':
						$o = $this->getClass('Options');
						$q = new \stdClass();
						$q->where = array('opt'=>"HomeImages");
						$x = $o->get($q);
						$haveData = false;
						$needUpdate = false;
						if (!empty($x)) {
							$x = array_pop($x);
							$x = json_decode($x->value, true); // decode into array
							$haveData = true;
						}
						if (!empty($in->data['id']) &&
							!empty($in->data['mode'])) {
							$needUpdate = false;
							$mode = $in->data['mode'];
							$id = intval($in->data['id']);
							if ( ($mode == 'true' || $mode == '1') &&
								!in_array($id, $x)) {// then adding
								$x[] = $id;
								$needUpdate = true;
								$l = $this->getClass('Listings');
								unset($q);
								$q = new \stdClass();
								$q->where = array('id'=>$id);
								$listing = $l->get($q);
								if (!empty($listing)) {
									$loc = '';
									$updateIt = false;
									foreach($listing[0]->images as $i=>$img) {
       									if (isset($img->file) &&
       										saveFile($img, $loc, $updateIt)) {
       										$Image = $this->getClass('Image');
       										$size = array('width'=>158,
       													  'height'=>64,
       													  'dir'=>'158x64');

       										$out = $Image->generateSingleSize($loc, "listings", $size, true);
       										$img->file = $out->data;
       										$listing[0]->images[$i] = $img;

       										$size['dir'] = '1440x714';
       										$size['width'] = 1440;
       										$size['height'] = 714;
       										$Image->generateSingleSize($loc, "listings", $size, true);

       										$size['dir'] = '1440x575';
       										$size['width'] = 1440;
       										$size['height'] = 575;
       										$Image->generateSingleSize($loc, "listings", $size, true);
       										
       										$size['dir'] = '210x120';
       										$size['width'] = 210;
       										$size['height'] = 120;
       										$Image->generateSingleSize($loc, "listings", $size, true	);
       										break;
       									}
       								}
       								if ($updateIt) {
       									$q2 = new \stdClass();
       									$q2->where = array('id'=>$listing[0]->id);
       									$q2->fields = array('images'=>$listing[0]->images);
       									$a = array();
       									$a[] = $q2;
       									$l->set($a);
       									unset($q2, $a);
       								}
								}
							}
							else if (in_array($id, $x)){ // then removing
								$x = array_diff($x, array($id));
								$needUpdate = true;
							}
						}
						if ($x)
							$out = new Out('OK', $x);
						else
							$out = new Out('OK', array());
						if ($needUpdate) {
							if ($haveData) {
								$q->where = array('opt'=>'HomeImages');
								$q->fields = array('value'=>JSON_encode($x));
								$a = array();
								$a[] = $q;
								$x = $o->set($a);
							}
							else { // new!
								unset($q);
								$q = array('opt'=>'HomeImages',
											'value'=>JSON_encode($x));
								$x = $o->add((object)$q);
							}
						}
						break;
					case 'getpage':
						$page = gettype($in->data['page']) == 'string' ? intval($in->data['page']) : $in->data['page'];
						$perPage = gettype($in->data['perPage']) == 'string' ? intval($in->data['perPage']) : $in->data['perPage'];
						$active = intval($in->data['active']);
						$option = intval($in->data['option']);
						$price = intval($in->data['price']);
						$which = intval($in->data['which']);
						$first = $in->data['first'];
						$second = $in->data['second'];
						$third = $in->data['third'];
						$haveKeywordMatch = $in->data['haveKeywordMatch'];
						$idSort = $in->data['sortid'];
						$priceSort = $in->data['sortprice'];
						$t = $this->getClass('Listings');
						$q = new \stdClass();
						if ($option) {
							if ($which == OPT_SELLER) {
								// need to get seller data, and decide whether to use it's id or author_id
								$s = $this->getClass("Sellers");
								if (!empty($first))
									$q->where = array('first_name'=>$first);
								if (!empty($second)) 
									if (property_exists($q, 'where'))
										$q->where['last_name'] = $second;
									else
										$q->where = array('last_name'=> $second);
									
								$x = $s->get($q);
								if (empty($x)) {
									$out = new Out('fail', "No seller by that name");
									break;
								}
								unset($q->where);
                                $q->where = array('author'=> (empty($x[0]->author_id) ? $x[0]->id : $x[0]->author_id));								
							}
							else { // must be OPT_ADDRESS
								if (!empty($first))
									$q->where = array('street_address'=>$first);
								if (!empty($second)) {
									if (property_exists($q, 'where'))
										$q->where['city'] = $second;
									else
										$q->where = array('city'=>$second);
								}
								if (!empty($third)) {
									if (property_exists($q, 'where'))
										$q->where['state'] = $third;
									else
										$q->where = array('state'=>$third);
								}
							}
						} // end if have option
						if (property_exists($q, 'where'))
							$q->where['active'] = $active;
						else
							$q->where = array('active' => $active);
						if ($haveKeywordMatch == 'true' ||
							$haveKeywordMatch == '1')
							$q->where['key_match'] = 1;

						$q->greaterthanequal = array('price' => $price);
						if ($idSort != '0') {
							$q->orderby = 'id';
							$q->order = $idSort == 'true' ? 'ASC' : 'DESC';
						}
						elseif ($priceSort != '0') {
							$q->orderby = 'price';
							$q->order = $priceSort == 'true' ? 'ASC' : 'DESC';
						}
					    $q->page = $page; // 0-based
					    $q->limit = $perPage;
						$out = $t->get($q);
						if (!empty($out)) {
							$tags = $this->getClass("ListingsTags");
							$t = new \stdClass();
							foreach($out as $listing) {
								$t->where = array('listing_id'=>$listing->id);
								$listing->tagCount = $tags->count($t);
								unset($t->where);
							}
						}
						else {
							$tmp = str_replace("\n "," ",print_r($q, true));
							$out = "Failed to find listings using: $tmp";
							$out = new Out('fail', $out);
							break;
						}
						$out = new Out('OK', $out);
					    break;
					case 'transfer-listing':
						$id = $in->data['id'];
						$active = $in->data['active'];
						$user = $in->data['user'];
						$l = $this->getClass('Listings');
						$activity = $this->getClass("ListingsActivity");
						$q = new \stdClass(); 
						$q->where = array('listing_id'=>$id);
						$activity = $activity->get($q); 
						
						$q->where = array('id'=>$id);
						$listing = $l->get($q); // need this for the current active state to record
						if (empty($listing)) {
							$out = new Out('fail', "Count not find listing with id: $id");
							break;
						}
						$listing = $listing[0];

						// create a new meta data to record active transition
						$meta = new \stdClass(); // record the forced transition
						$meta->action = "active state change";
						$meta->date = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));	
						$meta->from = $listing->active;
						$meta->to   = $active;
						$meta->who  = $user;

						// apply this meta data to the activity for the listing
						$newArray = array();
						if (!empty($activity)) { // append to it
							$activity = array_pop($activity);
							if (!empty($activity->data))
								foreach($activity->data as $value)
									$newArray[] = $value;

							$newArray[] = $meta;
							$q->fields = array('data'=>$newArray);
							$q->where = array('listing_id'=>$id);
							$a = array();
							$a[] = $q;
							$this->getClass("ListingsActivity")->set($a);
							unset($q->fields);
						}
						else { // create new activity set
							$newArray[] = $meta;
							$activity = new \stdClass();
							$activity->listing_id = $id;
							$activity->author = $user;
							$activity->data = $newArray;
							$x = $this->getClass("ListingsActivity")->add($activity);
						}

						// now update the actual listing's active col
						$q->where = array('id'=>$id);
						$q->fields = array('active'=>$active);
						$a = array();
						$a[] = $q;
						$x = $l->set($a);
						if (!empty($x))
							$out = new Out('OK', "Updated listing with id:".$id.", to active:".$active);
						else
							$out = new OUt('fail', "Count not update listing with id:".$id.", to active:".$active);
						break;

					case 'row-count':
						$active = intval($in->data['active']);
						$option = intval($in->data['option']);
						$price = intval($in->data['price']);
						$which = intval($in->data['which']);
						$first = $in->data['first'];
						$second = $in->data['second'];
						$third = $in->data['third'];
						$haveKeywordMatch = $in->data['haveKeywordMatch'];
						$l = $this->getClass('Listings');
						$q = new \stdClass();
						if ($option) {
							if ($which == OPT_SELLER) {
								// need to get seller data, and decide whether to use it's id or author_id
								$s = $this->getClass("Sellers");
								if (!empty($first))
									$q->where = array('first_name'=>$first);
								if (!empty($second)) 
									if (property_exists($q, 'where'))
										$q->where['last_name'] = $second;
									else
										$q->where = array('last_name'=> $second);
									
								$x = $s->get($q);
								if (empty($x)) {
									$out = new Out('fail', "No seller by that name");
									break;
								}
								unset($q->where);
                                $q->where = array('author'=> (empty($x[0]->author_id) ? $x[0]->id : $x[0]->author_id));								
							}
							else { // must be OPT_ADDRESS
								if (!empty($first))
									$q->where = array('street_address'=>$first);
								if (!empty($second)) {
									if (property_exists($q, 'where'))
										$q->where['city'] = $second;
									else
										$q->where = array('city'=>$second);
								}
								if (!empty($third)) {
									if (property_exists($q, 'where'))
										$q->where['state'] = $third;
									else
										$q->where = array('state'=>$third);
								}
							}
						} // end if have option
						if (property_exists($q, 'where'))
							$q->where['active'] = $active;
						else
							$q->where = array('active' => $active);
						if ($haveKeywordMatch == 'true' ||
							$haveKeywordMatch == '1')
							$q->where['key_match'] = 1;

						$q->greaterthanequal = array('price' => $price);
						$val = $l->count($q);
						$out = new Out('OK', $val);
						break;

					case 'applykeyword':
						$keywords = $in->data['key'];
						$active = intval($in->data['active']);
						$l = $this->getClass('ListHubData');
						$out = $l->applyKeywords($keywords, $active);
						break;

					case 'cleanTags':
						$active = intval($in->data['active']);
						$l = $this->getClass('ListHubData');
						$out = $l->cleanTags($active);
						break;

					case 'delete-listing':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No listing id sent.');
						if ( !$this->getClass('ListingsRejected')->delete(['id'=>$in->data['id']]) ) throw new \Exception('Unable to delete listing.');
						$out = new Out('OK');
						break;
					case 'get-listings':
						if ( !$out = $this->getClass('ListingsRejected')->get() ) throw new \Exception('No listings found.');
						$out = new Out('OK', $out);
						break;
					case 'get-listings-with-geoinfo':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						$x = [];
						foreach ($listings as &$listing) foreach ($geoinfo as $geo) if ($geo->listing_id == $listing->id && isset($geo->lat)) {
							$x[] = $listing->id;
							break;
						}
						$out = new Out('OK', $x);
						break;
					case 'get-listings-and-points':
						if ( !$listings = $this->getClass('ListingsRejected')->get( (object)['what'=>[ 'id','title','street_address' ]] ) ) throw new \Exception('No listings found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						if ( !$points = $this->getClass('ListingsPoints')->get() ) throw new \Exception('No points found for listings.');
						$x = [];
						foreach ($listings as $listing){
							$id = $listing->id;
							// setup geoinfo
							foreach ($geoinfo as $geo) if ($geo->listing_id == $id) {
								foreach ($geo as $field=>$value)
									if ($field == 'listing_id' || $field == 'id') continue;
									else $listing->$field = $value;
								break;
							}
							$listing->points = [];
							// setup points
							foreach ($points as $point) if ($point->listing_id == $id)
								$listing->points[$point->point_id] = $point->distance;
							$x[$id] = $listing;
							unset($id);
						}
						$out = new Out('OK', $x);
						break;
					case 'update-listing':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No listing id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass()->set( (object) ['where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']] ) )
							throw new \Exception('Unable to update listing.');
						$out = new Out('OK');
						break;
				/* Points */
					case 'add-points-in-radius':
						if (empty($in->data) || (empty($in->data) && $in->data !== 0)) throw new \Exception('No radius sent.');
						if ( !$points = $this->getClass('Points')->get() ) throw new \Exception('No points found.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get() ) throw new \Exception('No geoinfo found for listings.');
						foreach ($geoinfo as $geo)
							if ($geo->lat && $geo->lng)
								foreach ($points as $point) {
									$distance = $this->getClass('GoogleLocation')->getDistance($geo->lat, $geo->lng, $point->lat, $point->lng);
									if ($distance && $distance <= intval($in->data)) $this->getClass('ListingsPoints')->add((object)[ 'listing_id'=>$geo->listing_id, 'point_id'=>$point->id, 'distance'=>$distance ]);
								}
						$out = new Out('OK');
						break;
					case 'add-from-google':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (!isset($in->data['query'])) throw new \Exception('No query sent.');
						if (!isset($in->data['listing_id'])) throw new \Exception('No listing_id sent.');
						if ( !$geoinfo = $this->getClass('ListingsGeoinfo')->get((object)[ 'where'=>['listing_id'=>$in->data['listing_id']] ])[0] ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);

						$out = new Out('OK', $this->getClass('GoogleLocation')->nearbySearch($geoinfo->lat, $geoinfo->lng, $in->data['query']));
						break;
					case 'get-points-by-id':
						if (empty($in->data)) throw new \Exception('No point ids sent.');
						if ( !$PointsCategories = $this->getClass('PointsCategories')->get() ) throw new \Exception('No points categories found.');
						if ( !$PointsTaxonomy = $this->getClass('PointsTaxonomy')->get( (object)[ 'what'=>['category_id','point_id'], 'where'=>['point_id'=>$in->data] ] )) throw new \Exception('No points taxonomy found.');
						if ( !$Points = $this->getClass('Points')->get((object)[ 'what'=>['id','name','address'], 'where'=>['id'=>$in->data] ]) ) throw new \Exception('No geoinfo found for listing '.$in->data['listing_id']);
						$x = [];
						foreach ($Points as $point) {
							foreach ($PointsTaxonomy as $tax) if ($tax->point_id == $point->id){
								foreach ($PointsCategories as $cat) if ($cat->id == $tax->category_id){
									$point->category = $cat->category;
									break;
								}
								break;
							}
							$x[$point->id] = $point; unset($x[$point->id]->id);
						}
						$out = new Out('OK', $x);
						break;
					case 'add-points':
						if (empty($in->data)) throw new \Exception('No points sent.');
						$results = [];
						foreach ($in->data as $point)
							if (!$this->getClass('Points')->exists((object)['name'=>$point['name'], 'address'=>$point['address']])){
								$in_obj = (object)array(
									'name'=>$point['name'],
									'meta'=>$point['meta'],
									'lat'=>$point['lat'],
									'lng'=>$point['lng'],
									'address'=>$point['address']
								);
								$point_id = $this->getClass('Points')->add($in_obj);
								if ($point_id === false) throw new \Exception('Unable to add point to db.');
								$results[] = [ 'point'=>$point_id, 'category'=>$this->getClass('PointsTaxonomy')->add((object)[ 'point_id'=>$point_id, 'category_id'=>$point['category'] ]) ];
								unset($in_obj);
							} else $results[] = 'already exists';
						$out = new Out('OK', $results);
						break;
				/* Cities */
					case 'update-city':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No city id sent.');
						if (empty($in->data['fields'])) throw new \Exception('No fields sent.');
						if ( !$this->getClass('Cities')->set(array((object)[ 'where'=>['id'=>$in->data['id']], 'fields'=>$in->data['fields']]) ))
							throw new \Exception('Unable to update city.');
						$out = new Out('OK');
						break;
					case 'get-cities':
						if ( !$out = $this->getClass('Cities')->get((object)['orderby'=>'count']) ) throw new \Exception('No cities found.');
						$out = new Out('OK', array_reverse($out));
						break;
				/* Tags */
					case 'get-tags':
						$Tags = $this->getClass('Tags')->get((object)['sortby'=>'tag']);
						$TagsCategories = $this->getClass('TagsCategories')->get((object)['sortby'=>'category']);
						$TagsTaxonomy = $this->getClass('TagsTaxonomy')->get((object)['what'=> ['tag_id','category_id'] ]);
						$QuizTags = $this->getClass('QuizTags')->get();
						$ListingsTags = $this->getClass('ListingsTags')->get();
						$x = [];
						foreach ($QuizTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $QuizTags = $x;
						foreach ($ListingsTags as $tag) {
							if ( !isset($x[$tag->tag_id]) ) $x[$tag->tag_id] = 0;
							$x[$tag->tag_id]++;
						} $ListingsTags = $x;
						$x = [];
						foreach ($Tags as &$t){
							$t->count_quiz = !isset($QuizTags[$t->id]) ? 0 : $QuizTags[$t->id];
							$t->count_listings = !isset($ListingsTags[$t->id]) ? 0 : $ListingsTags[$t->id];
							foreach ($TagsTaxonomy as $tax) if ($tax->tag_id == $t->id) {
								foreach ($TagsCategories as $cat) if ($cat->id == $tax->category_id){
									if (isset($in->data['group-by-category'])){
										if (!isset($x[$cat->category])) $x[$cat->category] = [];
										$x[$cat->category][] = $t;
									}
									else $t->category = $cat->category;
									break;
								}
								break; // only one category per tag
							}
						}
						if (isset($in->data['group-by-category'])) $Tags = $x;
						// else usort($Tags,function($a,$b){ strcasecmp($a->tag, $b->tag); });
						$out = new Out('OK', $Tags);
						break;
				/* Quiz */
					case 'add-question':
						if ( $this->getClass('QuizQuestions')->add() !== false ) $out = new Out('OK');
						break;
					case 'delete-question':
						$out = ( $x = $this->getClass('QuizQuestions')->delete(array('id'=>$in->data['question_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags() ) :
							new Out(0, array('unable to delete question', $x));
						break;
					case 'get-quiz-questions':
						$out = new Out('OK', $this->getQuestionsSlidesAndTags());
						break;
					case 'update-questions':
						$q = $ids = array();
						foreach ($in->data as $d) {
							$ids[] = $d['question_id'];
							$q[] = (object) array('where'=>array('id'=>$d['question_id']), 'fields'=>$d['fields']);
						}
						$out = ( $x = $this->getClass('QuizQuestions')->set($q) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($ids) ) :
							new Out(0, $x);
						break;
					case 'add-slide':
						$out = ($x = $this->getClass('QuizSlides')->add(array( 'question_id'=>$in->data['question_id'], 'orderby'=> isset($in->data['orderby']) ? $in->data['orderby'] : null )) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id'])) :
							new Out(0, array('Unable to add slide.', $x));
						break;
					case 'delete-slide':
						$out = ( $x = $this->getClass('QuizSlides')->delete( array('id'=>$in->data['slide_id']) ) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete slide', $x));
						break;
					case 'update-slides':
						$q = array();
						foreach ($in->data as $d)
							$q[] = (object)array( 'where'=>array('id'=>$d['slide_id']), 'fields'=>$d['fields'] );
						$out = ( $x = $this->getClass('QuizSlides')->set($q) ) ?
							new Out('OK') :
							new Out(0, array('Unable to update slides.', $x));
						break;
					case 'add-tag-to-slide':
						$out = ( $x = $this->getClass('QuizTags')->add(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to add row', $x));
						break;
					case 'remove-tag-from-slide':
						$out = ( $x = $this->getClass('QuizTags')->delete(array('tag_id'=>$in->data['tag_id'],'slide_id'=>$in->data['slide_id'])) ) ?
							new Out('OK', $this->getQuestionsSlidesAndTags($in->data['question_id']) ) :
							new Out(0, array('Unable to delete tag from slide', $x));
						break;
 				default: throw new \Exception(__FILE__.':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if ($out) echo json_encode($out);
		} catch (\Exception $e) { parseException($e); die(); }
	}
	private function getQuestionsSlidesAndTags($question_id = null){
		$query = (object)array();
		if (!empty($question_id)) $query->where = array('id'=>$question_id);
		$Questions = $this->getClass('QuizQuestions')->get($query);
		$Slides = $this->getClass('QuizSlides')->get();
		$SlideTags = $this->getClass('QuizTags')->get();
		$Tags = $this->getClass('Tags')->get();
		foreach ($Questions as &$q){
			$q->slides = array();
			foreach ($Slides as &$slide) if ($slide->question_id == $q->id){
				$slide->tags = array();
				foreach ($SlideTags as $slide_tag) if ($slide_tag->slide_id == $slide->id) {
					foreach ($Tags as $tag) if ($tag->id == $slide_tag->tag_id){
						$slide->tags[] = $tag;
						break;
					}
				}
				$q->slides[] = $slide;
			}
		}
		return $Questions;
	}

	protected function getCityGeoCode($cityStr, $state, $zip, $country, &$geoCode = -1) {
		$c = $this->getClass('Cities');
		// do getCode for this city
		$state = fixNY($cityStr, $state);
		$cityStr = fixCity($cityStr, $state);
		$cityStr = ucwords(fixCityStr($cityStr));

		if (nonConformingLocation($cityStr))
			return null;
		if (nonConformingLocation($state))
			return null;

		$city = (object)['city'=>$cityStr,
						 'state' => $state,
						 'zip' => $zip,
						 'country' => nonConformingLocation($country) ? '' : $country,
						 'lng' => null,
						 'lat' => null];
		// if (!empty($listing['zip']))
		// 	$city->zip = $listing['zip'];
		$lng = $lat = -1;
		$result_city = $google_result = null;
		try {
			if (!$this->getClass('Cities')->geocodeCity($this->getClass('GoogleLocation'), $city, $lng, $lat, $result_city, $google_result)) {
				$this->log("Failed to geocode city:$city->city, $state $country");
				return null;
			}
		}
		catch(\Exception $e) {
			$this->log("Exception from geocodeCity:".$e->getMessage());
			parseException($e);
			return null;
		}
		$city->lng = $lng;
		$city->lat = $lat;
		
		if ($c->exists(['city'=>$cityStr,
						'state'=>$state])) {
			$c->set([(object)['where'=>['city'=>$cityStr,
										'state'=>$state],
							  'fields'=>['lng'=>$lng,
							  			 'lat'=>$lat]]]);
			$x = $c->get((object)['where'=>['city'=>$cityStr,
											'state'=>$state]]);
			$x = $x[0]->id; // return row id
		}
		else
			$x = $c->add($city);
		if ($geoCode !== -1) {
			$geoCode = $city;
			$geoCode->id = $x; // row id
		}
		unset($city);
		if ($x)
			return $x;
		else
			return null;
	}

	protected function createGeoCode(&$info)
	{
		$geo = $this->getClass("ListingsGeoinfo");
		$q = new \stdClass(); 
		$q->where = array('listing_id'=>$info->listing_id);
		$x = $geo->get($q); 
		unset($q);
		if(empty($x))
		{
			try{
				$x = $geo->add((object)$info);
				$this->log( 'createGeoCode - new activity at row:'.$x);
				return $x;
			}
			catch (\Exception $e){ $this->parseException($e); return false;}
		}
		else
		{
			$this->log( "This GeoInfo already exists, with listing id:".$info->listing_id );		
			$curGeo = array_pop($x);
			$fields = [];
			if ($curGeo->lat == -1 &&
				$info->lat != -1) {
				$fields['lat'] = $info->lat;
				$fields['lng'] = $info->lng;
			}
			if ($curGeo->address != $info->address)
				$fields['address'] = $info->address;

			if (count($fields)) {
				$geo->set([(object)['where'=>['id'=>$curGeo->id],
								    'fields'=>$fields]]);
				$this->log( "Updated GeoInfo exiting one, for listing id:".$info->listing_id.", with lng:$info->lng, lat:$info->lat");	
			}
			unset($curGeo);
			return true;
		}	
	}
}
new AJAX_wpAdmin();