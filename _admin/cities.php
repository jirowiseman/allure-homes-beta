<?php
require_once( __DIR__ . '/../_classes/Cities.class.php' ); $Cities = new AH\Cities();
$Cities = wp_list_pluck( $Cities->rawQuery( ' SELECT DISTINCT state FROM ' . $Cities->getTableName($Cities->table) . ' ORDER BY `state` ASC ' ), 'state');
// die();
?>
<style type="text/css">
#cities li {
	display: inline-block;
	height: 200px;
	margin: 0 1em 1em 0;
	text-align: center;
	width: 150px;
	vertical-align: top;
}
#cities li img {
	height: 150px;
	width: 150px;
}
#tb-submit img{
	display: block;
	margin: 1em auto;
}
#dropzone { margin: 1em auto; width: 90%; }
#dropzone .dz-message span { color: black; }
.city-menu li {
	display: inline-block;
	cursor: pointer;
}
#TB_ajaxContent {
	padding: 0;
	position: relative;
}
ul.page-menu{ float: right; }
ul.page-menu li { display: inline-block; margin-right: .5em; }
#cities th{ font-weight: 400; font-size: 0.8em; }
th.image{ width: 30px; }
th.count{ width: 30px; }
th.digit { position: relative; width: 30px; }
#tb-submit{ padding: 0 1em 1em; }
.slider{ margin: 1em auto; position: relative; width: 100%; }
td.other-tags{
	font-size: 0.75em;
	letter-spacing: 0.05em;
	line-height: 1.125em;
}
th.city{ width: 175px; }
ul.other-tags li{ display: inline-block; margin-right: 1.5em; }
</style>
<script type="text/javascript">
	var current_page_number = <?php echo (isset($_GET['page_num']) ? $_GET['page_num'] : 0); ?>;
	var current_query = <?php echo (isset($_GET['query']) ? $_GET['query'] : 'null'); ?>;
</script>
<ul class="page-menu">
	<li><input id="city-search" placeholder="Search" type="text" /></li>
	<li><a class="prev">Prev</a></li>
	<li><a class="next">Next</a></li>
</ul>
<ul class="city-menu">
	<!-- <li><a data-query="city-tag-xls">Upload city tag xls</a></li> -->
	<select id="filter-state">
		<option value="0" selected>(state)</option>
	<?php
		if (!empty($Cities)) foreach ($Cities as &$row){
			echo '<option value="'.$row.'">'.$row.'</option>';
		}
	?>
	</select>
</ul>
<table class="widefat" id="cities">
	<thead>
		<tr>
			<th class="image">Image</th>
			<th class="count">Count</th>
			<th class="city">City</th>
			<th class="golf digit">Golf</th>
			<th class="boating digit">Boating</th>
			<th class="beach digit">Beach</th>
			<th class="nightlife digit">Nightlife</th>
			<th class="wine-culture digit">Wine</th>
			<th class="equestrian digit">Equestrian</th>
			<th class="culinary digit">Culinary</th>
			<th class="music digit">Music</th>
			<th class="surfing digit">Surfing</th>
			<th class="hunting digit">Hunting</th>
			<th class="skiing digit">Skiiing</th>
			<th class="shopping digit">Shopping</th>
			<th class="fishing digit">Fishing</th>
			<th class="casinos digit">Casinos</th>
			<th class="fboating digit">FrshBoat</th>
			<th class="hiking digit">Hiking</th>
			<th class="artistic digit">Artistic</th>
			<th class="parks digit">Major Parks</th>
			<th class="others">Other</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>