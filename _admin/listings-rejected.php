<?php
require_once(__DIR__.'/../_classes/Tables.class.php');
global $wpdb;
$curPage = 0;
$pageSize = 40;
$last = $wpdb->get_results("SELECT * FROM ".getTableName('listings-rejected')." ORDER BY `id` DESC LIMIT 1");
//$x = $wpdb->get_results('SELECT id, title, author, price, beds, baths FROM '.getTableName('listings-rejected')." LIMIT $pageSize");
//$authors = $wpdb->get_results('SELECT * FROM '.getTableName('sellers'));
?>
<script type="text/javascript">ah_local = {
	tp: '<?php echo get_template_directory_uri(); ?>',
	wp: '<?php echo get_home_url(); ?>',
	bp: '<?php echo bloginfo('url'); ?>',
	//authors: <?php echo json_encode($authors); ?>,
	last: <?php echo $last; ?>
}</script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/_js/jquery.bxslider.min.js" type="text/javascript"></script>

<!--<pre><?php print_r($authors); ?></pre>-->
<style>
.column-listing{ width: 200px; }
.column-author{ width: 160px; }
#controls {
	width: 100%;
	display: inline;
}
.ui-slider {
	margin: 20px;
	margin-top: 10px;
}
.bx-wrapper{
		//overflow: hidden;
		z-index: 0;
		width: 210px;
		height: 120px;
		margin-left: 140px;
		margin-right: 10px;
		//margin-bottom: 10px;

		.listing-gallery {
			img {
				/* For Safari 3.1 to 6.0 */
			    -webkit-transition-property: width, height;
			    -webkit-transition-duration: 0.5s;
			    -webkit-transition-timing-function: linear;
			    -webkit-transition-delay: 0.25s;
			    /* Standard syntax */
			    transition-property: width, height;
			    transition-duration: 0.5s;
			    transition-timing-function: linear;
			    transition-delay: 0.25s;

				&:hover {
					width: 600px;
					height: 480px;
				}
			}
		}

		.bx-pager {
				position: relative;	
				z-index: 8;

				.bx-pager-item {
						display: inline;
						margin: 0.75em;
						a {
							position: relative;
							&:before {
								content: " ";
								width: 0;
								height: 0;
								//position: absolute;
								opacity: 0;
								//top: -59px;
								//left: 43%;
								margin: 0 auto;
								.transition(.125s);
								border-left: 10px solid transparent;
								border-right: 10px solid transparent;
								border-bottom: 10px solid rgba(255,255,255,.85);
							}
							img {
								border: thin solid rgba(255,255,255,.6);
								opacity: .9;
							}
						}
						.active {
							img {
								border: thin solid rgba(255,255,255,.85);
								opacity:1;
								height: auto;
								width: auto;
							}
						}
				}
		}
}


</style>
<!-- <p><a href="<?php bloginfo('wpurl'); ?>/sellers/new-listing/">Create New Listing</a></p> -->
<table class="widefat">
	<thead>
		<tr>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-price">Price</th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-image"  style="padding-left: 180px">Image</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th scope="col" class="manage-column column-listing-id">ID</th>
			<th scope="col" class="manage-column column-price">Price</th>
			<th scope="col" class="manage-column column-bed-bath">Bed/Bath</th>
			<th scope="col" class="manage-column column-listing">Listing</th>
			<th scope="col" class="manage-column column-image" style="padding-left: 180px">Image</th>
			<th scope="col" class="manage-column column-transfer">Transfer</th>
		</tr>
	</tfoot>
	<tbody>

	</tbody>
</table>
<div id="controls"> 
	<input type="button" id="prevButton" value="Previous"><span> </span><input type="button" id="nextButton" value="Next"><span> </span><span id="sliderValue"></span><span> </span><div id="pageSlider"></div>
</div>

