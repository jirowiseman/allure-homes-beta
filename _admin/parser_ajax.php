<?php
require_once(__DIR__.'/parser_class.php');
require_once(__DIR__.'/../_classes/Tables.class.php');

class parser {
	private $APIkey = 'AIzaSyA-X7VaeHvCIgXkiIHLao8_Yb1D5qmYtd8';
	private $tags = array(
		array('find'=>array('2 car garage', '3 car garage', '4 car garage'), 'tags'=>array('2-4 car garage')),
		array('find'=>array('5 car garage', '6 car garage', '7 car garage', '8 car garage', '9 car garage', '10 car garage'), 'tags'=>array('5+ car garage')),
		array('find'=>array('Jogging/Biking Path','mountain bike','adventurous'), 'tags'=>array('adventurous')),
		array('find'=>array('alpine'), 'tags'=>array('alpine')),
		array('find'=>array('ski resort','ski slopes','skiing'), 'tags'=>array('alpine','skiing','winter <40')),
		array('find'=>array('asian style','asian influenced architecture','asian influenced design'), 'tags'=>array('asian style')),
		array('find'=>array('boating','mooring','sailing','yacht club'), 'tags'=>array('boating')),
		array('find'=>array('hollywood actress','hollywood actor','hollywood producer','movie star','celebrity owned'), 'tags'=>array('celebrity')),
		array('find'=>array('classic decor','classical decor','classic style interior','classical style interior'), 'tags'=>array('classical style')),
		array('find'=>array('country style','country architecture','country decor'), 'tags'=>array('country style')),
		array('find'=>array('countryside'), 'tags'=>array('country home')),
		array('find'=>array('craftsman'), 'tags'=>array('craftsman')),
		array('find'=>array('culinary destination','culinary center','culinary hotspot','fine dining','gourmet cuisine'), 'tags'=>array('culinary')),
		array('find'=>array('doorman','guard gated entrance','security guard'), 'tags'=>array('doorman')),
		array('find'=>array('driving distance to beaches','driving distance to the beach','drive to the beach','drive to beaches'), 'tags'=>array('drive to beach')),
		array('find'=>array('drive to casinos','drive to the casinos','driving distance to casinos','driving distance to several casino','driving distance to the casino'), 'tags'=>array('drive to casino')),
		array('find'=>array('drive to nearby lakes','drive to the lake','driving distance to the lake'), 'tags'=>array('drive to lake')),
		array('find'=>array('drive to major city','drive to san francisco','drive to los angeles','drive to sacramento','drive to san jose','drive to san diego','drive to fresno','near fresno','near san francisco','near san diego','near los angeles','near sacramento'), 'tags'=>array('drive to major city')),
		array('find'=>array('drive to ski slopes','drive to ski resorts','drive to skiing','drive to tahoe','driving distance to skiing','driving distance to ski resports','driving distance to tahoe','lake tahoe','mammoth mountain','near mammoth','mammoth resort'), 'tags'=>array('drive to ski')),
		array('find'=>array('driving distance to napa','drive to napa','driving distance to wine tasting','drive to winery','drive to wineries','driving distance to wineries'), 'tags'=>array('drive to winery')),
		array('find'=>array('solar system','eco-home','eco home','green home','eco-friendly','eco friendly'), 'tags'=>array('eco-friendly')),
		array('find'=>array('horse property','horse trails','stables','equestrian property'), 'tags'=>array('equestrian')),
		array('find'=>array('estate property','estate home'), 'tags'=>array('estate')),
		array('find'=>array('famous architect','famous family','iconic family','renowned architect'), 'tags'=>array('famous')),
		array('find'=>array('fast pace'), 'tags'=>array('fast pace')),
		array('find'=>array('fenced property','perimeter fence'), 'tags'=>array('fenced property')),
		array('find'=>array('fireplace'), 'tags'=>array('fireplace')),
		array('find'=>array('fishing'), 'tags'=>array('fishing')),
		array('find'=>array('gated community','private community'), 'tags'=>array('gated community')),
		array('find'=>array('gated entry','gated driveway','gates','gated property'), 'tags'=>array('gated property')),
		array('find'=>array('golf front','golf course community','golf community','golf development','golf course development','golf course property','golf property'), 'tags'=>array('golf front')),
		array('find'=>array('state of the art appliances','top of the line appliances',"chef's kitchen",'gourmet kitchen','professional kitchen'), 'tags'=>array('gourmet kitchen')),
		array('find'=>array('greenbelt view'), 'tags'=>array('greenbelt view')),
		array('find'=>array('Heliport','helipad','heli-pad'), 'tags'=>array('heli-pad')),
		array('find'=>array('high ceilings','tall ceilings','Vaulted Ceiling'), 'tags'=>array('high ceilings')),
		array('find'=>array('hiking','hiking trail'), 'tags'=>array('hiking')),
		array('find'=>array('historical','renowned architect'), 'tags'=>array('historical')),
		array('find'=>array('Bar','bar-wet'), 'tags'=>array('home bar')),
		array('find'=>array('home theater','multi-media center','projection screening room','media room'), 'tags'=>array('home theater')),
		array('find'=>array('hot tub','spa'), 'tags'=>array('hot tub')),
		array('find'=>array('hunting','hunter'), 'tags'=>array('hunting')),
		array('find'=>array('island style','polynesian style'), 'tags'=>array('island style')),
		array('find'=>array('priviate island'), 'tags'=>array('island')),
		array('find'=>array('lake view','view of lake','view of the lake'), 'tags'=>array('lake view')),
		array('find'=>array('marble floors'), 'tags'=>array('marble interior')),
		array('find'=>array('spanish villa','mediterranean villa','spanish home','mediterranean home','spanish estate','mediterranean estate'), 'tags'=>array('mediterranean')),
		array('find'=>array('In-city'), 'tags'=>array('metropolitan','urban')),
		array('find'=>array('modern interior','modern home','modern architecture','contemporary architecture','contemporary home','contemporary style'), 'tags'=>array('modern')),
		array('find'=>array('Mountain View','view of mountains'), 'tags'=>array('mountain view')),
		array('find'=>array('museum'), 'tags'=>array('museum')),
		array('find'=>array('concerts','music hall'), 'tags'=>array('music')),
		array('find'=>array('newly constructed','never lived in','newly built'), 'tags'=>array('new build')),
		array('find'=>array('nightlife','nightclubs'), 'tags'=>array('nightlife')),
		array('find'=>array('no visible neighbors','no neighbors','no nearby neighbors','far from neighbors'), 'tags'=>array('no visible neighbor','privacy')),
		array('find'=>array('ocean front','on the beach','Beach access'), 'tags'=>array('ocean front')),
		array('find'=>array('open floor plan','open floor plan','Open plan living'), 'tags'=>array('open floor plan')),
		array('find'=>array('large BBQ area','Barbecue','BBQ','Courtyard','Deck',"Entertainer's yard",'outdoor space','Patio'), 'tags'=>array('outdoor living space')),
		array('find'=>array('oversize closet','Walk-In Closet','Walk In Closet'), 'tags'=>array('oversize closet')),
		array('find'=>array('floor to ceiling windows','large windows','tall windows'), 'tags'=>array('oversize windows')),
		array('find'=>array('pool'), 'tags'=>array('pool')),
		array('find'=>array('privacy','Private Setting'), 'tags'=>array('privacy')),
		array('find'=>array('private airstrip'), 'tags'=>array('private airstrip')),
		array('find'=>array('Dock','harbor'), 'tags'=>array('private dock','boating','fishing')),
		array('find'=>array('ranch facilities','ranch property'), 'tags'=>array('ranch')),
		array('find'=>array('river front'), 'tags'=>array('river front','river')),
		array('find'=>array('close to river','nearby river'), 'tags'=>array('river')),
		array('find'=>array('river view','view of river'), 'tags'=>array('river view','river','fishing')),
		array('find'=>array('rooftop'), 'tags'=>array('rooftop terrace')),
		array('find'=>array('rural'), 'tags'=>array('rural')),
		array('find'=>array('sauna'), 'tags'=>array('sauna')),
		array('find'=>array('secluded','privacy'), 'tags'=>array('secluded')),
		array('find'=>array('guest cottage','Guest House','in-law unit','separate unit','poolhouse'), 'tags'=>array('separate guest space')),
		array('find'=>array('shopping'), 'tags'=>array('shopping')),
		array('find'=>array('ski front','on the slope','ski resort development'), 'tags'=>array('ski front home','skiing','alpine','winter <40')),
		array('find'=>array('skiing','skiier'), 'tags'=>array('skiing')),
		array('find'=>array('skyline view','city views','views of the city','view of the city','view of the skyline','views of the skyline'), 'tags'=>array('skyline view')),
		array('find'=>array('small town'), 'tags'=>array('small town')),
		array('find'=>array('smart home'), 'tags'=>array('smart home')),
		array('find'=>array('stables'), 'tags'=>array('stables','equestrian')),
		array('find'=>array('surfing','surfer','surf spot'), 'tags'=>array('surfing')),
		array('find'=>array('tennis club'), 'tags'=>array('tennis club')),
		array('find'=>array('tennis court'), 'tags'=>array('tennis court')),
		array('find'=>array('updated interior','modern appliances'), 'tags'=>array('updated interior')),
		array('find'=>array('urban'), 'tags'=>array('urban')),
		array('find'=>array('valet'), 'tags'=>array('valet')),
		array('find'=>array('valley view','views of the valley','view of the valley','scenic valley'), 'tags'=>array('valley view')),
		array('find'=>array('commercial vineyard','vineyard property'), 'tags'=>array('vineyard')),
		array('find'=>array('wine cellar','wine storage','wine cave'), 'tags'=>array('wine cellar')),
		array('find'=>array('wine country'), 'tags'=>array('wine country'))
		// array('find'=>array(), 'tags'=>array()),
	);
	public function __construct(){
		global $wpdb;
		if (func_num_args() !== 1 && func_num_args() != 2) $this->out(0, 'contruct(): Invalid number of arguements: '.func_num_args().'. Requires 1 or 2.');
		elseif (!isset($wpdb)) $this->out(0, 'contruct(): Unable to load wpdb class.');
		else {
			if (!function_exists('file_get_html')) $this->out(0, 'parser: unable to load dom_parser.');
			else {
				$this->wpdb = $wpdb;
				if (func_num_args() == 2) $d = func_get_arg(1);
				switch(func_get_arg(0)){
					// get from db
					case 'get-unparsed-listings': 
						$x = $this->get('parser-listings', array('type'=>'%d','column'=>'parsed','value'=>0)); 
						$this->out($x->status, $x->data); 
						break;
					case 'get-parsed-listings':
						$x = $this->get('parser-details'); 
						$this->out($x->status, $x->data); 
						break;
					case 'get-ungeocoded':
						$geocoded = $this->wpdb->get_results('SELECT listing_id FROM '.getTableName('parser-geoinfo'));
						$listings = $this->get('parser-details')->data;
						$toGeocode = array();
						foreach ($listings as $l){
							$found = false;
							foreach ($geocoded as $g) { if ($l->id == $g->listing_id){ $found = true; break;} }
							if (!$found) array_push($toGeocode, $l->id);
						}
						$this->out('OK', $toGeocode);
						break;
					// parse data
					case 'parse-raw-listings':
						$out = new stdClass();
						$out->status = 'fail'; $out->data = 'Parsing failed.';
						$x = $this->wpdb->get_results(
							'SELECT a.*,b.html FROM '.getTableName('parser-details-raw').' AS b
							INNER JOIN '.getTableName('parser-listings').' AS a
							ON a.id = b.listing_id'
						);
						if ($x && count($x) > 0){
							$out->status = 'OK';
							$out->data = array();
							foreach ($x as $l) if ($l->id) array_push($out->data, $this->parseRawDetail($l));
						} else $out->data = 'No results from database query.';
						$this->out($out->status, $out->data);
						break;
					case 'parse-email-csv':
						// $emails = $this->get('agent-emails');
						$this->out(0, 'Just copy the DB.<br/> -Tom');
						// $this->parseEmailCSV();
						break;
					case 'geocode-by-id':
						$l = $this->wpdb->get_row($this->wpdb->prepare('SELECT id,street_address,city,state,zip,country FROM '.getTableName('parser-details').' WHERE id=%d', $d['id']));
						$a = $l->street_address.' '.$l->city.' '.$l->state.' '.$l->zip.' '.$l->country;
						$a = array('address'=>str_replace(' ', '+', $a),'id'=>$l->id);
						$a = $this->geocodeAddress($a);
						if ($a->status == 'OK'){
							$loc = $a->data->geometry->location;
							$add = $a->data->formatted_address;
							$d = array();
							$res = $this->wpdb->insert(getTableName('parser-geoinfo'), array('address'=>$add, 'lat'=>$loc->lat, 'lng'=>$loc->lng, 'listing_id'=>$l->id), array('%s','%f','%f','%d'));
						 	if (!$res) array_push($d, array('Unable to add: '.$l->id, $res)); else {
						 		array_push($d, 'Added succesfully: '.$l->id);
						 		$places = $this->wpdb->get_results('SELECT id,latlng FROM '.getTableName('listing-points'));
						 		$prev = $this->wpdb->get_results($this->wpdb->prepare('SELECT place_id FROM '.getTableName('parser-points-taxonomy').' WHERE listing_id=%d', $l->id));
						 		foreach ($places as $place){
						 			$found = false;
						 			foreach ($prev as $p){ if ($p->place_id == $place->id){ $found = true; break; } }
						 			if (!$found){
						 				$place->latlng = json_decode($place->latlng);
						 				$dist = $this->getDistance($place->latlng[0], $place->latlng[1], $loc->lat, $loc->lng);
						 				if ($dist < 40){
						 					$res = $this->wpdb->insert(getTableName('parser-points-taxonomy'), array('place_id'=>$place->id, 'listing_id'=>$l->id, 'distance'=>$dist), array('%d','%d','%f'));
						 					if (!$res) array_push($d, 'Unable to add point taxonomy.');
						 					else array_push($d, 'Added point '.$place->id.' to taxonomy.');
						 				}
						 			}
						 		}
						 	}
						 	$this->out('OK', $d);
						} else $this->out(0, array('Unable to geocode listing '.$d['id'], $a));
						break;
					case 'geocode-all':
						$out = new stdClass(); $out->status = 'fail'; $out->data = null;
						$ldb = $this->get('parser-details');
						if ($ldb->status != 'OK') $out->data = 'Unable to get details from DB';
						else {
							$out->data = array();
							foreach ($ldb->data as $l){
								$a = $l->street_address.' '.$l->city.' '.$l->state.' '.$l->zip.' '.$l->country;
								$a = array('address'=>str_replace(' ', '+', $a),'id'=>$l->id);
								$a = $this->geocodeAddress($a);
								if ($a->status == 'OK'){
									$loc = $a->data->geometry->location;
									$add = $a->data->formatted_address;
									$prev = $this->get('parser-geoinfo', array('type'=>'%s','column'=>'address','value'=>$add));
									if ( count($prev->data) > 0 ) array_push($out->data, 'Already exists in database.'); else {
										$d = array();
										$res = $this->wpdb->insert(getTableName('parser-geoinfo'), array('address'=>$add, 'lat'=>$loc->lat, 'lng'=>$loc->lng, 'listing_id'=>$l->id), array('%s','%f','%f','%d'));
									 	if (!$res) array_push($d, array('Unable to add: '.$l->id, $res)); else {
									 		$places = $this->wpdb->get_results('SELECT id,latlng FROM '.getTableName('listing-points'));
									 		$prev = $this->wpdb->get_results($this->wpdb->prepare('SELECT place_id FROM '.getTableName('parser-points-taxonomy').' WHERE listing_id=%d', $l->id));
									 		foreach ($places as $place){
									 			$found = false;
									 			foreach ($prev as $p){ if ($p->place_id == $place->id){ $found = true; break; } }
									 			if (!$found){
									 				$place->latlng = json_decode($place->latlng);
									 				$dist = $this->getDistance($place->latlng[0], $place->latlng[1], $loc->lat, $loc->lng);
									 				if ($dist < 40){
									 					$res = $this->wpdb->insert(getTableName('parser-points-taxonomy'), array('place_id'=>$place->id, 'listing_id'=>$l->id, 'distance'=>$dist), array('%d','%d','%f'));
									 					if (!$res) array_push($d, 'Unable to add point taxonomy.');
									 					else array_push($d, 'Added point to taxonomy.');
									 				}
									 			}
									 		}
									 		array_push($out->data, 'Added succesfully: '.$l->id);
									 	}
									 	array_push($out->data, $d);
									}
								} else array_push($out->data, $a);
							}
							if (count($out->data) > 0) $out->status = 'OK';
						}
						$this->out($out->status, $out->data);
						break;
					// pull raw data
					case 'pull-summary-pages': $this->pullSummaryPage($d); break;
					case 'pull-single-detail': 
						if (!$d) $this->out(0, 'No ID provided.');
						else {
							$x = $this->pullSingleDetailUnparsed($d);
							if ($x->status == 'OK'){
								$x = $this->get('parser-listings', array('type'=>'%d','column'=>'id','value'=>$d['id']));
								$this->out($x->status, $x->data);
							}
							else $this->out($x->status, $x->data); 
						}
						break;
					// error messages
					case 'no-data-sent': $this->out(0, 'No data sent to parser class.'); break;
					default: $this->out(0, 'Invalid query: "'.func_get_arg(0).'"'); break;
				}
			}
			
		}
	}
	private function get(){
		$out = new stdClass();
		$out->status = 'fail';
		$out->data = null;
		if (func_num_args() === 1 || func_num_args() == 2){
			$table = func_get_arg(0);
			if (func_num_args() == 2){
				$x = func_get_arg(1);
				$where = ' WHERE '.$x['column'].'='.$x['type'];
				$value = $x['value'];
			}
			switch($table){
				default:
					if (!getTableName($table)){ $out->data = 'get(): invalid table name - '.$table; }
					else {
						$out->status = 'OK';
						if (!$where) $out->data = $this->wpdb->get_results('SELECT * FROM '.getTableName($table));
						else $out->data = $this->wpdb->get_results($this->wpdb->prepare('SELECT * FROM '.getTableName($table).$where, $value));
					}
					break;
			}
		} else $out->data = 'get(): invalid number of arguments';
		return $out;
	}
	private function pullSummaryPage(){
		$d = new stdClass();
		$d->url = func_get_arg(0)['URL'];
		if (!filter_var($d->url, FILTER_VALIDATE_URL, (FILTER_FLAG_HOST_REQUIRED | FILTER_FLAG_PATH_REQUIRED) )) $this->out(0, 'pullSummaryPage(): invalid URL'); 
		else {
			if ( substr($d->url, 0, 32) == 'http://www.luxuryrealestate.com/' ) $d->site = 'luxuryrealestate';
			switch($d->site){
				case 'luxuryrealestate':
					$d->url_vars = array();
					foreach (explode('&', parse_url($d->url, PHP_URL_QUERY)) as $var)
						if (explode('=', $var)[0] != 'page') $d->url_vars[explode('=', $var)[0]] = explode('=', $var)[1];
					$d->html = file_get_contents($d->url);
					$d->html = stripslashes( substr($d->html, 21, strpos($d->html, '");')-21) );
					$listings = array();
					$dom = str_get_html($d->html);
					foreach ($dom->find('.card') as $card){
						$card = $this->parseSingle(array('site'=>$d->site, 'url'=>$d->url, 'site_vars'=>$d->url_vars, 'dom'=>$card));
						if ($card->status == 'OK') array_push($listings, $this->pushSingle($card->data) );
						else array_push($listings, $card);
					}
					$dom->clear();
					unset($dom);
					$this->out('OK', $listings);
					break;
				default: $this->out(0, 'no valid parser for site: "'.$d->site.'"'); break;
			}
		}
	}
	private function parseSingle(){
		$out = new stdClass();
		$out->status = 'fail';
		$out->data = null;
		if (func_num_args() !== 1) $out->data = 'parseSingle() invalid number of arguments';
		elseif (!is_array(func_get_arg(0))) $out->data = 'parseSingle() invalid argument - array required, '.gettype(func_get_arg(0)).' provided.';
		else {
			$in = func_get_arg(0);
			switch($in['site']){
				case 'luxuryrealestate':
					$dom = $in['dom'];
					if ($dom->find('span.sold', 0)->innertext == null){
						$l = new stdClass();
						$l->address = $dom->find('.city', 0)->plaintext;
						if ($l->address && $l->address != '') {
							$x = new stdClass();
							$l->site = $in['site'];
							$l->site_vars = $in['site_vars'];
							$l->price = $dom->find('.price', 0)->plaintext;
							$l->price = intval( str_replace(' ', '', str_replace( ',', '', substr($l->price, strpos($l->price, '$')+1, strpos($l->price,'USD')) )) );
							if ( $dom->find('.beds', 0) )
								$l->beds = intval(substr($dom->find('.beds', 0)->innertext, 6, -1));
							if ($dom->find('.baths', 0))
								$l->baths = floatval(str_replace('+', '.5', substr($dom->find('.baths', 0)->innertext, 7, -1)));
							$l->url = 'http://luxuryrealestate.com'.$dom->first_child()->href;
							$l->thumb = $dom->find('.search_result', 0)->src;
							$out->status = 'OK'; $out->data = $l;
						} else $out->data = 'parseSingle(): Unable to parse address.';
					} else { $out->status = 'OK'; $out->data = 'parseSingle(): Listing marked sold'; }
					break;
				default: $out->data = 'parseSingle(): no valid parser for "'.$in['site'].'"'; break;
			}
		}
		return $out;
	}
	private function parseRawDetail(){
		$l = new stdClass();
		$in = func_get_arg(0);
		$out = new stdClass();
		$out->status = 'fail'; $out->data = null;

		if (count($this->get('parser-details', array('type'=>'%d','column'=>'summary_id','value'=>$in->id))->data) > 0) { $out->status = 'OK'; $out->data = 'Already exists in database.'; } else {
			$l->id = $in->id;
			$l->url = $in->url;
			$l->price = $in->price;
			$l->address = $in->address;
			$dom = str_get_html($in->html);

			$l->title = trim($dom->find('span.listing_title', 0)->innertext);
			$l->interior = $dom->find('span.living-area', 0)->innertext;
			if (strpos($l->interior, 'sqft') !== false){
				$l->interior_std = 'ft';
				$l->interior = intval(substr($l->interior, 0, strpos($l->interior, 'sqft')-1));			
			} else if (strpos($l->interior, 'acres') !== false){
				$l->interior_std = 'acres';
				$l->interior = intval(substr($l->interior, 0, strpos($l->interior, 'acres')-1));
			}
			$l->lotsize = $dom->find('span.lot-size', 0)->innertext;
			if (strpos($l->lotsize, 'sqft') !== false){
				$l->lotsize_std = 'ft';
				$l->lotsize = intval(substr($l->lotsize, 0, strpos($l->lotsize, 'sqft')-1));
			} else if (strpos($l->lotsize, 'acres') !== false){
				$l->lotsize_std = 'acres';
				$l->lotsize = intval(substr($l->lotsize, 0, strpos($l->lotsize, 'acres')-1));
			}
			$l->beds = 0; $l->baths = 0; $l->baths_full = 0; $l->baths_partial = 0;
			foreach ($dom->find('div.information ul li') as $li){
				if ( $li->find('label.beds', 0)->innertext != null ) $l->beds = intval($li->last_child()->innertext);
				elseif ( $li->find('label.baths', 0)->innertext != null ){
					if ($li->find('label.baths', 0)->innertext == 'Full Baths:') { $l->baths = $l->baths_full = intval($li->last_child()->innertext); }
					elseif ($li->find('label.baths', 0)->innertext == 'Partial Baths:') { $l->baths+= .5*intval($li->last_child()->innertext); $l->baths_partial = intval($li->last_child()->innertext); }
				}
			}
			if (!isset($l->beds)) $l->beds = 0;
			if (!isset($l->baths_full)) $l->baths_full = 0;
			if (!isset($l->baths_partial)) $l->baths_partial = 0;

			$l->images = array();
			foreach ($dom->find('#details_content .slides li') as $li){
				$file = $li->find('img', 0)->src;
				$title = $li->find('.caption h3', 0)->plaintext;
				array_push($l->images, array('file'=>$file, 'title'=>$title));
			}
			foreach ($dom->find('label.web') as $d){
				if ($d->innertext == 'MLS #:'){
					$l->mls = trim($d->next_sibling()->innertext);
					break;
				}
			}
			$l->description = '';
			if ($dom->find('#description', 0)){
				foreach ($dom->find('#description', 0)->children() as $p) $l->description.= $p->innertext;
			}
			if ($l->description == '') unset($l->description);
			$l->cats = array(); $l->other = array();
			foreach ($dom->find('#highlights ul li') as $cat){
				if ($cat->find('h2', 0)->plaintext != 'Information') {
					strpos($cat->plaintext, ':') === false ? array_push($l->cats, trim($cat->plaintext)) : array_push($l->other, trim($cat->plaintext));
				}
			}
			$l = $this->parseForRealtor($l, $dom);
			$l = $this->parseForAddress($l, $dom);
			$l = $this->parseForTags($l);
			$dom->clear();
			unset($dom);
			$res = $this->wpdb->insert(getTableName('parser-details'), 
				array(
					'title'=>$l->title,
					'price'=>$l->price,
					'street_address'=>$l->street_address,
					'city'=>$l->city,
					'state'=>$l->state,
					'zip'=>$l->zip,
					'country'=>$l->country,
					'tags'=>json_encode($l->tags),
					'about'=>$l->description,
					'beds'=>$l->beds,
					'baths'=>json_encode(array($l->baths_full,$l->baths_partial)),
					'mls'=>$l->mls,
					'lotsize'=>$l->lotsize,
					'lotsize_std'=>$l->lotsize_std,
					'interior'=>$l->interior,
					'interior_std'=>$l->interior_std,
					'images'=>json_encode($l->images),
					'other'=>json_encode(array('cats'=>$l->cats,'other'=>$l->other,'realtors'=>$l->realtor_info)),
					'summary_id'=>$l->id
				), 
				array('%s','%d','%s','%s','%s','%d','%s','%s','%s','%d','%s','%s','%d','%s','%d','%s','%s','%s','%d')
			);
			if ($res) $out->status = 'OK';
			else $out->data = 'Unable to add to database.';
		}
		return $out;
	}
	private function parseForAddress($in, $dom){
		$x = split(', ', $dom->find('p#subtitle',0)->innertext);
		if (count($x) == 3){
			$in->city = $x[0];
			$in->state = $x[1];
			$in->country = $x[2];
		} elseif ( count($x) == 4 && is_int(intval($x[2])) ){
			$in->city = $x[0];
			$in->state = $x[1];
			$in->zip = intval($x[2]);
			$in->country = $x[3];
		} elseif (count($x) == 5 && is_int(intval($x[3])) ){
			$in->street_address = $x[0];
			$in->city = $x[1];
			$in->state = $x[2];
			$in->zip = intval($x[3]);
			$in->country = $x[4];
		} elseif (count($x) == 6 && is_int(intval($x[4])) ){
			$in->street_address = $x[0].', '.$x[1];
			$in->city = $x[2];
			$in->state = $x[3];
			$in->zip = intval($x[4]);
			$in->country = $x[5];
		}
		$in->address = $x;
		return $in;
	}
	private function parseForRealtor($in, $dom){
		$dom = $dom->find('div#contacts',0);
		if ($dom){
			$r = new stdClass();
			$r->company = array();
			foreach ($dom->find('.company') as $c){
				$cDom = str_get_html($c->find('h3',0)->innertext);
				$found = 0;
				foreach ($cDom->find('span') as $span){
					$found++;
					array_push($r->company, trim($span->innertext));
				}
				$cDom->clear();
				unset($cDom);
				if ($found < 1) array_push($r->company, trim($c->find('h3',0)->innertext));
			}
			$r->realtor = array();
			foreach ($dom->find('.member') as $m){
				$mDom = str_get_html($m->find('h3',0)->innertext);
				$found = 0;
				foreach ($mDom->find('span') as $span){
					$found++;
					array_push($r->realtor, trim($span->innertext));
				}
				$mDom->clear();
				unset($mDom);
				if ($found < 1) array_push($r->realtor, trim($m->find('h3',0)->innertext));
			}
			$in->realtor_info = $r;
		}
		if ($in->realtor_info->realtor) foreach ($in->realtor_info->realtor as $k => $realtor){
			$db = $this->wpdb->get_var($this->wpdb->prepare('SELECT email FROM '.getTableName('agent-emails').' WHERE name LIKE %s', '%'.str_replace(' ', '%', $realtor).'%'));
			$in->realtor_info->realtor[$k] = array($realtor, $db, 0);
		}
		return $in;
	}
	private function parseForTags(){
		$in = func_get_arg(0);
		$in->tags = array();
		foreach ($in->cats as $cat){
			foreach ($this->tags as $t) foreach ($t['find'] as $tagToFind) 
				if ($cat == $tagToFind || $cat == str_replace("'", '&#39;', $tagToFind)) {
					foreach ($t['tags'] as $tagToAdd) array_push($in->tags, $tagToAdd);
					break;
				}
		}
		foreach ($this->tags as $t)
			foreach ($t['find'] as $tagToFind){
				if (stripos($in->description, $tagToFind) !== false || stripos($in->description, str_replace("'", '&#39;', $tagToFind)) !== false ) {
					foreach ($t['tags'] as $tagToAdd) array_push($in->tags, $tagToAdd);
					break;
				} 
			}
		sort($in->tags);
		$x = array_unique($in->tags);
		$in->tags = array();
		foreach ($x as $t) array_push($in->tags, $t);
		return $in;
	}
	private function parseEmailCSV(){
		$out = array();
		$filename = __DIR__.'/agent_emails_california.csv';
		$count = 0;
		$file = fopen($filename,"r");
		while(!feof($file)){
			$f = fgetcsv($file);
			if ($f && $f[0] != '' && $f[0] != 'Email'){
				$x = new stdClass();
				$x->email = $f[0];
				$x->name = $f[1];
				$this->wpdb->insert(getTableName('agent-emails'), array('name'=>$x->name,'email'=>$x->email),array('%s','%s'));
			}
		}
		// $fp = fopen(__DIR__.'/results.json', 'w');
		// fwrite($fp, json_encode($out));
		// fclose($fp);
		fclose($file);

		$this->out('OK');

		 // $out;

		// $row = 1;
		// if (($handle = fopen($filename, "r")) !== FALSE) {
			// $out = fgetcsv($handle, 0, ",");
    	// while (($data = fgetcsv($handle, 0, ",")) !== FALSE){
    	// 	array_push($out, ($data == null ? null : $data));
        // $num = count($data);
        // echo "<p> $num fields in line $row: <br /></p>\n";
        // $row++;
        // for ($c=0; $c < $num; $c++) {
        //     echo $data[$c] . "<br />\n";
        // }
	    // }
	    // fclose($handle);
		// }
		// $this->out(0, $out);
		// return $out;
	}
	private function pushDetail(){
		$in = func_get_arg(0);
		$out = new stdClass();
		$out->status = 0;
		$out->data = 'preparing to push';
	}
	private function pushSingle(){
		// return $in;
		$out = new stdClass();
		$out->status = 'fail';
		$out->data = null;
		$in = func_get_arg(0);
		if ($in->url != ''){
			$listings = $this->wpdb->get_col('SELECT url FROM '.getTableName('parser-listings'));
			$found = false;
			if ($listings) foreach ($listings as $url){ if ($url == $in->url) {$found = true; break;} } 
			if ($found) { $out->status = 'OK'; $out->data = 'pushSingle(): listing url already exists in database.'; }
			else {
				$res = $this->wpdb->insert(getTableName('parser-listings'), array(
					'address'=>$in->address,
					'price'=>$in->price,
					'site'=>$in->site,
					'site_vars'=>json_encode($in->site_vars),
					'url'=>$in->url,
					'thumb_url'=>$in->thumb,
					'parsed'=>0
				));
				if ($res) { $out->status = 'OK'; $out->data = 'pushSingle(): added to listing db'; }
				else $out->data = array('pushSingle(): unable to add listing to db', $res);
			}
		} else $out->data = 'pushSingle(): invalid url.';
		return $out;
	}
	private function pullSingleDetailUnparsed(){
		$out = new stdClass();
		$out->status = 'fail';
		$out->data = null;
		$in = func_get_arg(0);
		$l = $this->get('parser-listings', array('type'=>'%d','column'=>'id','value'=>$in['id']));
		if ($l->status == 'OK' && $l->data[0]->parsed == 0) {
			$out->data = 'pulling from html';
			$html = file_get_contents($l->data[0]->url);
			if (!$html) $out->data = 'pullSingleDetailUnparsed(): unable to get html for #:'.$in['id'];
			else {
				$x = $this->wpdb->insert(getTableName('parser-details-raw'), array('listing_id'=>$l->data[0]->id, 'html'=>$html), array('%d','%s'));
				if ($x) {
					$x = $this->wpdb->update(getTableName('parser-listings'), array('parsed'=>1), array('id'=>$l->data[0]->id), array('%d'), array('%d'));
					if ($x) {
						$out->data = 'pullSingleDetailUnparsed(): inserted into database';
						$out->status = 'OK';
					}
				}		
			}
		} else $out->data = 'pullSingleDetailUnparsed(): no listing found: ';
		return $out;
	}
	private function geocodeAddress($x){
		$out = new stdClass(); $out->status = 'fail'; $out->data = null;
		$geocoded = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$x['address'].'&sensor=false&key='.$this->APIkey));
		if ($geocoded->status == 'OK') { $out->status = 'OK'; $out->data = $geocoded->results[0]; }
		else return $out->data = $geocoded;
		return $out;
	}
	private function getDistance($lat1, $lon1, $lat2, $lon2){
	  $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    return round($dist * 60 * 1.1515, 2);
	}
	private function out(){
		if (func_num_args() > 0){
			if (func_get_arg(0) != 'OK' || func_get_arg(0) === 0){
				if (func_num_args() > 1) echo json_encode(array('status'=>'fail', 'data'=>func_get_arg(1)));
				else echo json_encode(array('status'=>'fail', 'data'=>null));
			} else {
				if (func_num_args() > 1) echo json_encode(array('status'=>'OK', 'data'=>func_get_arg(1)));
				else echo json_encode(array('status'=>'OK', 'data'=>null));
			}
		} else echo json_encode(array('status'=>'fail', 'data'=>'out: no message suppled'));
	}
}

if ($_POST['query'] && $_POST['data']) new parser($_POST['query'], $_POST['data']);
elseif ($_POST['query']) new parser($_POST['query']);
else new parser('no-data-sent');
?>