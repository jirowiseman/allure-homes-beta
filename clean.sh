#!/bin/bash

rm -rf _css/_sellers/*.css &&
printf "Erasing /_css/_sellers/*.css\n" &&
rm -rf _css/_compiled/*.css &&
printf "Erasing /_css/_compiled/*.css\n\n"