<?php
namespace AH;
require_once(__DIR__.'/../_classes/_Controller.class.php');
require_once(__DIR__.'../../../../../wp-includes/pluggable.php');
require_once(__DIR__.'../../../../../wp-config.php');
require_once(__DIR__.'../../../../../wp-load.php');
require_once(__DIR__.'../../../../../wp-includes/wp-db.php');

define('UPDATE_AM_ORDER', 1 << 0);
define('UPDATE_NICKNAME', 1 << 1);
define( 'NOTIFY_EMAIL_BANNER', '<div style="text-align: left;"><p>Hi %first_name%,<br/>
&nbsp;&nbsp;This is a courtesy email with you email banner.  Please copy this into your email signature of mail client.<br/>
%banner%
<br/>
<span style="font-style: italic; color:lightgray;">The Lifestyled Team</span>
www.lifestyledlistings.com
831.508.8821</p></div>' 
);

class SellerDispatch extends Controller {
	// private $agent_match_specialties = array( 20, 23, 59, 9, 14, 144, 8, 86, 65, 69, 147, 70, 168);
	// private $agent_match_lifestyles = array( 143, 42, 164, 167, 123, 25, 163, 18, 169, 145, 6, 40, 49, 44, 172, 66, 10, 72);
	private $logIt = true; 
	// comes from Controller now
	// private $timezone_adjust = 7;
	private $RADIUS_SEARCH_LISTING = 8; // int so we can change it via Options later

	public function __construct(){
		$in = parent::__construct();
		try {
			if (empty($in->query)) throw new \Exception (substr(__FILE__, strpos(__FILE__, '/themes/allure/') + 8).':('.__LINE__.') - no query sent');
			$this->log = $this->logIt ? new Log(__DIR__.'/../_classes/_logs/ajax.log') : null;
			$this->log("ajax - query: $in->query");
			switch ($in->query){
					// from controllerManageCompanies.js
					case 'create-company':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['company'])) throw new \Exception('No company sent.');
						if (empty($in->data['logo'])) throw new \Exception('No logo sent.');
						$id = intval($in->data['id']);
						$company = filter_var($in->data['company'], FILTER_SANITIZE_STRING);
						$url = isset($in->data['url']) && !empty($in->data['url']) ? filter_var($in->data['url'], FILTER_SANITIZE_URL) : '';
						$street_address = isset($in->data['street_address']) && !empty($in->data['street_address']) ? filter_var($in->data['street_address'], FILTER_SANITIZE_STRING) : '';
						$city = isset($in->data['city']) && !empty($in->data['city']) ? filter_var($in->data['city'], FILTER_SANITIZE_STRING) : '';
						$state = isset($in->data['state']) && !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
						$zip = isset($in->data['zip']) && !empty($in->data['zip']) ? filter_var($in->data['zip'], FILTER_SANITIZE_NUMBER_INT) : '';
						$phone = isset($in->data['phone']) && !empty($in->data['phone']) ? filter_var($in->data['phone'], FILTER_SANITIZE_NUMBER_INT) : '';
						$phone2 = isset($in->data['phone2']) && !empty($in->data['phone2']) ? filter_var($in->data['phone2'], FILTER_SANITIZE_NUMBER_INT) : '';
						$logo = isset($in->data['logo']) && !empty($in->data['logo']) ? filter_var($in->data['logo'], FILTER_SANITIZE_STRING) : '';
						$contact = isset($in->data['contact']) && !empty($in->data['contact']) ? filter_var($in->data['contact'], FILTER_SANITIZE_STRING) : '';
						$contact_email = isset($in->data['contact_email']) && !empty($in->data['contact_email']) ? filter_var($in->data['contact_email'], FILTER_SANITIZE_EMAIL) : '';
						$type = isset($in->data['type']) && !empty($in->data['type']) ? filter_var($in->data['type'], FILTER_SANITIZE_NUMBER_INT) : 0;
						$creating = isset($in->data['creating']) && !empty($in->data['creating']) ? filter_var($in->data['creating'], FILTER_SANITIZE_NUMBER_INT) : 0;

						$this->log("create-company got for data:".print_r($in->data, true));

						$path = __DIR__."/../_img/_agency/$logo";
						$logo_x_scale = 0.0;
						if (file_exists($path)) {
							$width = $height = 0;
							$mime = 0;
							list($width, $height, $mime, $attr) = getimagesize($path);
							if (!$width || !$height)
								$out = new Out('fail', "Could not get dimension of $logo, got w:$width, h:$height");
							else {
								$logo_x_scale = number_format( (float)$width/$height, 2 );
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
								if (empty($seller)) 
									$out = new Out('fail', "Could not find seller:$id to update");
								else {
									$seller = array_pop($seller);
									$association = null;
									if (!empty($seller->association))
										$association = $this->getClass('Associations')->get((object)['where'=>['id'=>$seller->association]]);

									if (empty($association) ||
										$creating) {
										$fields = new \stdClass();
										$fields->company = ucwords($company);
										$fields->logo = $logo;
										$fields->logo_x_scale = $logo_x_scale;
										if (!empty($url)) $fields->url = $url;
										if (!empty($street_address)) $fields->street_address = ucwords($street_address);
										if (!empty($city)) $fields->city = $city;
										if (!empty($state)) $fields->state = $state;
										if (!empty($zip)) $fields->zip = $zip;
										if (!empty($phone)) $fields->phone = $phone;
										if (!empty($phone2)) $fields->phone2 = $phone2;
										if (!empty($contact)) $fields->contact = ucwords($contact);
										if (!empty($contact_email)) $fields->contact_email = strtolower($contact_email);
										if (!empty($type)) $fields->type = $type;
										$x = $this->getClass('Associations')->add($fields);
										$this->log("create-company - adding to Associations:".print_r($fields, true));

										if (!empty($x)) {
											$company = $this->getClass('Associations')->get((object)['where'=>['id'=>$x]]);
											$out = new Out('OK', ['id'=>$x,
																  'agency_logo_x_scale'=>$logo_x_scale,
																  'company'=>!empty($company) ? $company[0] : null ]);

											if (!$this->getClass('Sellers')->set([(object)['where'=>['id'=>$id],
																							'fields'=>['association'=>$x]]])) {
												$this->log("create-company - Could not update seller:$id association to $x");
												$out = new Out('fail', "Could not update seller:$id association to $x");
											}
											else {
												$this->log("create-company - Updated seller:$id association to $x");
												setcookie('AGENCY', $company[0]->id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												setcookie('AGENCY_ID', $company[0]->id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												setcookie('AGENCY_LOGO', $company[0]->logo, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												setcookie('AGENCY_LOGO_X_SCALE', $company[0]->logo_x_scale, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												setcookie('AGENCY_URL', !empty($company[0]->url) ? $company[0]->url : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												setcookie('AGENCY_SHORTNAME', !empty($company[0]->shortname) ? $company[0]->shortname : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
												$_SESSION['AGENCY'] = $company[0]->id;
												$_SESSION['AGENCY_ID'] = $company[0]->id;
												$_SESSION['AGENCY_LOGO'] = !empty($company[0]->logo) ? $company[0]->logo : 'blank.jpg';
												$_SESSION['AGENCY_LOGO_X_SCALE'] = !empty($company[0]->logo_x_scale) ? $company[0]->logo_x_scale : 1.00;
												$_SESSION['AGENCY_URL'] = !empty($company[0]->url) ? $company[0]->url : '';
												$_SESSION['AGENCY_SHORTNAME'] = !empty($company[0]->shortname) ? $company[0]->shortname : '';
											}
										}
										else
											$out = new Out('fail', "Failed to add $company to DB");
									}
									else { // have existing association 
										$association = array_pop($association);
										$fields = new \stdClass();
										if ( ucwords($company) != $association->company )
											$fields->company = ucwords($company);
										if ( abs($logo_x_scale*100) != abs(floatval($association->logo_x_scale)*100) ) {
											$this->log("create-company - association:$association->id current logo_x_scale:$association->logo_x_scale is different from calculated:$logo_x_scale");
											$fields->logo_x_scale = $logo_x_scale;
										}

										$this->log("create-company - association:$association->id, existing data:".print_r($association, true));

										if (!empty($url) && $association->url != $url) $fields->url = $url;
										if (!empty($street_address) && $association->street_address != $street_address) $fields->street_address = ucwords($street_address);
										if (!empty($city) && $association->city != $city) $fields->city = $city;
										if (!empty($state) && $association->state != $state) $fields->state = $state;
										if (!empty($zip) && $association->zip != $zip) $fields->zip = $zip;
										if (!empty($phone) && $association->phone != intval($phone)) $fields->phone = $phone;
										if (!empty($phone2) && $association->phone2 != intval($phone2)) $fields->phone2 = $phone2;
										if (!empty($contact) && $association->contact != $contact) $fields->contact = ucwords($contact);
										if (!empty($contact_email) && $association->contact_email != $contact_email) $fields->contact_email = strtolower($contact_email);
										if (!empty($type) && intval($association->type) != intval($type)) $fields->type = $type;

										if (count((array)$fields)) {
											$x = $this->getClass('Associations')->set([(object)['where'=>['id'=>$association->id],
																								'fields'=>$fields]]);
											$this->log('create-company - upating association:$association->id was '.(!empty($x) ? 'successful' : 'failure').' for these fields:'.print_r($fields, true));
											if (!empty($x)) {
												$association = $association = $this->getClass('Associations')->get((object)['where'=>['id'=>$association->id]]);
												$association = array_pop($association);
											}
										}
										else
											$this->log('create-company - nothing to update for association:$association->id');
										$out = new Out('OK', ['id'=>$association->id,
															  'agency_logo_x_scale'=>$logo_x_scale,
															  'company'=>$association,
															  'updateOnly'=>1]);


									}
								}
							}
						}
						else
							$out = new Out('fail', "Could not find $logo on system");
						break;

					// from seller.js
					case 'update-portal-user-flags-unset':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['flag'])) throw new \Exception('No flag sent.');
						if (empty($in->data['agentID'])) throw new \Exception('No agentID sent.');
						$portalUserId = $in->data['id'];
						$flag = intval($in->data['flag']);
						$agentID = intval($in->data['agentID']);
						$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserId]]);
						if (!empty($portalUser)) {
							if ( ($portalUser[0]->flags & $flag)) {
								$fields = ['flags'=>$portalUser[0]->flags & ~$flag];
								$updateMeta = false;
								if ($flag == PORTAL_USER_HIDDEN) {
									$hidden = null;
									if (!empty($portalUser[0]->meta)) foreach($portalUser[0]->meta as &$meta) {
										if ($meta->action == HIDDEN_AGENTS) {
											$hidden = $meta;
											if (!empty($meta->list) &&
												in_array($agentID, $meta->list)) {
												$x = [];
												$updateMeta = true;
												foreach($meta->list as $hider)
													if ($hider != $agentID)
														$x[] = $hider;
													else
														$this->log("update-portal-user-flags-unset removed $agentID from HIDDEN_AGENTS for $portalUserId");

												$meta->list = $x;
											}
										}
										unset($meta);
									}
								}
								if ($updateMeta)
									$fields['meta'] = $portalUser[0]->meta;
								else
									$this->log("update-portal-user-flags-unset no meta data to update");

								$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserId],
																			  'fields'=>$fields]]);
								unset($fields);
							}
							$out = new Out('OK');
						}
						else
							$out = new Out('fail', "Unable to find portalUser:$portalUserId");
						break;
						break;

					case 'update-portal-user-flags':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['flag'])) throw new \Exception('No flag sent.');
						if (empty($in->data['agentID'])) throw new \Exception('No agentID sent.');
						$portalUserId = $in->data['id'];
						$flag = intval($in->data['flag']);
						$agentID = intval($in->data['agentID']);
						$portalUser = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$portalUserId]]);
						if (!empty($portalUser)) {
							if ( !($portalUser[0]->flags & $flag)) {
								$fields = ['flags'=>$portalUser[0]->flags | $flag];
								$updateMeta = false;
								if ($flag == PORTAL_USER_HIDDEN) {
									$hidden = null;
									if (!empty($portalUser[0]->meta)) foreach($portalUser[0]->meta as &$meta) {
										if ($meta->action == HIDDEN_AGENTS) {
											$this->log("update-portal-user-flags found a HIDDEN_AGENTS meta");
											$hidden = $meta;
											if (!in_array($agentID, $meta->list)) {
												$meta->list[] = $agentID;
												$updateMeta = true;
												$this->log("update-portal-user-flags added $agentID to HIDDEN_AGENTS for $portalUserId");
											}
										}
										unset($meta);
									}
									if (!$hidden) {
										$meta = new \stdClass();
										$meta->action = HIDDEN_AGENTS;
										$meta->list = [$agentID];
										$portalUser[0]->meta[] = $meta;
										$updateMeta = true;
										$this->log("update-portal-user-flags new HIDDEN_AGENTS for $portalUserId added $agentID");
									}
								}
								if ($updateMeta)
									$fields['meta'] = $portalUser[0]->meta;
								else
									$this->log("update-portal-user-flags no meta data to update");
								$this->getClass('PortalUsers')->set([(object)['where'=>['id'=>$portalUserId],
																			  'fields'=>$fields]]);
								unset($fields);
							}
							$out = new Out('OK');
						}
						else
							$out = new Out('fail', "Unable to find portalUser:$portalUserId");
						break;

					case 'email-agent-email-banner':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['banner'])) throw new \Exception('No banner sent.');
						$agentID = $in->data['id'];
						$banner = stripslashes( $in->data['banner'] );
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$agentID]]);

						$this->log("email-agent-email-banner for agent:$agentID, banner:$banner");
						$sent = false;
						if (!empty($seller)) {
							$seller = array_pop($seller);
							$email = $seller->email;
							if (!empty($seller->meta)) foreach($seller->meta as $meta) {
								if ($meta->action == SELLER_PROFILE_DATA &&
									!empty($meta->contact_email) ) {
									$email = $meta->contact_email;
									$this->log("sendEmail is using contact_email:$email for sellerId:$agentID - $seller->first_name $seller->last_name");
									break;
								}
							}
							$msg = NOTIFY_EMAIL_BANNER;
							$msg = str_replace('%first_name%', $seller->first_name, $msg);
							$msg = str_replace('%banner%', $banner, $msg);

							$sent = $this->getClass('Email')->sendMail($email, "Email banner", $msg,
																		'Agent Assist',
																		'Please use the banner in your email signature');
						}
						
						$out = new Out($sent ? 'OK' : 'fail');
						break;

					case  'get-portal-user':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');

						$user = $this->getClass('PortalUsers')->get((object)['where'=>['id'=>$in->data['id']]]);
						$out = new Out( !empty($user) ? 'OK' : 'fail', !empty($user) ? $user[0] : "Failed to find portal user with id:".$in->data['id']);
						break;

					case 'update-geocode':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['lng'])) throw new \Exception('No lng sent.');
						if (empty($in->data['lat'])) throw new \Exception('No lat sent.');

						$this->log("update-geocode id:{$in->data['id']}, lng:{$in->data['lng']}, lat:{$in->data['lat']}");

						$geo = $this->getClass("ListingsGeoinfo")->get((object)['where'=>['listing_id'=>$in->data['id']]]);
						$mode = 'update';
						if (empty($geo)) {
							$geo = new \stdClass;
							$geo->lng = $in->data['lng'];
							$geo->lat = $in->data['lat'];
							$geo->listing_id = $in->data['id'];
							$geo->address = $in->data['address'];
							$x = $this->getClass("ListingsGeoinfo")->add($geo);
							$mode = 'add';
						}
						else {
							$fields = [];
							$this->log("update-geocode - existing:".print_r($geo[0], true));
							if ( empty($geo[0]->lng) ||
								$geo[0]->lng == -1) {
								$fields['lng'] = $in->data['lng'];
								$fields['lat'] = $in->data['lat'];
							}
							if ( empty($geo[0]->address) ||
								$geo[0]->address != $in->data['address']) 
								$fields['address'] = $in->data['address'];

							if (count($fields)) {
								$x = $this->getClass("ListingsGeoinfo")->set([(object)['where'=>['listing_id'=>$in->data['id']],
																					  'fields'=>$fields]]);
								$this->log("update-geocode fields:".print_r($fields, true));
							}
							else {
								$x = 1;
								$this->log("update-geocode - already have same data");
							}
							$this->log("update-geocode set:".(empty($x) ? 'failed' : print_r($x, true)));
						}
						$out = new Out( !empty($x) ? 'OK' : 'fail', !empty($x) ? "$mode lng/lat" : "failed... oops");
						break;
					case 'get-sample-listing':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['tag'])) throw new \Exception('No tag sent.');
						if (empty($in->data['location'])) throw new \Exception('No location sent.');

						$tag = $this->getClass('Tags')->get((object)['where'=>['id'=>$in->data['tag']]]);
						if (!empty($tag)) {
							// add city tag to database if needed
							$item = ['specialty'=>$in->data['tag'],
									 'location'=>$in->data['location']];
							$this->addCityTag($item);
							unset($item);

							// now go find a listing
							$tag = array_pop($tag);
							$Listings = $this->getClass('Listings');
							if ($tag->type != 1) {// listing type tag
								if ($tag->id == 170) {// multilingual
									$sql = "SELECT a.id, a.images FROM {$Listings->getTableName()} AS a ";
									$sql.= "WHERE a.city_id = {$in->data['location']} ";
									$sql.= "ORDER BY a.clicks DESC LIMIT 5";
								}
								else {
									$sql = "SELECT a.id, a.images FROM {$Listings->getTableName()} AS a ";
									$sql.= "INNER JOIN {$Listings->getTableName('listings-tags')} AS b ON b.listing_id = a.id ";
									$sql.= "WHERE a.city_id = {$in->data['location']} AND b.tag_id = {$in->data['tag']} ";
									$sql.= "ORDER BY a.clicks DESC LIMIT 5";
									$this->log("get-sample-listing for type 0 - sql:$sql");
								}
								$listing = $Listings->rawQuery($sql);
								if (empty($listing)) {
									$this->log("get-sample-listing - failed type 0, for any listing in this city:{$in->data['locationStr']} using tag:{$in->data['tagStr']}");
									$out = new Out('fail', ['status'=>"No example listings found at this time in {$in->data['locationStr']} matching {$in->data['tagStr']}.  However, you will still be recommended to buyers searching in the area for {$in->data['tagStr']}."]);
								}
								else {
									$gotOne = false;
									foreach($listing as $l) {
										$l->images = json_decode($l->images);
										$gotOne = $this->listingHasImages($l);
										if ($gotOne) break;
										unset($l);
									}
									if ($gotOne) {
										$out = new Out('OK', ['status'=>"Found listing:{$l->id} in City:{$in->data['locationStr']} using tag:{$in->data['tagStr']}",
															  'id'=>$l->id]);
										$this->log("get-sample-listing - Found listing:{$l->id} in City:{$in->data['locationStr']} using tag:{$in->data['tagStr']}");
									}
									else 
										$out = new Out('fail', ['status'=>"Listing with tag:{$in->data['tagStr']} in {$in->data['locationStr']} with valid images was not found in database"]);
								}
							}
							else { // city tag
								$Cities = $this->getClass('Cities');
								$city = $Cities->get((object)['where'=>['id'=>$in->data['location']]]);
								if (!empty($city)) {
									$city = array_pop($city);
									$this->log("get-sample-listing for type 1, city:$city->city, $city->state has no geocoding");

									$sql = "SELECT a.id, a.images FROM {$Listings->getTableName()} AS a ";
									$sql.= "INNER JOIN {$Listings->getTableName('cities-tags')} AS b ON b.city_id = a.city_id ";
									$sql.= "WHERE a.city_id = {$in->data['location']} AND b.tag_id = {$in->data['tag']} ";
									$sql.= "ORDER BY a.clicks DESC LIMIT 5";
									$this->log("get-sample-listing for type 1 - sql:$sql");

									$listing = $Listings->rawQuery($sql);
									if (empty($listing)) {
										$this->log("get-sample-listing - failed first try for type 1, no tag, get any listing in this city:{$in->data['locationStr']}");
										$listing = $Listings->get((object)['where'=>['city_id'=>$in->data['location']],
																			'what'=>['id','images'],
																			'limit'=>5,
																			'orderby'=>'clicks',
																			'order'=>'desc']);
										if (empty($listing)) {
											$this->log("get-sample-listing - failed second try for type 1, for any listing in this city:{$in->data['locationStr']}");
											$out = new Out('fail', ['status'=>"No listings found in City:{$in->data['locationStr']}"]);
											break;
										}
									}

									$gotOne = false;
									if (!empty($listing)) foreach($listing as $l) {
										$l->images = json_decode($l->images);
										$gotOne = $this->listingHasImages($l);
										if ($gotOne) break;
										unset($l);
									}
									if ($gotOne) {
										$out = new Out('OK', ['status'=>"Found listing:{$l->id} in City:{$in->data['locationStr']} using tag:{$in->data['tagStr']}",
															  'id'=>$l->id]);
										$this->log("get-sample-listing - Found listing:{$l->id} in City:{$in->data['locationStr']} using tag:{$in->data['tagStr']}");
									}
									else 
										$out = new Out('fail', ['status'=>"Listing with tag:{$in->data['tagStr']} in {$in->data['locationStr']} with valid images was not found in database"]);
								}
								else
									$out = new Out('fail', ['status'=>"City:{$in->data['locationStr']} was not found in database"]);
							}
						}
						else
							$out = new Out('fail', ['status'=>"Tag:{$in->data['tagStr']} was not found in database"]);
						break;

					case 'remove-photo':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						$photo = isset($in->data['name']) ? $in->data['name'] : '';
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$in->data['id']]]);
						$success = false;
						$msg = '';
						if (!empty($seller)) {
							$seller = array_pop($seller);
							if (!empty($seller->photo)) {
								$fields = [];
								$isSame = !empty($photo) ? strcmp($seller->photo, $photo) == 0 : 1;
								$this->getClass("Image")->removeImage('authors', $seller->photo);
								if (!$isSame &&
									!empty($photo)) // delete incoming name also
									$this->getClass("Image")->removeImage('authors', $photo);
								if ($seller->flags & SELLER_IS_PREMIUM_LEVEL_2) {
									$seller->flags = $fields['flags'] = $seller->flags | SELLER_NEEDS_BASIC_INFO;
								}
								$fields['photo'] = 'NULL';
								$seller->photo = null;
								$success = $this->getClass("Sellers")->set([(object)['where'=>['id'=>$in->data['id']],
																		  			 'fields'=>$fields]]);
								$msg = "remove-photo - updating seller with id:".$in->data['id']." with NULL photo and flags:".$seller->flags.", was a ".(!empty($success) ? "success" : "failure");
								unset($seller);
							}
							else {
								$msg = "remove-photo - seller with id:".$in->data['id']." already had a null photo";
								unset($seller);
							}
						}
						else
							$msg = "remove-photo - failed to locate seller with id:".$in->data['id'];
						$this->log($msg);
						$out = new Out(!$success ? 'fail' : 'OK', $msg);
						break;

					case 'remove-logo':
						if (empty($in->data)) throw new \Exception('No data sent.');
						$photo = isset($in->data['name']) ? $in->data['name'] : '';
						$imgDir = isset($in->data['imgDir']) ? $in->data['imgDir'] : '';
						$id = isset($in->data['id']) ? intval($in->data['id']) : 0;
						if (empty($photo))
							throw new \Exception('No photo sent.');
						if (empty($imgDir))
							throw new \Exception('No imgDir sent.');

						$retval = $this->getClass("Image")->removeImage($imgDir, $photo);
						if ($retval && $id) {
							$this->getClass('Associations')->set([(object)['where'=>['id'=>$id],
																			'fields'=>['logo'=>'NULL',
																					   'logo_x_scale'=>'NULL']]]);
							setcookie('AGENCY_LOGO', '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
							$_SESSION['AGENCY_LOGO'] = '';
						}
						$out = new Out(!$retval ? 'fail' : 'OK',!$retval ? "Failed to delete logo" : "Deleted logo");
						break;

					case 'update-association-logo':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['logo'])) throw new \Exception('No logo sent.');
						$id = intval($in->data['id']);
						$logo = isset($in->data['logo']) && !empty($in->data['logo']) ? filter_var($in->data['logo'], FILTER_SANITIZE_STRING) : '';
						$this->log("create-company got for data:".print_r($in->data, true));

						$path = __DIR__."/../_img/_agency/$logo";
						$logo_x_scale = 0.0;
						if (file_exists($path)) {
							$width = $height = 0;
							list($width, $height, $type, $attr) = getimagesize($path);
							if (!$width || !$height)
								$out = new Out('fail', "Could not get dimension of $logo, got w:$width, h:$height");
							else {
								$logo_x_scale = number_format( (float)$width/$height, 2 );
								$fields = ['logo'=>$logo,
											'logo_x_scale'=>$logo_x_scale];
								$x = $this->getClass('Associations')->set([(object)['where'=>['id'=>$id],
																					'fields'=>$fields]]);
								$this->log("update-association-logo - update to association:$id with ".print_r($fields, true));
								$out = new Out(!empty($x) ? 'OK' : 'fail', !empty($x) ? "Updated association:$id" : "Failed to update the logo");
							}
						}
						else
							$out = new Out('fail', "Could not find $logo on system");
						break;

					case 'check-lifestyle-availability':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['city_id'])) throw new \Exception('No city id sent.');
						if (empty($in->data['tag_id'])) throw new \Exception('No tag id sent.');
						$city_id = $in->data['city_id'];
						$tag_id  = $in->data['tag_id'];
						$Sellers = $this->getClass('Sellers');
						$opt = $this->getClass('Options')->get((object)['where'=>['opt'=>'MaxLifestylePerCity']]);
						$max =  !empty($opt) ? intval($opt[0]->value) : 3;
						$sql = 'SELECT a.author_id, a.first_name, a.last_name, c.tag_id  FROM '.$Sellers->getTableName().' AS a ';
						//$sql.= 'INNER JOIN '.$Sellers->getTableName('cities').' AS b ON a.city = b.city AND a.state = b.state ';
						$sql.= 'INNER JOIN '.$Sellers->getTableName('sellers-tags')." AS c ON c.author_id = a.author_id ";
						$sql.= "WHERE a.flags & 16 AND c.type = 1 AND c.tag_id = $tag_id AND c.city_id = $city_id";
						$this->log('check-lifestyle-availability - sql:'.$sql);
						$sellers = $Sellers->rawQuery($sql);
						$retval = empty($sellers) ? 'OK' : (count($sellers) < $max ? 'OK' : 'fail');
						$out = new Out($retval, $sellers);
						break;
					case 'check-coupon':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['coupon'])) throw new \Exception('No coupon id sent.');
						$Register = $this->getClass('Register');
						$out = $Register->verifyInvite($in->data['coupon'], IC_FEATURE_PORTAL);
						if ($out->status != 'OK') {
							$args = array(
							    'posts_per_page'   => -1,
							    'orderby'          => 'title',
							    'order'            => 'asc',
							    'post_type'        => 'shop_coupon',
							    'post_status'      => 'publish',
							);
							    
							$coupons = get_posts( $args );
							if (!empty($coupons)) {
								foreach($coupons as $code) {
									if ($code->post_title == strtolower($in->data['coupon'])) {
										$expiryDate = get_post_meta($code->ID, 'expiry_date', true);
										if (strtotime($expiryDate) < time()) {
											$out = new Out('fail', ['msg'=>"Coupon is expired."]);
											$this->log("check-coupon - called with ecommerce coupon: $code->post_title, expired on $expiryDate");
										}
										else {
											$this->log("check-coupon - called with ecommerce coupon: $code->post_title");
											$amount = get_post_meta($code->ID, 'coupon_amount', true);
											$out = new Out('OK', $amount);
										}
									}
									unset($code);
								}
							}
							else
								$out = new Out('fail', 'Coupon not found');
						}
						else
							$out = new OUt('OK', IC_ACCEPTED);
						break;

					case 'register-pre-portal':
					case 'register-pre-lifestyle':
						if (empty($in->data)) throw new \Exception('No data sent.');					
						if (empty($in->data['email'])) throw new \Exception('No email sent.');
						if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
						if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
						if (empty($in->data['phone'])) throw new \Exception('No phone sent.');
						if (empty($in->data['password'])) throw new \Exception('No password sent.');
						if (empty($in->data['login'])) throw new \Exception('No login id sent.');
						if (empty($in->data['realtor_id'])) throw new \Exception('No realtor_id sent.');
						if (empty($in->data['state'])) throw new \Exception('No state if registry sent.');
						// if (empty($in->data['coupon'])) throw new \Exception('No coupon id sent.');

						$portal = '';
						$desc = '';
						$tag_id = 0;
						$tag = '';
						$city_id = 0;
						$city = '';
						$icType = IC_FEATURE_PORTAL;
						$optIn = isset($in->data['optIn']) ? (is_true($in->data['optIn']) ? 1 : 0) : 0;

						switch($in->query) {
							case 'register-pre-portal':
								if (empty($in->data['portal'])) throw new \Exception('No portal sent.');
								$portal = filter_var($in->data['portal'], FILTER_SANITIZE_STRING);
								break;
							case 'register-pre-lifestyle':
								if (empty($in->data['tag_id'])) throw new \Exception('No tag_id sent.');
								if (empty($in->data['tag'])) throw new \Exception('No tag sent.');
								if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
								if (empty($in->data['city'])) throw new \Exception('No city sent.');
								// if (empty($in->data['desc'])) throw new \Exception('No desc sent.');
								$tag_id = intval($in->data['tag_id']);
								$city_id = intval($in->data['city_id']);
								$desc = isset($in->data['desc']) ? filter_var($in->data['desc'], FILTER_SANITIZE_STRING) : '';
								$tag = $in->data['tag'];
								$city = $in->data['city'];
								$icType = IC_FEATURE_LIFESTYLE;
								break;
						}

						$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
						$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
						$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
						$password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
	   					$realtor_id = !empty($in->data['realtor_id']) ? filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING) : '';
	   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);
	   					$phone = filter_var($in->data['phone'], FILTER_SANITIZE_STRING);
	   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
	   					$coupon = !empty($in->data['coupon']) ? filter_var($in->data['coupon'], FILTER_SANITIZE_STRING) : '';
	   					$et = isset($in->data['et']) ? $in->data['et'] : '';
	   					$key = isset($in->data['key']) ? intval($in->data['key']) : 0;
	   					$special = isset($in->data['special']) ? intval($in->data['special']) : REGISTRATION_SPECIAL_NONE; 
	   					$tracker = !empty($key) ? $this->getClass('EmailTracker')->get((object)['where'=>['id'=>$key]]) : null;

	   					if (!empty($tracker)) {
	   						if (!empty($et)) {
	   							if ($tracker[0]->track_id != $et) {
	   								$out = new Out('fail', "Unauthorized access!  Denied!");
	   								break;
	   							}
	   						}
	   						else {
	   							$out = new Out('fail', "Unauthorized access!  Denied!");
	   							break;
	   						}
	   					}
	   					$noVerification = !empty($tracker); // this API is called from agent-purchase-portal page, so if tracker is valid, then it was from email campaign
	   					$portal = str_replace(" ","_", strtolower($portal) );

	   					$Register = $this->getClass('Register');
	   					$ic = null;
	   					$inviteCode = '';
	   					if (!empty($coupon)) {
		   					$out = $Register->verifyInvite($coupon, $icType, $ic);
		   					if ($out !== false &&
		   						$out->status == 'OK') {
		   						$inviteCode = $coupon;
		   						$coupon = '';
		   						$this->log($in->query." - coupon is now an invitation:$inviteCode");
		   					}
		   					else {
		   						$this->log($in->query." - coupon:$coupon has failed");
		   						$special = REGISTRATION_SPECIAL_NONE;
		   					}
		   				}

		   				$this->log($in->query." - coupon:$coupon, inviteCode:$inviteCode, special:$special");
	   					$regResult = $out = $Register->basicRegistry($email, $first_name, $last_name, $password, $realtor_id, $login, $state, $inviteCode, $special, 0 /*fromAdmin*/,($in->query == 'register-pre-portal' ? ORDER_PORTAL : ORDER_AGENT_MATCH), $optIn); 
	   					$this->log($in->query." - after basicRegistry:".print_r($regResult, true));

	   					if ($out->status != 'OK') { // then this person already existed, so try to find out who..
	   						$user = null;
	   						if ( strpos($out->data, "This email address") !== false ) {
	   							$user = get_user_by( 'email', $email );
	   						}
	   						else if ( strpos($out->data, "his login id") != false) {
	   							$user = get_user_by( 'login', $login );
	   						}

	   						$this->log("$in->query - user:".($user ? $user->ID : "none").", login:$login");

	   						if ( !empty($user) ) {
	   							// log them in
	   							if ( $Register->loginUser($login, $password) !== true ) {
	   								$this->log("$in->query - failed to login:$login");
   									$out = new Out('fail', "Please verify your login and password.  We cannot log you in.");
   									break;
   								}
	   							$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$user->ID]]);
	   							if (!empty($seller)) {
	   								$seller = array_pop($seller);
	   							}
	   							else { // then odd case of pre-registered, but not as an agent and got mail campaign, or upgrading an organic user
	   								if (!empty($inviteCode) &&
	   									 $special != REGISTRATION_SPECIAL_NONE)  {
	   									$this->log("$in-query - calling upgradeRegistry for $login");
		   								$out = $Register->upgradeRegistry($realtor_id, $state, $inviteCode, $special);
		   								if ($out->status != 'OK' ||
		   									 gettype($out->data) != 'object') {
		   									$this->log("$in->query - failed upgradeRegistry: ".print_r($out, true));
		   									break;
		   								}
		   							}

		   							$this->log("$in->query - invite:$inviteCode, special:$special, after upgradeRegistry: ".print_r($out, true));
		   							if ( $out->status != 'OK') {
		   								$this->log("$in->query exiting for $login");
		   								break;
		   							}

	   								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$out->data->id]]);
	   								if (empty($seller)) {
	   									$out = new Out('fail', "Could not retrieve Seller after upgrading registry.");
	   									break;
	   								}
	   								$seller = array_pop($seller);
	   							}

	   							if ($seller->bre != $realtor_id) {
	   								$status = $regResult->data."  But the BRE you entered is not what we have on file.";
	   								$out = new Out('fail',$status);
	   								wp_logout(); // just in case...
									wp_set_current_user(0);
	   								break;
	   							}
	   						}
	   						else {
	   							// $out = new Out('fail', "Please login and go to your Seller's Dashboard and purchase the portal there.")
	   							// something failed... return
	   							break;
	   						}
	   					} 
	   					// if OK, then $out->data should be {'seller_id'=>##} or some message string
	   					elseif (gettype($out->data) != 'object') {
	   						$this->log("$in->query is exiting, returned from basicRegistry:".(isset($out->data) ? print_r($out->data, true) : 'nothing'));
	   						break;
	   					}
	   					else {
	   						if (isset($out->data) &&
	   							isset($out->data->seller_id))
	   							$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$out->data->seller_id]]);
	   						else if (isset($out->data) &&
	   							isset($out->data->id))
	   							$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$out->data->id]]);
	   						else
	   							$seller = $this->getClass('Sellers')->get((object)['where'=>['email'=>$email]]);
							if (empty($seller)) {
								$out = new Out('fail', "Could not retrieve Seller after basic registry.");
								$this->log("in->query - Could not retrieve Seller after basic registry.");
								wp_logout(); // just in case...
								wp_set_current_user(0);
								break;
							}
							$seller = array_pop($seller);
	   					}

	   					$SessionMgr = $this->getClass('SessionMgr');
	   					if ($inviteCode) { // then this is via IC, not a coupon!
	   						// check $icType and not $ic->flags, as the IC can be activated for both or either
	   						if ($icType == IC_FEATURE_PORTAL) {
								$out = $SessionMgr->getPortalName($portal, $inviteCode, $seller->id, $phone);
								if ($out->status == 'OK') {
									$out = new Out('OK', ['status'=>"Good to go!",
														  'msg'=>"Portal invitation accepted for portal:$portal, hit OK to be direct to your profile page to upload your photo.",
														  'url'=>get_home_url().'/sellers/#profile']);
								}
								else {
									$this->log($in->query." - tried $inviteCode, but getPortalName($portal) failed, seller account created");
									$out = new Out('fail', "Tried $inviteCode, but getPortalName($portal) failed");
								}
								break;
							}
							else { // should be IC_FEATURE_LIFESTYLE
								$seller = $this->getAuthor($seller->author_id); // this will make the SELLER_AGENT_MATCH_DATA if not already there
								$am_order = null;
								$metas = [];
								$fields = [];
								if (!empty($seller->meta)) foreach($seller->meta as $i=>&$info) {
									if ($info->action == SELLER_AGENT_ORDER) {
										$am_order = &$info;
									}
									else
										$metas[] = $info;
									unset($info);
								}
								$needUpdate = false;
								$item = (object) [	'mode'=>ORDER_BUYING,
													'type'=>ORDER_AGENT_MATCH,
													'desc'=>$desc,
													'location'=>$city_id,
													'locationStr'=>$city,
													'specialty'=>$tag_id,
													'specialtyStr'=>$tag,
													'cart_item_key'=>0,
													'order_id'=>0,
													'subscriptionType'=>MONTHLY_SUBSCRIPTION,
													'priceLevel'=>GRADE_9
												 ];

								if (!$am_order) {
									$am_order = (object) [
															'action'=>SELLER_AGENT_ORDER,
															'order_id_last_purchased'=>0,
															'order_id_last_cancelled'=>0,
															'agreed_to_terms'=>0,
															'item'=>[$item]
														 ];
									$metas[] = $am_order;
									$needUpdate = true;
								}
								else if (!empty($am_order->item)) {
									$found = false;
									foreach($am_order->item as &$order) {
										$order = (object)$order;
										if (intval($order->type) == ORDER_AGENT_MATCH &&
											intval($order->location) == $city_id &&
											intval($order->specialty) == $tag_id) {
											$found = true;
											if ($order->desc != $desc &&
												!empty($desc)) {
												$order->desc = $desc;
												$metas[] = $am_order;
												$needUpdate = true;
											}
										}
										unset($order);
										if ($found) break;
									}
									if (!$found) {
										$am_order->item[] = $item;
										$metas[] = $am_order;
										$needUpdate = true;
									}
								}
								if ($needUpdate)
									$fields['meta'] = $metas;

								if (empty($seller->mobile) ||
									$seller->mobile != $phone)
									$fields['mobile'] = $phone;
								if (empty($seller->phone))
									$fields['phone'] =$phone;

								if (!empty($city)) {
									$fields['service_areas'] = $city.":8";
									$location = explode(',', $city);
									$fields['flags'] = SELLER_NEEDS_BASIC_INFO;
									if (empty($seller->state))
										$fields['state'] = trim($location[1]);
								}

								if (count($fields))
									$this->getClass('Sellers')->set([(object)['where'=>['author_id'=>$seller->author_id],
																			  'fields'=>$fields]]);
								unset($fields);

								$out = $this->processAMInvite($am_order, $seller, $ic, $Register, true);
								if ($out->status == 'OK') {
									$out = new Out('OK', ['status'=>"Good to go!",
														  'msg'=>"Lifestyle invitation accepted, hit OK to be directed to complete your activation.",
														  'url'=>get_home_url().'/agent-basic-lifestyle-info']);
									// TODO: send email to them here...
									$msg = "<p>Hi $seller->first_name,<br/>";
									$msg.= "Welcome to LifestyledListings.com! Thank you for signing up with us as $tag expert in $city!<br/>";
									$msg.= "To go live as a Lifestyled Agent, simply log in to your agent dashboard and click on Lifestyled Agent to upload<br>";
									$msg.= "a photo and to complete the description as a $tag expert, if you haven't already done so.<br>";
									$msg.= "You can also go to your dashboard's My Profile page fill out some more information that users will be able to view.<br/>";
									$msg.= "If you already haven't, please get a portal also.  Spread the word through your clients and friends, to visit<br/>";
									$msg.= "our site from your portal.  Any listing they view will show you as a portal agent, giving you more opportunities<br/>";
									$msg.= "to get a referral!<br/>";
									$msg.= "Sincerely,<br/>Lifestyled Team</p>";
									$this->getClass('Email')->sendMail($seller->email, 
																"Welcome, .$seller->first_name $seller->last_name!",
																$msg,
																'LifeStyled Agent',
																"Expert in $tag for all of $city");
									$this->getClass('Email')->sendMail(SUPPORT_EMAIL, 
																"Welcome, .$seller->first_name $seller->last_name!",
																$msg,
																'Copy - LifeStyled Agent',
																"Copy - Expert in $tag for all of $city");
								}
								else {
									$origStatus = isset($out->data['status']) ? $out->data['status'] : '';
									$this->log("register-purchase-lifestyle - tried $inviteCode, but processAMInvite failed, seller account created, status:".$out->data['status']);
									$out = new Out('fail', ['status'=>'processAMInvite failed',
															'msg'=>"Have not set seller's tags.".(!empty($origStatus) ? "  Reason:".$origStatus : ''),
															'url'=>get_home_url().'/agent-basic-lifestyle-info']);
									if ( strpos($origStatus, 'has only') === false )
										$this->getClass('Email')->sendMail(SUPPORT_EMAIL, 
																		  "Registered $seller->first_name $seller->last_name",
																		  "Tried $inviteCode, but processAMInvite failed. Have not set seller's tags.".(!empty($origStatus) ? "  Reason:".$origStatus : ''),
																		  "Failure",
																		  "Unable to register agent for $tag in $city");
									else
										$this->getClass('Email')->sendMail(SUPPORT_EMAIL, 
																		  "Registered $seller->first_name $seller->last_name",
																		  "Tried $inviteCode, but processAMInvite failed. Have not set seller's tags.".(!empty($origStatus) ? "  Reason:".$origStatus : ''),
																		  "BasicInfoRequirement",
																		  "Unable to register agent for $tag in $city just yet, need them to fill out more info on lifestyle");
								}
								break;
							} // end else IC_FEATURE_LIFESTYLE
	   					}

	   					$this->log("$in->query - proceeding to process real order, not invite");
	   					// no longer about invite, actual purchase prep
						$out = $SessionMgr->getDirective('none', false, 0);
						$this->log($in->query." - session:".$out->data->session.", directive:".$out->data->directive);

	   					$amOrder = null;
	   					$nicknameMeta = null;
	   					$metas = [];
	   					$needUpdate = 0;
	   					$portalSpec = (object)['subscriptionType'=> MONTHLY_SUBSCRIPTION,
												'priceLevel'=> GRADE_9];

	   					if (!empty($seller->meta)) foreach($seller->meta as &$meta) {
	   						if ($meta->action == SELLER_AGENT_ORDER)
	   							$amOrder = $meta;
	   						elseif ($meta->action == SELLER_NICKNAME)
	   							$nicknameMeta = $meta;
	   						else
	   							$metas[] = $meta;
	   						unset($meta);
	   					}

	   					switch($in->query) {
							case 'register-pre-portal':
			   					if (!$nicknameMeta) {
			   						$meta = new \stdClass();
									$meta->action = SELLER_NICKNAME;
									$meta->nickname = $portal;
									$nicknameMeta = $meta;
									$needUpdate = UPDATE_NICKNAME;
			   					}
			   					elseif ($nicknameMeta->nickname != $portal) {
			   						$nicknameMeta->nickname = $portal;
									$needUpdate = UPDATE_NICKNAME;
			   					}
			   					break;
			   				default:
			   					break;
			   			}

			   			$agreed_to_terms = 1;
			   			$this->log($in->query." - regResult:".print_r($regResult, true));
			   			if (isset($regResult->data) &&
	   						isset($regResult->data->userMode) &&
	   						$regResult->data->userMode == AGENT_ORGANIC) {
			   				$agreed_to_terms = 0;
			   				$this->log($in->query." - agent is ORGANIC");
			   			}

	   					if (!$amOrder) {
	   						$meta = new \stdClass();
							$meta->action = SELLER_AGENT_ORDER;
							$meta->order_id_last_purchased = 0;
							$meta->order_id_last_cancelled = 0;
							$meta->agreed_to_terms = $agreed_to_terms; // need this to be 1, else processOrder() will not make a cart for it.
							$meta->item = [];
							$amOrder = $meta;
							$needUpdate |= UPDATE_AM_ORDER;
	   					}

	   					$found = false;
	   					$alreadyBought = false;
						if (!empty($amOrder->item)) foreach($amOrder->item as $i=>&$item) {
							$item = (object)$item;
							if ($in->query == 'register-pre-portal' &&
								intval($item->type) == ORDER_PORTAL) {
								if ($item->mode == ORDER_BOUGHT) {
									$alreadyBought = true;
								}
								else {
									if (isset($item->nickname) &&
										$item->nickname != $portal) {
										$item->nickname = $portal;
										$needUpdate |= UPDATE_AM_ORDER;
									}
									if ($item->mode != ORDER_BUYING) {
										$item->mode = ORDER_BUYING;
										$needUpdate |= UPDATE_AM_ORDER;
									}
									if (isset($item->priceLevel) &&
										$item->priceLevel != $portalSpec->priceLevel) {
										$item->priceLevel = $portalSpec->priceLevel;
										$needUpdate |= UPDATE_AM_ORDER;
									}
									if (isset($item->subscriptionType) &&
										$item->subscriptionType != $portalSpec->subscriptionType) {
										$item->subscriptionType = $portalSpec->subscriptionType;
										$needUpdate |= UPDATE_AM_ORDER;
									}
								}
								$found = true;
							}
							elseif ($in->query == 'register-pre-lifestyle' &&
								intval($item->type) == ORDER_AGENT_MATCH &&
								intval($item->location) == $city_id &&
								intval($item->specialty) == $tag_id) {
								$amOrder->agreed_to_terms = $agreed_to_terms; // need this to be 1, else processOrder() will not make a cart for it.
								if ($item->mode == ORDER_BOUGHT) {
									$alreadyBought = true;
								}
								else {
									if (!empty($desc) &&
										removeslashes($item->desc) != $desc) {
										$item->desc = $desc;
										$needUpdate |= UPDATE_AM_ORDER;
									}
								}
								$found = true;
							}
							if ($found) break;
							unset($item);
						}

						if ($alreadyBought) {
							// $out = new Out('fail', 'You already own a portal:'.$item->nickname);
							switch($in->query) {
								case 'register-pre-portal':
									$this->log("register-purchase-portal - sellerId:$seller->id, already has a portal:".(isset($item->nickname) ? $item->nickname : ''));
									$out = new Out('fail', ['msg'=>"You already has a portal:".(isset($item->nickname) ? $item->nickname : ''),
															'url'=>get_home_url().'/sellers/#portal']);
									break;
								case 'register-pre-lifestyle':	
									$this->log("register-purchase-lifestyle - sellerId:$seller->id, already has this lifestyle");
									$out = new Out('fail', ['msg'=>"You already has a lifestye:$tag_id at $city_id",
															'url'=>get_home_url().'/sellers/#agent_match_reserve']);
									break;
							}
							break;
						}
						elseif (!$found) {
							// $out = new Out('fail', "Failed to find ORDER_PORTAL");
							// break;
							switch($in->query) {
								case 'register-pre-portal':
									$portalOrder = new \stdClass();
									$portalOrder->type = ORDER_PORTAL;
									$portalOrder->mode = ORDER_BUYING;
									$portalOrder->order_id = 0;
									$portalOrder->cart_item_key = 0;
									$portalOrder->priceLevel = $portalSpec->priceLevel;
									$portalOrder->subscriptionType = $portalSpec->subscriptionType;
									$portalOrder->nickname = $portal;
									$amOrder->item[] = $portalOrder;
									$needUpdate |= UPDATE_AM_ORDER;
									break;
								case 'register-pre-lifestyle':	
									$item = (object) [	'mode'=>ORDER_BUYING,
														'type'=>ORDER_AGENT_MATCH,
														'desc'=>$desc,
														'location'=>$city_id,
														'locationStr'=>$city,
														'specialty'=>$tag_id,
														'specialtyStr'=>$tag,
														'cart_item_key'=>0,
														'order_id'=>0,
														'subscriptionType'=>MONTHLY_SUBSCRIPTION,
														'priceLevel'=>GRADE_6
												 ];
									$amOrder->item[] = $item;
									$amOrder->agreed_to_terms = $agreed_to_terms; // need this to be 1, else processOrder() will not make a cart for it.
									$needUpdate |= UPDATE_AM_ORDER;
									break;
							}
						}

						if ($nicknameMeta)
							$metas[] = $nicknameMeta;
						$metas[] = $amOrder;
						$fields = [];
						$needModUpdate = false;

						// just make sure we have the latest info...
						if (!isset($seller->mobile) ||
							empty($seller->mobile))
							$fields['mobile'] = $phone;
						elseif ($seller->mobile != $phone) {
							$fields['mobile'] = $phone;
							$needUpdate = true;
							$modMeta = null;
							$allMetas = [];
							foreach($metas as $meta) {
								$meta = (object)$meta;
								if ($meta->action == SELLER_MODIFIED_PROFILE_DATA) {
									$modMeta = $meta;
								}
								else
									$allMetas[] = $meta;
								unset($meta);
							}

							if ($modMeta &&
								empty($modMeta->mobile)) {
								$modMeta->mobile = 1;
								$needModUpdate = true;
							}
							else {
								$modMeta = new \stdClass();
								$modMeta->action = SELLER_MODIFIED_PROFILE_DATA;
								$modMeta->first_name = 0;
								$modMeta->last_name = 0;
								$modMeta->company = 0;
								$modMeta->website = 0;
								$modMeta->phone = 0;
								$modMeta->mobile = 1;
								$modMeta->email = 0;
								$needModUpdate = true;
							}
							if ($needModUpdate) {
								$allMetas[] = $modMeta;
								unset($metas);
								$metas = $allMetas;
							}
						}

						if ($needUpdate ||
							$needModUpdate) 
							$fields['meta'] = $metas;

						if (!$noVerification)
							$fields['flags'] = $seller->flags | SELLER_NEEDS_VERIFICATION_OF_BRE | SELLER_NEEDS_BASIC_INFO;
						else
							$fields['flags'] = $seller->flags | SELLER_NEEDS_BASIC_INFO;
						$fields['service_areas'] = $city.":8";
						$location = explode(',', $city);
						if (empty($seller->state))
							$fields['state'] = trim($location[1]);

						if (count($fields))
							$this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
																	  'fields'=>$fields]]);
						unset($fields, $metas);

						$this->log($in->query." - agreed_to_terms:$agreed_to_terms");

						if ($agreed_to_terms)
							$out = new Out('OK', ['status'=>"Agent $first_name $last_name with id:$seller->id is logged in and registered",
												  'coupon'=>$coupon,
												  'id'=>$seller->id,
												  'userId'=>$seller->author_id]);
						else {// else AGENT_ORGANIC
							$out = $regResult;
							$out->data->url = get_home_url();
						}
						break;

					case 'purchase-portal-post-registration':
					case 'purchase-lifestyle-post-registration':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						if (empty($in->data['email'])) throw new \Exception('No email sent.');
						if (empty($in->data['first_name'])) throw new \Exception('No first_name sent.');
						if (empty($in->data['last_name'])) throw new \Exception('No last_name sent.');
						if (empty($in->data['phone'])) throw new \Exception('No phone sent.');
						if (empty($in->data['password'])) throw new \Exception('No password sent.');
						if (empty($in->data['login'])) throw new \Exception('No login id sent.');
						if (empty($in->data['realtor_id'])) throw new \Exception('No realtor_id sent.');
						if (empty($in->data['state'])) throw new \Exception('No state if registry sent.');
						// if (empty($in->data['coupon'])) throw new \Exception('No coupon id sent.');

						$portal = '';
						$tag_id = 0;
						$tag = '';
						$city_id = 0;
						$city = '';
						$orderType = ORDER_PORTAL;
						switch($in->query) {
							case 'purchase-portal-post-registration':
								if (empty($in->data['portal'])) throw new \Exception('No portal sent.');
								$portal = filter_var($in->data['portal'], FILTER_SANITIZE_STRING);
								$portal = preg_replace("/[-\s]+/", '_', $portal);
								$portal = preg_replace('/[^a-zA-Z0-9_]/', '', $portal); 
								$portal = strtolower($portal);
								break;
							case 'purchase-lifestyle-post-registration':
								if (empty($in->data['tag_id'])) throw new \Exception('No tag_id sent.');
								if (empty($in->data['tag'])) throw new \Exception('No tag sent.');
								if (empty($in->data['city_id'])) throw new \Exception('No city_id sent.');
								if (empty($in->data['city'])) throw new \Exception('No city sent.');
								$tag_id = isset($in->data['tag_id']) ? intval($in->data['tag_id']) : 0;
	   							$city_id = isset($in->data['city_id']) ? intval($in->data['city_id']) : 0;
	   							$tag = $in->data['tag'];
								$city = $in->data['city'];
	   							$orderType = ORDER_AGENT_MATCH;
								break;
						}

						$email = filter_var($in->data['email'], FILTER_SANITIZE_EMAIL);
						$first_name = filter_var($in->data['first_name'], FILTER_SANITIZE_STRING);
						$last_name = html_entity_decode( filter_var($in->data['last_name'], FILTER_SANITIZE_STRING), ENT_QUOTES);
						$password = filter_var($in->data['password'], FILTER_SANITIZE_STRING);
	   					$realtor_id = !empty($in->data['realtor_id']) ? filter_var($in->data['realtor_id'], FILTER_SANITIZE_STRING) : '';
	   					$login = filter_var($in->data['login'], FILTER_SANITIZE_STRING);
	   					$state = !empty($in->data['state']) ? filter_var($in->data['state'], FILTER_SANITIZE_STRING) : '';
	   					$coupon = !empty($in->data['coupon']) ? filter_var($in->data['coupon'], FILTER_SANITIZE_STRING) : '';
	   					$et = isset($in->data['et']) ? $in->data['et'] : '';
	   					$key = isset($in->data['key']) ? intval($in->data['key']) : 0;
	   					$id = isset($in->data['id']) ? intval($in->data['id']) : 0;
	   					$tracker = !empty($key) ? $this->getClass('EmailTracker')->get((object)['where'=>['id'=>$key]]) : null;
	   					if (!empty($tracker)) {
	   						if (!empty($et)) {
	   							if ($tracker[0]->track_id != $et) {
	   								$out = new Out('fail', "Unauthorized access!  Denied!");
	   								break;
	   							}
	   						}
	   						else {
	   							$out = new Out('fail', "Unauthorized access!  Denied!");
	   							break;
	   						}
	   					}

	   					$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
	   					if (empty($seller)) {
	   						$out = new Out('fail', "Failed to find seller");
	   						break;
	   					}
	   					$seller = array_pop($seller);

	   					$user = wp_get_current_user();
	   					$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;

	   					if (empty($seller->author_id ||
	   						$seller->author_id != $userId) ) {
	   						$out = new Out('fail', "Current user is not the seller");
	   						break;
	   					}

	   					
	   					$amOrder = null;
	   					$bre = null;
	   					if (!empty($seller->meta)) foreach($seller->meta as &$meta) {
	   						if ($meta->action == SELLER_AGENT_ORDER)
	   							$amOrder = $meta;
	   						elseif ($meta->action == SELLER_NICKNAME)
	   							$nicknameMeta = $meta;
	   						elseif ($meta->action == SELLER_BRE)
	   							$bre = $meta;
	   						else
	   							$metas[] = $meta;
	   						unset($meta);
	   					}

	   					if (!$amOrder) {
	   						$out = new Out('fail', "Failed to find valid order");
	   						break;
	   					}

	   					$portalSpec = (object)['subscriptionType'=> 0,
												'priceLevel'=> GRADE_9];

						$out = $this->processOrder($amOrder, $seller->id, $orderType, $portalSpec, $coupon);
						if ($out->status == 'OK' &&
							$seller->flags & SELLER_NEEDS_VERIFICATION_OF_BRE) {
							$msg = '';
							switch($in->query) {
								case 'purchase-portal-post-registration':
									$msg = "This seller: $seller->first_name $seller->last_name, with id: $seller->id bought a portal: $portal, ";
									break;
								case 'purchase-lifestyle-post-registration':
									$msg = "This seller: $seller->first_name $seller->last_name, with id: $seller->id bought a lifestyle: $tag, city_id:$city, ";
									break;
							}
							$msg.= "but was not from an email campaign nor was using an invite code.  No 'verify email' check was done.  ";
							$msg.= "Therefore, we need to verify the bre:$seller->bre ".(isset($bre) ? "from $bre->state " : '')."or at least check to make sure this was legit.";
							$this->getClass('Email')->sendMail(SUPPORT_EMAIL, 
																'Verify Agent',
																$msg,
																'Internal Task',
																'Verify agent authenticity');
						}
						break;

					case 'update-password':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['newOne'])) throw new \Exception('No data sent.');
						if (empty($in->data['oldOne'])) throw new \Exception('No data sent.');
						$newOne = $in->data['newOne'];
						$oldOne = $in->data['oldOne'];
						$user = wp_get_current_user();
						if ( ($user && !is_wp_error( $user )) ) 
							$out = $this->getClass('Register')->userResetPassword($user, $newOne, $oldOne);
						else
							$out = new Out ('fail', "Cannot get current logged in user");
						break;
					// set update-exclusion-permission
					case 'update-exclusion-permission':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['permission'])) throw new \Exception('permission not provided.');
						if (empty($in->data['id'])) throw new \Exception('id not provided.');
						$id = isset($in->data['id']) ? $in->data['id'] : 0;
						$permission = $in->data['permission'];
						$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
						if (!empty($seller)) {
							$seller = array_pop($seller);
							$current = $seller->flags & SELLER_INACTIVE_EXCLUSION_AREA;
							$seller->flags = is_true($permission) ? $seller->flags | SELLER_INACTIVE_EXCLUSION_AREA : $seller->flags & ~SELLER_INACTIVE_EXCLUSION_AREA;
							if ( (is_true($permission) &&
								  empty($current)) ||
								  (!is_true($permission) &&
								  !empty($current)) )
								$this->getClass('Sellers')->set([(object)['where'=>['id'=>$id],
																		  'fields'=>['flags'=>$seller->flags]]]);
							$out = new Out('OK', "Updated seller flag to ".$seller->flags);
						}
						else
							$out =- new Out('fail', "Could not find seller with id:$id");
						break;
					// reserve products for sellers
					case 'reserve-agent-product':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['what'])) throw new \Exception('Option not provided.');
						$id = isset($in->data['id']) ? $in->data['id'] : 0;
						$what = intval($in->data['what']);
						$portal = isset($in->data['portal']) ? str_replace(" ", "_", strtolower($in->data['portal'])) : '';
						$lifestyles = isset($in->data['lifestyles']) ? $in->data['lifestyles'] : ['item'=>[]];
						$seller = null;

						if ($id == 0) {
							if (empty($in->data['first'])) throw new \Exception('First name not provided.');
							if (empty($in->data['last'])) throw new \Exception('Last name not provided.');
							if (empty($in->data['phone'])) throw new \Exception('Phone not provided.');
							if (empty($in->data['email'])) throw new \Exception('Email not provided.');	

							$seller = $this->getClass('Sellers')->get((object)['like'=>['email'=>$in->data['email']]]);
							if (!empty($seller)) {
								$this->log("reserve-agent-product - found seller - id:".$seller[0]->id." from email:".$in->data['email']);
								$id = $seller[0]->id;
							}					
						}

						if ($what == SELLER_IS_PREMIUM_LEVEL_2 &&
							!empty($lifestyles['item']))
							$this->updateProfileFromReservation($lifestyles['item'], $id);

						$reservation = null;
						$needUpdate = false;
						$newReservation = false;

						// try the email
						// if (empty($id)) {
						// 	$seller = $this->getClass('Sellers')->get((object)['like'=>['email'=>$in->data['email']]]);
						// 	if (!empty($seller)) {
						// 		$this->log("reserve-agent-product - found seller - id:".$seller[0]->id." from email:".$in->data['email']);
						// 		$id = $seller[0]->id;
						// 	}
						// }

						if (!empty($id) ||
							!empty($seller)) { // then you'd expect a valid seller...
							if (empty($seller))
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
							if (!empty($seller)) {
								// NOTE:
								// use seller_id instead of email.  the email used in the reservation may not be the same as the one in the Sellers table!!
								if ($this->getClass('Reservations')->exists(['seller_id'=>$seller[0]->id])) {
									$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller[0]->id]])[0];
									// if ($reservation->seller_id != $id) {
									// 	$reservation->seller_id = $id;
									// 	$needUpdate = true;
									// }
									if ($reservation->first_name != $seller[0]->first_name) {
										$reservation->first_name = $seller[0]->first_name;
										$needUpdate = true;
									}
									if ($reservation->last_name != $seller[0]->last_name) {
										$reservation->last_name = $seller[0]->last_name;
										$needUpdate = true;
									}
									if ($reservation->phone != $seller[0]->phone) {
										$reservation->phone = $seller[0]->phone;
										$needUpdate = true;
									}
								}
								// oK, try the email...
								elseif ($this->getClass('Reservations')->exists(['email'=>$seller[0]->email])) {
									$reservation = $this->getClass('Reservations')->get((object)['where'=>['email'=>$seller[0]->email]])[0];
									if ($reservation->seller_id != $id) {
										$reservation->seller_id = $id;
										$needUpdate = true;
									}
									if ($reservation->first_name != $seller[0]->first_name) {
										$reservation->first_name = $seller[0]->first_name;
										$needUpdate = true;
									}
									if ($reservation->last_name != $seller[0]->last_name) {
										$reservation->last_name = $seller[0]->last_name;
										$needUpdate = true;
									}
									if ($reservation->phone != $seller[0]->phone) {
										$reservation->phone = $seller[0]->phone;
										$needUpdate = true;
									}
								}
							}
						}
						else
							$seller = null;

						$zohoData = null;

						if (!empty($seller)) {
							$seller = array_pop($seller);
							if ( !isset($seller->phone) || empty($seller->phone)) $seller->phone = $in->data['phone'];
							if ($reservation == null)
								$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$id]])[0];
							foreach($seller->meta as $meta) {
								if ($meta->action == SELLER_ZOHO_LEAD_ID) {
									$zohoData = $meta;
								}
								if ($zohoData) break;
								unset($meta);
							}
						}
						else if ($reservation == null) {
							$seller = new \stdClass();
							$seller->id = $id;
							$seller->first_name = $in->data['first'];
							$seller->last_name = $in->data['last'];
							$seller->email = $in->data['email'];
							$seller->phone = $in->data['phone'];
							if ($this->getClass('Reservations')->exists(['email'=>$seller->email]))
								$reservation = $this->getClass('Reservations')->get((object)['where'=>['email'=>$seller->email]])[0];
						}

						$x = false;
						$reservation_id = 0;
						if (empty($reservation)) {
							$newReservation = true;
							$reservation = new \stdClass();
							$reservation->first_name = $seller->first_name;
							$reservation->last_name = $seller->last_name;
							$reservation->phone = $seller->phone;
							$reservation->email = $seller->email;
							$reservation->seller_id = $seller->id;
							$reservation->type = $what;
							if ($zohoData)
								$reservation->lead_id = $zohoData->lead_id;
							if (count($lifestyles['item'])) {
								$reservation->type |= SELLER_IS_PREMIUM_LEVEL_2;
								$reservation->meta = [$lifestyles];
							}
							if ( !empty($portal) ) {
								$reservation->type |= SELLER_IS_PREMIUM_LEVEL_1;
								$reservation->portal = $portal;
							}
							$x = $this->getClass('Reservations')->add($reservation);
							$reservation_id = $x;
							$reservation->added = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
							$reservation->id = $x;
						}
						else {
							$needUpdate = false;
							$fields = [];
							if ( !empty($portal) &&
								 $reservation->portal != $portal ) {
								$reservation->portal = $portal;
								if ( ($reservation->type & SELLER_IS_PREMIUM_LEVEL_1) == 0 ) {
									$reservation->type |= SELLER_IS_PREMIUM_LEVEL_1;
									$fields['type'] = $reservation->type;
								}
								$fields['portal'] = $portal;
								$needUpdate = true;
							} 
							// if ( ($reservation->type & $what) == 0 ) {
							// 	$reservation->type |= $what;
							// 	$needUpdate = true;
							// }

							if ( empty($reservation->meta) ) {
								if (count($lifestyles)) {
									$reservation->meta = [$lifestyles];
									$needUpdate = true;
									$reservation->type |= SELLER_IS_PREMIUM_LEVEL_2;
									$fields['type'] = $reservation->type;
									$fields['meta'] = $reservation->meta;
								}
							}
							else if ( count($lifestyles['item']) ) {
								$metas = [];
								$gotOne = 0;
								$updateMeta = false;
								$haveDup = false;
								foreach($reservation->meta as $data) {
									if ($data->action != ORDER_AGENT_MATCH)
										$metas[] = $data;
									else {
										if (!empty($data->item)) 
											foreach($data->item as $existing) {
												$break = false;
												foreach($lifestyles['item'] as $incoming) {
													$incoming = (object)$incoming;
													if ($incoming->location == $existing->location &&
														$incoming->specialty == $existing->specialty) {
														$gotOne++;
														$break = true;
													}
													unset($incoming);
													if ($break) break;
												}
												unset($existing);
											}

										if ($gotOne == count($lifestyles['item']) &&
											$gotOne == count($data->item)) {// then they all matched
											$haveDup = true;
											$metas[] = $data;
										}
									}	
									unset($data);
								}
								if (!$haveDup) {			
									$metas[] = $lifestyles; // append the new one, since it's different
									$needUpdate = true;
									$updateMeta = true;																	
								}

								$reservation->meta = $metas;
								if ($updateMeta) {
									if ( ($reservation->type & SELLER_IS_PREMIUM_LEVEL_2) == 0 ) {
										$reservation->type |= SELLER_IS_PREMIUM_LEVEL_2;
										$fields['type'] = $reservation->type;
									}
									$fields['meta'] = $reservation->meta;
								}
							}

							if ($id &&
								$reservation->seller_id != $id) {
								$fields['seller_id'] = $id;
								$needUpdate = true;
							}

							if ($zohoData &&
								$reservation->lead_id != $zohoData->lead_id)
								$fields['lead_id'] = $zohoData->lead_id;

							$reservation_id = $reservation->id;
							if ($needUpdate)
								$x = $this->getClass('Reservations')->set([(object)['where'=>['id'=>$reservation->id],
																					'fields'=>$fields]]);
							else
								$x = 1;
							unset($fields);
						}

						$fromEmailCampaign = false;
						if (!empty($id)) { // then you'd expect a valid seller...
							$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$id]]);
							if (!empty($emailDb)) {
								$fields = [];
								$update = false;
								if ( ($emailDb[0]->flags & RESERVATION_MADE) == 0 ) {
									$fields['flags'] = $emailDb[0]->flags | RESERVATION_MADE;
									$fromEmailCampaign = true;
								}

								if ( ($what & SELLER_IS_PREMIUM_LEVEL_1) && !empty($portal) ) {
									$meta = new \stdClass();
									$meta->action = RESERVED_PORTAL;
									$meta->date = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
									$meta->reservation_id = $reservation_id;
									$meta->portal = $portal;
									if ( empty($emailDb[0]->meta) )
										$emailDb[0]->meta = [$meta];
									else {
										$emailDb[0]->meta = (array)$emailDb[0]->meta;
										$emailDb[0]->meta[] = $meta;
									}
									$update = true;
								}
								if ( ($what & SELLER_IS_PREMIUM_LEVEL_2) && count($lifestyles['item']) ) {
									$meta = new \stdClass();
									$meta->action = RESERVED_AGENT_MATCH;
									$meta->date = date('Y-m-d H:i:s', time() + ($this->timezone_adjust*3600));
									$meta->reservation_id = $reservation_id;
									$meta->lifestyles = $lifestyles['item'];
									if ( empty($emailDb[0]->meta) )
										$emailDb[0]->meta = [$meta];
									else {
										$emailDb[0]->meta = (array)$emailDb[0]->meta;
										$emailDb[0]->meta[] = $meta;
									}
									$update = true;
								}
								if ($update)
									$fields['meta'] = $emailDb[0]->meta;
								if (count($fields))
									$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb[0]->id],
																					 'fields'=>$fields]]);
							}
							else
								$this->log("reserve-agent-product - got id:$id, but no associated SellersEmailDb entry");
						}

						$zoho = $this->getClass('Zoho')->processOneZohoCrm($newReservation, $reservation, $seller);
						$this->log("reserve-agent-product - zoho result:".($zoho ? 'success' : 'failure').", newReservation:$newReservation, reservation_id:$reservation->id, seller_id:".(empty($seller) ? "0" : $seller->id));

						if (!empty($x)) {
							$this->log("seller id: $seller->id");
							$product = $reservation->type & SELLER_IS_PREMIUM_LEVEL_1 && !empty($portal) ? "portal($portal)" : '';
							$product .= $reservation->type & SELLER_IS_PREMIUM_LEVEL_2 && count($lifestyles['item']) ? (empty($product) ? 'Lifestyled Agent' : ' and Lifestyled Agent') : '';
							$content = file_get_contents( !empty($seller->author_id) ? __DIR__.'/../_mail/_content/agent_reservation_confirmation_registered.html.trim' :
																				__DIR__.'/../_mail/_content/agent_reservation_confirmation_unregistered.html.trim');
							$content = trim($content);
							$content = str_replace('%first_name%', $seller->first_name, $content);
							$content = str_replace("%last_name%", $seller->last_name, $content);
							$content = str_replace("%product%", $product, $content);
							$style = 'style="display: %display%;"';
							$style = str_replace('%display%', (isset($lifestyles['item']) && count($lifestyles['item']) ? 'table-row' : 'none!important'), $style);
							$content = str_replace("%display_lifestyle%", $style, $content);
							$this->log("count lifestyles:".(isset($lifestyles['item']) ? count($lifestyles['item']) : "not set").", style:$style");
							if ( isset($lifestyles['item']) &&
								 count($lifestyles['item']) ) {
								$specialties = '';
								foreach($lifestyles['item'] as $index=>$incoming) {
									$incoming = (object)$incoming;
									if ($index == '0') {
										$content = str_replace("%location%", $incoming->locationStr, $content);
										$this->log("location: $incoming->locationStr");
									}
									$specialties .= '<br/>-'.$incoming->specialtyStr;
									unset($incoming);
								}
								$this->log("specialties: $specialties");
								$content = str_replace("%life_styles%", $specialties, $content);
							}

							$url = get_home_url().(!empty($seller->id) ? '/sellers/'.$seller->id : '/register');
							$content = str_replace("%url%", $url, $content);
							$this->log("content: $content");
							$this->getClass('Register')->sendMail($seller->email, 
																	"Reservation confirmation",
																	$content,
																	"Confirmation",
																	"You shall be contacted soon.");
							$msg = "Seller id: {$seller->id}, {$seller->first_name} {$seller->last_name}, phone:{$seller->phone}, email:{$seller->email}, reserved $product.";
							if (count($lifestyles['item'])) {
								foreach($lifestyles['item'] as $item) {
									$item = (object)$item;
									$msg .= "\nLocation: {$item->locationStr}, Specialty:{$item->specialtyStr}";
									unset($item);
								}
							}
							$this->log("msg: $msg");
							$this->getClass('Register')->sendMail( SUPPORT_EMAIL, 
																	"Agent product reservation".($fromEmailCampaign ? " from email campaign" : ''), 
																	$msg, 
								  	  								'Reservation confirmation',
								  									date("Y-m-d H:i:s", time()) );
						}
						$msg = empty($x) ? "Failed to save the reservation" : "Reservation for $what is completed, confirmation sent to $seller->email.";
						if (!empty($x) &&
							!empty($id) &&
							!empty($seller) &&
							strpos( strtolower($seller->email), "aol" ) !== false )
							$msg .= "  AOL users, please put ".SUPPORT_EMAIL." to your whitelist, otherwise AOL may bounce your confirmation email.";
						$out = new Out(empty($x) ? 'fail' : 'OK', $msg);
						break;
					// called from seller.page.agent_match
					case 'verify-invitecode':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['code'])) throw new \Exception('Code not provided.');
						if (empty($in->data['option'])) throw new \Exception('Option not provided.');
						$Register = $this->getClass('Register');
						$out = $Register->verifyInvite($in->data['code'], $in->data['option']);
						if ($out->status != 'OK') {
							$args = array(
							    'posts_per_page'   => -1,
							    'orderby'          => 'title',
							    'order'            => 'asc',
							    'post_type'        => 'shop_coupon',
							    'post_status'      => 'publish',
							);
							    
							$coupons = get_posts( $args );
							if (!empty($coupons)) foreach($coupons as $code) {
								if ($code->post_title == strtolower($in->data['code'])) {
									$expiryDate = get_post_meta($code->ID, 'expiry_date', true);
									if (strtotime($expiryDate) < time()) {
										$out = new Out('fail', "Coupon is expired.");
										$this->log("verify-invitecode - called with ecommerce coupon: $code->post_title, expired on $expiryDate");
									}
									else {
										$this->log("verify-invitecode - called with ecommerce coupon: $code->post_title");
										$out = new Out('OK', "Coupon is valid until $expiryDate");
									}
								}
								unset($code);
							}
						}
						break;

					case 'process-agent-match-invite':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['order'])) throw new \Exception('No order sent.');
						if (empty($in->data['code'])) throw new \Exception('Code not provided.');
						if (empty($in->data['id'])) throw new \Exception('Id not provided.');

						$amOrder = (object)$in->data['order'];
						$this->log('process-agent-match-invite - order:'.print_r($amOrder, true));
						$id = $in->data['id'];
						$r = $this->getClass('Register');
						$ic = null;
						$out = $r->verifyInvite($in->data['code'], IC_FEATURE_LIFESTYLE, $ic);
						if ($out->status != 'OK') {
							$args = array(
							    'posts_per_page'   => -1,
							    'orderby'          => 'title',
							    'order'            => 'asc',
							    'post_type'        => 'shop_coupon',
							    'post_status'      => 'publish',
							);
							    
							$coupons = get_posts( $args );
							if (!empty($coupons)) foreach($coupons as $code) {
								if ($code->post_title == strtolower($in->data['code'])) {
									$expiryDate = get_post_meta($code->ID, 'expiry_date', true);
									if (strtotime($expiryDate) < time()) {
										$out = new Out('fail', "Coupon is expired.");
										$this->log("process-agent-match-invite - called with ecommerce coupon: $code->post_title, expired on $expiryDate");
									}
									else {
										if (isset($in->data['portalSpec'])) 
											$portalSpec = (object)$in->data['portalSpec'];
										else
											$portalSpec = (object)['subscriptionType'=> 0,
																   'priceLevel'=> GRADE_9];
										$this->log("process-agent-match-invite - called with ecommerce coupon: $code->post_title");
										$out = $this->processOrder($amOrder, $id, ORDER_AGENT_MATCH, $portalSpec, $code->post_title);
									}
								}
								unset($code);
							}
							break;
						}

						$Sellers = $this->getClass('Sellers');
						$seller = $Sellers->get((object)['where'=>['id'=>$id]]);
						if (empty($seller)) {
							$out = new Out('fail', 'Could not find seller with sellerId:'.$id);
							break;
						}

						$out = $this->processAMInvite($amOrder, $seller[0], $ic, $r);
						if ($out->status == 'OK') {
							if (isset($out->data['seller'])) {
								$havePortal = $out->data['seller']->flags & SELLER_IS_PREMIUM_LEVEL_1;
								$xmlFile = !$havePortal ? 'haveLifestyle.xml' : 'haveAll.xml';
								$xmlFile = __DIR__."/_templates/$xmlFile";
								$out->data['dashboardCards'] = readDashCardFile($xmlFile);
							}
						}
						break;
					case 'update-agent-match-order':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['order'])) throw new \Exception('No order sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');
						$amOrder = (object)$in->data['order'];
						$id = $in->data['id'];
						$Sellers = $this->getClass('Sellers');
						$seller = $Sellers->get((object)['where'=>['id'=>$id]]);
						if (empty($seller)) {
							$out = new Out('fail', ['status'=>'Could not find seller with sellerId:'.$id]);
							break;
						}
						$out = $this->updateProfileFromOrder($amOrder, $seller[0]->id);
						break;
					case 'process-agent-order':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['order'])) throw new \Exception('No order sent.');
						if (empty($in->data['product'])) throw new \Exception('No product sent.');
						if (empty($in->data['id'])) throw new \Exception('No id sent.');

						$amOrder = (object)$in->data['order'];
						$id = $in->data['id'];
						$productType = intval($in->data['product']);
						if (isset($in->data['portalSpec'])) 
							$portalSpec = (object)$in->data['portalSpec'];
						else
							$portalSpec = (object)['subscriptionType'=> 0,
												   'priceLevel'=> GRADE_9];

						$coupon =  isset($in->data['coupon']) ? $in->data['coupon'] : null;

						$out = $this->processOrder($amOrder, $id, $productType, $portalSpec, $coupon);
						break;
					// called from seller:portal:completed page
					case 'test-email':
						if (empty($in->data)) throw new \Exception('No data sent.');
						if (empty($in->data['author_id'])) throw new \Exception('Author ID not provided.');
						$email = $this->getClass('Email');
						$out = $email->sendTestEmail($in->data['author_id']);
						break;
					// called from seller::profile for agent matching tags
					case 'get-agent-match-tags':
						$Tags = $this->getClass('Tags');
						global  $agent_match_specialties;
						global  $agent_match_lifestyles;
						$specialties = $Tags->get((object)['where'=>['id'=>$agent_match_specialties]]);
						$lifestyles = $Tags->get((object)['where'=>['id'=>$agent_match_lifestyles]]);
						$out = new Out('OK', [$specialties, $lifestyles]);
						break;
					// called from listings to see if this listing has any city tags it needs to show
					case 'get-city-tags':
						if (empty($in->data)) throw new \Exception('No data sent.');
						$city_tags = $this->getClass('CitiesTags')->get((object)[ 'where' => ['city_id' => $in->data]]);
						$out = new Out(!empty($city_tags) ? 1 : 0, $city_tags);
						break;
					// called from seller::portal, check if valid portal name
					case 'check-portal-name':
						if (empty($in->data)) throw new \Exception('No data sent.');
						$portal = filter_var($in->data, FILTER_SANITIZE_URL);
						$reg = $this->getClass('SessionMgr');
						$out = $reg->checkPortalName($portal);
						break;
					case 'get-portal-name':
						if (empty($in->data)) throw new \Exception('No data sent.');
						$portal = filter_var($in->data['portal'], FILTER_SANITIZE_URL);
						$coupon = $in->data['coupon'];
						$id = isset($in->data['id']) ? $in->data['id'] : 0;
						$amOrder = isset($in->data['order']) ? (object)$in->data['order'] : null;
						$this->log("get-portal-name - portal:$portal, coupon:$coupon, id:$id, amOrder:".($amOrder ? print_r($amOrder, true) : 'N/A'));

						$SessionMgr = $this->getClass('SessionMgr');
						$out = $SessionMgr->getPortalName($portal, $coupon, $id);
						if ($out->status == 'OK' &&
							$coupon != '0') {
							if ($this->getClass('Reservations')->exists(['seller_id'=>$id])) {
								$res = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$id]]);
								$this->getClass('Reservations')->set([(object)[	'where'=>['seller_id'=>$id],
															 					'fields'=>['flags'=>$res[0]->flags | RESERVATION_CONVERTED_PORTAL_CODE]]]);
							}
							else {
								$seller = $this->getClass('Sellers')->get((object)['where'=>['id'=>$id]]);
								if (!empty($seller)) {
									$seller = array_pop($seller);
									$newRes = ['seller_id'=>$seller->id,
											   'first_name'=>$seller->first_name,
											   'last_name'=>$seller->last_name,
											   'email'=>$seller->email,
											   'phone'=>(!empty($seller->mobile) ? $seller->mobile : $seller->phone),
											   'type'=>SELLER_IS_PREMIUM_LEVEL_1,
											   'flags'=>RESERVATION_CONVERTED_PORTAL_CODE];
									$this->getClass('Reservations')->add($newRes);
								}
							}

							if (isset($out->data['seller'])) {
								$haveLifestyle = $out->data['seller']->flags & SELLER_IS_PREMIUM_LEVEL_2;
								$xmlFile = !$haveLifestyle ? 'havePortal.xml' : 'haveAll.xml';
								$xmlFile = __DIR__."/_templates/$xmlFile";
								$out->data['dashboardCards'] = readDashCardFile($xmlFile);
							}
						}
						else {
							$args = array(
							    'posts_per_page'   => -1,
							    'orderby'          => 'title',
							    'order'            => 'asc',
							    'post_type'        => 'shop_coupon',
							    'post_status'      => 'publish',
							);
							    
							$coupons = get_posts( $args );
							if (!empty($coupons)) foreach($coupons as $code) {
								if ($code->post_title == strtolower($in->data['coupon']) ){
									$expiryDate = get_post_meta($code->ID, 'expiry_date', true);
									if (strtotime($expiryDate) < time()) {
										$out = new Out('fail', "Coupon is expired.");
										$this->log("get-portal-name - called with ecommerce coupon: $code->post_title, expired on $expiryDate");
									}
									else {
										if (isset($in->data['order'])) {
											$amOrder = (object)$in->data['order'];
											if (isset($in->data['portalSpec'])) 
												$portalSpec = (object)$in->data['portalSpec'];
											else
												$portalSpec = (object)['subscriptionType'=> 0,
																	   'priceLevel'=> GRADE_9];
											$this->log("get-portal-name - called with ecommerce coupon: $code->post_title");
											$found = false;
											if (!empty($amOrder->item)) foreach($amOrder->item as $i=>&$item) {
												$item = (object)$item;
												if (intval($item->type) == ORDER_PORTAL) {
													$found = true;
												}
												else
													$this->log("get-portal-name - item:$i has type $item->type, ORDER_PORTAL:".ORDER_PORTAL);
												unset($item);
												if ($found) break;
											}
											if (!$found) {
												// $out = new Out('fail', "Failed to find ORDER_PORTAL");
												// break;

												$portalOrder = new \stdClass();
												$portalOrder->type = ORDER_PORTAL;
												$portalOrder->mode = ORDER_BUYING;
												$portalOrder->order_id = 0;
												$portalOrder->cart_item_key = 0;
												$portalOrder->priceLevel = $portalSpec->priceLevel;
												$portalOrder->subscriptionType = $portalSpec->subscriptionType;
												$portalOrder->nickname = $portal;
												$amOrder->item[] = $portalOrder;
											}
											$out = $this->processOrder($amOrder, $id, ORDER_PORTAL, $portalSpec, $coupon);
											
										}
										else
											$out = new Out('fail', "No order passed to getting portal");
									}
								}
								unset($code);
							}
						}
						break;
				/* Listings */
					case 'get-listings-by-author':
						global $current_user;
      					get_currentuserinfo();
      					$havePortal = (	$current_user->user_nicename != 'unknown' &&
										$current_user->nickname != 'unknown');
						$listings = $this->getClass('Listings')->get((object)array( 'where'=>['author'=>$in->data,
																							  'author_has_account'=>1] ));
						if (!empty($listings)) foreach($listings as &$l) {
							$ListingsTags = $this->getClass('ListingsTags')->getGroup((object)[ 'field'=>'tag_id', 'where'=>[ 'listing_id'=>$l->id ] ]);
							$l->tags = $this->getClass('Tags')->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$ListingsTags ] ]);
							$l->havePortal = $havePortal;
							if (!empty($l->images)) foreach($l->images as &$img) {
								if (isset($img->file)) $img->file = removeslashes($img->file);
								if (isset($img->url)) $img->url = removeslashes($img->url);
								unset($img);
							}
							unset($l);
						}					

						$this->log("get-listings-by-author found ".count($listings)." for author:$in->data");
						$author = $this->getAuthor($in->data);
						$out = new Out('OK', (object)['listings'=>$listings,
													  'author'=>$author]);
						break;
					case 'get-listing':
						if (empty($in->data['id'])) throw new \Exception('Listing ID not provided.');

						$class = $this->getClass('Listings');

						$sql = $class->rawQuery("SELECT a.*, b.lat, b.lng FROM {$class->getTableName('listings')} as a LEFT JOIN {$class->getTableName('listings-geoinfo')} as b ON a.id = b.listing_id WHERE a.id = {$class->prepare('%d', $in->data['id'])}");
						$tags = $this->getClass('ListingsTags')->get((object)[ 'where' => ['listing_id' => $in->data['id']] ]);

						if ( empty($sql) )
							throw new \Exception("Unable to get listing from database.");

						$sql = $sql[0];
						// parse
						foreach ($sql as $key => &$value){
							if ( in_array( $key, [ 'active', 'author', 'author_has_account', 'beds', 'city_id', 'error', 'flags', 'id', 'interior', 'key_match', 'price' ] ) )
								$value = intval($value);
							elseif ( in_array( $key, [ 'baths', 'lat', 'lng', 'interior', 'lotsize' ] ) )
								$value = floatval($value);
							elseif ( ($key == 'images' || $key == 'meta' || $key == 'video') && !empty($value) ){
								$value = json_decode($value);
								if ($key == 'images' &&
									!empty($value)) foreach($value as &$img) {
										if (isset($img->file)) $img->file = removeslashes($img->file);
										if (isset($img->url)) $img->url = removeslashes($img->url);
										if (isset($img->title)) $img->title = removeslashes($img->title);
										if (isset($img->desc)) $img->desc = removeslashes($img->desc);
										unset($img);
								}
							}
							unset($value);
						}
						$sql->about = removeslashes($sql->about);
						$sql->tags = [];
						if (!empty($tags))
							foreach($tags as $ltag) {
								$sql->tags[] = $ltag->tag_id;
								unset($ltag);
							}

						$out = new Out('OK', $sql );
						break;
					case 'set-listing':
						if (empty($in->data['id']))
							throw new \Exception('Listing ID not provided.');

						if (empty($in->data['fields']))
							throw new \Exception('Fields not provided.');

						$new_activity = (object)[
							'action' => 'update',
							'fields' => $in->data['fields'],
							'date' => date("Y-m-d H:i:s", time()),
							'author' => intval($in->data['author']),
							'source' => 'user',
						];

						$activity = $this->getClass('ListingsActivity')->get((object)[ 'what' => ['data'], 'where' => [ 'listing_id' => $in->data['id'] ] ]);

						if (empty($activity)){
							$activity = (object)[
								'listing_id' => $in->data['id'],
								'author' => $in->data['author'],
								'data' => json_encode( [$new_activity] )
							];
							$this->getClass('ListingsActivity')->add($activity);
						} else {
							$activity = array_pop($activity);
							$activity->data = empty($activity->data) ? [$new_activity] : $activity->data;

							if (is_object( $activity->data ) && isset($activity->data->Query) && substr($activity->data->Query, 0, 22) == "Check for existance on" )
								$activity->data = [(object)[
									'action' => 'existance check',
									'date' => substr($activity->data->Query, 23),
									'author' => -1,
									'source' => 'script',
								], $new_activity];
							else if (is_array($activity->data))
								$activity->data[] = $new_activity;

							// $activity->data = json_encode($activity->data);
							$this->getClass('ListingsActivity')->set([(object)[ 'where' => [ 'listing_id' => $in->data['id'] ], 'fields' => $activity ]]);
						}

						$fields_listings = [];
						$fields_geoinfo = [];
						foreach ($in->data['fields'] as $field => $value) {
							if ($field !== 'lat' && $field !== 'lng' && $field !== 'address' && $field !== 'tags') {
								$fields_listings[$field] = $value;
							}
							else if ($field !== 'tags')
								$fields_geoinfo[$field] = $value;
							unset($value);
						}

						$res = (object)[
							'listings' => -1,
							'geoinfo' => -1,
							'tags' => -1,
						];

						// do geoInfo first, as it is totally independent of other data
						if (!empty($fields_geoinfo)){
							if ( !$this->getClass('ListingsGeoinfo')->exists([ 'listing_id' => $in->data['id'] ]) ){
								$fields_geoinfo['listing_id'] = $in->data['id'];
								$res->geoinfo = $this->getClass('ListingsGeoinfo')->add($fields_geoinfo);
							} else {
								$geo = $this->getClass('ListingsGeoinfo')->get((object)['where'=>[ 'listing_id' => $in->data['id'] ]]);
								if ($geo[0]->lng != $fields_geoinfo['lng'] ||
									$geo[0]->lat != $fields_geoinfo['lat'] ||
									$geo[0]->address != $fields_geoinfo['address'] )
								$res->geoinfo = $this->getClass('ListingsGeoinfo')->set([ (object)[ 'where' => [ 'listing_id' => $in->data['id'] ], 'fields' => $fields_geoinfo ]]);
							}
						}


						$aListing = null;
						$aListing = $this->getClass('Listings')->get((object)['where' => [ 'id' => $in->data['id']]]);
						$metas = [];
						$modifiedDataMeta = null;
						$aListing = array_pop($aListing);
						if (!empty($aListing->meta)) foreach($aListing->meta as $info) {
							if ($info->action == LISTING_MODIFIED_DATA) {
								$modifiedDataMeta = $info;
							}
							else
								$metas[] = $info;
							unset($info);
						}

						if (!$modifiedDataMeta) { $modifiedDataMeta = new \stdClass(); $modifiedDataMeta->action = LISTING_MODIFIED_DATA; }

						$diff_fields = [];

						// do direct listing data
						if (!empty($fields_listings)) {
							if (empty($aListing->city_id)) {
								$city_id = $this->getCityId($aListing);
								if (!empty($city_id)) {
									// $aListing->city_id = $city_id;
									$modifiedDataMeta->city_id = 1;
									$this->log("set-listing - setting city_id to $city_id");
									$fields_listings['city_id'] = $city_id;
								}
							}
							
							foreach ($fields_listings as $field => $value) {
								// deal with no more image case first
								if ($field == 'images' &&
									empty($value)) {
									if (!empty($aListing->images))
										$diff_fields[$field] = null;
									$modifiedDataMeta->$field = 0;
									// make sure the images are cleaned out!
									$this->getClass('Image')->unlinkListingImages($aListing->images);
									$aListing->$field = null;
								}
								else {									
									if ($field == 'images') { // then has images
										$value = (array)$value;
										$this->log("set-listing - image array: count:".count($value)." - ".print_r($value, true));
										$hasContent = [];
										// 
										if (!empty($value)) foreach($value as $i=>&$img) {
											// $this->log("set-listing - testing image $i:".$img['file']);
											$img = (object)$img;
											if (!isset($img->file) ||
												empty($img->file)) {
												$this->log("set-listing - removing image index:$i, appears empty");
												// unset($value[$i]); // get rid of any flaky empty data
											}
											else
												$hasContent[] = $img;
											unset($img);
											// $img->file = addslashes($img->file);
										}

										unset($value);
										$value = $hasContent;
										if (empty($value)) { // uh oh, everything's empty!
											$this->log("set-listing finds incoming image array had nothing valid, emptying images");
											if (!empty($aListing->images))
												$diff_fields[$field] = 'null';
											$modifiedDataMeta->$field = 0;
											// make sure the images are cleaned out!
											$this->getClass('Image')->unlinkListingImages($aListing->images);
											$aListing->$field = null;
										}
										elseif (count($value) >= count($aListing->images)) { // then just update the listing images
											$diff_fields[$field] = $value;
											$aListing->$field = $value;
											$modifiedDataMeta->$field = 1;
										}
										else { // must have deleted an image
											$existingKeys = [];
											$incomingKeys = [];
											foreach($aListing->images as $img) {
												$existingKeys[$img->file] = $img;
												unset($img);
											}
											foreach($value as $img) {
												$img = (object)$img;
												// $img->file = preg_replace('/[^a-zA-Z0-9_.]/', '', $img->file); // clean it
												$incomingKeys[$img->file] = $img;
												unset($img);
											}

											$diff = array_diff_key($existingKeys, $incomingKeys);
											$diff_fields[$field] = $value;
											if (count($diff)) {
												foreach($diff as $key=>$img) {
													if (substr($img->file, 0, 4 ) != 'http') {
														$this->log("set-listing - removing file:$img->file");
														$this->getClass('Image')->unlinkListingImage($img); // removing files
													}
													unset($img);
												}
											}
											$aListing->$field = $value;
											$modifiedDataMeta->$field = 1;
											unset($diff, $existingKeys, $incomingKeys);
										}
									}
									else if ($aListing->$field != $value) {
										// now update local copy
										$aListing->$field = $value;
										$diff_fields[$field] = $value;
										$modifiedDataMeta->$field = 1;
									}
									else
										$this->log("set-listing - unprocessed field:$field, value:$value, curent listing value: {$aListing->$field}");
								}
								unset($value);
							}
							// $this->log("set-listing - diff_fields:".print_r($diff_fields, true).", fields_listings:".print_r($fields_listings, true));
						}


						if (isset($in->data['fields']['tags']) && 
							!empty($in->data['fields']['tags'])) {
							$in_tags = [];
							if ($in->data['fields']['tags'] === 'null')
								$in->data['fields']['tags'] = [];
							foreach ($in->data['fields']['tags'] as $in_tag) {
								$in_tags[] = intval($in_tag);
								unset($in_tag);
							}
							$in_tag = null;
							$db_tags = $this->getClass('ListingsTags')->get((object)[ 'what' => ['tag_id'], 'where' => [ 'listing_id' => $in->data['id'] ] ]);

							if (empty($db_tags))
								$db_tags = [];

							$dbt = [];
							$toAdd = [];
							$toRemove = [];
							$res->tags = -1;
							if (!empty($db_tags))
								foreach ($db_tags as $tag_row){
									$dbt[] = intval($tag_row->tag_id);
									if (!in_array( intval($tag_row->tag_id), $in_tags ))
										$toRemove[] = intval($tag_row->tag_id);
									unset($tag_row);
								}

							foreach ($in->data['fields']['tags'] as $in_tag){
								if ( !in_array( intval($in_tag), $dbt ) )
									$toAdd[] = intval($in_tag);
								unset($in_tag);
							}

							if ($res->tags === -1 && !empty( $toAdd ))
								foreach ($toAdd as $id_to_add){
									if ($res->tags !== -1)
										break;
									$res->tags = -2;
									if ( $this->getClass('ListingsTags')->add((object)[ 'tag_id' => $id_to_add, 
																						'listing_id' => $in->data['id'],
																						'clicks' => 0,
																						'flags' => LISTING_TAG_ASSIGNED_BY_AUTHOR ]) )
										$res->tags = -1;
									else
										$res->tags = 0;
									unset($id_to_add);
								}

							if ($res->tags === -1 && !empty( $toRemove ))
								foreach ($toRemove as $id_to_del){
									if ($res->tags !== -1)
										break;
									$res->tags = -2;
									if ( $this->getClass('ListingsTags')->delete((object)[ 'tag_id' => $id_to_del, 'listing_id' => $in->data['id'] ]) )
										$res->tags = -1;
									else
										$res->tags = 0;
									unset($id_to_del);
								}

							if ($res->tags === -1)
								$res->tags = new Out('OK');

							if (count($in_tags)) {
								global $all_tag_city;
								foreach($in_tags as $i=>$tag)
									if (in_array($tag, $all_tag_city))
										unset($in_tags[$i]);

								if ( count($in_tags) >= MINIMUM_TAG_COUNT ) {
									$aListing->error = intval($aListing->error);
									if ( ($aListing->error & LOWTAGS) ) {
										$aListing->error = $diff_fields['error'] = $aListing->error & ~LOWTAGS;
										$modifiedDataMeta->error = 1;
									}

									// ok, we have enough tags now, so check if about has at least some description it, like 80 chars?
									if ( isset($aListing->about) &&
										!empty($aListing->about) &&
										 strlen( removeslashes($aListing->about) ) >= 80 &&
										 ($aListing->error & LACK_DESC) ) {
										$aListing->error = $diff_fields['error'] = $aListing->error & ~LACK_DESC;
										$modifiedDataMeta->error = 1;
									}
								}
							}
						}

						try {
							if (isset($aListing->street_address) && 
								isset($aListing->city) &&
								isset($aListing->state) &&
								isset($aListing->price) &&
								isset($aListing->beds) &&
								isset($aListing->baths) &&
								isset($aListing->images) &&
								count($aListing->images) &&
								!($aListing->error & (LOWTAGS | LACK_DESC)) ) {
								$this->checkSellerEmailDb($aListing->author, AH_ACTIVE);
								if ($aListing->active != AH_ACTIVE)
									$diff_fields['active'] = AH_ACTIVE;
							}
							else {
								$this->checkSellerEmailDb($aListing->author, AH_WAITING);
								if ($aListing->active != AH_WAITING)
									$diff_fields['active'] = AH_WAITING;
							}
						}
						catch( \Exception $e) {
							return parseException($e);
						}

						if (count($diff_fields)) {
							$this->log("set-listing - fields:".print_r($diff_fields, true));
							$metas[] = $modifiedDataMeta;
							$diff_fields['meta'] = $metas;
							$res->listings = $this->getClass('Listings')->set([ (object)[ 'where' => [ 'id' => $in->data['id'] ], 'fields' => $diff_fields ] ]);
						}

						$status = 'OK';
						foreach ($res as $rrow) {
							if ($rrow !== -1 && !$rrow){
								$status = 0;
							}
							unset($rrow);
							if (!$status) break;
						}
						
						// now check to see if the listing needs to have it's tags merged with the city tags
						// this is for organically added listings
						$sql =  'SELECT b.id, b.type FROM '.$this->getClass('ListingsTags')->getTableName().' AS a ';
						$sql .= 'INNER JOIN '.$this->getClass('Tags')->getTableName().' AS b ON a.tag_id = b.id ';
						$sql .= 'WHERE a.listing_id = '.$in->data['id'] .' AND b.type = 1';
						$x = $this->getClass('ListingsTags')->rawQuery($sql);
						if (empty($x) && $aListing) {// hey, no city tags for this listing!							
							if (!empty($aListing->city_id)) {
								// get all tags for this city
								$city_tags = $this->getClass('CitiesTags')->get((object)[ 'where' => ['city_id' => $aListing->city_id]]);
								if (!empty($city_tags)) {
									// add these to the listings tags
									foreach($city_tags as $tag) {
										$this->getClass('ListingsTags')->add((object)[ 'tag_id' => $tag->tag_id, 
																					   'listing_id' => $in->data['id'],
																					   'clicks' => 0,
																					   'flags' => LISTING_TAG_AUTO_ASSIGNED ]);
										unset($tag);
									}
								}
							}
						}

						$out = new Out($status);
						break;
					case 'new-listing':
						if (empty($in->data)) throw new \Exception('no data provided.');
						if (!isset($in->data['author'])) throw new \Exception('author ID not provided.');
						$in->data['flags'] = LISTING_PERMIT_ADDRESS;
						$in->data['active'] = 1;
						$in->data['author_has_account'] = 1;
						$x = $this->getClass('Listings')->add($in->data);
						$out = new Out($x !== false ? 'OK' : 0, $x);
						break;
					case 'delete-listing':
						if (empty($in->data)) throw new \Exception('listing ID not provided.');
						$out = new Out($this->getClass('Listings')->delete((object)array( 'id'=>$in->data['listing_id'] )));
						break;
					case 'update-listing':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['listing_id'])) throw new \Exception('no listing id provided.');
						if (empty($in->data['fields'])) throw new \Exception('no fields provided.');
						$out = new Out('OK', $this->getClass('Listings')->set((object)array( 'where'=>array('id'=>$in->data['listing_id']), 'fields'=>$in->data['fields'] )));
						break;
				/* Listing Geoinfo */
					case 'geocode-from-address':
						require_once(__DIR__.'/../_classes/GoogleLocation.class.php');
						$g = new GoogleLocation();
						$out = empty($in->data) ? new Out(0, 'address was not sent') : $g->geocodeAddress($in->data, 0);
						$this->log("geocode-from-address - status:{$out->status}, data:".(gettype($out->data) == 'object' || gettype($out->data) == 'array' ? print_r($out->data, true) : $out->data));
						break;
				/* Authors/Sellers */
					case 'author-has-inactive-listings':
						$inactive = false;
						$x = $this->getClass('Listings')->get((object)array( 'where'=>array('author'=>$in->data), 'what'=>array('author','active') ));
						if (!empty($x)) foreach ($x as $Listing) {
							if (!empty($Listing)){
								if ($Listing->active === 0){ 
									$inactive = true; 
								}
							}
							unset($Listing);
							if ($inactive) break;
						}
						$out = new Out('OK', $inactive);
						break;
					case 'check-for-author':
						$out = new Out( $this->getClass('Sellers')->exists((object)array( 'author_id'=>wp_get_current_user()->ID )) );
						break;
					case 'get-activity-by-author':
						$out = new Out(0, 'not built yet');
						break;
					case 'get-author':
						$x = $this->getAuthor(wp_get_current_user()->ID);
						if (!empty($x))
							$out = new Out('OK', $x);
						else
							$out = new Out('fail', "No seller found.");
						break;
					case 'get-author-by-id':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no author id provided.');
						if (empty($in->data['author_has_account']) && $in->data['author_has_account'] !== "0") throw new \Exception('no author_has_account info provided.');
						$field = 'id';
						if ($in->data['author_has_account'] == '1' ||
							$in->data['author_has_account'] == 'true')
							$field = 'author_id';
						$x = $this->getClass('Sellers')->get((object)array( 'where'=>array($field=>$in->data['id']) ));
						if (!empty($x))
							$out = new Out(array_pop( $x ));
						else
							$out = new Out('fail', "No seller found.");
						break;
					case 'update-beds-baths':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no author id provided.');
						if (empty($in->data['newbeds'])) throw new \Exception('no beds info provided.');
						if (empty($in->data['newbaths'])) throw new \Exception('no baths info provided.');
						$id = $in->data['id'];
						$beds = $in->data['newbeds'];
						$baths = $in->data['newbaths'];
						$t = $this->getClass('Listings');
						$q = new \stdClass();
						$q->where = array('id' => $id);
						$q->fields = array('beds'=>$beds,
											'baths'=>$baths);
						$a = array();
						$a[] = $q;
						$x = $t->set($a);
						if ($x)
							$out = new Out('OK', 'Updated beds/baths');
						else
							$out = new Out('fail', 'Cound not update beds/baths');
						break;
				/* Tags */
					case 'get-tags':
						try {
							//require_once(__DIR__.'/../_classes/TagsTaxonomy.class.php');
							$Tax = $this->getClass('TagsTaxonomy');
							$taxs= new \stdClass();
							$taxs = $Tax->get();
							//require_once(__DIR__.'/../_classes/TagsCategories.class.php');
							$Cats = $this->getClass('TagsCategories');
							$cats = $Cats->get();

							$out = array();
							foreach($cats as $in) {
								$out[$in->category] = array();
								unset($in);
							}

							//require_once(__DIR__.'/../_classes/Tags.class.php');
							$Tag= $this->getClass('Tags');
							$tags = $Tag->get();
							// $tag->tag = '';
							// $tag->description = '';
							// $cat->category = '';
							$tag = null;
							$cat = null;
							foreach($taxs as $tax) {
								if ($this->findTag($tax, $cats, $tags, $tag, $cat))
								{
									// $data->tag = $tag->tag;
									// $data->description = $tag->description;
									$out[$cat->category][] = $tag;
								}
								else
								{
									//echo "Failed to findTag";
								}
								unset($tax);
							}

							$out = new Out('OK', $out);
						}
						catch(Exception $e)
						{
							$out = new Out('fail', $e->getMessage());
						}
						break;
					case 'update-tags':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no listing id provided.');
						if (empty($in->data['tags'])) throw new \Exception('no tags provided.');
						$id = $in->data['id'];
						$tags = $in->data['tags'];
						$t = $this->getClass('ListingsTags');
						$q = new \stdClass();
						$q->listing_id =$id;
						$t->delete($q);
						foreach($tags as $tag) {
							$q->tag_id = $tag['id'];
							$t->add($q);
							unset($tag);
						}
						$out = new Out('OK', "Updated listing: $id with ".count($tags)." tags");
						break;
					// as long we it's being called from listings.js from saveSellerMeta(),
					// then we should deflag EmailDb for the $why value
					case 'update-seller':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no author id provided.');
						if (empty($in->data['author_has_account']) && $in->data['author_has_account'] !== "0") throw new \Exception('no author_has_account info provided.');
						if (empty($in->data['fields'])) throw new \Exception('no fields provided.');
						$why = isset($in->data['why']) ? intval($in->data['why']) : 0;
						$field = 'id';
						if ($in->data['author_has_account'] == '1' ||
							$in->data['author_has_account'] == 'true')
							$field = 'author_id';
						$s = $this->getClass('Sellers');
						$q = new \stdClass();
						$q->fields = $in->data['fields'];
						$q->where = array($field => $in->data['id']);
						$a = array();
						$a[] = $q;
						$x = $s->set($a);

						if ($why) {
							$seller = $s->get((object)['where'=>[$field => $in->data['id']]]);
							$emailDb = $this->getClass('SellersEmailDb')->get((object)['where'=>['seller_id'=>$seller[0]->id]]);
							if (!empty($emailDb)) {
								$emailDb = array_pop($emailDb);
								if ($emailDb->flags & $why) {
									$emailDb->flags &= ~$why;
									$this->getClass('SellersEmailDb')->set([(object)['where'=>['id'=>$emailDb->id],
																	 				 'fields'=>['flags'=>$emailDb->flags]]]);
								}
							}
						}

						$out = new Out('Ok', $x);
						break;
					case 'get-seller-meta-listings':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no author id provided.');
						if (empty($in->data['author_has_account']) && $in->data['author_has_account'] !== "0") throw new \Exception('no author_has_account info provided.');
						if (empty($in->data['current'])) throw new \Exception('no current listhub_key provided.');
						$field = 'id';
						if ($in->data['author_has_account'] == '1' ||
							$in->data['author_has_account'] == 'true')
							$field = 'author_id';
						$s = $this->getClass('Sellers');
						$q = new \stdClass();
						$q->where = array($field => $in->data['id']);
						$x = $s->get($q);
						if (!empty($x)) {
							$x = array_pop($x);
							if ($x->meta == null) {
								$out = new Out('fail', "Seller has no meta data");
								break;
							}
							$list = array();
							$l = $this->getClass('Listings');
							foreach($x->meta as $value) {
								if ( intval($value->action) == SELLER_IMPROVEMENT_TASKS &&
									 $value->listhub_key != $in->data['current'] ) {
									if ( !is_numeric($in->data['current']) )
										$q->where = array('listhub_key'=>$value->listhub_key);
									else
										$q->where = ['id'=>$value->listhub_key];
									$y = $l->get($q);
									if (!empty($y))
										$list[] = array_pop($y);
									unset($q->where);
								}
								unset($value);
							}
							if (count($list))
								$out = new Out('OK', $list);
							else
								$out = new Out('fail', "Thre are no other listings needing improvements from this seller.");
						}
						else
							$out = new Out('fail', "Count not find seller with $field of ".$in->data['id']);
						break;

					case 'update-author-flags':
						if (empty($in->data)) $out = new Out(0, 'no input provided.'); else {
						if (empty($in->data['id'])) throw new \Exception('no seller id provided.');
						if (empty($in->data['fields'])) throw new \Exception('no fields provided.');
						$id = intval($in->data['id']);
						$fields = $in->data['fields'];
						$x = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$id],
																		'fields'=>(array)$fields]]);
						$out = new Out( empty($x) ? 'fail' : 'OK', empty($x) ? "Failed to update seller:$id" : "Success updating seller flags");
						}
						break;

					case 'update-author':
						if (empty($in->data)) $out = new Out(0, 'no input provided.'); else {
						if (empty($in->data['id'])) throw new \Exception('no seller id provided.');
						if (empty($in->data['fields'])) throw new \Exception('no fields provided.');
						$fromBasicInfoPage = isset($in->data['fromBasicInfoPage']) ? intval($in->data['fromBasicInfoPage']) : 0;
						$callMe = isset($in->data['callMe']) ? is_true($in->data['callMe']) : 0;
						$fields = $in->data['fields'];
						$this->log("update-author - fromBasicInfoPage:$fromBasicInfoPage, callMe:$callMe, fields:".print_r($fields, true));

							require_once(__DIR__.'/../_classes/Sellers.class.php');
							$id = intval($in->data['id']);
							$s = new Sellers();
							$seller = $s->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);		
							if (empty($seller) ||
								$seller[0]->id != $id)
								throw new \Exception("Logged in user:".wp_get_current_user()->ID.", is not the seller:$id being updated");

							if (!empty($seller) &&
								(!empty($seller[0]->service_areas) ||
								 !empty($fields['service_areas'])) &&
								(empty($seller[0]->city) ||
								 empty($seller[0]->state)) ) {
								$area = explode(":", !empty($seller[0]->service_areas) ? $seller[0]->service_areas : $fields['service_areas']);
								if (count($area) == 2) {
									$area = explode(",", $area[0]);
									if (empty($seller[0]->city))
										$fields['city'] = $area[0];
									if (empty($seller[0]->state))
										$fields['state'] = trim($area[1]);
								}
							}

							if (empty($seller)) {
								$out = new Out('fail', "Could not find seller");
								break;
							}

							$this->log("update-author - sellerId:{$seller[0]->id}, flags:{$seller[0]->flags}");
							$seller = array_pop($seller);
							$amProfile = null; // can get
							$amData = null;    // can get
							$amOrder = null;
							$modData = null;   // can get
							$orderData = null;
							$portalMsg = null; // can get
							$cardMeta = null;  // can get
							$allExistingMetas = [];
							foreach($seller->meta as $meta) {
								$meta = (object)$meta;
								if ($meta->action == SELLER_PROFILE_DATA) 
									$amProfile = $meta;
								elseif ($meta->action == SELLER_AGENT_MATCH_DATA)
									$amData = $meta;
								elseif ($meta->action == SELLER_MODIFIED_PROFILE_DATA)
									$modData = $meta;
								elseif ($meta->action == SELLER_AGENT_ORDER)
									$orderData = $meta;
								elseif ($meta->action == SELLER_PORTAL_MESSAGE)
									$portalMsg = $meta;
								elseif ($meta->action == SELLER_COMPLETED_CARDS)
									$cardMeta = $meta;
								else
									$allExistingMetas[] = $meta;
								unset($meta);
							}

							$needUpdateMeta = false;
							$tags = [];
							$tagDifference = [];
							$haveMeta = false;
							$updateProfileData = false;
							$updateAmData = false;
							$updateModData = false;
							$updatePortalMsgData = false;
							$updateCardData = false;
							$havePhoto = false;
							$break = false;
							/**
							 * If fromBasicInfoPage is true, then only SELLER_AGENT_ORDER and SELLER_PROFILE_DATA can be different
							 * the rest are copies of existing meta data.
							 * Otherwise, author-update is geting called from the seller's profile page, so there will not be any
							 * modified SELLER_AGENT_ORDER data.
							 * Because updateProfileFromOrder() upates the Sellers table for both SELLER_AGENT_ORDER and SELLER_AGENT_MATCH_DATA
							 * we can't just call set(), since it would fail since the data will be the same.
							*/
							foreach($fields as $i=>&$data) {
								if ($i == 'meta') {
									$this->updateSellerMeta($data, $seller, $fromBasicInfoPage, $tags, $tagDifference);
									unset($fields[$i]); // all metas updated in updateSellerMeta()
								}
								else if ($i == 'photo')
									$havePhoto = true;
								else if ($i == 'flags') 
									$fields['flags'] = intval($fields['flags']);
								else if ($i == 'association') {
									$company = $this->getClass('Associations')->get((object)['where'=>['id'=>$data]]);
									if (!empty($company)) {
										setcookie('AGENCY', $company[0]->id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										setcookie('AGENCY_ID', $company[0]->id, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										setcookie('AGENCY_LOGO', $company[0]->logo, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										setcookie('AGENCY_LOGO_X_SCALE', $company[0]->logo_x_scale, time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										setcookie('AGENCY_URL', !empty($company[0]->url) ? $company[0]->url : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										setcookie('AGENCY_SHORTNAME', !empty($company[0]->shortname) ? $company[0]->shortname : '', time() + (86400 * 1), "/", null, false, true); // 86400 = 1 day
										$_SESSION['AGENCY'] = $company[0]->id;
										$_SESSION['AGENCY_ID'] = $company[0]->id;
										$_SESSION['AGENCY_LOGO'] = !empty($company[0]->logo) ? $company[0]->logo : 'blank.jpg';
										$_SESSION['AGENCY_LOGO_X_SCALE'] = !empty($company[0]->logo_x_scale) ? $company[0]->logo_x_scale : 1.00;
										$_SESSION['AGENCY_URL'] = !empty($company[0]->url) ? $company[0]->url : '';
										$_SESSION['AGENCY_SHORTNAME'] = !empty($company[0]->shortname) ? $company[0]->shortname : '';
									}
									else
										$this->log("update-author - failed to find association:$data for sellerId:$id");
								}
								
								unset($data);
								// if ($i == 'about')
								// 	$fields[$i] = addslashes($fields[$i]); 
							} // foreach($fields as $i=>&$data)

							$updateFlags = false;

							// if ($haveMeta) { // any SELLER_AGENT_ORDER would have been updated in updateOrderFromProfile() or updateProfileFromOrder()
							// 	if ($fromBasicInfoPage) { // then there won't be SELLER_AGENT_MATCH_DATA nor SELLER_MODIFIED_PROFILE_DATA changes
							// 		if ($updateProfileData)
							// 			$needUpdateMeta = true;
							// 	}
							// 	else if ($updateProfileData ||
							// 			 $updateAmData ||
							// 			 $updateModData ||
							// 			 $updatePortalMsgData ||
							// 			 $updateCardData)
							// 		$needUpdateMeta = true;

							// 	if (!$needUpdateMeta)
							// 		unset($fields['meta']); // get rid of meta data then...
							// }
							// else

							if ($havePhoto) {
								if (!empty($seller->photo)) {
									$isSame = strcmp($seller->photo, $fields['photo']) == 0;
									if (!$isSame)
										$this->getClass("Image")->removeImage('authors', $seller->photo);
								}
								if ($orderData) {
									$allGood = true;
									foreach($orderData->item as $item) {
										if ($item->type == 0 ||
											$item->type == 1) {
											$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
										}
										if ($item->type == ORDER_AGENT_MATCH) {
											$this->log("update-author - fromBasicInfoPage:".($fromBasicInfoPage ? 'yes' : 'no').", checking item:".print_r($item, true));
											if ($fromBasicInfoPage) {
												if ($item->mode == ORDER_BUYING) {
													if ( strlen( removeslashes($item->desc) ) < DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC) 
														$allGood = false;
												}
											}
											elseif ($item->mode == ORDER_BOUGHT) {
												if ( strlen( removeslashes($item->desc) ) < DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC) 
													$allGood = false;
											}
										}
										unset($item);
										if (!$allGood)
											break;
									}
									$this->log("update-author - fromBasicInfoPage:".($fromBasicInfoPage ? 'yes' : 'no').", allGood:".($allGood ? 'yes' : 'no'));
									if ($allGood) {
										$seller->flags = $seller->flags & ~SELLER_NEEDS_BASIC_INFO;
										if (isset($fields['flags'])) {
											$fields['flags'] = $fields['flags'] & ~SELLER_NEEDS_BASIC_INFO;
											if ($seller->flags != $fields['flags']) {
												$updateFlags = true;
												$seller->flags = $fields['flags'];
											}
										}
										else {
											$updateFlags = true;
											$fields['flags'] = $seller->flags;
										}
									}
									elseif ( !($seller->flags & SELLER_NEEDS_BASIC_INFO) ) {
										$updateFlags = true;
										$seller->flags = $fields['flags'] = $seller->flags | SELLER_NEEDS_BASIC_INFO ;
									}
								}
							}

							if (!$updateFlags && // means it didn't go through the $allGood check above
								isset($fields['flags']) &&
								$fields['flags'] == $seller->flags)
								unset($fields['flags']); // then flags still match, so don't bother updating...
							
							if ($callMe) {
								$orderitem = null;
								$break = false;
								if( !empty($orderData->item)) foreach($orderData->item as $item) {
									if ($item->type == 0 ||
										$item->type == 1) {
										$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
									}
									if ($item->mode == ORDER_BOUGHT &&
										$item->type == ORDER_AGENT_MATCH &&
										$item->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM) {
										$orderitem = $item;
										$break = true;
									}
									unset($item);
									if ($break) break;
								}
								$msg = "<p>Seller id:$seller->id, name:$seller->first_name $seller->last_name completed their basic info";
								$msg.- "input for ".(!empty($orderitem) ? $orderitem->specialtyStr : "Unknown Specialty")." in ".(!empty($orderitem) ? $orderitem->locationStr : "Unknown Location")."<br/>";
								$msg.= "Email: $seller->email, mobile:".(!empty($seller->mobile) ? $seller->mobile: "Unknown").", phone:".(!empty($seller->phone) ? $seller->phone: "Unknown")."<br/>";
								$msg.= "And have expressed intereste in being contacted for more lifestyles and cities</p>".
								$sent = $this->getClass('Email')->sendMail( SALES_EMAIL,
																			"Lifestyle Agent Callback",
																			$msg,
																			"Activated Agent Request",
																			"Call for additional lifestyles and cities");
								$this->log("update-author - sendMail to ".SALES_EMAIL." about Lifestyle Agent Callback was ".($sent ? "sent" : " a failure"));
							}

							$this->log("update-author - seller flags:$seller->flags, updating ".count($fields)." fields:".print_r($fields, true));
							// $q = new \stdClass(); $q->where = ['author_id'=>wp_get_current_user()->ID]; $q->fields = $in->data;
							if (count($fields)) {
								$x = $s->set([(object)['where'=>['author_id'=>wp_get_current_user()->ID],
													   'fields'=>$fields]]);
								$this->log("update-author - after set(), got back: ".(empty($x) ? 'failure' : 'success'));
								// get updated seller
								$seller = $s->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);
								$seller = array_pop($seller);
							}
							else {
								$x = 1; // just to make the rest of code happy
								$this->log("update-author - without any fields to update, flags:$seller->flags");
							}

							$sellerMetas = [];
							$this->updateMetaSet($sellerMetas, $seller);
							// update SellersTags based of what is now ORDER_BOUGHT lifestyles
							if ( (isset($sellerMetas[SELLER_AGENT_ORDER]) &&
								  $fromBasicInfoPage) ||
								count($tagDifference) ||
								count($tags)) { // from SELLER_AGENT_MATCH_DATA in updateSellerMeta(), if something changed
								$st = $this->getClass('SellersTags');
								// get existing ones!
								$oldTags = $st->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);
								// clear out table for this seller
								// $sql = "DELETE FROM ".$st->getTableName()." WHERE `author_id` = ".wp_get_current_user()->ID;
								// $st->rawQuery($sql);
								$cityTagList = [];

								$city = $seller->city; // default
								$state = $seller->state; // default
								$tagCities = [];

								if (!empty($seller->service_areas) ||
								 	!empty($fields['service_areas'])) {
									$area = explode(":", !empty($fields['service_areas']) ? $fields['service_areas'] : $seller->service_areas );
									if (count($area) == 2) {
										$area = explode(",", $area[0]);
										$city = $area[0]; // more specific
										$state = trim($area[1]);
									}
								} 

								// find a base city to associate agent with
								$c = $this->getClass('Cities')->get((object)['where'=>['city'=>$city,
																					   'state'=>$state]]);
								if (!empty($c)) 
									$tagCities[] = $c[0]->id;
								unset($c);

								// find the cities in the order
								
								$lifestyleItems = [];
								$lifestyleTags = [];
								global  $tag_city; // lifestyle city tags
								if (isset($sellerMetas[SELLER_AGENT_ORDER])) {
									$amOrder = $sellerMetas[SELLER_AGENT_ORDER];
									if (!empty($amOrder->item)) foreach($amOrder->item as $item) {
										$item = (object)$item;
										if ($item->type == 0 ||
											$item->type == 1) {
											$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
										}
										if ($item->type == ORDER_AGENT_MATCH &&
											$item->mode == ORDER_BOUGHT) {
											if (!in_array($item->location, $tagCities))
												$tagCities[] = $item->location;
											$lItem = clone $item;
											$lItem->type = in_array($item->specialty, $tag_city) ? 1 : 0;
											$lifestyleItems[] = $lItem;
											if (!in_array($item->specialty, $lifestyleTags))
												$lifestyleTags[] = $item->specialty;
										}
										unset($item);
									}
								}
								// get city tags/scoring for the current service area or city/state
								if (!empty($tagCities)) {
									$cityString = implode(',', $tagCities);
									$sql = "SELECT a.*, c.type FROM {$st->getTableName('cities-tags')} AS a ";
									$sql.= "INNER JOIN {$st->getTableName('cities')} AS b ON b.id = a.city_id ";
									$sql.= "INNER JOIN {$st->getTableName('tags')} as c ON c.id = a.tag_id ";
									$sql.= "WHERE b.id IN ($cityString)";
									$Tags = $st->rawQuery($sql);

									$this->log('update-author - sellerId: '.$seller->id.', gathering CitiesTags, sql: '.$sql);
									
									foreach($Tags as $tag) {
										if (!isset($cityTagList[$tag->city_id]))
											$cityTagList[$tag->city_id] = [];
										$cityTagList[$tag->city_id][$tag->tag_id] = $tag;
										unset($tag);
									}
								}

								// get city id for this set of city
								$cityId = !empty($tagCities) ? $tagCities[0] : 0;
								$updateTags = [];

								// clear out any tags from profile that is already in lifestyle
								if (!empty($lifestyleTags)) foreach($lifestyleTags as $laTag) {
									if (in_array($laTag, $tags))
										unset($tags[$laTag]);
								}

								// add any tags not part of order with type 0, meaning not used for finding premium lifestyle agents
								if (!empty($tags)) foreach($tags as $tag) {
									$updateTags[$tag.".".$cityId] = (object)['tag_id'=>$tag,
															 'score'=>$tag == 170 ? -1 : (!empty($cityTagList) && isset($cityTagList[$cityId]) && isset($cityTagList[$cityId][$tag]) && $cityTagList[$cityId][$tag]->type == 1 ? $cityTagList[$cityId][$tag]->score : 0),
															 'city_id'=>$cityId,
															 'type'=> $tag == 170 && ($seller->flags & (SELLER_IS_PREMIUM_LEVEL_2 | SELLER_IS_LIFETIME))? 1 : 0];
									unset($tag);
								}

								// add all valid order items
								if (!empty($lifestyleItems)) foreach($lifestyleItems as $item) {
									$updateTags[$item->specialty.".".$item->location] = (object)['tag_id'=>$item->specialty,
															 'score'=> $item->type == 1 ? (!empty($cityTagList) && isset($cityTagList[$item->location]) && isset($cityTagList[$item->location][$item->specialty]) && $cityTagList[$item->location][$item->location]->type == 1 ? $cityTagList[$item->location][$item->location]->score : 1) : 0,
															 'city_id'=>$item->location,
															 'type'=> 1];
								}

								if (!empty($oldTags)) foreach($oldTags as $i=>$existing) {
									$newkey = $existing->tag_id.".".$existing->city_id;
									if (array_key_exists($newkey, $updateTags)) { // this will leave alone this particular existing key
										unset($updateTags[$newkey]);
										unset($oldTags[$i]);
									}
									elseif ($existing->tag_id == 170) // this is a multi-language one, so leave alone, as it won't have the right $key
										unset($oldTags[$i]);		  // if it is set for a city other than one in service_areas

								}


								$q = new \stdClass();
								$q->author_id = wp_get_current_user()->ID;
								foreach($updateTags as $tag) {
									//update query
									$q->tag_id = $tag->tag_id;
									$q->score = $tag->score;
									$q->city_id = $tag->city_id;
									$q->type = $tag->type;
									$this->log("update-author - sellerId: ".$seller->id.", tagId:$tag->tag_id, score:$tag->score, city_id:$tag->city_id, type:$tag->type");
									$st->add($q);
									unset($tag);
								}

								if (!empty($oldTags)) {
									$this->log("update-autor - sellerId: ".$seller->id.", removing these SellersTags:".print_r($oldTags, true));
									foreach($oldTags as $tag)
										$st->delete((object)['id'=>$tag->id]);
								}
							}

							if (!empty($x)) {
								$seller->phone = !empty($seller->phone) ? fixPhone($seller->phone) : (!empty($seller->mobile) ? fixPhone($seller->mobile) : '');
								$l = $this->getClass('Listings')->count((object)['where'=>['author'=>$seller->author_id,
										 													'listhub_key'=>'notnull',
										 													'author_has_account'=>1]]);
								$seller->fromListhub = $l > 0;
								global $current_user;
		      					get_currentuserinfo();
		      					$havePortal = (	strpos($current_user->user_nicename,'unknown') !== false &&
												$current_user->nickname != 'unknown');
								$listings = $this->getClass('Listings')->get((object)array( 'where'=>['author'=>$seller->author_id,
																									  'author_has_account'=>1] ));
								if (!empty($listings)) foreach($listings as &$l) {
									$ListingsTags = $this->getClass('ListingsTags')->getGroup((object)[ 'field'=>'tag_id', 'where'=>[ 'listing_id'=>$l->id ] ]);
									$l->tags = $this->getClass('Tags')->get((object)[ 'what' => ['id','tag','type'], 'where'=>[ 'id'=>$ListingsTags ] ]);
									$l->havePortal = $havePortal;
									if (!empty($l->images)) foreach($l->images as &$img) {
										if (isset($img->file)) $img->file = removeslashes($img->file);
										if (isset($img->url)) $img->url = removeslashes($img->url);
										unset($img);
									}
									unset($l);
								}	
								$seller->listings = $listings;
							}

							$xmlFile = 'basicDashboard.xml';
							$havePortal = $seller->flags & SELLER_IS_PREMIUM_LEVEL_1;
							$haveLifestyle = $seller->flags & SELLER_IS_PREMIUM_LEVEL_2;
							if ($havePortal && !$haveLifestyle)
								$xmlFile = 'havePortal.xml';
							elseif (!$havePortal && $haveLifestyle)
								$xmlFile = 'haveLifestyle.xml';
							elseif ($havePortal && $haveLifestyle)
								$xmlFile = 'haveAll.xml';
							$xmlFile = __DIR__."/_templates/$xmlFile";
							$out = new Out( empty($x) ? 'fail' : 'OK', empty($x) ? "Failed to update seller" : ['seller'=>$seller,
																												'dashboardCards'=>readDashCardFile($xmlFile)]);
						}
						break;

					case 'image-edit':
						if (empty($in->data)) throw new \Exception('no input provided.');
						if (empty($in->data['id'])) throw new \Exception('no author id provided.');
						if (empty($in->data['mode']) && $in->data['mode'] !== "0") throw new \Exception('no mode info provided.');
						$l = $this->getClass('Listings')->get((object)array( 'where'=>array('id'=>$in->data['id']) ));
						$iniValue = ini_get("allow_url_fopen");
						ini_set("allow_url_fopen", true);
						if (!empty($l)) {
							$l = array_pop($l);
							$imageModified = false;
							$mode = intval($in->data['mode']);
							$imageClass = $this->getClass('Image');
							$needUpdate = false;
							$madeImages = false;
							$path = realpath(__DIR__."/../_img/_listings/uploaded");
							foreach($l->images as $image) {
								$img = null;
								$name = '';
								$dest = '';
								$top = 0; $bottom = 0;
								$left = 0; $right = 0;
								$alreadyDone = false;

								if ($mode == REGEN_IMAGE) {
									$name = $path.'/'.$image->file;
									$imageClass->generateSizes('listings', $name, false);
									$this->log("image-edit - regen sizes for $name");
									continue;
								}
								if (isset($image->file)) $image->file = removeslashes($image->file);
								if (isset($image->url)) $image->url = removeslashes($image->url);
								if ( isset($image->file) && substr($image->file, 0, 4 ) != 'http' ) {// then a static image exists for this
									$name = $path.'/'.$image->file;
									// if (file_exists(($name)))
									// 	$alreadyDone = true;
									// else
										unset($image->file);
								}

								if ( (isset($image->file) &&
									  substr($image->file, 0, 4 ) == 'http') ||
									  isset($image->url) ) {
									$name = (isset($image->file) && substr($image->file, 0, 4 ) == 'http') ? $image->file : $image->url;

									$this->log("mage-edit - $name");

									if (!$alreadyDone &&
							 			 $imageClass->hasWhiteBorder($name, $img, $name, $top, $bottom)) {
										$dest = $path."/".$this->getBaseImageName($name).".jpeg";								
										switch($mode) {
											case TRIM_IMAGE:
												$cropped = $imageClass->trimTopBottom($img, $top, $bottom);
												imagedestroy($img);
												$img = $cropped;
												break;
											case BLACKOUT_IMAGE:
												$imageClass->blackOutTopBottom($img, $top, $bottom);
												break;
										}
										imagejpeg($img, $dest, 95);
										$height = imagesy($img);
										$width = imagesx($img);
										imagedestroy($img);
										try {
											$imageClass->generateSizes('listings', $dest, true);
										}
										catch(\Exception $e) {
											$out = parseException($e);
											$this->log("mage-edit - exception: $out");
											continue;
										}
										$madeImages = true;
										if (!isset($image->url)) {
											$image->url = addslashes($image->file);
											unset($image->file);
											$image->file = addslashes(basename($dest));
											$image->width = $width;
											$image->height = $height;
											$needUpdate = true;
										}
										else if (!isset($image->file)) {
											$image->file = addslashes(basename($dest));
											$image->width = $width;
											$image->height = $height;
											$needUpdate = true;
										}
										if (empty($image->processed)) {
											$needUpdate = true;
										}
										$image->processed = BASE_IMAGE_PROCESSED_VALUE;
										$name = $dest; // run it through side borders
									} // if hasWhiteBorder()

									if (!$alreadyDone &&
							 			 $imageClass->hasWhiteBorderSides($name, $img, $name, $left, $right)) {
										$width = imagesx($img);
										if ( ($width - $left - $right) < 600) {// reject image
											$image->discard = (isset($image->file) && substr($image->file, 0, 4 ) == 'http') ? $image->file : $image->url;
											if (isset($image->file)) unset($image->file);
											if (isset($image->url)) unset($image->url);
											$needUpdate = true;
										}
										else {
											$dest = empty($dest) ? $path."/".$this->getBaseImageName($name).".jpeg" : $dest;
											switch($mode) {
												case TRIM_IMAGE:
													$cropped = $imageClass->trimLeftRight($img, $left, $right);
													imagedestroy($img);
													$img = $cropped;
													break;
												case BLACKOUT_IMAGE:
													$imageClass->blackOutLeftRight($img, $left, $right);
													break;
											}
											imagejpeg($img, $dest, 95);
											$height = imagesy($img);
											$width = imagesx($img);
											imagedestroy($img);
											$imageClass->generateSizes('listings', $dest, true);
											$madeImages = true;
											if (!isset($image->url)) {
												$image->url = addslashes($image->file);
												unset($image->file);
												$image->file = addslashes(basename($dest));
												$needUpdate = true;
											}
											else if (!isset($image->file)) {
												$image->file = addslashes(basename($dest));
												$needUpdate = true;
											}
											if ( (!property_exists($image, 'width') || // new case
												  !property_exists($image, 'height')) ||
												  ($image->width != $width ||          // if it had top/bottom border case
												   $image->height != $height) ) {
												$image->width = $width;
												$image->height = $height;
												$needUpdate = true;
											}
											if (empty($image->processed)) {
												$needUpdate = true;
											}
											$image->processed = BASE_IMAGE_PROCESSED_VALUE;
										}
									}

									if (isset($image->file) &&
									  	substr($image->file, 0, 4 ) != 'http') {
									  	if (!isset($image->width) || // then get width/height from static image and record it.
								  			!isset($image->height)) {
											list($width, $height, $type, $attr) = getimagesize(__DIR__."/../_img/_listings/uploaded/".removeslashes($image->file));
											$image->width = $width;
											$image->height = $height;
											$needUpdate = true;
										}
									}

									// restore file if necessary from url
									if (!isset($image->file) &&
										 isset($image->url)) {
										$image->file = addslashes($image->url);
										unset($image->url);
										$needUpdate = true;
									}

									if (!isset($image->processed) ||
										$image->processed == 0) { // means it was either processed or agent added, so set flag so it doesn't get reprocessed
										$image->processed = BASE_IMAGE_PROCESSED_VALUE;
										$needUpdate = true;
									}
								} // if have an url in 'file' or 'url'
								if ($img)
									unset($img);
								unset($image);
							} // foreach()
							if ($needUpdate){
								$q = new \stdClass();
								$q->fields = array('images'=>$l->images);
								$q->where = array('id'=>$in->data['id']);
								$a = array();
								$a[] = $q;
								$x = $this->getClass('Listings')->set($a);
								if ($x == false)
									$out = new Out('OK', "Updated images but could not update listing's images data");
								else
									$out = new Out('OK', "Updated images and listing's images data");
							}
							elseif ($madeImages)
								$out = new Out('OK', 'Updated images');
							elseif ($mode != REGEN_IMAGE)
								$out = new Out('OK', 'No images had white borders');
							else
								$out = new Out('OK', 'Images regenerated.');
						} // if have listing
						else
							$out = new Out('fail', "Failed to retrieve listing with id: ".$in->data['id']);
						break;
				default: throw new \Exception(substr(__FILE__, strpos(__FILE__, '/themes/allure/') + 8).':('.__LINE__.') - invalid query: '.$in->query); break;
			}
			if (!$out) throw new \Exception('No output returned.');
			echo json_encode($out);
		} catch (\Exception $e) { parseException($e, true); die(); }
	}

	protected function getBaseImageName($name) {
		$file_info = pathinfo($name);
		$base = $file_info['filename'];
		$base = preg_replace('/[^a-zA-Z0-9]/', '_', $base);

		$dir = $file_info['dirname'];
		$dir = explode("/", $dir);
		$prefix = '';
		$used = 0;
		if (count($dir) > 2) {
			for($i = count($dir)-1; $i > 0 && $used < 2; $i--) {
				if (!empty($dir[$i]) &&
					substr($dir[$i], 0, 4) != 'http') {
					$used++;
					$prefix .= preg_replace('/[^a-zA-Z0-9]/', '_', $dir[$i]).'_';
				}
			}
		}
		return $prefix.$base;
	}

	protected function getAuthor($author) {
		$x = $this->getClass('Sellers')->get((object)array('where'=>['author_id'=>$author] )); 
		if (!empty($x)) {
			$Tags = $this->getClass('Tags');
			global  $agent_match_specialties;
			global  $agent_match_lifestyles;
			$specialties = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_specialties]]);
			$lifestyles = $Tags->get((object)['what'=>['id','tag','type'], 'where'=>['id'=>$agent_match_lifestyles]]);
			$x = array_pop($x);
			$am_info = null;
			$am_order = null;
			$metas = [];
			if (!empty($x->meta)) foreach($x->meta as $i=>&$info) {
				if ($info->action == SELLER_AGENT_MATCH_DATA) {
					$am_info = &$info;
				}
				else if ($info->action == SELLER_AGENT_ORDER) {
					$am_order = &$info;
				}
				else
					$metas[] = $info;
				unset($info);
			}

			$needUpdate = false;
			if ($am_info == null) {
				$am_info = new \stdClass();
				$am_info->action = SELLER_AGENT_MATCH_DATA;
				foreach($specialties as &$tag) {
					$tag->used = 0;
					$tag->desc = '';
					unset($tag);
				}
				foreach($lifestyles as &$tag) {
					$tag->used = 0;
					$tag->desc = '';
					unset($tag);
				}
				$am_info->specialties = $specialties;
				$am_info->lifestyles = $lifestyles;
				
				$x->meta[] = clone $am_info;
				$metas[] = clone $am_info;
				if ($am_order)
					$metas[] = clone $am_order; // add back, so we don't lose it
				$needUpdate = true;
			}
			else {
				foreach($specialties as &$tag) {
					if (!$this->tagInArray($am_info->specialties, $tag)) {
						$tag->used = 0;
						$tag->desc = '';	
						$am_info->specialties[] = $tag;
						$needUpdate = true;
					}
					unset($tag);
				}
				foreach($lifestyles as &$tag)  {
					if (!$this->tagInArray($am_info->lifestyles, $tag)) {
						$tag->used = 0;
						$tag->desc = '';	
						$am_info->lifestyles[] = $tag;
						$needUpdate = true;
					}
					unset($tag);
				}
				if ($needUpdate)
					$metas[] = $am_info;
				if ($am_order)
					$metas[] = clone $am_order; // add back, so we don't lose it
			}
			if ($needUpdate) {
				$this->getClass('Sellers')->set([(object)['where'=>['author_id'=>wp_get_current_user()->ID],
														  'fields'=>['meta'=>$metas]]]);
				$this->log('get-author - seller:{wp_get_current_user()->ID}, updated SELLER_AGENT_MATCH_DATA with new tags');
			}

			foreach($am_info->specialties as &$tag) {
				$tag->desc = removeslashes($tag->desc);
				unset($tag);
			}
			foreach($am_info->lifestyles as &$tag) {
				$tag->desc = removeslashes($tag->desc);
				unset($tag);
			}
			if ($am_order) {
				if (!empty($am_order->item)) foreach($am_order->item as &$order) {
					$order = (object)$order;
					if ($order->type == 0 ||
						$order->type == 1) {
						$order->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
					}
					if ($order->type == ORDER_AGENT_MATCH)
						$order->desc = removeslashes($order->desc);
					unset($order);
				}
			}	

			$x->about = removeslashes($x->about);
		}
		return $x;
	}

	protected function checkSellerEmailDb($author, $active) {
		$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>$author]]);
		if (empty($seller)) {
			$this->log("checkSellerEmailDb got an empty seller for author_id:$author");
			return;
		}
		$seller = array_pop($seller);
		$EmailDb = $this->getClass("SellersEmailDb");
		if ( !$EmailDb->exists(['seller_id'=>$seller->id]) &&
			 !empty($seller->email) ) {
			$email = explode('@', $seller->email);
			if (count($email) == 2) {
				$db = (object)['seller_id'=>$seller->id,
								'name'=>$email[0],
								'flags'=>(1 << (20+$active)),
								'host'=>strtolower( $email[1] )];
				$this->log("checkSellerEmailDb - added emailDb for seller:$seller->id with email:$seller->email");
				$EmailDb->add($db);
				unset($db);
			}
		}
		else if (!empty($seller->email)) {
			$emailDb = $EmailDb->get((object)['where'=>['seller_id'=>$seller->id]])[0];
			if ( ($emailDb->flags & (1 << (20+$active))) == 0) {
				$EmailDb->set([(object)['where'=>['id'=>$emailDb->id],
										'fields'=>['flags'=>($emailDb->flags | (1 << (20+$active)))]]]);
				$this->log("checkSellerEmailDb - updated emailDb for seller:$seller->id with email:$seller->email");
			}
		}
	}

	protected function updateProfileFromReservation($lifestyles, $id) {
		$user = wp_get_current_user();
		$userId = ($user && !is_wp_error( $user )) ? $user->ID : 0;
		if (!$userId)
			return;

		$Sellers = $this->getClass('Sellers');
		$seller = $Sellers->get((object)['where'=>['author_id'=>$userId]]);
		if (empty($seller)) {
			return;
		}

		if ($seller[0]->id != $id) {
			$this->log("updateProfileFromReservation - incoming sellerId:$id is not the same as the logged in seller:$seller->id");
			return;
		}

		$seller = array_pop($seller);
		$amData = null;
		$amProfile = null;
		$needUpdate = false;
		$metas = [];
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amData = $meta;
			}
			elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
				$amProfile = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}

		// update AM Profile data with reservation data
		if ($lifestyles &&
			$amProfile) {
			// reset tags->used to 0 so that they are not AgentMatch lifestyles anymore
			foreach($amProfile->specialties as &$tag) {
				$tag->used = 0;
				unset($tag);
			}
			foreach($amProfile->lifestyles as &$tag) {
				$tag->used = 0;
				unset($tag);
			}

			foreach($lifestyles as $item) {
				$item = (object)$item;				
				$found = false;
				foreach($amProfile->specialties as &$tag) {
					$tag = (object)$tag;
					if (intval($tag->id) == intval($item->specialty)) {
						// $profileDesc = ereg_replace('/\\/g',"", $tag->desc);
						// $orderDesc = ereg_replace('/\\/g',"", $item->desc);
						if (isset($item->desc)) {
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$tag->desc = ($orderDesc);
								$needUpdate = true;												
							}
						}
						if (intval($tag->used) != 1) {
							$tag->used = 1;
							$needUpdate = true;
						}
						$found = true;
					}
					unset($tag);
					if ($found) break;
				}

				if (!$found) foreach($amProfile->lifestyles as &$tag) {
					$tag = (object)$tag;
					if (intval($tag->id) == intval($item->specialty)) {
						// $profileDesc = ereg_replace('/\\/g',"", $tag->desc);
						// $orderDesc = ereg_replace('/\\/g',"", $item->desc);
						if (isset($item->desc)) {
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$tag->desc = ($orderDesc);
								$needUpdate = true;												
							}
						}
						if (intval($tag->used) != 1) {
							$tag->used = 1;
							$needUpdate = true;
						}
						$found = true;
					}
					unset($tag);
					if ($found) break;
				}
				unset($item);
			}
		}

		// if incoming reservation is different from what's in database, replace it.
		if ($amData &&
			count($amData->item)) {
			foreach($amData->item as &$existing) {
				$existing = (object)$existing;
				if ($existing->type == 0 ||
					$existing->type == 1) {
					$existing->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
				}
				if ($existing->type != ORDER_AGENT_MATCH)
					continue;
				$gotIt = false;
				foreach($lifestyles as $item) {
					$item = (object)$item;
					if ($existing->location == $item->location &&
						$existing->specialty == $item->specialty) {
						$gotIt = true;
						if (isset($item->desc) &&
							isset($existing->desc) &&
							strcmp(removeslashes($existing->desc), removeslashes($item->desc)) != 0) { 
							$existing->desc = addslashes(removeslashes($item->desc)); // clean it up, then add slashes before saving
							$needUpdate = true;
						}
					}
					unset($item);
					if ($needUpdate) break;
				}
				unset($existing);
			}
		}
		else {
			$amData = new \stdClass();
			$amData->action = SELLER_AGENT_ORDER;
			$amData->order_id_last_purchased = 0;
			$amData->order_id_last_cancelled = 0;
			$amData->agreed_to_terms = 0;
			$amData->item = [];
			foreach($lifestyles as $item) {
				$item = (object)$item;
				$newItem = new \stdClass();
				$newItem->mode = ORDER_BUYING;
				$newItem->type = ORDER_AGENT_MATCH;
				$newItem->desc = addslashes($item->desc);
				$newItem->location = $item->location;
				$newItem->locationStr = addslashes($item->locationStr);
				$newItem->specialty = $item->specialty;
				$newItem->specialtyStr = $item->specialtyStr;
				$newItem->cart_item_key = 0;
				$newItem->order_id = 0;
				$amData->item[] = $newItem;
				unset($item);
			}
			$needUpdate = true;
		}

		if ($amProfile)
			$metas[] = $amProfile;
		if ($amData)
			$metas[] = $amData;
		if ($needUpdate)
			$Sellers->set([(object)['where'=>['author_id'=>$userId],
									'fields'=>['meta'=>$metas]]]);

		$out = new Out('OK', "Update made");
		return $out;
	}

	// this $amProfile comes from update-author's input to update the author
	// so use this to update any existing order, but don't add it back into
	// the meta array that is used to update the seller in update-author
	private function updateOrderFromProfile($amProfile, $id) {
		$Sellers = $this->getClass('Sellers');
		$seller = $Sellers->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);
		if (empty($seller)) {
			$this->log("updateOrderFromProfile - Could not find seller with ID:'".wp_get_current_user()->ID);
			$out = new Out('fail', ['status'=>'Could not find seller with ID:'.wp_get_current_user()->ID,
									'seller'=>null]);
			return $out;
		}

		if ($seller[0]->id != $id) {
			$this->log("updateOrderFromProfile - incoming sellerId:$id is not the same as the logged in seller:$seller->id");
			$out = new Out('fail', ['status'=>"incoming sellerId:$id is not the same as the logged in seller:$seller->id",
									'seller'=>null]);
			return $out;
		}

		$seller = array_pop($seller);
		$needsBasicInfo =  ($seller->flags & SELLER_NEEDS_BASIC_INFO);

		$amOrder = null; // this is the order
		$existingAmProfile = null;
		$needUpdate = false;
		$metas = [];
		// just strip out the SELLER_AGENT_ORDER
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amOrder = $meta;
			}
			elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
				$existingAmProfile = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}

		if (!$amOrder) {
			$this->log("updateOrderFromProfile - no order found for seller:$seller->id");
			return new Out('fail', ['status'=>"No order found",
									'seller'=>$seller]);
		}

		if ($amProfile) {
			foreach($amOrder->item as &$item) {
				$item = (object)$item;
				if ($item->type == 0 ||
					$item->type == 1) {
					$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
				}
				if ($item->type == ORDER_AGENT_MATCH) { // update regardless of mode (IDLE, BUYING, BOUGHT)
					$found = false;
					foreach($amProfile->specialties as &$tag) {
						$tag = (object)$tag;
						if (intval($tag->id) == intval($item->specialty) &&
							intval($tag->used) == 1) { // the tag is used and matches order specialty
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$this->log("updateOrderFromProfile - updating order, specialty tag:$tag->id with new desc");
								// if ( ($item->mode == ORDER_BOUGHT ||
								// 	  $item->mode == ORDER_BUYING) &&
								// 	// $item->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM &&
								// 	  strlen($profileDesc) >= DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC)
								// 	$hasMinimumDescLen = true;
								$item->desc = addslashes($profileDesc);
								$needUpdate = true;												
							}
							$found = true;
						}
						unset($tag);
						if ($found) break;
					}
					if (!$found) foreach($amProfile->lifestyles as &$tag) {
						$tag = (object)$tag;
						// $id = intval($tag->id);
						// $specialty = intval($item->specialty);
						// $used = intval($tag->used);
						if (intval($tag->id) == intval($item->specialty) &&
							intval($tag->used) == 1) {
						// if ($id == $specialty &&
						// 	$used) {
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$this->log("updateOrderFromProfile - updating order, lifestyles tag:$tag->id with new desc");
								// if ( ($item->mode == ORDER_BOUGHT ||
								// 	  $item->mode == ORDER_BUYING) &&
								// 	// $item->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM &&
								// 	  strlen($profileDesc) >= DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC)
								// 	$hasMinimumDescLen = true;
								$item->desc = addslashes($profileDesc);
								$needUpdate = true;												
							}
							$found = true;
						}
						unset($tag);
						if ($found) break;
					}
				}
				unset($item);
			}
			// metas[] already has the existing SELLER_AGENT_MATCH_DATA, don't update it here, let update-author do it
			// $metas[] = $amProfile;
		}

		$metas[] = $amOrder;
		$fields = [];

		if ($needUpdate) {
			$metas[] = $amProfile; // save new AM profile
			$seller->meta = $fields['meta'] = $metas;
		}

		$expectedFlags = 0;
		$hasMinimumDescLen = true; // assume the best
		foreach($amOrder->item as &$order) {
			$order = (object)$order;
			$order->type = intval($order->type);
			$order->mode = intval($order->mode);
			$order->order_id = intval($order->order_id);
			if ($order->type == 0 ||
				$order->type == 1) {
				$order->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
			}
			if ($order->type == ORDER_AGENT_MATCH) {
				if ($order->mode == ORDER_BOUGHT)
					$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_2;
				if (isset($order->desc) ) {
					$desc = removeslashes($order->desc);
					if ((($order->mode == ORDER_BOUGHT &&
						  $order->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM) ||
						  $order->mode == ORDER_BUYING) &&
						strlen($desc) < DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC) {
						$hasMinimumDescLen = false;
						$this->log("updateOrderFromProfile - specialty:order->specialtyStr:$order->specialty, location:$order->locationStr:$order->location has only ".strlen($desc)." chars");
					}
				}
			}
			else if ($order->type == ORDER_PORTAL &&
					 $order->mode == ORDER_BOUGHT)
				$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_1;
			unset($order);
		}

		if ( empty($seller->photo) ||
			!$hasMinimumDescLen )
		{
			if (!$needsBasicInfo) {
				$seller->flags = $fields['flags'] = $seller->flags | SELLER_NEEDS_BASIC_INFO;
				$this->log("updateOrderFromProfile - finds seller:$seller->id does not have all basic info now");
				$needUpdate = true;
			}
		}
		else if ($needsBasicInfo) {
			$seller->flags = $fields['flags'] = $seller->flags & ~SELLER_NEEDS_BASIC_INFO;
			$this->log("updateOrderFromProfile - finds seller:$seller->id has all basic info now");
			$needUpdate = true;
		}

		$updateFlags = false;
		if ( (($seller->flags & (SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) != $expectedFlags) ) {// uh, oh..
			$old = $seller->flags;
			$seller->flags = $fields['flags'] = ($seller->flags & ~(SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) | $expectedFlags;
			$updateFlags = true;
			$this->log("updateOrderFromProfile - finds seller:$seller->id has mismatched flags, old:$old, updated:$seller->flags, expectedFlags:$expectedFlags");
		}

		if (count($fields)) {
			$this->log("updateOrderFromProfile - updating seller:$seller->id with ".count($fields)." fields:".print_r($fields, true));
			$x = $Sellers->set([(object)['where'=>['author_id'=>wp_get_current_user()->ID],
										 'fields'=>$fields]]);
			$this->log("updateOrderFromProfile - updating seller was ".(!empty($x) ? "success" : "failure"));
		}

		unset($fields, $metas);
		$this->log("updateOrderFromProfile - needUpdateMeta:".($needUpdate | $updateFlags ? "yes" : "no")." for sellerId:$seller->id");
		$out = new Out($needUpdate ? 'OK' : 'fail', ['status'=>$needUpdate ? "Update made" : "Nothing changed",
							  						 'seller'=>$seller]);
		return $out;
	}

	// both SELLER_AGENT_ORDER and SELLER_AGENT_MATCH_DATA may get overwritten
	private function updateProfileFromOrder($amOrder, $id) {
		$Sellers = $this->getClass('Sellers');
		$seller = $Sellers->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);
		if (empty($seller)) { // then may be unregistered user
			$seller = $Sellers->get((object)['where'=>['id'=>$id]]);
			if (empty($seller)) {
				$this->log("updateProfileFromOrder - Could not find seller with ID:".wp_get_current_user()->ID.", nor with id:$id");
				$out = new Out('fail', ['status'=>'Could not find seller with ID:'.wp_get_current_user()->ID.", nor with id:$id",
										'seller'=>null]);
				return $out;
			}
		}

		if ($seller[0]->id != $id) {
			$this->log("updateProfileFromOrder - incoming sellerId:$id is not the same as the logged in seller:$seller->id");
			$out = new Out('fail', ['status'=>"incoming sellerId:$id is not the same as the logged in seller:$seller->id",
									'seller'=>null]);
			return $out;
		}

		$this->log("updateProfileFromOrder - incoming order:".print_r($amOrder, true));

		$seller = array_pop($seller);
		$needsBasicInfo =  ($seller->flags & SELLER_NEEDS_BASIC_INFO);

		$amExistingOrder = null;
		$amProfile = null;
		$needUpdate = false;
		$hasMinimumDescLen = false;
		$metas = [];
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amExistingOrder = $meta;
			}
			elseif ($meta->action == SELLER_AGENT_MATCH_DATA) {
				$amProfile = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}

		// if incoming order is different from what's in database, replace it.
		if ($amExistingOrder &&
			count($amExistingOrder->item)) {
			if ($amExistingOrder->agreed_to_terms != $amOrder->agreed_to_terms)
			{
				$needUpdate = true;
				$this->log("updateProfileFromOrder - agreed_to_terms will be changed to ".($amOrder->agreed_to_terms ? "1" : "0") );
			}

			if (count($amExistingOrder->item) != count($amOrder->item)) {
				$needUpdate = true;
				$this->log("updateProfileFromOrder - has different count for existing:".count($amExistingOrder->item)." vs incoming:".count($amOrder->item));
			}
			else foreach($amExistingOrder->item as $existing) {
				$existing = (object)$existing;
				// if ($existing->type != ORDER_AGENT_MATCH)
				// 	continue;
				$gotIt = false;
				foreach($amOrder->item as $order) {
					$order = (object)$order;
					if ($order->type == 0 ||
						$order->type == 1) {
						$order->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
					}
					if ($order->type == ORDER_AGENT_MATCH &&
						$existing->type == ORDER_AGENT_MATCH) {
						if ($existing->location == $order->location &&
							$existing->specialty == $order->specialty) {
							$desc1 = removeslashes($existing->desc);
							$desc2 = removeslashes($order->desc);
							if (strcmp($desc1, $desc2) == 0 &&
								$existing->mode == $order->mode) { 
								$gotIt = true;
							}
						}
					}
					elseif ($order->type == ORDER_PORTAL &&
							$existing->type == ORDER_PORTAL) {
						if ($order->nickname == $existing->nickname) {
							$gotIt = true;
						}
					}
					elseif ($order->type == ORDER_SIGN_UP &&
							$existing->type == ORDER_SIGN_UP) {
							$gotIt = true;
					}
					unset($order);
					if ($gotIt) break;
				}
				unset($existing);
				if (!$gotIt) {
					$needUpdate = true;
					break;
				}
			}
			$this->log("updateProfileFromOrder - after comparing with existing ORDER_AGENT_MATCH:".print_r($amExistingOrder, true).", needUpdate:".($needUpdate ? "yes" : "no")." for sellerId:$seller->id");
		}
		else {
			$needUpdate = true;
			$this->log("updateProfileFromOrder - did not find existing ORDER_AGENT_MATCH for sellerId:$seller->id");
		}

		// update AM Profile data with AM Order data
		if ($amOrder &&
			$amProfile) {
			// reset tags->used to 0 so that they are not AgentMatch lifestyles anymore
			// $currentTags = [];
			// foreach($amProfile->specialties as &$tag) {
			// 	if ($tag->used) $currentTags[$tag->id] = $tag; // keep track of what was active in the profile
			// 	$tag->used = 0;
			// }
			// foreach($amProfile->lifestyles as &$tag) {
			// 	if ($tag->used) $currentTags[$tag->id] = $tag; // keep track of what was active in the profile
			// 	$tag->used = 0;
			// }
			$needUpdateProfile = false;
			foreach($amOrder->item as $item) {
				$item = (object)$item;
				if ($item->type == 0 ||
					$item->type == 1) {
					$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
				}
				if ($item->type == ORDER_AGENT_MATCH &&
					($item->mode == ORDER_BUYING ||
					 $item->mode == ORDER_BOUGHT) ) {
					$found = false;
					foreach($amProfile->specialties as &$tag) {
						$tag = (object)$tag;
						if (intval($tag->id) == intval($item->specialty)) {
							// $profileDesc = ereg_replace('/\\/g',"", $tag->desc);
							// $orderDesc = ereg_replace('/\\/g',"", $item->desc);
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$tag->desc = addslashes($orderDesc);
								$needUpdate = true;	
								$needUpdateProfile = true;		
								$this->log("updateProfileFromOrder - $item->specialtyStr updating profile desc");									
							}
							if (intval($tag->used) != 1) {
								$tag->used = 1;
								// if (!isset($currentTags[$tag->id])) // then something new
								$needUpdate = true;
								$needUpdateProfile = true;	
								$this->log("updateProfileFromOrder - $item->specialtyStr updating profile status");
								// else
								// 	unset($currentTags[$tag->id]); // get rid of it
							}
							$found = true;
						}
						unset($tag);
						if ($found) break;
					}
					if (!$found) foreach($amProfile->lifestyles as &$tag) {
						$tag = (object)$tag;
						if (intval($tag->id) == intval($item->specialty)) {
							$profileDesc = removeslashes($tag->desc);
							$orderDesc = removeslashes($item->desc);
							if (strcmp($profileDesc, $orderDesc) != 0) {
								$tag->desc = addslashes($orderDesc);
								$needUpdate = true;	
								$needUpdateProfile = true;	
								$this->log("updateProfileFromOrder - $item->specialtyStr updating profile desc");											
							}
							if (intval($tag->used) != 1) {
								$tag->used = 1;
								// if (!isset($currentTags[$tag->id])) // then something new
								$needUpdate = true;
								$needUpdateProfile = true;	
								$this->log("updateProfileFromOrder - $item->specialtyStr updating profile status");
								// else
								// 	unset($currentTags[$tag->id]); // get rid of it
							}
							$found = true;
						}
						unset($tag);
						if ($found) break;
					}
				}
				unset($item);
			}
			// if (count($currentTags))
			// 	$needUpdate = true; // then some lifestyle that were active before is no longer active, so record that
			// unset($currentTags);
			$this->log("updateProfileFromOrder - after comparing with existing SELLER_AGENT_MATCH_DATA, needUpdateProfile:".($needUpdateProfile ? "yes" : "no")." for sellerId:$seller->id");
		}

		if ($amProfile)
			$metas[] = $amProfile;

		$expectedFlags = 0;
		$hasMinimumDescLen = true; // assume the best
		foreach($amOrder->item as &$order) {
			$order = (object)$order;
			$order->type = intval($order->type);
			$order->mode = intval($order->mode);
			$order->order_id = intval($order->order_id);
			if ($order->type == 0 ||
				$order->type == 1) {
				$order->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
			}
			if ($order->type == ORDER_AGENT_MATCH) {
				if ($order->mode == ORDER_BOUGHT)
					$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_2;
				if (isset($order->desc) ) {
					$desc = removeslashes($order->desc);
					if ((($order->mode == ORDER_BOUGHT &&
						  $order->order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM) ||
						  $order->mode == ORDER_BUYING) &&
						strlen($desc) < DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC) {
						$hasMinimumDescLen = false;
						$this->log("updateProfileFromOrder - specialty:$order->specialtyStr:$order->specialty, location:$order->locationStr:$order->location has only ".strlen($desc)." chars");
					}
				}
			}
			else if ($order->type == ORDER_PORTAL &&
					 $order->mode == ORDER_BOUGHT)
				$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_1;
			unset($order);
		}

		$metas[] = $amOrder;
		$fields = [];

		if ($needUpdate)
			$seller->meta = $fields['meta'] = $metas;

		if ( empty($seller->photo) ||
			!$hasMinimumDescLen )
		{
			if (!$needsBasicInfo) {
				$seller->flags = $fields['flags'] = $seller->flags | SELLER_NEEDS_BASIC_INFO;
				$needUpdate = true;
			}
			$this->log("updateProfileFromOrder - finds seller:$seller->id does not have all basic info now");
		}
		else if ($needsBasicInfo) {
			$seller->flags = $fields['flags'] = $seller->flags & ~SELLER_NEEDS_BASIC_INFO;
			$needUpdate = true;
			$this->log("updateProfileFromOrder - finds seller:$seller->id has all basic info now");
		}

		$updateFlags = false;
		if ( (($seller->flags & (SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) != $expectedFlags) ) {// uh, oh..
			$old = $seller->flags;
			$seller->flags = $fields['flags'] = ($seller->flags & ~(SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) | $expectedFlags;
			$updateFlags = true;
			$this->log("updateProfileFromOrder - finds seller:$seller->id has mismatched flags, old:$old, updated:$seller->flags, expectedFlags:$expectedFlags");
		}

		if (count($fields)) {
			$this->log("updateProfileFromOrder - updating seller:$id with ".count($fields)." fields:".print_r($fields, true));
			$Sellers->set([(object)['where'=>['id'=>$id],
									'fields'=>$fields]]);
		}

		unset($fields, $metas);
		$this->log("updateProfileFromOrder - had ".($needUpdate || $updateFlags ? "something" : "nothing")." to update for sellerId:$seller->id");
		$out = new Out('OK', ['status'=>$needUpdate ? "Update made" : "Nothing changed",
							  'seller'=>$seller]);
		return $out;
	}

	private function copyArray($src, $dst) {
		if (!empty($dst))
			unset($dst);
		$dst = array();
		foreach($src as $key=>$val) {
			$dst[$key] = $val;
			unset($val);
		}
	}

	private function tagInArray($arr, $tag) {
		foreach($arr as $t)
			if ($t->id == $tag->id)
				return true;

		return false;
	}

	private function findTag($tax, // taxonomy we are interested in
							$cats, // list of categories
							$tags, // list of tags
							&$tag, // found tag
							&$cat) // found category
	{
		$haveTag = 0;
		$haveCat = 0;
		foreach($cats as $c)
			if ($c->id == $tax->category_id)
			{
				$cat = $c;
				$haveCat = 1;
				break;
			}

		foreach($tags as $t)
			if ($t->id == $tax->tag_id)
			{
				$tag = $t;
				$haveTag = 1;
				break;
			}

		return $haveTag && $haveCat;
	}

	function getCityId(&$theListing) {
		$city_info = [$theListing]; // use local copy, mimic array
		if (!empty($city_info) && !empty($city_info[0]->city) && !empty($city_info[0]->state) && empty($city_info[0]->city_id)) {
			$this->log("getCityId - going to look for {$city_info[0]->city}, {$city_info[0]->state}");
			$city_info = array_pop($city_info);
			$cities = $this->getClass('Cities')->get((object)[ 'what' => [ 'id', 'city', 'state' ], 'like' => [ 'city' => $city_info->city, 'state' => $city_info->state ] ]);
			if (!empty($cities)){
				// city, state, country matched a city
				$cities = array_pop($cities);

				// update city count
				$count = $this->getClass('Listings')->count((object)[ 'city_id' => $cities->id ]);

				if ( !$this->getClass('Cities')->exists((object)[ 'id' => $cities->id, 'count' => $count ]) )
					$this->getClass('Cities')->set([(object)[ 'where' => [ 'id' => $cities->id ], 'fields' => [ 'count' => $count ] ]]);
				$this->log("getCityId - returning $cities->id");
				return $cities->id;
			} else if (!empty($city_info->city) && !empty($city_info->state) ) {
				// no matching city, geocode and add to db
				$geo = $this->getClass('GoogleLocation')->geocodeAddress(( $city_info->city ? $city_info->city : '').' '.($city_info->state ? $city_info->state : '').' '.($city_info->country ? $city_info->country : ''), 0);
				if ( $geo->status == 'OK' ){
					// setup variables for insert
					$loc = $geo->data->geometry->location;
					foreach ($geo->data->address_components as $adc){
						if (in_array( 'locality', $adc->types ))
							$city = $adc->short_name;
						else if (in_array( 'administrative_area_level_1', $adc->types ))
							$state = $adc->short_name;
						else if (in_array( 'country', $adc->types ))
							$country = $adc->short_name;
						unset($adc);
					}

					// insert new city
					$new_id = $this->getClass('Cities')->add([ 'city' => $city, 'state' => $state, 'country' => $country, 'lat' => $loc->lat, 'lng' => $loc->lng, 'count' => 1 ]);
					$this->log("getCityId - returning @2 $new_id");
					return $new_id;
				}
			}
			else
				return 0;
		}
		else
			return 0;
	}

	function listingHasImages($listing) {
		if (empty($listing->images))
			return false;

		foreach($listing->images as $image) {
			if ( isset($image->file) )
				return true;
			unset($image);
		}

		return false;
	}

	function addCityTag($item, $type = 0) {
		$item = (array)$item;
		global  $tag_city;
		$tag = intval($item['specialty']);
		if ( in_array($tag, $tag_city) ) { // it's a city tag..
			$city_tag = $this->getClass("CitiesTags")->get((object)['where'=>['city_id'=>$item['location'],
																			  'tag_id'=>$item['specialty']]]);
			if (empty($city_tag)) {
				$this->log("addCityTag - adding CityTag {$item['specialtyStr']} to {$item['locationStr']} of 1");
				$this->getClass("CitiesTags")->add((object)['city_id'=>$item['location'],
															 'tag_id'=>$item['specialty'],
															 'score'=>1,
															 'clicks'=>0,
															 'flags'=>CITY_TAG_NEED_REVIEW | $type]);
			}
			else
				unset($city_tag);
		}
	}

	function processOrder($amOrder, $id, $productType, $portalSpec, $coupon = null) {
		$amOrder->agreed_to_terms = is_true($amOrder->agreed_to_terms);
		$Sellers = $this->getClass('Sellers');
		$seller = $Sellers->get((object)['where'=>['id'=>$id]]);
		if (empty($seller)) {
			$out = new Out('fail', 'Could not find seller with sellerId:'.$id);
			return $out;
		}
		$amOrderExisting = null;
		$nickname = null;
		$metas = [];
		if (!empty($seller[0]->meta)) foreach($seller[0]->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amOrderExisting = $meta;
			}
			else {
				if ($meta->action == SELLER_NICKNAME) 
					$nickname = $meta;						
				$metas[] = $meta;
			}
			unset($meta);
		}

		$out = $this->updateProfileFromOrder($amOrder, $seller[0]->id);
		if ( $out->status != 'OK' &&
			 $out->data['status'] != "Nothing changed")
			return $out;				

		$Sellers = $this->getClass('Sellers');
		$userID = wp_get_current_user()->ID;
		if (!isset($out->data['seller']) ||
			empty($out->data['seller'])) {
			$seller = $Sellers->get((object)['where'=>['author_id'=>$userID]]);
			if (empty($seller)) {
				$out = new Out('fail', 'Could not find seller with ID:'.$userID);
				return $out;
			}

			$seller = array_pop($seller);
		}
		else
			$seller = $out->data['seller'];

		$this->log('processOrder for '.$seller->id." started, existing order:".(!empty($amOrderExisting) ? print_r($amOrderExisting, true) : "N/A").", incoming order:".print_r($amOrder, true));

		if ($productType == ORDER_PORTAL &&
			empty($nickname) ) {
			$out = new Out('fail', "Portal name must be defined and checked first.");
			return $out;
		}

		$needCart = false;
		foreach($amOrder->item as &$item) {
			$item = (object)$item;
			$item->mode = intval($item->mode);
			$item->type = intval($item->type);
			if ($item->type == 0 ||
				$item->type == 1) {
				$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
			}
			if ($item->mode == ORDER_BUYING) {
				if ($item->type == ORDER_PORTAL ||
					($item->type == ORDER_AGENT_MATCH &&
					 $amOrder->agreed_to_terms) ||
					$item->type == ORDER_SIGN_UP) {
					$needCart = true;
				}
			}
			unset($item);
			if ($needCart) break;
		}

		$needUpdate = false;
		$restored = false;
		global $woocommerce;
		$wc_cart = &$woocommerce->cart;

		$amProductId = $this->getClass('Options')->get((object)['where'=>['opt'=>'AgentMatchProductID']]);
		$amVariationId = $this->getClass('Options')->get((object)['where'=>['opt'=>'AgentMatchVariationID']]);

		$portalProductId = $this->getClass('Options')->get((object)['where'=>['opt'=>'PortalProductID']]);
		$portalVariationId = $this->getClass('Options')->get((object)['where'=>['opt'=>'PortalVariationID']]);

		$signUpId = $this->getClass('Options')->get((object)['where'=>['opt'=>'SignUpFee']]);
		if (empty($portalProductId) ||
			empty($portalVariationId) ||
			empty($amProductId) ||
			empty($amVariationId) ||
			empty($signUpId)) {
			$out = new Out('fail', 'Cound not get product details from Options');
			return $out;
		}
		$portalProductId = json_decode($portalProductId[0]->value);
		$portalVariationId = json_decode($portalVariationId[0]->value);
		$amProductId = json_decode($amProductId[0]->value);
		$amVariationId = json_decode($amVariationId[0]->value);
		$signUpId = intval($signUpId[0]->value);

		$this->log('processOrder for '.$seller->id." - needCart: $needCart, cart has ".count($wc_cart->cart_contents)." items");

		if ($needCart) {							
			// cart not empty?  Then lets see what we have already
			// clear all items in cart that is present in the order
			$wc_cart->empty_cart();
			// if (!empty($wc_cart->cart_contents)) {
			// 	$updateCart = false;
			// 	foreach($wc_cart->cart_contents as $key=>&$cartItem) {
			// 		$cartItem = (object)$cartItem;
			// 		$this->log("processOrder - item:$key => ".print_r($cartItem, true));
			// 		$gotOne = false;
			// 		if (!empty($amOrder->item)) foreach($amOrder->item as $j=>$item) {
			// 			if ($gotOne)
			// 				break;
			// 			$item = (object)$item;
			// 			// update cart inforamtion
			// 			if ($item->mode == ORDER_BUYING) {
			// 				if (isset($item->cart_item_key) &&
			// 					$item->cart_item_key == $key) {
			// 					$gotOne = true;
			// 					$persistent_cart_update = false;
			// 					// $this->log("process-agent-order item @1- ".print_r($item, true));
			// 					// $this->log("process-agent-order cartItem @1- ".print_r($cartItem, true));
			// 					switch ($item->type) {
			// 						case ORDER_AGENT_MATCH:
			// 							foreach(['location','locationStr','specialty','specialtyStr','priceLevel'] as $type) {
			// 								// if ($item->$type != $cartItem->variation[$type]) $cartItem->variation[$type] = $item->$type;
			// 								if ($type != 'priceLevel') {
			// 									if ($item->$type != $cartItem->variation[$type]) {
			// 										$this->log("processOrder $type different from cart:{$cartItem->variation[$type]} vs item:{$item->$type}");
			// 										// $wc_cart->cart_contents[$key]->variation[$type] = $item->$type;
			// 										$wc_cart->remove_cart_item($key); // this will trigger do_action('woocommerce_cart_item_removed')
			// 										$item->cart_item_key = 0;
			// 										$item->mode = ORDER_BUYING;
			// 										$persistent_cart_update = true;
			// 									}
			// 								}
			// 								else {
			// 									$subscriptionType = !isset($item->subscriptionType) || $item->subscriptionType == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
			// 									$priceLevel = !isset($item->priceLevel) || empty($item->priceLevel) ? 4 : $item->priceLevel;
			// 									// $this->log("process-agent-order item @2- ".print_r($item, true));
			// 									// $this->log("process-agent-order cartItem @2- ".print_r($cartItem, true));
			// 									if ($amVariationId[$priceLevel]->$subscriptionType != $cartItem->variation_id) {
			// 										$this->log("processOrder @1 different variationId from cart:$cartItem->variation_id vs item:{$amVariationId[$priceLevel]->$subscriptionType}");
			// 										// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
			// 										$wc_cart->remove_cart_item($key);  // this will trigger do_action('woocommerce_cart_item_removed')
			// 										$item->cart_item_key = 0;
			// 										$item->mode = ORDER_BUYING;
			// 										$persistent_cart_update = true;
			// 									}
			// 								}
			// 							}
			// 						break;
									
			// 						case ORDER_PORTAL:
			// 							$subscriptionType = !isset($item->subscriptionType) || $item->subscriptionType == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
			// 							$priceLevel = !isset($item->priceLevel) || empty($item->priceLevel) ? $portalSpec->priceLevel : $item->priceLevel;
			// 							// $this->log("process-agent-order item @2- ".print_r($item, true));
			// 							// $this->log("process-agent-order cartItem @2- ".print_r($cartItem, true));
			// 							if ($portalVariationId[$priceLevel]->$subscriptionType != $cartItem->variation_id) {
			// 								$this->log("processOrder @1-1 different variationId from cart:$cartItem->variation_id vs item:{$portalVariationId[$priceLevel]->$subscriptionType}");
			// 								// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
			// 								$wc_cart->remove_cart_item($key);  // this will trigger do_action('woocommerce_cart_item_removed')
			// 								$item->cart_item_key = 0;
			// 								$item->mode = ORDER_BUYING;
			// 								$persistent_cart_update = true;
			// 							}
			// 						break;
			// 						// these have no variations to worry about
			// 						case ORDER_SIGN_UP:
			// 						break;
			// 					}
			// 					break;
			// 				} // end if ($item->cart_item_key == $key)
			// 				else if ( empty($item->cart_item_key)) {
			// 					$cartType = strpos($cartItem->product_id, "0000-") !== false ? ORDER_PORTAL :
			// 								strpos($cartItem->product_id, "0001-") !== false ? ORDER_AGENT_MATCH :
			// 																				   ORDER_SIGN_UP;
			// 					if ($cartType == $item->type)													   	
			// 						switch ($item->type) {
			// 							case ORDER_AGENT_MATCH:
			// 								$foundSame = true;
			// 								foreach(['location','locationStr','specialty','specialtyStr','priceLevel'] as $type) {
			// 									if ($type != 'priceLevel') {
			// 										if ($item->$type != $cartItem->variation[$type]) {
			// 											$foundSame = false;
			// 										}
			// 									}
			// 									else {
			// 										$subscriptionType = !isset($item->subscriptionType) || $item->subscriptionType == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
			// 										$priceLevel = !isset($item->priceLevel) || empty($item->priceLevel) ? 4 : $item->priceLevel;
			// 										if ($amVariationId[$priceLevel]->$subscriptionType != $cartItem->variation_id) {
			// 											$foundSame = false;
			// 										}
			// 									}
			// 								}
			// 								if ($foundSame) {
			// 									$gotOne = true;
			// 									$item->cart_item_key = $key;
			// 									$amOrder->item[$j] = (array)$item;
			// 								}
			// 							break;
										
			// 							case ORDER_PORTAL:
			// 								$gotOne = true;
			// 								$subscriptionType = !isset($item->subscriptionType) || $item->subscriptionType == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
			// 								$priceLevel = !isset($item->priceLevel) || empty($item->priceLevel) ? $portalSpec->priceLevel : $item->priceLevel;
			// 								// $this->log("process-agent-order item @2- ".print_r($item, true));
			// 								// $this->log("process-agent-order cartItem @2- ".print_r($cartItem, true));
			// 								if ($portalVariationId[$priceLevel]->$subscriptionType != $cartItem->variation_id) {
			// 									$this->log("processOrder @1-1 different variationId from cart:$cartItem->variation_id vs item:{$portalVariationId[$priceLevel]->$subscriptionType}");
			// 									// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
			// 									$wc_cart->remove_cart_item($key);  // this will trigger do_action('woocommerce_cart_item_removed')
			// 									$item->cart_item_key = 0;
			// 									$item->mode = ORDER_BUYING;
			// 									$persistent_cart_update = true;
			// 								}
			// 								else {
			// 									$item->cart_item_key = $key;
			// 									$amOrder->item[$j] = (array)$item;
			// 								}
			// 							break;
			// 							// these have no variations to worry about
			// 							case ORDER_SIGN_UP:
			// 								$gotOne = true;
			// 								$item->cart_item_key = $key;
			// 								$amOrder->item[$j] = (array)$item;
			// 							break;
			// 						}
			// 				}
			// 			}
			// 		} // end foreach($amOrder->item)

			// 		// if !$gotOne, then there's something in the cart, but not in the order
			// 		if (!$gotOne) {
			// 			if (isset($cartItem->variation) &&
			// 				count($cartItem->variation)) {
			// 				$subscriptionType = MONTHLY_SUBSCRIPTION;
			// 				if (in_array($cartItem->product_id, (array)$amProductId)) {
			// 					$priceLevel = GRADE_10;
			// 					foreach($amVariationId as $id=>$var)
			// 						if ($var->monthly == $cartItem->variation_id ||
			// 							$var->quarterly == $cartItem->variation_id) {
			// 							$priceLevel = $id;
			// 							if ($var->quarterly == $cartItem->variation_id) $subscriptionType = QUARTERLY_SUBSCRIPTION;
			// 							break;
			// 						}

			// 					$item = ['mode'=>ORDER_BUYING,
			// 							 'type'=>ORDER_AGENT_MATCH,
			// 							 'desc'=>'',
			// 							 'location'=>$cartItem->variation['location'],
			// 							 'specialty'=>$cartItem->variation['specialty'],
			// 							 'subscriptionType'=>$subscriptionType,
			// 							 'priceLevel'=>$priceLevel,
			// 							 'cart_item_key'=>$key,
			// 							 'order_id'=>0
			// 							];
			// 				}
			// 				else if (in_array($cartItem->product_id, (array)$portalProductId)) {
			// 					$priceLevel = GRADE_9;
			// 					foreach($portalVariationId as $id=>$var)
			// 						if ($var->monthly == $cartItem->variation_id ||
			// 							$var->quarterly == $cartItem->variation_id) {
			// 							$priceLevel = $id;
			// 							if ($var->quarterly == $cartItem->variation_id) $subscriptionType = QUARTERLY_SUBSCRIPTION;
			// 							break;
			// 						}
			// 					$item = ['mode'=>ORDER_BUYING,
			// 							 'type'=>ORDER_PORTAL,
			// 							 'nickname'=>$cartItem->variation['nickname'],
			// 							 'subscriptionType'=>$subscriptionType,
			// 							 'priceLevel'=>$priceLevel,
			// 							 'cart_item_key'=>$key,
			// 							 'order_id'=>0
			// 							];
			// 				}
			// 				else {
			// 					$this->log("processOrder - $cartItem->product_id in cart, but not recognized.");
			// 				}
			// 			}
			// 			else {// need to find out what this $cartItem is...
			// 				$this->log("processOrder has a new item to add to order from cart with key:$key, item:$cartItem->product_id");
			// 				$item = ['mode'=>ORDER_BUYING,
			// 						 'type'=>in_array($cartItem->product_id, $portalProductId) ? ORDER_PORTAL : ORDER_SIGN_UP,
			// 						 'cart_item_key'=>$key,
			// 						 'order_id'=>0];
			// 			}

			// 			// stuff it at front
			// 			array_unshift($amOrder->item, $item);
			// 			$needUpdate = true;
			// 		}
			// 	} // foreach($wc_cart->cart_contents)
			// 	// if ($updateCart)
			// 	// 	$wc_cart->persistent_cart_update();
			// } // end if (!empty($wc_cart->cart_contents)) 

			// $wc_cart->empty_cart();
			$haveActiveSignUp = false;
			if (!empty($amOrder->item)) foreach($amOrder->item as &$item) {
				$restoredThis = false;
				$item = (array)$item;
				$item['mode'] = intval($item['mode']);
				$item['type'] = intval($item['type']);
				// $noCartKey = (!isset($item['cart_item_key']) || empty($item['cart_item_key']) || $item['cart_item_key'] === "0");
				if ($item['mode'] == ORDER_BUYING) {
					// $this->log('processOrder - item has cart_key:'.($noCartKey ? "no cart key" : "has key"));
					// if (!$noCartKey) { // them cart_item_key is there
					// 	$restoredThis = $wc_cart->restore_cart_item($item['cart_item_key']);
					// 	if (!empty($wc_cart->cart_contents)) {
					// 		foreach($wc_cart->cart_contents as $key=>&$cartItem) {
					// 			if ($key == $item['cart_item_key']) {
					// 				$restored = true; $restoredThis = true;
					// 				$this->log('processOrder - item with key:'.$item['cart_item_key']." is already in cart");
					// 			}
					// 		}
					// 	}
					// 	if (!$restored)
					// 		$this->log('processOrder - item with key:'.$item['cart_item_key']." is ".($restoredThis ? "restored" : "not restored"));
					// 	if ($restoredThis) {
					// 		$restored = true;
					// 		if ($item['type'] == ORDER_AGENT_MATCH) {
					// 			if (!empty($wc_cart->cart_contents)) foreach($wc_cart->cart_contents as $key=>&$cartItem) {
					// 				if ($key == $item['cart_item_key']) {// reset variation
					// 					// $this->log("process-agent-order item @3- ".print_r($item, true));
					// 					// $this->log("process-agent-order cartItem @3- ".print_r($cartItem, true));
					// 					$subscriptionType = !isset($item['subscriptionType']) || $item['subscriptionType'] == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
					// 					$priceLevel = !isset($item['priceLevel']) || (empty($item['priceLevel']) && $item['priceLevel'] !== 0) ? 4 : $item['priceLevel'];
					// 					if ($cartItem->product_id != $amProductId->$subscriptionType) {
					// 						$this->log("processOrder @2 $type different from {$cartItem->variation[$type]} vs {$item->$type}");
					// 						// $wc_cart->cart_contents[$key]->variation[$type] = $item->$type;
					// 						$wc_cart->remove_cart_item($key); // this will trigger do_action('woocommerce_cart_item_removed')
					// 						$item['cart_item_key'] = 0;
					// 						$noCartKey = true;
					// 						// $this->log("process-agent-order updating productId from $cartItem->product_id to $amProductId->$subscriptionType");
					// 						// $wc_cart->cart_contents[$key]->product_id = $amProductId->$subscriptionType;
					// 						// $needUpdate = true;
					// 					}
					// 					if ($cartItem->variation_id != $amVariationId[$priceLevel]->$subscriptionType) {
					// 						$this->log("processOrder @2 different variationId from $cartItem->variation_id vs {$amVariationId[$priceLevel]->$subscriptionType}");
					// 						// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
					// 						$wc_cart->remove_cart_item($key);  // this will trigger do_action('woocommerce_cart_item_removed')
					// 						$item['cart_item_key'] = 0;
					// 						$noCartKey = true;
					// 						// $this->log("process-agent-order @2 updating variationId from $cartItem->variation_id to {$amVariationId[$priceLevel]->$subscriptionType}");
					// 						// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
					// 						// $needUpdate = true;
					// 					}
					// 				}
					// 			}
					// 		}
					// 		else if ($item['type'] == ORDER_PORTAL) {
					// 			if (!empty($wc_cart->cart_contents)) foreach($wc_cart->cart_contents as $key=>&$cartItem) {
					// 				if ($key == $item['cart_item_key']) {// reset variation
					// 					// $this->log("process-agent-order item @3- ".print_r($item, true));
					// 					// $this->log("process-agent-order cartItem @3- ".print_r($cartItem, true));
					// 					$subscriptionType = !isset($item['subscriptionType']) || $item['subscriptionType'] == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
					// 					$priceLevel = !isset($item['priceLevel']) || (empty($item['priceLevel']) && $item['priceLevel'] !== 0) ? $portalSpec->priceLevel : $item['priceLevel'];
					// 					if ($cartItem->product_id != $portalProductId->$subscriptionType) {
					// 						$this->log("processOrder @2-2 $type different from {$cartItem->variation[$type]} vs {$item->$type}");
					// 						// $wc_cart->cart_contents[$key]->variation[$type] = $item->$type;
					// 						$wc_cart->remove_cart_item($key); // this will trigger do_action('woocommerce_cart_item_removed')
					// 						$item['cart_item_key'] = 0;
					// 						$noCartKey = true;
					// 						// $this->log("process-agent-order updating productId from $cartItem->product_id to $amProductId->$subscriptionType");
					// 						// $wc_cart->cart_contents[$key]->product_id = $amProductId->$subscriptionType;
					// 						// $needUpdate = true;
					// 					}
					// 					if ($cartItem->variation_id != $portalProductId[$priceLevel]->$subscriptionType) {
					// 						$this->log("processOrder @2-2 different variationId from $cartItem->variation_id vs {$amVariationId[$priceLevel]->$subscriptionType}");
					// 						// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
					// 						$wc_cart->remove_cart_item($key);  // this will trigger do_action('woocommerce_cart_item_removed')
					// 						$item['cart_item_key'] = 0;
					// 						$noCartKey = true;
					// 						// $this->log("process-agent-order @2 updating variationId from $cartItem->variation_id to {$amVariationId[$priceLevel]->$subscriptionType}");
					// 						// $wc_cart->cart_contents[$key]->variation_id = $amVariationId[$priceLevel]->$subscriptionType;
					// 						// $needUpdate = true;
					// 					}
					// 				}
					// 			}
					// 		}
					// 	}
					// }

					// if ( $noCartKey ||
					// 	!$restoredThis ) {
						switch($item['type']) {
							case ORDER_AGENT_MATCH:
								if ($amOrder->agreed_to_terms &&
									(isset($item['location']) && !empty($item['location'])) &&
									(isset($item['specialty']) && !empty($item['specialty'])) ) {
									$subscriptionType = !isset($item['subscriptionType']) || $item['subscriptionType'] == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
									$priceLevel = !isset($item['priceLevel']) || (empty($item['priceLevel']) && $item['priceLevel'] !== 0) ? 4 : $item['priceLevel'];
									$this->log("processOrder $subscriptionType priceLevel:$priceLevel, variation:".$amVariationId[$priceLevel]->$subscriptionType.", item @4- ".print_r($item, true));
									$cart_item_key = $wc_cart->add_to_cart($amProductId->$subscriptionType, 
																			1, 
																		    $amVariationId[$priceLevel]->$subscriptionType, 
																		    ['location'=>intval($item['location']),
																			 'locationStr'=>$item['locationStr'],
																			 'specialty'=>intval($item['specialty']),
																			 'specialtyStr'=>$item['specialtyStr']]);
									$item['cart_item_key'] = $cart_item_key;
									$item['order_id'] = 0;
									$needUpdate = true;
									$this->log('processOrder for '.$seller->id." added ORDER_AGENT_MATCH to cart with key: $cart_item_key, $subscriptionType, $priceLevel");

									$this->addCityTag($item, CITY_TAG_PURCHASED);
								}
								break;
							case ORDER_PORTAL:
								$subscriptionType = !isset($item['subscriptionType']) || $item['subscriptionType'] == MONTHLY_SUBSCRIPTION ? "monthly" : "quarterly";
								$priceLevel = !isset($item['priceLevel']) || (empty($item['priceLevel']) && $item['priceLevel'] !== 0) ? $portalSpec->priceLevel : $item['priceLevel'];
								$this->log("processOrder $subscriptionType priceLevel:$priceLevel, variation:".$portalVariationId[$priceLevel]->$subscriptionType.", item @5- ".print_r($item, true));
								$cart_item_key = $wc_cart->add_to_cart($portalProductId->$subscriptionType, 
																		1,
																		$portalVariationId[$priceLevel]->$subscriptionType, 
																		['nickname'=>$item['nickname']]);
								$item['cart_item_key'] = $cart_item_key;
								$item['order_id'] = 0;
								$needUpdate = true;
								$this->log('processOrder for '.$seller->id." added to cart with key: $cart_item_key, Portal");
								break;
							case ORDER_SIGN_UP:
								$cart_item_key = $wc_cart->add_to_cart($signUpId, 1);
								$item['cart_item_key'] = $cart_item_key;
								$item['order_id'] = 0;
								$needUpdate = true;
								$this->log('processOrder for '.$seller->id." added to cart with key: $cart_item_key, SignUpFee");
								break;
							default:
								$this->log('process-agent-order for '.$seller->id." not adding to cart, type:{$item['type']}, mode:{$item['mode']}, cart_item_key:{$item['cart_item_key']}");
						}
					// }
				}
				// elseif ($item['mode'] == ORDER_IDLE) {
				// 	// if cart_item_key is set, then remove it from the card, since the user has decided not to get it.
				// 	if (isset($item['cart_item_key']) && !empty($item['cart_item_key']) && $item['cart_item_key'] !== "0") {
				// 		$wc_cart->remove_cart_item($item['cart_item_key']);
				// 		$item['cart_item_key'] = 0;
				// 		$item['order_id'] = 0;
				// 		$needUpdate = true;
				// 	}
				// }
				unset($item);
			}
		}

		$wc_err = '';
		$wc_err_count = 0;
		// if (($needUpdate || $restored || count($wc_cart->cart_contents)) &&
		if ($needUpdate &&
			!empty($coupon)) {
			wc_clear_notices();
			$success = $wc_cart->add_discount($coupon);
			if (!$success) {
				$notices = wc_get_notices();
				$this->log("processOrder - failed to apply coupon:$coupon - notices:".print_r($notices, true));
				if (!empty($notices)) foreach($notices as $type=>$msgs) {
					foreach($msgs as $notice) {
						$wc_err .= "<br/><span>$notice</span>";
						$wc_err_count++;
						unset($notice);
					}
					unset($msgs);
				}
			}
			else
				$this->log("processOrder - applied coupon:$coupon");
		}

		// if ($amProfile)
		// 	$metas[] = $amProfile;
		$metas[] = $amOrder;
		if ($needUpdate)
			$Sellers->set([(object)['where'=>['author_id'=>wp_get_current_user()->ID],
									'fields'=>['meta'=>$metas]]]);

		// $out = new Out('OK', (object)["status"=>(!empty($wc_err) ? "Coupon error" : ($needUpdate || $restored || count($wc_cart->cart_contents) ? "Cart made" : "Nothing added to cart")),
		$out = new Out('OK', (object)["status"=>(!empty($wc_err) ? "Coupon error" : ($needUpdate ? "Cart made" : "Nothing added to cart")),
									  "error"=>$wc_err,
									  "errorCount"=>$wc_err_count,
									  "order"=>$amOrder]);
		return $out;
	} 

	function processAMInvite($amOrder, $seller, $ic, $register, $fromPurchasePage = false) {
		// then for each ORDER_AGENT_MATCH in the order, update the SellerTags and order status
		$out = $this->updateProfileFromOrder($amOrder, $seller->id);
		if ( $out->status != 'OK') {
			$this->log("processAMInvite - failed updateProfileFromOrder with:".$out->data['status']);
			return $out;
		}

		if (!isset($out->data['seller']) ||
			empty($out->data['seller'])) {
			$seller = $this->getClass('Sellers')->get((object)['where'=>['author_id'=>wp_get_current_user()->ID]]);
			if (empty($seller)) {
				$out = new Out('fail', ["status"=>'Could not find seller with userId:'.wp_get_current_user()->ID]);
				return $out;
			}

			// $userId = wp_get_current_user()->ID;
			$seller = array_pop($seller);
		}
		else
			$seller = $out->data['seller'];

		$amOrder->action = intval($amOrder->action);
		$userId = $seller->author_id;
		$amOrderExisting = null;
		$zohoData = null;
		// $amProfile = null;
		$needUpdate = false;
		$metas = [];
		if (!empty($seller->meta)) foreach($seller->meta as $meta) {
			if ($meta->action == SELLER_AGENT_ORDER) {
				$amOrderExisting = $meta;
			}
			else if ($meta->action == SELLER_ZOHO_LEAD_ID) {
				$zohoData = $meta;
				$metas[] = $meta;
			}
			else
				$metas[] = $meta;
			unset($meta);
		}

		$resMetas = [];
		$resMeta = new \stdClass();
		$resMeta->action = ORDER_AGENT_MATCH;
		$resMeta->item = [];

		$this->log('processAMInvite for '.$seller->id." started, existing order:".(!empty($amOrderExisting) ? print_r($amOrderExisting, true) : "N/A").", incoming order:".print_r($amOrder, true));

		$expectedFlags = 0;
		$gotActiveTag = false;
		// update order status and create or update the necessary tags for the seller.
		if ($amOrder) {
			$SellersTags = $this->getClass('SellersTags');
			if (!empty($amOrder->item)) foreach($amOrder->item as &$sellerItem) {
				$sellerItem = (object)$sellerItem;
				if ($sellerItem->type == 0 ||
					$sellerItem->type == 1) {
					$sellerItem->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
				}
				if ($sellerItem->type == ORDER_AGENT_MATCH) {
					$itsIdle = false;
					if ($sellerItem->mode == ORDER_BUYING) {
						$desc = removeslashes($sellerItem->desc);
						if (strlen($desc) < DEFAULT_MINIMUM_LIFESTYLE_AGENT_DESC) {
							$out = new Out('fail', ['status'=>"Lifestyle:$sellerItem->specialtyStr has only ".strlen($desc)." characters.",
													'order'=>$amOrder]);
							return $out;
						}
						$needUpdate = true;
						$sellerItem->mode = ORDER_BOUGHT;
						$sellerItem->order_id = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;
						$sellerItem->inviteCode = $ic->code;
						$sellerItem->inviteUsedDate = date("Y-m-d H:i:s", time() + ($this->timezone_adjust*3600));
						$sellerItem->cart_item_key = $ic->code;
						$gotActiveTag = true;
						$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_2;
					}
					else if ($sellerItem->mode == ORDER_BOUGHT) {
						$gotActiveTag = $gotActiveTag ? $gotActiveTag : ($sellerItem->mode == ORDER_BOUGHT ? true : false);
						$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_2;
					}
					else
						$itsIdle = true;

					$resMeta->item[] = clone $sellerItem;

					if ($itsIdle)
						continue; // must be ORDER_IDLE

					if ( $SellersTags->exists(['author_id'=>$userId,
											   'city_id'=>$sellerItem->location,
											   'tag_id'=>$sellerItem->specialty])) {
						$tag = $SellersTags->get((object)['where'=>['author_id'=>$userId,
																	'city_id'=>$sellerItem->location,
																	'tag_id'=>$sellerItem->specialty]]);
						if (!empty($tag) && $tag[0]->type != 1) {
							$SellersTags->set([(object)['where'=>['author_id'=>$userId,
																  'city_id'=>$sellerItem->location,
																  'tag_id'=>$sellerItem->specialty],
																  'fields'=>['type'=>1]]]);
							$this->log("processAMInvite - updated tag for author_id:$userId, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1");
						}
						else
							$this->log("processAMInvite - tag already exists for author_id:$userId, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1");
					}
					else {
						$tagId = intval($sellerItem->specialty);
						$tagList = [];

					
						$sql = "SELECT a.*, c.type FROM {$SellersTags->getTableName('cities-tags')} AS a ";
						$sql.= "INNER JOIN {$SellersTags->getTableName('cities')} AS b ON b.id = a.city_id ";
						$sql.= "INNER JOIN {$SellersTags->getTableName('tags')} as c ON c.id = a.tag_id ";
						$sql.= "WHERE b.id={$sellerItem->location} AND a.tag_id=$tagId";
						$this->log("processAMInvite - sql:$sql");

						$tag = $SellersTags->rawQuery($sql);				
					
						$score = (!empty($tag) && $tag[0]->type == 1 ? ($tag[0]->score == -1 ? 10 : $tag[0]->score) : 0);
						$x = $SellersTags->add(['author_id'=>$userId,
												'city_id'=>$sellerItem->location,
												'tag_id'=>$sellerItem->specialty,
												'score'=>$score,
												'type'=>1]);
						$this->log("processAMInvite - added tag for author_id:$userId, city_id:{$sellerItem->location}, city:{$sellerItem->locationStr}, specialty:{$sellerItem->specialty},{$sellerItem->specialtyStr}, score:{$tag[0]->score}, type:1 was ".(!empty($x) ? 'successful' : 'a failure'));
					}	

					$this->addCityTag($sellerItem, CITY_TAG_INVITATION);				
				}	
				else if ($sellerItem->type == ORDER_PORTAL &&
					 $sellerItem->mode == ORDER_BOUGHT)
				$expectedFlags |= SELLER_IS_PREMIUM_LEVEL_1;
				unset($sellerItem);								 																	
			}	
		}

		$resMetas[] = $resMeta;
		if ($needUpdate) {
			if ($this->getClass('Reservations')->exists(['seller_id'=>$seller->id])) {
				$res = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller->id]]);
				$fields = [];
				if ($res[0]->flags != ($res[0]->flags | SELLER_IS_PREMIUM_LEVEL_2 | RESERVATION_CONVERTED_LIFESTYLE_CODE))
					$fields['flags'] = $res[0]->flags | SELLER_IS_PREMIUM_LEVEL_2 | RESERVATION_CONVERTED_LIFESTYLE_CODE;
				if ( ($res[0]->type & SELLER_IS_PREMIUM_LEVEL_2) == 0)
					$fields['type'] = $res[0]->type | SELLER_IS_PREMIUM_LEVEL_2;
				// check $res->meta and update with ORDER_AGENT_MATCH
				$needUpdateMeta = false;
				if (!empty($res[0]->meta)) {
					$break = false;
					foreach($res[0]->meta as $meta) {
						$meta = (object)$meta; // just in case..
						if ($meta->action == ORDER_AGENT_MATCH) { // just do the first one...
							if (count($meta->item) != count($resMeta->item)) {
								$needUpdateMeta = true;
								unset($meta);
								break; // done
							}
							// now compare each one to see if we still have them all or not
							foreach($meta->item as $existing) {
								$found = false;
								foreach($resMeta->item as $incoming) {
									if ($existing->location == $incoming->location &&
										$existing->specialty == $incoming->specialty) {
										if ($existing->mode == $incoming->mode) {
											$found = true;
											$break = true;
										}
									}
								}
								if (!$found) {
									$needUpdateMeta = true;
									$break = true; // done
								}
								unset($existing);
							}
							$break = true; // doen with first ORDER_AGENT_MATCH
						}
						unset($meta);
						if ($break) break;
					}
				}
				else
					$needUpdateMeta = true;


				if ($needUpdateMeta) 
					$fields['meta'] = $resMetas;

				if ( empty($res[0]->lead_id) ||
					 ($zohoData &&
					  $zohoData->lead_id != $res[0]->lead_id) ) {
					$this->log("processAMInvite - updating reservation->lead_id with one from seller:$zohoData->lead_id");
					$fields['lead_id'] = $zohoData->lead_id;
				}

				if (count($fields))
					$this->getClass('Reservations')->set([(object)[	'where'=>['seller_id'=>$seller->id],
												 					'fields'=>$fields ]]);
				$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller->id]]);
				if (!empty($reservation)) {
					$reservation = array_pop($reservation);
					$zoho = $this->getClass('Zoho', 1)->processOneZohoCrm(false, $reservation, $seller);
					$this->log("processAMInvite - zoho result:".($zoho ? 'success' : 'failure').", for existing reservation_id:$reservation->id, seller_id:".(empty($seller) ? "0" : $seller->id));
					unset($reservation);
				}
				else
					$this->log("processAMInvite - failed to retrieve existing Reservation at row {$res[0]->id} for sellerId:$seller->id");
				unset($res);
			}
			else { 
				$newRes = ['seller_id'=>$seller->id,
						   'first_name'=>$seller->first_name,
						   'last_name'=>$seller->last_name,
						   'email'=>$seller->email,
						   'phone'=>(!empty($seller->mobile) ? $seller->mobile : $seller->phone),
						   'type'=>SELLER_IS_PREMIUM_LEVEL_2,
						   'flags'=>RESERVATION_CONVERTED_LIFESTYLE_CODE,
						   'meta'=>$resMetas];
				if ($zohoData)
					$newRes['lead_id'] = $zohoData->lead_id;
				$x = $this->getClass('Reservations')->add((object)$newRes);
				if (!empty($x)) {
					$reservation = $this->getClass('Reservations')->get((object)['where'=>['seller_id'=>$seller->id]]);
					if (!empty($reservation)) {
						$reservation = array_pop($reservation);
						$zoho = $this->getClass('Zoho', 1)->processOneZohoCrm(true, $reservation, $seller);
						$this->log("processAMInvite - zoho result:".($zoho ? 'success' : 'failure').", for new reservation_id:$reservation->id, seller_id:".(empty($seller) ? "0" : $seller->id));
						unset($reservation);
					}
					else
						$this->log("processAMInvite - failed to retrieve new Reservation at row $x for sellerId:$seller->id");
				}
				else {
					$this->log("processAMInvite - failed to add new Reservation for sellerId:$seller->id, $seller->first_name $seller->last_name");
				}
			}
		}

		unset($resMetas);

		$fields = [];
		if ($needUpdate)
			$amOrder->order_id_last_purchased = DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM;

		$metas[] = $amOrder;

		if ($needUpdate)
			$fields['meta'] = $metas;

		if ($gotActiveTag) {
			if ( !($seller->flags & SELLER_IS_PREMIUM_LEVEL_2) ) {
				$fields['flags'] = $seller->flags | SELLER_IS_PREMIUM_LEVEL_2 | SELLER_LIFESTYLE_FROM_INVITE;
				$seller->flags |= SELLER_IS_PREMIUM_LEVEL_2 | SELLER_LIFESTYLE_FROM_INVITE;
			}
		}

		$updateFlags = false;
		if ( (($seller->flags & (SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) != $expectedFlags) ) {// uh, oh..
			$old = $seller->flags;
			$seller->flags = $fields['flags'] = ($seller->flags & ~(SELLER_IS_PREMIUM_LEVEL_1 | SELLER_IS_PREMIUM_LEVEL_2)) | $expectedFlags;
			$updateFlags = true;
			$this->log("processAMInvite - finds seller:$seller->id has mismatched flags, old:$old, updated:$seller->flags, expectedFlags:$expectedFlags");
		}

		if (count($fields)) {
			$register->updateInvite($ic, $seller->id);
			$this->getClass('Sellers')->set([(object)['where'=>['author_id'=>wp_get_current_user()->ID],
													  'fields'=>$fields]]);
			$needUpdate = true; // if it wasn't already, just flag update
		}

		$this->log("processAMInvite - is returning OK, needUpdate:".($needUpdate | $updateFlags ? 'true' : 'false')." for sellerId:$seller->id");
		return new Out('OK', ['status'=>($needUpdate ? "Update made" : "Nothing changed"),
							  'seller'=>$seller,
							  'order'=>$amOrder]);
	}

	protected function updateMetaSet(&$metaSet, $seller) {
		foreach($seller->meta as $meta) {
			$metaSet[$meta->action] = $meta;
			unset($meta);
		}
	}

	protected function updateSellerMeta($metas, &$seller, $fromBasicInfoPage, &$tags, &$tagDifference) {
		$allExistingMetas = [];
		$this->updateMetaSet($allExistingMetas, $seller);
		$needUpdate = false;

		// massage incoming metas so they are all object and is in an associated array with action as key
		$x = [];
		foreach($metas as &$info) {
			$info = (object)$info;
			$info->action = intval($info->action);
			$x[$info->action]= $info;
			unset($info);
		}
		unset($metas);
		$metas = $x;

		// NOTE: do SELLER_AGENT_MATCH_DATA so that if fromBasicInfoPage, we get the tagDifference[] populated properly
		//       if we aren't fromBasicInfoPage then SELLER_AGENT_ORDER is pretty much left alone, except for updating the desc
		//       for an item if it differed from SELLER_AGENT_MATCH_DATA's tags
		$metaIds = [SELLER_AGENT_MATCH_DATA, SELLER_AGENT_ORDER, SELLER_PROFILE_DATA, SELLER_MODIFIED_PROFILE_DATA, SELLER_COMPLETED_CARDS, SELLER_PORTAL_MESSAGE];

		$existingUsedTags = [];
		foreach($metaIds as $mId) {
			if (!isset($metas[$mId]))
				continue; // it's a don't care..

			$info = $metas[$mId];
			if ($fromBasicInfoPage) {
				switch ($info->action) {
					case SELLER_AGENT_ORDER:  // just because fromBasicInfoPage is true, doesn't mean SELLER_AGENT_ORDER has any new data!!!
						$out = $this->updateProfileFromOrder($info, $seller->id); // this updated the seller's SELLER_AGENT_ORDER and SELLER_AGENT_MATCH_DATA
						if ($out->status == 'OK') {								  // so no need to set needUpdate to true
							$seller = $out->data['seller'];
							$this->updateMetaSet($allExistingMetas, $seller);
							if ($out->data['status'] == 'Update made') {
								// updated SELLER_AGENT_MATCH_DATA
								$amData = $allExistingMetas[SELLER_AGENT_MATCH_DATA];
								foreach($amData->specialties as &$tag) {
									$tag = (object)$tag;
									$tag->id = intval($tag->id);
									$tag->used = intval($tag->used);
									if ($tag->used)
										$tags[] = $tag->id;
									unset($tag);
								}
								foreach($amData->lifestyles as &$tag) {
									$tag = (object)$tag;
									$tag->id = intval($tag->id);
									$tag->used = intval($tag->used);
									if ($tag->used)
										$tags[] = $tag->id;
									unset($tag);
								}
								$tagDifference = array_diff($tags, $existingUsedTags);
							}
							else {
								$tags = array_merge($existingUsedTags); // makes copy, $tagDifference will remain empty
							}

						}
						else
							$this->log('updateSellerMeta - failed updateProfileFromOrder()');
						break;

					case SELLER_AGENT_MATCH_DATA: // just do this to get the tags and tagDifference arrays
						if (isset($allExistingMetas[SELLER_AGENT_MATCH_DATA])) {
							$amData = $allExistingMetas[SELLER_AGENT_MATCH_DATA];
							foreach($amData->specialties as &$tag) {
								$tag = (object)$tag;
								$tag->id = intval($tag->id);
								$tag->used = intval($tag->used);
								if ($tag->used)
									$existingUsedTags[] = $tag->id;
								unset($tag);
							}
							foreach($amData->lifestyles as &$tag) {
								$tag = (object)$tag;
								$tag->id = intval($tag->id);
								$tag->used = intval($tag->used);
								if ($tag->used)
									$existingUsedTags[] = $tag->id;
								unset($tag);
							}
						}
						break;

				}
				// ignore all other metas if fromBasicInfoPage
			}
			else {
				// if this meta hasn't existed, add it to the list and set for updating the meta field
				if (!isset($allExistingMetas[$info->action])) {
					if ($info->action != SELLER_AGENT_MATCH_DATA) {// else need to process tags, and it will save the metas via updateOrderFromProfile()
						// convert int values to integers
						switch($info->action) {
							case SELLER_PROFILE_DATA:
								foreach(['year','sold','inArea'] as $key) 
									$info->$key = intval($info->$key);
								break;
							case SELLER_AGENT_MATCH_DATA:
								foreach($info->specialties as &$tag) {
									$tag = (object)$tag;
									$tag->id = intval($tag->id);
									$tag->type = intval($tag->type);
									$tag->used = intval($tag->used);
									unset($tag);
								}
								foreach($info->lifestyles as &$tag) {
									$tag = (object)$tag;
									$tag->id = intval($tag->id);
									$tag->type = intval($tag->type);
									$tag->used = intval($tag->used);
									unset($tag);
								}
								break;
							case SELLER_MODIFIED_PROFILE_DATA:
								foreach($info as $i=>$value) {
									if (gettype($value) != 'array')
										$info->$i = intval($value);
									unset($value);
								}
								break;
							case SELLER_COMPLETED_CARDS:
								foreach($info as $i=>$value) {
									$info->$i = intval($value);
									unset($value);
								}
								break;
						}
						$allExistingMetas[$info->action] = $info;
						$needUpdate = true;
						continue;
					}
				}
				$break = false;
				switch($info->action) {
					case SELLER_PROFILE_DATA:
						$amProfile = $allExistingMetas[SELLER_PROFILE_DATA];
						$updateProfileData = false;
						foreach(['year','sold','inArea'] as $key) {
							$info->$key = intval($info->$key);
							$amProfile->$key = intval($amProfile->$key);
						}

						foreach($info as $id=>$value) {
							if (isset($amProfile->$id) ) {
								if (gettype($info->$id) == 'array') {
									if (count(array_diff($info->$id, $amProfile->$id))) {
										$updateProfileData = true;
										$this->log("updateSellerMeta - SELLER_PROFILE_DATA array $id is diffent");
										$break = true;
									}
									// else
									// 	$this->log("update-author - SELLER_PROFILE_DATA array $id is same");
								}
								elseif ($info->$id != $amProfile->$id) {
									$updateProfileData = true;
									$this->log("updateSellerMeta - SELLER_PROFILE_DATA data $id is diffent");
									$break = true;
								}
							}
							else  {
								$updateProfileData = true;
								$this->log("updateSellerMeta - SELLER_PROFILE_DATA does not have $id set yet");
								$break = true;
							}
							unset($value);
							if ($break) break;
						}
						if (isset($info->languages) &&
							(count($info->languages) > 1 ||
							 (count($info->languages) && $info->languages[0] != 'English')) )
							$tags[] = 170; // multi-lingual

						if ($updateProfileData) {
							$allExistingMetas[SELLER_PROFILE_DATA] = $info;
							$needUpdate = true;
						}
						break;

					case SELLER_AGENT_MATCH_DATA:
						$out = $this->updateOrderFromProfile($info, $seller->id); // this updates the order meta if needed
						$needProcessAmData = true;
						$this->log("updateSellerMeta - sellerId:$id, SELLER_AGENT_MATCH_DATA calling updateOrderFromProfile() status:".$out->data['status']);
						if ($out->status == 'OK' ||
							$out->data['status'] == "Update made") {
							$tempMetas = [];
							$this->updateMetaSet($tempMetas, $out->data['seller']);
							$allExistingMetas[SELLER_AGENT_ORDER] = $tempMetas[SELLER_AGENT_ORDER]; // may not need to do this, but what the heck..
							unset($tempMetas);
						}
						else {
							$this->log('updateSellerMeta - failed updateOrderFromProfile(), status:'.$out->data['status']);
							// same seller still
							if (!isset($allExistingMetas[SELLER_AGENT_MATCH_DATA])) {
								$allExistingMetas[SELLER_AGENT_MATCH_DATA] = $info;
								$needUpdate = true;
								$needProcessAmData = false;
							}
						}
						unset($out);

						if (isset($allExistingMetas[SELLER_AGENT_MATCH_DATA])) {
							$amData = $allExistingMetas[SELLER_AGENT_MATCH_DATA];
							foreach($amData->specialties as &$tag) {
								$tag = (object)$tag;
								$tag->id = intval($tag->id);
								$tag->used = intval($tag->used);
								if ($tag->used)
									$existingUsedTags[] = $tag->id;
								unset($tag);
							}
							foreach($amData->lifestyles as &$tag) {
								$tag = (object)$tag;
								$tag->id = intval($tag->id);
								$tag->used = intval($tag->used);
								if ($tag->used)
									$existingUsedTags[] = $tag->id;
								unset($tag);
							}
						}

						foreach($info->specialties as &$tag) {
							$tag = (object)$tag;
							$tag->id = intval($tag->id);
							$tag->used = intval($tag->used);
							$tag->type = intval($tag->type);
							unset($tag);
						}
						foreach($info->lifestyles as &$tag) {
							$tag = (object)$tag;
							$tag->id = intval($tag->id);
							$tag->used = intval($tag->used);
							$tag->type = intval($tag->type);
							unset($tag);
						}

						$foundDifference = false;
						$amOrder = isset($allExistingMetas[SELLER_AGENT_ORDER]) ? $allExistingMetas[SELLER_AGENT_ORDER] : null;

						if ($needProcessAmData) {
							$amData = $allExistingMetas[SELLER_AGENT_MATCH_DATA];
							$incomingTags = [];
							$existingTags = [];
							foreach($info->specialties as $tag) {
								$incomingTags[$tag->id] = $tag; unset($tag);
							}
							foreach($amData->specialties as $tag) {
								$existingTags[$tag->id] = $tag; unset($tag);
							}
							// now go through them
							foreach($incomingTags as $incomingTag) {
								if (!isset($existingTags[$incomingTag->id]))
									$foundDifference = true;
								else {
									$existingTag = $existingTags[$incomingTag->id];
									if ($incomingTag->used != $existingTag->used)
										$foundDifference = true;
								}
								if (!$foundDifference) {
									$desc1 = removeslashes($incomingTag->desc);
									$desc2 = removeslashes($existingTag->desc);
									if ( strcmp($desc1, $desc2) )
										$foundDifference = true;
								}
								unset($incomingTag);
								if ($foundDifference) 
									break;
							}

							if (!$foundDifference) {
								$incomingTags = [];
								$existingTags = [];
								foreach($info->lifestyles as $tag) {
									$incomingTags[$tag->id] = $tag; unset($tag);
								}
								foreach($amData->lifestyles as $tag) {
									$existingTags[$tag->id] = $tag; unset($tag);
								}
								// now go through them
								foreach($incomingTags as $incomingTag) {
									if (!isset($existingTags[$incomingTag->id]))
										$foundDifference = true;
									else {
										$existingTag = $existingTags[$incomingTag->id];
										if ($incomingTag->used != $existingTag->used)
											$foundDifference = true;
									}
									if (!$foundDifference) {
										$desc1 = removeslashes($incomingTag->desc);
										$desc2 = removeslashes($existingTag->desc);
										if ( strcmp($desc1, $desc2) )
											$foundDifference = true;
									}
									unset($incomingTag);
									if ($foundDifference) 
										break;
								}
							}

							unset($incomingTags, $existingTags);

							if ($foundDifference) {
								$needUpdate = true;
								$allExistingMetas[SELLER_AGENT_MATCH_DATA] = $info;
							}
						} // if ($needProcessAmData)


						if ($amOrder &&
							count($amOrder->item)) {
							$specialties = [];
							foreach($info->specialties as $tag) {
								$specialties[$tag->id] = $tag; unset($tag);
							}
							$lifestyles = [];
							foreach($info->lifestyles as $tag) {
								$lifestyles[$tag->id] = $tag; unset($tag);
							}
							$gotNewUsed = false;
							foreach($amOrder->item as $item) {
								$item = (object)$item;
								$item->type = intval($item->type);
								$item->specialty = intval($item->specialty);
								if ($item->type == 0 ||
									$item->type == 1) {
									$item->type = ORDER_AGENT_MATCH; // fix type messed up in bad update-author code! 8/25/2016
								}
								if (intval($item->type) != ORDER_AGENT_MATCH)
									continue;
								if (intval($item->mode) == ORDER_BOUGHT) {
									if (array_key_exists($item->specialty, $specialties) ) {
										if ($specialties[$item->specialty]->used != 1) {
											$gotNewUsed = true;
											$info->specialties[$item->specialty]->used = 1;
										}
									}
									elseif (array_key_exists($item->specialty, $lifestyles) )
										if ($lifestyles[$item->specialty]->used != 1) {
											$gotNewUsed = true;
											if (isset($info->lifestyles[$item->specialty]))
												$info->lifestyles[$item->specialty]->used = 1;
											else {
												$this->log("updateSellerMeta - !!!! - lifestyle:$item->specialty is not set in the meta data!");
												$missingTag = $this->getClass('Tags')->get((object)['where'=>['id'=>$item->specialty]]);
												if (!empty($missingTag)) {
													$info->lifestyles[$item->specialty] = (object)['id'=>$item->specialty,
																									'tag'=>$missingTag[0]->tag,
																									'type'=>$missingTag[0]->type,
																									'used'=>1,
																									'desc'=>''];
												}
												else
													$this->log("updateSellerMeta - !*!*!*! - lifestyle:$item->specialty could not be found in the tables");
											}
										}
								}
							}
							unset($specialties, $lifestyles);
							if ($gotNewUsed) {
								$needUpdate = true;
								$allExistingMetas[SELLER_AGENT_MATCH_DATA] = $info;
							}
						}

						// now process tags
						foreach($info->specialties as $tag) {
							if ($tag->used) 
								$tags[$tag->id] = $tag->id;
							unset($tag);
						}
						foreach($info->lifestyles as $tag) {
							if ($tag->used) 
								$tags[$tag->id] = $tag->id;
							unset($tag);
						}

						$tagDifference = array_diff($tags, $existingUsedTags);
						break;

					case SELLER_MODIFIED_PROFILE_DATA:
						foreach($info as $i=>$value) {
							if (gettype($value) != 'array')
								$info->$i = intval($value);
							unset($value);
						}
						$modData = $allExistingMetas[SELLER_MODIFIED_PROFILE_DATA];
						foreach($modData as $i=>$value) {
							if (gettype($value) != 'array')
								$info->$i = intval($value);
							unset($value);
						}
						$updateModData = false;
						foreach($info as $id=>$mod) {
							if (!isset($modData->$id)) {
								$updateModData = true;
								$break = true;
							}
							else if (gettype($mod) == 'array') {
								if ( ($updateModData = count( array_diff($modData->$id, $mod) ) > 0) ) {
									$this->log('updateSellerMeta - SELLER_MODIFIED_PROFILE_DATA array is different');
									$break = true;
								}
							}
							else if ($modData->$id != $mod) {
								$updateModData = true;
								$break = true;
							}
							unset($mod);
							if ($break) break;
						}
						if ($updateModData) {
							$needUpdate = true;
							$allExistingMetas[SELLER_MODIFIED_PROFILE_DATA] = $info;
						}
						break;

					case SELLER_PORTAL_MESSAGE:
						$portalMsg = $allExistingMetas[SELLER_PORTAL_MESSAGE];
						if (removeslashes($portalMsg->message) != removeslashes($info->message)) {
							$needUpdate = true;
							$allExistingMetas[SELLER_PORTAL_MESSAGE] = $info;
						}
						break;

					case SELLER_COMPLETED_CARDS:
						foreach($info as $i=>&$value) {
							if (gettype($value) == 'string')
								$info->$i = intval($value);
							elseif ( is_array($value) ||
									 gettype($value) == 'object') {
								$value = (array) $value;
								foreach($value as $j=>$subValue) {
									if (gettype($subValue) == 'string')
										$value[$j] = intval($subValue);
								}
							}
							unset($value);
						}

						$cardMeta = $allExistingMetas[SELLER_COMPLETED_CARDS];
						$updateCardData = false;
						$existing = get_object_vars($cardMeta);
						$incoming = get_object_vars($info);
						if (count($existing) != count($incoming))
							$updateCardData = true;
						else foreach($incoming as $key=>$value)
							if (!isset($existing[$key]))
								$updateCardData = true;
							elseif (gettype($incoming[$key]) == 'integer') {
								if ($existing[$key] != $incoming[$key])
									$updateCardData = true;
							}
							elseif (gettype($incoming[$key]) == 'object' ||
									is_array($incoming[$key])) {
								$incomingAr = (array)$incoming[$key];
								$existingAr = (array)$existing[$key];
								if (count($incomingAr) != count($existingAr))
									$updateCardData = true;
								else foreach($incomingAr as $i=>$subValue)
									if (!isset($existingAr[$i]))
										$updateCardData = true;
									elseif ($subValue != $existingAr[$i])
										$updateCardData = true;
							}

						if ($updateCardData) {
							$needUpdate = true;
							$allExistingMetas[SELLER_COMPLETED_CARDS] = $info;
						}
						break;
				} // end switch()
			} // end else if (fromBasicInfoPage)
			unset($info);
		} // foreach($metas as $j=>$info)

		if ($needUpdate) {
			$metas = [];
			foreach($allExistingMetas as $meta) {
				$metas[] = $meta;
				unset($meta);
			}
			$result = $this->getClass('Sellers')->set([(object)['where'=>['id'=>$seller->id],
																'fields'=>['meta'=>$metas]]]);
			$this->log("updateSellerMeta - sellerId:$seller->id, updating metas was ".($result ? 'successful' : 'failure'));
			$seller->meta = $metas;
			unset($metas);
		}
	} // end updateSellerMeta()
}
new SellerDispatch();
