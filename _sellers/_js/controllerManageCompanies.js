jQuery(document).ready(function($){

	function waitForController() {
		if (!controller) {
			window.setTimeout(waitForController, 250);
			return;
		}
		initController();

	}

	function showSpinner() {
		$('div#overlay-bg .spin-wrap').show();
	}

	function hideSpinner() {
		$('div#overlay-bg .spin-wrap').hide();
	}

	function initController() {
		controller.selectedCompany = 0;
		controller.newCompanyLogo = null;
		controller.logoDZ = null;
		controller.cropImg = null;
		controller.creatingCompany = 1;
		var uploadDir = ''; // could be 'uploaded/' but for now, all logos are just going into the _agency folder (no sub folders)

		$('#companiesDiv .closeDiv').on('click', function() {
			$('div.companiesList').hide();
		})

		$('#companiesDiv #controls #select').on('click', function() {
			controller.setLogo();
			if (controller.selectedCompany == validSeller.association) {
				console.log("Seller is already associated with company id:"+controller.selectedCompany);
				$('div.companiesList').hide();	
				return;
			}

			showSpinner();
			var data = {'association': controller.selectedCompany};
			seller.DBget({	query:'update-author', 
							data: {id: validSeller.id,
								   fields: data}, 
							done:function(d){
								for (var i in data) validSeller[i] = seller.page.profile.info[i] = data[i];
								hideSpinner();
								$('div.companiesList').hide();	
								controller.recreateCompanyList();
								controller.setLogo();
							},
							error:function(x) {
								hideSpinner();
								var msg = typeof x == 'string' ? x :
										  typeof x == 'object' ? JSON.stringify(x) :
										  "Unknown reason";
								$('#companiesDiv #error').html(msg);
								$('#companiesDiv #error').removeClass('hidden');
								// ahtb.alert("Failure saving profile: "+msg);
							}
						});
		})

		$('#companiesDiv #controls #create').on('click', function() {
			$('div#companiesDiv').fadeOut(100, function() {
				$('div#createCompanyDiv').fadeIn(100, function() {
					controller.selectedCompany = 0;
					controller.newCompanyLogo = null;
					controller.creatingCompany = 1;
					controller.prepLogoDropzone();
					$('div#createCompanyDiv [name=url]').val('http://');
					$('div#createCompanyDiv [what=brokerage]').prop('checked', true)
				});
			})
		})

		$('div#createCompanyDiv input').on('keypress', function() {
			var name = $(this).attr('name');
			controller.clearError(name);
		})

		$('div#createCompanyDiv #controls #exit').on('click', function() {
			$('div#createCompanyDiv').fadeOut(100, function() {
				$('div#companiesDiv').fadeIn(100);
			})
		})

		$('div#createCompanyDiv #controls #ok').on('click', function() {
			var name = $('div#createCompanyDiv [name=name]').val();
			if (name.length == 0) {
				controller.setError('name', '          Enter a company name');
				// $('#createCompanyDiv input[name=name]').css('border', '2px solid red');
				// $('#createCompanyDiv #error').html("Enter a company name");
				// $('#createCompanyDiv #error').removeClass('hidden');
				return;
			}

			if (!controller.newCompanyLogo) {
				controller.setError('logo', '          Upload a Logo');
				// $('#createCompanyDiv #error').html("Upload a logo");
				// $('#createCompanyDiv #error').removeClass('hidden');
				return;
			}

			var street_address = '';
			var city = '';
			var state = '';
			var zip = '';
			var contact = '';
			var contact_email = '';
			var phone1 = '';
			var phone2 = '';
			var url = '';
			var type = 1;

			if (agentPageOption.showExtendedCompanyFields) {
				street_address = $('div#createCompanyDiv [name=street_address]').val();
				var locality = $('div#createCompanyDiv [name=city]').val();
				if (locality.length) {
					var parts = locality.split(',');
					city = parts[0];
					if (parts.length == 2)
						state = parts[1].trim();
				}
				zip = $('div#createCompanyDiv [name=zip]').val();
				contact = $('div#createCompanyDiv [name=contact]').val();
				contact_email = $('div#createCompanyDiv [name=contact_email]').val();
				if (contact_email.length &&
					!validateEmail(contact_email)) {
					controller.setError('contact_email', '          Enter a valid email');
					return;
				}
				phone1 = $('div#createCompanyDiv [name=phone1]').val().replace(/[^0-9]/g, '');
				if (phone1.length &&
					!validatePhone(phone1)) {
					controller.setError('phone1', '          Enter a valid phone number');
					return;
				}
				phone2 = $('div#createCompanyDiv [name=phone2]').val().replace(/[^0-9]/g, '');
				if (phone2.length &&
					!validatePhone(phone2)) {
					controller.setError('phone2', '          Enter a valid phone number');
					return;
				}
				url = $('div#createCompanyDiv [name=url]').val();
				if (url.length > 8) {
					if (!validateURL(url)) {
						controller.setError('url', '          Enter a valid URL beginning with http://');
						return;
					}
				}
				else
					url = '';
			}
			type = $('div#createCompanyDiv input[name=type]:checked').val();
			showSpinner();
			seller.DBget({	query:'create-company', 
							data:{  id: validSeller.id,
									company: name,
									street_address: street_address,
									city: city,
									state: state,
									zip: zip,
									contact: contact,
									contact_email: contact_email,
									phone: phone1,
									phone2: phone2,
									url: url,
									type: type,
									logo: controller.newCompanyLogo,
									creating: controller.creatingCompany
							},
							done:function(data){
								hideSpinner();
								console.log("created new company with id:"+data.id); 
								validSeller.association = data.id;
								controller.page.profile.info.association = data.id;
								controller.selectedCompany = data.id;
								// add new company to association list
								if (typeof data.updateOnly == 'undefined') {// then was added, not just updated
									console.log("adding company:"+data.id+" to associations list");
									associations[associations.length] = data.company;
								}
								else
									controller.updateAssociations(data.company);

								controller.recreateCompanyList();
								controller.setLogo();
								$('div#createCompanyDiv').fadeOut(100, function() {
									if (controller.creatingCompany) {
										$('div.companiesList').hide();	
										$('div#companiesDiv').show();
									}
									else
										$('div#companiesDiv').fadeIn(100);
								})
							},
							error:function(x) {
								hideSpinner();
								var msg = typeof x == 'string' ? x :
										  typeof x == 'object' ? JSON.stringify(x) :
										  "Unknown reason";
								controller.setError(null, "Failed to create company: "+msg);
								// $('#createCompanyDiv #error').html("Failed to create company: "+msg);
								// $('#createCompanyDiv #error').removeClass('hidden');
							}
					}); 
			
		})

		controller.setupAutocomplete($('div#createCompanyDiv [name=city]'));
		$('ul.ui-autocomplete').css('z-index','1000001');

		if (agentPageOption.allowAdvancedAgentCompanyEdits) {
			$('div#companiesDiv').css('width', '380px');
			// $('div#companiesDiv .first').css('width', '50%');
			// $('div#companiesDiv .second').css('width', '25%');
			// $('div#companiesDiv .third').css('width', '25%');
		}

		$('div#editLogo #controls #exit').on('click', function() {
			controller.hideLogoEditor();
		})

		$('div#editLogo #controls #save').on('click', function() {
			controller.saveEditedLogo();
		})

		function scrollError() {
			if ($('#createCompanyDiv #error').hasClass('hidden'))
				return;

			var msg = $('#createCompanyDiv #error').html();
			var char = msg.charAt(0);
			msg = msg.slice(1) + char;
			$('#createCompanyDiv #error').html(msg);
			window.setTimeout( scrollError, 300);
		}

		controller.setError = function(what, msg) {
			if (what) {
				var ele = $('#createCompanyDiv input[name='+what+']');
				if (ele.length)
					ele.css('border', '2px solid red');
			}
			$('#createCompanyDiv #error').html(msg);
			$('#createCompanyDiv #error').removeClass('hidden');
			if (what)
				scrollError();
		}

		controller.clearError = function(what) {
			if (typeof what != 'undefined' && 
				what) {
				var ele = $('#createCompanyDiv input[name='+what+']');
				if (ele.length) {
					ele.css('border', '2px solid lightgray');
					ele.css('border-style', 'inset');
				}
			}
			$('#createCompanyDiv #error').addClass('hidden');
		}

		controller.findCompany = function(id) {
			for(var i in associations)
				if (associations[i].id == id)
					return associations[i];

			return null;
		}

		controller.updateAssociations = function(company) {
			for(var i in associations)
				if (associations[i].id == company.id) {
					associations[i] = company;
					break;
				}
		}

		controller.hideLogoEditor = function() {
			$('div#editLogo').fadeOut(100, function() {
				controller.jcrop = null;
				controller.cropImg = null;
				$('div#createCompanyDiv').fadeIn(100);
			})
		}

		controller.setLogo = function(initialization) {
			var company = controller.findCompany(controller.selectedCompany);
			if (!company) {
				console.log("Cannot find company for id:"+controller.selectedCompany)
				return;
			}
			controller.newCompanyLogo = company.logo;
			ah_local.agency_url = company.url;
			if (typeof initialization == 'undefined') {
				$(".agent-info .agency-logo").css('background-image', 'url('+ah_local.tp+"/_img/_agency/"+controller.newCompanyLogo+')');
				var baseLogoHeight = 50;
				var height = baseLogoHeight; // parseInt( $(".agent-info .agency-logo").css('width') );
				var width = ((parseFloat(company.logo_x_scale) * height) / 1);
				$(".agent-info .agency-logo").css('height', baseLogoHeight+"px");
				$(".agent-info .agency-logo").css('width', width+"px");
			}
			$(".agent-info .agency-logo").css('cursor', (!ah_local.agency_url || ah_local.agency_url.length == 0 ? 'none' : 'pointer'));
			$('.have-portal .company #current').removeClass('hidden');
			$('.have-portal .company img').attr('src', ah_local.tp+"/_img/_agency/"+controller.newCompanyLogo);
		}

		controller.editCompanyDetail = function(id) {
			$('div#companiesDiv').fadeOut(100, function() {
				$('div#createCompanyDiv').fadeIn(100, function() {
					controller.selectedCompany = id;
					var company = controller.findCompany(id);
					if (!company) {
						controller.prepLogoDropzone();
						$('div#createCompanyDiv [name=url]').val('http://');
						controller.setError(null,"Failed to find company with id:"+id);
						return;
					}
					controller.creatingCompany = 0;
					controller.newCompanyLogo = company.logo;
					$('#createCompanyDiv [name=name]').val(company.company);
					$('#createCompanyDiv [name=street_address]').val(company.street_address);
					var locality = (company.city && company.city.length ? company.city+", " : '')+(company.state && company.state.length ? company.state : '');
					$('div#createCompanyDiv [name=city]').val(locality);
					$('div#createCompanyDiv [name=zip]').val(company.zip);
					$('div#createCompanyDiv [name=contact]').val(company.contact);
					$('div#createCompanyDiv [name=email]').val(company.contact_email);
					$('div#createCompanyDiv [name=phone1]').val( controller.fixPhone(company.phone) );
					$('div#createCompanyDiv [name=phone2]').val( controller.fixPhone(company.phone2) );
					$('div#createCompanyDiv input[type=radio][value='+company.type+']').prop('checked', true);
					var url = company.url && company.url.length ? company.url : '';
					if (url.indexOf('http') == -1)
						url = 'http://'+url;
					$('div#createCompanyDiv [name=url]').val(url);
					controller.prepLogoDropzone();
				});
			})
		}

		controller.fixPhone = function(phone) {
			if (!phone || phone.length == 0)
				return '';
			var x = typeof phone == 'number' ? phone.toString() : phone;
			if (x.length == 10) 
				x = '('+x.substr(0, 3)+') '+x.substr(3,3)+'-'+x.substr(6);
			else if (x.length == 11) 
				x = x.substr(0,1)+' ('+x.substr(1,3)+') '+x.substr(4,3)+'-'+x.substr(7);
			else
				console.log("phone len is "+x.length);

			return x;
		}

		controller.recreateCompanyList = function() {
			var h = '';
			for(var i in associations) {
				var company = associations[i];
				h += '<li for="'+company.id+'">'+
						'<div class="container" id="company" for="'+company.id+'" >' +
							'<div class="first">'+
								'<a href="javascript:controller.selectCompany('+company.id+');">' +
									'<img src="'+ah_local.tp+"/_img/_agency/"+company.logo+'"></img>'+
								'</a>'+
							'</div>'+
							'<div class="second" id="more"><a href="javascript:controller.showCompanyDetail('+company.id+');">More<span class="entypo-down-open-big"/></a></div>'+
							'<div class="second hidden" id="closeDetail"><a href="javascript:controller.closeCompanyDetail('+company.id+');" >Close<span class="entypo-up-open-big"/></a></div>';
						if (agentPageOption.allowAdvancedAgentCompanyEdits) {
							if (company.id == validSeller.association)
						h+= '<div class="third" id="closeDetail"><a href="javascript:controller.editCompanyDetail('+company.id+');" >Edit</a></div>';
							else
						h+= '<div class="third" id="closeDetail"></div>';
						}
						h+= '<div class="details" for="'+company.id+'" style="display: none;">';
							h+= '<span class="detail" id="name">Name: '+company.company+'</span>';
						if (company.street_address && company.street_address.length)
							h+= '<span class="detail" id="address">Address: '+company.street_address+'</span>';
						if (company.city && company.city.length)
							h+= '<span class="detail" id="locality">Locality: '+company.city+', '+company.state+' '+company.zip+'</span>';
						if (company.contact && company.contact.length)
							h+= '<span class="detail">Contact: '+company.contact+'</span>';
						if (company.contact_email && company.contact_email.length)
							h+= '<span class="detail">Email: '+company.contact_email+'</span>';
						if (company.phone && company.phone.length)
							h+= '<span class="detail">Phone1: '+controller.fixPhone(company.phone)+'</span>';
						if (company.phone2 && company.phone2.length)
							h+= '<span class="detail">Phone2: '+controller.fixPhone(company.phone2)+'</span>';
						if (company.url && company.url.length)
							h+= '<span class="detail">WebSite: '+company.url+'</span>';
						h+= '</div>'+
						'</div>'+
					'</li>';
			}
			$('div#companies ul').html(h);
		}

		controller.prepLogoDropzone = function() {
			/* Dropzone and logo */
			if (controller.logoDZ)
				return;

			$('div#createCompanyDiv .logo').html('<form id="dropzoneLogo" class="dropzone"></form>'+
													'<div class="company-logo">'+
														'<img src="'+ah_local.tp+'/_img/_authors/272x272/_blank.jpg" />'+
														'<div class="overlay">'+
															'<a class="edit-cropping"><p><span class="entypo-popup"></span> Edit Cropping</p></a>'+
															// '<a class="upload-photo"><p><span class="entypo-upload-cloud"></span> Upload New</p></a>'+
														'</div>'+
														'<div id="deleteDiv"><button id="delete-photo">Delete photo</button></div>'+
													'</div>');

			controller.logoDZ = new Dropzone(".logo #dropzoneLogo", {
				acceptedFiles:'image/*',
				url: ah_local.tp+'/_classes/Image.class.php',
				maxFiles: 1,
				dictDefaultMessage: '<p>Upload Logo Image</p><span class="entypo-upload-cloud"></span><button id="dzBrowse">Browse</button>',
				dictFallbackText: 'Please choose your logo',
				headers:{"image_dir":"_agency/"+uploadDir},
				init: function(){
					setCookie('IMAGE_DIR', '_agency/'+uploadDir, 1);
					$('.dz-message button').on('click', function(e){e.preventDefault()});
					$('.logo #dropzoneLogo').hover(function(){
						$('.logo .dz-message button, .logo .dz-message span span, .logo .dz-message p').addClass('hover');
					},function(){
						$('.logo .dz-message button, .logo .dz-message span span, .logo .dz-message p').removeClass('hover');
					});
					this.on('maxfilesexceeded', function(){ 
						controller.setError(null, 'You can only upload one image for your company.');
					});
					this.on('error', function(file, responseText) {
						var x = typeof responseText == 'string' ? responseText : $.parseJSON(responseText);
						controller.setError(null, x);
					});
					this.on("success", function(file, responseText){
						var x = $.parseJSON(responseText);
						controller.updateAssociationLogo(x);
						controller.clearError();
						showSpinner();
						controller.setAssociationLogo(x.data[0]);
					});
				}
			});

			$('.logo div.dz-fallback input[name="file"]').on('change', controller.prepareUploadLogo);
			$('.logo div.dz-fallback input[value="Upload!"]').on('click', controller.uploadLogo);

			if (typeof controller.logoDZ.disable != 'function')
				$('.logo #dropzoneLogo').append('<div class="spin-wrap"><div class="spinner sphere"></div></div>');

			if (controller.newCompanyLogo) { 
				$('.logo #dropzoneLogo').hide(); 
				if (typeof controller.logoDZ.disable == 'function') 
					controller.logoDZ.disable(); 
				$('div.company-logo img').attr('src', ah_local.tp+'/_img/_agency/'+controller.newCompanyLogo);
				$('div.company-logo').show();
			}
			else $('div.company-logo').hide();

			$('div.company-logo .overlay a').on('click', function(){
				if ($(this).hasClass('upload-photo')) {
					controller.removeLogo( function() {
						if (typeof controller.logoDZ.enable == 'function') controller.logoDZ.enable();
						$('.company-logo').fadeOut(250,function(){
							$('.logo #dropzoneLogo').fadeIn(250);
							// var ele = $('#dzBrowse');
							// ele.trigger('click');
							var ele = controller.logoDZ.hiddenFileInput;
							if (ele.onclick) {
							   ele.onclick();
							} else if (ele.click) {
							   ele.click();
							}
							else console.log("no hiddenFileInput click method");
						});
					})
				}
				else if ($(this).hasClass('edit-cropping')) controller.editCropLogo();
			});

			$('div.company-logo #delete-photo').on('click', function() {
				if (!controller.newCompanyLogo) {
					ahtb.alert("No logo on file!");
					return;
				}
				controller.removeLogo();
			})
		}

		controller.removeLogo = function(callback) {
			showSpinner();
			seller.DBget({	query:'remove-logo', 
							data:{ id: controller.selectedCompany,
								   name: controller.newCompanyLogo,
								   imgDir: 'agency'
							},
							done:function(data){
								hideSpinner();
								console.log(typeof data == 'string' ? data : "Deleted logo"); 
								$('div.company-logo').hide();
							  	if (typeof controller.logoDZ.files != 'undefined' &&
							  		controller.logoDZ.files.length)
							  		if (typeof controller.logoDZ.removeFile == 'function') controller.logoDZ.removeFile(controller.logoDZ.files[0]);
							  	$('.logo #dropzoneLogo').show(); if (typeof controller.logoDZ.enable == 'function') controller.logoDZ.enable();
							  	controller.newCompanyLogo = null;
							  	if (typeof callback == 'function')
							  		callback();
							},
							error:function(x) {
								hideSpinner();
								var msg = typeof x == 'string' ? x :
										  typeof x == 'object' ? JSON.stringify(x) :
										  "Unknown reason";
								controller.setError(null, "Failure removing logo: "+msg);
							}
					}); 
		}

		controller.applyCrop = function(x) {
			showSpinner();
			$.post(	ah_local.tp+'/_classes/Image.class.php', 
					{query:'resize',
					 data:{
						file: controller.newCompanyLogo,
						src_path: '_agency/',
						src_width: x.srcBounds[0], src_height: x.srcBounds[1],
						dst_path: '_agency/cropped/',
						dst_x: x.dstBounds.x, dst_y: x.dstBounds.y,
						dst_x2: x.dstBounds.x2, dst_y2: x.dstBounds.y2,
						logo: 1
					}},function(){},'json')
				.done(function(d){
					if (d.status == 'OK'){
						if (controller.selectedCompany) {
							var company = controller.findCompany(controller.selectedCompany);
							company.logo = d.data;
							company.logo_x_scale = (x.dstBounds.x2-x.dstBounds.x)/(x.dstBounds.y2-x.dstBounds.y);
						}
						controller.updateAssociationLogo(d);
						controller.setLogo();
						if (controller.selectedCompany == 0) {
							hideSpinner();
							controller.hideLogoEditor();
							return;
						}
						controller.setAssociationLogo(d.data);
					}
					else {
						hideSpinner();
						console.log(d);
						var msg = typeof d == 'string' ? d :
								  typeof d == 'object' ? JSON.stringify(d) :
								  "Unknown reason";
						$('#editLogo #error').html(msg);
						$('#editLogo #error').removeClass('hidden');
					}
				})
				.error(function(d){
					hideSpinner();
					console.log(d);
					var msg = typeof d == 'string' ? d :
							  typeof d == 'object' ? JSON.stringify(d) :
							  "Unknown reason";
					$('#editLogo #error').html(msg);
					$('#editLogo #error').removeClass('hidden');
				})
		}

		// this assumes showSpinner() was called and active
		controller.setAssociationLogo = function(newLogo) {
			if (!controller.selectedCompany) {
				hideSpinner();
				return;
			}

			seller.DBget({query:'update-association-logo', 
						  data:{id: controller.selectedCompany,
						  		logo: newLogo}, 
						  done:function(){
						  	hideSpinner();
							controller.hideLogoEditor();
						  },
						  error:function(x) {
						  	hideSpinner();
							var msg = typeof x == 'string' ? x :
									  typeof x == 'object' ? JSON.stringify(x) :
									  "Unknown reason";
							$('#editLogo #error').html(msg);
							$('#editLogo #error').removeClass('hidden');
						  }
						});
		}

		controller.saveEditedLogo = function() {
			var dstBounds = controller.jcrop.tellSelect();
			if (dstBounds.w == 0 || dstBounds.h == 0) {
				var msg = 'Please select an area to crop before hitting save.';
				$('#editLogo #error').html(msg);
				$('#editLogo #error').removeClass('hidden');
			}
			else 
				controller.applyCrop({ dstBounds: dstBounds, srcBounds: controller.jcrop.getBounds() });
		}

		controller.printCropLogo = function() {
			img = controller.cropImg;
			var margin = 150;
			var i = img.width;
			while (i*img.height/img.width > 500) i-=10;
			var x = { height: (i*img.height/img.width), width: i };
			$('div#editLogo #editor').css('width', x.width+'px');
			$('div#editLogo #editor').css('height', x.height+'px');
			$('div#editLogo #editImage').attr('src', img.src);
			$('div#editLogo #title').addClass('hidden');
			$('div#editLogo #controls').prop('disabled', false);
			$('div#editLogo #editor').removeClass('hidden');

			$('div#editLogo #editImage').Jcrop({
												// aspectRatio: 1.0,
												bgFade: true,
												bgOpacity: .3,
												setSelect: [ 0, 0, 100, 300 ],
												boxHeight: x.height,
												boxWidth: x.width
											},function(){ 
												controller.jcrop = this; 
											});
		}

		controller.editCropLogo = function() {
			// setting for initial display
			$('div#editLogo #title').removeClass('hidden');
			$('div#editLogo #editor').addClass('hidden');
			$('#editLogo #error').addClass('hidden');
			$('div#editLogo #controls').prop('disabled', true);
			$('div#editLogo #controls #save').prop('disabled', false);

			$('div#createCompanyDiv').fadeOut(100, function() {
				$('div#editLogo').fadeIn(100, function() {
					var img = new Image();
					img.onerror = function(){ 
						$('div#editLogo #controls #save').prop('disabled', true);
						$('div#editLogo #controls').prop('disabled', false);
						$('#editLogo #error').html('Sorry, an error occurred while loading your image.');
						$('#editLogo #error').removeClass('hidden');
					}
					img.onload = function(x){ controller.cropImg = img; controller.printCropLogo(); }
					img.src =ah_local.tp+'/_img/_agency/'+controller.newCompanyLogo;
				})
			})
		}

		controller.prepareUploadLogo = function(e) {
			files = e.target.files;
		}

		controller.uploadLogo = function(e) {
			setCookie("IMAGE_DIR", "_agency/"+uploadDir, 1);
			if (e) {
				e.stopPropagation(); // Stop stuff happening
			    e.preventDefault(); // Totally stop stuff happening
			}
		 		 
		    // Create a formdata object and add the files
			formData = new FormData();
			formData.append ('image_dir','_agency/'+uploadDir);
			$.each(files, function(key, value)
			{
				console.log(key, value);
				formData.append(key, value);
			});

			showSpinner();

			$.ajax({
		        url: ah_local.tp+'/_classes/Image.class.php',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				error: function($xhr, $status, $error){
					controller.clearError();
					hideSpinner();
					if ( !$xhr.responseText &&
						 !$error)
						return; // ignore it

					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
			  		hideSpinner();
				  	if ( data == null || data.status == null) 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else {
				  		controller.updateAssociationLogo(data);
				  		controller.setLogo();
				  		// ahtb.alert(data.data, {width: 450, height: 150});
				  	}
				}
			})
		}

		controller.updateAssociationLogo = function(x) {
			controller.clearError();
			if (x.status != 'OK') ahtb.alert(typeof x.data == 'string' ? '<p>'+x.data+'</p>' : '<p>Unable to upload image.</p>');
			else {
				controller.newCompanyLogo = typeof x.data == 'string' ? x.data : x.data[0];
				if (typeof controller.logoDZ.disable == 'function') controller.logoDZ.disable();
				$('.logo #dropzoneLogo').hide();
				$('div.company-logo img').attr('src', ah_local.tp+'/_img/_agency/'+controller.newCompanyLogo);
				$('div.company-logo').show();
			}
				// ahtb.loading('Saving logo...',{
				// opened: function(){
				// 	controller.DBget({	query:'update-association-logo', 
				// 					data:{ id: validSeller.id,
				// 							fields: {photo:x.data[0]}
				// 						  }, 
				// 					done:function(d){
				// 						if (typeof dz.disable == 'function') dz.disable();
				// 						if (typeof d == 'object') {
				// 							seller.page.profile.info = d.seller;
				// 							dashboardCards = d.dashboardCards;
				// 						}
				// 						else
				// 							seller.page.profile.info.photo = x.data[0];
				// 						$('.sellerimg #dropzone').hide();
				// 						$('div.seller-photo img').attr('src', ah_local.tp+'/_img/_authors/272x272/'+x.data[0]);
				// 						$('div.header-login img').attr('src', ah_local.tp+'/_img/_authors/55x55/'+x.data[0]);
				// 						$('div.seller-photo').show();
				// 						ahtb.close();
				// 					},
				// 					error:function(x) {
				// 						var msg = typeof x == 'string' ? x :
				// 								  typeof x == 'object' ? JSON.stringify(x) :
				// 								  "Unknown reason";
				// 						ahtb.alert("Failure saving image: "+msg);
				// 					}
				// 				});
				// }
			// });
		}

		/* Phone numbers */
		$('[name=phone1], [name=phone2]').mask('(999) 000-0000');

		controller.showCompanyDetail = function(id) {
			$('#companies .container[for='+id+'] #more').addClass('hidden');
			$('#companies .container[for='+id+'] #closeDetail').removeClass('hidden');
			$('#companies .container[for='+id+'] .details').css('display','table').show();
		}

		controller.closeCompanyDetail = function(id) {
			$('#companies .container[for='+id+'] #closeDetail').addClass('hidden');
			$('#companies .container[for='+id+'] #more').removeClass('hidden');
			$('#companies .container[for='+id+'] .details').hide();
		}

		controller.selectCompany = function(id) {
			if (controller.selectedCompany)
				$('#companies li[for='+controller.selectedCompany+']').removeClass('selected');
			controller.selectedCompany = id;
			$('#companies li[for='+controller.selectedCompany+']').addClass('selected');
			$('#companiesDiv #controls #select').prop('disabled', false);
		}

		controller.selectedCompany = validSeller.association;
	}

	waitForController();
})