jQuery(document).ready(function($){

	function waitForController() {
		if (!controller) {
			window.setTimeout(waitForController, 250);
			return;
		}
		initController();

	}

	function initController() {
		controller.captureSlide = sellerLeadCaptureDivTitleList[0];
		$('div.capture-slide').css('left', '100%');

		var domId = 'div.capture-slide select';
		$(domId).on('change', function() {
			var id = $(this).attr('id');
			var value = parseInt( $(this).find(":selected").attr("value") );
			if (id == 'viewListing') {
				if (value && 
					typeof captureOptions.viewListingLeadIn != 'undefined' && 
					captureOptions.viewListingLeadIn.captureMode) {
					captureOptions.viewListingLeadIn.captureMode = PortalUserCaptureMode.CAPTURE_NONE;
					domId = 'select#viewListingLeadIn option[value='+captureOptions.viewListingLeadIn.captureMode+']';
					var ele = $(domId);
					ele.prop('selected', true);
				}
			}
			captureOptions[id].captureMode = value;
		});

		controller.startLeadCaptureSlides = function() {
		 	var whichSlide = 'div.capture-slide[id='+controller.captureSlide+']';
		 	var slideHeight = $(whichSlide).innerHeight();
	        var slideHeight = (slideHeight) / 2;
	        $(whichSlide).css('margin-top', '-'+slideHeight+'px');
			$('div.capture-slide[id='+controller.captureSlide+']').animate({ left: "0%" },
																		   { queue: false, 
																			 duration: 100,
																			 done: function() {
																			 }});
		}
	
		controller.onNoCapture = function() {
			$('div.leadCaptureDivs').hide();
			$('div.capture-slide[id='+controller.captureSlide+']').animate({ left: "100%" },
																		   { queue: false, 
																			 duration: 100, 
																			 done: function() {}
																			});
			controller.captureSlide = sellerLeadCaptureDivTitleList[0];
		}

		controller.onBack = function(where) {
			$('div.capture-slide[id='+controller.captureSlide+']').animate({ left: "-150%" },
																		   { queue: false, 
																			 duration: 100, 
																			 done:function(){
																			 	controller.captureSlide = where;
																			 	var whichSlide = 'div.capture-slide[id='+controller.captureSlide+']';
																			 	var predefinedQuizDivHeight = $(whichSlide).innerHeight();
																		        var predefinedQuizDivHeightHalf = (predefinedQuizDivHeight) / 2;
																		        $(whichSlide).css('margin-top', '-'+predefinedQuizDivHeightHalf+'px');
																			 	$(whichSlide).animate({ left: "0%" },
								 																   	  { queue: false, 
																									    duration: 100, 
																									    done:function(){
								 																	    }
								 																      }); 
																		   }});
		}

		controller.onNext = function(where) {
			$('div.capture-slide[id='+controller.captureSlide+']').animate({ left: "100%" },
																		   { queue: false, 
																			 duration: 100, 
																			 done:function(){
																			 	controller.captureSlide = where;
																			 	var whichSlide = 'div.capture-slide[id='+controller.captureSlide+']';
																			 	var predefinedQuizDivHeight = $(whichSlide).innerHeight();
																		        var predefinedQuizDivHeightHalf = (predefinedQuizDivHeight) / 2;
																		        $(whichSlide).css('margin-top', '-'+predefinedQuizDivHeightHalf+'px');
																			 	$(whichSlide).animate({ left: "0%" },
								 																   	  { queue: false, 
																									    duration: 100, 
																									    done:function(){
																									    	if (controller.captureSlide == 'capture_options')
																									    		controller.setup();
								 																	    }
								 																      }); 
																		   }});
		}

		controller.setup = function() {
			var domId = 'select#quizLoad option[value='+captureOptions.quizLoad.captureMode+']';
			var ele = $(domId);
			ele.prop('selected', true);

			domId = 'select#cityScroll option[value='+captureOptions.cityScroll.captureMode+']';
			ele = $(domId);
			ele.prop('selected', true);

			domId = 'select#listingScroll option[value='+captureOptions.listingScroll.captureMode+']';
			ele = $(domId);
			ele.prop('selected', true);

			if (typeof captureOptions.viewListingLeadIn != 'undefined' &&
				captureOptions.viewListingLeadIn.captureMode == 2) 
				captureOptions.viewListing.captureMode = 0;
			domId = 'select#viewListing option[value='+captureOptions.viewListing.captureMode+']';
			ele = $(domId);
			ele.prop('selected', true);

			domId = 'input#quizLoad';
			ele = $(domId);
			ele.val( captureOptions.quizLoad.delay.toString() );

			domId = 'input#cityScroll';
			ele = $(domId);
			ele.val( captureOptions.cityScroll.page.toString() );

			domId = 'input#listingScroll';
			ele = $(domId);
			ele.val( captureOptions.listingScroll.page.toString() );

			domId = 'div#viewListingLeadIn';
			ele = $(domId);
			if (!ele.length &&
				typeof captureOptions.viewListingLeadIn != 'undefined') {
				var h = '<div class="trigger-option" id="viewListingLeadIn"><span id="title">ViewListingLeadIn:</span>'+
							'<span id="subtitle">mode</span><select id="viewListingLeadIn" class="leadin">';
							var Modes = {'Not Used':0,'Optional':1,'Hard Stop':2};
							for(mode in Modes)
							h+= '<option value="'+Modes[mode]+'" '+(Modes[mode] == captureOptions.viewListingLeadIn.captureMode ? 'selected' : '')+'>'+mode+'</option>';
						h+=	'</select>' +
							'<div class="leadin"><span id="subtitle">title</span><input type="text" id="viewListingLeadIn" what="title"  value="'+captureOptions.viewListingLeadIn.title+'"></input></div>'+
							'<div class="leadin"><span id="subtitle" >message</span><input type="text" id="viewListingLeadIn" what="message"  value="'+captureOptions.viewListingLeadIn.message+'"></input></div>'+
						'</div>';
				ele = $('div.trigger-option#viewListing');
				ele.after(h);

				var domId = 'div.capture-slide select#viewListingLeadIn';
				$(domId).on('change', function() {
					var id = $(this).attr('id');
					var value = parseInt( $(this).find(":selected").attr("value") );
					if (value && 
						captureOptions.viewListing.captureMode) {
						captureOptions.viewListing.captureMode = PortalUserCaptureMode.CAPTURE_NONE;
						domId = 'select#viewListing option[value='+captureOptions.viewListing.captureMode+']';
						ele = $(domId);
						ele.prop('selected', true);
					}
					captureOptions.viewListingLeadIn.captureMode = value;
				});
			}
		}

		controller.onSetCapture = function() {
			var domId = 'select#quizLoad';
			var value = parseInt( $(domId).find(":selected").attr("value") );
			captureOptions.quizLoad.captureMode = value;

			domId = 'input#quizLoad';
			value = parseInt( $(domId).val() );
			captureOptions.quizLoad.delay = value;
			captureOptions.quizLoad.mobileDelay = value;

			domId = 'select#cityScroll';
			value = parseInt( $(domId).find(":selected").attr("value") );
			captureOptions.cityScroll.captureMode = value;
			domId = 'input#cityScroll';
			value = parseInt( $(domId).val() );
			captureOptions.cityScroll.page = value;

			domId = 'select#listingScroll';
			value = parseInt( $(domId).find(":selected").attr("value") );
			captureOptions.listingScroll.captureMode = value;
			domId = 'input#listingScroll';
			value = parseInt( $(domId).val() );
			captureOptions.listingScroll.page = value;

			domId = 'select#viewListing';
			value = parseInt( $(domId).find(":selected").attr("value") );
			captureOptions.viewListing.captureMode = value;

			domId = 'input#viewListingLeadIn[what=title]';
			var ele = $(domId);
			if (ele.length)
				captureOptions.viewListingLeadIn.title = ele.val();

			domId = 'input#viewListingLeadIn[what=message]';
			ele = $(domId);
			if (ele.length)
				captureOptions.viewListingLeadIn.message = ele.val();


			var data = {query: 'update-one-option',
						url: ah_local.tp+'/_admin/ajax_options.php',
						data: {option: captureOptions,
							   name: 'QuizResultsPortalUserCaptureOptions'},
						done: function(d) {
							console.log(d);
						}
					   };
			controller.DBget(data);

			controller.onNoCapture();
		}

		controller.onChooseOptimal = function() {
			agentOptions.nameMode = PortalUserCaptureMode.CAPTURE_NONE;
			var data = {query: 'update-one-option',
						url: ah_local.tp+'/_admin/ajax_options.php',
						data: {option: agentOptions,
							   name: 'PortalLandingAgentOptions'},
						done: function(d) {
							console.log(d);
						}
					   };
			controller.DBget(data);

			captureOptions.quizLoad.captureMode = PortalUserCaptureMode.CAPTURE_OPTIONAL
			captureOptions.quizLoad.delay = 8000;
			captureOptions.quizLoad.mobilDelay = 8000;
			captureOptions.cityScroll.captureMode = PortalUserCaptureMode.CAPTURE_NONE;
			captureOptions.listingScroll.captureMode = PortalUserCaptureMode.CAPTURE_NONE;
			captureOptions.viewListing.captureMode = PortalUserCaptureMode.CAPTURE_NONE;
			if (typeof captureOptions.viewListingLeadIn == 'undefined')
				captureOptions.viewListingLeadIn = {};
			captureOptions.viewListingLeadIn.captureMode = PortalUserCaptureMode.CAPTURE_MUST;
			captureOptions.viewListingLeadIn.title = "Please sign up";
			captureOptions.viewListingLeadIn.message = "By signing up, you can save your search profiles, and have %agent% contact you if there are new listings that you may like.";
			var data = {query: 'update-one-option',
						url: ah_local.tp+'/_admin/ajax_options.php',
						data: {option: captureOptions,
							   name: 'QuizResultsPortalUserCaptureOptions'},
						done: function(d) {
							console.log(d);
						}
					   };
			controller.DBget(data);

			controller.onNoCapture();
		}

		controller.onPassword = function(mode) {
			if (mode) {
				agentOptions.phoneMode = PortalUserCaptureMode.CAPTURE_MUST;
				agentOptions.emailMode = PortalUserCaptureMode.CAPTURE_MUST;
			}
			else
				agentOptions.phoneMode = PortalUserCaptureMode.CAPTURE_OPTIONAL;

			var data = {query: 'update-one-option',
						url: ah_local.tp+'/_admin/ajax_options.php',
						data: {option: agentOptions,
							   name: 'PortalLandingAgentOptions'},
						done: function(d) {
							console.log(d);
						}
					   };
			controller.DBget(data);

			controller.onNoCapture();
		}
	}

	waitForController();
})