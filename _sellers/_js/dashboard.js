function setSellerDashBoardCards(seller, cardMeta) {

    // ---- Gather Info ---- //
    var metas = seller.meta;
    cardMeta = typeof cardMeta == 'undefined' ? {action: SellerMetaFlags.SELLER_COMPLETED_CARDS} : cardMeta;
    var profileMeta = null;
    var portalMsgMeta = null;
    if (metas) for(var i in metas) {
        if (metas[i].action == SellerMetaFlags.SELLER_PROFILE_DATA) 
            profileMeta = metas[i];
        else if (metas[i].action == SellerMetaFlags.SELLER_PORTAL_MESSAGE) 
            portalMsgMeta = metas[i];
    }
    // Check Profile
    var profileOk = seller.photo != null && seller.about && seller.about.length && 
                    // seller.service_areas != null && seller.service_areas.length > 2 &&
                    profileMeta && parseInt(profileMeta.year) > -1 && parseInt(profileMeta.inArea) > -1 &&
                    parseInt(profileMeta.sold) > 0;
    // Check Portal
    var havePortalMessage = portalMsgMeta && portalMsgMeta.message.length;
    var viewedAllListingsVideo = typeof cardMeta['video'] == 'undefined' ? 0 :
                                 (typeof cardMeta['video']['sellers-editListing'] != 'undefined' &&
                                  cardMeta['video']['sellers-editListing'] == 1 && 
                                  typeof cardMeta['video']['sellers-addListing'] != 'undefined' &&
                                  cardMeta['video']['sellers-addListing'] == 1 );
    var haveListingsVideoButtons = $('div.card.listings div.content button.video').length;

    var viewedLifestyleVideo =  typeof cardMeta['video'] == 'undefined' ? 0 :
                                 (typeof cardMeta['video']['sellers-lifestyle'] != 'undefined' &&
                                  cardMeta['video']['sellers-lifestyle'] == 1);
    // var haveLifestyleVideoButtons = $('div.card.lifestyle div.content button.video').length;

    var viewedStatsVideo =      typeof cardMeta['video'] == 'undefined' ? 0 :
                                 (typeof cardMeta['video']['sellers-stats'] != 'undefined' &&
                                  cardMeta['video']['sellers-stats'] == 1);

    var viewedToolsVideo =      typeof cardMeta['video'] == 'undefined' ? 0 :
                                 (typeof cardMeta['video']['sellers-tools'] != 'undefined' &&
                                  cardMeta['video']['sellers-tools'] == 1);
    // var haveToolsVideoButtons = $('div.card.learntools div.content button.video').length;

    var completedToolsPage =    typeof cardMeta['socialMedia'] == 'undefined' || typeof cardMeta['emailBanner'] == 'undefined' ? 0 : havePortalMessage;
    
    // ---- Card Functionality ---- //
    var cards = ['profile', 'lifestyle', 'listings', 'learnstats', 'learntools', 'tools'];
    for(var i in cards) {
        var baseCondition = false;
        switch(cards[i]) {
            case 'profile':     baseCondition = !profileOk; break;
            case 'lifestyle':   baseCondition = !viewedLifestyleVideo; break;
            case 'listings':    baseCondition = (haveListingsVideoButtons && !viewedAllListingsVideo) || // video buttons case takes precedence, else default to checking latter
                                                !seller.allCompleteListings; break;
            case 'learnstats':  baseCondition = !viewedStatsVideo; break;
            case 'learntools':  baseCondition = !viewedToolsVideo; break;
            case 'tools':       baseCondition = !completedToolsPage; break;
        }
        if (baseCondition) {
            cardMeta[cards[i]] = 0; // uh, oh
            $('div.card.'+cards[i]+' #i-completed-it').prop('disabled', true);
        }
        else if (typeof cardMeta[cards[i]] == 'undefined' ||
                 cardMeta[cards[i]] == 0)
            $('div.card.'+cards[i]+' #i-completed-it').prop('disabled', false);
        
        if (typeof cardMeta[cards[i]] != 'undefined' &&
            cardMeta[cards[i]] == 1) 
            $('div.card.'+cards[i]).parent().hide();
    }
    
   // ---- Steps and Buttons ---- //
    if (havePortal) {
      var statsAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(2)').innerWidth();
      var toolsAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(3)').innerWidth();
    }
    else {
      var dashAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(2)').innerWidth();
      var statsAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(3)').innerWidth();
      var toolsAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(4)').innerWidth();
    }
    var profileAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(5)').innerWidth();
    var listingsAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(6)').innerWidth();
    var lifestyledAdjust = $('#seller-admin .sidebar .profile-menu li:nth-child(7)').innerWidth();
  
    var statsSlide = statsAdjust / 2;
    var statsSlideFull = statsSlide - 140;
  
    var toolsAdjustHalf = toolsAdjust / 2;
    var toolsSlide = (statsAdjust + 9) + toolsAdjustHalf;
    var toolsSlideFull = toolsSlide - 140;
  
    var profileAdjustHalf = profileAdjust / 2;
    var profileSlide = ((statsAdjust + 9) + (toolsAdjust + 9)) + profileAdjustHalf;
    var profileSlideFull = profileSlide - 140;
  
    var listingsAdjustHalf = listingsAdjust / 2;
    var listingsSlide = ((statsAdjust + 9) + (toolsAdjust + 9) + (profileAdjust + 9)) + listingsAdjustHalf;
    var listingsSlideFull = listingsSlide - 140;
  
    var lifestyledAdjustHalf = lifestyledAdjust / 2;
    var lifestyledSlide = ((statsAdjust + 9) + (toolsAdjust + 9) + (profileAdjust + 9) + (listingsAdjust + 9)) + lifestyledAdjustHalf;
    var lifestyledSlideFull = lifestyledSlide - 140;
  
    if (!havePortal) {
      var dashAdjustFull = dashAdjust + 9;
      var statsSlideFull = statsSlideFull + dashAdjustFull;
      var toolsSlideFull = toolsSlideFull + dashAdjustFull;
      var profileSlideFull = profileSlideFull + dashAdjustFull;
      var listingsSlideFull = listingsSlideFull + dashAdjustFull;
      var lifestyledSlideFull = lifestyledSlideFull + dashAdjustFull;
    }
  
    function openNavModal() {
      $('#seller-admin .mobile-modal-bg').fadeIn(250);
      $('#seller-admin .sidebar .mobilemenutop .show-nav-modal').addClass( 'popout' );
      $('#seller-admin .sidebar .profile-menu .show-nav-modal').addClass( 'popout' );
      $(document).scrollTop(0);
    }
    function closeNavModal() {
      $('#seller-admin .mobile-modal-bg').fadeOut(250);
			if (!isMobile) {
				$('#seller-admin .mobile-modal-bg').hide();
			}
      $('#seller-admin .sidebar .profile-menu .show-nav-modal').removeClass( 'popout' );
      $('#seller-admin .sidebar .mobilemenutop .show-nav-modal').removeClass( 'popout' );
    }
    
    function stepNumberCount() {
        var stepNumber2 = $('.page-home ul#cardlist > li:nth-child(-n+2):visible').length;
        $('#seller-admin .page-home ul#cardlist li:nth-child(2) .stepDiv .step-number').html(stepNumber2);
        var stepNumber3 = $('.page-home ul#cardlist > li:nth-child(-n+3):visible').length;
        $('#seller-admin .page-home ul#cardlist li:nth-child(3) .stepDiv .step-number').html(stepNumber3);
        var stepNumber4 = $('.page-home ul#cardlist > li:nth-child(-n+4):visible').length;
        $('#seller-admin .page-home ul#cardlist li:nth-child(4) .stepDiv .step-number').html(stepNumber4);
        var stepNumber5 = $('.page-home ul#cardlist > li:nth-child(-n+5):visible').length;
        $('#seller-admin .page-home ul#cardlist li:nth-child(5) .stepDiv .step-number').html(stepNumber5);
        var stepNumber6 = $('.page-home ul#cardlist > li:nth-child(-n+6):visible').length;
        $('#seller-admin .page-home ul#cardlist li:nth-child(6) .stepDiv .step-number').html(stepNumber6);
        if (stepNumber6 == 1 ||
            stepNumber6 == 2){
          $('#seller-admin section.content .page-home ul#cardlist').css('padding-top', '4em');
          $('#seller-admin section.content .page-home ul#cardlist li').css('margin-bottom', '3em');
        }
        else if (stepNumber6 == 3 ||
            stepNumber6 == 4){
          $('#seller-admin section.content .page-home ul#cardlist').css('padding-top', '6em');
          $('#seller-admin section.content .page-home ul#cardlist li').css('margin-bottom', '3em');
        }
    }
    stepNumberCount();
  
    $('#seller-admin .page-home div.card button#i-completed-it').on('click', function() {
        jQuery.fx.speeds.fast = 100;
        $(this).parent().parent().parent().hide('normal', function(){ $(this).hide(); });
        timeoutID = window.setTimeout(stepNumberCount, 500);
    });
    $('#seller-admin .page-home div.card.learnstats a.show-nav').on('click', function() {
        var h = 'This is where you can see the contact info and activity of clients you’ve captured as leads in your Agent Portal.';
        $('#seller-admin .sidebar .profile-menu .show-nav-modal p').html(h);
        $('#seller-admin .sidebar .profile-menu .show-nav-modal').css('margin-left', statsSlideFull);
        openNavModal();
    });
    $('#seller-admin .page-home div.card.learntools a.show-nav').on('click', function() {
        var h = 'This is where you can setup your personal message to clients, email signature banner, and social media posts.';
        $('#seller-admin .sidebar .profile-menu .show-nav-modal p').html(h);
        $('#seller-admin .sidebar .profile-menu .show-nav-modal').css('margin-left', toolsSlideFull);
        openNavModal();
    });
    $('#seller-admin .page-home div.card.profile a.show-nav').on('click', function() {
        var h = 'This is where you upload your profile picture, bio, business stats, designations, and edit your contact info.';
        $('#seller-admin .sidebar .profile-menu .show-nav-modal p').html(h);
        $('#seller-admin .sidebar .profile-menu .show-nav-modal').css('margin-left', profileSlideFull);
        openNavModal();
    });
    $('#seller-admin .page-home div.card.listings a.show-nav').on('click', function() {
        var h = 'This is where you can view and edit your listings. Add HD images and search tags that help buyers find your listing.';
        $('#seller-admin .sidebar .profile-menu .show-nav-modal p').html(h);
        $('#seller-admin .sidebar .profile-menu .show-nav-modal').css('margin-left', listingsSlideFull);
        openNavModal();
    });
    $('#seller-admin .page-home div.card.lifestyle a.show-nav').on('click', function() {
        var h = 'This is where you can advertise your unique local knowledge and property type expertise to buyers searching in your area.';
        $('#seller-admin .sidebar .profile-menu .show-nav-modal p').html(h);
        $('#seller-admin .sidebar .profile-menu .show-nav-modal').css('margin-left', lifestyledSlideFull);
        openNavModal();
    });
    $('#seller-admin .sidebar .profile-menu li a').on('click', function() {
        closeNavModal();
    });
    $('#seller-admin .sidebar .mobilemenutop .centerarrow .arrow .click').on('click', function() {
        closeNavModal();
    });
    $('#seller-admin .sidebar .profile-menu .show-nav-modal button.close').on('click', function() {
        closeNavModal();
    });
    $('#seller-admin .sidebar .mobilemenutop .show-nav-modal button.close').on('click', function() {
        closeNavModal();
    });
}