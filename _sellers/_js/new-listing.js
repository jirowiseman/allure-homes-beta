var setupWizard;
var dz;
var files;
var imagesUploading = 0;
var imagesUploaded = 0;
var turnedOff = false;

var LISTING_PERMIT_ADDRESS = 1;

jQuery(document).ready(function ($) {
	var self = null;
	setupWizard = {
		is_geocoded: false,
		save_geocode: false,
		is_changed: false,
		currentPage: null,
		dbBusy: false,
		newImages: [],
		currentLeftPos: 0, // for left-bar listing-dot
		geoCoder: null,
		defaults: {
			MAX_LISTING_DOT: 20
		},
		prepareUpload: function(e) {
			files = e.target.files;
		},

		upload: function(e) {
			setCookie("IMAGE_DIR", "_listings/uploaded/", 1);
			if (e) {
				e.stopPropagation(); // Stop stuff happening
			    e.preventDefault(); // Totally stop stuff happening
			}

			imagesUploading = files.length;
			imagesUploaded = 0;
			setupWizard.newImages.length = 0;
			// turnedOff = false;
			if (imagesUploading > 1) {
				self.is_changed = true;
				if (!turnedOff) {
					turnedOff = true;
					$('form#image-titles button').prop('disabled', true);
					$('a.delete-listing-image').prop('disabled', true);
					$('a.delete-all-images').prop('disabled', true);
				}
			}
			else {
				ahtb.alert("No file(s) selected");
				return;
			}

			$('#dropzone .spin-wrap').show();

			$.each(files, function(key, value)
			{
				console.log(key, value);
				// Create a formdata object and add the files
				formData = new FormData();
				formData.append ('image_dir','_listings/uploaded/');
				formData.append ('gen_sizes', 1 );
				formData.append(key, value);
			
				$.ajax({
			        url: ah_local.tp+'/_classes/Image.class.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        dataType: 'json',
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					error: function($xhr, $status, $error){
						imagesUploading--;
						if (imagesUploading < 1)
							$('#dropzone .spin-wrap').hide();
						if ( !$xhr.responseText &&
							 !$error)
							return; // ignore it

						var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
						ahtb.open({ title: 'You Have Encountered an Error', 
									height: 150, 
									html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
								});
					},					
				  	success: function(data){
				  		imagesUploading--;
						imagesUploaded++;
						if (imagesUploading < 1)
							$('#dropzone .spin-wrap').hide();
					  	if ( data == null || data.status == null) 
					  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
					  	else {
					  		setupWizard.updateAuthorPhoto(data);
					  		// ahtb.alert(data.data, {width: 450, height: 150});
					  	}
					}
				})
			});
		},

		resetFormElement: function(e) {
		  	e.wrap('<form>').closest('form').get(0).reset();
		  	e.unwrap();
		},

		updateAuthorPhoto: function(x) {
			self = setupWizard;
			if (x.status == 'OK') {							
				self.newImages.push({
					file: x.data[0]
				});
				
			} else
				console.error(x);

			if (imagesUploading < 1) {
				imagesUploaded = 0;
				turnedOff = false;
				$('#dropzone .spin-wrap').hide();
				setupWizard.resetFormElement($('#dropzone'));
				$('form#image-titles button').prop('disabled', false);
				$('a.delete-listing-image').prop('disabled', false);
				$('a.delete-all-images').prop('disabled', false);
				// self.newImages = self.newImages.concat(self.listing_data.images); // add any existing ones to outgoing array
				self.newImages = self.listing_data.images && self.listing_data.images.length ? self.listing_data.images.concat(self.newImages) : self.newImages; // add new images to existing ones.
				var len = self.newImages.length;
				while( len ) {
					if (self.newImages[len-1] == null ||
						!self.newImages[len-1].file) // hey, empty one!
						self.newImages.splice(len-1, 1);
					len--;
				}

				self.listing.set({
					images: self.newImages
				}, function () {
					self.is_changed = false;
					self.listing_data.images = self.deepCopy(self.newImages);
					if (self.listing_data.images.length > 1)
						$('footer .controls button#arrange').show();
					else
						$('footer .controls button#arrange').hide();
					ahtb.close();
					self.background.update(imagesUploaded < 2 ? self.background.goto(self.newImages.length - 1) : (self.background.current ? self.background.current : 0));
					self.newImages.length = 0;
				});
			}
		},

		page_list: {
			splash: {
				title: "Welcome to the Setup Wizard",
				html: function () {
					var $html = '<div class="splash-start" style="text-align: center;">' +
							'<div class="title">'+
								'Create a Superior Listing</div>'+
								'<div class="subtitle">Lifestyled listings give you the tools you need to create a HD listing and show off what’s unique about your listings.</div>'+
							  '<div class="icon-group"><div class="icons"><img src="' + ah_local.tp + '/_img/_sellers/start-page/icon-1.png" />'+
									'<p>1. Upload HD Images</p></div><div class="icons"><img src="' + ah_local.tp + '/_img/_sellers/start-page/icon-2.png" />'+
									'<p>2. Set Basic Info</p></div><div class="icons"><img src="' + ah_local.tp + '/_img/_sellers/start-page/icon-3.png" />'+
									'<p>3. About Your Listing</p></div><div class="icons"><img src="' + ah_local.tp + '/_img/_sellers/start-page/icon-4.png" />'+
									'<p>4. Tag Your Listing</p></div><div class="icons"><img src="' + ah_local.tp + '/_img/_sellers/start-page/icon-5.png" />'+
									'<p>5. Embed a Video</p></div></div>' +
									'<a href="#" class="start-button">Start</a>'+
									'<p class="">FOR BROKERS - Learn how to import generic listings through MLS<a href="#" style="margin-left: 20px;">LEARN MORE <span class="entypo-right-dir"></span></a></p></div>';

					return $html;
				}
			},
			create: {
				title: "Creating your new listing...",
				html: function () {
					var $html = '<h2 class="loading">Creating your new listing...</h2>';
					return $html;
				},
				done: function () {
					self.DB({
						query: 'new-listing',
						done: function (d) {
							self.hash.set({
								listingID: parseInt(d)
							});
							self.listing.get({
								id: self.hash.listingID
							}, function (d) {
								self.listing_data = d;
								self.page.get_page('images');
							});
						}
					});
				}
			},
			images: {
				title: "Images",
				html: function () {
			var $html = '<div class="image-titles left-bar">' +
							'<h3 class="title">Title Images</h3>' +
							'<p>Upload images to be featured on your listing’s profile. Make sure your pictures are the best possible resolution and quality.</p>'+
							'<div class="images">' +
								'<div class="image-controls">' +
									'<a class="prev-image entypo-left"></a>' +
									'<div class="listing-dot-container" >' +
										'<ul class="image-pager">';
									if (self.listing_data.images)
										for (var sldij in self.listing_data.images)
											if (self.listing_data.images[sldij].file)
												$html += '<li class="listing-dot' + (sldij < 1 ? ' active' : '') + '" data-id="' + sldij + '" />';

									$html += '</ul>' +
									'</div>' + 
								'<a class="next-image entypo-right"></a>' +
								'</div>' +
								'<form id="image-titles">';
							if (self.listing_data.images) {
									for (var sldik in self.listing_data.images)
									if (self.listing_data.images[sldik].file) {
										$html += '<img class="listing-thumbnail" data-id="0" src="' + (self.listing_data.images[sldik].file.substr(0, 7) == 'http://' ? self.listing_data.images[sldik].file : ah_local.tp + '/_img/_listings/100x70/' + self.listing_data.images[sldik].file) + '" />';
										break;
									}
							} else
								$html += '<img class="listing-thumbnail blank-thumbnail" src="' + ah_local.tp + '/_img/_listings/100x70/_blank.jpg" />';

							$html+= '<span class="imageheaders">' +
									'<label for="title">Image Title:</label>' +
									'<input type="text" name="title" placeholder="Image Title" value="' + (self.listing_data.images ? '' : 'No images for listing.') + '" />' +
									'<label for="description">Description:</label>' +
									'</span><textarea rows="9" name="description">' + (self.listing_data.images ? '' : 'Please upload an image.') + '</textarea>' +
									'<a class="delete-listing-image" style="font-size: .85em;">Delete Image</a>' +
									'<a class="delete-all-images" style="font-size: .85em;">Delete All</a>' +
									'<button value="Save">Save</button>' +
								'</form>' +
							'</div>' +
						'</div>' +
						'<div class="upload-images info-box">' +
							'<div class="spin-wrap upload"><div class="spinner sphere"></div></div>'+
							'<span class="entypo-upload-cloud"></span>' +
							'<h2>1. Upload Images</h2>' +
							'<form id="dropzone" class="dropzone"></form>' +
						'</div>';
					//'<span class="information"><span class="entypo-info-circled"></span> Drag image to adjust crop</span>'

					// window.setTimeout(function() {
					// 	var h = '<div id="uploadNotice">Upload times will vary depending on connection speed and file size. If you experience load times slower than 500Kb/s, please consider waiting until you have a faster internet connection</div>';
					// 	ahtb.open({html: h,
					// 			   width: 500,
					// 			   height: 200
					// 			});
					// }, 250);

					return $html;
				},
				done: function () {
					$('#page-content').prepend(self.background.get_html({
						with_controls: true
					}));
					imagesUploading = 0;
					imagesUploaded = 0;
					turnedOff = false;
					dz = new Dropzone("#dropzone", {
						dictDefaultMessage: "Click to browse or drop files here to upload",
						acceptedFiles: 'image/*',
						url: ah_local.tp + "/_classes/Image.class.php",
						headers: {
							'image-dir': "_listings/uploaded/"
						},
						dictFallbackText: 'Please choose your file(s)',
						accept: function (file, done) {
							imagesUploading++;
							if (imagesUploading >= 1) {
								self.is_changed = true;
								if (!turnedOff) {
									turnedOff = true;
									$('form#image-titles button').prop('disabled', true);
									$('a.delete-listing-image').prop('disabled', true);
									$('a.delete-all-images').prop('disabled', true);
									$('.spin-wrap.upload ').show();
								}
							}
							done();
						}
					});
					if (typeof dz.disable == 'function') {
						dz.on("error", function (file, responseText) {
							dz.removeFile(file);
							imagesUploading--;
							ahtb.alert(responseText, {height: 150 });
						});

						dz.on("success", function (file, responseText) {
							dz.removeFile(file);
							imagesUploading--;
							imagesUploaded++;
							x = $.parseJSON(responseText);
							if (x.status == 'OK') {							
								self.newImages.push({
									file: x.data[0]
								});
								
							} else
								console.error(x);

							if (imagesUploading < 1) {
								imagesUploaded = 0;
								turnedOff = false;
								$('form#image-titles button').prop('disabled', false);
								$('a.delete-listing-image').prop('disabled', false);
								$('a.delete-all-images').prop('disabled', false);
								$('.spin-wrap.upload ').hide();
								// self.newImages = self.newImages.concat(self.listing_data.images); // add any existing ones to outgoing array
								self.newImages = self.listing_data.images && self.listing_data.images.length ? self.listing_data.images.concat(self.newImages) : self.newImages; // add new images to existing ones.
								var len = self.newImages.length;
								while( len ) {
									if (self.newImages[len-1] == null ||
										!self.newImages[len-1].file) // hey, empty one!
										self.newImages.splice(len-1, 1);
									len--;
								}

								self.listing.set({
									images: self.newImages
								}, function () {
									self.is_changed = false;
									self.listing_data.images = self.deepCopy(self.newImages);
									if (self.listing_data.images.length > 1)
										$('footer .controls button#arrange').show();
									else
										$('footer .controls button#arrange').hide();
									ahtb.close();
									self.background.update(imagesUploaded < 2 ? self.background.goto(self.newImages.length - 1) : (self.background.current ? self.background.current : 0));
									self.newImages.length = 0;
								});
							}
							
						});
					}

					$('div.dz-fallback input[name="file"]').on('change', setupWizard.prepareUpload);
					$('div.dz-fallback input[name="file"]').attr('multiple', 'multiple');
					$('div.dz-fallback input[value="Upload!"]').on('click', setupWizard.upload);

					if (typeof dz.disable != 'function') {
						$('div.dz-message').css('margin', '0');
						$('div.dz-message').css('padding', '0');
						$('#dropzone').css('box-sizing', 'initial');
						$('#dropzone').append('<div class="spin-wrap"><div class="spinner sphere"></div></div>');
					}


					$('form#image-titles button').on('click', function (e) {
						e.preventDefault();
						var t = [];
						for (var sldim in self.listing_data.images) {
							if (parseInt(sldim) === self.background.current.index) {
								var tt = self.listing_data.images[sldim];
								tt.title = $.trim($('form#image-titles input[name=title]').val()).length > 0 ? $.trim($('form#image-titles input[name=title]').val()) : null;
								tt.desc = $.trim($('form#image-titles textarea[name=description]').val()).length > 0 ? $.trim($('form#image-titles textarea[name=description]').val()) : null;
								t.push(tt);
							} else {
								t.push(self.listing_data.images[sldim]);
							}
						}
						self.listing.set({
							images: t
						}, function () {
							self.listing_data.images = t;
							$('form#image-titles button').prop('disabled', true);
							ahtb.close();
						});
					});

					$('a.delete-listing-image').on('click', function () {
						var t = [];
						for (var sldin in self.listing_data.images)
							if (parseInt(sldin) !== self.background.current.index)
								t.push(self.listing_data.images[sldin]);

						if (t.length < 1)
							t = 0;

						self.listing.set({
							images: t
						}, function () {
							self.listing_data.images = t || [];
							self.background.update();
							if (self.listing_data.images.length > 1)
								$('footer .controls button#arrange').show();
							else
								$('footer .controls button#arrange').hide();
							$('form#image-titles button').prop('disabled', true);
							ahtb.close();
						});
					});

					$('a.delete-all-images').on('click', function () {
						if (self.listing_data.images.length)
							ahtb.open({ html: "<p>Are you sure?</p>",
										title: "Delete All Images",
										width: 450,
										height: 150,
										buttons:[
										{text:"OK", action:function() {
											self.listing.set({
												images: null
											}, function () {
												self.listing_data.images = [];
												self.background.update();
												$('form#image-titles button').prop('disabled', true);
												$('footer .controls button#arrange').hide();
												ahtb.close();
											});
										}},
										{text:"Cancel", action:function() {
											ahtb.close();
										}}]});
					});
				},
				is_changed: function () {
					has_changed = false;

					if (self.listing_data.images) {
						chk = {
							title: $.trim($('form#image-titles input[name=title]').val()).length > 0 ? $.trim($('form#image-titles input[name=title]').val()) : null,
							desc: $.trim($('form#image-titles textarea[name=description]').val()).length > 0 ? $.trim($('form#image-titles textarea[name=description]').val()) : null,
							src: $('form#image-titles img.listing-thumbnail').attr('src')
						};

						if (!has_changed && (chk.title !== self.background.current.title || 
											 chk.desc !== self.background.current.desc ||
											 chk.src != self.background.current.src))
							has_changed = true;
					}

					return has_changed;
				},
				check_for_changes: function ($in) {
					self.is_changed = self.page_list.images.is_changed();
					$('form#image-titles button').prop('disabled', self.page_list.images.is_changed() ? false : true);
				},
				shufflePhotos: function() {
					self.is_changed = false;
					cancelled = false;
					var h = '<div id="instructions">' +
								'<span>Enter a title and description for any image.  Click and drag images to reorder them, flowing left to right, top to bottom</span>'+
							'</div>' +
							'<div id="shufflePhotos">'+
								'<ul id="sortable">';
								if (self.listing_data.images) {
									for (var sldik in self.listing_data.images)
										if (self.listing_data.images[sldik].file) {
											h += '<li class="ui-state-default">' +
													'<div id="photoGroup">'+
													 	'<label for="title">Image Title:</label>' +
														'<input type="text" name="title" placeholder="Image Title" placeHolder="Enter title" value="' + (self.listing_data.images[sldik].title ? self.listing_data.images[sldik].title : '') + '" />' +
														'<img class="listing-thumbnail" data-id="'+sldik+'" src="' + (self.listing_data.images[sldik].file.substr(0, 7) == 'http://' ? self.listing_data.images[sldik].file : ah_local.tp + '/_img/_listings/210x120/' + self.listing_data.images[sldik].file) + '" />' +
														'<label id="description">Description:</label>' +
														'<button id="description" data-id="'+sldik+'">Edit</button>'+
												 	'</div>'+
												 '</li>';
												 
										}
								}	 	 
							h+=	'</ul>'+
								'<div id="description">' +
									'<textarea rows="9" name="description" placeHolder="Enter description" ></textarea>' +
									'<div id="control" data-id="">'+
										'<button id="cancel" >Cancel</button>'+
										'<button id="done" >Done</button>'+
									'</div>'+
								'</div>'+
							'</div>';
					ahtb.open({ html: h,
								width: 960,
								height: 700,
								buttons: [
								{text:'Cancel', action: function() {
									cancelled = true;
									ahtb.close();
								}},
								{text:'OK', action:function() {
    								ahtb.close();
									console.log("ahtb exiting");
								}}],
								opened: function() {
									$('#sortable').sortable();
									$( "#sortable" ).disableSelection();
									$('button#description').on('click', function() {
										var id = parseInt($(this).attr('data-id'));
										$('div#description').show();
										$('div#description #control').attr("data-id", id);
										$('div#description textarea').val( self.listing_data.images[id].desc ? self.listing_data.images[id].desc: '' );
									})
									$('#control button').on('click', function() {
										var id = $(this).attr('id');
										console.log("control button "+id+" was clicked");
										$('div#description').hide();
										if (id == 'done') {
											var id = parseInt($(this).parent().attr('data-id'));
											self.listing_data.images[id].desc = $('div#description textarea').val();
											self.is_changed = true;
										}
									})
								},
								closed: function() {
									if (cancelled)
										return;

									var descVisible = $('div#description').css('display') != 'none';
									if (descVisible) {
										var id = parseInt($('div#description').find('#control').attr('data-id'));
										self.listing_data.images[id].desc = $('div#description textarea').val();
										self.is_changed = true;
									}

									var x = [];
									$('ul#sortable li').each(function(index, element) {
    									var img = $(element).find('img');
    									var id = img.attr('data-id');
    									var title = $(element).find('input').val();
    									if (typeof self.listing_data.images[id].title == 'undefined' ||
    										self.listing_data.images[id].title == null ||
    										title != self.listing_data.images[id].title) {
    										self.listing_data.images[id].title = title;
    										self.is_changed = true;
    									}
    									x[x.length] = parseInt(id);
    									console.log("Index "+index+" now has image:"+img.attr('data-id'));
    								});
									
    								var outOfOrder = false;
    								for(var i in x)
    									if (i != x[i]) {
    										outOfOrder = true;
    										break;
    									}

    								if (outOfOrder ||
    									self.is_changed) {
    									var img = [];
    									for(var i in x)
    										img[i] = self.listing_data.images[x[i]];

    									window.setTimeout(function() {
	    									self.listing.set({
												images: img
											}, function () {
												self.is_changed = false;
												self.listing_data.images = self.deepCopy(img);
												ahtb.close();
												// self.background.update(imagesUploaded < 2 ? self.background.goto(self.newImages.length - 1) : (self.background.current ? self.background.current : 0));
												// self.newImages.length = 0;
												self.currentPage = "splash";
												self.page.get_page('images', true);
											})
	    								}, 250);
    								}
									console.log("ahtb closed");
								}
						})
				}
			},
			info: {
				title: "Main Info",
				html: function () {
					var $html = '<div class="left-bar">' +
						'<table id="listing-info">';
					var keys = ['street_address', 'country', 'city', 'zip', 'price', 'lotsize', 'interior', 'beds', 'baths'];
					for (var k in keys)
						switch (keys[k]) {
						case 'street_address':
							$html += '<tr>' +
								'<td colspan="2" class="street_address click-to-edit"  data-class="street_address">' +
								(self.listing_data.street_address && self.listing_data.street_address != -1 && $.trim(self.listing_data.street_address) !== '' ? self.listing_data.street_address : '<a style="font-size: 1.5em;">Set Street Address</a> <div style="font-size: .9em; line-height: .9em;">(click to edit)</div>') +
								'</td>' +
								'</tr>' +
								'<tr>' +
								'<td colspan="2" class="hide_address">' +
								'<input type="checkbox" id="hide_address_checkbox" ' + ( !(self.listing_data.flags & LISTING_PERMIT_ADDRESS) ? 'checked ' : '') + '/> ' +
								'Hide Address?' +
								'</td>' +
								'</tr>';
							break;
						default:
							$html += '<tr>' +
								'<th>';
							switch (keys[k]) {
							case 'price':
								$html += 'Offered Price';
								break;
							case 'city':
								$html += 'Location';
								break;
							case 'zip':
								$html += 'Zip Code';
								break;
							case 'lotsize':
								$html += 'Lot Size';
								break;
							case 'title':
								$html += 'title';
								break;
							default:
								$html += keys[k].charAt(0).toUpperCase() + keys[k].slice(1);
								break;
							}
							$html += ':</th>';
							switch (keys[k]) {
							case 'baths':
								$html += '<td class="baths click-to-edit"  data-class="baths">' +
									'<span data-class="full-baths">' + (self.listing_data.baths ? self.listing_data.baths.toString().split('.')[0] : '<a>Set Baths</a>') + '</span>' +
									(self.listing_data.baths ? ' / ' : '') +
									'<span data-class="partial-baths">' + (self.listing_data.baths ? (self.listing_data.baths.toString().split('.')[1] ? self.listing_data.baths.toString().split('.')[1] : 0) : '') + '</span>' +
									'</td>';
								break;
							case 'beds':
								$html += '<td class="beds click-to-edit"  data-class="beds">' +
									(self.listing_data.beds ? self.listing_data.beds : '<a>Set Beds</a>') +
									'</td>';
								break;
							case 'city':
								$html += '<td class="location">' +
									'<span class="click-to-edit" data-class="city">' +
									(self.listing_data.city ? self.listing_data.city : '<a>Set City</a>') +
									'</span>' +
									', ' +
									'<span class="click-to-edit" data-class="state">' +
									(self.listing_data.state ? self.listing_data.state : '<a>Set State</a>') +
									'</span>' +
									'</td>';
								break;
							case 'interior':
							case 'lotsize':
								$html += '<td class="click-to-edit" data-class="' + keys[k] + '">' +
									'<span data-class="' + keys[k] + '">' + ((self.listing_data[keys[k]] ? self.listing_data[keys[k]].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '<a>Set ' + keys[k] + '</a>') + '</span> <span data-class="' + keys[k] + '_std">' + (self.listing_data[keys[k]] && self.listing_data[(keys[k] + '_std')] === 'acres' ? 'acres' : (self.listing_data[keys[k]] ? 'ft&sup2;' : ''))) + '</span>' +
									'</td>';
								break;
							case 'price':
								$html += '<td class="' + keys[k] + ' click-to-edit"  data-class="' + keys[k] + '">' +
									(self.listing_data.price ? '$' + self.listing_data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '<a>Set Price</a>') +
									'</td>';
								break;
							default:
								$html += '<td class="' + keys[k] + ' click-to-edit"  data-class="' + keys[k] + '">' +
									((typeof self.listing_data[keys[k]] == 'string' || !isNaN(self.listing_data[keys[k]])) && self.listing_data[keys[k]] !== null && self.listing_data[keys[k]] != -1 && $.trim(self.listing_data[keys[k]]) !== '' ? self.listing_data[keys[k]] : '<a>Set ' + keys[k] + '</a>') +
									'</td>';
								break;
							}
							$html += '</tr>';
							break;
						}
					$html += '</table>' +
						'<ul id="geocode-panel" class="down">' +
						'<li class="top"><span class="title">Is this your address?</span> <span class="geocode-pager"></span><span class="close">x</span></li>' +
						'<li class="address"></li>' +
						'<li class="bottom">' +
						// '<a class="left entypo-left"></a>'+
						'<a class="accept">Save Address</a>' +
						// '<a class="right entypo-right"></a>'+
						'</li>' +
						'</ul>' +
						'</div>' +
						'<div class="main-info info-box">' +
						'<span class="main-info-icon"></span>' +
						'<h2>2. Main Information</h2>' +
						'<p>Please enter the listing address, basic listing information, and a captivating title/tagline!</p>' +
						'</div>';

					return $html;
				},
				done: function () {
					$('#page-content').prepend(self.background.get_html({
						with_controls: true
					}));

					$('.click-to-edit').on('click', function () {
						field = $(this).attr('data-class');
						$pretty_title = field.charAt(0).toUpperCase() + field.substring(1);
						$height = 145;
						$buttons = [{
							text: 'Save',
							action: function () {
								var data = {};
								data[field] = !isNaN($('#tb-submit input').val()) ? parseInt($('#tb-submit input').val()) : $('#tb-submit input').val();
								if (self.listing_data[field] == data[field]) {
									ahtb.close();
									return;
								}
								self.listing.set(data, function (d) {
									self.listing_data[field] = data[field];
									$('[data-class=' + field + ']').html(field !== 'zip' && !isNaN(data[field]) ? data[field].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : data[field]);
									ahtb.close();
								});
							}
						}, {
							text: 'Cancel',
							action: function () {
								ahtb.close();
							}
						}];
						$html = '<p><input type="text" style="width: 80%;" placeholder="' + $pretty_title + '" value="' + (self.listing_data[field] != null ? self.listing_data[field] : '' )+ '" /></p>';

						switch ($(this).attr('data-class')) {
						case 'price':
							$buttons[0].action = function () {
								$price = parseInt($('#tb-submit input').val().replace(/\D/g, ''));
								self.listing.set({
									price: $price
								}, function (d) {
									self.listing_data.price = $price;
									$('[data-class=price]').html($price ? '$' + $price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '(price not set)');
									ahtb.close();
								});
							};
							break;
						case 'street_address':
							$pretty_title = 'Street Address';
							break;
						case 'zip':
							$pretty_title = 'Zip Code';
							break;
						case 'title':
							$pretty_title = 'Title';
							break;
						case 'country':
							$html = '<p><select class="country"><option value="-1" disabled' + (!self.listing_data.country ? ' selected' : '') + '>(choose a country)</option>';
							for (var country in self.countries)
								$html += '<option value="' + self.countries[country].short_name + '"' + (self.countries[country].short_name === self.listing_data.country ? ' selected' : '') + '>' + country + '</option>';
							$html += '</select></p>';
							$buttons[0].action = function () {
								$country = $('#tb-submit select').val();
								self.listing.set({
									country: $country
								}, function () {
									self.listing_data.country = $country;
									$('[data-class=country]').html($country);
									ahtb.close();
								});
							};
							break;
						case 'state':
							if (!self.listing_data.country) {
								$height = 125;
								$buttons = [{
									text: 'OK',
									action: function () {
										ahtb.close();
									}
									}];
								$html = '<p>Please select a country first.</p>';
							} else {
								$states = null;
								for (var scountry in self.countries)
									if (self.countries[scountry].short_name === self.listing_data.country) {
										$states = self.countries[scountry].states;
										break;
									}
								if (!$states) {
									$height = 125;
									$buttons = [{
										text: 'OK',
										action: function () {
											ahtb.close();
										}
										}];
									$html = '<p>Unable to find state / region list for ' + self.listing_data.country + '</p>';
								} else {
									$html = '<p><select class="state"><option value="-1" disabled' + (!self.listing_data.state ? ' selected' : '') + '>(choose a state)</option>';
									for (var si in $states)
										$html += '<option value="' + (self.listing_data.country === 'US' ? us_states[$states[si]] : $states[si]) + '"' + (self.listing_data.state === us_states[$states[si]] ? ' selected' : null) + '>' + $states[si] + '</option>';

									$html += '</select></p>';
									$buttons[0].action = function () {
										$state = $('#tb-submit select').val();
										self.listing.set({
											state: $state
										}, function () {
											self.listing_data.state = $state;
											$('[data-class=state]').html($state);
											ahtb.close();
										});
									};
								}
							}
							break;
						case 'interior':
							$pretty_title = 'Interior';
							$html = '<p>' +
								'<input type="text" style="width: 50%; margin-right: 1em;" placeholder="Interior" value="' + self.listing_data.interior + '" />' +
								'<select>' +
								'<option value="ft"' + (!self.listing_data.interior_std || self.listing_data.interior_std === 'ft' ? ' selected' : '') + '>ft&sup2;</option>' +
								'<option value="acres"' + (self.listing_data.interior_std === 'acres' ? ' selected' : '') + '>acres</option>' +
								'</select>' +
								'</p>';
							$buttons[0].action = function () {
								$interior = parseFloat($('#tb-submit input').val().replace(/\D/g, ''));
								$interior_std = $('#tb-submit select').val();
								self.listing.set({
									interior_std: $interior_std,
									interior: $interior
								}, function () {
									self.listing_data.interior = $interior;
									self.listing_data.interior_std = $interior_std;
									$('span[data-class=interior]').html($interior ? $interior.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '(interior not set)');
									$('span[data-class=interior_std]').html($interior ? ($interior_std === 'acres' ? 'acres' : 'ft&sup2;') : '');

									ahtb.close();
								});
							};
							break;
						case 'lotsize':
							$pretty_title = 'Lot Size';
							$html = '<p>' +
								'<input type="text" style="width: 50%; margin-right: 1em;" placeholder="Lot Size" value="' + self.listing_data.lotsize + '" />' +
								'<select>' +
								'<option value="ft"' + (!self.listing_data.lotsize_std || self.listing_data.lotsize_std === 'ft' ? ' selected' : '') + '>ft&sup2;</option>' +
								'<option value="acres"' + (self.listing_data.lotsize_std === 'acres' ? ' selected' : '') + '>acres</option>' +
								'</select>' +
								'</p>';
							$buttons[0].action = function () {
								$lotsize = parseFloat($('#tb-submit input').val().replace(/\D/g, ''));
								$lotsize_std = $('#tb-submit select').val();
								self.listing.set({
									lotsize_std: $lotsize_std,
									lotsize: $lotsize
								}, function () {
									self.listing_data.lotsize = $lotsize;
									self.listing_data.lotsize_std = $lotsize_std;
									$('span[data-class=lotsize]').html($lotsize ? $lotsize.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '(lotsize not set)');
									$('span[data-class=lotsize_std]').html($lotsize ? ($lotsize_std === 'acres' ? 'acres' : 'ft&sup2;') : '');

									ahtb.close();
								});
							};
							break;
						case 'beds':
							$html = '<p><select><option value="-1" disabled' + (!self.listing_data.beds ? ' selected' : '') + '>(choose the number of bedrooms)</option>';
							for (var bedcount = 0; bedcount < 50; bedcount++)
								$html += '<option value="' + bedcount + '"' + (self.listing_data.beds === bedcount ? ' selected' : '') + '>' + bedcount + '</option>';
							$html += '</select></p>';
							$buttons[0].action = function () {
								$beds = $('#tb-submit select').val() === -1 ? null : parseInt($('#tb-submit select').val());
								self.listing.set({
									beds: $beds,
								}, function () {
									self.listing_data.beds = $beds;
									$('td.beds').html($beds);
									ahtb.close();
								});
							};
							break;
						case 'baths':
							$pretty_title = 'Bathrooms';
							$html = '<p><select id="fbaths" style="width: 40%; margin-right: 2%; display: inline;"><option value="-1" disabled' + (!self.listing_data.baths ? ' selected' : '') + '>(full bathrooms)</option>';
							for (var fbath = 0; fbath < 50; fbath++)
								$html += '<option value="' + fbath + '"' + (fbath && parseInt(self.listing_data.baths.toString().split('.')[0]) === fbath ? ' selected' : '') + '>' + fbath + '</option>';
							$html += '</select><select id="pbaths" style="display: inline; width: 40%;"><option value="-1" disabled' + (!self.listing_data.baths.toString().split('.')[1] ? ' selected' : '') + '>(partial bathrooms)</option>';
							for (var pbath = 0; pbath < 50; pbath++)
								$html += '<option value="' + pbath + '"' + (pbath && parseInt(self.listing_data.baths.toString().split('.')[1]) === pbath ? ' selected' : '') + '>' + pbath + '</option>';
							$html += '</select></p>';
							$buttons[0].action = function () {
								$baths = parseFloat(($('#fbaths').val() > 0 ? $('#fbaths').val() : 0) + '.' + ($('#pbaths').val() > 0 ? $('#pbaths').val() : 0));
								self.listing.set({
									baths: $baths
								}, function () {
									self.listing_data.baths = $baths;
									$('[data-class=baths]').html('<span data-class="full-baths">' + (self.listing_data.baths ? self.listing_data.baths.toString().split('.')[0] : '(baths not set)') + '</span>' +
										(self.listing_data.baths ? ' / ' : '') +
										'<span data-class="partial-baths">' + (self.listing_data.baths ? (self.listing_data.baths.toString().split('.')[1] ? self.listing_data.baths.toString().split('.')[1] : 0) : '') + '</span>');
									ahtb.close();
								});
							};
							break;
						}

						ahtb.open({
							height: $height,
							width: 350,
							buttons: $buttons,
							html: $html,
							title: 'Edit ' + $pretty_title,
							opened: function() {
								$('#tb-submit input').focus();
							}
						});
					});

					$("#hide_address_checkbox").change(function () {
						var flags = self.listing_data.flags ^ 1;
						self.listing.set({
							flags: flags
						}, function () {
							self.listing_data.flags = flags;
							ahtb.close();
						});
					});
				},
			},
			about: {
				title: "About",
				html: function () {
					$html = '<div class="main-info info-box">' +
						'<span class="main-info-icon"></span>' +
						'<h2>3. About</h2>' +
						'<p>Talk about your listing. What is special about it? Sell the buyer on the lifestyle.</p>' +
						'</div>' +
						'<div class="left-bar">' +
						'<h2>About</h2>' +
						'<div class="special-tags">' +
						'<div class="border-top"></div>' +
						'<span class="tag tag-1" data-class="tag-1">Describe</span>' +
						'<span class="tag tag-2"data-class="tag-2">Your</span> <span class="div">|</span> <span class="tag tag-3" data-class="tag-3">Listing</span>' +
						'</div>' +
						'<textarea class="about" data-class="about">' + (self.listing_data.about ? self.listing_data.about : 'Please tell us about your listing.') + '</textarea>' +
						'<button id="save-about" disabled>Save</button>' +
						'</div>';
					return $html;
				},
				done: function () {
					$('#page-content').prepend(self.background.get_html({
						with_controls: true
					}));

					$('#page-content textarea.about').bind('input propertychange', function () {
						var $val = $(this).val();
						if ($val === 'Please tell us about your listing.' || $val === '')
							$val = null;

						var $changed = $val !== self.listing_data.about;
						self.is_changed = $changed;
						$('#save-about').prop('disabled', !$changed);

					}).bind('click', function () {
						if ($(this).val() === 'Please tell us about your listing.')
							$(this).val(null);
					}).bind('blur', function () {
						if ($(this).val() === '')
							$(this).val('Please tell us about your listing.');
					});

					$('#save-about').on('click', function () {
						var $val = $('#page-content textarea.about').val();
						if ($val === 'Please tell us about your listing.')
							$val = null;
						self.listing.set({
							about: $val
						}, function () {
							self.listing_data.about = $val;
							$('#save-about').prop('disabled', true);
							ahtb.close();
						});
					});
				}
			},
			tags: {
				title: "Tags",
				html: function () {
					first = true;
					var $tag_uls = '';
					$html = '<div class="add-tags"><div id="tag-window">' +
						'<ul class="tag-categories">';
					for (var cat_id in tag_cats) {
						var num_tags = 0;
						var the_tags = [];
						for (var tag_id in listing_tags)
							if (listing_tags[tag_id].category_id === parseInt(cat_id)) {
								num_tags++;
								the_tags[tag_id] = listing_tags[tag_id];
							}

						if (num_tags) {
							$html += '<li class="tag-category' + (first ? ' active' : '') + '" cat-id="' + cat_id + '">' + tag_cats[cat_id].category + '<span class="entypo-record"></span></li>';

							$tag_uls += '<ul class="tags-list' + (first ? ' active' : '') + '" cat-id="' + cat_id + '" style="width: ' + 250 * Math.ceil(num_tags / 7) + 'px">';
							for (var ltag_id in the_tags) {
								$tag_uls += '<li class="tag' + ($.inArray(parseInt(ltag_id), self.listing_data.tags) > -1 ? ' active' : '') + '" tag-id="' + ltag_id + '">' +
									'<span class="text">' +
									'<span class="checkmark"><img src="' + ah_local.tp + '/_img/_sellers/check.png" /></span>' +
									the_tags[ltag_id].tag + '</span>' +
									'</li>';
							}
							$tag_uls += '</ul>';
						}
						if (first)
							first = false;
					}
					$html += '</ul>' + '<div class="tag-definition" style="display: none;"><h5>Definition:</h5><span></span></div>' + $tag_uls +
						'<div class="main-info"><span class="main-info-icon"></span><h2>4. Tags</h2><p>Tag your listings with simple defining terms and let our matching system put your home in front of the right buyers.</p></div>' +
						'</div></div>';
					return $html;
				},
				done: function () {
					$('li.tag-category').on('click', function () {
						if (!$(this).hasClass('active')) {
							$('.tag-category.active,.tags-list.active').removeClass('active');
							$(this).addClass('active');
							$('.tags-list[cat-id=' + $(this).attr('cat-id') + ']').addClass('active');
						}
					});

					$('li.tag').on('click', function () {
						var tagId = $(this).attr('tag-id');
						if (self.dbBusy) {
							setTimeout(function(){
								$('li.tag[tag-id='+tagId+']').click();
							}, 50);
							return;
						}
						var x = [];
						if ($(this).hasClass('active')) {
							$(this).removeClass('active');
							for (var sldti in self.listing_data.tags)
								if (self.listing_data.tags[sldti] !== parseInt($(this).attr('tag-id')))
									x.push(self.listing_data.tags[sldti]);
						} else {
							$(this).addClass('active');
							for (var sldtj in self.listing_data.tags)
								x.push(self.listing_data.tags[sldtj]);

							x.push(parseInt($(this).attr('tag-id')));
						}
						self.listing.set({
							tags: x
						}, function () {
							self.listing_data.tags = x;
							ahtb.close();
						});
					});

					$('li.tag').hover(function () {
						var tag_desc = listing_tags[parseInt($(this).attr('tag-id'))].description;
						if ($('.tag-definition').is(':visible'))
							$('.tag-definition').fadeOut(125, function () {
								$(this).children('span').html(tag_desc);
								$(this).fadeIn(125);
							});
						else {
							$('.tag-definition').children('span').html(tag_desc);
							$('.tag-definition').fadeIn(125);
						}
					}, function () {});
				}
			},
			video: {
				title: "Video",
				html: function () {
					$html = '<div class="video-container">' +
						(!self.listing_data.video ? '<img class="no-video" src="' + ah_local.tp + '/_img/_sellers/video.jpg" />' : '<iframe width="560" height="315" src="http://www.youtube.com/embed/' + self.listing_data.video.file + '" frameborder="0" allowfullscreen></iframe>') +
						'</div>' +
						'<div class="main-info">' +
						'<span class="main-info-icon"></span>' +
						'<h2>5. Embed a Video</h2>' +
						'<p>Take your listing to the next level by including a video for buyers to get a better view of the home. Embed a youtube video by entering the url for the video below.</p>' +
						'<form>' +
						'<input class="data-video" type="text" placeholder="Paste YouTube URL here (ex: http://youtu.be/rhUqDy4tg50)" style="padding-left: 5px;" value="' + (!self.listing_data.video ? '' : 'youtu.be/' + self.listing_data.video.file) + '" />' +
						'<button disabled>Save</button>' +
						'</form>' +
						'</div>';
					return $html;
				},
				done: function () {
					$('#page-content .data-video').bind('input propertychange', function () {
						var newVid = $.trim($(this).val());
						if (newVid.length < 1)
							newVid = null;

						var $changed = newVid != self.listing_data.video;
						self.is_changed = $changed;
						$('.main-info button').prop('disabled', !$changed);
					});
					$('.main-info button').on('click', function (e) {
						e.preventDefault();
						var newVid = $.trim($('#page-content .data-video').val());
						if (newVid.length < 1)
							newVid = null;
						else {
							if (newVid.indexOf('v=') != -1)
								newVid = newVid.substr(newVid.indexOf('v=')+2); //, 11); // include the 'v='
							else {
								var parts = newVid.split('/');
								newVid = parts.pop();
							}
						}

						self.listing.set({
							video: newVid != null ? {file: newVid} :null
						}, function () {
							self.listing_data.video = newVid;
							$('div.video-container').html(!self.listing_data.video ? '<img class="no-video" src="' + ah_local.tp + '/_img/_sellers/video.jpg" />' : '<iframe width="560" height="315" src="//www.youtube.com/embed/' + self.listing_data.video + '" frameborder="0" allowfullscreen></iframe>');
							ahtb.close();
						});
					});
				}
			},
		},
		init: function ($listing_data) {
			self = this;

			$('#header-nav').hide();
			$('#page-content').html('<h2 class="loading">Initializing setup wizard...</h2>');

			self.footer_controls.off();
			self.geocoder.hide();

			if (typeof google != 'undefined'){
				self.geoCoder = new google.maps.Geocoder();
			}

			// read hash
			if (typeof window.location.hash !== 'undefined' && window.location.hash !== '') {
				self.hash.parse();

				if (self.hash.listingID !== null)
					self.listing.get(self.hash.listingID, function (d) {
						self.listing_data = d;

						if (d.lat && d.lng && d.lat !== '-1' && d.lng !== '-1')
							self.is_geocoded = true;

						self.background.init();

						if (self.hash.page === null)
							self.page.get_page('images');
						else
							self.page.get_page(self.hash.page);
					});
			} else
				self.page.get_page('splash');

			// init header controls
			$('header .nav-link').each(function () {
				$(this).on('click', function () {
					$page = self.page.get_page_info($(this).attr('data-page'), true);

					if ($page !== false)
						self.page.get_page($page);
					$page = false;
				});
			});

			$(window).bind("beforeunload", function () {
				if (self.is_changed)
					return 'You will lose all unsaved changes.';
			});

			self.countries = {};
			for (var i in sorted_countries)
				self.countries[sorted_countries[i][0]] = {
					short_name: sorted_countries[i][1],
					states: sorted_countries[i][5].split('|'),
				};
		},
		/**
		 * get / set listing info from / to database
		 * @type {Object}
		 */
		listing: {
			/**
			 * get listing from database by listing id
			 * @param  {int} $id    listing_id
			 * @param {function} callback
			 * @return {bool}     success / fail
			 */
			get: function ($id, callback) {
				self.DB({
					query: 'get-listing',
					data: { id: $id },
					done: function (d) {
						if (typeof callback === 'function')
							callback(d);
					},
				});
			},
			/**
			 * saves fields to the current listing, if changes were made
			 * @param {[type]} fields [description]
			 */
			set: function (fields, callback) {
				if (fields) {
					var fields_to_save = {};
					var quickSave = false;
					var keys = ['street_address', 'country', 'city', 'zip', 'price', 'lotsize', 'interior', 'beds', 'baths', 'tags', 'about', 'video'];
					for (var f in fields) {
						if (self.listing_data[f] !== fields[f])
							fields_to_save[f] = (fields[f] instanceof Array && fields[f].length < 1) ? 'null' : fields[f];
						quickSave = quickSave | keys.indexOf(f) != -1;
					}

					var fKeys = Object.keys(fields_to_save);
					if (fKeys.length < 1 && typeof callback === 'function')
						callback({});
					else if (fKeys.length > 0) {
						self.is_changed = true;

						var $run_geocoder = false;
						if (!self.save_geocode) {
							found = false;
							inarray = ['street_address', 'city', 'state', 'zip', 'country'];
							for (var ia in inarray)
								if ($.inArray(inarray[ia], fKeys) > -1) {
									found = true;
									break;
								}

							if (found) {
								$run_geocoder = true;
								self.is_geocoded = false;
								if (self.listing_data.lat !== -1 && self.listing_data.lng !== -1 && $.inArray('lat', fKeys) < 0 && $.inArray('lng', fKeys) < 0) {
									fields_to_save.lat = -1;
									fields_to_save.lng = -1;
									fields_to_save.city_id = null;
									self.listing_data.lat = -1;
									self.listing_data.lng = -1;
									self.listing_data.city_id = null;
								}
							}
						}

						if (!quickSave) {
							var msg = $.inArray('images', fKeys) ? 'Saving listings...this may take a moment' : 'Saving listing...';
							ahtb.loading(msg, {
								height: 65,
								hideTitle: true,
								opened: function () {
									self.listing.runSet(fields_to_save, callback, $run_geocoder, true);
								}
							});
						}
						else
							self.listing.runSet(fields_to_save, callback, $run_geocoder);
					}
				} else if (typeof callback === 'function')
					callback({});
			},
			runSet: function(fields_to_save, callback, run_geocoder, closeAhtb) {
				self.DB({
					query: 'set-listing',
					data: {
						id: self.listing_data.id,
						fields: fields_to_save
					},
					done: function (d) {
						self.is_changed = false;

						if (typeof callback === 'function')
							callback(d);			
						else if (typeof closeAhtb != 'undefined')
							ahtb.close();

						if (run_geocoder)
							self.geocoder.query();
					},
				});
			}
		},
		geocoder: {
			map_zoom: 8,
			is_hidden: true,
			is_local_geocode: true,
			query: function (address_query) {
				$('#geocode-panel .bottom .left').off('click').on('click', function () {
					self.geocoder.prev();
				});
				$('#geocode-panel .bottom .right').off('click').on('click', function () {
					self.geocoder.next();
				});
				$('#geocode-panel .bottom .accept').off('click').on('click', function () {
					self.geocoder.save_to_listing();
				});
				this.addresses = [];
				var complete = true;
				if (!address_query) {
					address_query = '';
					aqarray = ['street_address', 'city', 'state', 'zip', 'country'];
					for (var aq in aqarray){
						$val = self.listing_data[aqarray[aq]] && self.listing_data[aqarray[aq]].length ? (this.is_local_geocode ? self.listing_data[aqarray[aq]] : self.listing_data[aqarray[aq]].toString().replace(/ /g, '+')) : null;
						if ($val == null &&
							(aqarray[aq] == 'street_address' || aqarray[aq] == 'city' || aqarray[aq] == 'state'))
							complete = false;
						$val = $val && aqarray[aq] === 'street_address' ? $val.split('#')[0] : $val;
						if (this.is_local_geocode)
							address_query += $val ? ((address_query.length > 0 ? ', ' : '') + $val) : '';
						else
							address_query += $val ? ((address_query.length > 0 ? '+' : '') + $val) : '';
					}
				}
				if (complete &&
					address_query.length > 0)
					if (this.is_local_geocode) {
						setupWizard.geoCoder.geocode( { 'address': address_query}, function(results, status) {
						    if (status == google.maps.GeocoderStatus.OK) {
						        console.log("Got geocode for "+address_query+", lat:"+results[0].geometry.location.lat()+", lng:"+results[0].geometry.location.lng());
						        var d = results[0];
						        if (d.address_components) {
									var a = {
										street_address: {
											complete: 0,
											value: '',
										},
										city: {
											complete: 0,
											value: '',
										},
										state: {
											complete: 0,
											value: '',
										},
										zip: {
											complete: 0,
											value: '',
										},
										country: {
											complete: 0,
											value: '',
										}
									};
									for (var adi in d.address_components) {
										if ($.inArray('street_number', d.address_components[adi].types) > -1) {
											a.street_address.complete++;
											a.street_address.value = d.address_components[adi].long_name + (a.street_address.value.length > 0 ? (' ' + a.street_address.value) : '');
										} else if ($.inArray('route', d.address_components[adi].types) > -1) {
											a.street_address.complete++;
											a.street_address.value = (a.street_address.value.length > 0 ? (a.street_address.value + ' ') : '') + d.address_components[adi].short_name;
										} else if ($.inArray('locality', d.address_components[adi].types) > -1) {
											a.city.complete++;
											a.city.value = d.address_components[adi].long_name;
										} else if ($.inArray("administrative_area_level_1", d.address_components[adi].types) > -1) {
											a.state.complete++;
											a.state.value = d.address_components[adi].short_name;
										} else if ($.inArray('country', d.address_components[adi].types) > -1) {
											a.country.complete++;
											a.country.value = d.address_components[adi].short_name;
										} else if ($.inArray('postal_code', d.address_components[adi].types) > -1) {
											a.zip.complete++;
											a.zip.value = parseInt(d.address_components[adi].long_name);
										}
									}

									if (a.street_address.complete && a.city.complete && a.state.complete && a.country.complete && a.zip.complete)
										self.geocoder.addresses.push({
											street_address: a.street_address.value,
											city: a.city.value,
											state: a.state.value,
											zip: a.zip.value,
											country: a.country.value,
											lat: d.geometry.location.lat(),
											lng: d.geometry.location.lng(),
										});
								}

								if (self.geocoder.addresses.length)
									self.geocoder.goto(0, function () {
										self.geocoder.show();
									});
						    }
						    else { // failed
						    	ahtb.alert("We're sorry, that address could not be geocoded.  Please verify the address.", {height: 180, width: 450});
						    }
						});
					}
					else
						self.DB({
							query: 'geocode-from-address',
							data: address_query,
							done: function (d) {
								if (d.address_components) {
									var a = {
										street_address: {
											complete: 0,
											value: '',
										},
										city: {
											complete: 0,
											value: '',
										},
										state: {
											complete: 0,
											value: '',
										},
										zip: {
											complete: 0,
											value: '',
										},
										country: {
											complete: 0,
											value: '',
										}
									};
									for (var adi in d.address_components) {
										if ($.inArray('street_number', d.address_components[adi].types) > -1) {
											a.street_address.complete++;
											a.street_address.value = d.address_components[adi].long_name + (a.street_address.value.length > 0 ? (' ' + a.street_address.value) : '');
										} else if ($.inArray('route', d.address_components[adi].types) > -1) {
											a.street_address.complete++;
											a.street_address.value = (a.street_address.value.length > 0 ? (a.street_address.value + ' ') : '') + d.address_components[adi].short_name;
										} else if ($.inArray('locality', d.address_components[adi].types) > -1) {
											a.city.complete++;
											a.city.value = d.address_components[adi].long_name;
										} else if ($.inArray("administrative_area_level_1", d.address_components[adi].types) > -1) {
											a.state.complete++;
											a.state.value = d.address_components[adi].short_name;
										} else if ($.inArray('country', d.address_components[adi].types) > -1) {
											a.country.complete++;
											a.country.value = d.address_components[adi].short_name;
										} else if ($.inArray('postal_code', d.address_components[adi].types) > -1) {
											a.zip.complete++;
											a.zip.value = parseInt(d.address_components[adi].long_name);
										}
									}

									if (a.street_address.complete && a.city.complete && a.state.complete && a.country.complete && a.zip.complete)
										self.geocoder.addresses.push({
											street_address: a.street_address.value,
											city: a.city.value,
											state: a.state.value,
											zip: a.zip.value,
											country: a.country.value,
											lat: d.geometry.location.lat,
											lng: d.geometry.location.lng,
										});
								}

								if (self.geocoder.addresses.length)
									self.geocoder.goto(0, function () {
										self.geocoder.show();
									});
							},
							error: function(d) {
								ahtb.alert("We're sorry, that address could not be geocoded.  Please verify the address.", {height: 180, width: 450});
							}
						});
			},
			goto: function (index, callback) {
				if (!this.is_hidden)
					this.hide(index, function () {
						this.goto(index);
						if (typeof callback === 'function') callback();
					});
				else {
					index = parseInt(index);
					var a = this.addresses[index];
					$('#geocode-panel li.address').empty();
					if (typeof a !== 'undefined') {
						this.current = index;
						$('#geocode-panel li.address').html('<img class="map-image" src="https://maps.googleapis.com/maps/api/staticmap?zoom=' + self.geocoder.map_zoom + '&center=' + a.lat + ',' + a.lng + '&size=125x70&markers=size:mid%7Ccolor:0xFFFF00%7C' + a.lat + ',' + a.lng + '" />' +
							'<div class="meta">' +
							'<span class="street_address">' + a.street_address + '</span><br/>' +
							'<span class="city">' + a.city + '</span>, <span class="state">' + a.state + '</span> <span class="zip">' + a.zip + '<span>' +
							'</div>').attr('data-index', index);
						if (typeof callback === 'function')
							callback();
					} else if (typeof callback === 'function')
						callback();
				}
			},
			save_to_listing: function () {
				this.hide();
				self.save_geocode = true;
				var $a = this.addresses[this.current];
				$a['address'] = this.addresses[this.current]['street_address'];
				self.listing.set($a, function () {
					self.save_geocode = false;
					self.is_geocoded = true;

					self.listing.get(parseInt(self.listing_data.id), function(d){
						self.listing_data = d;

						var $arr = [ 'street_address', 'city', 'state', 'zip' ];
						for (var i in $arr){
							$('[data-class='+$arr[i]+']').html( d[$arr[i]] );
						}
					});

					ahtb.close();
				});
			},
			next: function () {
				this.goto(this.current + 1 > this.addresses.length ? 0 : this.current + 1);
			},
			prev: function () {
				this.goto(this.current - 1 > 0 ? this.current - 1 : this.addresses.length - 1);
			},
			hide: function (callback) {
				this.is_hidden = true;
				$('#geocode-panel').stop();
				$('#geocode-panel .close').off('click');
				if (!$('#geocode-panel').hasClass('up') && typeof callback === 'function')
					callback();
				else if ($('#geocode-panel').hasClass('up'))
					$('#geocode-panel').removeClass('up').addClass('animating').animate({
						bottom: '-' + $('#geocode-panel').height()
					}, {
						duration: 350,
						complete: function () {
							$(this).removeClass('animating').addClass('down');
							if (typeof callback === 'function')
								callback();
						}
					});
			},
			show: function (callback) {
				this.is_hidden = false;
				$('#geocode-panel').stop().css('bottom', '-' + $('#geocode-panel').height());
				if (!$('#geocode-panel').hasClass('down') && typeof callback === 'function')
					callback();
				else if ($('#geocode-panel').hasClass('down'))
					$('#geocode-panel').removeClass('down').addClass('animating').animate({
						bottom: 0
					}, {
						duration: 350,
						complete: function () {
							$(this).removeClass('animating').addClass('up');
							$('#geocode-panel .close').one('click', function () {
								self.geocoder.hide();
							});
							if (typeof callback === 'function')
								callback();
						}
					});
			}
		},
		/**
		 * basic displaying functions / page controls
		 * @type {Object}
		 */
		page: {
			/**
			 * get page info and print the page into the setup wizard, including fade in/out animation and footer controls
			 * @param  {string} $page page name (should be in page_list)
			 */
			get_page: function ($page, force) {
				force = force || false;
				var $page_info = null;
				if ($page !== self.currentPage) {
					$page_info = self.page.get_page_info($page);
					self.footer_controls.off();

					if ($page_info) {
						if (force || !self.is_changed) {
							// if $page was in page_list and found info for it
							self.currentPage = $page;
							window.onhashchange = function () {};
							self.hash.set({
								page: $page
							});

							// fade out, update header, change html, fade in, execute done() (if available)
							$('header .nav-link').removeClass('active');
							$('header .nav-link[data-page=' + $page + ']').addClass('active');
							$('footer .controls').fadeOut({
								duration: 250,
								queue: true,
								complete: function () {
									$('footer .controls button.control').hide({
										duration: 250,
										complete: function () {
											switch (self.currentPage) {
											case 'splash':
												$('footer .controls button.control.start').show();
												break;
											case 'create':
												break;
											case 'video':
												$('footer .controls button.control.back, footer .controls button.control.done').show();
												break;
											default:
												$('footer .controls button.control.back, footer .controls button.control.next').show();
												if (self.currentPage != 'tags' &&
													self.listing_data.images &&
													self.listing_data.images.length > 1)
													$('footer .controls button.control.back, footer .controls button.control.arrange').show();
												break;
											}

											$('footer .controls').fadeIn(250, function () {
												self.footer_controls.on();
											});
										}
									});
								}
							});
							$('#page-content').fadeOut(250, function () {

								$(this).attr('class', 'page-' + $page);

								if ($page != 'splash' && $page != 'create') {
									$('#listing-admin > header > a.logo').css({
										'display': 'inline-block',
										'float': 'left'
									});
									$('#listing-admin > header > ul:hidden').fadeIn(250);
								} else {
									$('#listing-admin > header > a.logo').css({
										'display': 'inline-block'
									});
									$('#listing-admin > header > ul:visible').fadeOut(250);
								}

								$(this).html($page_info.html ? $page_info.html() : '<h2 class="loading">404: Page Not Found</h2>').fadeIn({
									duration: 250,
									complete: function () {
										window.onhashchange = function () {
											$old_page = self.hash.page;
											$old_id = self.hash.listingID;
											self.hash.parse();
											if ($old_id !== self.hash.listingID) {
												self = null;
												setupWizard.init();
											} else
												self.page.get_page(self.hash.page);
										};
										
										if ($page == 'splash')
												$('.start-button').on('click', function() {
													self.page.get_page('create');
										});

										if ($page_info.done)
											$page_info.done();
									},
								});
							});

							// progress bar
							if ($page == 'create' || $page == 'splash')
								$("#progress-bar:visible").hide();
							else {
								$("#progress-bar:hidden").fadeIn(250);
								$pages = Object.keys(self.page_list);
								for (var i in $pages)
									if ($pages[i] == $page) {
										$percent = Math.round(100 * (i - 2) / ($pages.length - 2));
										$(".percent").html($percent + "%");
										$("#progress-bar > .bar > .fill").stop().animate({
											width: $percent + '%'
										}, 1000);
										break;
									}
							}
						} else {
							self.footer_controls.on();
							self.unsaved_changes(function () {
								self.page.get_page($page, true);
							});
						}
					} else {
						$('header .nav-link').removeClass('active');
						$('footer .controls').fadeOut({
							duration: 250,
							queue: true
						});
						$('#page-content').attr('class', '404').fadeOut(250, function () {
							if ($page != 'splash' && $page != 'create') {
								$('#listing-admin > header > a.logo').css({
									'display': 'inline-block',
									'float': 'left'
								});
								$('#listing-admin > header > ul:hidden').fadeIn(250);
							}

							$(this).html('<h2 class="loading">404: Page Not Found</h2>').fadeIn(250);
						});
					}
				}
			},
			/**
			 * returns $page from page_list object
			 * @param  {string} $page page name
			 * @param {bool}	$key 		return key instead of value
			 * @return {bool / obj}       returns page obj or false if not found
			 */
			get_page_info: function ($page, $key) {
				if (typeof $key == 'undefined')
					$key = false;
				found = false;
				if (typeof $page !== 'undefined')
					for (var i in self.page_list)
						if (i == $page) {
							found = $key ? i : self.page_list[i];
							break;
						}
				return found;
			},
			/**
			 * go to next page
			 */
			next: function (force) {
				force = force || false;
				if (!self.is_changed || force) {
					$pages = Object.keys(self.page_list);
					$next = false;
					for (var i in $pages)
						if ($pages[i] == self.currentPage) {
							index = parseInt(i) + 1;
							if (index > self.page_list.length)
								self.page.finish();
							else
								$next = $pages[index];
							break;
						}

					if ($next !== false)
						self.page.get_page($next);
				} else
					self.unsaved_changes(function () {
						self.page.next();
					});
			},
			/**
			 * go to previous page
			 */
			prev: function (force) {
				force = force || false;
				if (!self.is_changed || force) {
					$pages = Object.keys(self.page_list);
					$prev = false;
					for (var i in $pages)
						if ($pages[i] == self.currentPage) {
							index = parseInt(i) - 1;
							if (index < 2)
								self.page.get_page('splash');
							else
								$prev = $pages[index];
							break;
						}

					if ($prev !== false)
						self.page.get_page($prev);
				} else
					self.unsaved_changes(function () {
						self.page.prev();
					});
			},
			/**
			 * called when Finish button clicked
			 */
			finish: function (force) {
				force = force || false;
				if (!self.is_changed || force)
					window.location.href = ah_local.wp + '/sellers/#listings';
				else
					self.unsaved_changes(function () {
						self.page.finish(true);
					});
			}
		},
		/**
		 * get / set / parse url hash
		 * @type {Object}
		 */
		hash: {
			page: null,
			listingID: null,
			parse: function () {
				var $h = window.location.hash;
				$h = $h.substr($h.indexOf('#') + 1).split('&');

				for (var i in $h) {
					if ($h[i].substr(0, 5) == 'page=')
						self.hash.page = $h[i].substr(5);
					else if ($h[i].substr(0, 10) == 'listingID=')
						self.hash.listingID = parseInt($h[i].substr(10));
				}
			},
			set: function ($in) {
				if (typeof $in == 'undefined')
					$in = {};

				if (typeof $in.listingID == 'undefined')
					$in.listingID = self.hash.listingID;

				if (typeof $in.page == 'undefined')
					$in.page = self.hash.page;

				self.hash.listingID = $in.listingID;
				self.hash.page = $in.page;

				document.title = self.page_list[$in.page].title + " » Setup Wizard | " + document.title.split(" | ")[1];

				if ($in.page == 'splash' || $in.page == 'create')
					window.location.hash = '';
				else
					window.location.hash = "#listingID=" + $in.listingID + "&page=" + $in.page;
			},
		},
		/**
		 * deals with the background / bg loader
		 * @type {Object}
		 */
		background: {
			current: null,
			init: function () {
				if (self.background.current === null && self.listing_data.images && self.listing_data.images.length) {
					// get rid of non-file images, such as discard...
					for(var k = self.listing_data.images.length - 1; k >= 0; k--)
						if (!self.listing_data.images[k].file)
							self.listing_data.images.splice(k, 1);

					if (self.listing_data.images.length == 0) // all junk images, removed.
						return;

					for (var j in self.listing_data.images) {
						if (!self.listing_data.images[j].file)
							continue;
						self.background.current = {
							src: self.listing_data.images[j].file.substr(0, 7) == 'http://' ? self.listing_data.images[j].file : ah_local.tp + '/_img/_listings/1440x575/' + self.listing_data.images[j].file,
							index: parseInt(j),
							title: self.listing_data.images[j].title || null,
							desc: self.listing_data.images[j].desc || null,
						};
						break;
					}
				} else if (self.listing_data.images && self.listing_data.images.length && typeof self.listing_data.images[self.background.current.index] !== 'undefined')
					self.background.current = {
						src: self.listing_data.images[self.background.current.index].file.substr(0, 7) == 'http://' ? self.listing_data.images[self.background.current.index].file : ah_local.tp + '/_img/_listings/1440x575/' + self.listing_data.images[self.background.current.index].file,
						index: self.background.current.index,
						title: self.listing_data.images[self.background.current.index].title || null,
						desc: self.listing_data.images[self.background.current.index].desc || null,
					};
				else
					self.background.current = null;
			},
			get_html: function ($in) {
				if (typeof $in == 'undefined')
					$in = {};
				if (typeof $in.with_controls == 'undefined')
					$in.with_controls = self.background.with_controls ? true : false;
				else
					self.background.with_controls = $in.with_controls ? true : false;

				var html =  '<ul class="uploaded-images">' +
								'<div class="bgshadow"></div>' +
								'<li class="uploaded-image" data-id=' + (self.background.current ? self.background.current.index : '') + '></li>';

					html += 	'<div class="image-controls" style="display: ' + (self.background.with_controls ? 'block' : 'none') + ';">' +
									'<a class="prev-image entypo-left"></a>' +
									'<ul class="image-pager">';

				if (self.listing_data.images)
					for (var sldii in self.listing_data.images)
						if (self.listing_data.images[sldii].file)
							html += '<li class="listing-dot' + (sldii < 1 ? ' active' : '') + '" data-id="' + sldii + '"></li>';

				html += 			'</ul>' +
									'<a class="next-image entypo-right"></a>' +
								'</div>' +
							'</ul>';

				setTimeout(function () {
					if (self.background.current)
						self.background.goto(self.background.current.index);
					// $('.image-controls a.next-image').off('click').on('click', function () {
					$('.image-controls a.next-image').on('click', function () {
						self.background.next();
					});
					// $('.image-controls a.prev-image').off('click').on('click', function () {
					$('.image-controls a.prev-image').on('click', function () {
						self.background.prev();						
					});
					$('.image-controls ul li').off('click').on('click', function () {
						self.background.goto(parseInt($(this).attr('data-id')));
					});
				}, 800);

				return html;
			},
			setScrollPos: function() {
				if ( self.background.current.index >= self.currentLeftPos &&
					 self.background.current.index < self.currentLeftPos + self.defaults.MAX_LISTING_DOT )
					return;

				var scrollLeft = 0;
				var width = $('.image-controls ul li[data-id='+self.background.current.index+']').width() + 3;
						
				if ( self.background.current.index < self.currentLeftPos ) {
					scrollLeft = scrollLeft = width * self.background.current.index;
					self.currentLeftPos = self.background.current.index;
				}
				else {
					self.currentLeftPos = self.background.current.index - self.defaults.MAX_LISTING_DOT + 1;
					scrollLeft = scrollLeft = width * self.currentLeftPos;
				}
				$(".left-bar div.listing-dot-container").animate({
					scrollLeft: scrollLeft+15
				},'fast');
			},
			goto: function (index, force) {
				force = force || false;
				if (force || !self.is_changed || self.currentPage !== 'images') {
					index = typeof index === 'undefined' ? 0 : parseInt(index);
					if (index === -1 || (self.listing_data.images && typeof self.listing_data.images[index] !== 'undefined' && self.listing_data.images[index].file)) {
						if (index !== -1)
							self.background.current = {
								src: self.listing_data.images[index].file.substr(0, 7) == 'http://' ? self.listing_data.images[index].file : ah_local.tp + '/_img/_listings/1440x575/' + self.listing_data.images[index].file,
								index: index,
								title: self.listing_data.images[index].title || null,
								desc: self.listing_data.images[index].desc || null,
							};

						if (self.currentPage == 'images')
							$('form#image-titles').fadeOut(125);

						$('ul.uploaded-images li.uploaded-image').fadeOut($('ul.uploaded-images li.uploaded-image').is(':visible') ? 250 : 5, function () {
							if (index === -1) {
								if (self.currentPage == 'images')
									$('form#image-titles').fadeOut(250, function () {
										$('form#image-titles .listing-thumbnail').attr({
											src: (self.background.current ? self.background.current.src : null),
											'data-id': (self.background.current ? self.background.current.index : -1)
										});
										$('form#image-titles input[name=title]').val('Error');
										$('form#image-titles textarea[name=description]').val('Unable to load image.');
										$(this).fadeIn(250);
									});

								$('li.uploaded-image').fadeOut(250, function () {
									$(this).attr('src', 'about:blank');
								});
							} else {
								self.background.loading = new Image();
								self.background.loading.onerror = function () {
									if (self.currentPage == 'images')
										$('form#image-titles').fadeOut(250, function () {
											$('form#image-titles .listing-thumbnail').attr({
												src: self.background.current.src,
												'data-id': self.background.current.index
											});
											$('form#image-titles input[name=title]').val('Error');
											$('form#image-titles textarea[name=description]').val('Unable to load image.');
											$(this).fadeIn(250);
										});

									$('.uploaded-image[data-id=' + self.background.current.index + ']').fadeOut(250, function () {
										$(this).html('<span class="entypo-cancel error"></span>').fadeIn(250);
									});
									self.background.loading = null;
								};
								self.background.loading.onload = function () {
									if (self.currentPage == 'images') {
										$('form#image-titles .listing-thumbnail').attr({
											src: self.background.current.src,
											'data-id': self.background.current.index
										});
										$('form#image-titles input[name=title]').val(self.background.current.title ? self.background.current.title : '').bind('input propertychange', function () {
											self.page_list.images.check_for_changes();
										});
										$('form#image-titles textarea[name=description]').val(self.background.current.desc ? self.background.current.desc : '').bind('input propertychange', function () {
											self.page_list.images.check_for_changes();
										});
										$('form#image-titles').fadeIn(250);
									}

									$('.uploaded-image[data-id=' + self.background.current.index + ']').fadeOut(250, function () {
										$(this).html('<img src="' + self.background.current.src + '" />').fadeIn(250);
									});
									self.background.loading = null;
									self.background.setScrollPos();
								};
								self.background.loading.src = self.background.current.src;
								$(this).html('<div class="spin-wrap"><div class="spinner">' +
									'<div class="cube1"></div>' +
									'<div class="cube2"></div>' +
									'</div></div>').attr('data-id', self.background.current.index).fadeIn(250);
							}
							index = index == -1 ? 0 : (self.background.current ? self.background.current.index : index);
							$('li.listing-dot[data-id="'+index+'"]').addClass('active');
						});

						$('ul.image-pager li.active').removeClass('active');
						$('ul.image-pager li[data-id=' + index + ']').addClass('active');
					} else if (self.listing_data.images && index < 75)
						this.goto(index + 1);
				} else
					self.unsaved_changes(function () {
						self.background.goto(index, true);
					});
			},
			clear: function () {
				self.background.goto(-1);
				$('ul.uploaded-images li.uploaded-image').fadeOut(250, function () {
					$(this).attr('src', '');
				});
			},
			next: function () {
				found = false;
				$next_index = self.background.current.index;
				if ($next_index < self.listing_data.images.length - 1)
					for (var i = 1; i < self.listing_data.images.length; i++)
						if (typeof self.listing_data.images[($next_index + i)] !== 'undefined') {
							found = true;
							self.background.goto($next_index + i);
							break;
						}
				if (!found)
					self.background.goto(0);
			},
			prev: function () {
				found = false;
				$next_index = parseInt(self.background.current.index) - 1;
				while (!found && $next_index > -1) {
					if (typeof self.listing_data.images[$next_index] !== 'undefined') {
						found = true;
						self.background.goto($next_index);
						break;
					}
					$next_index--;
				}
				if (!found) {
					$next_index = self.listing_data.images.length;
					found = false;
					while (!found && $next_index > -1) {
						if (typeof self.listing_data.images[$next_index] !== 'undefined') {
							found = true;
							self.background.goto($next_index);
						}
						$next_index--;
					}
				}
			},
			update: function ($new_index) {
				var $html = '';
				var length = 0;
				if (self.listing_data.images && self.listing_data.images.length) {
					for (var sldip in self.listing_data.images)
						if (self.listing_data.images[sldip].file) {
							// $html += '<li class="listing-dot' + (sldip < 1 ? ' active' : '') + '" data-id="' + sldip + '"></li>';
							$html += '<li class="listing-dot" data-id="' + sldip + '"></li>';
						}
				} else
					self.background.current = null;

				if (self.background.current === null) {
					self.background.init();
					self.currentLeftPos = 0;
				}
				else if ( self.listing_data.images.length <= self.defaults.MAX_LISTING_DOT ||
						  self.background.current.index <= (self.defaults.MAX_LISTING_DOT-1) )
					self.currentLeftPos = 0;
				else if ( self.background.current.index <= self.currentLeftPos)
					self.currentLeftPos = self.background.current.index;
				else if ( self.background.current.index >= (self.currentLeftPos + self.defaults.MAX_LISTING_DOT))
					self.currentLeftPos = self.background.current.index - self.defaults.MAX_LISTING_DOT + 1;

				if (self.background.current) {
					if (self.background.current.index >= self.listing_data.images.length)
							self.background.current.index = self.listing_data.images.length - 1;
					self.background.goto($new_index || self.background.current.index);
				}
				else
					self.background.clear();

				$('ul.image-pager').html($html);
				$('.image-controls ul li').off('click').on('click', function () {
					self.background.goto(parseInt($(this).attr('data-id')));
				});
			}
		},
		footer_controls: {
			on: function () {
				self.footer_controls.off();
				$('footer .controls button.control').prop('disabled', false).each(function () {
					if ($(this).hasClass('start') || $(this).hasClass('next'))
						$(this).on('click', function () {
							self.page.next();
						});
					else if ($(this).hasClass('back'))
						$(this).on('click', function () {
							self.page.prev();
						});
					else if ($(this).hasClass('done'))
						$(this).on('click', function () {
							self.page.finish();
						});
					else if ($(this).hasClass('arrange'))
						$(this).on('click', function () {
							self.page_list.images.shufflePhotos();
						});
				});
				self.currentPage == 'images' ? $('footer .footnote').show() : $('footer .footnote').hide();				
			},
			off: function () {
				$('footer .controls button.control').off('click').prop('disabled', true);
			}
		},
		unsaved_changes: function ($on_submit) {
			ahtb.open({
				height: 175,
				width: 500,
				title: 'Unsaved Changes',
				html: '<p>You have unsaved changes that will be lost,<br/>click OK to discard changes and continue.</p>',
				buttons: [
					{
						text: 'OK',
						action: function () {
							if (typeof $on_submit === 'function')
								$on_submit();
							self.is_changed = false;
							ahtb.close();
						}
					}, {
						text: 'Cancel',
						action: function () {
							ahtb.close();
						}
					}
				]
			});
		},
		DB: function (xx) {
			if (xx) {

				if (!xx.data)
					xx.data = {};

				xx.data.author = ah_local.author_id;
				self.dbBusy = true;
				$.post(ah_local.tp + '/_sellers/_ajax.php', {
					query: xx.query,
					data: xx.data
				}, function () {}, 'JSON')
				.done(function (d) {
					self.dbBusy = false;
					if (d.status != 'OK' && xx.error)
						xx.error(d.data);
					else if (d.status != 'OK')
						ahtb.open({
							hideTitle: true,
							height: 125,
							width: 500,
							html: '<p>There was a problem.</p><p style="font-size: 0.6em; width:90%; line-height:1.3em; margin: 0 auto;">' + d.data + '</p>',
						});
					else if (xx.done !== null)
						xx.done(d.data);
					else
						console.log(d.data);
				})
				.error(function(d){
					self.dbBusy = false;
					ahtb.alert("Database error, sorry!", {height: 150});
				})
			}
		},
		deepCopy: function (obj) {
			return JSON.parse(JSON.stringify(obj)); // makes copy of it
		    // if (Object.prototype.toString.call(obj) === '[object Array]') {
		    //     var out = [], i = 0, len = obj.length;
		    //     for ( ; i < len; i++ ) {
		    //         out[i] = arguments.callee(obj[i]);
		    //     }
		    //     return out;
		    // }
		    // if (typeof obj === 'object') {
		    //     var out = {}, i;
		    //     for ( i in obj ) {
		    //         out[i] = arguments.callee(obj[i]);
		    //     }
		    //     return out;
		    // }
		    // return obj;
		}
	};
	setupWizard.init();
});