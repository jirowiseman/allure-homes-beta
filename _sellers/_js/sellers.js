var seller;
var dz;
var files;
var timeoutID = 0;
var timeoutID2 = 0;

var UpdateAuthorOrigin = {
	Normal: 0,
	Exit: 1,
	PageChange: 2,
	ViewAgentPage: 3,
	PortalMessage: 4,
	Dashboard: 5
}

var IC_CodeType = {
	IC_USED: 1,
	IC_EXPIRE_IN_30: 2,
	IC_EXPIRE_IN_180: 4,
	IC_EXPIRE_IN_365: 8,
	IC_FEATURE_PORTAL: 16,
	IC_FEATURE_LIFESTYLE: 32,
	IC_FEATURE_LIFETIME: 64
}

var stateKeys = []; //Array.isArray(usStates) ? Array.keys(usStates) : Object.keys(usStates);

window.onbeforeunload = function (e) {
	var e = e || window.event;
	if (seller.am_order) {
		$('.define-agent-match-container #define-agent-match #description').each(function() {
			var item = $(this).attr('for');
			var desc = $(this).val();
			seller.am_order.item[item].desc = desc;
			console.log("AM desc for:"+item+", is:"+desc);
		});
		seller.DBget({
			query: 'update-agent-match-order',
			data: {order: seller.am_order,
				   id: validSeller.id  },
			done: function(d) {
				console.log("update-agent-match-order - "+d.status);
				seller.page.profile.info = d.seller;
				validSeller = d.seller;
				// ahtb.close();							
    		},
			error: function(d) {
				console.log("We're very sorry, had trouble updating the descriptions. "+d.status);
				// ahtb.alert("We're very sorry, had trouble updating the descriptions. "+d, {height: 150, width: 450});
			}
		});
	}
	if (seller.currentPage == 'profile') seller.page.profile.updateAuthor( UpdateAuthorOrigin.Exit );
};

var ActiveStateNames = [
	'inactive',
	'active',
	'waiting',
	'criteria not met',
	'below min price',
	'requirements unmet', // 'commercial',
	'below min price'
]

function listingErrorCode(err) {
	var errors = '';
	err = parseInt(err);
	for(var i in ListingErrorCode)
		if ( (err & ListingErrorCode[i]) != 0)
			errors += (errors.length ? ', ' : '')+ListingErrorCodeStr[ListingErrorCode[i]];
	return errors;
}

var Languages = [
	'English',
	'Mandarin',
	'Cantonese',
	'Spanish',
	'French',
	'Japanese',
	'Russian',
	'Portugese',
	'Vietnamese',
	'Korean',
	'Italian',
	'Indian'];

var YearsRange = 10;
var eCommerceReady = true;
var EMAIL_BANNER_COUNT = 10;
var maxTaglineLen = 50;

var ActiveStatPage = {
	PortalStats: 1,
	ListingStats: 2,
	ActivityStats: 3
}

function makeSelector(id, title, type, family, tag, checked) {
	checked = parseInt(checked);
	var h = '<div class="selection" id="'+id+'" >' +
				'<input type="'+type+'" name="'+family+'" id="'+id+'" value="'+tag+'" '+(checked ? 'checked' : '')+'>' +
				'<span class="matchTitle" >'+title+'</span>' +
				'<a href="javascript:;" class="desc'+(checked ? '' : ' hidden')+'" name="'+family+'" value="'+tag+'" >Edit description</a>' +
			'</div>';
	return h;
}

function loadedLinkedIn() {
	console.log("loadedLinkedIn called");
}

// ---- Seller's Menu Functions ---- //
	function sellerMenuClose() {
		$('.mobile-profile-menu .profile-menu li').removeClass( 'animatein' );
		$('.centerarrow .arrow').removeClass( 'active' );
		$('.centerarrow .arrow .left').removeClass( 'active' );
		$('.centerarrow .arrow .right').removeClass( 'active' );
        function delayanimateup() {
			$('.mobile-profile-menu .profile-menu').addClass( 'animateup' );
			$('.mobile-profile-menu .profile-menu').removeClass( 'animatedown' );
			timeoutID = 0;
		}
		timeoutID = window.setTimeout(delayanimateup, 750);
		function delayanimatehide() {
			$('.mobile-profile-menu .profile-menu').removeClass( 'animateup' );
			timeoutID2 = 0;
		}
		timeoutID2 = window.setTimeout(delayanimatehide, 1750);
	}

	function sellerMenuOpen() {
		$('.mobile-profile-menu .profile-menu').addClass( 'animatedown' );
		$('.mobile-profile-menu .profile-menu').removeClass( 'animateup' );
		$('.centerarrow .arrow').addClass( 'active' );
		$('.centerarrow .arrow .left').addClass( 'active' );
		$('.centerarrow .arrow .right').addClass( 'active' );
		timeoutID = window.setTimeout(delayanimatedown, 200);
		function delayanimatedown() {
			$('.mobile-profile-menu .profile-menu li').addClass( 'animatein' );
			timeoutID = 0;
		}
	}

// ---- Seller's Menu Transitions ---- //
	function sellerTransitionStart() {
		$('.loading-overlay-sellers').show();
	}

	function sellerTransitionEnd() {
		function delayopacity() {
			$('.loading-overlay-sellers').css('opacity', '0');
			timeoutID = 0;
		}
		function delayhide() {
			$('.loading-overlay-sellers').hide();
			$('.loading-overlay-sellers').css('opacity', '1');
			timeoutID2 = 0;
		}
		timeoutID = window.setTimeout(delayopacity, 500);
		timeoutID2 = window.setTimeout(delayhide, 1000);
	}

	function CreateElementForExecCommand (textToClipboard) {
        var forExecElement = document.createElement ("div");
            // place outside the visible area
        forExecElement.style.position = "absolute";
        forExecElement.style.left = "-10000px";
        forExecElement.style.top = "-10000px";
            // write the necessary text into the element and append to the document
        forExecElement.textContent = textToClipboard;
        document.body.appendChild (forExecElement);
            // the contentEditable mode is necessary for the  execCommand method in Firefox
        forExecElement.contentEditable = true;

        return forExecElement;
    }

    function SelectContent (element) {
            // first create a range
        var rangeToSelect = document.createRange ();
        rangeToSelect.selectNodeContents (element);

            // select the contents
        var selection = window.getSelection ();
        selection.removeAllRanges ();
        selection.addRange (rangeToSelect);
    }


jQuery(document).ready(function($){
	stateKeys = Array.isArray(usStates) ? Array.keys(usStates) : Object.keys(usStates);
	var cities = [];
	var cityName = '';
	for (var i in ah_local.cities)
	    cities[i] = {label: ah_local.cities[i].label, value: ah_local.cities[i].label, id: ah_local.cities[i].id};

	seller = new function(){
		this.meta = null;
		this.am_meta = null; // agent match meta
		this.mod_meta = null; // agent modifed profile fields, not to be overwritten by the parser
		this.am_order = null; // agent match order list, past, present, future, retired
		this.visitations = null;
		this.visitationCities = visitationCities;
		this.visitationsListings = null;
		this.portalVisitCount = 0;
		this.queryIPCount = 0;
		this.specialty_checked = -1;
		this.lifestyle_count = 0;
		this.specialty_count = 0;
		this.editingTag = 0;
		this.tagFamily = '';
		this.initial_listings = null;
		this.am_agree_to_terms = 0;
		this.am_invitation = null;
		this.doingSales = false;
		this.monthly = true;
		this.priceLevel = 4;
		this.subscriptionType = ProductType.MONTHLY_SUBSCRIPTION;
		this.addSignUpFee = false;
		// new portal stuff
		this.portalSpec = { subscriptionType: 0,
							priceLevel: PricingLevels.GRADE_2};
		this.clipboardData = '';
		this.clipboardImage = '';
		this.clipboardImageLinkedIn = '';
		this.zeroClipboard = null;
		this.statPage = havePortal ? ActiveStatPage.PortalStats : ActiveStatPage.ListingStats;
		this.showStatTagImgs = false;
		this.showSiteEntryStatsInActivityPage = showSiteEntryStatsInActivityPage;
		this.showAllListingDetails = false;
		this.queuedPage = '';
		this.haveHidden = false;

		this.fbURL = 'https://www.facebook.com/sharer/sharer.php?u='+ah_local.wp+"/"+nickname;
		this.gpURL = 'https://plus.google.com/share?url='+ah_local.wp+"/"+nickname;
		this.twURL = 'https://twitter.com/share?url='+ah_local.wp+"/"+nickname;

		this.login = function() {
	        $('.global-signin-popup').show();
	        // tempSignInScrollTop = $(window).scrollTop();
	        $('body').css('overflow','hidden');
			if (isMobile)
				$('body').css('position','fixed');
	        $('.global-signin-popup .signin-content .close').on('click', function() {
	          closeLoginForm();
	        });
	    }
	    function closeLoginForm() {
			$('.signin-content').hide();
	      	window.location = ah_local.wp;
	    }

		this.init = function(){			
			if (typeof ahtb == 'undefined' ||
				!ahtb) {
				window.setTimeout(function() {
					seller.init();
				}, 200);
				return;
			}

			if (!eCommerceReady) {
				var ele = $('.profile-menu li[data-page="account"]');
				ele.hide();
				ele = $('.profile-menu li[data-page="cart"]');
				ele.hide();
			}

			// ['cut', 'copy', 'paste'].forEach(function(event) {
			// ['copy'].forEach(function(event) {
			//     document.addEventListener(event, function(e) {
			//         console.log(event);
			//         // focusHiddenArea();
			//         var ele = $('div.clipboard-manual #data-clipboard');
			//         ele.dblclick();
			//         e.preventDefault();
			//     });
			// });

			this.oldTitle = document.title;
			$('#seller-content').hide();
			if (validSeller == 1) {
				ahtb.open({
				    title: 'Access Restriction',
				    height: 205,
				    width: 580,
				    html: '<p style="text-align:center;font-size:2em;font-family:Perpetua;width:95%;margin:.5em auto 0;letter-spacing:.025em;">You are not yet a verified agent.</p><p style="font-size:.9em;width:95%;margin:.75em auto;">Please follow the link in the email we sent you. If you have not received an email, hit resend to get another.</p>',
				    buttons:[
							{text:"OK", action:function(){ window.location = ah_local.wp; }},
							{text:"Resend", action:function(){ 
								$.ajax({
									type: "POST",
									dataType: "json",
									url:  ah_local.tp+"/_pages/ajax-register.php",
									data: { query: 'resend-verification' },
									error: function($xhr, $status, $error){
										var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
										console.log("failed ip:"+msg);
										ahtb.open({ title: 'You Have Encountered an Error during Resend', 
													height: 150, 
													html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>',
													buttons: [
														{text:"OK", action:function(){ window.history.back(); }}
													]
												});
									},		
									success: function(d){
										ahtb.open({ title: 'Completed', 
													height: 240, 
													html: '<p>'+d.data+'</p>',
													buttons: [
														{text:"OK", action:function(){ window.history.back(); }}
													]
												});
									}
								})
							}},
						],
				    closed: function(){
				      window.history.back(); 
				      // ahtb.showClose(250);
				      // ahtb.closeOnClickBG(1);
				    }
				});
			}
			else if (validSeller == 0) {
				if (window.location.hash.length)
				$('input[name=redirect_to]').val(ah_local.wp+'/sellers/'+window.location.hash);
				seller.login();
				// ahtb.open({
				//     title: 'Access Restriction',
				//     height: 185,
				//     width: 550,
				//     html: '<p>You are not an agent.  Please register as an agent to access the agent page.</p>',
				//     	buttons:[
				// 			{text:"OK", action:function(){ window.location = ah_local.wp; }},
				// 		],
				//     closed: function(){
				//       window.history.back(); 
				//       // ahtb.showClose(250);
				//       // ahtb.closeOnClickBG(1);
				//     }
				// });
			}
			else if (validSeller == 2) {
				ahtb.open({
				    title: 'Access Restriction',
				    height: 185,
				    width: 550,
				    html: '<p>You are an admin.  Please create a sellers account and retry.</p>',
				    	buttons:[
							{text:"OK", action:function(){ window.history.back(); }},
						],
				    closed: function(){
				      window.history.back(); 
				      // ahtb.showClose(250);
				      // ahtb.closeOnClickBG(1);
				    }
				});
			}
			else {
				seller.createDescDateOrderedKeys();
				seller.getPage(window.location.hash ? window.location.hash.slice(1) : 'home');
				if (clearBrowserUrl)
					window.history.pushState('pure','Title',ah_local.wp+'/sellers');
			}

			$('div#portal-user-activity button').on('click', function() {
				$('div#portal-user-activity').fadeOut({duration: 1000});
				$('#seller-content').animate({opacity: 1},{duration: 500});
			});

			$('div#details button').on('click', function() {
				$('div#details').fadeOut({duration: 1000});
				$('div#portal-user-activity').animate({opacity: 1},{duration: 500});
			});
		}

		this.getPortalUser = function(id) {
			for(var i in portalUsersList)
				if (portalUsersList[i].id == id)
					return portalUsersList[i];

			return null;
		}

		// this.shareTweet = function() {
		// 	if (seller.clipboardImageLinkedIn.length == 0)
		// 		seller.clipboardImageLinkedIn = ah_local.tp+"/_img/_banners/linkedin/Linked-in-1.jpg";
		// 		// seller.clipboardImageLinkedIn = "http://lifestyledlistings.com/wp-content/themes/allure/_img/_banners/linkedin/Linked-in-1.jpg";

		// 	var url = 'https://twitter.com/share?url='+ah_local.wp+"/"+nickname+'&image-src='+seller.clipboardImageLinkedIn; //+'&text=Share%20my%20portal';
		// 	window.open(url, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
		// 	return;

		// 	// $.post(url,
		// 	// 		{},
		// 	// 		function(){},'json')
		// 	// .done(function(data){
		// 	// 	console.log("got back: code"+data.code+", state:"+code.state); 
		// 	// })
		// 	// .error(function(x) {
		// 	// 	var msg = typeof x == 'string' ? x :
		// 	// 			  typeof x == 'object' ? JSON.stringify(x) :
		// 	// 			  "Unknown reason";
		// 	// 	ahtb.alert("Failure tweeting: "+msg);
		// 	// });

		// }

		this.shareSocialNetwork = function(which) {
			seller.completeStep(which, 'socialMedia');
			switch (which) {
				case "FB":
					shareSocialNetwork(which, 2, seller.fbURL);
					break;
				case "GP":
					shareSocialNetwork(which, 2, seller.gpURL);
					break;
				case "TW":
					shareSocialNetwork(which, 2, seller.twURL);
					break;
			}
		}

		// this.shareFB = function() {
		// 	FB.ui({
		// 	  method: 'share',
		// 	  href: 'https://developers.facebook.com/docs/'
		// 	}, function(response){});
		// }

		// this.loggedIntoLinkedIn = function() {
		// 	var goodToGo = IN.User.isAuthorized();
		// 	console.log("shareLinkedIn, goodToGo:"+goodToGo);

		// 	if (!goodToGo) {
		// 		ahtb.aler("Failed to log into LinkedIn");
		// 		return;
		// 	}

		// 	if (seller.clipboardImageLinkedIn.length == 0)
		// 		seller.clipboardImageLinkedIn = ah_local.tp+"/_img/_banners/linkedin/Linked-in-1.jpg";

		// 	var clipboardData = {
		// 			  "comment": "Hi friends check out this new way to find homes that fit your lifestyle at LifeStyledListings.com!",
		// 			  "content": {
		// 			    "title": "My Portal at LifeStyled Listings",
		// 			    "description": "Check out all the listings that match your lifestyle!",
		// 			    // "submitted-url": ah_local.wp+'/'+nickname,  
		// 			    "submitted-url": ah_local.wp+'/'+nickname,  
		// 			    "submitted-image-url": seller.clipboardImageLinkedIn
		// 			  },
		// 			  "visibility": {
		// 			    "code": "anyone"
		// 			  }  
		// 			}

		// 	console.log("Logged into LinkedIn");
		// 	IN.API.Raw('/people/~/shares?format=json')
		// 		.method("POST")
		// 		.body( JSON.stringify(clipboardData) )
		// 		.result(function(x) {
		// 			var msg = typeof x == 'string' ? x :
		// 					  typeof x == 'object' ? JSON.stringify(x) :
		// 					  "Unknown reason";
		// 			console.log(msg);
		// 			var h = '<div id="gotoLinkedIn">Uploading portal link to LinkedIn was successful.  Press '+
		// 					'<a href="'+x.updateUrl+'" target="_blank">here</a>'+
		// 					' to be directed to LinkedIn to edit as you see fit.</div>';
		// 			ahtb.alert(h,
		// 					   {width: 500,
		// 					    height: 210});
		// 		})
		// 		.error(function(x) {
		// 			var msg = typeof x == 'string' ? x :
		// 					  typeof x == 'object' ? JSON.stringify(x) :
		// 					  "Unknown reason";
		// 			console.log(msg);
		// 			msg = x.message == 'Do not post duplicate content' ? "You already posted to linkedin. To post again, please click on a different banner image from the images below and click on the linkedin share button again." : x.message;
		// 			ahtb.alert(msg,
		// 					   {width: 450,
		// 					    height: 140+((msg.length/60)*30)});
		// 		});
		// }

		this.shareLinkedIn = function() {
			if (seller.clipboardImageLinkedIn.length == 0)
				seller.clipboardImageLinkedIn = ah_local.tp+"/_img/_banners/linkedin/Linked-in-1.jpg";	

			var data = {comment:"Hi friends check out this new way to find homes that fit your lifestyle at LifeStyledListings.com!",
						title:"My Portal at LifeStyled Listings",
						description:"Check out all the listings that match your lifestyle!",
						url:ah_local.wp+'/'+nickname,
						img:seller.clipboardImageLinkedIn,
						which: 2
					  };
			shareLinkedIn(data);
			seller.completeStep('LI', 'socialMedia');
		}

		this.encourageListingViewing = function() {
			var h = '<div id="improve-listing-views"><p>To increase the chances that your listing will show up in the quiz results and viewed, there are steps you '+
					'can take to make that happen.<br/>'+
					'&nbsp;&nbsp;&#149;&nbsp;use HD images<br/>'+
					'&nbsp;&nbsp;&#149;&nbsp;add tags to highlight features of the listing to match tags that users might use in their search<br/>'+
					'&nbsp;&nbsp;&#149;&nbsp;send us a message so we can score your city better, helping the city&#39;s odds of matching user&#39;s tags<br/>'+
					'&nbsp;&nbsp;&#149;&nbsp;complete all information about your listing, such as bed/bath count, address (make sure it geocodes), price, etc.<br/>' +
					'&nbsp;&nbsp;&#149;&nbsp;add better description to the listing (verbage)'+
					'</div>';
			ahtb.open({
					html: h,
					width: 650,
					height: 340
			});
		}

		this.buildVisitationStatsListings = function() {
			var d = seller.sellerData.listings; 
			if (!length(d))
				return;

			if ( typeof seller.visitationsListings != 'undefined' &&
				 seller.visitationsListings != null ) 
				return;

			seller.visitationsListings = [];

			for(var listing in d) { // each listing is a row of table
				var visitations = seller.updateVisitationsListing(d[listing]); // update seller.visitationCities[] with any new ip's and get the meta data
				var oneListing = [];
				if (!visitations)
					oneListing = {	id: d[listing].id,
									street_address: typeof d[listing].street_address == 'undefined' || d[listing].street_address == null ? 'ID:'+d[listing].id : d[listing].street_address,
									users: {html: 'None',
											sub: {}
							   				}
						 		 };
				else {
					var id = length(visitations.users) ? parseInt(Object.keys(visitations.users)[0]) : portalUsersOffset + parseInt(Object.keys(visitations.portalUsers)[0]); // this is the first userId
					var usersCount = length(visitations.users) + length(visitations.portalUsers);

					oneListing = {	id: d[listing].id,
									street_address: typeof d[listing].street_address == 'undefined' || d[listing].street_address == null ? 'ID:'+d[listing].id : d[listing].street_address,
									users: {html: usersCount > 1 ? '<div class="dropdown-arrow"><span class="entypo-down-open"></span></div><select id="user-list" for="'+d[listing].id+'" listing-id="'+d[listing].id+'">' : visitorNames[id],
											current: id,
											sub: {}
										   }
					 		  	 };
					
					for(id in visitations.users) {
						if (usersCount > 1)
							oneListing.users.html += '<option value="'+id+'">'+visitorNames[id]+'</option>';
						var ip = Object.keys(visitations.users[id])[0]; // this is the first ip
						var ipCount = length(visitations.users[id]);
						oneListing.users.sub[id] = {html:'', current: ip, sub:{}};
						oneListing.users.sub[id].html = ipCount > 1 ? '<div class="dropdown-arrow"><span class="entypo-down-open"></span></div><select id="ip-list" user-id="'+id+'" listing-id="'+d[listing].id+'">' : seller.visitationCities[ ip ];
						for(ip in visitations.users[id]) {
							if (ipCount > 1)
								oneListing.users.sub[id].html += '<option value="'+ip+'">'+seller.visitationCities[ ip ]+'</option>';
							var isArray = Array.isArray(visitations.users[id][ip]); // then old-school, no tags, just array of dates
							var date = !isArray ? Object.keys(visitations.users[id][ip])[0] : visitations.users[id][ip][0]; // this is the first date
							var dateCount = length(visitations.users[id][ip]);
							oneListing.users.sub[id].sub[ip] = {html:'', current: date, sub:{}};
							oneListing.users.sub[id].sub[ip].html = dateCount > 1 ? '<select id="date-list" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : seller.page.stats.convertDate(date);
							for(date in visitations.users[id][ip]) {
								date = isArray ? visitations.users[id][ip][date] : date;
								if (dateCount > 1)
									oneListing.users.sub[id].sub[ip].html += '<option value="'+date+'">'+(!isArray ? seller.page.stats.convertDate(date) : seller.page.stats.convertDate( visitations.users[id][ip][date] ))+'</option>';
								oneListing.users.sub[id].sub[ip].sub[date] = {html:'', init: '', current: '0', tagCount: 0};
								var tagCount = !isArray && length(visitations.users[id][ip][date]);
								if (tagCount) {
									oneListing.users.sub[id].sub[ip].sub[date].tagCount = tagCount;
									var firstSetKey = Object.keys(visitations.users[id][ip][date])[0]; // this is the first date
									var tags = typeof visitations.users[id][ip][date][firstSetKey] == 'string' ? visitations.users[id][ip][date][firstSetKey] : visitations.users[id][ip][date][firstSetKey].tagList;
									oneListing.users.sub[id].sub[ip].sub[date].html =  tagCount > 1 ? '<ul class="tags" id="tag-list" date="'+date+'" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : tags;
									oneListing.users.sub[id].sub[ip].sub[date].html2 =  tagCount > 1 ? '<ul class="tags" id="tag-list" date="'+date+'" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : tags;
									oneListing.users.sub[id].sub[ip].sub[date].init = tags;
									for(set in visitations.users[id][ip][date]) {
										if (tagCount > 1) {
											tags = typeof visitations.users[id][ip][date][set] == 'string' ? visitations.users[id][ip][date][set] : visitations.users[id][ip][date][set].tagList;
											oneListing.users.sub[id].sub[ip].sub[date].html += '<li value="'+tags+'">'+seller.convertToTagImgs(tags)+'</li>';
											oneListing.users.sub[id].sub[ip].sub[date].html2 += '<li value="'+tags+'">'+seller.convertToTagStrings(tags)+'</li>';
										}
									}
									if (tagCount > 1) {
										oneListing.users.sub[id].sub[ip].sub[date].html += '</ul>'; // close off
										oneListing.users.sub[id].sub[ip].sub[date].html2 += '</ul>'; // close off
									}
								}
								else {
									oneListing.users.sub[id].sub[ip].sub[date].html = 'No tags';
									oneListing.users.sub[id].sub[ip].sub[date].init = 'No tags';
								}
							}
							if (dateCount > 1)
								oneListing.users.sub[id].sub[ip].html += '</select>'; // close off
						}
						if (ipCount > 1)
							oneListing.users.sub[id].html += '</select>'; // close off
					}

					for(pid in visitations.portalUsers) {
						var id = parseInt(pid) + portalUsersOffset;
						if (usersCount > 1)
							oneListing.users.html += '<option value="'+id+'">'+visitorNames[id]+'</option>';
						var ip = Object.keys(visitations.portalUsers[pid])[0]; // this is the first ip
						var ipCount = length(visitations.portalUsers[pid]);
						oneListing.users.sub[id] = {html:'', current: ip, sub:{}};
						oneListing.users.sub[id].html = ipCount > 1 ? '<div class="dropdown-arrow"><span class="entypo-down-open"></span></div><select id="ip-list" user-id="'+id+'" listing-id="'+d[listing].id+'">' : seller.visitationCities[ ip ];
						for(ip in visitations.portalUsers[pid]) {
							if (ipCount > 1)
								oneListing.users.sub[id].html += '<option value="'+ip+'">'+seller.visitationCities[ ip ]+'</option>';
							var isArray = Array.isArray(visitations.portalUsers[pid][ip]); // then old-school, no tags, just array of dates
							var date = !isArray ? Object.keys(visitations.portalUsers[pid][ip])[0] : visitations.portalUsers[pid][ip][0]; // this is the first date
							var dateCount = length(visitations.portalUsers[pid][ip]);
							oneListing.users.sub[id].sub[ip] = {html:'', current: date, sub:{}};
							oneListing.users.sub[id].sub[ip].html = dateCount > 1 ? '<select id="date-list" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : (!isArray ? date : visitations.portalUsers[pid][ip][date]);
							for(date in visitations.portalUsers[pid][ip]) {
								date = isArray ? visitations.portalUsers[pid][ip][date] : date;
								if (dateCount > 1)
									oneListing.users.sub[id].sub[ip].html += '<option value="'+date+'">'+(!isArray ? date : visitations.portalUsers[pid][ip][date])+'</option>';
								oneListing.users.sub[id].sub[ip].sub[date] = {html:'', init: '', current: '0', tagCount: 0};
								var tagCount = !isArray && length(visitations.portalUsers[pid][ip][date]);
								if (tagCount) {
									oneListing.users.sub[id].sub[ip].sub[date].tagCount = tagCount;
									var firstSetKey = Object.keys(visitations.portalUsers[pid][ip][date])[0]; // this is the first date
									var tags = typeof visitations.portalUsers[pid][ip][date][firstSetKey] == 'string' ? visitations.portalUsers[pid][ip][date][firstSetKey] : visitations.portalUsers[pid][ip][date][firstSetKey].tags;
									oneListing.users.sub[id].sub[ip].sub[date].html =  tagCount > 1 ? '<ul class="tags" id="tag-list" date="'+date+'" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : tags;
									oneListing.users.sub[id].sub[ip].sub[date].html2 =  tagCount > 1 ? '<ul class="tags" id="tag-list" date="'+date+'" ip="'+ip+'" user-id="'+id+'" listing-id="'+d[listing].id+'">' : tags;
									oneListing.users.sub[id].sub[ip].sub[date].init = tags;
									for(set in visitations.portalUsers[pid][ip][date]) {
										if (tagCount > 1) {
											tags = typeof visitations.portalUsers[pid][ip][date][set] == 'string' ? visitations.portalUsers[pid][ip][date][set] : visitations.portalUsers[pid][ip][date][set].tags;
											oneListing.users.sub[id].sub[ip].sub[date].html += '<li value="'+tags+'">'+seller.convertToTagImgs(tags)+'</li>';
											oneListing.users.sub[id].sub[ip].sub[date].html2 += '<li value="'+tags+'">'+seller.convertToTagStrings(tags)+'</li>';
										}
									}
									if (tagCount > 1) {
										oneListing.users.sub[id].sub[ip].sub[date].html += '</ul>'; // close off
										oneListing.users.sub[id].sub[ip].sub[date].html2 += '</ul>'; // close off
									}
								}
								else {
									oneListing.users.sub[id].sub[ip].sub[date].html = 'No tags';
									oneListing.users.sub[id].sub[ip].sub[date].init = 'No tags';
								}
							}
							if (dateCount > 1)
								oneListing.users.sub[id].sub[ip].html += '</select>'; // close off
						}
						if (ipCount > 1)
							oneListing.users.sub[id].html += '</select>'; // close off
					}
					if (usersCount > 1)
						oneListing.users.html += '</select>'; // close off
				} // end else
				seller.visitationsListings[oneListing.id] = oneListing;
			} // end for()
		}

		this.convertToTagImgs = function(html) {
			if (html == 'No tags' ||
				html == '0')
				return 'No tags';

			var h = '';
			var tags = html.split(",");
			for(var i in tags) {
				if (typeof allTags[tags[i]] != 'undefined')
					h += '<img src="'+ah_local.tp+'/_img/tag_icons/'+(typeof allTags[tags[i]].icon != 'undefined' && allTags[tags[i]].icon != null ? allTags[tags[i]].icon : "Default.png")+'" alt="'+allTags[tags[i]].tag+'" title="'+allTags[tags[i]].tag+'" />';
			}
			return h;
		}

		this.convertToTagStrings = function(html) {
			if (html == 'No tags' ||
				html == '0')
				return 'No tags';

			var h = '<span id="tagList">';
			var tags = html.split(",");
			var count = 0;
			for(var i in tags) {
				if (typeof allTags[tags[i]] != 'undefined') {
					h += (count ? ', ' : '')+allTags[tags[i]].tag;
					count++;
				}
			}
			h += '</span>';
			return h;
		}

		this.populateVisitationsListings = function() {

			var d = seller.sellerData.listings; 
			if (!length(d)) {
				return 	'<h1><a href="javascript:seller.encourageListingViewing();">Listing Stats</a></h1>' +
						'<div id="stat-div-listing-visits">'+
							'<span>No listings currently under your name, click on <a href="'+ah_local.wp+'/new-listing">Add Listing</a> and make one now!</span>' +
						'</div>';
			}

			seller.buildVisitationStatsListings();

			var table = '<h1><a href="javascript:seller.encourageListingViewing();">Listing Stats</a></h1>' +
						'<div id="stat-div-listing-visits" class="container">'+
							'<table id="stat-table-listing">' +
								'<thead>'+
									'<th>Listing</th><th>User</th><th>Location</th><th>Dates</th><th>Tags</th><th>Clicks</th><th>Impressions</th>'+
								'</thead>'+
								'<tbody>';
								for(var index in d) { // each listing is a row of table
									var listing = d[index].id;
							table +='<tr id="'+seller.visitationsListings[listing].id+'">' +
										'<td>'+seller.visitationsListings[listing].street_address+'</td>' + // Listing
										'<td><div id="users">'+seller.visitationsListings[listing].users.html+'</div></td>';	// User
										var usersCount = length(seller.visitationsListings[listing].users.sub);
										
										if ( !usersCount ) {
											table += '<td><span>Click <a href="javascript:seller.encourageListingViewing();">here</a></span></td></tr>'; // Location
											continue;
										}
										// var userId = Object.keys(seller.visitationsListings[listing].users.sub)[0]; // this is the first 
										var userId = seller.visitationsListings[listing].users.current; // this is the current one 
							table += 	'<td><div id="location">'+seller.visitationsListings[listing].users.sub[userId].html+'</div></td>'; // userId
										// var ip = Object.keys(seller.visitationsListings[listing].users.sub[userId].sub)[0]; // this is the first 
										var ip = seller.visitationsListings[listing].users.sub[userId].current; // this is the current one 
							table +=	'<td><div id="dates">'+seller.visitationsListings[listing].users.sub[userId].sub[ip].html+'</div></td>'; // IP

										// var date = Object.keys(seller.visitationsListings[listing].users.sub[userId].sub[ip].sub)[0]; // this is the first 
										var date = seller.visitationsListings[listing].users.sub[userId].sub[ip].current; // this is the current one 
										var tagCount = seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].tagCount;
										if (tagCount) {
											var html = seller.showStatTagImgs ? seller.convertToTagImgs(seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].init) :
																				seller.convertToTagStrings(seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].init);
								table +=	'<td><div id="tags">'+
													'<span class="selectArrow" style="visibility: '+(tagCount > 1 ? "visible" : "hidden" )+'"></span>' +
													'<div class="selected">'+html+'</div>' +
													(tagCount > 1 ? seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].html : '')+
												'</div>'+
											'</td>'; // Tags
										}	
										else
								table +=	'<td><div id="users">No tags</div></tag>'	;
							table +=	'<td>'+d[index].clicks+'<td>';
							table +=	'<td>'+d[index].impressions+'<td>';
							table +='</tr>';
								}
						table +='</tbody>'+
							'</table>' +
						'</div>';

			return table;
		}

		this.setCurrentIP = function(listingId, userId, hostElement) {
   			var currentIP = seller.visitationsListings[listingId].users.sub[userId].current; // current IP selection
   			var len = length(seller.visitationsListings[listingId].users.sub[userId].sub); // count of IPs
   			var html = seller.visitationsListings[listingId].users.sub[userId].html; // this is the ip html
			$(hostElement+' #location').html(html); // ip html
			if (len > 1) // then set the option to be selected
				$(hostElement+' #location option[value="'+currentIP+'"]').prop('selected', true); // current location selection

			$(hostElement+' select#ip-list').change(function() {
	   			var listingId = $(this).attr('listing-id'); // listing id
	   			var userId = $(this).attr('user-id'); // listing id
	   			var currentIP= $(this).val();

	   			seller.visitationsListings[listingId].users.sub[userId].current = currentIP;
	   			seller.setCurrentIP(listingId, userId, hostElement);
			})

			seller.setCurrentDate(listingId, userId, currentIP, hostElement);
		}

		this.setCurrentDate = function(listingId, userId, currentIP, hostElement) {
			var currentDate = seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].current;
			var len = length(seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub); // count of dates
			var html = seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].html; // date html
			$(hostElement+' #dates').html(html); // date value	
			if (len > 1) // then set the option to be selected
				$(hostElement+' #dates option[value="'+currentDate+'"]').prop('selected', true); // current date selection

			$(hostElement+' select#date-list').change(function() {
	   			var listingId = $(this).attr('listing-id'); // listing id
	   			var userId = $(this).attr('user-id'); // listing id
	   			var ip = $(this).attr('ip');
	   			var currentDate = $(this).val();

	   			seller.visitationsListings[listingId].users.sub[userId].sub[ip].current = currentDate;
	   			seller.setCurrentDate(listingId, userId, ip, hostElement);
			})		

			seller.setCurrentTag(listingId, userId, currentIP, currentDate, hostElement);
		}

		this.setCurrentTag = function(listingId, userId, currentIP, currentDate, hostElement) {
			// get current date's tags
			$(hostElement+' #tags ul').remove();
			var currentTag = seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub[currentDate].current;
			var html = seller.showStatTagImgs ? seller.convertToTagImgs(seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub[currentDate].init) :
												seller.convertToTagStrings(seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub[currentDate].init); // this is the tag html
			var len = seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub[currentDate].tagCount; // count of tags
			if (len > 1) {// then set the option to be selected
				$(hostElement+' #tags').append(seller.visitationsListings[listingId].users.sub[userId].sub[currentIP].sub[currentDate].html);
			}
			$(hostElement+' .selectArrow').css('visibility', (len > 1 ? 'visible' : 'hidden'));
			$(hostElement+' #tags .selected').html(html);

			$(hostElement+' #tags ul').find('li').click(function(){
				$(hostElement+' #tags ul li[value]').removeClass('active');
				var val = $(this).attr('value');
				var listingId = $(this).parent().attr('listing-id'); // listing id
	   			var userId = $(this).parent().attr('user-id'); // listing id
	   			var ip = $(this).parent().attr('ip');
	   			var date = $(this).parent().attr('date');
	   			seller.visitationsListings[listingId].users.sub[userId].sub[ip].sub[date].init = val; 
				$(this).parent().css('display','none');
				$(this).addClass('active');
				$(this).closest('div#tags').attr('value',val);
				$(this).parent().siblings('.selected').html(seller.showStatTagImgs ? seller.convertToTagImgs(val) : seller.convertToTagStrings(val));
				console.log("Tag value:"+val);
			});
		}

		this.buildVisitationStatsPortal = function() {
			var haveMyPortal = havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING;
			if (!haveMyPortal)
				return '<div id="stat-div-portal-visits">' +
							'<span>You don&#39;t have a portal yet. Click on <a href="javascript:seller.getPage('+"'portal'"+');">My Portal</a> on the left and get yours now!</span>' +
						'</div>';

			// seller.updateVisitationsPortal();

			if (seller.visitations == null) 
				return 	'<h1><a href="javascript:seller.encouragePortalUse();">Portal Stats</a></h1>' +
						'<div id="stat-div-portal-visits">' +
							'<span>There are no portal visitations yet. Click <a href="javascript:seller.encouragePortalUse();">here</a> to see how you can improve on that!</span>' +
						'</div>';

			var table = '<h1><a href="javascript:seller.encouragePortalUse();">Portal Stats</a></h1>' +
						'<div id="stat-div-portal-visits" class="container">'+
							'<table id="stat-table-portal">' +
								'<thead>';
								if (!agentPageOption.displayStatsPagePhoneNumbers)
							table +='<th>User</th><th>Email</th><th>Location</th><th>Dates</th><th>Activity</th>';
								else
							table +='<th>User</th><th>Email</th><th>Phone</th><th>Location</th><th>Dates</th><th>Activity</th>';
						table +='</thead>'+
								'<tbody>';
								for(var id in seller.visitations.users) {
							table +='<tr id="'+id+'"><td>'+visitorNames[id]+'</td>'+
									'<td>'+
										'<a href="javascript:seller.emailUser('+id+');">';
											if (agentPageOption.displayStatsPagePhoneNumbers) {
												table +='<span id="agent-email">'+visitorEmails[id]+'</span>';
												table +='<span id="phone">'+visitorPhones[id]+'</span>';
											}
											else
												table +='<span id="agent-email" class="solo">'+visitorEmails[id]+'</span>';
										table +='</a>'+
									'</td>';
									var ip = '';
									if ( length(seller.visitations.users[id]) > 1 ) {
									table +='<td><select id="ip-list" for="'+id+'">';
										for(ip in seller.visitations.users[id]) 
										table +='<option value="'+ip+'">'+seller.visitationCities[ ip ]+'</option>';
									table +='</select></td>';
									}
									else {
										ip = Object.keys(seller.visitations.users[id])[0];
										table += '<td><span>'+seller.visitationCities[ ip ]+'</span></td>';
									}
									var dateList = '';
									ip = Object.keys(seller.visitations.users[id])[0]; // reset to the first one to show
									if ( length(seller.visitations.users[id][ip]) > 1 ) {
									dateList +='<select id="date-list" for="'+id+'" ip="'+ip+'">';
										for(var date in seller.visitations.users[id][ip])
										dateList +='<option value="'+ip+'">'+seller.visitations.users[id][ip][date]+'</option>';
									dateList +='</select>';
									}
									else {
										date = Object.keys(seller.visitations.users[id][ip])[0];
										dateList += '<span>'+seller.visitations.users[id][ip][date]+'</span>';
									}
								table +='<td><div id="date-list">'+dateList+'</div></td>';
								table +='<td></td>';
							table +='</tr>';
								}

								for(var pid in seller.visitations.portalUsers) {
									var id = parseInt(pid) + portalUsersOffset;
							table +='<tr id="'+id+'"><td>'+visitorNames[id]+'</td>'+
									'<td><a href="javascript:seller.emailUser('+id+');">'+visitorEmails[id]+'</a></td>';
									if (agentPageOption.displayStatsPagePhoneNumbers)
								table +='<td>'+visitorPhones[id]+'</td>';
									var ip = '';
									if ( length(seller.visitations.portalUsers[pid]) > 1 ) {
									table +='<td><select id="ip-list" for="'+id+'">';
										for(ip in seller.visitations.portalUsers[pid]) 
										table +='<option value="'+ip+'">'+seller.visitationCities[ ip ]+'</option>';
									table +='</select></td>';
									}
									else {
										ip = Object.keys(seller.visitations.portalUsers[pid])[0];
										table += '<td><span>'+seller.visitationCities[ ip ]+'</span></td>';
									}
									var dateList = '';
									if ( length(seller.visitations.portalUsers[pid][ip]) > 1 ) {
									dateList +='<select id="date-list" for="'+pid+'" ip="'+ip+'">';
										for(var date in seller.visitations.portalUsers[pid][ip])
										dateList +='<option value="'+ip+'">'+seller.visitations.portalUsers[pid][ip][date]+'</option>';
									dateList +='</select>';
									}
									else {
										date = Object.keys(seller.visitations.portalUsers[pid][ip])[0];
										dateList += '<span>'+seller.visitations.portalUsers[pid][ip][date]+'</span>';
									}
								table +='<td><div id="date-list">'+dateList+'</div></td>';
								table +='<td><a href="javascript:seller.showActivity('+id+');" id="activity">Show</a>';
							table +='</tr>';
								}
						table +='</tbody>'+
							'</table>' +
						'</div>';

			return table;
		}

		this.emailPortalUser = function(portalUser) {
			var email = portalUser.email;
			var emailSubject = "Welcome to my Portal, "+portalUser.first_name;
			var emailBody = "Hello "+portalUser.first_name+" "+(portalUser.last_name && portalUser.last_name.length ? portalUser.last_name : '')+",%0D%0A%0D%0A"+
							"How may I help you?%0D%0A%0D%0A- "+validSeller.first_name+" "+validSeller.last_name;

			var emailHref = "mailto:"+email+"?subject="+emailSubject+"&body="+emailBody;
			la(AnalyticsType.ANALYTICS_TYPE_CLICK,
			   'portalUserReachOut',
			   portalUser.id,
			   "Seller id:"+validSeller.id+" "+validSeller.first_name+" "+validSeller.last_name+" reaching out to portalUser:"+portalUser.id+
			   " "+portalUser.first_name+" "+(portalUser.last_name.length ? portalUser.last_name : ''),
			   function(query, status, msg) {
			   	if (query.status != 'OK')
			   		console.log("emailPortalUser - failed:"+query+", status:"+status+", msg:"+msg);
			   }
			);

			window.location.href = emailHref;
		}

		this.createDescDateOrderedKeys = function() {
			for(var i in portalUsersList) {
				// make foolproof descending date keys, independent of browser
				// var keys = Array.isArray(portalUsersList[i].activity.dates) ? portalUsersList[i].activity.dates.keys() : Object.keys(portalUsersList[i].activity.dates);
				var keys = Object.keys(portalUsersList[i].activity.dates);
				keys.sort(function(a, b) {
					return Date.parse(b) - Date.parse(a);
				})
				portalUsersList[i].activityKeyOrder = keys; //.reverse();
			}
		}

		this.emailUser = function(id) {
			if (id == 0)
				return;

			var email = visitorEmails[id];
			if (email == 'Unknown')
				return;

			var emailSubject = "Welcome to my Portal, "+visitorNames[id];
			var emailBody = "Hello "+visitorNames[id]+",%0D%0A%0D%0A"+
							"How may I help you?%0D%0A%0D%0A- "+validSeller.first_name+" "+validSeller.last_name;
			var emailHref = "mailto:"+email+"?subject="+emailSubject+"&body="+emailBody;
			la(AnalyticsType.ANALYTICS_TYPE_CLICK,
			   'userReachOut',
			   id,
			   "Seller id:"+validSeller.id+" "+validSeller.first_name+" "+validSeller.last_name+" reaching out to usr:"+visitorNames[id]+', email:'+visitorEmails[id],
			   function(query, status, msg) {
			   	if (query.status != 'OK')
			   		console.log("emailUser - failed:"+query+", status:"+status+", msg:"+msg);
			   }
			);

			window.location.href = emailHref;
			return false;
		}

		this.showListing = function(listingId) {
			window.open(ah_local.wp+"/listing/A-"+listingId);
		}

		this.showListingDetail = function(portalUserID, listingId) {
			if (!seller.showAllListingDetails) {
				seller.showListing(listingId);
				return;
			}
			var portalUser = this.getPortalUser(portalUserID);
			for(var i in portalUser.meta) {
				if (portalUser.meta[i].action == PortalUserMetaFlags.LISTINGS_VIEWED) {
					for(var j in portalUser.meta[i].list) {
						if (j == listingId) {
							var details = portalUser.meta[i].list[j].details;
							$('div#details a').html("View Listing");
							$('div#details a').off().on('click', function() {
								seller.showListing(listingId);
							})
							var h = '';
							if (details.quiz.length) {
								h = '<span id="quizzes">'+'Quizzes used: ';
								for(var k in details.quiz)
								h += (k != 0 ? ', ' : '')+'<a href="javascript:seller.runQuiz('+details.quiz[k]+');">'+details.quiz[k]+'</a>';
								h += '</span>';
							}
							var homeOptions = details.beds+" beds, ";
							 	homeOptions+= details.baths+" baths";
							var price = '$'+details.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							h += '<span id="title">'+details.title+'</span>'+
								 '<span id="homeOptions">'+homeOptions+'</span>'+
								 '<span id="price">'+price+'</span>';
								 '<span id="address">'+details.street_address+", "+details.city+" "+details.state+'</span>';
							var listingTags = '';
							var cityTags = '';
							for(var k in details.tags)
								if (details.tags[k].type == 1)
									cityTags += (cityTags.length? ", " : 'City tags: ')+details.tags[k].tag;
								else
									listingTags += (listingTags.length? ", " : 'Listing tags: ')+details.tags[k].tag;
							h += '<span id="listing-tags">'+listingTags+'</span>';
							h += '<span id="city-tags">'+cityTags+'</span>';
							$('div#details div#description').html(h);

							$('div#portal-user-activity').animate({opacity: 0.5},{duration: 500});
							$('div#details').fadeIn({duration: 1000});

						}
					}
				}
			}
		}

		this.runQuiz = function(quizId) {
			window.open(ah_local.wp+"/quiz-results/R-"+quizId);
		}

		this.showQuizDetail = function(portalUserID, quizId) {
			var portalUser = this.getPortalUser(portalUserID);
			for(var i in portalUser.meta) {
				if (portalUser.meta[i].action == PortalUserMetaFlags.QUIZS_TAKEN) {
					for(var j in portalUser.meta[i].list) {
						if (j == quizId) {
							var details = portalUser.meta[i].list[j].details;
							$('div#details a').html("Run Quiz");
							$('div#details a').off().on('click', function() {
								seller.runQuiz(quizId);
							})
							var homeOptions = typeof details.homeOptions.beds == 'undefined' || details.homeOptions.beds == '0' ? 'Any beds, ' : details.homeOptions.beds+" beds, ";
							 	homeOptions+= typeof details.homeOptions.baths == 'undefined' || details.homeOptions.baths == '0' ? 'Any baths' : details.homeOptions.baths+" baths";
							var price = '$'+details.price[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' to $'+(details.price[1] == -1 ? 'MAX' : details.price[1].replace(/\B(?=(\d{3})+(?!\d))/g, ","));
							var h = '<span id="quizType">'+'QuizType:'+(details.quizType == 1 ? "Nationwide" :
																		(details.quizType == 5 ? "State: "+details.state :
																		 "City:"+details.location+"<br/>Range: "+(details.distance == 0 ? 'city limit' : details.distance+" miles")))+'</span>'+
									'<span id="homeOptions">'+homeOptions+'</span>'+
									'<span id="price">'+price+'</span>';
							var tags = '';
							for(var k in details.tags)
								tags += (tags.length? ", " : 'Tags: ')+details.tags[k].tag;
							h += '<span id="tags">'+tags+'</span>';
							$('div#details div#description').html(h);

							$('div#portal-user-activity').animate({opacity: 0.5},{duration: 500});
							$('div#details').fadeIn({duration: 1000});

						}
					}
				}
			}
		}

		this.showActivity = function(id) {
			console.log("entered showActivity with "+id+", portalUsersOffset:"+portalUsersOffset);
			id = id - portalUsersOffset;
			var portalUser = this.getPortalUser(id);
			// now do something with it...
			$('div#portal-user-activity span#first-name').html(portalUser.first_name);
			$('div#portal-user-activity span#last-name').html(portalUser.last_name && portalUser.last_name.length ? portalUser.last_name : '');
			var ele = $('div#portal-user-activity a#mailto');
			ele.html(portalUser.email ? portalUser.email : 'Unknown');
			ele.off().on('click', function() {
				seller.emailPortalUser(portalUser);
			})
			var activity = null;
			$('div#portal-user-activity div#quizzes tbody').empty();
			$('div#portal-user-activity div#listings tbody').empty();
			$('div#portal-user-activity div#entries tbody').empty();
			$('div#portal-user-activity div#activities div').each(function() { $(this).hide(); })
			$('div#portal-user-activity div#activities th#main').each(function() { $(this).hide(); })
			$('div#portal-user-activity div#activities td#main').each(function() { $(this).hide(); })
			var pos = 0;
			var extraItem = 0;
			for(var i in portalUser.meta) {
				switch (portalUser.meta[i].action) {
					case PortalUserMetaFlags.QUIZS_TAKEN:
						ele = $('div#portal-user-activity div#quizzes');
						pos = 1;
						extraItem = 1; // 'detail' appended to list[item]
						break;
					case PortalUserMetaFlags.LISTINGS_VIEWED:
						ele = $('div#portal-user-activity div#listings');
						pos = 2;
						extraItem = 1; // 'detail' appended to list[item]
						break;
					case PortalUserMetaFlags.SITE_ENTRIES:
						ele = $('div#portal-user-activity div#entries');
						pos = 3;
						extraItem = 0;
						break;
				}
				var rows = '';
				for(var item in portalUser.meta[i].list){
					var row = '';
					row += '<tr action="'+portalUser.meta[i].action+'" user="'+portalUser.id+'" id="'+item+'"><td><span>';
					switch(portalUser.meta[i].action) {
					case PortalUserMetaFlags.QUIZS_TAKEN:
						row += '<a href="javascript:seller.showQuizDetail('+portalUser.id+','+item+');">'+item+'</a>';
						break;
					case PortalUserMetaFlags.LISTINGS_VIEWED:
						row += '<a href="javascript:seller.showListingDetail('+portalUser.id+','+item+');">'+item+'</a>';
						break;
					case PortalUserMetaFlags.SITE_ENTRIES:
						row += item;
						break;
					}
					row += '</span></td>';

					var ip = '';
					if ( length(portalUser.meta[i].list[item]) > (1+extraItem) ) {
					row += '<td><select id="ip-list" action="'+portalUser.meta[i].action+'" user="'+portalUser.id+'" item="'+item+'">';
						for(ip in portalUser.meta[i].list[item])
						row += '<option value="'+ip+'">'+seller.visitationCities[ip]+'</option>';
					row +='</select></td>';
					}
					else {
						ip = Object.keys(portalUser.meta[i].list[item])[0];
						row += '<td><span>'+seller.visitationCities[ ip ]+'</span></td>';
					}

					var dateList = '';
					ip = Object.keys(portalUser.meta[i].list[item])[0]; // reset to the first one to show
					if ( length(portalUser.meta[i].list[item][ip]) > 1 ) {
					dateList +='<select id="date-list" action="'+portalUser.meta[i].action+'" user="'+portalUser.id+'" id="'+item+'" ip="'+ip+'">';
						for(var date in portalUser.meta[i].list[item][ip])
						dateList +='<option value="'+ip+'">'+portalUser.meta[i].list[item][ip][date].date+'/'+portalUser.meta[i].list[item][ip][date].count+'</option>';
					dateList +='</select>';
					}
					else {
						date = Object.keys(portalUser.meta[i].list[item][ip])[0];
						dateList += '<span>'+portalUser.meta[i].list[item][ip][date].date+'/'+portalUser.meta[i].list[item][ip][date].count+'</span>';
					}
					row +='<td><div id="date-list">'+dateList+'</div></td>';
					row +='</tr>';
					rows += row;
				}
				$('div#portal-user-activity div#activities th#main:nth-child('+pos+')').show();
				$('div#portal-user-activity div#activities td#main:nth-child('+pos+')').show();
				ele.find('tbody').html(rows);
				ele.show();

				$('#portal-user-activity select#ip-list').change(function() {
		   			var item = $(this).attr('item');
		   			var action = parseInt($(this).attr('action'));
		   			var userId = parseInt($(this).attr('user'));
		   			var ip = $(this).val();
		   			console.log("selected ip-list for action:"+action+", userId"+userId+", ip:"+ip);
		   			var ele = null;
		   			switch (action) {
						case PortalUserMetaFlags.QUIZS_TAKEN:
							ele = $('div#portal-user-activity div#quizzes tbody tr[id='+item+'] div#date-list');
							break;
						case PortalUserMetaFlags.LISTINGS_VIEWED:
							ele = $('div#portal-user-activity div#listings tbody tr[id='+item+'] div#date-list');
							break;
						case PortalUserMetaFlags.SITE_ENTRIES:
							ele = $('div#portal-user-activity div#entries tbody tr[id='+item+'] div#date-list');
							break;
					}

					ele.empty();
					var portalUser = seller.getPortalUser(userId);
					var dataSet = null;
					for(var i in portalUser.meta) {
						if (portalUser.meta[i].action == action) {
							dataSet = portalUser.meta[i];
							break;
						}
					}

					if (!dataSet) {
						console.log("Failed to find data for "+item+", action:"+action+", userId"+userId+", ip:"+ip);
						return;
					}

					var dateSet = dataSet.list[item][ip];
					var dateList = '';
					if ( length(dateSet) > 1 ) {
					dateList +='<select id="date-list" action="'+action+'" user="'+portalUser.id+'" id="'+item+'" ip="'+ip+'">';
						for(var date in dateSet)
						dateList +='<option value="'+ip+'">'+dateSet[date].date+'/'+dateSet[date].count+'</option>';
					dateList +='</select>';
					}
					else {
						date = Object.keys(dateSet)[0];
						dateList += '<span>'+dateSet[date].date+'/'+dateSet[date].count+'</span>';
					}
					ele.html(dateList);
				})
			}
			$('#seller-content').animate({opacity: 0.5},{duration: 500});
			$('div#portal-user-activity').fadeIn({duration: 1000});
		}

		this.expandStats = function() {
			if (seller.visitations == null) {
				seller.encouragePortalUse();
				return;
			}
			
			var table = seller.buildVisitationStatsPortal();

			var len = length(seller.visitations.users);
			ahtb.open({html: table,
					   width: 900,
					   height: (len * 38)+140, // account for header and OK button
					   maxHeight: 350,
					   minHeight: 150,
					   opened: function() {
					   		$('select').change(function() {
					   			var id = $(this).attr('for');
					   			var ip = $(this).val();
					   			
					   			var dateList = '';
					   			if (id < portalUsersOffset)
									for(var date in seller.visitations.users[id][ip])
										dateList += (dateList.length ? ", " : '')+seller.visitations.users[id][ip][date];
								else
									for(var date in seller.visitations.portalUsers[id-portalUsersOffset][ip])
										dateList += (dateList.length ? ", " : '')+seller.visitations.portalUsers[id-portalUsersOffset][ip][date];

								$('#stat-table-portal tr[id="'+id+'"] #date-list').html(dateList);
								console.log("For id:"+id+":"+visitorNames[id]+", ip:"+ip+", dateList:"+dateList);
					   		})
					   }	
					});
		}

		this.encouragePortalUse = function() {
			ahtb.open({html: '<div id="portal-usage" ><p>For additional help getting the most out of your portal, please call us at 831.508.8821.<br><br>Our phone hours are Monday - Friday from 9am - 5pm PST, or email us any time at Info@LifestyledListings.com.</p></div>',
							 //'<strong>Portal is a tool</strong> for you to use, much like a saw or hammer to build a house, ' +
							 //'and as such, you need to use it and apply it, or else it just sits there.  So to get '+
							 //'the <strong>best results</strong> from your portal, you need to <strong>insert the portal</strong> into your <strong>email signature, ' +
							 //'social media outlets, website</strong>, and into your <strong>business literature</strong>.  Tell your friends and clients to visit ' +
							 //'your portal, and encourage them to tell their friends and family.</p>'+
							 //'<p>The more effort you put in promoting your portal, the more <strong>you will appear in the listings '+
							 //'viewed</strong> via your portal, <strong>nationwide</strong>, and create more <strong>opportunities to earn a referral fee</strong>.  It is a powerful tool.  ' +
							 //'You need to work it, pass it around to make it effective and increase your visitor counts</p>'+
							 //'<p>So <strong>share it on '+
							 //'<a href="avascript:seller.shareSocialNetwork('+"'FB'"+');" ><img style="width:1.5em; height:1.5em; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/facebook.png'+'" /></a>' +
							 //'</strong>, <strong>'+
							 //'<a href="javascript:seller.shareLinkedIn();"><img style="width:1.5em; height:1.5em; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/linkedin.png'+'" /></a>' +
							 //'</strong>, <strong>'+
							 //'<a href="avascript:seller.shareSocialNetwork('+"'GP'"+');" ><img style="width:1.5em; height:1.5em; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/googleplus.png'+'" /></a>' +
							 //'</strong>, and <strong>'+
							 //'<a href="avascript:seller.shareSocialNetwork('+"'TW'"+');" t><img style="width:1.5em; height:1.5em; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/twitter.png'+'" /></a>' +
							 //'</strong>'+
							 //', we have made it very easy for you, just a click away.  Copy the '+
							 //'email signature banner, and put it into your <strong>email signature</strong> via your email client.  <strong>Add a link</strong> to your portal '+
							 //'in your <strong>own website</strong>.  Feel free to call us '+
							 //'if you need assistance, but it is <strong>up to you to wield this tool and build your house filled with clients and '+
							 //'referrals</strong>.</p>'+
							 //'<p>Best of luck!  Lifestyled Listings Team</p>' +
						width: 320,
						height: 250
					});
		}

		this.sendBannerToAgent = function() {
			var data = { banner: seller.clipboardData,
						 id: validSeller.id };
			$('div.clipboard-manual').removeClass('active');
			seller.DBget({
				query: 'email-agent-email-banner',
				data: data,
				done: function(d) {
					ahtb.alert("Please check both your inbox and spam box for your email banner", {height: 170, width: 450});
				},
				error: function(d) {
					ahtb.alert("Failed to email banner", {height: 150, width: 450});
				}
			});
		}

		this.copyToClipboard = function(which) {
			seller.clipboardImage = '<img width="409" style="max-width: 100%" src="'+ah_local.tp+'/_img/_banners/_email/banners-v2/new-banners-'+which+'-copy.png" />';
			seller.clipboardImageLinkedIn = ah_local.tp+"/_img/_banners/linkedin/Linked-in-"+which+".jpg";
			seller.clipboardData = 	'<a href="'+ah_local.wp+'/'+nickname+'";>'+seller.clipboardImage+'</a>';
			var portalLink = ah_local.wp+'/'+nickname;
			var doPopup = 	browser.shortname == 'iPhone' ||
							browser.shortname == 'iPad' ||
							browser.shortname == 'iPad';
							//browser.shortname == 'Safari';
			var isSafari = browser.shortname == 'Safari';
			var isIE = browser.shortname.indexOf('MSIE') != -1 && document.body.createControlRange;
			var isEdge =  browser.shortname == 'Edge';
			var isFirefox = browser.shortname == 'Firefox';
			var ele = $('div.clipboard-manual #data-clipboard');

			seller.completeStep(which, 'emailBanner');
			// seller.clipboardData = '<div>'+seller.clipboardData+'</div>';
			function bannerOverlayClose() {
				$('.email-banners .overlay').fadeOut({duration:750,
																									done: function(){
																										$('.email-banners .overlay').hide();
																									}
																								 });
			}
			function copyModalClose() {
				$('.email-banners .overlay .copymodal').removeClass( 'popout' ).fadeOut({duration:300,
																									done: function(){
																										$('.email-banners .overlay .copymodal').hide();
																									}
																								 });
			}
			
            // ---- Internet Explorer ---- //
			if (isIE) {
				$('#seller-admin .have-portal .email-banners .area-right').append( '<div class="hidecopydiv" style="display:none;"><div id="iecopycontent">'+seller.clipboardData+'</div></div>' );
				var htmlContent = document.getElementById('iecopycontent');
				
				var controlRange;
				var range = document.body.createTextRange();
				range.moveToElementText(htmlContent);
				//range.select();

				controlRange = document.body.createControlRange();
				controlRange.addElement(htmlContent);
				controlRange.execCommand('Copy');
				
				$('#seller-admin .have-portal .email-banners .area-right .hidecopydiv').remove();
				ahtb.alert('Copied to clipboard');
				return;
			}
          
            // ---- Firefox and Edge ---- //
			if (isFirefox ||
				 isEdge) {
				new Clipboard('#copybutton');
				$('#seller-admin .have-portal .email-banners .overlay').html( '<button id="copybutton" data-clipboard-target="#ffcopycontent">Copy Banner</button><div id="ffcopycontent">'+seller.clipboardData+'</div><a class="cancel" href="javascript:;">Cancel</a>' ).fadeIn({duration: 750}).css('display', 'inline-block');
				
				$('.email-banners .overlay a.cancel').on('click', function(){
					bannerOverlayClose();
				});
				$('.email-banners .overlay button#copybutton').on('click', function(){
						ahtb.alert('Copied to clipboard');
						bannerOverlayClose();
				});
				return;
			}
          
            // ---- Safari ---- //
			if (isSafari) {
				new Clipboard('#selectbutton');
				$('#seller-admin .have-portal .email-banners .overlay').html( '<div class="copymodal" style="display:none;">Press Command+C to Copy!</div><button id="selectbutton" data-clipboard-target="#safaricopycontent">Select</button><div class="safari-wrapper"><div id="safaricopycontent">'+seller.clipboardData+'</div></div><a class="cancel" href="javascript:;">Cancel</a>' ).fadeIn({duration: 750}).css('display', 'inline-block');
				
				$('.email-banners .overlay a.cancel').on('click', function(){
					bannerOverlayClose();
					copyModalClose();
					document.getSelection().removeAllRanges();
				});
				$('.email-banners .overlay button#selectbutton').on('click', function(){
						$('.email-banners .overlay .copymodal').fadeIn({duration:300,
																									start: function(){
																										$('.email-banners .overlay .copymodal').addClass( 'popout' );
																									}
																								 });
				});
				
				$(window).keydown(function (e){
					if (e.metaKey || e.ctrlKey) {
						function delaymodalclose() {
							copyModalClose();
                            $('.email-banners .overlay a.cancel').html('Done');
						}
						timeoutID = window.setTimeout(delaymodalclose, 250);
					}
				});
				return;
			}
          
			// ---- Iphones ---- //
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if ((/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) ||
                (/android/i.test(userAgent))) {
              
              jQuery.fn.selectText = function(){
                var doc = document
                , element = this[0]
                , range, selection
                ;
                if (doc.body.createTextRange) {
                  range = document.body.createTextRange();
                  range.moveToElementText(element);
                  range.select();
                } else if (window.getSelection) {
                  selection = window.getSelection();        
                  range = document.createRange();
                  range.selectNodeContents(element);
                  selection.removeAllRanges();
                  selection.addRange(range);
                }
              };
              var tempScrollTop = $(window).scrollTop();
                
				$('#seller-admin .have-portal .email-banners .overlay').html( '<div class="first-mobile-wrap"><div class="mobile-wrap"><button id="selectbutton">Select</button><div class="safari-wrapper" contenteditable="true"><div id="safaricopycontent"><br/>'+seller.clipboardData+'&#8291;<br/></div></div><input class="fake-input" style="size:0;opacity:0;" readonly><div class="copymodal" style="display:none;">Press \'copy\', then paste the image into your signature</div><a class="cancel" href="javascript:;">Cancel</a></div></div>' ).fadeIn({duration: 750}).css('display', 'inline-block');
                function delayfixedbody() {
                  $('body').css('overflow','hidden');
                  $('body').css('position','fixed');
                }
                timeoutID = window.setTimeout(delayfixedbody, 1000);
              
                var selectButtonTouch = $('.email-banners .overlay button#selectbutton')[0];
                selectButtonTouch.addEventListener("click", function() {
                  $('.email-banners .overlay #safaricopycontent').selectText();
                  $('.email-banners .overlay .copymodal').fadeIn({duration:300,
                                                                  start: function(){
                                                                    $('.email-banners .overlay .copymodal').addClass( 'popout' );
                                                                  }
                                                                 });
                }, false);
              
                document.addEventListener('copy', function(e){
                  //$('.safari-wrapper').blur();
                  copyModalClose();
                  $('.email-banners .overlay a.cancel').html('Done');
                });
                document.addEventListener('focusout', function(e) {
                  document.getSelection().removeAllRanges();
                  copyModalClose();
                });
                
                $('.email-banners .overlay a.cancel').on('click', function(){
					bannerOverlayClose();
					copyModalClose();
					document.getSelection().removeAllRanges();
                    $('body').css('overflow','');
                    $('body').css('position','relative');
                    $(window).scrollTop(tempScrollTop);
				});
				return;
            }
          
            // ---- Everything Else ---- //
			else if (doPopup) {
			//else if (doPopup ||
					 //isEdge ||
					 //isEdge) {
					//browser.shortname == 'Firefox') {
				ahtb.alert('Failed to copy to clipboard . Feature not supported, please check to make sure your browser is up to date.', {width: 500, height: 180});
				return;

				var input = '<input id="data-clipboard-input" value="'+portalLink+'"></input>';
				ele.html(input);
				// ele.html(seller.clipboardData);
				// ele.attr('data-clipboard-text', seller.clipboardData);
				// var instruction = 'Receive copy of your email banner in your email, and open it using non-Apple email client to copy and paste.  Otherwise right-click and copy the image URL and link URL separately and compose it manually in your email client.';
				var instruction = "Copy your portal URL into your clipboard and paste it into your email signature.  "+
								  "Otherwise right-click and copy the image URL from the available banners and compose it manually in your email client with your portal URL.  "+
								  "You can also email the banner to yourself and see if you can copy and paste from there."
				// switch(browser.platform) {
				// 	case 'mac': instruction = "Receive copy of your email banner in your email"; break;
				// 	case 'windows': instruction = "Press Ctrl-C to copy"; break;
				// }
				$('div.clipboard-manual span#instruction').html(instruction);
				$('div.clipboard-manual').addClass('active');

				var clipboardDemos = new Clipboard('.btn');
				clipboardDemos.on('success',function(e){e.clearSelection();console.info('Action:',e.action);console.info('Text:',e.text);console.info('Trigger:',e.trigger);showTooltip(e.trigger,'Copied!');});
				clipboardDemos.on('error',function(e){console.error('Action:',e.action);console.error('Trigger:',e.trigger);showTooltip(e.trigger,fallbackMessage(e.action));});
				// var client = new Clipboard('.btn');
				// ele = $('button.clip_button');
				// var client = null;
				// if (!seller.zeroClipboard) {
				// 	seller.zeroClipboard = new ZeroClipboard( ele );
				// 	client = seller.zeroClipboard;
				// 	client.on( {'ready': function(event) { console.log( 'movie is loaded' ); },
		  //       			'copy' : function(event) {
		  //       						console.log("copy event triggered");
				// 			          	event.clipboardData.setData({
				// 							'text/plain': 'Link to your portal, paste into your email signature.',
				// 							'text/html': seller.clipboardData
				// 						});
				// 			        },
				// 			'aftercopy': function(event) {
				// 						console.log('Copied text to clipboard: ' + event.data['text/html']);
				// 				        $('div.clipboard-manual').removeClass('active');
				// 				        },
				// 			'error': function(event) {
				// 				        console.log( 'ZeroClipboard error of type "' + event.name + '": ' + event.message );
				// 				        ZeroClipboard.destroy();
				// 				        $('div.clipboard-manual').removeClass('active');
				// 				      } 
				// 			});
				// }

				// client = seller.zeroClipboard;
		        
			    
		    
				// ele = $(ele, "a");
				// window.setTimeout(function() {
				// 	var ele = $('div.clipboard-manual #data-clipboard');
				// 	// ele.focus();
				// 	// ele.select();
				// 	ele.dblclick();
				// }, 2000);

				// ele.dblclick(function() {
				// 	console.log("dblclick entered");
				// 	$(this).select();
				// 	$(this).focus();

				// 	if (window.clipboardData) {
				// 		window.clipboardData.setData('text/html', seller.clipboardData);
				// 		console.log("setData on clipboardData");
				// 	}
				// 	else {
	   //                  // create a temporary element for the execCommand method
		  //               var forExecElement = CreateElementForExecCommand (seller.clipboardData);

		  //                       /* Select the contents of the element 
		  //                           (the execCommand for 'copy' method works on the selection) */
		  //               SelectContent (forExecElement);

		  //               var supported = true;
		  //               var success = false;

		  //                   // UniversalXPConnect privilege is required for clipboard access in Firefox
		  //               try {
		  //                   if (window.netscape && netscape.security) {
		  //                       netscape.security.PrivilegeManager.enablePrivilege ("UniversalXPConnect");
		  //                   }

		  //                       // Copy the selected content to the clipboard
		  //                       // Works in Firefox and in Safari before version 5
		  //                   success = document.execCommand ("copy", false, null);
		  //               }
		  //               catch (e) {
		  //                   success = false;
		  //               }
		                
		  //                   // remove the temporary element
		  //               document.body.removeChild (forExecElement);

		  //               if (!success) {
				// 			// var flashcopier = 'flashcopier';
				// 		 //    if(!document.getElementById(flashcopier)) {
				// 		 //      var divholder = document.createElement('div');
				// 		 //      divholder.id = flashcopier;
				// 		 //      document.body.appendChild(divholder);
				// 		 //    }
				// 		 //    document.getElementById(flashcopier).innerHTML = '';
				// 		 //    var divinfo = '<embed src="'+ah_local.tp+'/js/_clipboard.swf" FlashVars="clipboard='+encodeURIComponent(seller.clipboardData)+'" width="0" height="0" type="application/x-shockwave-flash"></embed>';
				// 		 //    document.getElementById(flashcopier).innerHTML = divinfo;
				// 		 // var clipboard = new ZeroClipboard();
				// 		 // ZeroClipboard.setData('text/html', seller.clipboardData);
		  //               }
		  //           }


				// 	// var selection = window.getSelection();
			 //  //       var range = document.createRange();

			 //  //       var ele = document.getElementById('data-clipboard');

			 //  //       range.selectNode(ele);
			 //  //       // range.selectNodeContents(ele);
			 //  //       // selection.removeAllRanges();
			 //  //       selection.addRange(range);

			 //  //       selectedText = selection.toString();
			 //  //       console.log("selectedText:"+selectedText);
				// })
				
				return;
			}
			// else if (browser.shortname == 'Firefox') {
			// 	var input = '<input id="data-clipboard-input" value="'+seller.clipboardData+'"></input>';
			// 	ele.html(input);
			// 	var selection = window.getSelection();
		 //        var range = document.createRange();

		 //        var ele = document.getElementById('data-clipboard-input');

		 //        // range.selectNode(ele);
		 //        range.selectNodeContents(ele);
		 //        selection.removeAllRanges();
		 //        selection.addRange(range);

		 //        selectedText = selection.toString();
		 //        console.log("selectedText:"+selectedText);
		 //        if (selectedText.length)
		 //        	ahtb.alert('Copied to clipboard');
		 //        else
		 //        	ahtb.alert('Failed to copy to clipboard.');
			// }
			else {
				var clip = {'text/html': seller.clipboardData};
				// if (!isIE)
				clip['text/plain'] = 'Link to your portal, paste into your email signature.';
				// clip['text/plain'] = portalLink;
				clipboard.copy(clip).then(
					function() { console.log('copied to clipboard - '+seller.clipboardData); ahtb.alert('Copied to clipboard'); },
					function(err) { console.log('failed to copy:'+seller.clipboardData+' to clipboard - '+err); ahtb.alert('Failed to copy to clipboard.', {width: 500, height: 180}); }
				)
			}
		}

		this.print = function(which, asCsv) {
			var asCsv = typeof asCsv == 'undefined' ? false : asCsv;
			if (!asCsv) {
		        var originalContents = document.body.innerHTML;
		        var data = 	'<section id="seller-admin">'+
		        				'<section class="content">' +
		        					'<div class="page-stats">';
		        data += which == ActiveStatPage.PortalStats ? seller.page.stats.portalStats(true) : seller.page.stats.listingStats(true);
		        data +=				'</div>'+
		        				'</section>'+
		        			'</section>';

			    document.body.innerHTML = data;
			    window.print();
                setTimeout(statsEvents, 100);
                function statsEvents() {
                  //document.body.innerHTML = originalContents;
                  window.location.reload();
                }
			}
			else {
				var csv = (!agentPageOption.displayStatsPagePhoneNumbers) ? 'First,Last,Email\n\r' : 'First,Last,Email,Phone\n\r';
				if (length(portalUsersList)) {
					for(var i in portalUsersList) {
						csv += portalUsersList[i].first_name+','+portalUsersList[i].last_name+","+portalUsersList[i].email;
						if (agentPageOption.displayStatsPagePhoneNumbers)
							csv += ","+(portalUsersList[i].phone != 0 ? portalUsersList[i].phone : 'N/A');
						csv += '\n\r';
					}
				}
				if (seller.visitations &&
					length(seller.visitations.users)) {
					for(var id in seller.visitations.users) {
						var name = visitorNames[id].split(' ');
						var first = name[0];
						var last = name.splice(0, 1).join(' ');
						csv += first+','+last+","+visitorEmails[id];
						if (agentPageOption.displayStatsPagePhoneNumbers)
							csv += ","+(visitorPhones[id] != 0 ? visitorPhones[id] : 'Phone Unknown');
						csv += '\n\r';
					}
				}
				saveAs( new Blob([csv], 
				  				 {type: "plain/text;charset=" + document.characterSet} ),
				  				 "activity.csv");	
			}

	        return true;
		}

		this.prepareUpload = function(e) {
			files = e.target.files;
		}

		this.upload = function(e) {
			setCookie("IMAGE_DIR", "_authors/uploaded/", 1);
			if (e) {
				e.stopPropagation(); // Stop stuff happening
			    e.preventDefault(); // Totally stop stuff happening
			}
		 		 
		    // Create a formdata object and add the files
			formData = new FormData();
			formData.append ('image_dir','_authors/uploaded/');
			formData.append ('gen_sizes', 1 );
			$.each(files, function(key, value)
			{
				console.log(key, value);
				formData.append(key, value);
			});

			$('.spin-wrap').show();

			$.ajax({
		        url: ah_local.tp+'/_classes/Image.class.php',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				error: function($xhr, $status, $error){
					$('.spin-wrap').hide();
					if ( !$xhr.responseText &&
						 !$error)
						return; // ignore it

					var msg = $xhr.responseText ? $xhr.responseText : ($error ? $error : "No data available");
					ahtb.open({ title: 'You Have Encountered an Error', 
								height: 150, 
								html: '<p>Status: '+($status ? $status : "No status.")+", Detail: "+msg+'</p>' 
							});
				},					
			  	success: function(data){
			  		$('.spin-wrap').hide();
				  	if ( data == null || data.status == null) 
				  		ahtb.open({ title: 'You Have Encountered an Error', height: 150, html: '<p>Got a return of NULL</p>' });
				  	else {
				  		seller.updateAuthorPhoto(data);
				  		// ahtb.alert(data.data, {width: 450, height: 150});
				  	}
				}
			})
		}

		this.updateAuthorPhoto = function(x) {
			if (x.status != 'OK') ahtb.alert(typeof x.data == 'string' ? '<p>'+x.data+'</p>' : '<p>Unable to upload image.</p>');
			else ahtb.loading('Saving photo...',{
				opened: function(){
					seller.DBget({	query:'update-author', 
									data:{ id: validSeller.id,
											fields: {photo:x.data[0]}
										  }, 
									done:function(d){
										if (typeof dz.disable == 'function') dz.disable();
										if (typeof d == 'object') {
											seller.page.profile.info = d.seller;
											dashboardCards = d.dashboardCards;
										}
										else
											seller.page.profile.info.photo = x.data[0];
										$('.sellerimg #dropzone').hide();
										$('div.seller-photo img').attr('src', ah_local.tp+'/_img/_authors/272x272/'+x.data[0]);
										$('div.header-login img').attr('src', ah_local.tp+'/_img/_authors/55x55/'+x.data[0]);
										$('div.seller-photo').show();
										ahtb.close();
									},
									error:function(x) {
										var msg = typeof x == 'string' ? x :
												  typeof x == 'object' ? JSON.stringify(x) :
												  "Unknown reason";
										ahtb.alert("Failure saving image: "+msg);
									}
								});
				}
			});
		}

		this.postVideoViewing = function() {
			var metas = validSeller.meta;
			var cardMeta = {action: SellerMetaFlags.SELLER_COMPLETED_CARDS};
			var ownedMetas = [];
			if (metas) for(var i in metas) {
				if (metas[i].action == SellerMetaFlags.SELLER_COMPLETED_CARDS) 
					cardMeta = metas[i];
				else
					ownedMetas[ownedMetas.length] = metas[i];
			}

			setSellerDashBoardCards(validSeller, cardMeta);
		}

		this.showVideo = function(which) {
			var h = '<iframe src="//player.vimeo.com/video/'+videoList[which]+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			$('.videoDiv .video').html(h);
			videoControl.showVid( null, seller.postVideoViewing );

			seller.completeStep(which, 'video');
		}

		this.completeStep = function(which, type) {
			var metas = validSeller.meta;
			var cardMeta = {action: SellerMetaFlags.SELLER_COMPLETED_CARDS};
			var ownedMetas = [];
			var needUpdate = false;
			if (metas) for(var i in metas) {
				if (metas[i].action == SellerMetaFlags.SELLER_COMPLETED_CARDS) 
					cardMeta = metas[i];
				else
					ownedMetas[ownedMetas.length] = metas[i];
			}

			if (typeof type == 'undefined') {
				if (typeof cardMeta[which] != 'undefined') {
					if (cardMeta[which] != 1)
						needUpdate = true;
				}
				else
					needUpdate = true;
				cardMeta[which] = 1;
			}
			else {
				if (typeof cardMeta[type] != 'undefined') {
					if (typeof cardMeta[type][which] != 'undefined') {
						if (cardMeta[type][which] != 0)
							needUpdate = true;
					}
					else
						needUpdate = true;

					cardMeta[type][which] = 1;
				}
				else {
					cardMeta[type] = {};
					cardMeta[type][which] = 1;
					needUpdate = true;
				}
			}
			
			if (needUpdate) {
				ownedMetas[ownedMetas.length] = cardMeta;
				var data = {meta: ownedMetas};
				var sans = {data: data};
				seller.page.profile.saveToDb(sans, UpdateAuthorOrigin.Dashboard);
			}
		}

		this.setupSellerMetas = function() {
			if (typeof seller.page.profile.info != 'undefined' &&
				seller.page.profile.info) {
				var metas = seller.page.profile.info.meta;
				var d = seller.page.profile.info;

				if (metas) for(var i in metas)
					if (metas[i].action == SellerMetaFlags.SELLER_PROFILE_DATA) {
						metas[i].contact_email = typeof metas[i].contact_email == 'undefined' || metas[i].contact_email == null ? '' : metas[i].contact_email;
						seller.page.profile.info.contact_email = metas[i].contact_email;
						seller.meta = metas[i];
					}		
					else if (metas[i].action == SellerMetaFlags.SELLER_AGENT_MATCH_DATA) {
						seller.am_meta = JSON.parse(JSON.stringify(metas[i])); // makes exact copy
					}	
					else if (metas[i].action == SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA) {
						seller.mod_meta = JSON.parse(JSON.stringify(metas[i])); // makes exact copy
					}
					else if (metas[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) {
						seller.am_order = JSON.parse(JSON.stringify(metas[i])); // makes exact copy
					}	

				if (seller.mod_meta == null) 
					seller.mod_meta = {action: SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA,
									'first_name': 0,
									'last_name': 0,
									'company': 0,
									'website': 0,
									'phone': 0,
									'mobile': 0,
									'email': 0 };				
			}
		}

		this.goQueuedPage = function(force) {
			seller.getPage(seller.queuedPage, force);
			seller.queuedPage = '';
		}

		this.getPage = function(pageName, force){
			console.log("getPage - "+pageName+", author_id:"+ah_local.author_id);
			if (pageName == 'account') {
				if (timeoutID) window.clearTimeout(timeoutID);
				if (timeoutID2) window.clearTimeout(timeoutID2);
				window.location = ah_local.wp+'/my-account';
				return;
			}
			if (pageName == 'cart') {
				if (timeoutID) window.clearTimeout(timeoutID);
				if (timeoutID2) window.clearTimeout(timeoutID2);
				window.location = ah_local.wp+'/cart';
				return;
			}

			if (typeof seller.page.profile.info == 'undefined') {
				seller.DBget({
					//query: 'author-has-inactive-listings',
					query: 'get-listings-by-author',
					data: ah_local.author_id,
					done:function(d){
						seller.sellerData = d;
						seller.initial_listings = d.listings;
						seller.page.profile.info = d.author;
						seller.updateMyListingsButton();
						seller.setupSellerMetas();
						seller.gatherDataOffNet(pageName);
						// seller.getPage(pageName, force);
					}
				});
				return;
			}

			if ( this.currentPage != pageName ||
				 (this.currentPage == pageName &&
				  typeof force != 'undefined' &&
				  force) ) {
				if (pageName == 'agent-match-sales') {
					seller.doingSales = true;
					pageName = 'agent_match';
					window.history.pushState('pure','Title',ah_local.wp+'/sellers/#agent-match-sales');
				}
				else if (pageName == 'agent_match_reserve') {
					seller.doingSales = false;
					if (typeof seller.page.profile.info != 'undefined') {
						if (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) {
							pageName = 'agent_match';
							if (seller.page.profile.info.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO &&
								!hasBoughtLifestyle) {
								if (timeoutID) window.clearTimeout(timeoutID);
								if (timeoutID2) window.clearTimeout(timeoutID2);
								window.location = ah_local.wp+"/agent-basic-lifestyle-info";
								return;
							}
						}
						else {
							if (timeoutID) window.clearTimeout(timeoutID);
							if (timeoutID2) window.clearTimeout(timeoutID2);
							window.location = ah_local.wp+"/agent-basic-lifestyle-info";
							return;
						}
					}
				}
				window.location.hash = pageName == 'home' ? '' : pageName;
				if (this.currentPage == 'agent_match') {
					if (seller.am_order) {
						$('.define-agent-match-container #define-agent-match #description').each(function() {
							var item = $(this).attr('for');
							var desc = $(this).val();
							seller.am_order.item[item].desc = desc;
							console.log("AM desc for:"+item+", is:"+desc);
						});
						// this.currentPage = pageName;
						this.currentPage = ''; // prevent recursion
						seller.DBget({
							query: 'update-agent-match-order',
							data: {order: seller.am_order,
								   id: validSeller.id },
							done: function(d) {
								console.log("update-agent-match-order - "+d.status);
								seller.page.profile.info = d.seller;
								validSeller = d.seller;
								seller.getPage(pageName, true); // force a page load
								// ahtb.close();							
				    		},
							error: function(d) {
								console.log("We're very sorry, had trouble updating the descriptions. "+d.status);
								ahtb.alert("We're very sorry, had trouble updating the descriptions. "+d.status, {height: 150, width: 450});
								seller.getPage(pageName, true); // force a page load
							}
						});
						return;
					}
				}
				if (this.currentPage == 'profile' &&
					!seller.saving &&
					seller.queuedPage.length == 0 ) {
					seller.queuedPage = pageName;
					seller.page.profile.updateAuthor( UpdateAuthorOrigin.PageChange, seller.goQueuedPage );
					return;
				}

				this.currentPage = pageName;
				$('.profile-menu .menu-item').attr('class','menu-item');
				$('[data-page='+pageName+']').addClass('current-page');
				if (this.page[pageName]) {
					if (typeof seller.sellerData == 'undefined' ||
						typeof seller.initial_listings == 'undefined' ||
						typeof seller.page.profile.info == 'undefined') {
						//ahtb.loading('Loading...',{
						//	opened:function(){
								seller.DBget({
									//query: 'author-has-inactive-listings',
									query: 'get-listings-by-author',
									data: ah_local.author_id,
									done:function(d){
										seller.sellerData = d;
										seller.initial_listings = d.listings;
										seller.page.profile.info = d.author;
										seller.updateMyListingsButton();
										seller.setupSellerMetas();	
										seller.gatherDataOffNet(pageName);
										// seller.page[pageName].get();
										// if (portalMsg.length) {
										// 	ahtb.alert(portalMsg);
										// 	portalMsg = '';
										// }
										// else
										// 	ahtb.close();
									}
								})
						//	}
						//})
					}
					else {
						DBanalytics('record-analytic',
							{
								type: AnalyticsType.ANALYTICS_TYPE_PAGE_ENTRY,
								session_id: ah_local.activeSessionID,
								origin: pageName == 'home' ? 'dashboard' : pageName,
								referer: Referer,
								value_int: seller.page.profile.info.author_id
							},
							function(query, status, msg){},
							function(query, status, msg){}
						);
						this.page[pageName].get();
						if (seller.page.profile.info.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO &&
					  		pageName == 'profile') {
					  		window.setTimeout(function() {
					  			seller.showGetBasicInfoMsg();
					  		}, 1500);
					  	}
					}
				}
				else this.content({ title: '404: Page Not Found', content: '<p>Unable to locate page: <strong>'+pageName+'</strong></p>' });
			}
		}

		this.gatherDataOffNet = function(pageName) {
			var d = seller.sellerData.listings; 
			if (d != null &&
				length(d)) {
				for(var listing in d)  // each listing is a row of table
					seller.updateVisitationsListing(d[listing]); // update seller.visitationCities[] with any new ip's and get the meta data
			}
			author = seller.page.profile.info;
			for(var i in author.meta) {
				if (author.meta[i].action == SellerMetaFlags.SELLER_VISITATIONS) {
					if (typeof author.meta[i].updated != 'undefined') delete author.meta[i].updated;
					break;
				}
			}
			seller.updateVisitationsPortal();
			seller.waitUntilDataIsAcquired(pageName);
		}

		this.showGetBasicInfoMsg = function() {
			$('.page-profile .lifestyled-agent-alert').slideToggle('fast');
		}

		this.waitUntilDataIsAcquired = function(pageName) {
			if (seller.queryIPCount) {
				window.setTimeout(function() {
					seller.waitUntilDataIsAcquired(pageName);
				}, 100);
				return;
			}

			// seller.currentPage = pageName;
			// seller.page[pageName].get();
			seller.getPage(pageName);
			if (portalMsg.length) {
				ahtb.open({html: portalMsg,
						  width: 400,
						  height: 180,
						  buttons: [{text:'OK', action: function() {
						  	ahtb.close();
						  }}],
						  closed: function() {
						  	if (seller.page.profile.info.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO &&
						  		pageName == 'profile') {
						  		window.setTimeout(function() {
						  			seller.showGetBasicInfoMsg();
						  		}, 250);
						  	}
						  }});
				portalMsg = '';
			}
			else if (seller.page.profile.info.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO &&
					 pageName == 'profile') 
				window.setTimeout(function() {
		  			seller.showGetBasicInfoMsg();
		  		}, 250);
			//else
				//ahtb.close();
			sellerTransitionEnd();
		}

		this.setupAutocomplete = function(element) {
			element.autocomplete({
			    source: function( request, response ) {
		          var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
		          response( $.grep( cities, function( item ){
		              var match = matcher.test( item.label );
		              return match;
		          }) );
		      	},
			    select: function(e, ui){ 
			      if (ui.item.value != null) {
			        element.attr('city_id', ui.item.id); 
			        element.attr('city_name', ui.item.label); 
			        cityName = ui.item.label;
			        element.change();
			      }
			    }
			}).on('focus',function(){
			    if ($(this).val() == 'ENTER CITY') {
			      $(this).val(''); $(this).removeClass('inactive');
			    }
			}).on('keypress', function(e){
			    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
			    if(keynum == 13) {//13 is the enter char code
			      e.preventDefault();
			      // $('#quiz .search').focus();
			      // $('#quiz .search').click();
			    }
			});
		}

		this.getOtherLanguage = function() {
			if (seller.meta == null)
				return '';

			for(var i in seller.meta.languages)
				if ( Languages.indexOf(seller.meta.languages[i]) == -1)
					return seller.meta.languages[i];
			return '';
		}

		this.saveAMData = function(val) {
			if (val.length == 0)
				return;

			if (seller.tagFamily == 'specialties') {
				for(var i in seller.am_meta.specialties) {
					if (seller.am_meta.specialties[i].id == seller.editingTag)
						seller.am_meta.specialties[i].desc = val;
				}
			}
			else
				for(var i in seller.am_meta.lifestyles) {
					if (seller.am_meta.lifestyles[i].id == seller.editingTag)
						seller.am_meta.lifestyles[i].desc = val;
				}
		}

		this.getAMData = function() {
			if (seller.tagFamily == 'specialties') {
				for(var i in seller.am_meta.specialties) {
					var tag = seller.am_meta.specialties[i];
					if (tag.id == seller.editingTag)
						return tag.desc.replace(/\\/g,"");
				}
			}
			else
				for(var i in seller.am_meta.lifestyles) {
					var tag = seller.am_meta.lifestyles[i];
					if (tag.id == seller.editingTag)
						return tag.desc.replace(/\\/g,"");
				}
			return '';
		}

		this.setAMTagState = function(tag, family, isChecked) {
			if (family == 'specialties') {
				for(var i in seller.am_meta.specialties) {
					if (seller.am_meta.specialties[i].id == tag)
						seller.am_meta.specialties[i].used = isChecked;
				}
			}
			else
				for(var i in seller.am_meta.lifestyles) {
					if (seller.am_meta.lifestyles[i].id == tag)
						seller.am_meta.lifestyles[i].used = isChecked;
				}
		}

		this.getPortal = function(val, coupon) {
			var myCoupon = typeof inviteCode == 'object' && typeof inviteCode.flags != 'undefined' && inviteCode.flags & (IC_CodeType.IC_FEATURE_PORTAL | IC_CodeType.IC_FEATURE_LIFETIME) ? inviteCode.code :
							(typeof coupon != 'undefined' && coupon.length && coupon != '0' ? coupon : '0');
			seller.DBget({
				query: 'get-portal-name',
				data: { portal: val,
						coupon: myCoupon,
						id: validSeller.id,
						order: seller.am_order,
						portalSpec: seller.portalSpec
					},
				done: function(d) {
					console.log("get-portal-name - "+d.status);
					if (d.status == "Cart made") {
						seller.am_order = d.order;
						window.location = ah_local.wp+'/cart';		
					}
					else if (d.status == "Coupon error") {
						seller.am_order = d.order;
						ahtb.open({html: "Coupon had "+d.errorCount+" error(s):"+d.error,
								   width: 450,
								   heigth: 110 + (d.errorCount *25),
								   buttons: [{text:'OK', action: function() {
								   		window.location = ah_local.wp+'/cart';	
								   }}]});
					}
					else {
						if (typeof d.dashboardCards != 'undefined')
							dashboardCards = d.dashboardCards;
						if (typeof d.seller != 'undefined') {
							seller.page.profile.info = d.seller;
							seller.setupSellerMetas();	
							validSeller = d.seller;
						}

						var ele = $('ul.profile-menu.desktop');
						ele.addClass('premium');
						ele.before('<div class="premium-agent"><span class="main-text"><a href="javascript:seller.getPage('+"'home'"+');"><span>Premium</span> Agent Dashboard</a></span></div><div class="premium-agent-bg"></div>');

						ele = $('ul.profile-menu.desktop li[data-page=home]');
						ele.hide();
						ele = $('ul.profile-menu.desktop li[data-page=stats]');
						ele.attr('id','prem-lis');
						ele = $('ul.profile-menu.desktop li[data-page=portal]');
						ele.attr('id','prem-lis');
						ele.find('a').html('Tools');
						ele.after('<div class="prem-bg"></div>');

						ele = $('div.mobile-profile-menu ul li[data-page=stats]');
						ele.html('<span id="premium"><span class="premium-color">Premium</span>&nbsp;</span>Stats');

						ele = $('div.mobile-profile-menu ul li[data-page=portal]');
						ele.html('<span id="premium"><span class="premium-color">Premium</span>&nbsp;</span>Tools');

						ahtb.open(
							{html: "<p>Congratulations, this portal name is yours!</p>", 
							 height: 120, 
							 width: 450,
							 hideSubmit: true,
							 opened: function() {
							 	window.setTimeout(function() {
							 		ahtb.close();
							 		havePortal = PortalState.PORTAL_VALID;
							 		nickname = val;
							 		seller.currentPage = '';
							 		seller.getPage('portal');
							 	}, 500)
							 }});			
					}		
				},
				error: function(d) {
					var msg = typeof d == 'string' ? d : "Sorry, had a database problem.";
					ahtb.alert(msg, {height: 150, width: 450});
					$('#define-portal #portal').val('');
					$('#marker').hide();
				}
			});
		}

		this.updateMyListingsButton = function() {
			var h = 'Listings ('+(seller.initial_listings ? seller.initial_listings.length : '0')+')';
			$('.sidebar ul li[data-page=listings] a').html(h);
		}

		this.fixRegion = function(region) {
			var index = stateKeys.indexOf(region);
			if (index !== -1)
				return usStates[region];

			return region;
		}

		this.findIPLocation = function(ip) {
			seller.queryIPCount++;
			$.ajax({url: "http://ipinfo.io/"+ip,
					dataType: 'json',
					success: function(d){
						if (typeof d != 'object' ||
							d == null) {
							seller.queryIPCount--;
							return;
						}
						var result = '';
						if ( (typeof d.city == 'undefined' ||
							  typeof d.ip == 'undefined') &&
							  typeof d.hostname != 'undefined')
							result = d.hostname;
						else 
							result = (d.city.length ? d.city : d.ip)+(d.region.length ? ', '+seller.fixRegion(d.region) : ''); //+(d.country.length ? ', '+d.country : '');
						console.log(result+' from ip:'+d.ip);
				    	seller.visitationCities[ d.ip ] = result;
				    	seller.queryIPCount--;
					},
					error: function() {
						seller.queryIPCount--;
			    		console.log( "Failed to locate listing ip:"+ip);
			    		seller.visitationCities[ ip ] = ip;
					}
				})
		}

		this.updateVisitationsListing = function(listing) {
			var visitations = null;
			if (typeof listing.meta == 'undefined' ||
				listing.meta == null ||
				length(listing.meta) == 0)
				return null;

			for(var i in listing.meta) {
				if (listing.meta[i].action == ListingMetaFlags.LISTING_VISITATIONS) {
					visitations = listing.meta[i];
					break;
				}
			}
			if (visitations == null)
				return null;

			if (typeof visitations.updated != 'undefined')
				return visitations;

			visitations.updated = true;

			for(var id in visitations.users) {
				for(var ip in visitations.users[id]) {
					if (typeof seller.visitationCities[ ip ] == 'undefined')
					{
						if (ip != '::1' &&
							ip != '0' &&
							ip != '127.0.0.1') {
							seller.findIPLocation(ip);
						}
						else
							seller.visitationCities[ ip ] = "This PC";
					}
				}
			}
			for(var id in visitations.portalUsers) {
				for(var ip in visitations.portalUsers[id]) {
					if (typeof seller.visitationCities[ ip ] == 'undefined')
					{
						if (ip != '::1' &&
							ip != '0' &&
							ip != '127.0.0.1') {
							seller.findIPLocation(ip);
						}
						else
							seller.visitationCities[ ip ] = "This PC";
					}
				}
			}
			return visitations;
		}

		this.updateVisitationsPortal = function() {
			// author = seller.page.profile.info;
			var author = validSeller;
			for(var i in author.meta) {
				if (author.meta[i].action == SellerMetaFlags.SELLER_VISITATIONS) {
					seller.visitations = author.meta[i];
					break;
				}
			}
			if (seller.visitations == null)
				return;

			if (typeof seller.visitations.updated != 'undefined')
				return;

			seller.visitations.updated = true;

			var visits = 0;
			for(var id in seller.visitations.users) {
				for(var ip in seller.visitations.users[id]) {
					if (typeof seller.visitationCities[ ip ] == 'undefined')
					{
						if (ip != '::1' &&
							ip != '0' &&
							ip != '127.0.0.1') {
							seller.findIPLocation(ip);
						}
						else
							seller.visitationCities[ ip ] = "This PC";
					}
					
					for(var date in seller.visitations.users[id][ip])
						visits++;
				}
			}
			for(var id in seller.visitations.portalUsers) {
				for(var ip in seller.visitations.portalUsers[id]) {
					if (typeof seller.visitationCities[ ip ] == 'undefined')
					{
						if (ip != '::1' &&
							ip != '0' &&
							ip != '127.0.0.1') {
							seller.findIPLocation(ip);
						}
						else
							seller.visitationCities[ ip ] = "This PC";
					}
					
					for(var date in seller.visitations.portalUsers[id][ip])
						visits++;
				}
			}
			seller.portalVisitCount = visits;
			// $('#stats-anchor').html(visits.toString());
		}

		this.testEmail = function() {
			ahtb.loading("Sending test email...");
			seller.DBget({
				query: 'test-email',
				data: { author_id: validSeller.author_id },
				done: function(d) {
					ahtb.alert(d, {height: 150, width: 450});
				},
				error: function(d) {
					console.log(d);
					ahtb.alert(d, {height: 150, width: 450});
				}
			})
		}

		this.miniPortalAgent = function() {
			if (typeof validSeller != 'object')
				return '';

			var h = '<div class="premier-agent">' +
						'<h3>Have a question?</h3>' +
						"<p>I'm here to help you</p>" +
						'<a href="javascript:agentProfile('+ah_local.wp+'/agent/'+nickname+');" target="_blank"><img src="'+ah_local.tp+'/_img/_authors/250x250/'+(validSeller.photo != null ? validSeller.photo : '_blank.jpg')+'" style="width:100%; height:100%"/></a>' +
						'<div class="meta">' +
							'<h4 class="name">'+validSeller.first_name+' '+validSeller.last_name+'</h4>' +
							'<span class="company">'+(validSeller.company ? validSeller.company : '')+'</span>' +
							'<span class="phone">'+(validSeller.phone ? validSeller.phone : '')+'</span>' +
							'<a href="javascript:;" class="contact-button" for="'+validSeller.id+'">Contact Me</a>' +
						'</div>' +
					'</div>';
			return h;
		}

		this.agentMatchSection = function(index, item) {
			item.specialty = parseInt(item.specialty);
			var desc = item.desc;
			
			var h = '<div id="define-agent-match" for="'+index+'" >' +
						'<div class="mainoptions">' +
							'<span class="define-agent-match-span1">' +
								'<input type="checkbox" class="mode" for="'+index+'" '+(item.mode == AgentOrderMode.ORDER_BUYING? "checked " : '')+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'style="display: none;"': '')+' /><span id="buy" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'style="display: none;"': '')+'>'+(couponLA.length ? 'Activate' : 'Buy')+'</span>' +
								'<label for="service_areas">Expertise Service Area</label><input type="text" class="service_areas" for="'+index+'" placeholder="eg: San Francisco, CA" style="color:#444; height: 23px; padding: 0 .25em; position: relative; top: -1px;" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'disabled': '')+' value="'+(item.location ? seller.matchCity(item.location) : '')+'" />' +
							'</span>' +
							'<span class="define-agent-match-span2">' +
								'<label for="field_expertise">Field of Expertise</label>'+
								'<select class="field_expertise" for="'+index+'" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'disabled': '')+'>';
							h+=	lifestyleOptions;
							item.specialtyStr = amTagList[item.specialty].tag;
						h+= '</select>' +
							'</span>';
						if (item.mode == AgentOrderMode.ORDER_BOUGHT)
						h+=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>ACTIVATED LIFESTYLE</span>';
					h+=	'</div>' +
						'<textarea id="description" for="'+index+'" placeholder="Please enter detailed information that showcases your strength for this selection. You need to be knowledgable about and experienced enough to answer any question about this specialty, WITHOUT searching on the internet for it.\n\nFor example:\nWhy do you consider yourself an expert/specialist?\nHow long have you done this?\nDo you have any specialized certifcations or won awards?\nWhat makes you stand out from the rest, in your area, that will make a potential buyer want to talk to you about this?">'+desc+'</textarea>' +
						'<div class="below-textarea">' +
							'<button for="'+index+'" id="update" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? 'class="disabled"' : 'style="display:none;"')+'>Save Description</button>'+
							'<button for="'+index+'" id="sample" '+(item.mode == AgentOrderMode.ORDER_BOUGHT ? (seller.page.profile.info.flags & SellerFlags.SELLER_NEEDS_BASIC_INFO ? 'class="disabled"' : '') : 'style="display:none;"')+'>See it live</button>'+
							'<div id="counter"><label>Minimum Characters Needed:</label>&nbsp;<label id="chCount" >300</label></div>' +
						'</div>' +
					'</div>';
			return h;
		}

		this.matchCity = function(id) {
			for(var i in cities)
				if (cities[i].id == id)
					return cities[i].label;

			console.log("matchCity could not find id:"+id);
			return '';
		}

		this.validateCode = function(code, what, callback) {
			seller.DBget({
				query: 'verify-invitecode',
				data: {code: code,
					   option: what },
				done: function(d) {
					console.log("verify-invitecode - "+d);
					seller.am_invitation = code;
					if (typeof callback == 'function')
						callback();								
	    		},
				error: function(d) {
					if (ahtb.opened) ahtb.push();
					ahtb.alert("We're very sorry, please try another code. "+(typeof d.responseText != 'undefined' && d.responseText.length ? d.responseText : 'Reason unknown'), 
								{height: 150, 
								width: 450,
								buttons: [
									{text:'OK', action: function() {
										ahtb.pop();
									}}]});
				}
			});
		}

		this.processPurchase = function(type) {
			// now do ajax to backend to get the order going...
			ahtb.alert("Processing order...",
						{height: 140,
						 hideSubmit: true});
			seller.am_order.agreed_to_terms = seller.am_agree_to_terms ? 1 : 0;
			seller.DBget({
				query: 'process-agent-order',
				data: {	order: seller.am_order,
					   	product: type,
					   	id: validSeller.id,
					   	portalSpec: seller.portalSpec
					  },
				done: function(d) {
					console.log("process-agent-order - "+d.status);
					if (d.status == "Cart made") {
						seller.am_order = d.order;
						window.location = ah_local.wp+'/cart';		
					}
					else if (d.status == "Coupon error") {
						seller.am_order = d.order;
						ahtb.open({html: "Coupon had "+d.errorCount+" error(s):"+d.error,
								   width: 450,
								   heigth: 110 + (d.errorCount *25),
								   buttons: [{text:'OK', action: function() {
								   		window.location = ah_local.wp+'/cart';	
								   }}]});
					}
					else 
						ahtb.alert(d.status);
	    		},
				error: function(d) {
					ahtb.alert("We're very sorry, had trouble making your cart order.<br/>"+d, {height: 180, width: 450});
				}
			});
		}

		this.page = new function(){
			this.agent_match_reserve = new function() {
				this.get = function() {
					if (timeoutID) window.clearTimeout(timeoutID);
					if (timeoutID2) window.clearTimeout(timeoutID2);
					window.location = ah_local.wp+'/agent-reservation/'+validSeller.id;
				}
			}

			this.agent_match = new function() {
				this.terms = function() {
					lifestyleAgentTermsAndConditions();
					// ahtb.open({
					// 	html: '<div id="ecommerce-terms">'+
					// 		  '<p>LifeStyled&#8482; Agent is an exclusive feature available to agents whose lifestyle specialty has been vetted and approved by the LifeStyled Listings review panel.  '+
					// 		  'Every agent will receive a call for an interview about their selected lifestyle specialty and will need to prove to us that they are genuinely positioned to be part of our exclusive group of agents in a particular area</p>' +
					// 		  '<p>You may apply for any number of cities and specialties as you feel qualified to provide excellent service to unique clientele.  You will be charged for each city/specialty combination at $149.00/month each unless you have received a special promotion offer.  If the vetting process rules unfavorably, then the agent '+
					// 		  ' will have one appeal process to try to convince us of their strength.</p>' +
					// 		  '<p>REFUND POLICY<br/>' +
					// 		  'Refunds will be issued ONLY in the case that an agent has applied to be a Lifestyled&#8482; Agent, was charged, then it was found that he or she did not qualify for one or any of those lifestyles. He or she will be refunded in a reasonable amount of time for those lifestyles that have been paid for but were not found qualified for. No subscriptions fees paid shall be refunded should an agent choose to discontinue his or her subscription.  ' +
					// 		  'Sign up fees, if incurred, are non-refundable at any time.</p>'+
					// 		  '<p>Only three LifeStyled&#8482; Agent positions are open to any city/specialty combination.  Once those three positions are filled, no more agents are accepted until one of those agents relinquishes their position, or moves out of the area.  ' +
					// 		  'Any vetted agent will be placed on a waiting list (fees refunded), and will be notified when a position opens up.  Additional positions may open up if the number of clients visiting an area for a particular lifestyle begins to overload ' +
					// 		  'the approved agents capacity to accommodate them.</p>' +
					// 		  '<p>As a LifeStyled&#8482; participating agent, you also agree to abide by NAR guidelines for paying referral fees to the portal agent, if the buyer found you through the portal agent&#39;s link. The portal agent will be notified ' +
					// 		  'when a buyer contacts a LifeStyled&#8482; agent from within a listing, and will be supplied with the LifeStyled&#8482; agent&#39;s contact information.  LifeStyled&#8482; agents should expect a call from the portal agent to discuss referral agreements.</p>' +
					// 		  '<p>LifeStyled&#8482; Listings and it’s parent company Allure Technologies, LLC is not liable for any materialization of any referral fees expected by parties involved.  By agreeing to this Terms and Conditions, you are hereby agreeing to a &quot;gentleman&#39;s&quot; agreement, ' +
					// 		  'and it is wholly the responsibility of real estate agents to abide by their own agreements and ethics.</p>' +
					// 		  '<p>You hereby agree to the fees imposed, the vetting process and ethics standard.</p>' +
					// 		  '</div>',
					// 	width: 850,
					// 	height: 630,
					// 	title: "Terms And Conditions"
					// })
				}
				this.matchDescWithProfile = function(id, force) {
					var textarea = $('.define-agent-match-container #define-agent-match #description[for="'+id+'"]');
					var item = seller.am_order.item[id];
					var desc = item.desc;
					var forceIt = typeof force != 'undefined' ? force : false;
					// use existing description if there is one from profile's agent match section
					if (seller.am_meta &&
						item.specialty &&
						(item.desc.length == 0 || forceIt) ) {
						desc = ''; // blank it
						var found = false;
						for(var i in seller.am_meta.specialties)
							if (seller.am_meta.specialties[i].id == item.specialty) {
								found = true;
								//if (seller.am_meta.specialties[i].desc.length)
									desc = seller.am_meta.specialties[i].desc;
								break;
							}

						if (!found) for(var i in seller.am_meta.lifestyles)
							if (seller.am_meta.lifestyles[i].id == item.specialty) {
								found = true;
								//if (seller.am_meta.lifestyles[i].desc.length)
									desc = seller.am_meta.lifestyles[i].desc;
								break;
							}
						item.desc = desc;
					}
					textarea.val(desc.replace(/\\/g,""));

					var len = MIN_LENGTH_DESC - textarea.val().length;
					var counter = len > 0 ? len : 0;
					var parent = $('.define-agent-match-container');
					var sub = parent.find('#define-agent-match[for='+id+']');
					var ele = sub.find('div#counter #chCount');
					ele.html(counter.toString());

					if (parseInt(item.location) == 0) {
						var service_area = validSeller.service_areas ? validSeller.service_areas.replace(/"/g, '&quot;') : '';
						service_area = service_area != '' ? service_area.split(":") : [];
						if (service_area.length)
							seller.page.agent_match.getCityId(id, service_area[0], true);
						else if (validSeller.city != null &&
							validSeller.state != null) {
							var city = validSeller.city+", "+validSeller.state;
							seller.page.agent_match.getCityId(id, city);
						}
					}
					else {
						var city = seller.matchCity(item.location);
						$('.define-agent-match-container #define-agent-match .service_areas[for="'+id+'"]').val(city);
						item['locationStr'] = city;
						if (!isNaN(item.specialty) && parseInt(item.specialty) != 0)
							item['specialtyStr'] = amTagList[item.specialty].tag;
					}
				}
				this.calculateSubTotal = function(updateModels) {
					var buying = 0;
					var price = 0;
					updateModels = typeof updateModels == 'undefined' ? true : updateModels;
					for(var i in seller.am_order.item)
						if (seller.am_order.item[i].mode == AgentOrderMode.ORDER_BUYING &&
							seller.am_order.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH) {
							if (updateModels) {
								seller.am_order.item[i].subscriptionType = seller.subscriptionType;
								seller.am_order.item[i].priceLevel = seller.priceLevel;
							}
							buying++;
							var subscriptionType = typeof seller.am_order.item[i].subscriptionType == 'undefined' ? 0 : seller.am_order.item[i].subscriptionType;
							var priceLevel = typeof seller.am_order.item[i].priceLevel == 'undefined' ? 4 : seller.am_order.item[i].priceLevel;
							var curPrice = getPrice(subscriptionType, priceLevel);
							if (curPrice <= discountLA)
								curPrice = 0;
							else
								curPrice -= discountLA;
							price += curPrice;
					}
				 	price += (seller.addSignUpFee ? 100 : 0);
					var money = '$'+price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					$('.agent-match-div .maincontent .purchase #subtotal').html(money);

				}
				this.pickedCity = function(id, city_id, city) {
					seller.am_order.item[id].location = city_id;
					seller.am_order.item[id].locationStr = city;
					$('#define-agent-match input.service_areas[for="'+id+'"]').val(city);
					ahtb.close();
				}
				this.getCityId = function(id, city, fromMatchDescWithProfile) {
					$.post(ah_local.tp+'/_pages/ajax-quiz.php',
		                { query:'find-city',
		                  data: { city: city,
		                  		  distance: 0 }
		                },
		                function(){}, 'JSON')
						.done(function(d){
							if (d &&
		                        d.data &&
		                        d.data.city &&
		                        (typeof d.data.city == 'object' ||
		                        d.data.city.length) ){
			                    if (d.status == 'OK' &&
			                        (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
			                        console.log(JSON.stringify(d.data));

			                    	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
				                        ahtb.alert("Please enter a city name along with the state.", {height: 150});
				                        return;
				                    }

				                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
			                    	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
									if (len > 1) {
										if (d.data.allState == 'false' || d.data.allState == false) {
					                        var h = '<p>Please pick one of the cities:</p>' +
					                                  '<ul class="cities">';
					                        for(var i in d.data.city) {
					                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
					                          	h += '<li><a href="javascript:seller.page.agent_match.pickedCity('+id+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
					                        }
					                        h += '</ul>';
					                        ahtb.open(
					                          { title: 'Cities List',
					                            width: 380,
					                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
					                            html: h,
					                            buttons: [
					                                  {text: 'Cancel', action: function() {
					                                    ahtb.close();
					                                  }}],
					                            opened: function() {
					                            }
					                          })
					                    }
					                    else {
				                          	ahtb.alert("Only one city/state combination accepted.", {height: 150});
				                        }
				                    } // only one!
				                    else {
				                    	seller.page.agent_match.pickedCity(id, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state);
				                    	if (seller.matchCity(d.data.city[0].id).length == 0) {
				                    		var label = d.data.city[0].city+', '+d.data.city[0].state;
				                    		cities[cities.length] = {label: label, value: label, id: d.data.city[0].id};
				                    	}
				                    }
			                    }
			                    else if ( typeof fromMatchDescWithProfile == 'undefined' ||
			                		  !fromMatchDescWithProfile) {
			                      ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
			                      			{title: 'City Match', 
			                      			 height:180, 
			                      			 width:600});
			                    }
			                }
			                else if ( typeof fromMatchDescWithProfile == 'undefined' ||
			                		  !fromMatchDescWithProfile) {
			                    ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
			                      			{title: 'City Match', 
			                      			 height:180, 
			                      			 width:600});
			                }
		                })
		                .fail(function(d){
		                	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
			                      			{title: 'City Match', 
			                      			 height:180, 
			                      			 width:600});
		                });
				}
				this.add_callbacks = function() {
					$('.define-agent-match-container #define-agent-match .mode').prop('disabled', !seller.am_agree_to_terms);
					$('.define-agent-match-container #define-agent-match .mode').off().on('change', function () {
						var item = $(this).attr('for');
						var checked = $(this).prop('checked');
						seller.am_order.item[item].mode = checked ? AgentOrderMode.ORDER_BUYING : AgentOrderMode.ORDER_IDLE;
						console.log("AM mode is "+(checked ? "Buying" : "Idle")+' for:'+item);
						seller.page.agent_match.calculateSubTotal();
					});

					for(var j in seller.am_order.item) {
						seller.page.agent_match.add_callback(j);
					}
				}

				this.add_callback = function(id) {
					var ele = $('.define-agent-match-container #define-agent-match .mode[for='+id+']');
					ele.prop('disabled', !seller.am_agree_to_terms);
					$('.define-agent-match-container #define-agent-match .mode[for='+id+']').on('change', function () {
						var item = $(this).attr('for');
						var checked = $(this).prop('checked');
						seller.am_order.item[item].mode = checked ? AgentOrderMode.ORDER_BUYING : AgentOrderMode.ORDER_IDLE;
						console.log("AM mode is "+(checked ? "Buying" : "Idle")+' for:'+item);
						seller.page.agent_match.calculateSubTotal();
					});

					$('.define-agent-match-container #define-agent-match .service_areas[for='+id+']').on('change', function() {
						var item = $(this).attr('for');
						var val = $(this).attr('city_id');
						var cityName = $(this).attr('city_name');
						var city = $(this).val();
						console.log("AM service area is "+city+' or '+cityName+', val:'+val+', for:'+item);
						if (typeof val == 'undefined' &&
							city.length)
							seller.page.agent_match.getCityId(item, city);
						else if (typeof val != 'undefined' &&
								 typeof cityName == 'string') {
							seller.am_order.item[item].location = val;
							seller.am_order.item[item].locationStr = cityName;
						}
					});
					
					$('.define-agent-match-container #define-agent-match .field_expertise[for='+id+']').on('change', function() {
						var item = $(this).attr('for');
						var val = $(this,'option:selected').val();
						console.log("AM specialty is "+val+' for:'+item);
						seller.am_order.item[item].specialty = parseInt(val);
						seller.am_order.item[item].specialtyStr = amTagList[val].tag;
						seller.page.agent_match.matchDescWithProfile(item, true);
					});

					$('.define-agent-match-container #define-agent-match textarea[for='+id+']').on('keyup', function(e) {
						var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
					    if(keynum == 13) {//13 is the enter char code
					      e.preventDefault();
					    }
						var len = MIN_LENGTH_DESC - $(this).val().length;
						var counter = len > 0 ? len : 0;
						var parent = $('.define-agent-match-container');
						var sub = parent.find('#define-agent-match[for='+id+']');
						var ele = sub.find('div#counter #chCount');
						ele.html(counter.toString());
						ele = sub.find('div.below-textarea button#update');
						ele.removeClass('disabled');
						// $('.define-agent-match-container #define-agent-match  #chCount[for='+id+']').html(counter.toString());
					})
                    
                    $('.popup-block-modal button').on('click', function() {
                      $('.popup-block-modal').removeClass('popout');
                    });

					var parent = $('.define-agent-match-container');
					var sub = parent.find('#define-agent-match[for='+id+']');
					var ele = sub.find('button#update');
					// $('.define-agent-match-container #define-agent-match[for='+id+'] button#update').click(function() {
					ele.click(function() {
						if ( $(this).hasClass('disabled'))
							return;
						$('.define-agent-match-container #define-agent-match #description').each(function() {
							var item = $(this).attr('for');
							var desc = $(this).val();
							seller.am_order.item[item].desc = desc;
							console.log("AM desc for:"+item+", is:"+desc);
						});
						var metas = validSeller.meta;
						var id = 0;
						var original = null;
						for(var i in metas) {
							if (metas[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) {
								original = metas[i];
								break;
							}							
						}
						var needUpdate = false;
						if (original != null) {
							for(var i in original.item)
								if (original.item[i].desc != seller.am_order.item[i].desc) {
									needUpdate = true;
									break;
								}
						}

						if (!needUpdate) {
							ahtb.alert("Nothing to update", {height: 150});
							return;
						}

						// now do ajax to backend to update meta data
						ahtb.alert("Updating description...",
									{height: 140,
									 hideSubmit: true});
						seller.DBget({
							query: 'update-agent-match-order',
							data: {order: seller.am_order,
								   id: validSeller.id },
							done: function(d) {
								console.log("update-agent-match-order - "+d.status);
								seller.page.profile.info = d.seller;
								validSeller = d.seller;
								ahtb.close();							
				    		},
							error: function(d) {
								ahtb.alert("We're very sorry, had trouble updating the descriptions. "+d.status, {height: 150, width: 450});
							}
						});
					})

					ele = sub.find('button#sample');
					ele.on('click', function() {
						var id = ele.attr('for');
						console.log("viewProfile clicked for "+id+", specialty:"+seller.am_order.item[id].specialtyStr+", location:"+seller.am_order.item[id].locationStr);
										
						var query = {query: 'get-sample-listing',
									 data: {
									 	id: validSeller.id,
									 	tag: seller.am_order.item[id].specialty,
									 	tagStr: seller.am_order.item[id].specialtyStr,
									 	location: seller.am_order.item[id].location,
									 	locationStr: seller.am_order.item[id].locationStr
									 },
									 done: function(d) {
                                        var isIE = browser.shortname.indexOf('MSIE') != -1 && document.body.createControlRange;
                                        var isEdge =  browser.shortname == 'Edge';
										 if (isMobile) {
											 window.location.replace(ah_local.wp+'/listing/S'+seller.am_order.item[id].specialty+'-'+d.id);
										 }
										 else {
											 var newWin = window.open(ah_local.wp+'/listing/S'+seller.am_order.item[id].specialty+'-'+d.id);
											 if(!newWin || newWin.closed || typeof newWin.closed=='undefined') {
												 $('.popup-block-modal').addClass('popout');
												 if (isIE ||
														 isEdge) {
													 $('.popup-block-modal').addClass('ieoredge');
												 }
											}
										 }
									 },
									 error: function(d) {
									 	var msg = typeof d.status != 'undefined' ? d.status : "Error occured trying to get a sample listing";
									 	ahtb.alert(	msg, 
									 				{width: 500,
									 				 height: 150 + (Math.floor(msg.length/70) * 32) });
									 }};
						seller.DBget(query);
					})
				}
				this.add_another = function() {
					var newItem = {
						mode: seller.doingSales ? AgentOrderMode.ORDER_IDLE : AgentOrderMode.ORDER_BUYING,
						type: AgentOrderMode.ORDER_AGENT_MATCH,
						desc: '',
						location: 0,
						specialty: '169',
						locationStr: '',
						specialtyStr: 'Artistic',
						cart_item_key: 0,
						order_id: 0,
						subscriptionType: seller.subscriptionType,
						priceLevel: seller.priceLevel
					}
					var id = 0;
					if (seller.am_order) {
						id = seller.am_order.item.length;
						seller.am_order.item[seller.am_order.item.length] = newItem;
					}
					else {
						seller.am_order = {
							action: SellerMetaFlags.SELLER_AGENT_ORDER,
							order_id_last_purchased: 0,
							order_id_last_cancelled: 0,
							agreed_to_terms: 0,
							item: [newItem]
						}
					}
					$('.agent-match-div .maincontent .define-agent-match-container').append(seller.agentMatchSection(id, newItem));		
					$('select.field_expertise[for='+id+'] option[value="'+newItem.specialty+'"]').prop('selected', true);		
					seller.page.agent_match.add_callback(id);

					// var element = $('.define-agent-match-container #define-agent-match .service_areas[for='+id+']');
					var element = $('input.service_areas[for='+id+']');
					seller.setupAutocomplete(element);
					seller.page.agent_match.matchDescWithProfile(id);				

					var scrollTop = seller.am_order.item.length > 2 ? (seller.am_order.item.length - 2) * $('.define-agent-match-container #define-agent-match').outerHeight(true) : 0;
					$('.define-agent-match-container').scrollTop(scrollTop);
					console.log("calculateSubTotal - scrollTop:"+scrollTop);

					seller.page.agent_match.calculateSubTotal();
				}

				this.processAMInvite = function() {
					// now do ajax to backend to get the order going...
					seller.am_order.agreed_to_terms = seller.am_agree_to_terms ? 1 : 0;
					ahtb.alert("Processing invitation...",
								{height: 140,
								 hideSubmit: true});
					seller.DBget({
						query: 'process-agent-match-invite',
						data: {order: seller.am_order,
								code: seller.am_invitation,
								id: validSeller.id,
								portalSpec: seller.portalSpec},
						done: function(d) {
							if (typeof d.dashboardCards != 'undefined')
								dashboardCards = d.dashboardCards;

							console.log("process-agent-match-invite - "+d.msg);
							if (d.status == "Update made") {
								seller.am_order = d.order;
								window.location.reload();
							}	
							else if (d.status == "Cart made") {
								seller.am_order = d.order;
								window.location = ah_local.wp+'/cart';		
							}
							else if (d.status == "Coupon error") {
								seller.am_order = d.order;
								ahtb.open({html: "Coupon had "+d.errorCount+" error(s):"+d.error,
										   width: 450,
										   heigth: 110 + (d.errorCount *25),
										   buttons: [{text:'OK', action: function() {
										   		window.location = ah_local.wp+'/cart';	
										   }}]});
							}
							else 
								ahtb.alert(d.status, {width: 450, height: 150});								
			    		},
						error: function(d) {
							ahtb.alert("We're very sorry, had trouble processing your invitation. "+(typeof d.responseText != 'undefined' && d.responseText.length ? d.responseText : 'Reason unknown'), {height: 150, width: 450});
						}
					});
				}

				this.applyCoupon = function(inviteCodeUsed) {
					var laInviteUsedCount = 0;
					var addedBuyingOnes = false;

					if (inviteCodeUsed.length == 0)
						return 0;

					// first count how many have been bought using this code
					for(var i in seller.am_order.item) {
						if (seller.am_order.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH) {
							if ( seller.am_order.item[i].mode == AgentOrderMode.ORDER_BOUGHT &&
							     seller.am_order.item[i].order_id == DEFAULT_INVITED_LIFESTYLE_AGENT_MAGIC_NUM &&
							     seller.am_order.item[i].cart_item_key == inviteCodeUsed)
							laInviteUsedCount++;
						}
					}

					var byPassCounter = typeof inviteCode == 'object' && typeof inviteCode.flags != 'undefined' && inviteCode.flags & IC_CodeType.IC_FEATURE_LIFETIME ? true : false;
					var overUsed = byPassCounter ? false : laInviteUsedCount >= maxFreeLA;
					// now let's see how many more we can add to this code usage
					// if there are more BUYING than can fit in maxFreeLA limit, set those to IDLE
					if (!overUsed) for(var i in seller.am_order.item) {
						if (seller.am_order.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH) {
							if (seller.am_order.item[i].mode == AgentOrderMode.ORDER_BUYING) {
								if (byPassCounter ||
									laInviteUsedCount < maxFreeLA) {
									laInviteUsedCount++;
									addedBuyingOnes = true; // mark that we have legit ones
								}
								else { // sorry, limit reached, these will be reset to IDLE
									seller.am_order.item[i].mode == AgentOrderMode.ORDER_IDLE;
									console.log("Forcing LA for "+seller.am_order.item[i].specialtyStr+" in "+seller.am_order.item[i].locationStr+" to IDLE");
								}
							}
						}
					}

					if ( addedBuyingOnes ) {
						seller.validateCode(inviteCodeUsed,
											InviteCodeMode.IC_FEATURE_LIFESTYLE,
											seller.page.agent_match.processAMInvite);
						return 1; // all OK
					}

					return !addedBuyingOnes && !overUsed ? 2 : 3; // 2 = nothing added to buy, 3 = invite code used up
				}

				this.get = function() {
					var page_content_agent_match =
									'<div class="agent-match-div">' +
										'<div class="maincontent">' +
											'<div id="headers"></div>' +
											'<div id="options" style="margin: 10px 0 10px 20px; width: 100%"></div>' +
											'<div class="define-agent-match-container">' + '</div>'  +
											'<div id="more">' +
												'<a href="javascript:;" class="another"><span>Add Another</span><span class="entypo-down-open"></span></a>' +
												'<a href="javascript:seller.page.agent_match.terms();" id="terms">Terms and Conditions</a>' +
												'<input type="checkbox" id="agree">I agree</input>' +
											'</div>' +
											'<div class="purchase" style="text-align:center">' +
												'<button id="checkout">Get it Now</button>' +
												'<span id="subtotal_label" style="margin-left:10px;">Total fees: </span><span id="subtotal">$0.00</span>' +
											'</div>' +
											'<div id="agent-lifestyle-reservation-sellers">' +
												'<div class="top">' +
													'<div class="whatislifestyle">' +
														'<div class="left">' +
															'<iframe src="https://player.vimeo.com/video/133201928" style="padding-left: 12%;" width="600" height="334" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' +
														'</div>' +
														'<div class="right">' +
															'<span class="title"><span style="color:#F9A054">What</span> is Lifestyled Agent?</span>' +
															'<p>A Lifestyled Agent is an expert in one or more lifestyles that our home buyers are looking for. Agents can choose from 30 lifestyles such as equestrian, golfing, sailing or even property types like gated communities, ranches, condos and more. Lifestyled agents have more to offer their clients than a real estate license!</p>' +
															'<p>And perhaps best of all - it\'s free!</p>' +
														'</div>' +
													'</div>' +
												'</div>' +
												'<div class="mid">' +
													'<div class="left">' +
														'<span class="title"><span style="color:#F9A054">Why</span> become a Lifestyled Agent?</span>' +
														'<p>Because you have knowledge, insight and passion of certain lifestyles that can truly help our buyers.</p>' +
														'<p class="mobilespace">Studies like NAR\'s Buyer and Seller Profile and Better Homes and Gardens survey show that buyers are most concerned with trust, communication, and insight into the local lifestyle. <span style="font-weight:400">Sharing a common passion or being an expert in the client\'s needs is the perfect foundation for building a strong relationship of trust, understanding, and value to the consumer.</span></p>' +
													'</div>' +
													'<div class="divider">' +
														'<div class="line1"></div>' +
														'<span>+</span>' +
														'<div class="line2"></div>' +
													'</div>' +
													'<div class="right">' +
														'<span class="title"><span style="color:#F9A054">What\'s</span> in it for you?</span>' +
														'<p>How about being the preferred agent that shares the lifestyle with the buyer?</p>' +
														'<p class="mobilespace" style="font-weight:400">If you are looking for a new home, wouldn\'t you rather work with an agent that shares a passion with you, or is an expert about the particular property type you’re searching for?  By matching you to a specific buyer interests and needs we recommend you as the best agent for them.</p>' +
													'</div>' +
												'</div>' +
												'<div class="circle">' +
													'<span class="title">Why are we so different?</span>' +
													'<p>You\'ll get the opportunity to work with buyers that are carefully matched to your exact lifestyle. This will bring agents and buyers closer than ever before.</p>' +
													'<p style="width: 60%; margin: 20px auto 0;">Lifestyled Agents are agents that make a difference.</p>' +
												'</div>' +
												'<div class="bottom"></div>' +
												// '<div class="calltoaction">' +
												// 	'<a href="#"><span class="entypo-up-open-mini"></span> Sound Good? Call us to get more lifestyles.<span class="entypo-up-open-mini"></span></a>' +
												// '</div>' +
											'</div>' +
										'</div>' +
									'</div>';
					seller.content({
						title: 'Lifestyled Agent',
						content: page_content_agent_match,
						callback:function() {
							var metas = validSeller.meta;
							var id = 0;
							// populate options to select product variations
							if (seller.doingSales) {
								$('.maincontent #headers').hide();
								$('.maincontent .define-agent-match-container').css('max-height', '615px');
								var h = '<table style="width: 96%;">' +
											'<tbody>'+
												'<tr>' +
													'<td style="width: 14%;">' +
														'<span id="subscription-type"><h1>Type</h1></span>' +
														'<input type="radio" id="monthly" name="subscription" value="0" checked >Monthly</input><br/>' +
														'<input type="radio" id="quarterly" name="subscription" value="1" >Quarterly</input>' +
													'</td>' +
													'<td style="padding-left: 25px; width: 42%; overflow: wrap;">' +
														'<span id="pricing-level"><h1>Pricing Level</h1></span>' +
														'<table id="pricing">' +
															'<tbody>' +
																'<tr>';
															for(var index in productPricing) {
														h +=	'<td>';
															h +=	'<span style="display: block;" id="price">$'+productPricing[index].monthly+'</span>';
															h += 	'<input type="radio" id="'+productPricing[index].grade+'" name="pricing" value="'+index+'" checked >'+productPricing[index].grade+'</input>&nbsp;' +
																'</td>';
															}
														h +=	'</tr>' +	
															'</tbody>' +
														'</table>' +
												 	'</td>' +
													'<td style="width: 18%;">' +
														'<span id="signupfee" ><strong>Add Signup Fee</strong></span><input type="checkbox" id="signupfee" />' +
													'</td>'
												'</tr>' +
											'</tbody>' +
										'</table>';

								$('.maincontent #options').html(h).css('border-bottom', '1px solid black');
								$('.maincontent input[name="pricing"][value="'+seller.priceLevel+'"]').prop('checked', true);
								$('#options input#signupfee').prop('checked', seller.addSignUpFee);

								$('#options input[name="subscription"]').on('change', function() {
									var val = $(this).attr('id');
									var checked = $(this).prop('checked');
									console.log(val+" is now "+checked);
									seller.subscriptionType = parseInt($(this).attr('value'));
									seller.page.agent_match.calculateSubTotal();
									var index = 0;
									$('#pricing span#price').each(function() {
										$(this).text('$'+productPricing[index][val]);
										index++;
									})
								})
								$('#options input[name="pricing"]').on('change', function() {
									var val = $(this).attr('id');
									var checked = $(this).prop('checked');
									console.log(val+" is now "+checked);
									seller.priceLevel = parseInt($(this).attr('value'));
									seller.page.agent_match.calculateSubTotal();
								})
								$('#options input#signupfee').on('change', function() {
									var val = $(this).attr('id');
									var checked = $(this).prop('checked');
									console.log(val+" is now "+checked);
									seller.addSignUpFee = checked;
									seller.page.agent_match.calculateSubTotal();
									var gotOne = false;
									if (seller.am_order) {
										for(var j in seller.am_order.item) {
											if ( parseInt(seller.am_order.item[j].type) ==  AgentOrderMode.ORDER_SIGN_UP) {
												gotOne = true;
												var mode = parseInt(seller.am_order.item[j].mode); 
												if (mode == AgentOrderMode.ORDER_BOUGHT)
													$('#options input#signupfee').prop('checked', true); // force it no matter what if already bought
												else
													seller.am_order.item[j].mode = checked ? AgentOrderMode.ORDER_BUYING : AgentOrderMode.ORDER_IDLE;
												break;
											}
										}
									}
									if (!gotOne) { // add a new item
										var newItem = {
											mode: checked ? AgentOrderMode.ORDER_BUYING : AgentOrderMode.ORDER_IDLE,
											type: AgentOrderMode.ORDER_SIGN_UP,
											cart_item_key: 0,
											order_id: 0,
										}
										seller.am_order.item[seller.am_order.item.length] = newItem;
									}
								})
							}
							else {
								//var h = '<span id="title1">Welcome to LifeStyled<span style="font-size: 0.6em;position: absolute;margin-top: -6px;">&#8482;</span> <span style="margin-left:14px;">Agent</span></span><span class="subtitle">Edit your current Lifestyled Agent areas and fields of expertise, or add more!</span>';
								var h = '<span id="title1">Welcome to Lifestyled Agent</span><span class="subtitle">Edit your current Lifestyled Agent areas and fields of expertise, or add more!</span><button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>';
								$('.maincontent #headers').html(h);
								$('.video').on('click', function() {
									var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-lifestyle']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								$('.videoDiv .video').html(h);
									videoControl.showVid();
								})

								if (isFreeLA) {
									$('span#subtotal_label').hide();
									$('span#subtotal').hide();
								}

							}

							for(var i in metas) {
								if (metas[i].action == SellerMetaFlags.SELLER_AGENT_ORDER &&
									seller.am_order == null) {
									seller.am_order = JSON.parse(JSON.stringify(metas[i])); // makes copy of it
								}
								else if (metas[i].action == SellerMetaFlags.SELLER_AGENT_MATCH_DATA &&
										seller.am_meta == null) {
									seller.am_meta = JSON.parse(JSON.stringify(metas[i])); // makes copy of it
								}	
							}
							if (seller.am_order) {
								for(var j in seller.am_order.item) {
									if ( parseInt(seller.am_order.item[j].type) ==  AgentOrderMode.ORDER_AGENT_MATCH) {
										if (seller.am_order.item[j].location) {
											if (seller.matchCity(seller.am_order.item[j].location).length == 0) {
												cities[cities.length] = {label: seller.am_order.item[j].locationStr, value: seller.am_order.item[j].locationStr, id: parseInt(seller.am_order.item[j].location) };
											}
										}
										$('.agent-match-div .maincontent .define-agent-match-container').append(seller.agentMatchSection(j, seller.am_order.item[j]));
										$('select.field_expertise[for='+j+'] option[value="'+seller.am_order.item[j].specialty+'"]').prop('selected', true);
										if (seller.am_order.item[j].mode == AgentOrderMode.ORDER_BUYING)
											seller.priceLevel = seller.am_order.item[j].priceLevel;
										// $('.define-agent-match-container #define-agent-match input.service_areas[for="'+j+'"]').val(seller.am_order.service_area);
										// seller.setupAutocomplete($('.define-agent-match-container #define-agent-match input.service_areas[for='+j+']'));
										seller.setupAutocomplete($('input.service_areas[for='+j+']'));
										seller.page.agent_match.matchDescWithProfile(j);
										id++;
										if (seller.am_order.item[j].mode == AgentOrderMode.ORDER_BOUGHT )
											seller.am_agree_to_terms = 1;
									}
									else if ( parseInt(seller.am_order.item[j].type) ==  AgentOrderMode.ORDER_SIGN_UP) {
										var mode = parseInt(seller.am_order.item[j].mode); 
										$('#options input#signupfee').prop('checked', mode != AgentOrderMode.ORDER_IDLE); // if not idle, then have either bought it or is buying it
										if (mode == AgentOrderMode.ORDER_BOUGHT) {
											$('#options input#signupfee').prop('disabled', true)	;
											$('#options span#signupfee').html('<strong>Bought Signup Fee</strong>');
										}
										else if (mode == AgentOrderMode.ORDER_BUYING)
											seller.addSignUpFee = true;
									}
								}
								seller.am_agree_to_terms = seller.am_order.agreed_to_terms === 'true' || seller.am_order.agreed_to_terms === true || seller.am_order.agreed_to_terms === 1 ? 1 : 0;
								$('.maincontent input[name="pricing"][value="'+seller.priceLevel+'"]').prop('checked', true);
							}
							if (id == 0)
								seller.page.agent_match.add_another();
							else
								seller.page.agent_match.add_callbacks();

							seller.page.agent_match.calculateSubTotal(false); // true will have set all ORDER_BUYING to the current subscription and pricing models


							$('.maincontent #more a.another').on('click', function() {
								seller.page.agent_match.add_another();;
							})
						
							$('.maincontent .purchase #checkout').on('click', function() {
								$('.define-agent-match-container #define-agent-match #description').each(function() {
									var item = $(this).attr('for');
									var desc = $(this).val();
									seller.am_order.item[item].desc = desc;
									console.log("AM desc for:"+item+", is:"+desc);
								});

								for(var i in seller.am_order.item) {
									if (seller.am_order.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH) {
										if (seller.am_order.item[i].mode == AgentOrderMode.ORDER_BUYING) {
											if (seller.am_order.item[i].desc.length < MIN_LENGTH_DESC) {
												var msg = "Please enter at least "+MIN_LENGTH_DESC+" characters to describe "+seller.am_order.item[i].specialtyStr;
												    msg+= " in "+seller.am_order.item[i].locationStr;
												ahtb.alert(msg, {width: 500, height: 180});
												return;
											}
										}
									}
								}
								
								if ( !seller.am_order.agreed_to_terms) {
									ahtb.alert("Please agree to Terms and Conditions first.");
									return;
								}

								var myCoupon = typeof inviteCode == 'object' && typeof inviteCode.flags != 'undefined' && inviteCode.flags & (IC_CodeType.IC_FEATURE_LIFESTYLE | IC_CodeType.IC_FEATURE_LIFETIME) ? inviteCode.code : couponLA;

								if (myCoupon.length) {
									switch (seller.page.agent_match.applyCoupon(myCoupon)) {
										case 0: // shouldn't happen here, since couponLA has length
											break;
										case 1: console.log("called validateCode for "+myCoupon);
											return;
										case 2: ahtb.alert("No lifestyle was checked to ACTIVATE");
											return;
										case 3: 
											couponLA = '';
											discountLA = 0;
											isFreeLA = false;
											return;
									}
								}
								
								var h = "<span>Do you have an invitation code or coupon?</span>" +
										'<input type=text id="invite" placeholder="Enter code" style="margin-bottom:15px;"></input>';
								ahtb.open({
									title: "Lifestyled Agent Checkout",
									html: h,
									height: 150,
									opened: function() {
										if (couponLA.length)
											$('#invite').val(myCoupon);
									},
									buttons: [
										{text: "Apply", action: function() {
											var inviteCodeUsed = $('#invite').val();
											switch (seller.page.agent_match.applyCoupon(inviteCodeUsed)) {
												case 0: 
													ahtb.push();
													ahtb.alert("Please enter a code.", 
															{height: 150,
															 buttons: [{text:"OK", action: function() { ahtb.pop(); }}]});
													break;
												case 1: 
													console.log("called validateCode for "+inviteCodeUsed);
													return;
												case 2: 
													ahtb.alert("No lifestyle was checked to BUY");
													return;
												case 3: 
													ahtb.push();
													ahtb.alert("Please enter a different code, maximum usage of "+maxFreeLA+ ' reached for this code.', 
																	{height: 160,
																	 buttons: [{text:"OK", action: function() { ahtb.pop(); }}]});
													return;
											}
										}},
										{text: "Purchase", action: function() {
											if (eCommerceReady) {
												var index = 0;
												var validItems = [];
												if (seller.am_order.item.length > 1) {
													var haveOnePortal = false;
													// this for() loop will sanitize the the order list such that there will be only
													// one Portal and one combination of AM, no duplication....
													for(var i in seller.am_order.item) {
														if (seller.am_order.item[i].cart_item_key == 'false' ||
															seller.am_order.item[i].cart_item_key == false)
															seller.am_order.item[i].cart_item_key = 0; // make into a number
														// if (validItems.length == 0) {
														// 	validItems[validItems.length] = seller.am_order.item[i];
														// 	haveOnePortal = seller.am_order.item[i].type == AgentOrderMode.ORDER_PORTAL;
														// }
														// else {
															var foundIt = false;
															for(var j in validItems) {
																if (seller.am_order.item[i].type == validItems[j].type) {
																	if (validItems[j].type == AgentOrderMode.ORDER_PORTAL) {
																		if (haveOnePortal)
																			foundIt = true;
																		haveOnePortal = true; // don't need a second one..
																	}
																	else if (seller.am_order.item[i].location == validItems[j].location &&
																			 seller.am_order.item[i].specialty == validItems[j].specialty)
																		foundIt = true;
																	if (foundIt) break;
																}
															}
															if (!foundIt) 
																validItems[validItems.length] = seller.am_order.item[i];
														// }
													}
													seller.am_order.item = validItems;
												}
												seller.processPurchase(AgentOrderMode.ORDER_AGENT_MATCH);												
											}
											else {
												var h = '<div id="noEcommerce">' +
															"eCommerce is not ready yet.</br>" +
															"Would you like to reserve your spots as a LifeStyled&#8482; Agent?</br></br>" +
															"*Reserving your spot does not guarantee you will be accepted.&nbsp;On a first come basis, each agent will be verified of their expertise." +
														'</div>';
												ahtb.open(
													{html: h,
													height: 220, 
													 width: 550,
													 buttons: [
													 {text:"Yes", action: function() {
													 	var itemList = {};
													 	itemList.action = AgentOrderMode.ORDER_AGENT_MATCH;
													 	itemList.item = [];
													 	for(var i in seller.am_order.item) {
													 		if (seller.am_order.item[i].type == AgentOrderMode.ORDER_AGENT_MATCH &&
													 			seller.am_order.item[i].mode == AgentOrderMode.ORDER_BUYING) {
														 		var item = {};
														 		item.location = seller.am_order.item[i].location;
														 		item.locationStr = seller.am_order.item[i].locationStr;
														 		item.specialty = seller.am_order.item[i].specialty;
														 		item.specialtyStr = seller.am_order.item[i].specialtyStr;
														 		item.desc = seller.am_order.item[i].desc;
														 		itemList.item[itemList.item.length] = item;
														 	}
													 	}
													 	seller.DBget({
															query: 'reserve-agent-product',
															data: {id: validSeller.id,
																   what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_2,
																   lifestyles: itemList },
															done: function(d) {
																console.log("reserve-agent-product - "+d);
																var msg = "Your LifeStyled&#8482; Agent reservation ";
																ahtb.alert(msg +", is now reserved.  Please expect a call from us soon.  If you don't hear from us within 48hr or less, please contact us via our Feedback!",
																			{height: 210,
																			 width: 450});							
												    		},
															error: function(d) {
																console.log("We're very sorry, had trouble reserving your spots. "+d);
																ahtb.alert("We're very sorry, had trouble reserving your spots. "+d, {height: 180, width: 450});
															}
														});
													 }},
													 {text:"Not Yet", action: function() { ahtb.close(); }}] 
												});
											}
										}}]
								})		
							});

							$('.agent-match-div .purchase button').prop('disabled', !seller.am_agree_to_terms);
							$('.agent-match-div #more #agree').prop('checked', seller.am_agree_to_terms);
							$('.agent-match-div #more #agree').on('click', function() {
								var val = $(this).prop('checked');
								seller.am_order.agreed_to_terms = seller.am_agree_to_terms = val ? 1 : 0;
								$('.define-agent-match-container #define-agent-match input[type="checkbox"]').prop('disabled', !val);
								$('.agent-match-div .purchase button').prop('disabled', !val);
								console.log("Agree to terms:"+val)
							})
						}
					})
				}
			}
			this.portal = new function() {
				this.showLogo = function() {
					if (typeof controller.setLogo == 'function')
						controller.setLogo(true);
					else
						window.setTimeout(function() {
							seller.page.portal.showLogo();
						}, 500);
				}

				this.get = function() {
					var page_contents_need_portal = 
									'<div class="need-portal">' +
										'<div id="portal-content">' +
												'<div class="top">' +
													'<div class="title">It&#39;s simple, just choose your Client Portal Name</div>' +
													'<div class="portalinput">' +
														'<div class="urlinput">www.lifestyledlistings.com/</div>' +
														'<div class="urlinputmobile">lifestyledlistings.com/</div>' +
														'<input autocomplete="off" autocorrect="off" spellcheck="false" id="portal" type="text" placeholder="Your portal name" />' +
														'<div class="check-name"><span id="marker" class="entypo-check"></span></div><button id="check-it">Check</button>' +
													'</div>' +
													'<div class="purchase">' +
														'<button id="checkout">Get it Now</button>' +
													'</div>'+
													'<div class="whatisportal">' +
														'<div class="leftdesc">' +
															'<iframe src="https://player.vimeo.com/video/141358615" style="padding-left: 12%;" width="600" height="334" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' +
														'</div>' +
														'<div class="rightdesc">' +
															'<span class="title"><span style="color:#F9A054">What</span> is the Client Portal?</span>' +
															'<p>Client Portal gives your clients a nation wide search that keeps you in touch with them, and their transaction. Your clients search with an agent they trust and you increase your outbound referral business. It&#39;s a win-win for you and your clients.  Know exactly what they are searching for in the Stats page.</p>' +
															'<p>And perhaps best of all - it&#39;s '+(isFree ? 'free!' : (discount > 0 ? "$"+discount+" off!" : "only $"+portalDefaultPrice+"/month"))+'</p>' +
														'</div>' +
													'</div>' +
												'</div>' +
												'<div class="mid">' +
													'<div class="leftdesc">' +
														'<span class="title"><span style="color:#F9A054">Why</span> get a Client Portal?</span>' +
														'<p>Because YOU are important to your clients no matter where they buy their next home.</p>' +
														'<p class="mobilespace">Client portal is about giving your clients the ability to have someone they trust at their fingertips. <span style="font-weight:400">Whether your clients are shopping for a vacation home, retiring, or just relocating, people who know and trust you can ask you questions or get a second opinion on things at anytime because you are with them the whole way.</span></p>' +
													'</div>' +
													'<div class="divider">' +
														'<div class="line1"></div>' +
														'<span>+</span>' +
														'<div class="line2"></div>' +
													'</div>' +
													'<div class="rightdesc">' +
														'<span class="title"><span style="color:#F9A054">What&#39;s</span> in it for you?</span>' +
														'<p>How about a 25% referral fee paid to you for sending your network through the portal? Or knowing exactly what your portal users were looking for? We will give you stats for every user.</p>' +
														'<p class="mobilespace" style="font-weight:400">You show up on every listing across the US and we track your clients whenever they go through your portal so we can give you credit when they contact another agent on our site. We start the referral process for you automatically, so you simply get an email notifying you that you just sent a referral.</span></p>' +
													'</div>' +
													'<div class="mobilefree">' +
														'<span class="title"><span style="color:#F9A054">Why</span> is it free?</span>' +
														'<p class="mobilespace">Because helping you helps us! The more traffic that you put through our site, the more success we will have!</p>' +
													'</div>' +
												'</div>' +
												'<div class="circle">' +
													'<span class="title">Why are we so different?</span>' +
													'<p>We&#39;ve found that most consumers don&#39;t think of their local agent when considering property out of the area, so we&#39;ve built a tool that makes sure you get in the loop.</p>' +
													'<p style="width: 60%; margin: 20px auto 0;">Then we get you paid a 25% referral fee for it.</p>' +
												'</div>' +
												'<div class="bottom">' +
													'<div class="sidetext">' +
														'<span class="title">'+(isFree ? 'Why is it free?' : 'Why is it so affordable?')+'</span>' +
														'<p>Because helping you helps us! When you bring your clients through our site, it helps all the agents involved!</p>' +
													'</div>' +
												'</div>' +
												'<div class="calltoaction">' +
													'<a href="#"><span class="entypo-up-open-mini"></span> Sound Good? Sign up now! <span class="entypo-up-open-mini"></span></a>' +
												'</div>' +
											// '<div id="define-portal">' +
											// 	'<span id="title3">Apply for our program and get your own PORTAL today.</span>' +
											// 	'<span id="message"></span>' +
											// 	'<span id="title4">Please choose a portal name</span>'+
											// 	'<span style="text-align:center;display:block;margin:.5em 0"><label id="base-url" style="letter-spacing:.05em;font-weight:400;font-size:1.1em;color:#333"></label><input id="portal" type="text" placeholder="Your name..." style="margin-left:2px;margin-right:10px" /><span id="marker" class="entypo-check"></span><button id="check-it">Check it</button></span>' +
											// '</div>'  +
											// '<div class="purchase" style="text-align:center">' +
											// 	'<button id="checkout">Get it Now</button>' +
											//'</div>'+
										'</div>' +
									'</div>';
					var page_contents_have_portal =
									'<div class="have-portal">' +
										'<div id="portal-content">' +
												'<div class="header">' +
													'<div class="title"><img src="'+ah_local.tp+"/_img/_sellers/tools-icon.png"+'" /><span>Tools</span></div>' +
													'<div class="portal-name">www.lifestyledlistings.com/<span style="text-transform: capitalize;">'+nickname+'</span></div>'+
													//'<div class="purchase" style="text-align:center">' +
													//	'<span>Your free portal is about to expire!</span>&nbsp;&nbsp;' +
													//	'<button id="checkout">Buy it Now</button>' +
													//'</div>'+
												'</div>' +
												'<div class="portal-explain">' +
                                                    '<div class="mobile-area-left">' +
														'<img src="'+ah_local.tp+"/_img/_sellers/portal-explain-mobile.png"+'" />' +
													'</div>' +
													'<div class="area-left">' +
														'<img src="'+ah_local.tp+"/_img/_sellers/portal-explain"+(isRetina ? '-retina' : '')+".jpg"+'" />' +
													'</div>' +
													'<div class="area-right">'+
														'<span class="title">As a premium agent you have your very own Lifestyled Listings URL and custom landing page</span>' +
														'<span class="subtitle">Use these tools to customize your landing page, drive your network of contacts to it. Contacts that sign in and start searching are likely to list their home with you locally and be a referral out to our Lifestyled Agent network. Setup is easy! Watch this tutorial video to learn how.</span>' +
														'<button class="video" id="tools-overview"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>';
														if (doLeadCaptureSetup)
														page_contents_have_portal += '<button class="setup-portal-capture"><p>Setup </p><span class="entypo-right-dir"></span></button>';
						page_contents_have_portal +='</div>' +
												'</div>' +
												'<div id="portal-message">' +
													'<div class="area-left">' +
														'<span class="title">Customize your portal</span>' +
														'<span class="subtitle">Write a message to your clients to tell them why they should sign in and start a search with you</span>' +
														'<textarea id="portal-message" placeholder="Enter personal message..."></textarea>' +
														'<button id="portal-message-done" class="edited" disabled>Save</button>';
					                            if (agentPageOption.showFindCompanyButton) {
					page_contents_have_portal +='<div class="company">'+
					                                '<span id="companyMessage">Select a company to be assocated with: </span>'+
					                                '<button id="selectCompany">Find One</button>'+
					                                '<div id="current" class="hidden">'+
					                                  '<span id="currentMessage">Current One:</span>'+
					                                  '<img id="logo" src="" />'+
					                                '</div>'+
					                            '</div>';
					                            }
					page_contents_have_portal +='</div>' +
													'<div class="area-right">'+
														'<img src="'+ah_local.tp+"/_img/_sellers/portal-message"+(isRetina ? '-retina' : '')+(isMobile ? '-mobile' : '')+".jpg"+'" />' +
													'</div>' +
												'</div>' +
												'<div class="middle">' +
													'<div class="email-banners">' +
														'<div class="title">Add an Email Banner<span class="desktop-video-title"> <button class="video" id="tools-banners-desktop"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button></span></div>'+
														'<div class="subtitle">Every email you send has the potential to bring you new business. Adding a linked email banner to your signature can bring in an average of 10 visitors monthly. <a class="help" href="javascript:;">Not working?</a></div>'+
                                                        '<div class="mobile-banners">'+ 
                                                          '<div class="mobile-unavailable">Setup of this feature is currently unavailable for your mobile device. Supported devices include iPhones, iPads, and all desktop/laptop computers.</div>'+
                                                          '<div class="mobile-text">How do I add my signature?</div>'+
                                                          '<div class="how-to-instructions" id="iphone">'+
                                                            '<button class="video" id="iphone"><p>How to video </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>'+
                                                            '<div class="text-instructions">'+
                                                              '<div class="steps"><span>Step 1.</span><p>Select your email banner from the list below and copy the content.</p></div>'+
                                                              '<div class="steps"><span>Step 2.</span><p>Open your Settings app.</p></div>'+
                                                              '<div class="steps"><span>Step 3.</span><p>Navigate to Mail, Contacts, Calendar.</p></div>'+
                                                              '<div class="steps"><span>Step 4.</span><p>Navigate to Signature.</p></div>'+
                                                              '<div class="steps"><span>Step 5.</span><p>Paste your banner in the signature.</p></div>'+
                                                            '</div>'+
                                                          '</div>' +
                                                        '</div>'+
														'<span class="area-left">'+
															'<div class="white-overlay"></div>'+
															'<img src="'+ah_local.tp+"/_img/_sellers/email-banner-example"+(isRetina ? '-retina' : '')+".jpg"+'" />'+
														'</span>'+
														'<div class="portal-modal">'+
															'<p>Click on the email banner you want to copy, then simply paste it at the bottom of your email signature and anyone who clicks it will be sent directly to your client portal!</p>'+
															'<button class="close">Got it!</button>'+
														'</div>'+
														'<div class="clipboard-manual" style="display:none;">'+
															'<span>Your browser prevents copying the images directly into your clipboard.</span>'+
															'<div id="data-clipboard"></div>'+
															'<span id="instruction"></span>'+
															'<button data-clipboard-action="copy" data-clipboard-target="#data-clipboard-input" class="clip_button btn">Copy URL to clipboard</button>'+
															'<button class="email_banner_button">Email banner to yourself</button>'+
															'<button class="close_button">Close</button>'+
														'</div>'+
														'<div class="overlay" style="display: none;"></div>'+
														'<span class="area-right">'+
															'<ul id="banner-list"></ul>'+
														'</span>'+
													'</div>'+
													'<div id="shareDiv">' +
														'<span class="title"><span class="regular">Quick s</span><span class="tiny-mobile">S</span>hare on social media<span class="desktop-video-title"> <button class="video" id="tools-social-desktop"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button></span></span>' +
														'<span class="subtitle">Share your client portal with your social network instantly</span>' +
														'<a href="javascript:seller.shareSocialNetwork('+"'FB'"+');" ><img style="width:68px; height:68px; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/facebook.png'+'" /></a>' +
														'<a href="javascript:seller.shareLinkedIn();"><img style="width:68px; height:68px; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/linkedin.png'+'" /></a>'+
														'<a href="javascript:seller.shareSocialNetwork('+"'GP'"+');" ><img style="width:68px; height:68px; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/googleplus.png'+'" /></a>' +
														'<a href="javascript:seller.shareSocialNetwork('+"'TW'"+');" ><img style="width:68px; height:68px; margin-top:10px; vertical-align:sub" src="'+ah_local.tp+'/_img/_promo/twitter.png'+'" /></a>' +
													'</div>'+
													'<div class="featured">' +
														'<span class="title">Featured Content</span>' +
														'<span class="subtitle">You will now be emailed frequently with new content to offer to your network. Those emails will automatically have every mention of your "lifestyle search tool" linked to your client portal, so your leads will be tracked for you.</span>' +
														'<span class="subtitle below"><strong>Coming Soon,</strong> the content will be available to download here.</span>' +
													'</div>'+
													'<div class="bottom">' +
														'<div class="title">Settings</div>' +
														'<div class="wrapper">' +
															'<input type="checkbox" name="allow" id="allow" '+(validSeller.flags & SellerFlags.SELLER_INACTIVE_EXCLUSION_AREA ? 'checked' : '')+' />' +
															'<label id="allowLabel" style="font-size: 1.2em;font-weight: 400;color: #444;"> Show any LifeStyled Agents within my service area</label>'+
															'<span id="allowMsg"></span>'+
														'</div>';
							page_contents_have_portal +='<div class="addemail"><a href="javascript:seller.testEmail();">Click here</a> to have an e-mail sent to you as a test.</div><br/>' +
													'</div>' +
												'</div>' +
											'</div>' +
										'</div>' +
										//seller.miniPortalAgent() +
									'</div>';

					var contents = (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING ? page_contents_have_portal : page_contents_need_portal);
					seller.content({
						title: 'Portal',
						content: contents,
						callback: function() {
							var msg = 	'Lifestyledlistings.com wants you to <strong>capture leads and receive referral fees</strong> by using the PORTAL feature of our website.  ' +
										'This user friendly tool allows you to <strong>attract and manage interested buyers</strong>.  '+
										'Once a buyer enters the site through your PORTAL <strong>they will remain tied to you*.' +
										'</strong>  The benefit for you is you get a highly developed, lifestyle site that captures leads whom will remain with you <strong>' +
										'whether you refer them out or close an escrow.</strong><br/><br/> Share your link through your social medias, email contacts, advertisements, '+
										'add it to your business cards and email signatures. Spread the word and <strong>keep your business growing faster than ever!</strong>  ' +
										'Call us, and our specialists can help you work your social media sites to show your portal URL to your clients!' +
										'<span style="font-size:.9em;width:90%;text-align:center;display:block;margin-top:35px">*Disclaimer: Lifestyled Listings is legally required ' +
										'to give users the ability to detach from your PORTAL if they choose to.</span>';
							var msg2 =  'This will allow our Lifestyled Agents to appear in searches in your service area when your clients are logged in through your client portal. We recommend you leave it off to have a higher chance of getting buyer leads in your area';
		
							// get the order data
							var metas = validSeller.meta;
							for(var i in metas) {
								if (metas[i].action == SellerMetaFlags.SELLER_AGENT_ORDER &&
									seller.am_order == null) {
									seller.am_order = JSON.parse(JSON.stringify(metas[i])); // makes copy of it
								}
								else if (metas[i].action == SellerMetaFlags.SELLER_AGENT_MATCH_DATA &&
										seller.am_meta == null) {
									seller.am_meta = JSON.parse(JSON.stringify(metas[i])); // makes copy of it
								}	
							}

							var options = '';
							for(var i = 1; i <= EMAIL_BANNER_COUNT; i++) {
								options += 	'<li><a href="javascript:seller.copyToClipboard('+i+')" which="'+i+'">'+
													'<img src="'+ah_local.tp+'/_img/_banners/_email/banners-v2/new-banners-'+i+'.png" />' +
												'</a>'+
												// '<button id="copyIt" which="'+i+'">Copy to clipboard</button>'+
											'</li>';
							}

							$('#banner-list').html(options);
							$('div.clipboard-manual .email_banner_button').on('click', function() {
								// ZeroClipboard.emit('copy');
								seller.sendBannerToAgent();
							})
							
							$('div.clipboard-manual .close_button').on('click', function() {
								$('div.clipboard-manual').removeClass('active');
							})
							
							$('.portal-explain .video').on('click', function() {
								var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-tools']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								$('.videoDiv .video').html(h);
								videoControl.showVid();
							})
							
							$('.middle .video#tools-banners-desktop').on('click', function() {
								var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-tools-banners-desktop']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								$('.videoDiv .video').html(h);
								videoControl.showVid();
							})
							
							$('.middle .video#tools-social-desktop').on('click', function() {
								var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-tools-social-desktop']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								$('.videoDiv .video').html(h);
								videoControl.showVid();
							})
                            
							$('.email-banners .how-to-instructions#iphone .video').on('click', function() {
								var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-tools-iphone']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								$('.videoDiv .video').html(h);
								videoControl.showVid();
							})
							
							$('#marker').css('color', '#71E171');
							$('#marker').hide();
							
							$('#seller-admin .have-portal .email-banners .subtitle a.help').on('click', function() {
								 ahtb.alert("<span>Please check to make sure your browser is up to date. If you are still experiencing problems, please <a href='"+ah_local.wp+"/contact' target='_blank' style='font-weight:400;'>contact us</a>!</span>", {height: 195, width: 450})
							});
							
							$('.portalinput .urlinput').on('click', function() {
								event.preventDefault(); 
								$('.portalinput #portal').focus(); 
							});
							$('.portalinput #portal').keypress(function(e){
								if(e.keyCode==13)
								$('.portalinput #check-it').click();
							});

							$('button.setup-portal-capture').on('click', function() {
								$('div.leadCaptureDivs').show();
								controller.startLeadCaptureSlides();
							});

							$('button#selectCompany').on('click', function() {
								$('div.companiesList').show();
								// controller.startLeadCaptureSlides();
							});
							
							var viewed = getCookie("portalModalViewed");
							if (viewed.length == 0|| viewed != 'true') {
								function delaymodal() {
									$('#seller-admin .have-portal .email-banners .portal-modal').addClass( 'popout' );
									$('#seller-admin .have-portal .email-banners .white-overlay').addClass( 'shown' );
								}
								timeoutID = window.setTimeout(delaymodal, 1000);
							}

							$('#seller-admin .have-portal .email-banners .portal-modal button.close').on('click', function() {
								$('#seller-admin .have-portal .email-banners .portal-modal').removeClass( 'popout' );
								$('#seller-admin .have-portal .email-banners .white-overlay').removeClass( 'shown' );
								setCookie("portalModalViewed", "true", 180);
							});
                            
                            function runIOS() {
                              $('#seller-admin .page-portal .have-portal .email-banners .mobile-banners .mobile-text').show();
                              $('#seller-admin .page-portal .have-portal .email-banners .mobile-banners .how-to-instructions#iphone').show();
                              $('#seller-admin section.content .page-portal .have-portal .middle .email-banners .area-right').show();
                            }
                          
                            function runAndroid() {
                              $('#seller-admin .page-portal .have-portal .email-banners .mobile-banners .mobile-unavailable').show();
                            }
                          
                            function runWindowsPhone() {
                              $('#seller-admin .page-portal .have-portal .email-banners .mobile-banners .mobile-unavailable').show();
                            }
                          
                            function mobileUnrecognized() {
                              $('#seller-admin .page-portal .have-portal .email-banners .mobile-banners .mobile-unavailable').show();
                            }
                          
                            function getMobileOperatingSystem() {
                              var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                              if (/windows phone/i.test(userAgent)) {
                                runWindowsPhone();
                              }
                              if (/android/i.test(userAgent)) {
                                runAndroid();
                              }
                              if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                                runIOS();
                              }
                              else
                                mobileUnrecognized();
                            }
                            getMobileOperatingSystem();
							
							var metas = validSeller.meta;
							var portalMsgMeta = null;
							for(var i in metas) 
								if (metas[i].action == SellerMetaFlags.SELLER_PORTAL_MESSAGE) {
									portalMsgMeta = metas[i];
									break;
								}
							var textArea = $('div#portal-message .area-left textarea');
							if (portalMsgMeta)
								textArea.val(portalMsgMeta.message.replace(/\\/g,""));
							$('div#portal-message .area-left textarea').addClass( 'active' );
							
							$('div#portal-message .area-left textarea').keyup(function(){
								$('div#portal-message .area-left button.edited').prop('disabled', false);
							});
							
							$('div#portal-message .area-left button.edited').on('click', function() {
								var textArea = $('div#portal-message .area-left textarea');
								var msg = textArea.val();
								var metas = validSeller.meta;
								var portalMsgMeta = null;
								var savedMetas = [];
								for(var i in metas) 
									if (metas[i].action == SellerMetaFlags.SELLER_PORTAL_MESSAGE) {
										portalMsgMeta = metas[i];
									}
									else
										savedMetas[savedMetas.length] = metas[i];

								if (msg.length &&
									(!portalMsgMeta ||
									 portalMsgMeta.message != msg) ) {
									// save it!
									$(this).prop('disabled', true);
									console.log("New portal message:"+msg);
									if (!portalMsgMeta)
										portalMsgMeta = {action: SellerMetaFlags.SELLER_PORTAL_MESSAGE,
														 message: msg};				 
									else
										portalMsgMeta.message = msg;
									savedMetas[savedMetas.length] = portalMsgMeta
									var data = {meta: savedMetas};
									var sans = {data: data};
									seller.page.profile.saveToDb(sans, UpdateAuthorOrigin.PortalMessage);

								}
							});

							if (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) {
								//var height = '1480px';
								//$('.page-portal').css('height', height);
								//$('.have-portal').css('height', height);
								//$('#page-content').css('height', height);
								//$('#portal-content').css('height', height);
								if (havePortal == PortalState.PORTAL_EXPIRING) {
									$('#title3').css('margin', '0');
									$('.purchase').css('margin-bottom', '20px');
								}
							}
							else {
								$('#portal').val(reservedPortalName);
								if (reservedPortalName.length)
									$('#marker').show();
							}

							$('#message').html(msg);
							$('#allowMsg').html(msg2);
							$('.purchase').append(ecommercePortalAgent);
							$('.purchase p').hide();
							$('.purchase .amount').hide();
							$('.purchase a.add_to_cart_button').hide();
							$('.purchase a').html("Go to cart!");
							$('a#stats-anchor').html(seller.portalVisitCount.toString());
							// seller.updateVisitationsPortal();

							if (havePortal == PortalState.PORTAL_VALID)
								$('.purchase').hide();

							// $('button#copyIt').on('click', function() {
							// 	var which = $(this).attr('which');
							// 	seller.copyToClipboard(which);
							// })

							$('.bottom #allow').on('click', function() {
								var val = (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) ? nickname : $('#portal').val();
								var checked = $(this).prop('checked');
								validSeller.flags = checked ? validSeller.flags | SellerFlags.SELLER_INACTIVE_EXCLUSION_AREA : validSeller.flags & ~SellerFlags.SELLER_INACTIVE_EXCLUSION_AREA;
								seller.page.profile.info.flags = validSeller.flags;
								console.log("AM allow is now "+checked+", portal:"+val+", flags:"+validSeller.flags);
								var data = {
									permission: checked,
									id: validSeller.id
								}
								seller.DBget({
									query: 'update-exclusion-permission',
									data: data,
									done: function(d) {
										// ahtb.alert("Congratulations, this portal name is available!", {height: 150, width: 450})
									},
									error: function(d) {
										ahtb.alert("Could not update the exlusion permission flag "+(typeof d == 'string' ? '- '+d : ''), {height: 180, width: 450});
									}
								});
							})
							
							//$('section.ecommerce').removeClass('hidden');
							$('#base-url').html('LifestyledListings.com/');
							$('#portal').keyup(function(e){
								e.preventDefault();
								$('#marker').hide();
							})
							$('#check-it').on('click', function() {
								var val = (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) ? nickname : $('#portal').val();
								if (val.length == 0) {
									ahtb.alert("Please enter a portal name.", {height: 150});
									return;
								}
								seller.DBget({
									query: 'check-portal-name',
									data: val,
									done: function(d) {
						    			$('#marker').show();
						    			if (d.indexOf('belongs to current') == -1)
						    				$('#portal').val(d);
						    			else
						    				ahtb.alert("You already own this portal:"+val);
										// ahtb.alert("Congratulations, this portal name is available!", {height: 150, width: 450})
									},
									error: function(d) {
										ahtb.alert("The portal name "+val+" is already in use.</br>Please try a different name.", {height: 180, width: 450});
										$('#portal').val('');
										$('#marker').hide();
									}
								});
							});
							$('#checkout').on('click', function(){
								var val = (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) ? nickname : $('#portal').val();
								if (val.length == 0) {
									ahtb.alert("Please enter a portal name and check it.", {height: 150});
									return;
								}
								if (!(havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) &&
									typeof validSeller == 'object' &&
									validSeller != null &&
									validSeller.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_1 | SellerFlags.SELLER_IS_LIFETIME)) {
									console.log("Allow Portal");
									if (seller.page.portal.fixAmOrder())
										seller.getPortal(val);
								}
								else {
									var h = "<span>Do you have an invitation code or coupon?</span>" +
											'<input type=text id="invite" placeholder="Enter code" style="margin-bottom:15px;"></input>';
									ahtb.open({
										title: "Portal Checkout",
										html: h,
										height: 150,
										opened: function() {
											if (typeof coupon != 'undefined' &&
												coupon.length)
												$('input#invite').val(coupon);
										},
										buttons: [
											{text: "Apply", action: function() {
												if ($('#invite').val() != '') {
													if (seller.page.portal.fixAmOrder())
														seller.getPortal(val, $('#invite').val());
													return;
												}

												ahtb.push();
												ahtb.alert("Please enter a code.", 
															{height: 150,
															 buttons: [{text:"OK", action: function() { ahtb.pop(); }}]});

											}},
											{text: "Purchase", action: function() {
												if (eCommerceReady) {
													// get latest
													var metas = validSeller.meta;
													if (seller.am_order == null) for(var i in metas) {
														if (metas[i].action == SellerMetaFlags.SELLER_AGENT_ORDER) {
															seller.am_order = JSON.parse(JSON.stringify(metas[i])); // makes copy of it
														}
													}
													var newItem = {
														mode: AgentOrderMode.ORDER_BUYING,
														type: AgentOrderMode.ORDER_PORTAL,
														nickname: val,
														cart_item_key: 0,
														order_id: 0,	
														priceLevel: seller.portalSpec.priceLevel,
														subscriptionType: seller.portalSpec.subscriptionType													
													}
													if (typeof seller.am_order != 'undefined' &&
														seller.am_order) {
														var needOne = true;
														for(var i in seller.am_order.item) {
															if (seller.am_order.item[i].type == AgentOrderMode.ORDER_PORTAL) {
																if (seller.am_order.item[i].mode == AgentOrderMode.ORDER_BOUGHT) {
																	ahtb.alert("It appears the Portal product has already been purchased.<br/>Please inform the site administrator to check the status of order:"+seller.am_order.item[i].order_id, {height: 180, width: 500});
																	return;
																}
																seller.am_order.item[i].mode = AgentOrderMode.ORDER_BUYING;
																seller.am_order.item[i].cart_item_key = 0;
																seller.am_order.item[i].priceLevel = seller.portalSpec.priceLevel;
																seller.am_order.item[i].subscriptionType = seller.portalSpec.subscriptionType;
																seller.am_order.item[i].nickname = val;
																needOne = false;
															}
														}
														if (needOne) {
															id = seller.am_order.item.length;
															seller.am_order.item[seller.am_order.item.length] = newItem;
														}
													}
													else {
														seller.am_order = {
															action: SellerMetaFlags.SELLER_AGENT_ORDER,
															order_id_last_purchased: 0,
															order_id_last_cancelled: 0,
															agreed_to_terms: 0,
															item: [newItem]
														}
													}
													seller.processPurchase(AgentOrderMode.ORDER_PORTAL);	
													// $('#checkout').hide();
													// $('.purchase p').show();
													// $('.purchase a.add_to_cart_button').show();
													// $('.purchase a.add_to_cart_button').click();
													// $('.purchase a.add_to_cart_button').off().on('click', function() {
													// 	window.location = ah_local.wp+"/cart";
													// })
													// $('.purchase a.add_to_cart_button').click();
													// window.location = ah_local.wp+"/product/portal-agent";
												}
												else {
													var h = '<div id="noEcommerce">' +
																"eCommerce is not ready yet.</br>" +
																"Would you like to reserve Portal Agent?</br></br>" +
																"*Reserving your spot does not guarantee you will get it.&nbsp;On a first come basis, we will contact the agent if they still want it, and if that agent happened to use the same portal entry as yours, you will lose this portal entry."+
															'</div>'
													ahtb.open(
														{html: h,
														 height: 280, 
														 width: 450,
														 buttons: [
														 {text:"Yes", action: function() {
														 	seller.DBget({
																query: 'reserve-agent-product',
																data: {id: validSeller.id,
																	   what: SellerFlags.SELLER_IS_PREMIUM_LEVEL_1,
																	   portal: val },
																done: function(d) {
																	console.log("reserve-agent-product - "+d);
																	ahtb.alert("Your portal, "+val+", is now reserved.  Please expect a call from us soon.  If you don't hear from us within 48hr or less, please contact us via our Feedback!",
																			{height: 190,
																			 width: 450});								
													    		},
																error: function(d) {
																	console.log("We're very sorry, had trouble reserving your portal. "+d);
																	ahtb.alert("We're very sorry, had trouble reserving your portal. "+d, {height: 150, width: 450});
																}
															});
														 }},
														 {text:"Not Yet", action: function() { ahtb.close(); }}] 
													});
												}
											}}]
									})								
									// ahtb.alert("Sorry, feature not permitted.", {height: 150});
								}
							});

							$('.premier-agent .contact-button').on('click', function() {
						        var agentId = validSeller.id;
						        console.log("Contact button for ", agentId);
						        var mode = FeedbackMode.MESSAGE_EMAIL_AGENT;
						        ahfeedback.openModal(mode, agentId, 0, 0, 0, validSeller.author_id);
						    })
							$('.have-portal .bottom-right #instructionDiv a.more-info').on('click', function() {
								$('.have-portal .bottom-right #instructionDiv a.more-info').hide();
								$('.have-portal .bottom-right #instructionDiv a.less-info').show();
								$('.have-portal .bottom-right #instructionDiv .expanded-instructions').slideToggle('fast');
							})
							$('.have-portal .bottom-right #instructionDiv a.less-info').on('click', function() {
								$('.have-portal .bottom-right #instructionDiv a.less-info').hide();
								$('.have-portal .bottom-right #instructionDiv a.more-info').show();
								$('.have-portal .bottom-right #instructionDiv .expanded-instructions').slideToggle('fast');
							})
							$('.page-portal .have-portal .middle #stats .extra-info a.back-to-profile').on('click', function() {
								sellerTransitionStart();
							});
						}
					})
				}
				this.fixAmOrder = function() {
					var val = (havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING) ? nickname : $('#portal').val();
					if (val.length == 0)
						return false;

					var newItem = {
						mode: AgentOrderMode.ORDER_BUYING,
						type: AgentOrderMode.ORDER_PORTAL,
						nickname: val,
						cart_item_key: 0,
						order_id: 0,	
						priceLevel: seller.portalSpec.priceLevel,
						subscriptionType: seller.portalSpec.subscriptionType													
					}
					if (typeof seller.am_order != 'undefined' &&
						seller.am_order != null) {
						var needOne = true;
						for(var i in seller.am_order.item) {
							if (seller.am_order.item[i].type == AgentOrderMode.ORDER_PORTAL) {
								if (seller.am_order.item[i].mode == AgentOrderMode.ORDER_BOUGHT) {
									ahtb.alert("It appears the Portal product has already been purchased.<br/>Please inform the site administrator to check the status of order:"+seller.am_order.item[i].order_id, {height: 180, width: 500});
									return false;
								}
								seller.am_order.item[i].mode = AgentOrderMode.ORDER_BUYING;
								seller.am_order.item[i].cart_item_key = 0;
								seller.am_order.item[i].priceLevel = seller.portalSpec.priceLevel;
								seller.am_order.item[i].subscriptionType = seller.portalSpec.subscriptionType;
								seller.am_order.item[i].nickname = val;
								needOne = false;
							}
						}
						if (needOne) {
							id = seller.am_order.item.length;
							seller.am_order.item[seller.am_order.item.length] = newItem;
						}
					}
					else {
						seller.am_order = {
							action: SellerMetaFlags.SELLER_AGENT_ORDER,
							order_id_last_purchased: 0,
							order_id_last_cancelled: 0,
							agreed_to_terms: 0,
							item: [newItem]
						}
					}
					return true;
				}
			}

			this.stats = new function() {
				this.convertDate = function(date) {
					// strip off any time element from the date
					var dateTime = date.split(' ');
					date = dateTime[0];
					var dates = date.split('-');
					if (dates[0].length == 4)
						return dates[1]+'/'+dates[2]+'/'+dates[0];
					else
						return date;
				}

				this.hideRow = function(pid) {
					var ele = $('li#user[for="'+pid+'"]');
					console.log("hideRow for "+pid+", ele.length:"+ele.length);
					$('li#user[for="'+pid+'"]').fadeOut(500);
					$('a#hideAll').fadeIn(250);
					seller.haveHidden = true;
				}

				this.unhideAll = function() {
					for(var i in portalUsersList) {
						var pid = parseInt(portalUsersList[i].id)+portalUsersOffset;
						if (portalUsersList[i].flags & PortalUserFlags.PORTAL_USER_HIDDEN) {
							seller.page.stats.updatePortalUserFlagsUnset(PortalUserFlags.PORTAL_USER_HIDDEN,portalUsersList[i].id);
							$('li#user[for="'+pid+'"]').fadeIn(200);
						}
					}
					$('a#hideAll').fadeOut(250);
					seller.haveHidden = false;
				}

				this.portalStats = function(forPrinter) {
					function getUserEntryCount(id) {
						if (!seller.visitations ||
							length(seller.visitations.userActivity) == 0)
							return 0;

						for(var i in seller.visitations.userActivity)
							if (seller.visitations.userActivity[i].id == id)
								return seller.visitations.userActivity[i].entries;

						return 0;
					}

					var forPrinter = typeof forPrinter == 'undefined' ? false : forPrinter;
					var showPhone = agentPageOption.displayStatsPagePhoneNumbers;
					seller.haveHidden = false;
					var h =	'<section class="stats" id="portal-visitors">' +
								'<div id="column-header">'+
									'<span id="title" class="first">Visits</span>'+
									'<span id="title" class="second">Name</span>'+
									'<span id="title" class="third desktop">'+(showPhone ? 'Contact' : 'Email')+'</span>';
								if (forPrinter)
								h+=	'<span id="title" class="fourth combined desktop">Location / Date</span>';
								// else if ( seller.showSiteEntryStatsInActivityPage) 
								// h+=	'<span id="title" class="fourth desktop">Location</span>'+
								// 	'<span id="title" class="fifth desktop">Date</span>'+
								// 	'<span id="title" class="sixth desktop">Activity</span>';
								else
								h+=	'<span id="title" class="fourth combined desktop">Location / Date</span>'+
									'<span id="title" class="fifth combined desktop">Activity</span>';
							h+=	'</div>'+
								'<div id="list-container">'+
									'<ul id="users">';
									if (length(portalUsersList)) {
										for(var i in portalUsersList) {
											if (portalUsersList[i].flags & PortalUserFlags.PORTAL_USER_HIDDEN)
												seller.haveHidden = true;
											var pid = parseInt(portalUsersList[i].id)+portalUsersOffset;
										h+= '<li id="user" for="'+pid+'" '+(portalUsersList[i].flags & PortalUserFlags.PORTAL_USER_VIEWED ? 'class="viewed"' : '')+(portalUsersList[i].flags & PortalUserFlags.PORTAL_USER_HIDDEN ? ' style="display: none;"' : '')+'>'+
												'<div class="container" id="portal-users">'+
													'<span id="entries" class="first">'+portalUsersList[i].activity.entries+'</span>'+
													'<div id="name" class="second">'+
														'<span id="name">'+visitorNames[pid]+'</span>';
														if (!forPrinter)
													h+=	'<a class="mobile" id="more" href="javascript:seller.page.stats.showUserDetailMobile('+pid+');">More<span class="entypo-down-open-big"/></a>'+
														'<a id="closeDetail" class="mobile" href="javascript:seller.page.stats.closeUserDetailMobile('+pid+');" style="display:none;">Close<span class="entypo-up-open-big"/></a>';
													h+=	'<div class="mobile details" for="'+pid+'" style="display:none;" >'+
															'<div class="main-details">'+
																'<a id="email" class="mobile" href="javascript:seller.emailUser('+pid+');">'+(portalUsersList[i].email && portalUsersList[i].email.length ? portalUsersList[i].email : 'Email Unknown')+'</a>';
																if (agentPageOption.displayStatsPagePhoneNumbers)
																	h+= '<span id="phone">'+(portalUsersList[i].phone && portalUsersList[i].phone != 0 ? portalUsersList[i].phone : 'Phone Unknown')+'</span>';
															// if ( seller.showSiteEntryStatsInActivityPage) {
															// h+=	'<span id="location" class="mobile">'+seller.visitationCities[ portalUsersList[i].ip ]+'</span>'+
															// 	'<span id="date" class="mobile">'+seller.page.stats.convertDate(portalUsersList[i].activityKeyOrder[0])+'</span>';
															// }
															// else {
															h+= seller.page.stats.getUserIPandDatePortalUser(portalUsersList[i].id,'', 'combined mobile');
															// }
														if (!forPrinter) {
															if ( seller.page.stats.portalUserHasActivity(portalUsersList[i].id) ||
													 				seller.showSiteEntryStatsInActivityPage)
															h+=	'<a id="activity" class="mobile" href="javascript:seller.page.stats.goToStatsPage('+ActiveStatPage.ActivityStats+','+portalUsersList[i].id+');">View Activity</a>';
															else
																h+=	'<span id="activity" class="mobile">No Activity</span>';
															// h+= '<a id="hide-user" class="mobile" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.goToStatsPage('+ActiveStatPage.PortalStats+');">Hide user</a>';
															h+= '<a id="hide-user" class="mobile" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.hideRow('+pid+');">Hide user</a>';
														}
															h+=	'</div><div class="extra-bg"></div>'+
														'</div>'+
													'</div>'+
													'<a id="email" class="third desktop" href="javascript:seller.emailUser('+pid+');">';
															if (agentPageOption.displayStatsPagePhoneNumbers) {
																h+= '<span id="agent-email">'+(portalUsersList[i].email && portalUsersList[i].email.length ? portalUsersList[i].email : 'Email Unknown')+'</span>';
																h+= '<span id="phone">'+(portalUsersList[i].phone && portalUsersList[i].phone != 0 ? portalUsersList[i].phone : 'Phone Unknown')+'</span>';
															}
															else
																h+= '<span id="agent-email" class="solo">'+(portalUsersList[i].email && portalUsersList[i].email.length ? portalUsersList[i].email : 'Email Unknown')+'</span>';
													h+= '</a>';
												
												// if ( seller.showSiteEntryStatsInActivityPage) {
												// h+=	'<span id="location" class="fourth desktop">'+seller.visitationCities[ portalUsersList[i].ip ]+'</span>'+
												// 	'<span id="date" class="fifth desktop">'+seller.page.stats.convertDate(portalUsersList[i].activityKeyOrder[0])+'</span>';
												// }
												// else {
												h+= seller.page.stats.getUserIPandDatePortalUser(portalUsersList[i].id,'fourth', 'combined desktop');
												// }
											if (!forPrinter) {
												if ( seller.page.stats.portalUserHasActivity(portalUsersList[i].id) ||
													 seller.showSiteEntryStatsInActivityPage)
												h+= '<div class="'+(seller.showSiteEntryStatsInActivityPage ? 'sixth desktop' : 'fifth combined desktop')+'" >'+
														'<span id="activity" >' +
															'<a id="activity" href="javascript:seller.page.stats.goToStatsPage('+ActiveStatPage.ActivityStats+','+portalUsersList[i].id+');">View</a><span class="default-cursor"> / </span>' +
															// '<a id="activity" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.goToStatsPage('+ActiveStatPage.PortalStats+');">Hide User</a>' +
															'<a id="activity" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.hideRow('+pid+');">Hide user</a>' +
														'</span>' +
													'</div>';
												else if ( !seller.showSiteEntryStatsInActivityPage )
												h+= '<div class="fifth combined desktop">'+
												 		// '<span id="activity" >None/<a id="activity" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.goToStatsPage('+ActiveStatPage.PortalStats+');">Hide User</a></span>'+ // place holder
												 		'<span id="activity" >None<span class="default-cursor"> / </span><a id="activity" href="javascript:seller.page.stats.updatePortalUserFlags('+PortalUserFlags.PORTAL_USER_HIDDEN+','+portalUsersList[i].id+'), seller.page.stats.hideRow('+pid+');">Hide user</a></span>'+ // place holder
													'</div>';
											}
											h+=	'</div>'+
											'</li>';
										}
									}

									// do oldstyle users without any activity tracking...
									if (seller.visitations &&
										length(seller.visitations.users)) {
										for(var id in seller.visitations.users) {
										h+=	'<li id="user" for="'+id+'">'+
												'<div class="container" id="oldtime-users">'+
													'<span id="entries" class="first">'+getUserEntryCount(id)+'</span>'+
													'<div id="name"  class="second">'+
														'<span id="name">'+visitorNames[id]+'</span>';
													if (!forPrinter)
													h+=	'<a class="mobile" id="more" href="javascript:seller.page.stats.showUserDetailMobile('+id+');">More<span class="entypo-down-open-big"/></a>'+
														'<a id="closeDetail" class="mobile" href="javascript:seller.page.stats.closeUserDetailMobile('+id+');" style="display:none;">Close<span class="entypo-up-open-big"/></a>';
													h+=	'<div class="mobile details" for="'+id+'" style="display:none;" >'+
															'<div class="main-details">'+
																'<a id="email" class="mobile" href="javascript:seller.emailUser('+id+');">'+visitorEmails[id]+'</a>';
															if (agentPageOption.displayStatsPagePhoneNumbers)
																h +='<span id="phone" style="display:block;margin-top:-1.4em;">'+visitorPhones[id]+'</span>';
															if ( seller.showSiteEntryStatsInActivityPage) {
																h+= seller.page.stats.getUserIP(id, '', 'mobile');
																h+= '<div id="date">';
																h+= seller.page.stats.getUserDate(id, Object.keys(seller.visitations.users[id])[0], 'mobile');
																h+= '</div>';
															}
															else {
																h+= seller.page.stats.getUserIPandDate(id, '', 'combined mobile');
															}
													h+= '</div><div class="extra-bg"></div>'+
														'</div>'+
													'</div>'+
													'<a id="email" class="third desktop" href="javascript:seller.emailUser('+pid+');">';
															if (agentPageOption.displayStatsPagePhoneNumbers) {
																h +='<span id="agent-email">'+visitorEmails[id]+'</span>';
																h +='<span id="phone">'+visitorPhones[id]+'</span>';
															}
															else
																h +='<span id="agent-email" class="solo">'+visitorEmails[id]+'</span>';
													h+= '</a>';
													if ( seller.showSiteEntryStatsInActivityPage) {
														h+= seller.page.stats.getUserIP(id, 'fourth', 'desktop');
														h+= '<div id="date" class="fifth">';
														h+= seller.page.stats.getUserDate(id, Object.keys(seller.visitations.users[id])[0], 'desktop');
														h+= '</div>';
													if (!forPrinter)
														h+= '<span id="activity" class="sixth desktop">None</span>'; // place holder
													}
													else {
														h+= seller.page.stats.getUserIPandDate(id, 'fourth', 'combined desktop');
													if (!forPrinter)
														h+= '<span id="activity" class="fifth combined desktop">None</span>'; // place holder
													}
											h+=	'</div>'+
											'</li>';
										}
									} // end if (length(seller.visitations.users)) 

									if (length(portalUsersList) == 0 &&
										(!seller.visitations ||
										 length(seller.visitations.users) == 0) )
										h += '<li><a href="javascript:seller.getPage('+"'portal'"+');" >No Activity - go to Tools page</a></li>';

								h+= '</ul>'+
								'</div>'+
							'</section>';

					return h;
				}

				this.listingStats = function(forPrinter) {
					var d = seller.sellerData.listings; 
					if (!length(d)) {
						return 	'<section class="stats" id="listing-stats">' +
									//'<h1><a href="javascript:seller.encourageListingViewing();">Notice (click me)</a></h1>' +
									'<div id="stat-div-listing-visits">'+
										'<span class="no-listings">No listings currently under your name, click on <a href="'+ah_local.wp+'/new-listing">Add Listings</a> to make one now!</span>' +
									'</div>'+
								'</section>';
					}

					seller.buildVisitationStatsListings();

					var forPrinter = typeof forPrinter == 'undefined' ? false : forPrinter;
					var h =	'<section class="stats" id="listing-stats">' +
								'<div id="column-header">'+
									'<span id="title" class="first">Clicks</span>'+
									//'<span id="title" class="first">Impressions</span>'+
									'<span id="title" class="second desktop">Listing Address</span>'+
									'<span id="title" class="third desktop">Name</span>'+
									'<span id="title" class="fourth desktop">Location</span>'+
									'<span id="title" class="fifth desktop">Date</span>'+
									'<span id="title" class="sixth desktop">Search Criteria</span>'+
								'</div>'+
								'<div id="list-container">'+
									'<ul id="users">';
									// loop here for the listings in d
									for(var index in d) { // each listing is a row of table
										var listing = d[index].id;
										h+=	'<li id="user" for="'+d[index].id+'">'+
												'<div class="container" id="stat-div-listing-visits">'+
													'<span id="address-mobile" class="listing-mobile">'+seller.visitationsListings[listing].street_address+'</span>'+
													'<span id="clicks" class="first"><div class="listing-mobile title">Clicks</div>'+d[index].clicks+'</span>'+
													//'<span id="impressions" class="first"><div class="listing-mobile title">Impressions</div>'+d[index].impressions+'</span>'+
													'<span id="address" class="second listing-desktop">'+seller.visitationsListings[listing].street_address+'</span>';
													var usersCount = length(seller.visitationsListings[listing].users.sub);
											
													if (havePortal) {
														if ( !usersCount ) {
															h+= '<span id="users" class="third listing-desktop">Unknown</span>'+ // users
																'<span id="location" class="fourth listing-desktop">Unknown</span>'+ // place holder
																'<span id="dates" class="fifth listing-desktop">N/A</span>'+ // place holder
																'<span id="tags" class="sixth"><div class="listing-mobile title">Search Criteria</div>No Tags</span></li>'; // place holder, close off li
															continue;
														}
													h+= '<div id="users" class="third"><div class="listing-mobile title">User Name</div>'+seller.visitationsListings[listing].users.html+'</div>';
														var userId = seller.visitationsListings[listing].users.current; // this is the current one 
													h+= '<div class="listing-mobile title fourth">User Location</div><div id="location" class="fourth">'+seller.visitationsListings[listing].users.sub[userId].html+'</div>'; // location
														var ip = seller.visitationsListings[listing].users.sub[userId].current; // this is the current one 
													h+=	'<div class="listing-mobile title fifth">Date Visited</div><div id="dates" class="fifth">'+seller.visitationsListings[listing].users.sub[userId].sub[ip].html+'</div>'; // date
														var date = seller.visitationsListings[listing].users.sub[userId].sub[ip].current; // this is the current one 
														var tagCount = seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].tagCount;
														if (tagCount) {
															var html = seller.showStatTagImgs ? seller.convertToTagImgs(seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].init) :
																								seller.convertToTagStrings(seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].init);
													h+=	'<div id="tags" class="sixth"><div class="listing-mobile title">Search Criteria</div>'+
															'<span class="selectArrow" style="visibility: '+(tagCount > 1 ? "visible" : "hidden" )+'"></span>' +
															'<div class="selected">'+html+'</div>' +
															(tagCount == 1 ? '' : (seller.showStatTagImgs ? seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].html : 
																											seller.visitationsListings[listing].users.sub[userId].sub[ip].sub[date].html2))+
														'</div>'; // Tags
														}
														else
													h+=	'<div id="tags" class="sixth"><div class="listing-mobile title">Search Criteria</div>No tags</div>';
												}
												else
													h+= '<div class="not-premium">User activity is only available for <a href="javascript:seller.getPage('+"'portal'"+');">Premium Accounts</a>.</div>';
											h+=	'</div>'+
										'</li>';
									}
								h+= '</ul>'+
								'</div>'+
							'</section>';
					return h;
				}

				this.activityStats = function(id) {
					console.log("entered showUserDetail for "+id);
					var h =	'<section class="stats" id="activity-stats">' +
								'<div id="column-header" class="activity-events">';
								if ( seller.showSiteEntryStatsInActivityPage )
								h+=	'<span id="title" class="zero">Activity Log</span>';
								h+=	'<div id="titles">'+
										'<span id="title" class="first">Date</span>'+
										'<span id="title" class="second">Area</span>'+
										'<span id="title" class="third desktop">Search Criteria</span>'+
										// '<span id="title" class="fourth desktop">Name</span>'+
										'<span id="title" class="fourth desktop">Price</span>'+
										'<span id="title" class="fifth desktop">Listings Viewed</span>'+
									'</div>'+
								'</div>';
								var portalUser = seller.getPortalUser(id);
								if (!(portalUser.flags & PortalUserFlags.PORTAL_USER_VIEWED))
									seller.page.stats.updatePortalUserFlags(PortalUserFlags.PORTAL_USER_VIEWED, id);
								var haveSiteEntries = false;
								var haveActivity = false;
								h+=	'<div id="list-container" class="activity-events">'+
									'<div class="mobile-name">'+portalUser.first_name+' '+portalUser.last_name+'\'s <strong>Activity</strong></div>'+
									'<ul id="activity">';
								for(var i in portalUser.activityKeyOrder) {
									var date = portalUser.activityKeyOrder[i];
									var activity = portalUser.activity.dates[date];
									haveActivity = (length(activity.quiz) != 0 || length(activity.listing) != 0) || haveActivity;

									if ( length(activity.quiz) ) {
										for(var quizId in activity.quiz) {
											var details = activity.quiz[quizId];
											if (typeof details.homeOptions == 'undefined')
												continue;
									h+=	'<li id="event">'+
											'<div class="container">'+	
												'<span id="date" class="first">'+seller.page.stats.convertDate(date)+'</span>'+
												'<span id="quizType" class="second"><div class="mobile title">Area Searched</div>'+(	details.quizType == 1 ? "Nationwide" :
																					   (details.quizType == 5 ? "Statewide: "+details.state :
																						 						details.location+"<br/>Range: "+(details.distance == 0 ? 'city limit' : details.distance+" miles")))+'</span>';
											var homeOptions = typeof details.homeOptions.beds == 'undefined' || details.homeOptions.beds == '0' ? 'Any beds, ' : details.homeOptions.beds+" beds, ";
											 	homeOptions+= typeof details.homeOptions.baths == 'undefined' || details.homeOptions.baths == '0' ? 'Any baths' : details.homeOptions.baths+" baths";
											var price = '$'+details.price[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' to $'+(details.price[1].replace(/\B(?=(\d{3})+(?!\d))/g, ",") == -1 ? 'MAX' : details.price[1].replace(/\B(?=(\d{3})+(?!\d))/g, ","));
											var tags = '';
											for(var k in details.tags)
												tags += (tags.length? ", " : 'Tags: ')+details.tags[k].tag;
											h+= '<div id="searchCriteria" class="third"><div class="mobile title">Search Criteria</div>'+
													'<span id="homeOptions"  style="display: block;">'+homeOptions+'</span>'+
													'<span id="tags">'+tags+'</span>'+
													'<a id="run-quiz" href="javascript:seller.runQuiz('+quizId+');">View Search</a>'+
												'</div>'+
												'<span id="price" class="fourth"><div class="mobile title">Price Range</div>'+price+'</span>';
												if ( length(details.listings) ) {
													h+=	'<div id="listings" class="fifth"><div class="mobile title">Listings Viewed</div>';
														for(var listingId in details.listings) {
															var listing = details.listings[listingId];
														h+= '<div class="address-wrapper"><span id="address">'+listing.street_address+', '+listing.city+' '+listing.state+'</span>'+
															'<a id="view-listing" href="javascript:seller.showListingDetail('+id+','+listingId+');">View</a></div>';
														}
													h+= '</div>';
												}
												else
												h+=	'<span id="listings" class="fifth"><div class="mobile title">Listings Viewed</div>None</span>';

											'</div>'+
										'</li>';
										}
									} // end if (activity.quiz.length)

									if ( length(activity.listing) ) {
										for(var listingId in activity.listing) {
											var details = activity.listing[listingId].detail;
											var searchTags = activity.listing[listingId].searchTags;
											var homeOptions = details.beds+" beds, "+details.baths+" baths";
									h+=	'<li id="event">'+
											'<div class="container">'+	
												'<span id="date" class="first">'+seller.page.stats.convertDate(date)+'</span>'+
												'<span id="area" class="second">'+details.city+' '+details.state+'</span>'+
												'<div id="searchCriteria" class="third">'+
													'<span id="homeOptions"  style="display: block;">'+homeOptions+'</span>';
												if ( searchTags.length == 0)
												h+= '<span id="tags" >No Tags</span>';
												else if ( searchTags.length > 1){
												h+= '<select id="tags">';
													for(var i in searchTags)
													h+= '<option>'+(seller.showStatTagImgs ? seller.convertToTagImgs(searchTags[i]) :
																							 seller.convertToTagStrings(searchTags[i]))+'</option>';
												h+= '</select>';
												}
												else
												h+= '<span id="tags" >'+'Tags:'+(seller.showStatTagImgs ? seller.convertToTagImgs(searchTags[0]) :
																							 	  seller.convertToTagStrings(searchTags[0]))+'</span>';
											h+= '</div>';
											h+= '<span id="price" class="fourth">$'+details.price+'</span>'+
												'<div id="listings" class="fifth">' +
													'<div class="address-wrapper"><span id="address">'+details.street_address+'</span>'+
													'<a id="view-listing" href="javascript:seller.showListingDetail('+id+','+listingId+');">View</a></div>'+
												'</div>'+
											'</div>'+
										'</li>';
										}
									}

									if ( length(activity.visit) ) {
										haveSiteEntries = true;
										// h+= seller.page.stats.activitySiteEntryStats(id);
									// 	for(var page in activity.visit) {
									// 		for(var ip in activity.visit[page]) {
									// h+=	'<li id="event">'+
									// 		'<div class="container">'+	
									// 			'<span id="date" class="first">'+date+'</span>'+	
									// 			'<span id="page" class="second">'+seller.visitationCities[ activity.visit[page][ip] ]+'</span>'+	
									// 			'<span id="tags" class="third">'+page+'</span>'+ // place holder
									// 			// '<span id="name" class="fourth">N/A</span>'+ // place holder
									// 			'<span id="price" class="fourth">N/A</span>'+ // place holder
									// 			'<span id="listing" class="fifth">N/A</span>'+ // place holder
									// 		'</div>'+
									// 	'</li>';
									// 		}
									// 	}
									}

								} // end for(var i in portalUser.activityKeyOrder)

								if (!haveActivity) {
									h+=	'<li id="event">'+
											'<div class="container">'+	
												'<span id="notice">No Activity</span>'+
											'</div>'+
										'</li>';
									}

								if ( haveSiteEntries &&
									 seller.showSiteEntryStatsInActivityPage ) {	
								h+= '<li><div id="column-header" class="site-entry">'+
										'<span id="title" class="zero">Site Entry Log</span>'+
										'<div id="titles">'+
											'<span id="title" class="first">Date</span>'+
											'<span id="title" class="second">Logged From</span>'+
											'<span id="title" class="third desktop">Page entered</span>'+
										'</div>'+
									'</div>';							
									h+= seller.page.stats.activitySiteEntryStats(id);
									h+= '</li>';
								}


								h+= '</ul>'+
								'</div>'+
							'</section>';

					return h;
				}

				this.updatePortalUserFlags = function(flag, id) {
					var portalUser = seller.getPortalUser(id);
					portalUser.flags |= flag;
					seller.DBget({	query:'update-portal-user-flags', 
									data: {id: id,
										   flag: flag,
										   agentID: validSeller.author_id}, 
									done:function(d){
									},
									error:function(x) {
										console.log("Failed to update portal user:"+id+", with flag:"+flag+", msg:"+x);
									}
								});
				}

				this.updatePortalUserFlagsUnset = function(flag, id) {
					var portalUser = seller.getPortalUser(id);
					portalUser.flags = portalUser.flags & ~flag;
					seller.DBget({	query:'update-portal-user-flags-unset', 
									data: {id: id,
										   flag: flag,
										   agentID: validSeller.author_id}, 
									done:function(d){
									},
									error:function(x) {
										console.log("Failed to unset portal user:"+id+", with flag:"+flag+", msg:"+x);
									}
								});
				}

				this.activitySiteEntryStats = function(id) {
					var h = '<div id="list-container" class="site-entry">'+
								'<ul id="activity">';
							var portalUser = seller.getPortalUser(id);
								for(var i in portalUser.activityKeyOrder) {
									var date = portalUser.activityKeyOrder[i];
									var activity = portalUser.activity.dates[date];
									if ( length(activity.visit) ) {
										for(var page in activity.visit) {
											for(var ip in activity.visit[page]) {
										h+=	'<li id="event">'+
												'<div class="container">'+	
													'<span id="date" class="first">'+date+'</span>'+	
													'<span id="location" class="second">'+seller.visitationCities[ activity.visit[page][ip] ]+'</span>'+	
													'<span id="page" class="third">'+page+'</span>'+ // place holder
												'</div>'+
											'</li>';
											}
										}
									}
								}
							h+= '</ul>'+
							'</div>';
					return h;
				}

				this.portalUserHasActivity = function(id) {
					var portalUser = seller.getPortalUser(id);
					var haveSiteEntries = false;
					var haveActivity = false;
					for(var i in portalUser.activityKeyOrder) {
						var date = portalUser.activityKeyOrder[i];
						var activity = portalUser.activity.dates[date];
						haveActivity = (length(activity.quiz) != 0 || length(activity.listing) != 0) || haveActivity;
					}
					return haveActivity;
				}

				this.getPage = function(what, id) {
					var h = '';
					var havePortalStats = (length(portalUsersList) ||
											(seller.visitations &&
										 	 length(seller.visitations.users)) );
					var haveListings = seller.sellerData.listings && seller.sellerData.listings.length;
                    var isIE = browser.shortname.indexOf('MSIE') != -1 && document.body.createControlRange;
                    var isEdge =  browser.shortname == 'Edge';
                    var isFirefox = browser.shortname == 'Firefox';
					switch(what) {
						case ActiveStatPage.PortalStats:
						h = '<div id="header">'+
								'<span id="title"><img src="'+ah_local.tp+"/_img/_sellers/stats-icon.png"+'" /><p>Stats</p></span>';
							if (havePortal)
							h+= '<span id="subtitle">Agent Portal</span>';
							h+= '<a id="subtitle" href="javascript:seller.page.stats.goToStatsPage('+ActiveStatPage.ListingStats+');">Listing Stats</a>' +
								'<div class="right-links">';
								if (havePortalStats) {
									h+= '<a href="javascript:seller.page.stats.unhideAll();" id="hideAll" class="hideAll" style="display: none;">Unhide All </span></a>';
								 	if (!isFirefox && !isEdge && !isIE) 
										h+= '<a href="javascript:;" class="export">Export <span class="entypo-down-open-big"></span></a>' +
											'<div class="export-drop">' +
												'<a id="print" href="javascript:seller.print('+ActiveStatPage.PortalStats+');">Print</a>' +
												'<a id="print" href="javascript:seller.print('+ActiveStatPage.PortalStats+',true'+');">Save As CSV</a>' +
											'</div>';
								}
								h+= '<button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>' +
									'</div>' +
								'</div>';
							h += seller.page.stats.portalStats();
							break;

						case ActiveStatPage.ListingStats:
						h = '<div id="header">'+
								'<span id="title"><img src="'+ah_local.tp+"/_img/_sellers/stats-icon.png"+'" /><p>Stats</p></span>';
							if (havePortal) {
								h+= '<a id="subtitle" href="javascript:seller.page.stats.goToStatsPage('+ActiveStatPage.PortalStats+');">Agent Portal</a>' +
									'<span id="subtitle">Listing Stats</span>' +
									'<div class="right-links">';
								if (haveListings && !isFirefox && !isEdge && !isIE) {
									h+= '<a href="javascript:;" class="export">Export <span class="entypo-down-open-big"></span></a>' +
										'<div class="export-drop">' +
											'<a id="print" href="javascript:seller.print('+ActiveStatPage.ListingStats+');">Print</a>' +
											//'<a id="print" href="javascript:seller.print('+ActiveStatPage.ListingStats+',true'+');">Save As CSV</a>' +
										'</div>';
								}
								h+= '<button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>' +
									'</div>'
							}
							else {
								h+= '<span id="subtitle">Listing Stats</span>' +
									'<div class="right-links">' +
										'<button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>' +
									'</div>';
							}
						h+= '</div>';
							h += seller.page.stats.listingStats();
							break;

						case ActiveStatPage.ActivityStats:
						h = '<div id="header">'+
								'<span id="title"><img src="'+ah_local.tp+"/_img/_sellers/stats-icon.png"+'" /><p>Stats</p></span>'+
								'<a id="subtitle" href="javascript:seller.page.stats.goToStatsPage('+ActiveStatPage.PortalStats+');">Back To All</a>'+
								'<button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button>'+
							'</div>';
							h += seller.page.stats.activityStats(id);
							break;
					}
					return h;
				}

				this.goToStatsPage = function(where, id) {
					$(document).scrollTop(0);
					seller.statPage = where;
					seller.page.stats.get(id);
				}

				this.get = function(portalUserId){
					portalUserId = typeof portalUserId == 'undefined' ? seller.page.profile.info.author_id : portalUserId;
					seller.content({
						title: 'Stats',
						content: seller.page.stats.getPage(seller.statPage, portalUserId),
						callback: function(){
                          var exportDropWidth = $('#seller-admin .page-stats div#header .right-links .export').innerWidth();
                          $('#seller-admin .page-stats div#header .right-links .export-drop').css('width', exportDropWidth);
                          function exportDropToggle() {
                            $('#seller-admin .page-stats div#header .right-links .export-drop').slideToggle();
                            if ($('#seller-admin .page-stats div#header .right-links .export span').hasClass('entypo-down-open-big')) {
                              $('#seller-admin .page-stats div#header .right-links .export span').removeClass('entypo-down-open-big');
                              $('#seller-admin .page-stats div#header .right-links .export span').addClass('entypo-up-open-big');
                            }
                            else {
                              $('#seller-admin .page-stats div#header .right-links .export span').removeClass('entypo-up-open-big');
                              $('#seller-admin .page-stats div#header .right-links .export span').addClass('entypo-down-open-big');
                            }
                          }
                          $('#seller-admin .page-stats div#header .right-links a.export').on('click', function() {
                            exportDropToggle();
                          });
                          $('#seller-admin .page-stats div#header .right-links .export-drop a:nth-child(2)').on('click', function() {
                            exportDropToggle();
                          });
                          $('.video').on('click', function() {
                              var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-stats']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                          $('.videoDiv .video').html(h);
                              videoControl.showVid();
                          })
                          $(document).click(function() {
                              $('.page-stats section li div').removeClass( 'desktop-drop-active' )
                          });
                          $('.page-stats section li .container select').on('click', function(e) {
                              e.stopPropagation();
                              $(this).parent().toggleClass( 'desktop-drop-active' );
                          });
                          $('li select#location').change(function() {
                              var id = parseInt($(this).attr('for'));
                              var ip = $(this).val();
                              var where = $(this).hasClass('desktop') ? 'desktop' : 'mobile';
                              var dateList = seller.page.stats.getUserDate(id, ip, where);
                              var ele = $('li#user[for='+id+']');
                              var div = $(this).siblings('div#date');
                              div.html(dateList);
                              console.log("For id:"+id+":"+visitorNames[id]+", ip:"+ip+", dateList:"+dateList);
                          })

                          $('section#listing-stats ul#users select#user-list').change(function() {
                              var listingId = $(this).attr('listing-id'); // listing id
                              var userId = $(this).val();

                              seller.visitationsListings[listingId].users.current = userId; // current user selection
                              var html = seller.visitationsListings[listingId].users.sub[userId].html; // this is the ip html
                              // get current location
                              seller.setCurrentIP( listingId, userId, 'ul#users li[for="'+listingId+'"]' );
                          })

                          $('section#listing-stats ul#users select#ip-list').change(function() {
                              var listingId = $(this).attr('listing-id'); // listing id
                              var userId = $(this).attr('user-id'); // listing id
                              var currentIP= $(this).val();

                              seller.visitationsListings[listingId].users.sub[userId].current = currentIP;
                              seller.setCurrentIP(listingId, userId, 'ul#users li[for="'+listingId+'"]');
                          })

                          $('section#listing-stats ul#users select#date-list').change(function() {
                              var listingId = $(this).attr('listing-id'); // listing id
                              var userId = $(this).attr('user-id'); // listing id
                              var ip = $(this).attr('ip');
                              var currentDate = $(this).val();

                              seller.visitationsListings[listingId].users.sub[userId].sub[ip].current = currentDate;
                              seller.setCurrentDate(listingId, userId, ip, 'ul#users li[for="'+listingId+'"]');
                          })

                          $('div#tags').attr('value', 0); // default value - ALL
                          //$('.selectBed ul').html(options3);
                          $('div#tags ul').find('li').click(function(){
                              $('div#tags ul li[value]').removeClass('active');
                              var val = $(this).attr('value');
                              var listingId = $(this).parent().attr('listing-id'); // listing id
                              var userId = $(this).parent().attr('user-id'); // listing id
                              var ip = $(this).parent().attr('ip');
                              var date = $(this).parent().attr('date');
                              seller.visitationsListings[listingId].users.sub[userId].sub[ip].sub[date].init = val; 
                              $(this).parent().css('display','none');
                              $(this).addClass('active');
                              $(this).closest('div#tags').attr('value',val);
                              $(this).parent().siblings('.selected').html( seller.showStatTagImgs ? seller.convertToTagImgs(val) : seller.convertToTagStrings(val));
                              console.log("Tag value:"+val);
                          });

                          $('div#tags').each(function(){
                              $(this).children('.selected,span.selectArrow').click(function(event){
                                  event.stopPropagation();
                                  if($(this).parent().children('ul').css('display') == 'none'){
                                      $('#stat-table-listing div#tags').removeClass('active');
                                      $('#stat-table-listing div#tags ul').hide();
                                      $(this).parent().children('ul').css('display','block');
                                      $('div#tags').addClass('active');
                                  }
                                  else {
                                      $(this).parent().children('ul').css('display','none');
                                      $('div#tags').removeClass('active');
                                  }
                              });
                          });

                          if (seller.haveHidden)
							$('a#hideAll').fadeIn(250);
                        }
					});
				} // end this.get()

				this.showUserDetailMobile = function(id) {
					$('div.details[for='+id+']').css('display','table');
					$('#seller-admin section.content .page-stats section#portal-visitors li[for='+id+'] .second #more').hide();
					$('#seller-admin section.content .page-stats section#portal-visitors li[for='+id+'] .second #closeDetail').css('display','inline-block');
				}

				this.closeUserDetailMobile = function(id) {
					$('div.details[for='+id+']').hide();
					$('#seller-admin section.content .page-stats section#portal-visitors li[for='+id+'] .second #more').css('display','inline-block');
					$('#seller-admin section.content .page-stats section#portal-visitors li[for='+id+'] .second #closeDetail').hide();
				}

				this.getUserIPandDatePortalUser= function(id, pos, where) {
					var portalUser = seller.getPortalUser(id);
					if (!seller.page.stats.portalUserHasActivity(portalUser.id))
						return '<span id="location" class="'+pos+' '+where+'">No Activity</span>';
					
					var count = 0;
					var h = '';
					// for(var i in portalUser.activityKeyOrder) {
					// 	var date = portalUser.activityKeyOrder[i];
					// 	var activity = portalUser.activity.dates[date];
					// 	if ( length(activity.visit) ) {
					// 		for(var page in activity.visit) 
					// 			for(var ip in activity.visit[page]) 
					// 				count++;
					// 	}
					// }

					// var multi = count > 1;
					var multi = length(portalUser.activityKeyOrder) > 1;
					if (multi) 
					h +='<div class="dropdown-arrow"><span class="entypo-down-open"></span></div><select for="'+id+'" id="location" class="'+pos+' '+where+'">';

					for(var i in portalUser.activityKeyOrder) {
						var date = portalUser.activityKeyOrder[i];
						var activity = portalUser.activity.dates[date];
						var activityStr = '';
						var ipDate = '';
						var siteCount = 0;
						var quizCount = 0;
						var listingCount = 0;
						if ( seller.showSiteEntryStatsInActivityPage &&
							 length(activity.visit) ) {
							for(var page in activity.visit) 
								for(var ip in activity.visit[page]) {
									siteCount++;
									if (ipDate.length == 0)
										ipDate = activity.visit[page][ip];
									// if (!multi)
									// h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ activity.visit[page][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+'</span>';
								}

							activityStr += 'P:'+siteCount;

							// if (multi)
							// h +='<option value="'+ip+'">'+seller.visitationCities[ activity.visit[page][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+'</option>'; //+(count > 1 ? ' ('+count+')' : '')+'</option>';
						}
						
						if ( length(activity.quiz) ) {
							for(var quiz in activity.quiz) {
								quizCount++;
								if (ipDate.length == 0)
									ipDate = activity.quiz[quiz].ip;
									// if (!multi)
									// h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ activity.quiz[quiz][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+'</span>';
								// add up how many listings were viewed from this quiz
								if (typeof activity.quiz[quiz].listings != 'undefined')
									listingCount += length(activity.quiz[quiz].listings);
							}
							activityStr += (activityStr.length ? ',' : '')+'Q:'+quizCount;
							// if (multi)
							// 	h +='<option value="'+ip+'">'+seller.visitationCities[ activity.quiz[quiz][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+(count > 1 ? ' ('+count+')' : '')+'</option>';
						}
						
						if ( length(activity.listing) ) {
							listingCount += length(activity.listing);
							for(var id in activity.listing) 
								if (ipDate.length == 0)
									ipDate = activity.listing[id].ip;
								// for(var ip in activity.listing[id]) {
								// 	count++;
								// 	if (!multi)
								// 	h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ activity.quiz[id][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+'</span>';
								

							// if (multi)
							// 	h +='<option value="'+ip+'">'+seller.visitationCities[ activity.listing[id][ip] ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+(count > 1 ? ' ('+count+')' : '')+'</option>';
						}
						if (listingCount)
							activityStr += (activityStr.length ? ',' : '')+'L:'+listingCount;

						if ( (!quizCount && !listingCount &&
							  (siteCount &&
							   seller.showSiteEntryStatsInActivityPage)) ||
							   quizCount ||
							   listingCount ) {
							if (!multi)
								h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ ipDate ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+' '+activityStr+'</span>';
							else
								h +='<option value="'+ip+'">'+seller.visitationCities[ ipDate ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+' '+activityStr+'</option>';
						}
						// else if (!multi)
						// 	h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ ipDate ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+' '+activityStr+'</span>';
						// else
						// 	h +='<option value="'+ip+'">'+seller.visitationCities[ ipDate ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate(date)+' '+activityStr+'</option>';
					}

					if (multi)
					h +='</select>';
					
					return h;
				}

				this.getUserIPandDate = function(id, pos, where) {
					var h = '';
					var ip = Object.keys(seller.visitations.users[id])[0];
					if ( length(seller.visitations.users[id]) > 1 ||
						 length(seller.visitations.users[id][ip]) > 1 ) {
					h +='<div class="dropdown-arrow"><span class="entypo-down-open"></span></div><select for="'+id+'" id="location" class="'+pos+' '+where+'">';
						for(ip in seller.visitations.users[id]) {
							for(var date in seller.visitations.users[id][ip])
							h +='<option value="'+ip+'">'+seller.visitationCities[ ip ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate( seller.visitations.users[id][ip][date] )+'</option>';
						}
					h +='</select>';	
					}
					else {
						var date = Object.keys(seller.visitations.users[id][ip])[0];
						h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ ip ]+' &nbsp;&nbsp; '+seller.page.stats.convertDate( seller.visitations.users[id][ip][date] )+'</span>';
					}
					return h;
				}

				this.getUserIP = function(id, pos, where) {
					var h = '';
					var ip = '';
					if ( length(seller.visitations.users[id]) > 1 ) {
					h +='<select for="'+id+'" id="location" class="'+pos+' '+where+'">';
						for(ip in seller.visitations.users[id]) 
						h +='<option value="'+ip+'">'+seller.visitationCities[ ip ]+'</option>';
					h +='</select>';
					}
					else {
						ip = Object.keys(seller.visitations.users[id])[0];
						h += '<span id="location" class="'+pos+' '+where+'">'+seller.visitationCities[ ip ]+'</span>';
					}
					return h;
				}

				this.getUserDate = function(id, ip, where) {
					// do dates
					var dateList = '';
					// ip = Object.keys(seller.visitations.users[id])[0]; // reset to the first one to show
					if ( length(seller.visitations.users[id][ip]) > 1 ) {
					dateList +='<select id="date" for="'+id+'" ip="'+ip+'" class="'+where+'">';
						for(var date in seller.visitations.users[id][ip])
						dateList +='<option value="'+ip+'">'+seller.page.stats.convertDate( seller.visitations.users[id][ip][date] )+'</option>';
					dateList +='</select>';
					}
					else {
						date = Object.keys(seller.visitations.users[id][ip])[0];
						dateList += '<span id="date" class="'+where+'">'+seller.page.stats.convertDate( seller.visitations.users[id][ip][date] )+'</span>';
					}
					return dateList;
				}
				this
			}

			this.home = new function(){
				this.get = function(){
					seller.content({
						title: 'Home',
						callback: function(){						
							var d = seller.sellerData; // just to keep things simple and not do a find-replace right now
							var allCompleteListings = true;
							var haveMyPortal = havePortal == PortalState.PORTAL_VALID || havePortal == PortalState.PORTAL_EXPIRING;

							if (typeof d == 'object') for(var i in d) {
								// haveMyPortal = d[i].havePortal;
								var complete = 	d[i].street_address != null &&
												d[i].beds != null &&
												d[i].baths != null &&
												d[i].city != null &&
												d[i].state != null &&
												d[i].tags != null &&
												d[i].tags != false &&
												// d[i].tags.length != 0 &&
												//l.tags.length &&
												d[i].about != null &&
												d[i].images != null &&
												d[i].lotsize != null &&
												d[i].interior != null &&
												d[i].price != null;
								if (!complete) {
									allCompleteListings = false;
									break;
								}
							}
							else
								allCompleteListings = false;

							validSeller.allCompleteListings = allCompleteListings;

							var h = '<div class="home-content">'+
										'<ul id="cardlist">';

							if (length(dashboardCards)) {
								for(var i in dashboardCards) {
										h+=	'<li>'+dashboardCards[i]+'</li>';
								}
							}
							else
								h+= 		'<li>No cards to show</li>';
								
							h+= 		'</ul>'+
										'<div class="feedbackdiv"><a href="javascript:ahfeedback.openModal();" class="feedback">Give Us Feedback <span class="entypo-right-dir"></span></a></div>' +
									'</div>';


							// var showStep3 = (validSeller.photo != null || allCompleteListings == true) && !haveMyPortal;
							// h= 	'<div class="home-content incomplete">';
							// 		h+= '<div class="step1" '+(validSeller.photo != null ? 'style="display:none;"' : '' )+'>'+
							// 			'<div class="left">'+
							// 				'<span class="title">Step 1</span>'+
							// 				'<span class="subtitle">Complete Your Profile</span>'+
       //                '<img style="margin: 0 auto 30px auto; display: block; height: 152px; border-radius: 100%;" src="'+ah_local.tp+'/_img/_sellers/step1-large.jpg" />'+
							// 				'<div class="mobilesteps">'+
							// 					'<p>Give buyers and agents reasons to work with you by putting a face to your name, and listing personal and professional accomplishments.</p><p style="points">Add a Photo</p><p style="points">Add a Bio</p><p style="points">Update Basic Agent Info</p>'+
							// 				'</div>'+
       //                '<a href="#" class="steps-button">Update Profile</a>'+
							// 			'</div>'+
							// 			'<div class="right">'+
							// 				'<span class="title">Get Personal</span>'+
       //                '<span class="line"></span>'+
							// 				'<p>Give buyers and agents reasons to work with you by putting a face to your name, and listing personal and professional accomplishments.</p><p style="points">Add a Photo</p><p style="points">Add a Bio</p><p style="points">Update Basic Agent Info</p>'+
							// 			'</div>'+
							// 		'</div>'+
							// 		'<div class="step2" '+(allCompleteListings == true ? 'style="display:none;"' : '' )+'>'+
							// 			'<div class="left">'+
							// 				'<span class="title">Step 2</span>'+
							// 				'<span class="subtitle">Improve / Add Listings</span>'+
			    //             '<img style="margin: 0 auto 30px auto; display: block; height: 152px; border-radius: 100%;" src="'+ah_local.tp+'/_img/_sellers/step2-large.jpg" />'+
							// 				'<div class="mobilesteps">'+
							// 					'<p>Give your listings the edge FREE by uploading high quality images and tagging your listing so we can match buyers to your home.</p>'+
							// 					'<p style="points">Add High Res Photos</p>'+
							// 					'<p style="points">Add Listing Tags, <span style="color: #f5912c;">IMPORTANT!</span></p>'+
							// 					'<p style="points">Update Basic Agent Info</p>'+
							// 				'</div>'+
							// 				'<a href="#" class="steps-button">Update Listings</a>'+
							// 			'</div>'+
							// 			'<div class="right">'+
							// 				'<span class="title">Get Discovered</span>'+
       //                                      '<span class="line"></span>'+
							// 				'<p>Give your listings the edge FREE by uploading high quality images and tagging your listing so we can match buyers to your home.</p>'+
							// 				'<p style="points">Add High Res Photos</p>'+
							// 				'<p style="points">Add Listing Tags, <span style="color: #f5912c;">IMPORTANT!</span></p>'+
							// 				'<p style="points">Update Basic Agent Info</p>'+
							// 			'</div>'+
							// 		'</div>'+
							// 		'<div class="step3" '+(showStep3 != true ? 'style="display:none;"' : '' )+'>'+
							// 			'<div class="left">'+
							// 				'<span class="title">Step 3</span>'+
							// 				'<span class="subtitle">Get Your Portal</span>'+
       //                '<img style="margin: 1em 0 .95em; max-height: 175px; display: block; width: 100%;" src="'+ah_local.tp+'/_img/_sellers/step3-large.jpg" />'+
							// 				'<div class="mobilesteps">'+
							// 					'<p>Agent Portal makes you the gateway for your clients&#39; real estate needs no matter where they buy in the U.S.</p>'+
							// 					'<p style="points">Get your own Agent Portal</p>'+
							// 					'<p style="points">Capture <span style="color: #f5912c;">Leads!</span></p>'+
							// 					'<p style="points">Earn <span style="color: #f5912c;">Referral Fees</span></p>'+
							// 				'</div>'+
							// 				'<a href="#" class="steps-button">My Portal</a>'+
							// 			'</div>'+
							// 			'<div class="right">'+
							// 				'<span class="title">Stand Out</span>'+
       //                '<span class="line"></span>'+
							// 				'<p>Agent Portal makes you the gateway for your clients&#39; real estate needs no matter where they buy in the U.S.</p>'+
							// 				'<p style="points">Get your own Agent Portal</p>'+
							// 				'<p style="points">Capture <span style="color: #f5912c;">Leads!</span></p>'+
							// 				'<p style="points">Earn <span style="color: #f5912c;">Referral Fees</span></p>'+
							// 			'</div>'+
							// 		'</div>';
							// 	// // Temporarily commented out until JD confirms new design
							// 	// h+= '<div id="statsDiv">';
							// 	// 	h+= seller.buildVisitationStatsPortal();
							// 	// 	h+= seller.populateVisitationsListings();
							// 	// h+= '</div>';
							// 	h+= '<a href="javascript:ahfeedback.openModal();" class="feedback">Give Us Feedback <span class="entypo-right-dir"></span></a>' +
							// 		//'<div class="footnote">'+
							// 		//	'<span id="message">*For the time being the Agent Dashboard is best experienced on the Google Chrome Browser. More compatibility and mobile coming soon!</span>'+
							// 		//	'<div id="opt-in">'+
							// 		//		'<input type="checkbox" id="opt-in"/>'+
							// 		//		'<span>I would like to receive emails with helpful guides and updates related to my account.</span>'+
							// 		//	'</div>'+
							// 		//'</div>'+
							// 	'</div>';

							// // allCompleteListings = true;// cheat, remove later!!
							
							// if ( allCompleteListings &&
							// 	 validSeller.photo != null &&
							// 	 !showStep3 ) {
							// 	   h = '<div class="home-content complete">' +
							// 				//'<div class="maintitle"><span class="title">Welcome to Your Agent Dashboard</span></div>'+
							// 				'<div class="alldone">' +
							// 					'<h3 class="title">'+"Congratulations!  Your dashboard is up to date."+'</h3>' +
							// 					// '<h1 class="sub-title">More features to come soon</h1>'+
							// 					'<span>- Team Lifestyled</span>' +
							// 				'</div>';
							// 				// Temporarily commented out until JD confirms new design
							// 		   // h+=  '<div id="statsDiv">';
							// 					// h+= seller.buildVisitationStatsPortal();
							// 					// h+= seller.populateVisitationsListings();
							// 		   // h+=  '</div>';
							// 			h+= '<a href="javascript:ahfeedback.openModal();" class="feedback">Give Us Feedback <span class="entypo-right-dir"></span></a>'+
							// 			'</div>';
							// }
										
							// if (!allCompleteListings ||
							// 	showStep3)
							// 	$('.page-home').css('min-height', '725px');

							var metas = validSeller.meta;
							var cardMeta = {action: SellerMetaFlags.SELLER_COMPLETED_CARDS};
							var ownedMetas = [];
							if (metas) for(var i in metas) {
								if (metas[i].action == SellerMetaFlags.SELLER_COMPLETED_CARDS) 
									cardMeta = metas[i];
								else
									ownedMetas[ownedMetas.length] = metas[i];
							}

							$('.page-home').html(h).hide().fadeIn(250, function() {
								var origCardMeta = JSON.parse(JSON.stringify(cardMeta)); // makes copy of it
								setSellerDashBoardCards(validSeller, cardMeta);
								$('input#opt-in').prop('checked', seller.page.profile.info.flags & SellerFlags.SELLER_HAS_OPTED_IN ? true : false);
								var needUpdate = false;
								if (Object.keys(origCardMeta).length != Object.keys(cardMeta).length)
									needUpdate = true;
								else {
									for(var i in cardMeta) {
										if (typeof origCardMeta[i] != 'undefined') {
											if (typeof cardMeta[i] != 'object') {
												if (origCardMeta[i] != cardMeta[i])
													needUpdate = true;
											}
											else for(var j in cardMeta[i]) {
												if (typeof origCardMeta[i][j] == 'undefined' ||
													origCardMeta[i][j] != cardMeta[i][j])
													needUpdate = true;
											}
										}
										else
											needUpdate = true;
										if (needUpdate)
											break;
									}
								}
								if (needUpdate) {
									ownedMetas[ownedMetas.length] = cardMeta;
									var data = {meta: ownedMetas};
									var sans = {data: data};
									seller.page.profile.saveToDb(sans, UpdateAuthorOrigin.Dashboard);
								}
							});
							
							// timeoutID = window.setTimeout(delayhomeanimate, 500);
							// function delayhomeanimate() {
							// 	$('.page-home .home-content .step1').addClass( 'animate' );
							// 	$('.page-home .home-content .step2').addClass( 'animate' );
							// 	$('.page-home .home-content .step3').addClass( 'animate' );
							// 	timeoutID = 0;
							// }
							
							// $('.step1 a').on('click', function() {
							// 	seller.getPage('profile');
							// })

							// $('.step2 a').on('click', function() {
							// 	seller.getPage('listings');
							// })

							// $('.step3 a').on('click', function() {
							// 	seller.getPage('portal');
							// })

							// $('input#opt-in').on('change', function() {
							// 	var checked = this.checked;
							// 	console.log("OptIn is now "+checked);
							// 	var fields = {};
							// 	fields.flags = checked ? seller.page.profile.info.flags | SellerFlags.SELLER_HAS_OPTED_IN :
							// 							 seller.page.profile.info.flags & ~SellerFlags.SELLER_HAS_OPTED_IN;
							// 	seller.saving = true;
							// 	seller.DBget({	query:'update-author-flags', 
							// 					data: {id: validSeller.id,
							// 						   fields: fields}, 
							// 					done:function(d){
							// 						seller.page.profile.info.flags = fields.flags;
							// 						// if (san.data.first_name != $('.header-login .name').html()) $('.header-login .name').html(data.first_name);
							// 						seller.saving=false;
							// 						// ahtb.alert("Profile saved.", {height: 150});
													
							// 						seller.am_meta = JSON.parse(JSON.stringify(seller.am_meta));
							// 						validSeller.flags = seller.page.profile.info.flags;
							// 						// seller.initial_listings = validSeller.listings;
							// 						// seller.updateMyListingsButton();													
							// 					},
							// 					error:function(x) {
							// 						var msg = typeof x == 'string' ? x :
							// 								  typeof x == 'object' ? JSON.stringify(x) :
							// 								  "Unknown reason";
							// 						ahtb.alert("Failure saving opt-in flag: "+msg);
							// 					}
							// 				});
							// })

							// $('#stat-div-portal-visits select#ip-list').change(function() {
					  //  			var id = parseInt($(this).attr('for'));
					  //  			var ip = $(this).val();
					   			
					  //  			var dateList = '';
					  //  			if (id < portalUsersOffset) {
							// 		if ( length(seller.visitations.users[id][ip]) > 1 ) {
							// 		dateList +='<select id="date-list" for="'+id+'" ip="'+ip+'">';
							// 			for(var date in seller.visitations.users[id][ip])
							// 			dateList +='<option value="'+ip+'">'+seller.visitations.users[id][ip][date]+'</option>';
							// 		dateList +='</select>';
							// 		}
							// 		else {
							// 			date = Object.keys(seller.visitations.users[id][ip])[0];
							// 			dateList += '<span>'+seller.visitations.users[id][ip][date]+'</span>';
							// 		}
							// 	}
							// 	else {
							// 		pid = id - portalUsersOffset;
							// 		if ( length(seller.visitations.portalUsers[pid][ip]) > 1 ) {
							// 		dateList +='<select id="date-list" for="'+pid+'" ip="'+ip+'">';
							// 			for(var date in seller.visitations.portalUsers[pid][ip])
							// 			dateList +='<option value="'+ip+'">'+seller.visitations.portalUsers[pid][ip][date]+'</option>';
							// 		dateList +='</select>';
							// 		}
							// 		else {
							// 			date = Object.keys(seller.visitations.portalUsers[pid][ip])[0];
							// 			dateList += '<span>'+seller.visitations.portalUsers[pid][ip][date]+'</span>';
							// 		}
							// 	}

							// 	$('#stat-table-portal tr[id="'+id+'"] div#date-list').html(dateList);
							// 	console.log("For id:"+id+":"+visitorNames[id]+", ip:"+ip+", dateList:"+dateList);
					  //  		})

					  //  		$('#stat-div-listing-visits select#user-list').change(function() {
					  //  			var listingId = $(this).attr('listing-id'); // listing id
					  //  			var userId = $(this).val();

					  //  			seller.visitationsListings[listingId].users.current = userId; // current user selection
					  //  			var html = seller.visitationsListings[listingId].users.sub[userId].html; // this is the ip html
					  //  			// get current location
					  //  			seller.setCurrentIP(listingId, userId, '#stat-table-listing tr[id="'+listingId+'"]');
					  //  		})

							// $('#stat-div-listing-visits select#ip-list').change(function() {
					  //  			var listingId = $(this).attr('listing-id'); // listing id
					  //  			var userId = $(this).attr('user-id'); // listing id
					  //  			var currentIP= $(this).val();

					  //  			seller.visitationsListings[listingId].users.sub[userId].current = currentIP;
					  //  			seller.setCurrentIP(listingId, userId, '#stat-table-listing tr[id="'+listingId+'"]');
							// })

							// $('#stat-div-listing-visits select#date-list').change(function() {
					  //  			var listingId = $(this).attr('listing-id'); // listing id
					  //  			var userId = $(this).attr('user-id'); // listing id
					  //  			var ip = $(this).attr('ip');
					  //  			var currentDate = $(this).val();

					  //  			seller.visitationsListings[listingId].users.sub[userId].sub[ip].current = currentDate;
					  //  			seller.setCurrentDate(listingId, userId, ip, '#stat-table-listing tr[id="'+listingId+'"]');
							// })

							// $('div#tags').attr('value', 0); // default value - ALL
							// //$('.selectBed ul').html(options3);
							// $('div#tags ul').find('li').click(function(){
							// 	$('div#tags ul li[value]').removeClass('active');
							// 	var val = $(this).attr('value');
							// 	var listingId = $(this).parent().attr('listing-id'); // listing id
					  //  			var userId = $(this).parent().attr('user-id'); // listing id
					  //  			var ip = $(this).parent().attr('ip');
					  //  			var date = $(this).parent().attr('date');
					  //  			seller.visitationsListings[listingId].users.sub[userId].sub[ip].sub[date].init = val; 
							// 	$(this).parent().css('display','none');
							// 	$(this).addClass('active');
							// 	$(this).closest('div#tags').attr('value',val);
							// 	$(this).parent().siblings('.selected').html(seller.showStatTagImgs ? seller.convertToTagImgs(val) : seller.convertToTagStrings(val));
							// 	console.log("Tag value:"+val);
							// });

							// $('div#tags').each(function(){
							// 	$(this).children('.selected,span.selectArrow').click(function(event){
							// 		event.stopPropagation();
							// 		if($(this).parent().children('ul').css('display') == 'none'){
							// 			$('#stat-table-listing div#tags').removeClass('active');
							// 			$('#stat-table-listing div#tags ul').hide();
							// 			$(this).parent().children('ul').css('display','block');
							// 			$('div#tags').addClass('active');
							// 		}
							// 		else {
							// 			$(this).parent().children('ul').css('display','none');
							// 			$('div#tags').removeClass('active');
							// 		}
							// 	});
				   //  		});

				    		// $(document).click( function(){
				    		// 	$('#stat-table-listing div#tags').removeClass('active');
				    		// 	$('#stat-table-listing div#tags ul').hide();
				    		// });

							// inactive
							// $('.page-home button').on('click',function(){ console.log('activate'); });
							// $('.page-home').addClass('inactive');

							// $('.page-home button').on('click',function(){ seller.initial_listings = null; window.location = ah_local.wp+'/new-listing/' });
							$('.page-home').addClass('active');				
						}
					});
				}
			}
			this.listings = new function(){
				this.displayListingList = function(d, closeAhtb) {
					var $this = seller.page.listings;
					$this.info = $this.parseRaw(d);
					if ($this.info.length)
						for (var i in $this.info) {
							$($this.parseRawSingle($this.info[i])).appendTo('ul#listings').hide().fadeIn({duration:500, queue: false});
						}
					else {
						var h = '<li class="listing">'+
									'<div class="nolisting">' +
										'<h3 class="title">'+"There are no listings yet for this account"+'</h3>' +
										'<div class="meta-actions">'+
											'<button class="add-listing">Add Listing</button>'+
										'</div>'+
									'</div>'+
								'</li>';
						$(h).appendTo('ul#listings').hide().fadeIn({duration:500, queue: false});
					}
					if (typeof closeAhtb != 'undefined')
						ahtb.close();
					$('.listing .meta .meta-actions > button').on('click', function(){
						if ($(this).hasClass('edit-price'))
							$this.edit('price', {
								id: $(this).attr('listing-id') ? parseInt($(this).attr('listing-id')) : 0,
								price: $(this).attr('price') && $(this).attr('price') != 'null' ? $(this).attr('price') : 0
							});
						else if ($(this).hasClass('edit-listing')) { seller.initial_listings = null; window.location = ah_local.wp+'/new-listing/#listingID='+$(this).attr('listing-id'); }
						else if ($(this).hasClass('delete-listing')) $this.delete($(this).attr('listing-id'));
					});
					$('.listing .nolisting .meta-actions > button').on('click', function(){
							seller.initial_listings =  null;
							window.location = ah_local.wp+'/new-listing';
					});
					$('.listing .overlay button[for="editing"]').on('click', function(){ $this.edit('active', {id: $(this).attr('listing-id')}); });
					$('.listing .background .view-listing button').on('click', function(){ seller.initial_listings = null; window.location = ah_local.wp+'/listing/'+$(this).attr('listing-id')});
					$('.listing .meta-actions-mobile button.view-listing').on('click', function(){ seller.initial_listings = null; window.location = ah_local.wp+'/listing/'+$(this).attr('listing-id')});
					$('.listing .overlay #whats_wrong button[for="go_away"]').on('click', function() {
						$(this).parent().hide();
					})
				}
				this.get = function(){
					if (seller.initial_listings != null) {
						seller.content({
								title: 'My Listings',
								callback: function(){
									$('.page-listings').html('<div class="mobilebanner"><p>Editing listings is unavailable on mobile devices at this time.</p></div>'+
															 '<div id="add-listing"><button class="add-listing">Add Listing</button></div>'+
															 '<ul id="listings"></ul>');
									$('div#add-listing button').on('click', function() {
										seller.initial_listings = null;
										window.location = ah_local.wp+'/new-listing';
									})
									setTimeout( function() { seller.page.listings.displayListingList(seller.initial_listings); }, 50 );
								}
						});
						return;
					}
					ahtb.loading('Loading...',{
						opened: function(){
							seller.content({
								title: 'My Listings',
								callback: function(){
									$('.page-listings').html('<div class="mobilebanner"><p>Editing listings is unavailable on mobile devices at this time.</p></div>'+
															 '<div id="add-listing"><button class="add-listing">Add Listing</button></div>'+
															 '<ul id="listings"></ul>');
									$('div#add-listing button').on('click', function() {
										seller.initial_listings = null;
										window.location = ah_local.wp+'/new-listing';
									})
									var $this = seller.page.listings;
									seller.DBget({
										query: 'get-listings-by-author',
										data: ah_local.author_id,
										done: function(d){
											seller.initial_listings = d;
											seller.updateMyListingsButton();
											$this.displayListingList(d, true);
										}
									});
								}
							});
						}
					})
				}
				this.parseRaw = function(raw){
					var parsed = [];
					if (raw && raw.length > 0) for (var i in raw){
						var ints = ['author','id','active','beds','baths','interior','lotsize','price','favorites'];
						var json = ['tags','images'];
						for (var j in ints) if (typeof raw[i][ints[j]] !== 'undefined' && raw[i][ints[j]] != null) raw[i][ints[j]] = parseInt(raw[i][ints[j]]);
						for (var j in json) if (typeof raw[i][json[j]] == 'string' && raw[i][json[j]] != null) raw[i][json[j]] = $.parseJSON(raw[i][json[j]]);
						if (raw[i].added == "0000-00-00 00:00:00") raw[i].addedFormatted = { obj: null, date: 'Before Time', time: 'Before Time' };
						else {
							var date = raw[i].added.replace(" ","T");
							var d = new Date(date);
							raw[i].addedFormatted = { obj: d, date: d.toDateString(), time: d.toLocaleTimeString() };
						}
						parsed[raw[i].id] = raw[i];
					}
					return parsed;
				}
				this.parseRawSingle = function(l){
					var title = "N/A";
					if (l.street_address != null) {
						title = l.street_address;
						var len = l.street_address.length;
						var temp = l.street_address;
						var title = temp;
						if (len > 20)
							title = temp.substring(0, 20)+"...";
					}

					var complete = l.street_address != null &&
									l.beds != null &&
									l.baths != null &&
									l.city != null &&
									l.state != null &&
									l.tags != null &&
									l.tags != false &&
									//l.tags.length &&
									l.about != null &&
									l.images != null &&
									l.lotsize != null &&
									l.interior != null &&
									l.price != null;

					var msg = '';
					if (!complete) {
					 	msg = ''; var needComma = false;
						if (l.street_address == null) { msg += "street address"; needComma = true; }
						if (l.beds == null) { msg += (needComma ? ", " : '') + "beds"; needComma = true; }
						if (l.baths == null) { msg += (needComma ? ", " : '') + "baths"; needComma = true; }
						if (l.city == null) { msg += (needComma ? ", " : '') + "city"; needComma = true; }
						if (l.state == null) { msg += (needComma ? ", " : '') + "state"; needComma = true; }
						if (l.tags == null || l.tags == false) { msg += (needComma ? ", " : '') + "tags"; needComma = true; }
						if (l.about == null) { msg += (needComma ? ", " : '') + "about"; needComma = true; }
						if (l.images == null) { msg += (needComma ? ", " : '') + "images"; needComma = true; }
						if (l.lotsize == null) { msg += (needComma ? ", " : '') + "lotsize"; needComma = true; }
						if (l.interior == null) { msg += (needComma ? ", " : '') + "interior"; needComma = true; }
						if (l.price == null) { msg += (needComma ? ", " : '') + "price"; needComma = true; }
					}

					var image = l.images && l.images[0] && l.images[0].file ? (l.images[0].file.indexOf('http:') != -1 ? l.images[0].file : ah_local.tp+'/_img/_listings/845x350/'+l.images[0].file) : "_blank.jpg";
					var h = '<li class="listing'+(l.active ? '' : ' inactive')+'" listing-id="'+l.id+'">'+
						'<div class="meta">'+
							'<h3 class="title">'+title+'</h3>'+
							'<ul class="meta-details">'+
                                '<li class="date-created"><span class="label">Created:</span> '+l.addedFormatted.date+'</li>'+
								'<li class="price"><span class="label">Price:</span> <span class="value">'+(l.price ? '$'+l.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 'Price Not Set')+'</span></li>'+
								'<li class="status"><span class="label">Status:</span> <span class="value">'+
								'<span class="active">'+ActiveStateNames[l.active]+'</span></span></li>';
								switch(l.active) {
									case ListingActiveCode.AH_NEVER:
									h+= '<li class="status"><span class="label">*Issues: '+listingErrorCode(l.error)+'</span></li>' +
										 '<li class="status"><span class="label">Thus excluded from search results at this time.</span></li>';
										break;
									case ListingActiveCode.AH_WAITING:
									h+= '<li class="status"><span class="label">*Listing may need better images, or</span></li>' +
										 '<li class="status"><span class="label">more tags/better description.</span></li>';
										break;
									case ListingActiveCode.AH_REJECTED:
									// h+= '<li class="status"><span class="label">*JD didn&#39;t like your listing.</span></li>' +
									// 	 '<li class="status"><span class="label">call for his number.</span></li>';
									h+= '<li class="status"><span class="label">*Contact us.</span></li>';
										break;
									case ListingActiveCode.AH_TOOCHEAP:
									case ListingActiveCode.AH_AVERAGEM:
									h+= '<li class="status"><span class="label">*Listing price is below our threshold.</span></li>';
										break;
								}
							h+= '<li class="certified"><span class="label"></span> </li>'+
							'</ul>'+
							'<div class="meta-actions">'+
								// '<button class="edit-price" listing-id="'+l.id+'" price="'+l.price+'">Edit Price</button>'+
								'<button class="edit-listing" listing-id="'+l.id+'">Edit Listing</button>'+
								'<button class="delete-listing" listing-id="'+l.id+'">Delete Listing</button>'+
							'</div>'+
							'<div class="meta-actions-mobile">'+
								// '<button class="edit-price" listing-id="'+l.id+'" price="'+l.price+'">Edit Price</button>'+
								'<button class="view-listing" listing-id="'+l.id+'" for="viewing">View Listing</button>'+
								'<button class="delete-listing" listing-id="'+l.id+'">Delete Listing</button>'+
							'</div>'+
						'</div>'+
						'<div class="background'+(complete ? '' : ' incomplete')+'">'+
							'<img src="'+image+'" />'+
							'<div class="overlay">'+
								'<div class="edit-arrow">Edit</div><img src="'+ah_local.tp+'/_img/_sellers/edit-arrow.png" class="edit-arrow-img" />'+
								'<div><span>Listing Incomplete</span><button class="activate" listing-id="'+l.id+'" for="editing">Edit Listing</button></div>'+
								'<div id="whats_wrong"><span id="show_fix">Please add to your listing</span><span id="fixings">'+msg+'</span><button for="go_away">Ignore</button></div>'+
							'</div>'+
							'<div class="view-listing"><button listing-id="'+l.id+'" for="viewing">View Listing</button></div>'+
						'</div>';
					h+= '</li>';
					return h;
				}
				this.delete = function(id){
					ahtb.open({
						title: 'Delete Listing?',
						hideSubmit: true,
						width: 450,
						height: 180,
						html: '<p style="width:90%;margin:1em auto;font-size:1em;">Are you sure you want to delete this listing? <strong>WARNING:</strong> This cannot be undone.</p><button class="delete">Delete</button><button class="cancel">Cancel</button>',
						open: function(){
							$('#tb-submit button').on('click', function(e){
								e.preventDefault();
								if (!$(this).hasClass('delete')) ahtb.close(); else {
									ahtb.edit({html:'<p>Deleting listing...</p>', hideClose: true, closeOnClickBG: false, opened: function(){
										seller.DBget({ query: 'delete-listing', data: {listing_id: id}, done: function(){
											seller.page.listings.info.splice(id, 1);
											$('li.listing[listing-id='+id+']').slideUp(250,function(){
												$(this).remove();
												ahtb.close();
												if ( $('.page-listings ul#listings li').length == 0) {
													var h = '<li class="listing">'+
																'<div class="nolisting">' +
																	'<h3 class="title">'+"There are no listings yet for this account"+'</h3>' +
																	'<div class="meta-actions">'+
																		'<button class="add-listing">Add Listing</button>'+
																	'</div>'+
																'</div>'+
															'</li>';
													$(h).appendTo('.page-listings ul#listings').hide().fadeIn({duration:500, queue: false});
													$('.listing .nolisting .meta-actions > button').on('click', function(){
														seller.initial_listings = null;
														window.location = ah_local.wp+'/new-listing';
													});
												}
											});										
										}});
									}});
								}
							});
						}
					})
				}
				this.edit = function(field, x){
					switch(field){
						case 'price':
							var id = x.id;
							var price = x.price;
							ahtb.open({
								title: 'Edit Price',
								hideSubmit: true,
								width: 350,
								height: 155,
								html: '<p style="text-align:left;"><label for="price">Enter new price:</label><br/>'+
									'<input name="price" type="text" id="price-field" value="$'+price+'"  style="width:99%"/></p>',
								buttons: [
									{text: 'Save', action:function(){ 
										var newPrice = parseInt( $('#tb-submit input').val().replace(/[^0-9]/g, '') );
										ahtb.edit({
											html: '<p>Saving...</p>',
											hideClose: true,
											open: function(){
												seller.DBget({
													query: 'update-listing',
													data: { listing_id:id, fields:{price:newPrice} },
													done: function(d){
														seller.page.listings.info[id].price = newPrice;
														$('.listing[listing-id='+id+'] .meta .meta-actions button.edit-price').attr('price', newPrice);
														$('.listing[listing-id='+id+'] .meta-details .price .value').html('$'+newPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
														ahtb.close();
													}
												});
											}
										});
									}},
									{text: 'Cancel', action:function(){ ahtb.close(); }}
								],
								opened:function(){
									$('#tb-submit input').mask("000,000,000,000,000,000,000,000", {reverse: true});
								}
							});
							break;
						case 'active':
							var id = x.id;
							ahtb.loading('Activating...',{opened:function(){
								seller.DBget({
									query: 'update-listing',
									data: { listing_id:id, fields:{active:1} },
									done: function(d){
										seller.page.listings.info[id].active = 1;
										$('.listing[listing-id='+id+']').removeClass('inactive');
										$('.listing[listing-id='+id+'] li.status .value').html('<span class="active">Active</span>');
										seller.initial_listings =  null;
										window.location = ah_local.wp+'/new-listing/#listingID='+id;
									}
								});
							}});
					}
				}
			}
			this.activity = new function(){
				this.get = function(){
					seller.content({
						title: 'My Activity',
						content: '<ul id="user-activity"><li>Loading user activity...</li></ul>',
						callback: function(){
							seller.DBget({
								title: 'My Activity',
								query: 'get-activity-by-author',
								done: function(d){
									$('#user-activity').empty();
									for (var i in d){
										var a = $.parseJSON(d[i].activity).a;
										a.reverse();
										var h = '';
										for (var j in a){
											if (a[j].field != 'images'){
												var t = new Date(a[j].time);
												t = Math.round(new Date().getTime() / 1000) - t.getTime(); // seconds since update
												h+= '<li>'+
												'<img class="listing-thumb" src="'+ah_local.tp+'/_img/_listings/80x80/'+(d[i].listing && d[i].listing.image ? d[i].listing.image : '_blank.jpg')+'" />'+
												'<div class="info"><strong class="listing-title">'+(d[i].listing.title)
												+'</strong><span class="activity">';
												// console.log(a[j]);
												switch(a[j].field){
													case 'tags':
														var tags = eval(a[j].data);
														var oldTags = eval(a[j].oldData);
														var added = []; var removed = [];
														for (var k in tags){
															found = false;
															for (var l in oldTags){ if (tags[k] == oldTags[l]) { found = true; break; } }
															if (!found) added.push(tags[k]);
														}
														for (var k in oldTags){
															found = false;
															for (var l in tags){ if (oldTags[k] == tags[l]) { found = true; break; } }
															if (!found) removed.push(oldTags[k]);
														}
														if (removed.length > 0) h+= 'Removed tag: '; for (var k in removed){ if (k>0) h+= ', ';  h+= removed[k]; }
														if (added.length > 0) h+= 'Added tag: '; for (var k in added){ if (k>0) h+= ', ';  h+= added[k]; }
														break;
													case 'price':
														h+= 'Updated Price to $'+a[j].data.replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
														break;
													default:
														h+= 'Updated '+a[j].field.charAt(0).toUpperCase()+a[j].field.slice(1)+' to '+a[j].data;
														break;
												}
												h+= '</span><small class="time">';
												if (t < 60) h+= 'less than a minute ago';
												else if (t/60 < 2) h+= 'one minute ago';
												else if (t/60 < 60) h+= Math.round(t/60)+' minutes ago';
												else if (t/3600 < 2) h+= '1 hour ago';
												else if (t/3600 < 24) h+= Math.round(t/3600)+' hours ago';
												else if (t/86400 < 2) h+= 'yesterday';
												else h+= Math.round(t/86400)+' days ago';
												h+= '</small>'+
												'</div></li>';
											}

										}
										$('#user-activity').append(h);
									}
								}
							});
						}
					});
				}
			}
			this.profile = new function(){
				this.get = function(){
					seller.content({
						noFade: true,
						//before: function() { sellerTransitionEnd(); },
						title: 'My Profile',
						callback: function(){
							seller.saving = false;
							if (typeof seller.page.profile.info == 'undefined') {
								ahtb.loading('Loading...',{
									opened: function(){
										seller.DBget({ query: 'get-author', done: function(d){
											seller.sellerData = d;
											seller.page.profile.info = d;
											seller.page.profile.displayProfile();
										}})
									}
								})
							}
							else
								seller.page.profile.displayProfile();
						}
					});
				}
				this.displayProfile = function() {
					var d = seller.page.profile.info;
					seller.setupSellerMetas();						

					if (seller.page.profile.info.associations) {
						// d.associations = $.parseJSON(d.associations);
						for (var i in d.associations) d.associations[i] = parseInt(d.associations[i]);
					}
					if (seller.meta)
						d.contact_email = seller.meta.contact_email;

					if (seller.mod_meta == null) {
						seller.mod_meta = { 'action': SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA,
											'first_name': 0,
											'last_name': 0,
											'company': 0,
											'website': 0,
											'phone': 0,
											'mobile': 0,
											'email': 0};

					}
					// d.accomplishments = d.accomplishments ? $.parseJSON(d.accomplishments) : [];
					// d.social = d.social ? $.parseJSON(d.social) : [];
					seller.lifestyle_count = 0;
					seller.specialty_count = 0;
					d.about = d.about && d.about.length ? d.about.replace(/\\/g,"") : '';
					
					var h = '<div class="lifestyled-agent-alert" style="display:none;"><span class="close">x</span><p>Some basic information is missing to activate your Lifestyled Agent product. Please <a href="javascript:seller.getPage('+"'agent_match_reserve'"+');">click here</a> to finish filling out your lifestyled agent section.<br><br>The information we still need is what a user will see when they view a listing that matches your expertise: A photo and a brief description about what makes you an expert in your area. Once we have this information, you will be activated and show up in appropriate listings.</p></div>'+
					'<div class="topsection"><div class="sellerimg"><form id="dropzone" class="dropzone"></form>'+
						'<div class="seller-photo">'+
							'<img src="'+ah_local.tp+'/_img/_authors/272x272/'+(d.photo != null ? d.photo : '_blank.jpg' )+'" />'+
							'<div class="overlay"><a class="edit-cropping"><p><span class="entypo-popup"></span> Edit Cropping</p></a><a class="upload-photo"><p><span class="entypo-upload-cloud"></span> Upload New</p></a></div>'+
							'<div id="deleteDiv"><button id="delete-photo">Delete photo</button></div>'+
						'</div></div>'+
							
						'<div id="user-data">'+
							'<div class="button-container"><button class="video"><p>Learn </p><span class="videocircle"><span class="entypo-right-dir"></span></span></button></div>' +
							'<div class="form">'+
								'<div class="names">'+
									'<input type="text" id="first_name" name="first_name" placeholder="First Name" value="'+(d.first_name ? d.first_name : '')+'" />'+
									'<input type="text" id="last_name" name="last_name" placeholder="Last Name" value="'+(d.last_name ? d.last_name : '')+'" />'+
								'</div>'+
								'<input type="text" name="company" placeholder="Company Name" value="'+(d.company ? d.company : '')+'" />'+
								'<input type="text" name="website" placeholder="Website" value="'+(d.website ? d.website : '')+'" />'+
								'<input type="text" id="phone" name="phone" placeholder="Main Phone" value="';
							if (d.phone){
								x = d.phone;
								if (x.length == 10) x = '('+x.substr(0, 3)+') '+x.substr(3,3)+'-'+x.substr(6);
								else if (x.length == 11) x = x.substr(0,1)+' ('+x.substr(1,3)+') '+x.substr(4,3)+'-'+x.substr(7);
								h+= x;
							}
							h+= '" />'+
								'<input type="text" name="mobile" placeholder="Mobile Phone" value="';
								if (d.mobile){
									x = d.mobile;
									if (x.length == 10) x = '('+x.substr(0, 3)+') '+x.substr(3,3)+'-'+x.substr(6);
									else if (x.length == 11) x = x.substr(0,1)+' ('+x.substr(1,3)+') '+x.substr(4,3)+'-'+x.substr(7);
									h+= x;
								}
								h+= '" />'+
								'<input type="text" id="email" name="email" placeholder="Name@example.com" value="'+(d.email ? d.email : '')+'" '+( validSeller.fromListhub ? ' disabled' : '')+'/>'+
								'<input type="text" name="contact_email" placeholder="Contact Email" class="contactemail" value="'+(d.contact_email ? d.contact_email : "")+'" '+(d.contact_email == d.email? ' disabled' : '')+'/>'+
								'<span class="sameemail"><input type="checkbox" name="contact_email_same" '+(d.contact_email == d.email ? ' checked' : '')+'/><p class="text">Same as above</p></span>'+
							'</div>'+
							'<div id="buttons"><button id="changepwd">Change Password</button>&nbsp;<button id="viewProfile">View Profile As User</button></div>'+
						'</div></div>'+
						'<div class="main-profile-bg">';
								h+= '<div id="tagline"><label for="tagline" style="text-align:center;font-size:1.4em;line-height:1em;">Tag Line</label><input type="text" name="tagline" placeholder="A tagline about yourself..." maxlength="'+maxTaglineLen+'"></input><span class="characters"><label>Characters Left:</label>&nbsp;<label id="chCount">'+maxTaglineLen+'</label></span></div>';
								h+= '<label for="about" style="text-align:center;font-size:1.4em;line-height:1em;">About Me<span class="required">*required</span></label><textarea name="about" rows="6" placeholder="This is separate from your specialized lifestyle and property type descriptions, and a great place to explain your background, approach to real estate, and what your clients love about you..." style="width:98%;">'+(d.about ? d.about : '')+'</textarea>'+
								'<div class="row">';
								h+= '<div class="business-history">';
								h+= '<label style="text-align: center; font-size: 1.4em;">Business History<span class="required">*required</span></label>';
								h+= '<table class="experience1">';
									h+= '<tr>';
										h+= '<td>';
											var service_area = d.service_areas ? d.service_areas.replace(/"/g, '&quot;') : '';
											service_area = service_area != '' ? service_area.split(":") : [];
											h+= '<div class="experience"><label class="req" for="experience" style="display:inline">When did you receive your Real Estate License?</label>';
											var date = new Date();
											var n = date.getFullYear();
												h+= '<select name="year" id="year" style="display: block; margin: .5em auto 0;">';
													for(var i = -1; i < 50; i++) {
														if (i == -1) // default
															h+= '<option value="'+i.toString()+'" '+(seller.meta && parseInt(seller.meta.year) == i ? 'selected="selected"' : '')+' >Unknown</option>';
														else
															h+= '<option value="'+(n-i).toString()+'" '+(seller.meta && parseInt(seller.meta.year) == (n-i) ? 'selected="selected"' : '')+' >'+(n-i).toString()+'</option>';
							                         }
							                         h+= '<option value="'+(n-i).toString()+'" '+(seller.meta && parseInt(seller.meta.year) <= (n-i) ? 'selected="selected"' : '')+'>Over 50 years</option>';
												h+= '</select>';
											h+= '</div>';
											h+= '<div class="experience"><label class="req" for="homes-sold" style="display:inline">Total transactions in the past 12 months</label>';
												h+= '<select name="sold" id="sold" style="display: block; margin: .5em auto 0;">';
													for(var i = 0; i <= 100; i++) {
														if (i == 0)
															h+= '<option value="'+(i).toString()+'" '+(seller.meta && parseInt(seller.meta.sold) == i ? 'selected="selected"' : '')+' >'+"Unknown"+'</option>';
														else
															h+= '<option value="'+(i).toString()+'" '+(seller.meta && seller.meta.sold == (i).toString() ? 'selected="selected"' : '')+' >'+(i).toString()+(i == 1 ? " Transaction": " Transactions")+'</option>';
							                         }
												h+= '</select>';
											h+= '</div>';
											h+= '<div class="experience"><label class="req" for="years-in-area" style="display:inline">When did you start living in your service area?	</label>';
												h+= '<select name="inArea" id="inArea" style="display: block; margin: .5em auto 0;">';
													for(var i = -1; i < 50; i++) {
														if (i == -1)
															h+= '<option value="'+(i).toString()+'" '+(seller.meta && parseInt(seller.meta.inArea) == i ? 'selected="selected"' : '')+' >Unknown</option>';
														else
															h+= '<option value="'+(n-i).toString()+'" '+(seller.meta && seller.meta.inArea == (n-i).toString() ? 'selected="selected"' : '')+' >'+(n-i).toString()+'</option>';
							                         }
							                         h+= '<option value="'+(n-i).toString()+'" '+(seller.meta && parseInt(seller.meta.inArea) <= (n-i) ? 'selected="selected"' : '')+'>Over 50 years</option>';
								         		h+= '</select>';
												h+= '</div>';
												h+= '<div class="experience"><label for="service_areas">Primary Service Area</label><input type="text" name="service_areas" placeholder="eg: San Francisco, CA" value="'+(service_area.length ? service_area[0] : '')+'" style="width: 30%; margin: .5em auto 0; color: #444; height: 30px; padding: 0 .25em" /></div>';
											h+= '<div class="experience"><label for="service_areas" style="display:inline;">Working Range Around Primary Service Area</label>';
													h+= '<select name="areas" id="areas" style="display: block; margin: .5em auto 0;">' +
								                          	'<option value="8" '+(service_area.length > 1 && parseInt(service_area[1]) == 8 ? 'selected="selected"' : '')+'>8 Miles</option>' +
								                          	'<option value="12" '+(service_area.length > 1 && parseInt(service_area[1]) == 12 ? 'selected="selected"' : '')+'>12 Miles</option>' +
								                          	'<option value="20" '+(service_area.length > 1 && parseInt(service_area[1]) == 20 ? 'selected="selected"' : '')+'>20 Miles</option>' +
								                          	'<option value="30" '+(service_area.length > 1 && parseInt(service_area[1]) == 30 ? 'selected="selected"' : '')+'>30 Miles</option>' +
								         				'</select>';
											h+= '</div>';
										h+= '</td>';
										// h+= '<td>';
										// 	h+= '<div class="specialties right"><label for="specialties">Specialties</label><input type="text" name="specialties" placeholder="eg: Investment Properties" value="'+(d.specialties ? d.specialties.replace(/"/g, '&quot;') : '')+'" /><a><span class="entypo-plus-circled"></span> Add Specialty</a></div>';
										// h+= '</td>';
									h+= '</tr>';
									h+= '</table>'; // experience1
								h+= '</div>';
								h+= '<div class="additional-info">';
									h+= '<label style="text-align: center; font-size: 1.4em;">Additional Information</label>';
									h+= '<table class="experience2">';
										h+= '<tr>';
											h+= '<td style="display: block; width: 100%"><div class="experience"><label for="languages">What Languages can you do business in?</label>';
													h+= '<table class="language" style="text-align: left; display: block; margin: 0 auto; width: 500px; position: relative; left: 45px;">';
													for(var i in Languages) {
														if ( (i%4) == 0)
															h+= '<tr>';
														h+= '<td><div class="language">';
														h+= '<input type="checkbox" name="'+Languages[i]+'" value="'+Languages[i]+'" '+(seller.meta && seller.meta.languages && seller.meta.languages.indexOf(Languages[i]) != -1 ? 'checked' : '')+'/>';
														h+= '<span class="language">'+Languages[i]+'</span></div></td>';
														if ( (i%4) == 3)
															h+= '</tr>';
													}
													if ( (i%4) != 3)
														h+= '</tr>';
													h+= '</table>';
													var other = (seller.meta && length(seller.meta.languages)) ? seller.getOtherLanguage() : '';
													h+= '<div class="other"><input type="checkbox" name="other" value="Other" '+(other.length ? 'checked' : '')+'/><span class="language">Other</span><input type="text" id="other" placeholder="Your Language" value="'+(other.length ? other : '')+'" style="padding:.35em 4em .35em .5em;"/></div>';
											h+= '</div></td>';
										h+= '</tr>';
									h+= '</table>'; //experience2
								h+= '</div>';
										//h+= '</div>';
									//h+= '</tr>';
								//h+= '</table>'; //experience1

								h+= '<div class="associations"><label for="associations" style="font-size:1.4em;text-align:center;">Designations and Certifications</label>';
									h+= '<ul>';
										var assocs = ['areaa','luxuryhome','graduate','rce','pmn','rrealtor','wcr','abr','cips','crb','crs','epro','mcne','rsps','sres','leverage'];
										for (var i in assocs) h+= '<li><input type="checkbox" name="'+assocs[i]+'" '+( (d.associations != null) && (typeof d.associations[assocs[i]] != 'undefined') && d.associations[assocs[i]] == 1 ? 'checked' : '')+'/><img src="'+ah_local.tp+'/_img/_sellers/assoc_'+assocs[i]+'.png" /></li>';
									h+= '</ul>';
								h+= '</div>';

								h+= '<div class="social">';
									h+= '<label for="social" style="text-align:center;font-size:1.4em;"><div>Social Media</div><a>Add New <span class="entypo-plus-circled"></span></a></label>';
									h+= '<ul>';
									for (var i in d.social)
										h+= seller.page.profile.addField('social', {i:i, value:d.social[i].value, type:d.social[i].type });
									h+= '</ul>';
								h+= '</div>'
							h+= '</div>'; //row
							h+= '<div id="more">' +
									'<a href="javascript:;" style="position: static;"><img src="'+ah_local.tp+'/_img/page-home/arrow-down.png"/><span> More</span></a>' +
								'</div>';
							h+= seller.page.profile.makeAgentMatchSection();
							h+= '<button id="submit-seller-profile">Save Profile</button>';
						h+= '</div>'// main-profile-bg
					
					h+= '</div></div>';
					$('.page-profile').html(h);
					$('.video').on('click', function() {
						var h = '<iframe src="//player.vimeo.com/video/'+videoList['sellers-profile']+'" width="100%" height="100%" frameborder="0" title="Allure Homes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
					$('.videoDiv .video').html(h);
						videoControl.showVid();
					})
					$('.page-profile .lifestyled-agent-alert .close').click(function(){
						$('.page-profile .lifestyled-agent-alert').slideToggle('fast');
					});
					$('.page-profile .lifestyled-agent-alert p a').on('click', function() {
						sellerTransitionStart();
					});
					var tagline = (seller.meta && seller.meta.tagline ? seller.meta.tagline : '');
					tagline = tagline.length ? tagline.replace(/\\/g,"") : '';
					$('input[name=tagline]').val(tagline);
					var taglineLeft = maxTaglineLen - tagline.length > 0 ? maxTaglineLen - tagline.length : 0;
					$('#chCount').html( taglineLeft.toString() 	);
					$('input[name=tagline]').on('keyup', function(e) {
						var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
					    if(keynum == 13) {//13 is the enter char code
					      e.preventDefault();
					    }
						var len = maxTaglineLen - $(this).val().length;
						var counter = len > 0 ? len : 0;
						$('#chCount').html(counter.toString());
					})

					// $('#agentMatchSection').hide();

					seller.setupAutocomplete($('.experience input[name=service_areas]'));

					/* Dropzone and seller photo */
					dz = new Dropzone(".sellerimg #dropzone", {
						acceptedFiles:'image/*',
						url: ah_local.tp+'/_classes/Image.class.php',
						maxFiles: 1,
						dictDefaultMessage: '<p>Upload Profile Image</p><span class="entypo-upload-cloud"></span><button>Browse</button>',
						dictFallbackText: 'Please choose your photo',
						headers:{"image_dir":"_authors/uploaded/","gen_sizes":1},
						init: function(){
							setCookie('IMAGE_DIR', '_authors/uploaded/', 1);
							$('.dz-message button').on('click', function(e){e.preventDefault()});
							$('.sellerimg #dropzone').hover(function(){
								$('.dz-message button, .dz-message span span, .dz-message p').addClass('hover');
							},function(){
								$('.dz-message button, .dz-message span span, .dz-message p').removeClass('hover');
							});
							this.on('maxfilesexceeded', function(){ ahtb.alert({html:'<p>You can only upload one image for your profile.</p>'}) });
							this.on('error', function(file, responseText) {
								var x = $.parseJSON(responseText);
							});
							this.on("success", function(file, responseText){
								var x = $.parseJSON(responseText);
								seller.updateAuthorPhoto(x);
							});
						}
					});

					$('div.dz-fallback input[name="file"]').on('change', seller.prepareUpload);
					$('div.dz-fallback input[value="Upload!"]').on('click', seller.upload);

					if (typeof dz.disable != 'function')
						$('.sellerimg #dropzone').append('<div class="spin-wrap"><div class="spinner sphere"></div></div>');


					if (d.photo){ $('.sellerimg #dropzone').hide(); if (typeof dz.disable == 'function') dz.disable(); }
					else $('div.seller-photo').hide();
					$('div.seller-photo .overlay a').on('click', function(){
						if ($(this).hasClass('upload-photo')) {
							seller.page.profile.removePhoto(function() {
								if (typeof dz.enable == 'function') dz.enable();
								$('.seller-photo').fadeOut(250,function(){$('.sellerimg #dropzone').fadeIn(250)});
							})
						}
						else if ($(this).hasClass('edit-cropping')) seller.page.profile.editCrop();
					});
					$('div.seller-photo #delete-photo').on('click', function() {
						d = seller.page.profile.info;
						if (!d.photo) {
							ahtb.alert("No photo on file!");
							return;
						}
						seller.page.profile.removePhoto();
					})
					/* Phone numbers */
					$('[name=mobile], [name=phone]').mask('(999) 000-0000');
					/* Contact Email */
					$('input[name=contact_email_same]').on('click',function(){
						if ( $(this).prop('checked') ){
							$('[name=contact_email]').prop('disabled', true).val(seller.page.profile.info.email);
						} else $('[name=contact_email]').prop('disabled', false).val('');
					});
					/* Social Media */
					$('.social li a').on('click',function(){
						$(this).parent().fadeOut(250, function(){
							$(this).remove()})
					})
					$('.social label a').on('click',function(){
						seller.page.profile.addField('social', null, true);
					});
					/* Accomplishments */
					$('.accomplishments input').on('click',function(){ if ($(this).val().substr(0,5) == 'Accom' || $(this).val().substr(0,5) == 'Sourc') $(this).val(''); });
					/* Save the page */
					$('#submit-seller-profile').on('click', function(e){ e.preventDefault(); seller.page.profile.updateAuthor(); });
					ahtb.close();
					$('#seller-content').fadeIn(250);
					// agent match
					$('#more a').on('click', function() {
						if (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME))
							$('#agentMatchSection').show();
						else
							ahtb.alert("This feature only permitted for Lifestyled Agent", {height: 180 });
					});
					//change password
					$('#changepwd').on('click', function() {
						var h = '<div id="changepwdDiv">' +
									'<table style="margin: 10px 0 10px 10px;">' +
										'<tbody>'+
											'<tr>' +
												'<td><span>Current Password:</span></td><td><input type="password" id="current-pwd" placeholder="Current Password" /></td>' +
											'</tr>' +
											'<tr>' +
												'<td><span>New Password:</span></td><td><input type="password" id="new-pwd" placeholder="Enter New Password" /></td>' +
											'</tr>' +
											'<tr>' +
												'<td><span>Retype Password:</span></td><td><input type="password" id="verify-pwd" placeholder="Retype New Password" /></td>' +
											'</tr>' +
										'</tbody>' +
									'</table>' +
									'<span id="message"></span>';
								'</div>';
						ahtb.open( {
								html: h,
								width: 350,
								height: 185,
								buttons: [
									{text:'OK', action: function() {
										var current = $('#current-pwd').val();
										var newOne = $('#new-pwd').val();
										var verify = $('#verify-pwd').val();
										if (newOne != verify) {
											$('#message').text("Passwords don't match!");
											$('#message').show();
										}
										else {
											seller.DBget({	query:'update-password', 
												data: {newOne: newOne,
													   oldOne: current }, 
												done:function(d){
													ahtb.alert("Password changed!", {height: 150});
												},
												error:function(d){
													ahtb.alert(typeof d != 'undefined' ? d : "Trouble updating password!", {height: 150});
												}
											});
										}
									}},
									{text:'Cancel', action: function() {
										ahtb.close();
									}}
								],
								opened: function() {
									$('#message').hide();
									$('#verify-pwd').keyup(function(e) {
										e.preventDefault();
										$('#message').hide();
									});
									$('#new-pwd').keyup(function(e) {
										e.preventDefault();
										$('#message').hide();
									});
								}
						});
					})

					$('#viewProfile').on('click', function() {
						seller.page.profile.updateAuthor( UpdateAuthorOrigin.ViewAgentPage );
					})

					if ( !(seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) )
						$('#agentMatchSection .desc-popup .description #chCount').hide();

					$('#agentMatchSection input[name=specialties]').on('click', function() {
						var family = $(this).attr('name');
						var value = parseInt($(this).attr('value'));
						var checked = $(this).is(':checked');
						var id = $(this).attr('id');
						console.log("Got "+family+", value: "+value+", checked: "+checked+", id: "+id);
						
						if (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) {
							if (checked) {
								if (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) {
									if (seller.specialty_count == 3) {
										ahtb.alert("Only 3 topics please.", {height: 150 });
										$(this).prop('checked', false);
										return;
									}
								}
							}

							var div = $(this).parent();
							var a = div.children("a.desc");
							if (checked) {									
								a.removeClass('hidden');
								seller.specialty_count++;
								seller.setAMTagState(value, family, 1);
								seller.editingTag = value;
								seller.tagFamily = family;
								var desc = seller.getAMData();
								$('#agentMatchSection .desc-popup .description textarea').val( desc );
								$('#agentMatchSection .desc-popup').removeClass('hidden');
								$('html, body').css('overflow', 'hidden').on('touchmove', function(e) {
									e.preventDefault();
								});
								var len = MIN_LENGTH_DESC - desc.length;
								var counter = len > 0 ? len : 0;
								seller.page.profile.showCounter() ? $('#agentMatchSection .desc-popup .description #counter').show() : $('#agentMatchSection .desc-popup .description #counter').hide();
								$('#agentMatchSection .desc-popup .description #chCount').html(counter.toString());
							}
							else {
								a.addClass('hidden');
								seller.specialty_count--;
								seller.setAMTagState(value, family, 0);
							}
						}
						else { // treat as radio button
							if (seller.specialty_checked == value) {
								$(this).prop('checked', false);
								seller.specialty_checked = -1;
								$('#specialist .buttons a[value='+value+']').addClass('hidden');
							}
							else {
								var div = $(this).parent();
								var a = div.children("a.desc");
								// $(this).prop('checked', true);
								a.removeClass('hidden');
								seller.setAMTagState(value, family, 1);
								seller.editingTag = value;
								seller.tagFamily = family;
								var desc = seller.getAMData();
								$('#agentMatchSection .desc-popup .description textarea').val( desc );
								$('#agentMatchSection .desc-popup').removeClass('hidden');
								$('html, body').css('overflow', 'hidden').on('touchmove', function(e) {
									e.preventDefault();
								});
								var len = MIN_LENGTH_DESC - desc.length;
								var counter = len > 0 ? len : 0;
								seller.page.profile.showCounter() ? $('#agentMatchSection .desc-popup .description #counter').show() : $('#agentMatchSection .desc-popup .description #counter').hide();
								$('#agentMatchSection .desc-popup .description #chCount').html(counter.toString());
								
								if (seller.specialty_checked != -1) {
									$('#specialist .buttons a[value='+seller.specialty_checked+']').addClass('hidden');
									// $('#specialist .buttons a[value='+seller.specialty_checked+']').prop('checked', false);
									seller.setAMTagState(seller.specialty_checked, family, 0);
								}
								seller.specialty_checked = value;
							}
						}
					})

					// $("#agentMatchSection input[type=radio]").on("click", function() {
					//     // $(this).attr("checked",false);
					//     var family = $(this).attr('name');
					// 	var value = $(this).attr('value');
					// 	var checked = $(this).prop('checked');
					// 	var id = $(this).attr('id');
					// 	console.log("Reclick "+family+", value: "+value+", checked: "+checked+", id: "+id);
						
					// });

					$('#agentMatchSection .desc-popup .description textarea').on('keyup', function(e) {
						var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
					    if(keynum == 13) {//13 is the enter char code
					      e.preventDefault();
					    }
						var len = MIN_LENGTH_DESC - $(this).val().length;
						var counter = len > 0 ? len : 0;
						$('#agentMatchSection .desc-popup .description #chCount').html(counter.toString());
					})

					$('#agentMatchSection input[name=lifestyles]').on('change', function() {
						var family = $(this).attr('name');
						var value = $(this).attr('value');
						var checked = $(this).prop('checked');
						var id = $(this).attr('id');
						console.log("Got "+family+", value: "+value+", checked: "+checked+", id: "+id);

						if (checked) {
							if (seller.lifestyle_count == 3) {
								ahtb.alert("Only 3 topics please.", {height: 150 });
								$(this).prop('checked', false);
								return;
							}
						}
						
						var div = $(this).parent();
						var a = div.children("a.desc");
						if (checked) {									
							a.removeClass('hidden');
							seller.lifestyle_count++;
							seller.setAMTagState(value, family, 1);
							seller.editingTag = value;

							seller.tagFamily = family;
							var desc = seller.getAMData();
							$('#agentMatchSection .desc-popup .description textarea').val( desc );
							$('#agentMatchSection .desc-popup').removeClass('hidden');
							$('html, body').css('overflow', 'hidden').on('touchmove', function(e) {
								e.preventDefault();
							});
							var len = MIN_LENGTH_DESC - desc.length;
							var counter = len > 0 ? len : 0;
							seller.page.profile.showCounter() ? $('#agentMatchSection .desc-popup .description #counter').show() : $('#agentMatchSection .desc-popup .description #counter').hide();
							$('#agentMatchSection .desc-popup .description #chCount').html(counter.toString());
						}
						else {
							a.addClass('hidden');
							seller.lifestyle_count--;
							seller.setAMTagState(value, family, 0);
						}						
					});

					$('#agentMatchSection a').on('click', function() {
						var value = $(this).attr('value');
						var family = $(this).attr('name');
						seller.editingTag = value;
						seller.tagFamily = family;
						var desc = seller.getAMData();
						$('#agentMatchSection .desc-popup .description textarea').val( desc );
						$('#agentMatchSection .desc-popup').removeClass('hidden');
						$('html, body').css('overflow', 'hidden').on('touchmove', function(e) {
							e.preventDefault();
						});
						var len = MIN_LENGTH_DESC - desc.length;
						var counter = len > 0 ? len : 0;
						seller.page.profile.showCounter() ? $('#agentMatchSection .desc-popup .description #counter').show() : $('#agentMatchSection .desc-popup .description #counter').hide();
						$('#agentMatchSection .desc-popup .description #chCount').html(counter.toString());
					})

					$('#agentMatchSection .desc-popup .description button').on('click', function(){
						var value = $(this).attr('value');
						
						if (value == 'Save') {
							if ( seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME) ) {
								for(var i in seller.am_order.item) {
									if (seller.am_order.item[i].action == AgentOrderMode.ORDER_AGENT_MATCH &&
										seller.am_order.item[i].specialty == seller.editingTag &&
										seller.am_order.item[i].mode == AgentOrderMode.ORDER_BOUGHT &&
										$('#agentMatchSection .desc-popup .description textarea').val().trim().length < MIN_LENGTH_DESC) {
										ahtb.alert("Please enter at least "+MIN_LENGTH_DESC+" character description.");
										return;
									}
								}
							}
							console.log("Save this thing!");
							if ($('#agentMatchSection .desc-popup .description textarea').val().trim() != '')
								seller.saveAMData($('#agentMatchSection .desc-popup .description textarea').val());
						}
						$('#agentMatchSection .desc-popup').addClass('hidden');
						$('html, body').css('overflow','').off('touchmove');
						$('html, body').css('position','relative');
						if (isMobile)
							$(document).scrollTop(4400);
					})
				}

				this.removePhoto = function(callback) {
					var d = seller.page.profile.info;
					seller.DBget({	query:'remove-photo', 
									data:{ id: d.id,
									   	   name: d.photo 
									},
									done:function(data){
										console.log(typeof data == 'string' ? data : "Deleted photo"); 
										$('div.seller-photo').hide();
									  	if (typeof dz.files != 'undefined' &&
									  		dz.files.length)
									  		if (typeof dz.removeFile == 'function') dz.removeFile(dz.files[0]);
									  	$('.sellerimg #dropzone').show(); if (typeof dz.enable == 'function') dz.enable();
									  	d.photo = null;
									  	d.flags |= SellerFlags.SELLER_NEEDS_BASIC_INFO;
									  	if (typeof callback == 'function')
									  		callback();
									},
									error:function(x) {
										var msg = typeof x == 'string' ? x :
												  typeof x == 'object' ? JSON.stringify(x) :
												  "Unknown reason";
										ahtb.alert("Failure removing photo: "+msg);
									}
							}); 
				}

				this.showCounter = function() {
					if (typeof seller.am_order == 'undefined' ||
						seller.am_order == null)
						return false;

					if ( seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME) ) {
						for(var i in seller.am_order.item) {
							if (seller.am_order.item[i].specialty == seller.editingTag &&
								(seller.am_order.item[i].mode == AgentOrderMode.ORDER_BOUGHT ||
								 seller.am_order.item[i].mode == AgentOrderMode.ORDER_BUYING) )
								return true;
						}
					}
					return false;
				}
				this.makeAgentMatchSection = function() {
					var h = '<div class="line"></div>' +
							'<section id="agentMatchSection">' +
								'<div class="desc-popup hidden">' +
									'<div class="bg-dim"></div>' +
									'<div class="description">' +
										'<div class="title">What makes you an expert or specialist in this area?</div>' +
										'<textarea id="description" placeholder="Please enter detailed information that showcases your strength for this selection.  For example: Why do you consider yourself an expert/specialist?  How long have you done this?  Do you have any specialized certifcations or won awards?  What makes you stand out from the rest, in your area, that will make a potential buyer want to talk to you about this?"></textarea>' +
										'<div class="buttons">' +
											'<button id="save" value="Save">Save</button><button id="cancel" value="Cancel">Cancel</button>' +
										'</div>' +
										'<div id="counter"><label>Minimum Characters Needed:</label>&nbsp;<label id="chCount">300</label></div>' +
									'</div>' +
								'</div>' +
								'<div class="section" id="section-description">' +
									'<span>Optional information to display on your profile page to the buyer.  Please add information in the sections below that makes you unique.</span>' +
								'</div>' +
								'<div class="section" id="specialist" >'+
									'<div class="agent-match-header">' +
										'<div class="title" >' +
											'<span class="main">Property Type Specialist</span>';
										if (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME))
											h+= '<span class="desc">Please select up to 3 property types you are a specialist in.  If none apply to you, please skip.</span>';
										else
											h+= '<span class="desc">Please select 1 property type you are a specialist in.  If none apply to you, please skip.</span>';
									h+= '</div>' +
										//'<div class="lifestyledimg"><img src="'+ah_local.tp+'/_img/_sellers/lifestyled-agent.png"/><p>Lifestyled Agent</p></div>' +
										'<div class="buttons">';
										for(var i in seller.am_meta.specialties) {
											var tag = seller.am_meta.specialties[i];
											var slug = tag.tag.replace(' ','_');
											if (tag.used == "1" ||
												tag.used == 1) {
												if ( !(seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) &&
													 seller.specialty_checked != -1)
													seller.am_meta.specialties[i].used = 0;
												else
													seller.specialty_checked = tag.id;
												seller.specialty_count++;
											}
											h+= makeSelector(slug, tag.tag, (seller.page.profile.info.flags & (SellerFlags.SELLER_IS_PREMIUM_LEVEL_2 | SellerFlags.SELLER_IS_LIFETIME)) ? 'checkbox' : 'radio', 'specialties', tag.id, parseInt(tag.used));
										}
						h+=				'</div>' +
									'</div>'+
								'</div>' +
								'<div class="section" id="lifestyle" >'+
									'<div class="agent-match-header">' +
										'<div class="title" >' +
											'<span class="main">What Lifestyle Are You An Expert In?</span>' +
											'<span class="desc">Select up to three topics you are knowledgable about in your area, and write a brief descripton.</span>' +
										'</div>' +
										//'<div class="lifestyledimg"><img src="'+ah_local.tp+'/_img/_sellers/lifestyled-agent.png"/><p>Lifestyled Agent</p></div>' +
										'<div class="buttons">';
										for(var i in seller.am_meta.lifestyles) {
											var tag = seller.am_meta.lifestyles[i];
											if (tag.id == 44)
												tag.tag = "Aviation";
											var slug = tag.tag.replace(' ','_');
											if (tag.used == "1" ||
												tag.used == 1)
												seller.lifestyle_count++;
											h+= makeSelector(slug, tag.tag, 'checkbox', 'lifestyles', tag.id, parseInt(tag.used));
										}
						h+=				'</div>' +
									'</div>'+
								'</div>' +
							'</section>';

					return h;
				}
				this.updateAuthor = function(calledFrom, callback){
					var meta = {};
					meta.action = SellerMetaFlags.SELLER_PROFILE_DATA;
					calledFrom = typeof calledFrom == 'undefined' ? UpdateAuthorOrigin.Normal : calledFrom;
					
					if (seller.saving == false){
						var san = {};
					 	san.data = this.sanitizeInput(meta);
						if (this.needsToSave(san, meta)) {
							if (calledFrom == UpdateAuthorOrigin.Exit)
								this.saveToDb(san, calledFrom, callback);
							else {
								seller.saving = true;
								ahtb.loading(calledFrom != UpdateAuthorOrigin.ViewAgentPage ? 'Saving your profile...' : 'Switching to User View...', {
									opened:function(){
										seller.page.profile.saveToDb(san, calledFrom, callback);
									}
								});		
							}						
						}
						else if (calledFrom == UpdateAuthorOrigin.ViewAgentPage) {
							seller.queuedPage = '';
							window.location = ah_local.wp+"/agent/"+validSeller.author_id;
						}
						else if (calledFrom == UpdateAuthorOrigin.Normal) {
							ahtb.open({html: "Nothing to save.", 
									   width: 400,
									   height: 150,
									   buttons:[{text:'OK', action: function() {
									   	if (typeof callback != 'undefined')
									   		callback();
									   }}]
									});
						}
						else if (typeof callback != 'undefined')
							callback();
					}
				}	

				this.saveToDb = function(san, calledFrom, callback) {
					seller.saving = true;
					seller.DBget({	query:'update-author', 
									data: {id: validSeller.id,
										   fields: san.data}, 
									done:function(d){
										for (var i in san.data) seller.page.profile.info[i] = san.data[i];
										// if (san.data.first_name != $('.header-login .name').html()) $('.header-login .name').html(data.first_name);
										seller.saving=false;
										seller.setupSellerMetas();	
										
										validSeller = d.seller;
										dashboardCards = d.dashboardCards;

										seller.initial_listings = validSeller.listings;
										seller.updateMyListingsButton();

										if (calledFrom == UpdateAuthorOrigin.ViewAgentPage)
											window.location = ah_local.wp+"/agent/"+validSeller.author_id;
										else if (calledFrom != UpdateAuthorOrigin.PortalMessage &&
												 calledFrom != UpdateAuthorOrigin.Dashboard)
											ahtb.close();

										if (typeof callback != 'undefined') 
											callback();
										
									},
									error:function(x) {
										var msg = typeof x == 'string' ? x :
												  typeof x == 'object' ? JSON.stringify(x) :
												  "Unknown reason";
										ahtb.alert("Failure saving profile: "+msg);

										if (typeof callback != 'undefined') 
											callback();
									}
								});
				}

				this.sanitizeInput = function(meta){ // gets all the data from the form in the page
					var data = {};
					var vars = ['company','website','phone','mobile','email','about','service_areas','associations','contact_email','tagline'];
					for (var i in vars){
						if (vars[i] == 'phone' || vars[i] == 'mobile') data[vars[i]] = $('[name='+vars[i]+']').val().replace(/[^0-9]/g, '');
						else if (vars[i] == 'contact_email' ||
							     vars[i] == 'tagline') {
							meta[vars[i]] = ($.trim($('[name='+vars[i]+']').val()).length < 1) ? "" : $('[name='+vars[i]+']').val();
						}
						else if (vars[i] == 'service_areas')
							data[vars[i]] = $('[name='+vars[i]+']').val()+":"+$('select[name="areas"] option').filter(':selected').attr('value');
						else if (vars[i] == 'associations') {
							data[vars[i]] = {};
							$.each($('.associations input'), function() {
								data[vars[i]][$(this).attr('name')] = $(this).prop('checked') ? 1 : 0;
							})
						}
						else data[vars[i]] = $('[name='+vars[i]+']').val();
					}
					vars = ['first_name','last_name','year','sold','inArea'];
					for (var i in vars){
						if (vars[i] == 'first_name' || vars[i] == 'last_name')
							meta[vars[i]] = $('[name='+vars[i]+']').val();
						else
							meta[vars[i]] = $('select[name='+vars[i]+'] option').filter(':selected').attr('value');
					}
					// now do language check boxes
					meta.languages = [];
					$.each( $('table.language input'), function() {
						if ($(this).prop('checked')) {
							var val = $(this).attr('value'); 
							meta.languages[meta.languages.length] = val;
						}
					});
					if ($('.other input[type="checkbox"]').prop('checked') &&
						$('.other #other').val() != '')
						meta.languages[meta.languages.length] = $('.other #other').val();

					data.accomplishments = [];
					$('.accomplishments li').each(function(i){
						var quote = $.trim($(this).children('input:first-child').val());
						var source = $.trim($(this).children('input:last-child').val());
						if (quote.substr(0, 20) == "Accomplishment Quote") quote = null;
						if (source == "Source 1" || source == "Source 2" || source == "Source 3") source = null;
						if (quote.length > 0) data.accomplishments.push({quote: quote.length>0?quote:null, source: source&&source.length>0?source:null });
					});
					data.associations = {};
					$('.associations input').each(function(){data.associations[$(this).attr('name')] = $(this).prop('checked') ? 1 : 0;});
					data.social = [];
					$('.social li').each(function(){
						var val = $.trim($(this).children('input').val());
						if (val.length > 0) data.social.push({value: val, type: $(this).children('select').val() });
					});
					return data;
				}
				this.needsToSave = function(san, meta){ // data verification, returns boolean
					var data = san.data;
					newData = {};
					var needsToSave = false;
					var needModMetaSaved = false;;
					var oldData = seller.page.profile.info;
					for (var i in data){
						var dataChanged = false;
						if ( (data[i] && data[i] != oldData[i]) ||
							 (!data[i] && oldData[i]) ){
							if (oldData[i]  && typeof data[i] == 'object' && !arrayIsSame(obj2array(data[i]), obj2array(oldData[i])) ) {
								needsToSave = true; newData[i] = data[i]; dataChanged = true;
							} else if (typeof data[i] != 'object' || (oldData[i] == null && length(data[i])) ) {
								needsToSave = true; newData[i] = data[i];  dataChanged = true;
							} else if (!arrayIsSame(data[i], oldData[i]) && length(data[i])) {
								needsToSave = true; newData[i] = data[i];  dataChanged = true;
							}
							else if (!data[i] && oldData[i]) {
								needsToSave = true; newData[i] = data[i];  dataChanged = true;
							}
							if (dataChanged) {
								if (typeof seller.mod_meta[i] == 'undefined')
									seller.mod_meta[i] = 1;
								needModMetaSaved = true;
								seller.mod_meta[i]++;							
							}
						}
					}

					var needPDMetaSaved = false;
					var firstNameDiff = (typeof seller.meta == 'undefined' ||
										 seller.meta == null ||
										 typeof seller.meta.first_name == 'undefined' ||
						 				 seller.meta.first_name != meta.first_name);
					var lastNameDiff  = (typeof seller.meta == 'undefined' ||
										 seller.meta == null ||
										 typeof seller.meta.last_name == 'undefined' ||
						 				 seller.meta.last_name != meta.last_name);
					if (typeof seller.meta == 'undefined' ||
						seller.meta == null ||
						seller.meta.year != meta.year ||
						seller.meta.sold != meta.sold ||
						seller.meta.inArea != meta.inArea ||
						typeof seller.meta.contact_email == 'undefined' ||
						seller.meta.contact_email != meta.contact_email ||
						typeof seller.meta.tagline == 'undefined' ||
						seller.meta.tagline != meta.tagline ||
						(typeof seller.meta.languages != 'undefined' &&
						!arrayIsSame(seller.meta.languages, meta.languages)) ||
						firstNameDiff || 
						lastNameDiff ) {
						// now pass the data over to the seller's info
						var metas = [];
						for (var i in seller.page.profile.info.meta)
							if (seller.page.profile.info.meta[i].action != SellerMetaFlags.SELLER_PROFILE_DATA)
								metas[metas.length] = seller.page.profile.info.meta[i];
						metas[metas.length] = meta;
						newData.meta =  metas;
						seller.page.profile.info.meta = metas; // for next meta check
						needPDMetaSaved = true;
						if (firstNameDiff && 
							!seller.mod_meta['first_name']) {
						 	seller.mod_meta['first_name'] = 1;
						 	needModMetaSaved = true;
						}
						if (lastNameDiff &&
							!seller.mod_meta['last_name']) {
						 	seller.mod_meta['last_name'] = 1;
						 	needModMetaSaved = true;
						}
					}

					var metas = [];
					var needAmSaved = false;
					var haveModMeta = false;
					for (var i in seller.page.profile.info.meta) {
						if (seller.page.profile.info.meta[i].action == SellerMetaFlags.SELLER_MODIFIED_PROFILE_DATA) {
							haveModMeta = true;
							 if (!needModMetaSaved) 
							 	metas[metas.length] = seller.page.profile.info.meta[i];
							 else
							 	metas[metas.length] = seller.mod_meta;
						}							
						else if (seller.page.profile.info.meta[i].action == SellerMetaFlags.SELLER_AGENT_MATCH_DATA) {
							for(var j in seller.am_meta.specialties) {
								var edited = seller.am_meta.specialties[j].used;
								var existing = seller.page.profile.info.meta[i].specialties[j].used;
								if (edited != existing ) {
									needAmSaved = true;
									break;
								}
								if (seller.am_meta.specialties[j].desc != seller.page.profile.info.meta[i].specialties[j].desc ) {
									needAmSaved = true;
									break;
								}
							}
							for(var j in seller.am_meta.lifestyles) {
								if (needAmSaved)
									break;
								if (seller.am_meta.lifestyles[j].used != seller.page.profile.info.meta[i].lifestyles[j].used ) {
									needAmSaved = true;
									break;
								}
								if (seller.am_meta.lifestyles[j].desc != seller.page.profile.info.meta[i].lifestyles[j].desc ) {
									needAmSaved = true;
									break;
								}
							}
							if (!needAmSaved)
								metas[metas.length] = seller.page.profile.info.meta[i]; // restore
						}
						else
							metas[metas.length] = seller.page.profile.info.meta[i];
					}

					if (!haveModMeta) {
						metas[metas.length] = seller.mod_meta;
					}

					if (needAmSaved) {
						metas[metas.length] = seller.am_meta;
					}

					if (!haveModMeta || needAmSaved || needPDMetaSaved || needModMetaSaved) {
						newData.meta = metas;
						needsToSave = true;
					}

					san.data = newData;

					return needsToSave; // boolean
				}
				this.addField = function(type, data, print){
					switch(type){
						case 'social':
							var socials = ['Facebook','LinkedIn','Google+','Twitter'];
							var h = '<li social="'+(data ? data.i : $('.social ul li').length)+'"><input type="text" placeholder="Please enter the URL to your social page." value="'+(data ? data.value : '')+'" /><select>';
							for (var i in socials) h+= '<option value="'+socials[i]+'"'+(data && data.type == socials[i] ? ' selected' : (i == 0 ? 'selected' : ''))+'>'+socials[i]+'</option>';
							h+= '</select><a class="entypo-cancel"></a></li>';
							if (!print) return h;
							else {
								$(h).prependTo('.social ul').hide().fadeIn(250);
								$('li[social='+(data ? data.i : $('.social ul li').length-1)+'] a').on('click',function(){
									$(this).parent().fadeOut(250, function(){$(this).remove()});
								});
							}
							break;
					}
				}
				this.applyCrop = function(x){
					ahtb.open({ html: '<p>Cropping image...</p>', 
								height: 150, 
								width: 450, 
								showClose: false,
								opened: function() {
								$.post(ah_local.tp+'/_classes/Image.class.php', 
									{query:'resize',
									 data:{
										file: seller.page.profile.info.photo,
										src_path: '_authors/uploaded/',
										src_width: x.srcBounds[0], src_height: x.srcBounds[1],
										dst_path: '_authors/cropped/',
										dst_x: x.dstBounds.x, dst_y: x.dstBounds.y,
										dst_x2: x.dstBounds.x2, dst_y2: x.dstBounds.y2
									}},function(){},'json')
								.done(function(d){
									if (d.status == 'OK'){
										ahtb.edit({ html: '<p>Saving image...</p>' });
										seller.DBget({query:'update-author', 
													  data:{id: validSeller.id,
													  		fields: {photo:d.data}}, 
													  done:function(){
														seller.page.profile.info.photo = d.data;
														// ahtb.close();
														seller.saving=false;
														seller.page.profile.cropImg = null;
														var toLoad = ['/55x55/'+x.data, '/420x260/'+d.data];
														ahtb.edit({html:'<p>Loading images...</p>'});
														var loaded = 0;
														var img = new Image();
														img.onload = function(){ loaded++; $('.seller-photo img').attr('src', ah_local.tp+'/_img/_authors/272x272/'+d.data); if (loaded == 2) ahtb.close(); }
														img.src = ah_local.tp+'/_img/_authors/272x272/'+d.data;
														var imgProf = new Image();
														imgProf.onload = function(){ loaded++; $('.header-login img.profile-picture').attr('src', ah_local.tp+'/_img/_authors/55x55/'+d.data); if (loaded == 2) ahtb.close(); }
														imgProf.src = ah_local.tp+'/_img/_authors/55x55/'+d.data;
													  },
													  error:function(x) {
														var msg = typeof x == 'string' ? x :
																  typeof x == 'object' ? JSON.stringify(x) :
																  "Unknown reason";
														ahtb.alert("Failure saving image: "+msg);
													  }
													});
									}
									else {
										console.log(x);
										ahtb.close();
									}
								})
								.error(function(d){
									ahtb.alert(d);
								})
							}
						})
				}
				this.printCrop = function(){
					img = seller.page.profile.cropImg;
					var margin = 150;
					var i = 720;
					while (i*img.height/img.width > 500) i-=10;
					var x = { height: (i*img.height/img.width), width: i };
					ahtb.edit({
						width: x.width,
						height: x.height+margin,
						html: 'Please adjust the cropping for your photo, then click save.<img src="'+img.src+'" />'+
							'<button class="save">Save</button><button class="cancel">Cancel</button></div>',
						opened: function(){
							ahtb.showClose(100);
							$('#tb-submit img').Jcrop({
								aspectRatio: 1.0,
								bgFade: true,
								bgOpacity: .3,
								setSelect: [ 0, 0, 10, 360 ],
								boxHeight: x.height,
								boxWidth: x.width
							},function(){ seller.page.profile.jcrop = this; });
							$('#tb-submit button').on('click',function(e){
								e.preventDefault();
								if ($(this).hasClass('cancel')) ahtb.close();
								else if ($(this).hasClass('save')){
									var dstBounds = seller.page.profile.jcrop.tellSelect();
									if (dstBounds.w == 0 || dstBounds.h == 0) ahtb.edit({
										html: '<p>Please select an area to crop before hitting save.</p><button>OK</button>',
										width: 450, height: 155,
										opened: function(){
											$('#tb-submit button').on('click',function(e){ e.preventDefault(); seller.page.profile.printCrop(); });
										}
									});
									else seller.page.profile.applyCrop({ dstBounds: dstBounds, srcBounds: seller.page.profile.jcrop.getBounds() });
								}
							});
						}
					});
				}
				this.editCrop = function(){
					ahtb.loading('Loading full size image...',{
						opened: function(){
							var img = new Image();
							img.src =ah_local.tp+'/_img/_authors/272x272/'+seller.page.profile.info.photo;
							img.onerror = function(){ ahtb.alert('<p>Sorry, an error occurred while loading your image.</p>', {width: 500, height: 180}); }
							img.onload = function(x){ seller.page.profile.cropImg = img; seller.page.profile.printCrop(); }
							img.src = ah_local.tp+'/_img/_authors/uploaded/'+seller.page.profile.info.photo;
						}
					});
				}
			}
		}
		this.listings = new function(){
			this.updateFields = function(id, fields){
				var l = this.getByID(id);
				for (var i in fields) l[i] = fields[i];
				this.setByID(id, l);
			}
			this.setByID = function(id, listing){
				var l = null, found = false;
				if (this.db) for (var i in this.db){
					if (this.db[i].id == id){ this.db[i] = listing; found = true; break; }
				}
				return found ? true : false;
			}
			this.getByID = function(id){
				var l = null, found = false;
				if (this.db) for (var i in this.db){
					if (this.db[i].id == id){ l = this.db[i]; found = true; break; }
				}
				return found ? l : false;
			}
			this.removeByID = function(id){
				var found = false;
				if (this.db) for (var i in this.db){
					if (this.db[i].id == id){ this.db.splice(i,1); found = true; break; }
				}
				return found ? true : false;
			}
		}
		this.checkAuthorImages = function(){
			$.ajax({ type: "POST", dataType: 'json', url: ah_local.tp+'/_admin/image.php', data: { query: 'check-images-authors' },
			  success: function(d){ console.log(d); },
			});
		}
		this.content = function(x){
			if (typeof x.callback === 'undefined') x.callback = function(){};
			document.title = x.title ? x.title+' \u2039 '+seller.oldTitle : seller.oldTitle;
			$('#seller-content').fadeOut({duration:250,
										  start: function() { sellerTransitionStart(); },
										  done: function(){
				$('section.ecommerce').addClass('hidden');
				var h = '<div class="page-'+seller.currentPage+'">';
				h+= x.content?x.content:''+'</div>';
				$('#seller-content').html(h);
				if (x.before) x.before();
				if (x.noFade) {
				 	x.callback();
				 	if (agentPageOption.showFindCompanyButton)
						seller.page.portal.showLogo();
		
				} 
				else
					$('#seller-content').fadeIn({duration:50,
											   //start: function() { $('.loading-overlay-sellers').hide(); },
											     done: function(){
											     	x.callback();
											     	if (agentPageOption.showFindCompanyButton)
														seller.page.portal.showLogo();
											     }});
				sellerTransitionEnd();
			}});
		}
		this.DBget = function(d){
			if(typeof d.query !== 'undefined'){
				var h = [];
				if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
				if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
				// if (typeof d.fail === 'undefined') h.fail = function(){}; else h.fail = d.fail;
				if (typeof d.error !== 'undefined') h.error = d.error;
				if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
				var url = typeof d.url != 'undefined' ? d.url : ah_local.tp+'/_sellers/_ajax.php';
				$.post(url, h.headers, function(){h.before()},'json')
					.done(function(x){
						seller.saving = false;
						if (x.status == 'OK') h.done(x.data);
						else if (h.error) h.error(x.data);
						else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
					})
					.fail(function(x){
						seller.saving = false;
						if (h.fail) h.fail(x);
						if (h.error) h.error(x);
						else ahtb.alert("Database failure, please retry.", {height: 150});
					});
			} else ahtb.alert('Query was undefined');
		}
	}
	seller.init();

	controller = seller;
});