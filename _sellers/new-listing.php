<?php
require_once(__DIR__.'/../_classes/Tags.class.php');
require_once(__DIR__.'/../_classes/TagsTaxonomy.class.php');
require_once(__DIR__.'/../_classes/TagsCategories.class.php');
$Tags = new AH\Tags;
$TagsTaxonomy = new AH\TagsTaxonomy;
$TagsCategories = new AH\TagsCategories;
$Tags = $Tags->get((object)[ 'where' => [ 'type' => 0 ] ]);
$TagsCategories = $TagsCategories->get();
$TagsTaxonomy = $TagsTaxonomy->get((object)[ 'what' => [ 'tag_id', 'category_id' ],  'where' => [ 'tag_id' => wp_list_pluck( $Tags, 'id' ) ] ]);
$listingTags = new stdClass();

$cats = [];
foreach ($TagsCategories as $tag_cat){
	$cats[$tag_cat->id] = (object)[
		'category' => $tag_cat->category,
		'description' => $tag_cat->description,
		'parent' => $tag_cat->parent,
	];
}

if (!empty($Tags))
	foreach ($Tags as $row){
		$id = intval($row->id);
		$cat_id = null;
		if (!empty($TagsTaxonomy))
			foreach($TagsTaxonomy as $tax_row){
				if ($tax_row->tag_id === $id){
					$cat_id = $tax_row->category_id;
					break;
				}
			}

		$listingTags->$id = (object)[
			'tag' => $row->tag,
			'description' => $row->description,
			'category_id' => $cat_id,
		];
	}
?>
<div id="listing-admin">
	<header>
		<a href="<?php bloginfo('wpurl'); ?>" class="logo"></a>
		<ul>
			<li><a href="<?php bloginfo('wpurl'); ?>/sellers/" class="back-to-profile"><span class="entypo-left"></span> Back to Your Profile</a></li>
			<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
			<li><a class="nav-link" data-page="images">Images</a> |</li>
			<li><a class="nav-link" data-page="info">Main Info</a> |</li>
			<li><a class="nav-link" data-page="about">About</a> |</li>
			<li><a class="nav-link" data-page="tags">Tags</a> |</li>
			<li><a class="nav-link" data-page="video">Video</a></li>
			<!-- <li><a class="nav-link link-checkout" data-page="checkout">Checkout</a></li> -->
		</ul>
		<div id="progress-bar">
			<div class="percent"></div>
			<div class="bar"><div class="fill"></div></div>
		</div>
	</header>
	<div id="content-background"></div>
	<section id="page-content"><h2 class="loading">Loading...</h2></section>
	<footer>
		<div class="controls">
			<button class="control back"><span class="entypo-left"> Back</span></button>
			<button class="control arrange">Photos <span class="entypo-shuffle"></span></button>
			<button class="control next">Next <span class="entypo-right"></span></button>
			<button class="control start">Start <span class="entypo-right"></span></button>
			<button class="control done">Finish <span class="entypo-right"></span></button>
		</div>
		<div class="footnote"><span id="message">* Upload times will vary depending on connection speed and file size. If you experience load times slower than 2 seconds per MB, please consider waiting until you have a faster internet connection</span></div>
	</footer>
</div>
<script src="<?php bloginfo('stylesheet_directory'); ?>/_js/states.js" type="text/javascript"></script>
<script>
var us_states = {'Alabama': 'AL', 'Alaska': 'AK', 'Arizona': 'AZ', 'Arkansas': 'AR', 'California': 'CA', 'Colorado': 'CO', 'Connecticut': 'CT', 'Delaware': 'DE', 'Florida': 'FL', 'Georgia': 'GA', 'Hawaii': 'HI', 'Idaho': 'ID', 'Illinois': 'IL', 'Indiana': 'IN', 'Iowa': 'IA', 'Kansas': 'KS', 'Kentucky': 'KY', 'Louisiana': 'LA', 'Maine': 'ME', 'Maryland': 'MD', 'Massachusetts': 'MA', 'Michigan': 'MI', 'Minnesota': 'MN', 'Mississippi': 'MS', 'Missouri': 'MO', 'Montana': 'MT', 'Nebraska': 'NE', 'Nevada': 'NV', 'New Hampshire': 'NH', 'New Jersey': 'NJ', 'New Mexico': 'NM', 'New York': 'NY', 'North Carolina': 'NC', 'North Dakota': 'ND', 'Ohio': 'OH', 'Oklahoma': 'OK', 'Oregon': 'OR', 'Pennsylvania': 'PA', 'Rhode Island': 'RI', 'South Carolina': 'SC', 'South Dakota': 'SD', 'Tennessee': 'TN', 'Texas': 'TX', 'Utah': 'UT', 'Vermont': 'VT', 'Virginia': 'VA', 'Washington': 'WA', 'West Virginia': 'WV', 'Wisconsin': 'WI', 'Wyoming': 'WY', 'American Samoa': 'AS', 'Washington, D.C.': 'DC', 'Federated States of Micronesia': 'FM', 'Guam': 'GU', 'Marshall Islands': 'MH', 'Northern Mariana Islands': 'MP', 'Palau': 'PW', 'Puerto Rico': 'PR', 'Virgin Islands': 'VI', 'Armed Forces Africa': 'AE', 'Armed Forces Americas': 'AA', 'Armed Forces Canada': 'AE', 'Armed Forces Europe': 'AE', 'Armed Forces Middle East': 'AE', 'Armed Forces Pacific': 'AP'};
var listing_tags = <?php echo json_encode($listingTags); ?>;
var tag_cats = <?php echo json_encode($cats); ?>;
</script>