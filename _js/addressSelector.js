var controller = null;
var addresses = []
var myAddresses = [];
var listing_id = 0;
var listing_address = '';
var addressAutoComplete = null;
var addressRequestCount = 0;
var getAddressCall = null;

function updateMyAddresses(fromGet) {
  myAddresses = [];
  for(var i in addresses)
    myAddresses[i] = {label: addresses[i].street_address+', '+addresses[i].city+' '+addresses[i].state,
                      value: addresses[i].id};
  if (addressAutoComplete &&
      typeof fromGet != 'undefined') {
    var doSearch = true;
    if ( controller &&
         typeof controller.class == 'function') {
      switch(controller.class()) {
        case 'quiz':
          if (controller.getMode() != QuizType.QUIZ_BY_ADDRESS)
            doSearch = false;
          break;
      }
    }
    if (doSearch)
      addressAutoComplete.autocomplete('search', addressAutoComplete.val());
  }
}

function setupAddressAutocomplete(element) {
	if (!controller)
		return;

  words = 0;

  element.val('');

  updateMyAddresses();

  addressAutoComplete = element;
  
	element.autocomplete({
    //source: cities,
    source: function( request, response ) {
          var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
          response( $.grep( myAddresses, function( item ){
              var match = matcher.test( item.label );
              return match;
          }) );
      },
    select: function(e, ui){ 
      if (ui.item.value == null) {
      	for(var i in addresses)
      		if (ui.item.label == (addresses[i].street_address+', '+addresses[i].city+' '+addresses[i].state)) {
      			ui.item.value = addresses[i].id;
      			break;
      		}
      }
      element.attr('listing_id', ui.item.value); 
      element.attr('listing_address', ui.item.label); 
      listing_address = ui.item.label;
      listing_id = ui.item.value;
      ui.item.value = ui.item.label;
      element.change();
      controller.showAll();
    }
  }).on('focus',function(){
    if ($(this).val() == 'Enter Address') {
      $(this).val(''); $(this).removeClass('inactive');
    }
  // }).on('keypress', function(e){
  }).on('keyup', function(e){
    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
    if(keynum == 13) {//13 is the enter char code
      e.preventDefault();
      // $('ul.ui-autocomplete').hide();
      if (listing_id == 0)
        getListingAddress($(this).val(), updateMyAddresses);
      else if (typeof controller.doAction != 'undefined' &&
               typeof controller.doAction == 'function')
        controller.doAction();
    }
    else if (keynum == 32 || // space
             keynum == 44) { // comma
      // myAddresses = [];
      e.preventDefault();
      // $('ul.ui-autocomplete').hide();
      words = ($(this).val().match(/ /g)||[]).length;
      console.log ("words count:"+words);
      if (words > 1) getListingAddress($(this).val(), updateMyAddresses);
    }
    else {
        if (keynum == 8 ||   // backspace
            keynum == 127) { // DEL
          words = ($(this).val().match(/ /g)||[]).length;
          $('span#marker').hide();   
        }
        if (words) {
          var val = $(this).val();
          var len = val.length;
          if ( val.charAt(len - 3) == ' ') {
            e.preventDefault();
            // $('ul.ui-autocomplete').hide();
            getListingAddress($(this).val(), updateMyAddresses);
          }
        }
    }
    $(this).attr('listing_id', 0);
    $(this).attr('listing_address', '');	
    listing_id = 0;
    controller.showAll();
  });
}

// function pickedCity(wasPushed, city_id, city, callback) {
//   testCityName = city;
//   for(var i in cities)
//     if (testCityName == cities[i].label) {
//       city_id = cities[i].id;
//       break;
//     }
// 	$('.autocomplete input').attr('city_id', city_id); 
//   $('.autocomplete input').attr('city_name', city); 
// 	$('.autocomplete input').val(city);

// 	testCityId = city_id;
// 	if (wasPushed)
// 		ahtb.pop();
// 	else if (ahtb.opened)
// 		ahtb.close();

// 	window.setTimeout(function() {
// 		if (typeof callback == 'function')
//             callback(QuizType.QUIZ_LOCALIZED, [city_id], (typeof controller.doDistance != 'undefined' ? (controller.doDistance ? controller.distance : 0) : controller.distance) );
//         else
//         	controller.showAll(); // default
// 	}, 350);
// }

function getListingAddress(address, callback) {
	var haveAhtbOpen = ahtb.opened;
  addressRequestCount++;
  if (getAddressCall)
    getAddressCall.abort();

  getAddressCall = $.ajax({
          url: ah_local.tp+'/_pages/ajax-quiz.php',
          data: { query:'find-listing',
                  data: { address: address }
                },
          dataType: 'json',
          type: 'POST',
          success: function(d){  
            getAddressCall = null;
            if (d &&
                d.data &&
                (typeof d.data == 'object' ||
                d.data.length) ){
                if (d.status == 'OK' &&
                    (typeof d.data == 'object' || d.data.length) ) {     
                    $('span#marker').hide();                                 
                    console.log(JSON.stringify(d.data));
                    var len = length(d.data); 
                    addresses = d.data;
                    if (typeof callback == 'function') // last one back
                      callback(len > 1);
                    if (len == 1) {
                      listing_id = addresses[0].id;
                      controller.showAll();
                    }
                    addressRequestCount--;
                }
                else {
                  addressRequestCount--;
                  $('span#marker').show();
                  console.log("no listings returned for "+address);
                  addresses = [];
                  if (typeof callback == 'function')
                    callback();
                }
            }
            else {
              console.log('No listings found for '+address);
              $('span#marker').show();
            }
          },
          error: function(d) {
            getAddressCall = null;
            addressRequestCount--;
            $('span#marker').hide();   
            if ( typeof d.statusText != 'undefined' &&
                 d.statusText == 'abort')
              return;

            window.setTimeout(function() {
              ahtb.alert(d && d.data ? d.data : 'Failed find a matching address for '+address, 
                            {title: 'Address Match', 
                             height:180, 
                             width:600,
                             buttons:[
                      {text:'OK', action: function() {
                        haveAhtbOpen ? ahtb.pop() : ahtb.close();
                      }}]});
            }, 150);
          }
        });
}

 