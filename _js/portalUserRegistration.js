/**
 * 
 * This js requires that the user file has nameMode, phoneMode and emailMode defined and available for this file.
 * The html that uses this js should have a button or anchor with onclick calling javascript:portal_user_register.start(#) so that it can get connected
 * properly.  The # will be a  number, like 1 or 2, so that if there are two sets of inputs, we know which one it came from and update the
 * other
 * 
 * 
 */


jQuery(document).ready(function($){
  var cityName = '';
  // console.log("home sees myPortalRow:"+myPortalRow);

  portal_user_register = new function() {
    this.userName = '';
    this.userEmail = '';
    this.userPhone = '';
    this.userPhoto = '';
    this.fbId = 0;
    this.triggerEvent = '';

    this.init = function() {
      $('input.phone').mask('(999) 000-0000');
      if (nameMode == PortalLandingElementMode.PORTAL_LANDING_NOT_USED) {
        $('input.name').hide();
        $('input.email').hide();
        $('input.phone').hide();
        phoneMode = emailMode = PortalLandingElementMode.PORTAL_LANDING_NOT_USED;
      }
      // else {
      //   $('input.name').fadeIn(250, function() {
      //     if (emailMode)
      //       $('input.email').fadeIn(250, function() {
      //         if (phoneMode) {
      //           $('input.phone').fadeIn(250);
      //         }
      //       });
      //     else if (phoneMode) 
      //       $('input.phone').fadeIn(250);
      //   });
      // }
      else {
        $('input.name').show();
        if (emailMode)
          $('input.email').show();
        if (phoneMode) 
          $('input.phone').show();
      }
      
      $('a.start').show();

      $('input.portalUserInfo').keyup(function(e) {
				var keynum = e.keyCode || e.which;
				if(keynum == 13) {
					portal_user_register.start();
				}
				else {
					e.preventDefault();
					var theClass = $(this).attr('class');
					var theOther = $(this).attr('id') == 'one' ? 'two' : 'one';
					var val = $(this).val();
					$('input.'+theClass+'#'+theOther).val(val);
					var ele = $('input.portalUserInfo');
					ele.each(function() {
						$(this).removeClass('invalid');
					});
				}
      });

      $('#confirm-skip .confirm-popup button.back-to-signup').on('click', function() {
        $('#confirm-skip').hide();
        $('div#portal-user-capture').css('z-index', 1000000);
      });

      $('#confirm-skip .confirm-popup a.skip-signup').on('click', function() {
        $('#confirm-skip').hide();
        $('div#portal-user-capture').css('z-index', 1000000);
        portal_user_register.reallyStart(portal_user_register.triggerEvent);
      });
    }

    this.start = function(which, event) {
      // var a = $('div#userdata a[id="'+(which == 1 ? 'one' : 'two')+'"]');
      // var name = a.siblings('input.name').val();
      // var email = a.siblings('input.email').val();
      // var phoneEle = a.siblings('input.phone');
      var div = $('div#userdata[group="'+(which == 1 ? 'one' : 'two')+'"]');
      var name = div.find('input.name').val();
      var email = div.find('input.email').val();
      var phoneEle = div.find('input.phone');      
      var phone = '';
      if (phoneEle.length)
        phone = phoneEle.val().replace(/[^0-9]/g, '');
      var warning = '';
      if (nameMode > PortalLandingElementMode.PORTAL_LANDING_NOT_USED &&
          name.length == 0) {
        if (emailMode >= PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE &&
            email.length == 0) {
					warning = '*Please enter your info';
          $('input').addClass('invalid');
				}
        else {
          $('input.name').addClass('invalid');
					warning = '*Please enter your full name';
				}
      }
      else if (emailMode >= PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE &&
               email.length == 0) {
        $('input.email').addClass('invalid');
				warning = '*Please enter your email';
			}
      else if (email.length &&
              !validateEmail(email)) {
				$('input.email').addClass('invalid');
        warning = '*Please enter valid email';
			}
      else if ( (phoneMode >= PortalLandingElementMode.PORTAL_LANDING_MUST_HAVE &&
                 !validatePhone(phone)) ||
                (phoneMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL &&
                 phone.length && // it's optional, so if they entered anything, make sure it's valid
                 !validatePhone(phone)) ) {
        $('input.phone').addClass('invalid');
        warning = '*Please enter valid phone number';
      }
			
      if (warning.length) {
        $('.warning').each(function() {
          $(this).html(warning);
        });
        $('.warning').each(function() {
          $(this).removeClass("hidden");
        });
        return;
      }

      portal_user_register.userName = name;
      portal_user_register.userEmail = email;
      portal_user_register.userPhone = phone;
      portal_user_register.triggerEvent = event;

      if ( emailMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL &&
           email.length == 0) {
        $('div#portal-user-capture').css('z-index', 100000);
        $('#confirm-skip p').html("Are you sure you don't want to enter your email?  This will allow "+portalAgent.first_name+" to send you information about listings you may be interested in.");
        $('#confirm-skip .options .back-to-signup').html('Enter email');
        $('#confirm-skip').show();

        // ahtb.open({html: "<p>Are you sure you don't want to enter your email?  This will allow "+portalAgent.first_name+" to send you information about listings you may be interested in.</p>",
        //            width: 450,
        //            height: 190,
        //            buttons: [
        //             {text:"Enter email", action: function() {
        //               ahtb.close();
        //             }},
        //             {text:"Proceed", action: function() {
        //               ahtb.close(portal_user_register.reallyStart);
        //             }}
        //            ],
        //            closed: function() {
        //             $('div#portal-user-capture').css('z-index', 1000000);
        //            }
        //          });
        return;
      }

      if ( phoneMode == PortalLandingElementMode.PORTAL_LANDING_OPTIONAL &&
           phone.length == 0) {
        $('div#portal-user-capture').css('z-index', 100000);
        $('#confirm-skip p').html('Get full access every time you visit us by entering your phone number (used as password) to create a proper account.');
        $('#confirm-skip .options .back-to-signup').html('Enter password');
        $('#confirm-skip').show();
        /*ahtb.open({html: '<div id="phoneNumberMissing"><p>Get full access every time you visit us by entering your phone number (used as password) to create a proper account.</p></div>',
                   width: 450,
                   height: 190,
                   buttons: [
                    {text:"Enter password", action: function() {
                      ahtb.close();
                    }},
                    {text:"Skip for now", action: function() {
                      ahtb.close(portal_user_register.reallyStart);
                    }}
                   ],
                   opened: function() {
                    // can put in css modification code here to change look that can't be changed easily in style.less
                   },
                   closed: function() {
                    $('div#portal-user-capture').css('z-index', 1000000);
                   }
                 });*/
				
        return;
      }

      portal_user_register.reallyStart(event);
    }

    this.reallyStart = function(event) {
      event = typeof event == 'undefined' ? portal_user_register.triggerEvent : event;
      console.log("Got name:"+portal_user_register.userName+", email:"+portal_user_register.userEmail+', phone:'+portal_user_register.userPhone+', id:'+portal_user_register.fbId+", event:"+(typeof event != 'undefined' ? event : 'unknown'));
      if (nameMode == PortalLandingElementMode.PORTAL_LANDING_NOT_USED) { // then we must be going straight to quiz or something...
        if (controller &&
            typeof controller.actionPortalUserRegistered == 'function')
            controller.actionPortalUserRegistered();
        else
          console.log("nameMode is PORTAL_LANDING_NOT_USED, but no valid controller");
        return;
      }

      // anchors don't like disabled attribute, so use class if it is, else if button, would work also 
      $('a.start').addClass('disabled');
      $('a.start').prop('disabled', true);

      prepOverlaySpinner(portal_user_register.ajax,{
                query: 'new-portal-user',
                data: {name: portal_user_register.userName,
                       email: portal_user_register.userEmail,
                       phone: portal_user_register.userPhone,
                       photo: portal_user_register.userPhoto,
                       emailMode: emailMode,
                       phoneMode: phoneMode,
                       fbId: portal_user_register.fbId,
                       agentID: portalAgent.author_id,
                       session_id: ah_local.activeSessionID,
                       thisPage: thisPage,
                       campaign: ah_local.agentCampaign,
                       event: typeof event != 'undefined' ? event : 'unknown'},
                done: function(d) {
                  if (controller &&
                      typeof controller.actionPortalUserRegistered == 'function')
                      controller.actionPortalUserRegistered(d);
                  else
                    console.log("successfully did new-portal-user, but no valid controller");
                },
                error: function(d) {
                  $('div#overlay-bg').hide();
                  $('a.start').prop('disabled', false);
                  $('a.start').removeClass('disabled');
                  var msg = typeof d.data.msg != 'undefined' ? d.data.msg :
                            (typeof d.data != 'undefined' ? (typeof d.data == 'string' ? d.data : JSON.stringify(d.data)) : "Unknown failure");
                  $('div#portal-user-capture').css('z-index', 100000);
                  ahtb.open({html:'<div id="error">'+msg+'</div>',
                            width: 450,
                            height: 120,
                            closed: function() {
                              $('div#portal-user-capture').css('z-index', 1000000);
                            }})
                },
                fail: function(d) {
                  $('div#overlay-bg').hide();
                  $('a.start').prop('disabled', false);
                  $('a.start').removeClass('disabled');
                  $('div#portal-user-capture').css('z-index', 100000);
                  ahtb.open({html:'<div id="error">Sorry had some database failure.</div>',
                            width: 450,
                            height: 120,
                            closed: function() {
                              if (controller &&
                                  typeof controller.actionPortalUserRegistered == 'function')
                                  controller.actionPortalUserRegistered(d);
                              else
                                console.log("successfully did new-portal-user, but no valid controller");
                            }})
                }
          } // ajax()
      )
    }

    this.ajax = function(d){
      if(typeof d.query !== 'undefined'){
        var h = [];
        if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
        if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
        if (typeof d.fail !== 'undefined') h.fail = d.fail;
        if (typeof d.error !== 'undefined') h.error = d.error;
        if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
        var url = typeof d.url != 'undefined' ? d.url : ah_local.tp+'/_pages/ajax-register.php';
        $.post(url, 
             h.headers, 
             function(){h.before()},
             'json')
          .done(function(x){
            if (x.status == 'OK') h.done(x);
            else if (h.error) h.error(x);
            else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
          })
          .fail(function(x){
            if (h.fail) h.fail(x);
            else ahtb.close(function(){ahtb.alert("Database failure, please retry.", {height: 150})});
          });
      } else ahtb.close(function(){ahtb.alert('Query was undefined')});
    }

  }

  portal_user_register.init();
})