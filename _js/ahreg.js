var ahreg = null;

var reg_html_divopen = '<div id="registration">';
var reg_html_new_user = 
				 '<div id="title">'+
			     	'<h2>Create an Account</h2>'  +
                    '<p id="extra-message1" style="margin:0 auto 1.5em;width:90%;font-size:.85em;text-align:center;">To save your search, you must first register with us.</p>' +
                 '</div>'+
			     '<form id="register">'  +
			     	'<div id="name">'  +
							'<label for="user_login">Name:</label>'  +
					    '<input type="text" name="first-name" placeholder="First Name" />'  +
					    '<input type="text" name="last-name" placeholder="Last Name" />'  +
					'</div>'  +
					'<div id="login">'  +
                            '<label for="user_login">Username:</label>'  +					
					    '<input type="text" name="login-id" class="login-id" placeholder="(Defaults to Email)" title="Optional, defaults to your email">'  +
					'</div>'  +
					'<div id="email">'  +
							'<label for="user_login">Email:</label>'  +
					    '<input type="text" name="email" placeholder="john@example.com" />'  +
					'</div>'  +
					'<div id="passwordDiv">'  +
							'<label for="user_login">Password:</label>'  +
				    	'<input type="password" name="password" id="password" placeholder="Minimum of 8 Characters" />'  +
					'</div>'  +
					'<div id="verifyDiv">'  +
							'<label for="user_login">Verify Password:</label>'  +
				    	'<input type="password" name="password" id="verify" placeholder="Match Password" /><span id="marker" class="entypo-attention"></span>'  +
				  '</div>'  +
				  '<div id="professional">'  +
				    	'<input type="checkbox" id="realtor" /> <span id="identify">I am a real estate professional </span>'  +
				  '</div>'  +
			      '<div id="bre">'  +
					'<label>License Number</label>' +
				    '<input type="text" id="breID" name="realtor-id" placeholder="ID Required" />'  +
				    	'<select name="States" id="states">'  +
						  	'<option value="-1" selected="selected">State</option>'  +
						 	ahreg_data.states  +
						'</select>'  +
					'<span id="invite">  Invite code: </span><input type="text" id="invitation_code" placeHolder="(Optional)">'  +
				  '</div>'  +
			      '<input type="submit" id="submitbtn" name="submit" value="Join Us" />'  +
			     '</form>'+
			     '<div id="result"></div>';
var reg_html_login =
				'<form id="sacrifice"></form>' +
				    '<span id="extra-message2"></span>' +
				     ahreg_data.login_form + 
			    	'<p class="forgot-password"><a href="'+ahreg_data.lost_password_url+'" title="Forgot your password?">Forgot your password?</a></p>' +
	             //	'</form>'+
	             '</form>';
var reg_html_divclose = '</div>';

jQuery(document).ready(function($){
	ahreg = new function(){
		this.state = -1;
		this.password = '';
		this.verify = '';
		this.professional = false;
		this.bre = '';
		this.first = '';
		this.last = '';
		this.email = '';
		this.login = '';
		this.invitationCode = '';
		this.callback = null;
		this.args = null;
		this.cancelCallback = null;
		this.cancelArgs = null;
		this.redirect = '';
		this.extraMessage = '';
		this.loginOnly = false;
		this.isAgent = false;
		this.havePortalAgent = false;
		this.spinner = $('div#overlay-bg .spin-wrap');

		this.init = function() {
			console.log("ahreg is created.");
    		$('#states option[value="'+ahreg.state+'"]').prop('selected', true);
    		$('#password').val(ahreg.password);
    		$('#verify').val(ahreg.verify);
    		$('#professional #realtor').prop('checked', ahreg.professional);
    		// var height = ahreg.cancelCallback != null ? 510 : 580;
    		var height = 580;
    		if (ahreg.professional) {
    			ahtb.resize(450, height+115);
	    		$('#bre').show();
	    	}
	    	else {
	    		ahtb.resize(450, height+45); 
	    		$('#bre').hide();
	    	}
	    	$("input[name='realtor-id']").val(ahreg.bre);
	    	$('input[name="first-name"]').val(typeof ah_local.portalUserFirstName != 'undefined' && ah_local.portalUserFirstName && ah_local.portalUserFirstName.length ? ah_local.portalUserFirstName : ahreg.first);
	    	$('input[name="last-name"]').val(typeof ah_local.portalUserLastName != 'undefined' && ah_local.portalUserLastName && ah_local.portalUserLastName.length ? ah_local.portalUserLastName : ahreg.last);
	    	$('input[name="email"]').val(typeof ah_local.portalUserEmail != 'undefined' && ah_local.portalUserEmail && ah_local.portalUserEmail.length ? ah_local.portalUserEmail : ahreg.email);
	    	$('input[name="login-id"]').val(ahreg.login);
	    	$('#invitation_code').val(ahreg.invitationCode);

	    	if (window.location.href.indexOf('quiz-results') != -1 &&
	    		typeof ahreg.extraMessage != 'undefined') {
	    		var h = '<div class="divide"><div class="line-left"></div><span class="or">Or</span><div class="line-right"></div></div>'+
						'<p style="margin:0 auto 1.5em;width:90%;font-size:.85em;text-align:center;">'+ahreg.extraMessage+'</p>';
				$('#registration #extra-message2').html(h);
	    	}

	    	if (ahreg.redirect != '')
	    		$('#login-form input[name=redirect_to]').attr('value', ahreg.redirect);

	    	ahreg.password == ahreg.verify ? $('#marker').hide() : $('#marker').show();
		}

		this.setCallback = function(callback, args, cancelCb, cancelArgs) {
			ahreg.callback = callback;
			ahreg.args = args;
			if (typeof cancelCb != 'undefined')
				ahreg.cancelCallback = cancelCb;
			if (typeof cancelArgs != 'undefined')
				ahreg.cancelArgs = cancelArgs;
		}

		this.setRedirect = function(d) {
			ahreg.redirect = d;
		}

		this.success = function(d){
			if (ahreg.spinner.length)
				$('div#overlay-bg').fadeOut(50);

			$('#result').html('Registering...').fadeOut();
			//if (d.status == 'OK') window.location = ah_local.wp+'/sellers';
			console.log(d.data);
			$('div#TB_window').animate({opacity: 1},{duration: 500});
			if (d.status == 'OK') {
				var msg = "Welcome to our site!  Please enjoy finding the listing that fits your lifestyle!";
				var extra = 0;
				d.data.userMode = parseInt(d.data.userMode);
				if (d.data.userMode == AgentType.AGENT_ORGANIC) {
					msg = "Welcome aboard!  Please browse our site.  Your agent page will be available once your email has been verified.";
					extra = 70;
				}
				else if (d.data.userMode)
					msg = "Welcome to our site!  Please use the seller's page and update your photo and description, then add your listings.";
				console.log(msg);

				ah_local.author_id = d.data.id;
				ahtb.open({
				    title: 'Define Your Lifestyle',
				    height: 205 + extra,
				    width: 600,
				    html: d.data.msg,
				    	buttons:[
							{text:"OK", action:function(){ 
								// window.location = ahreg_data.redirect; 
								ahtb.close(function() {
									if (ahreg.callback != null) {
								      	cb = ahreg.callback;
								      	args = ahreg.args;
								      	ahreg.callback = null;
								      	ahreg.args = null;
								      	ahreg.cancelCallback = null;
								      	ahreg.cancelArgs = null;
								      	ahreg.redirect = '';
								      	ahreg.extraMessage = '';
								      	if (typeof args == 'string' &&
								      		args.indexOf('http') != -1) {
											if (ahreg.spinner.length)
												$('div#overlay-bg').fadeIn(50);
								      	}
								      	window.setTimeout( function() {
								      		if (d.data.userMode == AgentType.AGENT_LISTHUB ||
								      			d.data.userMode == AgentType.AGENT_INVITE)
								      			window.location = ah_local.wp+"/sellers";
								      		else
								      			cb(args) }, 250 );
								      }
								});
							}},
						],
				    closed: function(){
				      ahtb.showClose(250);
				      ahtb.closeOnClickBG(1);
				      if (ahreg.callback != null) {
				      	cb = ahreg.callback;
				      	args = ahreg.args;
				      	ahreg.callback = null;
				      	ahreg.args = null;
				      	ahreg.cancelCallback = null;
				      	ahreg.cancelArgs = null;
				      	ahreg.redirect = '';
				      	ahreg.extraMessage = '';
				      	if (typeof args == 'string' &&
				      		args.indexOf('http') != -1) {
							if (ahreg.spinner.length)
								$('div#overlay-bg').fadeIn(50);
				      	}
				      	window.setTimeout( function() {
				      		cb(args) }, 250 );
				      }
				    }
				});
			}
			else {
				$("#submitbtn").prop('disabled', false);
				ahtb.push();
				ahtb.alert(d.data, 
					{height: 180,
					 buttons: [{text: "OK", action: function() {
						ahtb.pop();
					}}]});
			}
		}

		this.error = function(d){
			if (ahreg.spinner.length)
				$('div#overlay-bg').fadeOut(50);
			$("#submitbtn").prop('disabled', false);
			$('div#TB_window').animate({opacity: 1},{duration: 500});
			d = d.data;
			var msg = typeof d == 'string' ? d : "We're very sorry, there was a system failure.";
			if (typeof d == 'object') {
				if (typeof d.msg != 'undefined')
					msg += "  "+d.msg;
				else if (typeof d.responseText != 'undefined' &&
						d.responseText.length)
					msg += "  "+d.responseText;
				else if (typeof d.statusText != 'undefined' &&
						d.statusText.length &&
						d.statusText != 'error')
					msg += "  "+d.statusText;
				else if (typeof d.status != 'undefined' &&
						 typeof d.status == 'string' &&
						 d.status.length)
					msg += "  "+d.status;
				
				ahtb.open({html: msg,
						  title: 'You Have Encountered an Error during Registration', 
						  width: 450,
						  height: 110 + (Math.floor(msg.length / 60) * 25),
						  buttons:[{text:'OK', action:function() {
						  	ahtb.close();
						  }}]});
			}
			else {
				console.log("Failed: "+msg);
				ahtb.open({ title: 'You Have Encountered an Error during Registration', 
						height: 150, 
						html: '<p>Status: '+msg+'</p>' 
					});
			}
		}

		this.checkBRE = function() {
			var bre = '';
			ahreg.isAgent = false;
			if ( $('#professional #realtor').prop('checked') ) {
				if ((bre = $("input[name='realtor-id']").val()) == '' ||
				     bre.length < 5) {
					ahtb.push();
					ahtb.alert("Please enter a RE license number",
								{height: 150,
								 buttons: [{text: "OK", action: function() {
									ahtb.pop();
								}}]});
					return false;
				}
				ahreg.bre = bre;
				if (ahreg.state == -1) {
					ahtb.push();
					ahtb.alert("Please select the state your Realtor ID is registered.",
								{height: 150,
								 buttons: [{text: "OK", action: function() {
									ahtb.pop();
								}}]});
					return false;
				}
				ahreg.isAgent = true;
			}
			return true;
		}

		this.checkFirst = function() {
			var first = '';
			if ( (first = $('input[name="first-name"]').val()) == '') {
				ahtb.push();
				ahtb.alert("Please enter your first name.",
							{height: 150,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}
			ahreg.first = first;
			return true;
		}

		this.checkLast = function() {
			var last = '';
			if ( (last = $('input[name="last-name"]').val()) == '') {
				ahtb.push();
				ahtb.alert("Please enter your last name.",
							{height: 150,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}
			ahreg.last = last;
			return true;
		}

		this.checkEmailLogin = function() {
			var email = '';
			if ( (email = $('input[name="email"]').val()) == '') {
				ahtb.push();
				ahtb.alert("Please enter your email adress.",
							{height: 150,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}

			var login = '';
			if ( (login = $('input[name="login-id"]').val()) == '') 
				login = email;

			if (!validateEmail(email)) {
				ahtb.push();
				ahtb.alert("Please check your email adress, it doesn't appear to be valid.",
							{height: 150,
							 width: 600,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}
			ahreg.email = email;
			ahreg.login = login;
			return true;
		}

		this.checkPassword = function() {
			var passwd = $('input[name="password"]').val();
			if (ahreg.havePortalAgent)
				passwd = passwd.replace(/[^0-9]/g, '');

			if ( passwd.length == 0 ||
				 passwd.length < 8 ||
				 (ahreg.havePortalAgent &&
				  passwd.length != 10)) {
				ahtb.push();
				var msg = ahreg.havePortalAgent ? "Please enter a valid phone number." : "Please enter a password of at least 8 characters.";
				ahtb.alert(msg,
							{height: 150,
							 width: 500,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}
			ahreg.password = passwd;

			var verify = $('#verify').val();
			if (ahreg.havePortalAgent)
				verify = verify.replace(/[^0-9]/g, '');

			if ( passwd != verify) {
				ahtb.push();
				ahtb.alert("Passwords do not match.",
							{height: 150,
							 buttons: [{text: "OK", action: function() {
								ahtb.pop();
							}}]});
				return false;
			}
			return true;
		}

		this.openModal = function(){	
			// DBget('check-current-seesion-has-owner',
			// 	  {	session: ah_local.sessionID },
			// 	  ahreg.checkedCurrentSessionHasOwner,
			// 	  ahreg.error);
			if ( ahreg.spinner.length )
				$('div#overlay-bg').fadeIn(50);

			ajax({
				url: ah_local.tp+'/_pages/ajax-register.php',
				query: 'check-current-session-has-owner',
				data: {	session: ah_local.sessionID },
				done: ahreg.checkedCurrentSessionHasOwner,
				error: ahreg.error,
				fail: ahreg.error
			})
		}

		this.completeRegistration = function() {
			var invitationCode = $('#invitation_code').val();
			ahreg.invitationCode = invitationCode;

			// if (!ahreg.isAgent)
			$("#submitbtn").prop('disabled', true);
			$('#result').html('Registering...').fadeIn();

			// DBget('basic-register',
			// 	  {	first_name: ahreg.first, 
			// 		last_name: ahreg.last,
			// 		email: ahreg.email, 
			// 		login: ahreg.login,
			// 		password: ahreg.password,
			// 		realtor_id: ahreg.bre,
			// 		state: ahreg.state,
			// 		inviteCode: invitationCode
			// 	  },
			// 	  ahreg.success,
			// 	  ahreg.error);

			if ( ahreg.spinner.length )
				$('div#overlay-bg').fadeIn(50);

			ajax({
				url: ah_local.tp+'/_pages/ajax-register.php',
				query: 'basic-register',
				data: {	first_name: ahreg.first, 
						last_name: ahreg.last,
						email: ahreg.email, 
						login: ahreg.login,
						password: ahreg.password,
						realtor_id: ahreg.bre,
						state: ahreg.state,
						inviteCode: invitationCode,
						page: typeof thisPage != 'undefined' ? thisPage : '',
						campaign: ah_local.agentCampaign,
						agentID: ah_local.agentID, // portal agent
						isPhoneNumber: ahreg.havePortalAgent
					  },
				done: ahreg.success,
				error: ahreg.error,
				fail: ahreg.error
			})
		}

		this.goGetIt = function() {
			if ( ahreg.checkBRE() == false)
				return;

			if (ahreg.checkFirst() == false)
				return;

			if (ahreg.checkLast() == false)
				return;
			
			if (ahreg.checkEmailLogin() == false)
				return;
			
			if (ahreg.checkPassword() == false)
				return;
			
			// call check maybe
			if (!ahreg.isAgent) {
				ahreg.completeRegistration();
				return;
			}

			var parent = $('div#TB_window');
			var button = $("#submitbtn");
			var emailEle = $('input[name=email]');
			var firstNameEle = $('input[name=first-name]');
			var lastNameEle = $('input[name=last-name]');
			var phoneEle = null;
			var phone = '';
			checkSellerInDB(ahreg.first, ahreg.last, ahreg.email, ahreg.state, phone, ahreg.login,
							ahreg.completeRegistration, // successful callback
							ahreg.goGetIt, // retry callback
							parent, // div to make opaque
							button, // button to enable disablity (this one)
							emailEle, // email element
							firstNameEle, // first_name element
							lastNameEle, // last_name element
							phoneEle); // phone element
		}


		this.checkedCurrentSessionHasOwner = function(d) {
			if (d.status == 'fail') {
				ahtb.push();
				ahtb.alert(d.data, 
					{height: 180,
					 buttons: [{text: "OK", action: function() {
						ahtb.pop();
					}}]});
				return;
			}

			ahreg.loginOnly = false;

			console.log("Session:"+ah_local.sessionID+" is owned by "+d.data);
			var reg_html = reg_html_divopen;
			if ( typeof ahreg.extraMessage != 'undefined' &&
				 ahreg.extraMessage.indexOf('existing credentials') != -1) {// then caller has determined that only simple login should be shown
			// if (d.data != '0') {// then owned by someone already, so just login;
				reg_html += reg_html_login + reg_html_divclose;
				ahreg.loginOnly = true;
			}
			else
				reg_html += reg_html_new_user + reg_html_login + reg_html_divclose;

			var opened = function(){
				if (hideAgent) { // defined in header.js
					$('#professional').hide();
					$('#bre').hide();
				}

				var ele = $('#TB_window #TB_ajaxContent form#tb-submit');
				ele.focus();

				ahreg.init();

				ahreg.havePortalAgent = parseInt(ah_local.agentID) > 0;

				if ( ahreg.havePortalAgent ) {
					$('div#passwordDiv input').attr('placeholder', "Use phone number as password with area code");
					$('div#passwordDiv input').mask('(999) 000-0000');
					$('div#verifyDiv input').mask('(999) 000-0000');
				}

				if (ahreg.spinner.length)
					$('div#overlay-bg').fadeOut(50);
				
			    $('div#verifyDiv #verify').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.verify = $(this).val();
			    	var len = $(this).val().length;
			    	len ? $('div#verifyDiv #marker').show() : $('#marker').hide();
			    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
			    	var hasCancel = $('div#verifyDiv #marker').hasClass('entypo-attention');
			    	if ($(this).val() == $('#password').val()) {
			    		if (hasCancel) {
			    			$('div#verifyDiv #marker').removeClass('entypo-attention');
			    			$('div#verifyDiv #marker').addClass('entypo-check');
			    			$('div#verifyDiv #marker').css('color', 'lightgreen');
			    		}
			    	}
			    	else if (!hasCancel) {
			    			$('div#verifyDiv #marker').removeClass('entypo-check');
			    			$('div#verifyDiv #marker').addClass('entypo-attention');
			     			$('div#verifyDiv #marker').css('color', 'red');
			   		}
			    	
			    })

			    $('div#passwordDiv #password').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.password = $(this).val();
			    	var len = $(this).val().length;
			    	var vLen = $('div#verifyDiv #verify').val().length;
			    	len && vLen ? $('div#verifyDiv #marker').show() : $('div#verifyDiv #marker').hide();
			    	//$('#marker').attr('visibility', len ? 'visible' : 'hidden');
			    	var hasCancel = $('div#verifyDiv #marker').hasClass('entypo-attention');
			    	if ($(this).val() == $('div#verifyDiv #verify').val()) {
			    		if (hasCancel) {
			    			$('div#verifyDiv #marker').removeClass('entypo-attention');
			    			$('div#verifyDiv #marker').addClass('entypo-check');
			    			$('div#verifyDiv #marker').css('color', 'lightgreen');
			    		}
			    	}
			    	else {
			    		if (!hasCancel) {
			    			$('div#verifyDiv #marker').removeClass('entypo-check');
			    			$('div#verifyDiv #marker').addClass('entypo-attention');
			     			$('div#verifyDiv #marker').css('color', 'red');
			   			}
			    	}
			    })

			    $('div#registration input[name="last-name"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.last = $(this).val();
			    })

			    $('div#registration input[name="first-name"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.first = $(this).val();
			    })

			    $('div#registration input[name="email"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.email = $(this).val();
			    	// $('input#submitbtn').focus();
			    })
			    $('div#registration input[name="email"]').on('keypress', function(e){
		      		var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
					if(keynum == 13) {//13 is the enter char code
						e.preventDefault();
						$('input[name="email"]').focus();
						return false;
					}
		      	})

			    $('div#registration input[name="login-id"]').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.login = $(this).val();
			    })

			    $('#invitation_code').keyup(function(e) {
			    	e.preventDefault();
			    	ahreg.invitationCode = $(this).val();
			    })

			    $('#professional #realtor').on('change', function() {
			    	var val = $(this).prop('checked');
			    	ahreg.professional = val;
			    	// var height = ahreg.cancelCallback != null ? 510 : 580;
			    	var height = 580;
			    	if (val) {
			    		ahtb.resize(450, height+115);
			    		$('#bre').show();
			    	}
			    	else {
			    		ahtb.resize(450, height+45); 
			    		$('#bre').hide();
			    	}
	    		})

	    		$("#submitbtn").click(function(e){
					e.preventDefault();
					ahreg.goGetIt();
				});				

				$('#bre #states').change(function() {
						var val = $("#bre #states option:selected").val();
						if (val == -1 &&
							$('#register [name=realtor-id]').val() != '') {
							ahtb.alert("Please pick the state where your license is registered.",
								{height: 150});
						}
						else {
							ahreg.state = val;
							console.log("State is now set to "+ahreg.state);
						}
				});
			} // opened

			var height = ahreg.cancelCallback != null ? 580 : 625;
			if (ahreg.loginOnly)
				height = 240;
			ahtb.open({
				html: reg_html,
				title: "Registration",
				width: 450,
				height: height,
				// buttons: [{text:"Cancel", action: function(){ 
				// 	if (ahreg.cancelCallback != null) {
				//       	cb = ahreg.cancelCallback;
				//       	args = ahreg.cancelArgs;
				//       	ahreg.callback = null;
				//       	ahreg.args = null;
				//       	ahreg.cancelCallback = null;
				//       	ahreg.cancelArgs = null;
				//       	ahreg.redirect = '';
				//       	window.setTimeout( function() {
				//       		cb(args) }, 250 );
				//     }
				//     else
				// 		ahtb.close(); 
				// }}],
				hideSubmit: true,
				// closeOnClickBG: true,
				opened: !ahreg.loginOnly ? opened : function() {
					if (window.location.href.indexOf('quiz-results') != -1 &&
						typeof ahreg.extraMessage != 'undefined') {
			    		var h = '<p style="margin:0 auto 1.5em;width:90%;font-size:.85em;text-align:center;">'+ahreg.extraMessage+'</p>';
						$('#registration #extra-message2').html(h);
			    	}
			    	if (ahreg.redirect != '')
	    				$('#login-form input[name=redirect_to]').attr('value', ahreg.redirect);
				},
				closed: function() {
					if (ahreg.cancelCallback != null) {
				      	cb = ahreg.cancelCallback;
				      	args = ahreg.cancelArgs;
				      	ahreg.callback = null;
				      	ahreg.args = null;
				      	ahreg.cancelCallback = null;
				      	ahreg.cancelArgs = null;
				      	ahreg.redirect = '';
				      	ahreg.extraMessage = '';
				      	window.setTimeout( function() {
				      		cb(args) }, 250 );
				    }
				}
			})

		} // end openModal()
	}
	ahreg.init();
})