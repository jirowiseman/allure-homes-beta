var agentList = null;

function ajax(d){
	if(typeof d.query !== 'undefined'){
		var h = [];
		if (typeof d.data === 'undefined') h.headers = {query:d.query}; else h.headers = {query:d.query,data:d.data};
		if (typeof d.done === 'undefined') h.done = function(){}; else h.done = d.done;
		if (typeof d.fail !== 'undefined') h.fail = d.fail;
		if (typeof d.error !== 'undefined') h.error = d.error;
		if (typeof d.before === 'undefined') h.before = function(){}; else h.before = d.before;
		var url = typeof d.url != 'undefined' ? d.url : ah_local.tp+'/_sellers/_ajax.php';
		$.post(url, 
			   h.headers, 
			   function(){h.before()},
			   'json')
			.done(function(x){
				if (x.status == 'OK') h.done(x);
				else if (h.error) h.error(x);
				else ahtb.close(function(){ahtb.alert(x.data, 'Database Error')});
			})
			.fail(function(x){
				if (h.fail) h.fail(x);
				else ahtb.close(function(){ahtb.alert("Database failure, please retry.", {height: 150})});
			});
	} else ahtb.close(function(){ahtb.alert('Query was undefined')});
}

function checkSellerInDB(first, last, email, state, phone, login, registerCB, retryCB, parentEle, buttonEle, emailEle, firstNameEle, lastNameEle, phoneEle ) {
	ajax({
			url: ah_local.tp+'/_pages/ajax-register.php',
			query: 'check-if-listhub-seller',
			data: {//city: testCityName,
				   email: email,
				   first_name: first,
				   last_name: last,
				   login: login,
				   //realtor_id: bre,
				   state: state},
			done: function(d) {
				agentList = typeof d.data != 'undefined' ? d.data.matches : [];
				if (agentList.length == 1 &&
					d.data.exact) {// got a good match
					if (typeof registerCB == 'function') {
						// parentEle.animate({opacity: 0.5},{duration: 1000});
						// buttonEle.prop('disabled', true);
						parentEle.animate({opacity: 0.5},{duration: 500});
						registerCB();
					}
				}
				else {
					var h =  '<button class="closeVid">Close</button>'+ // reusing closeVid, even tho it's a misnomer here...
							 '<div id="noMatch">'+
								'<span id="intro">We could not associate</span>'+
								'<span id="first">'+first+'&nbsp;</span>'+
								'<span id="last">'+last+',&nbsp;</span>'+
								'<span id="email">'+email+',&nbsp;</span>'+
								'<span id="state">'+state+'<br/></span>'+
								'<span id="introClose">with our list of agents in our database.</span>'+
							 '</div>';
					if (agentList.length == 0) { // nothing matched...
					h += '<div id="retry">'+
						 	'<span id="intro">If you think you should have been found, then please try again with another email address and last name that you know is used in your MLS feed.</span>'+
						 	'<input type="text" placeholder="Enter last name" id="last_name"></input>&nbsp;'+
						 	'<input type="text" placeholder="Enter another email" id="email"></input>&nbsp;'+
						 	'<button id="retry">Retry</button>'+
						 	'<span id="warning" style="display: none;"></span>'+
						 '</div>';
					}
					else { // multiple matches
					var list = '';
						for(var i in agentList)
							list += '<li><input type="radio" name="agent" for="'+i+'"></input>&nbsp;'+
									'<input id="first" readonly value="'+agentList[i].first_name+'">&nbsp;'+
									'<input id="last" readonly value="'+agentList[i].last_name+'">&nbsp;'+
									'<input id="email" readonly value="'+agentList[i].email+'">&nbsp;'+
									'<input id="city" readonly value="'+agentList[i].city+'"></li>';
					h += '<div id="multiMatch">'+
							'<span id="intro">These are possible '+agentList.length+' agents that may be you in our database.</span>'+
							'<div id="agentList">'+
								'<ul id="agentList">'+
									list +
								'</ul>'+
							'</div>'+
							'<span id="introClose">Please pick one and enter a phone number used in the MLS that is associated with that account for verification.  This phone number may be your cell phone, office or brokerage number.</span>'+
							'<input type="text" id="phone" placeholder="Enter phone # used in MLS"></input>&nbsp;&nbsp;'+
							'<button id="verify">Verify</button>'+
							'<span id="warning" style="display: none;"></span>'+
						 '</div>';
					}

					h += '<span id="or">OR</span>'+
						 '<div id="regularAgeng">' +
						 	'<span id="intro">Register as a real estate agent, but existing listings in our database will not be associated with this account.  By continuing the registration, you will still become a Lifestyled Agent.</span>' +
						 	'<button id="simpleRegister">Register</button>&nbsp;&nbsp;' +
						 	'<input type="checkbox" id="agree">&nbsp;I acknowledge the restriction described above.</input>' +
						 	'<span id="warning" style="display: none;">Please check the acknowledgement</span>'+
						 '</div>';

					parentEle.animate({opacity: 0.5},{duration: 1000});
					var curZ = parseInt( parentEle.css('z-index') );
					$('div#agent-verify').css('z-index', curZ+2);
					$('div#agent-verify').html(h).fadeIn({duration: 2000,
														  done: function() {

							$('input#phone').mask('(999) 000-0000');
							$('input#phone').val(phone);
							$('button.closeVid').on('click', function() {
								$('div#agent-verify').fadeOut({duration: 2000}).empty();
								var parentSelector = parentEle.selector;
								var buttonSelector = buttonEle.selector;
								$(parentSelector).animate({opacity: 1},{duration: 1000});
								$(buttonSelector).prop('disabled', false);
							})

							$('input[type=text]').keyup(function() {
								$('div#retry span#warning').hide();
								$('div#multiMatch span#warning').hide();
							})

							$('input[type=radio]').change(function() {
								$('div#multiMatch span#warning').hide();
							})

							$('button#retry').on('click', function() {
								var last_name = $('input#last_name').val();
								var email = $('input#email').val();
								var msg = 'Please enter ';
								if (last_name.length == 0)
									msg += 'last name ';
								if (email.length == 0) 
									msg += (last_name.length == 0 ? 'and ' : '')+'email' 
								if (last_name.length == 0 || email.length == 0) {
									// ahtb.alert(msg);
									$('div#retry span#warning').html(msg).show();
								}
								else {
									lastNameEle.val(last_name);
									emailEle.val(email);
									if (typeof retryCB == 'function')
										retryCB();
								}
							})

							$('button#verify').on('click', function() {
								var phone = $('input#phone').val();
								if (phone.length == 0) {
									// ahtb.alert("Please enter a phone number");
									$('div#multiMatch span#warning').html("Please enter a phone number").show();
								}
								else {
									var radio = $('input[type=radio]:checked');
									if (radio.length == 0) {
										// ahtb.alert("Please check one agent from the list");
										$('div#multiMatch span#warning').html("Please check one agent from the list above").show();
									}
									else {
										$('div#multiMatch span#warning').hide();
										var newPhone = phone;
										phone = phone.replace(/[^0-9]/g, '');
										var id = radio.attr('for');
										var agent = agentList[id];
										ajax({
											url: ah_local.tp+'/_pages/ajax-register.php',
											query: 'verify-seller',
											data: {
												   	email: agent.email,
												   	first_name: agent.first_name,
												   	last_name: agent.last_name,
												   	state: state,
													phone: phone},
											done: function(d) {
												var msg = '<div id="success-verify-agent">That phone number matched our records.<br/>';
												if (agent.email != email &&
													agent.first_name != first) 
													msg += 'However, neither your email/name:'+email+'/'+first+", match the chosen agent's data:"+agent.email+'/'+agent.first_name+"<br/>"+
															'Are you sure you are claiming the right account?';
												else if (agent.email != email)
													msg +=  'However, your initial email:'+email+', does not match the one chosen:'+agent.email+'.<br/>' +
															'You can use '+email+' as your contact email, by editing your profile from the sellers dashboard after completion of the registration.';
												else if (agent.first_name != first) 
													msg += 	'However, your name:'+first+', does not match the one chosen: '+agent.first_name+
															'.<br/>Are you sure you are claiming the right account?';
												else
													msg +=	'By pressing OK, you will continue to be registered as '+agent.first_name+' '+agent.last_name+', '+agent.email+
															', from '+agent.city+', '+state;
												msg += '</div>';
												var ahtbOpened = ahtb.opened;
												// var curZ = parseInt( parentEle.css('z-index') );
												$('div#agent-verify').css('z-index', curZ-2); 
												if (ahtb.opened) {
													ahtb.push();
													$('div#TB_window').css('opacity', '1');
												}
												ahtb.open({	html: msg,
														 	width: 450, 
														 	height: 150 + (Math.floor(msg.length/80)* 25),
														 	buttons: [{text:'OK', action: function() { 
														 									var parentSelector = parentEle.selector;
														 									var emailSelector = emailEle.selector;
														 									var firstNameSelector = firstNameEle.selector;
														 									var lastNameSelector = lastNameEle.selector;
														 									var phoneSelector = phoneEle ? phoneEle.selector : '';
														 									var buttonSelector = buttonEle.selector;
																					 		ahtb.pop(function() {
																					 			$(emailSelector).val(agent.email);
																						 		$(firstNameSelector).val(agent.first_name);
																						 		$(lastNameSelector).val(agent.last_name);
																						 		if (phoneEle) $(phoneSelector).val(newPhone);
																						 		$(buttonSelector).prop('disabled', false);
																						 		$(parentSelector).animate({opacity: 1},{duration: 1000});
																						 		if (typeof retryCB == 'function')
																						 			window.setTimeout(function() {
																						 				retryCB();
																						 			}, 500);
																					 		}); 
																					 		$('div#agent-verify').css('z-index', curZ+2); 
																					 		$('div#agent-verify').fadeOut({duration: 2000}).empty();																					 			
																					 	}
																	  },
														 			  {text:'Cancel', action: function() { 
														 			  						ahtb.pop(function() {
														 			  							$('div#TB_window').css('opacity', '0.5');
														 			  						}); 
														 			  						$('div#agent-verify').css('z-index', curZ+2); 
														 			  					}}],
														});

											},
											error: function(d) {
												$('div#multiMatch span#warning').html("Sorry, that phone number didn't match the email.  Please try again.").show();
												// ahtb.alert("Sorry, that phone number didn't match the email.  Please try again.");
											}
										});
									}									
								}
							})

							$('button#simpleRegister').on('click', function() {
								var checked = $('input#agree').prop('checked');
								if (!checked) {
									$('span#warning').show();
								}
								else {
									if (typeof registerCB == 'function') {
										$('div#agent-verify').fadeOut({	duration: 1000,
																		start: function() {
																			registerCB();
																		}
																	 });
										var parentSelector = parentEle.selector;
										// var buttonSelector = buttonEle.selector;
										$(parentSelector).animate({opacity: 1},{duration: 1000});
										// $(buttonSelector).prop('disabled', false);
										
									}
								}
							})
						}
					});
				}
			},
			error: function(d) {
				var msg = typeof d == 'string' ? d : "We're very sorry, had trouble finding you into the system.";
				parentEle.animate({opacity: 1},{duration: 1000});
				buttonEle.prop('disabled', false);
				var curZ = parseInt( parentEle.css('z-index') );
				$('div#agent-verify').css('z-index', curZ-2); 
				if (ahtb.opened) {
					ahtb.push();
				}
				if (typeof d == 'object') {
					if (typeof d.msg != 'undefined')
						msg += "  "+d.msg;
					else if (typeof d.data != 'undefined' &&
							 typeof d.data == 'string')
						msg = d.data;
					
					ahtb.open({html: msg,
							  width: 450,
							  height: 110,
							  buttons:[{text:'OK', action:function() {
							  	$('div#agent-verify').css('z-index', curZ+2); 
							  	ahtb.pop();
							  }}]});
				}
				else {
					console.log("Failed: "+msg);
					ahtb.open({ html:msg, 
								height: 150, 
								width: 450,
								buttons:[{text:'OK', action:function() {
								  	$('div#agent-verify').css('z-index', curZ+2); 
								  	ahtb.pop();
								  }}]});
				}
			},
			fail: function(d) {
				var msg = typeof d == 'string' ? d : "We're very sorry, there was a system failure.";
				parentEle.animate({opacity: 1},{duration: 1000});
				buttonEle.prop('disabled', false);

				var curZ = parseInt( parentEle.css('z-index') );
				$('div#agent-verify').css('z-index', curZ-2); 
				if (ahtb.opened) {
					ahtb.push();
				}
				if (typeof d == 'object') {
					if (typeof d.msg != 'undefined')
						msg += "  "+d.msg;
					else if (typeof d.responseText != 'undefined' &&
							d.responseText.length)
						msg += "  "+d.responseText;
					else if (typeof d.statusText != 'undefined' &&
							d.statusText.length &&
							d.statusText != 'error')
						msg += "  "+d.statusText;
					else if (typeof d.status != 'undefined' &&
							 typeof d.status == 'string' &&
							 d.status.length)
						msg += "  "+d.status;
					
					ahtb.open({html: msg,
							  width: 450,
							  height: 110 + (Math.floor(msg.length / 60) * 25),
							  buttons:[{text:'OK', action:function() {
							  	$('div#agent-verify').css('z-index', curZ+2); 
							  	ahtb.pop();
							  }}]});
				}
				else {
					console.log("Failed: "+msg);
					ahtb.open({ html:msg, 
								height: 150, 
								width: 450,
								buttons:[{text:'OK', action:function() {
								  	$('div#agent-verify').css('z-index', curZ+2); 
								  	ahtb.pop();
								  }}]});
				}
			}
	});
}