
function setupAutocomplete(element) {
	if (!controller)
		return;

	var myCities = [];
	for(var i in cities)
		myCities[i] = cities[i].label;

	element.autocomplete({
    //source: cities,
    source: function( request, response ) {
          var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
          response( $.grep( myCities, function( item ){
              var match = matcher.test( item );
              return match;
          }) );
      },
    select: function(e, ui){ 
      if (ui.item.value != null) {
      	for(var i in cities)
      		if (ui.item.label == cities[i].label) {
      			ui.item.id = cities[i].id;
      			break;
      		}
        element.attr('city_id', ui.item.id); 
        element.attr('city_name', ui.item.label); 
        testCityName = ui.item.label;
        testCityId = ui.item.id;
        element.val(testCityName);
        element.change();
        controller.showAll();
      }
    }
  }).on('focus',function(){
    if ($(this).val() == 'ENTER CITY') {
      $(this).val(''); $(this).removeClass('inactive');
    }
  // }).on('keypress', function(e){
  }).on('keyup', function(e){
    var keynum = e.keyCode || e.which;  //for compatibility with IE < 9
    if(keynum == 13) {//13 is the enter char code
      e.preventDefault();
      $('ul.ui-autocomplete').hide();
      getCityId(0, $(this).val(), controller.showAll);
    }
    $(this).attr('city_id', 0);
    $(this).attr('city_name', '');	
    testCityId = 0;
    controller.showAll();
  });
}

function pickedCity(wasPushed, city_id, city, callback) {
  testCityName = city;
  for(var i in cities)
    if (testCityName == cities[i].label) {
      city_id = cities[i].id-cityLimit;
      break;
    }
	$('.autocomplete input').attr('city_id', city_id); 
  $('.autocomplete input').attr('city_name', city); 
	$('.autocomplete input').val(city);

	testCityId = city_id;
	if (wasPushed)
		ahtb.pop();
	else if (ahtb.opened)
		ahtb.close();

	window.setTimeout(function() {
		if (typeof callback == 'function')
            callback(QuizType.QUIZ_LOCALIZED, [city_id], (typeof controller.doDistance != 'undefined' ? (controller.doDistance ? controller.distance : 0) : controller.distance) );
        else
        	controller.showAll(); // default
	}, 350);
}

function getCityId(id, city, callback) {
	var haveAhtbOpen = ahtb.opened;
	$.post(ah_local.tp+'/_pages/ajax-quiz.php',
        { query:'find-city',
          data: { city: city,
          		  distance: 0 }
        },
        function(){}, 'JSON')
		.done(function(d){
			if (d &&
                d.data &&
                d.data.city &&
                (typeof d.data.city == 'object' ||
                d.data.city.length) ){
                if (d.status == 'OK' &&
                    (typeof d.data.city == 'object' || d.data.city.length) ) {							                        
                    console.log(JSON.stringify(d.data));

                	if (d.data.allState == 'true' || d.data.allState == true || d.data.allState == 1) {// doing statewide
                		if (typeof callback == 'function')
                        	callback(QuizType.QUIZ_STATE, [d.data.city[0]], 0);
                        return;
                    }

                    var len = length(d.data.city); //(typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
                	// var len = (typeof d.data.city == 'object') ? Object.keys(d.data.city).length : d.data.city.length;
					if (len > 1) {
						if (d.data.allState == 'false' || d.data.allState == false) {
	                        var h = '<p>Please pick one of the cities:</p>' +
	                                  '<ul class="cities">';
	                        for(var i in d.data.city) {
	                        	var location = d.data.city[i].city+', '+d.data.city[i].state;
	                          	h += '<li><a href="javascript:pickedCity('+0+','+d.data.city[i].id+",'"+location+"'"+');" >'+location+'</a></li>';
	                        }
	                        h += '</ul>';
	                        if (haveAhtbOpen) ahtb.push();
	                        window.setTimeout(function() {
	                        	ahtb.open(
		                          { title: 'Cities List',
		                            width: 380,
		                            height: (150 + (len * 25)) < 680 ? (150 + (len * 25)) : 680,
		                            html: h,
		                            buttons: [
		                                  {text: 'Cancel', action: function() {
		                                    haveAhtbOpen ? ahtb.pop() : ahtb.close();
		                                  }}],
		                            opened: function() {
		                            }
		                          })
	                        }, 150);
	                        
	                    }
	                    else {
	                    	if (haveAhtbOpen) ahtb.push();
	                    	window.setTimeout(function() {
	                    		ahtb.alert("Only one city/state combination accepted.", 
	                    			{height: 150,
	                    			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		haveAhtbOpen ? ahtb.pop() : ahtb.close();
			 						 	}}]});
	                    	}, 150);
                          	
                        }
                    } // only one!
                    else {
                    	pickedCity(0, d.data.city[0].id, d.data.city[0].city+', '+d.data.city[0].state, callback);
                    }
                }
                else {
                	if (haveAhtbOpen) ahtb.push();
                	window.setTimeout(function() {
                		ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		haveAhtbOpen ? ahtb.pop() : ahtb.close();
			 						 	}}]});
                	}, 150);
                }
            }
            else {
            	if (haveAhtbOpen) ahtb.push();
            	window.setTimeout(function() {
	                ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		haveAhtbOpen ? ahtb.pop() : ahtb.close();
			 						 	}}]});
	            }, 150);
            }
        })
        .fail(function(d){
        	// ahtb.push();
            window.setTimeout(function() {
	        	ahtb.alert(d && d.data ? d.data : 'Failed find a matching city id for '+city, 
	                  			{title: 'City Match', 
	                  			 height:180, 
	                  			 width:600,
	                  			 buttons:[
			 						 	{text:'OK', action: function() {
			 						 		haveAhtbOpen ? ahtb.pop() : ahtb.close();
			 						 	}}]});
	        }, 150);
        });
}
