		</div><!-- /main -->
    <!--<div class="push"></div>-->
	</div><!-- /content -->  
	<?php global $ALR; if ( is_user_logged_in() ) $seller = $ALR->get('seller'); ?>
		<footer data-role="footer" id="page-footer">
				<?php if (!is_user_logged_in()): ?>
						<span></span>
						<?php else: ?>
						<span class="mobilelogout"><a href="javascript:logout('<?php echo wp_logout_url(get_bloginfo('wpurl')); ?>');" title="Log Out">Log Out</a></span>
				<?php endif; ?>
			<section class="listing-agent">
				<script type="text/javascript">
				jQuery(document).ready(function($){
					if (typeof listingAgentDetail != 'undefined')
						$('.listing-agent').append(listingAgentDetail);
				})
				</script>
			</section>
			<section class="contact">
				<h4>Member Services</h4>
				<span class="phone">831.508.8821</span>
				<span class="hours">MON-FRI 9am-5pm PST</span><br/>
				Info@lifestyledlistings.com<br/>
        877 Cedar St. #150<br/>
        Santa Cruz, CA 95060
			</section>
			<section class="navigation">
				<h4>Quick Links</h4>
				<ul class="footer-nav">
					<li><a href="<?php bloginfo('wpurl'); ?>/">Home</a></li>
					<li><a href="<?php bloginfo('wpurl'); ?>/contact">Contact Us</a></li>
					<!--<li><a href="<?php bloginfo('wpurl'); ?>/">FAQ</a></li>-->
				<?php if (isset($seller)) : ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/sellers">My Profile</a></li>
				<?php endif; ?>
					<?php 
					global $showAgentBenefits;
					if ($showAgentBenefits) : ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/agent-benefits">Agent Benefits</a></li>
					<?php endif; ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/about">About Us</a></li>
					<li><a href="<?php bloginfo('wpurl'); ?>/terms">Terms &amp; Conditions</a></li>
					<!--<li><a href="<?php bloginfo('wpurl'); ?>">Buyers</a></li>-->
				<?php if (isset($seller)) : ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/sellers">Advertise</a></li>
				<?php endif; ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/terms">Privacy Policy</a></li>
					<li><a href="<?php bloginfo('wpurl'); ?>/faq">FAQ</a></li>
				<!--<?php if (isset($seller)) { ?>
					<li><a href="<?php bloginfo('wpurl'); ?>/sellers">Sellers</a></li>
				<?php } else if (is_user_logged_in()) { ?>
					<li><a class="sell-a-home" href="javascript:void(0);" onclick="register();" >Sellers</a></li>
				<?php } else { ?>
					<li><a class="sell-a-home" href="<?php bloginfo('wpurl'); ?>/register/seller">Sellers</a></li>
				<?php } ?>-->
					<li><a href="https://www.youtube.com/channel/UCQL2mnFws64hYDKqA6EgkoQ" target="_blank">Media</a></li>
					<li><a href="https://www.lifestyledlistingsproduct.com" target="_blank">For Agents</a></li>
				</ul>
			</section>
			<section class="social-media">
				<h4>Follow Us</h4>
				<ul class="footer-social">
					<li><a href="https://www.facebook.com/lifestyledlistings" target="_blank" class="entypo-facebook-circled"></a></li>
					<li><a href="https://plus.google.com/u/0/117554602398161835731" target="_blank" class="entypo-gplus-circled"></a></li>
					<li><a href="https://twitter.com/lifestyledRE" target="_blank" class="entypo-twitter-circled"></a></li>
					<li><a href="https://instagram.com/lifestyledlistings/" target="_blank" class="entypo-instagrem" style="padding-left:2px;"></a></li>
				</ul>
				<h4 style="margin-bottom:0">Share Us</h4>
				<?php 
				global $nickname;
				global $havePortal;
				?>
				<ul class="footer-social" style="margin:0">
					<li><a id="facebook-share" href="javascript:shareSocialNetwork('FB',0,'https://www.facebook.com/sharer/sharer.php?u=<?php echo get_home_url().($havePortal && !empty($nickname) ? "/$nickname" : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/facebook.png'; ?>" /></a></li>
					<li><a id="linkedin-share" style="margin-left:5px" href="javascript:shareLinkedIn({	comment:'Hi friends check out this new way to find homes that fit your lifestyle at LifeStyledListings.com!', 
																										title:'Revolutionary way to find homes -> LifeStyled Listings', 
																										description:'Check out all the listings that match your lifestyle!', 
																										url:'<?php echo get_home_url().($havePortal && !empty($nickname) ? "/$nickname" : ''); ?>', 
																										img:'<?php echo get_template_directory_uri().'/_img/_banners/linkedin/Linked-in-ski.jpg'; ?>',
																										where: 0
																									  });"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/linkedin.png'; ?>" /></a></li>
					<li><a id="googleplus-share" style="margin-left:5px" href="javascript:shareSocialNetwork('GP',0,'https://plus.google.com/share?url=<?php echo get_home_url().($havePortal && !empty($nickname) ? "/$nickname" : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/googleplus.png'; ?>" /></a></li>
					<li><a id="twitter-share" style="margin-left:5px" href="javascript:shareSocialNetwork('TW',0,'https://twitter.com/share?url=<?php echo get_home_url().($havePortal && !empty($nickname) ? "/$nickname" : ''); ?>')" target="_blank"><img style="width:34px; height:34px; margin-top:10px; vertical-align:sub" src="<?php echo get_template_directory_uri().'/_img/_promo/twitter.png'; ?>" /></a></li>
				</ul>
			</section>
			<section class="copyright">&copy; 2017 Lifestyled Listings. All rights reserved.
			</section>
			<div>
			  <span id="fb_status"></span>
			</div>
		</footer>
</div><!-- /page -->
<?php wp_footer(); ?>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>